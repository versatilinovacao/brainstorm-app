CREATE SCHEMA word;
CREATE SCHEMA excel;
CREATE SCHEMA corel;
CREATE SCHEMA photoshop;
CREATE SCHEMA reader_pdf;
CREATE SCHEMA jpg;
CREATE SCHEMA png;
CREATE SCHEMA mp4;
CREATE SCHEMA txt;

CREATE TABLE mp4.arquivos(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					BYTE
);

CREATE TABLE png.arquivos(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					BYTE
);
