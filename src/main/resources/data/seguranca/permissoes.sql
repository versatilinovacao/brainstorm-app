-- Table: permissao

-- DROP TABLE permissao;

CREATE TABLE permissao
(
  permissao_id serial NOT NULL,
  nome character varying(100),
  label character varying(100),
  descricao character varying(50),
  CONSTRAINT permissao_pk PRIMARY KEY (permissao_id),
  CONSTRAINT permissao_unica UNIQUE (nome)
)
