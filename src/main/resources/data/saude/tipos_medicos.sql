CREATE TABLE tipos_medicos(
	tipomedico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(60) NOT NULL UNIQUE
);

INSERT INTO tipos_medicos(nome) VALUES('alopata'),('homeopata');

CREATE INDEX nome_idx	ON tipos_medicos	USING hash(nome);