CREATE TABLE doencas(
	doença_id			BIGSERIAL NOT NULL PRIMARY KEY,
	nome				VARCHAR(100) NOT NULL UNIQUE,
	tipodoenca_id			BIGINT NOT NULL REFERENCES tipos_doencas(tipodoenca_id)
);

CREATE INDEX tipodoenca_idx	ON doencas	USING btree(tipodoenca_id);

INSERT INTO doencas(nome,tipodoenca_id) VALUES('Deficiência mental',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Nariz achatado ou ausente',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Fissura labial',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Planda dos pés arredondados',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Rosto muito alongado',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Orelhas muito baixas',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));

INSERT INTO doencas(nome,tipodoenca_id) VALUES('Epilético',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Hemofilico',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('hipertenso',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Asmático',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Diabético',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Cardiáco',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));

CREATE TABLE tipos_medicos(
	tipomedico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(60) NOT NULL UNIQUE
);

INSERT INTO tipos_medicos(nome) VALUES('alopata'),('homeopata');

CREATE TABLE necessidades_especiais(
	necessidadeespecial_id			BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100);
);

INSERT INTO necessidades_especiais(nome) VALUES('Cego'),('Surdo'),('Mudo'),('Hemiplégico'),('Paraplégico'),('Tetraplégico'),('Distúrbio de aprendizagem'),
		    			       ('Transtorno de déficit de atenção com hiperatividade'),('Síndrome de Down'),('Miopia'),('Hipermetropia'),
					       ('Astigmatismo'),('Presbiopia'),('Estrabismo'),('Glaucoma'),('Catarata'),('Descolamento retina'),
					       ('Ausencia de olfato');

CREATE TABLE tipos_alegicos(
	tipoalergico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO tipos_alergicos(nome)	VALUES('Medicamentos'),('Alimentos'),('Embalagens');

CREATE TABLE alergenicos(
	alergenico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	descricao				VARCHAR(500),
	tipoalergico_id				BIGINT NOT NULL REFERENCES medicina.tipos_alergicos(tipoalergico_id)
);

CREATE INDEX tipoalergico_idx	ON alergenicos		USING bree(tipoalergico_id);

INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Crustáceos','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Ovos','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Peixes','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Amendoim','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Soja','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Leites','Origem animal',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Amêndoa','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Avelãs','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Castanha-de-caju','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Castanha-do-pará','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Trigo','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Centeio','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Cevada','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Aveia','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Macadâmias','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Nozes','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Pecãs','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Pistaches','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
INSERT INTO alergenicos(nome,descricao,tipoalergico_id) VALUES('Pinoli','',(SELECT tipoalegico_id FROM medicina.tipos_alergicos WHERE nome = "Alimentos"));
