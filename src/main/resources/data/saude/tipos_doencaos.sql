CREATE TABLE tipos_doencas(
	tipodoenca_id			BIGSERIAL NOT NULL PRIMARY KEY,
	nome				VARCHAR(100) NOT NULL UNIQUE,
	descricao			VARCHAR(255)
);

CREATE INDEX tipo_doenca_nome_idx	ON tipos_doencas	USING btree(nome);

INSERT INTO tipos_doencas(nome,descricao)	VALUES('Congênita','Doença adquirida com o nascimento'),('Genética','Doença produzida por alterações no DNA'),('Comorbidades','Coexistência de doenças'),('Viral','Doença contraida por virus'),('Bacteriana','Doença desenvolvida por ação de bactérias'),('Cronicas','São doenças de longa duração e de progressão lenta, e muitas, infelizmente,  ainda não tem cura');
