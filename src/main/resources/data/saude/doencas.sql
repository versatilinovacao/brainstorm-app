CREATE TABLE doencas(
	doenca_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	tipodoenca_id			BIGINT NOT NULL REFERENCES tipos_doencas(tipodoenca_id)
);

CREATE INDEX tipodoenca_idx	ON doencas	USING btree(tipodoenca_id);

INSERT INTO doencas(nome,tipodoenca_id) VALUES('Deficiência mental',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Nariz achatado ou ausente',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Fissura labial',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Planda dos pés arredondados',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Rosto muito alongado',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Orelhas muito baixas',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Congênita'));

INSERT INTO doencas(nome,tipodoenca_id) VALUES('Epilético',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Hemofilico',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('hipertenso',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Asmático',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Diabético',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO doencas(nome,tipodoenca_id) VALUES('Cardiáco',(SELECT tipodoenca_id FROM tipos_doencas WHERE nome = 'Cronica'));