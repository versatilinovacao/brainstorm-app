CREATE OR REPLACE FUNCTION dblink_tiposdoencas() RETURNS SETOF record_tiposdoencas AS $$
DECLARE
	reg			record_tiposdoencas%ROWTYPE;
	conexao		text;
BEGIN
	SELECT findbyserver INTO conexao FROM findbyserver('saude_db');
	FOR reg IN 
		SELECT tipodoenca_id,nome,descricao
		FROM( 
		SELECT * FROM public.dblink (conexao,'select tipodoenca_id,nome,descricao from public.tipos_doencas')  AS DATA(tipodoenca_id BIGINT, nome VARCHAR, descricao VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.tiposdoencas_view AS 
select tipodoenca_id, nome, descricao from dblink_tiposdoencas(); 