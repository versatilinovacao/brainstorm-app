CREATE SCHEMA odi;
CREATE TABLE odi.cep_import(
	cep_id				VARCHAR(8) NOT NULL PRIMARY KEY,
	logradouro			VARCHAR(100) NOT NULL,
	complemento			VARCHAR(60),
	bairro				VARCHAR(60),
	localidade			VARCHAR(60),
	uf				VARCHAR(3),
	unidade				VARCHAR(5),
	ibge				VARCHAR(10),
	gia				VARCHAR(20)
);
