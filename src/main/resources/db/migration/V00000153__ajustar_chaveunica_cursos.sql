ALTER TABLE public.cursos DROP CONSTRAINT cursos_descricao_key;
ALTER TABLE cursos 	  ADD CONSTRAINT cursos_descricao_key UNIQUE (descricao, periodo_letivo_id);
