CREATE TYPE record_doencas	AS ( doenca_id BIGINT, nome VARCHAR, tipodoenca_id BIGINT);

CREATE OR REPLACE FUNCTION dblink_doencas() RETURNS SETOF record_doencas AS $$
DECLARE
	reg	record_doencas%ROWTYPE;
BEGIN
	FOR reg IN 
		SELECT doenca_id,nome,tipodoenca_id
		FROM( 
		SELECT * FROM public.dblink ('saude_server','select doenca_id,nome,tipodoenca_id from public.doencas')  AS DATA(doenca_id BIGINT, nome VARCHAR, tipodoenca_id BIGINT)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.doencas_view AS 
select doenca_id, nome, tipodoenca_id from dblink_doencas(); 
