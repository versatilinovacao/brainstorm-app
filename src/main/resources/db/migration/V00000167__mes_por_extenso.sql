CREATE OR REPLACE FUNCTION mes_por_extenso(double precision) RETURNS VARCHAR AS $$
DECLARE
	_retorno		VARCHAR;			
BEGIN
	IF ($1 = 1) THEN
		_retorno = 'Janeiro';
	ELSIF ($1 = 2) THEN
		_retorno = 'Fevereiro';
	ELSIF ($1 = 3) THEN
		_retorno = 'Março';
	ELSIF ($1 = 4) THEN
		_retorno = 'Abril';
	ELSIF ($1 = 5) THEN
		_retorno = 'Maio';
	ELSIF ($1 = 6) THEN
		_retorno = 'Junho';
	ELSIF ($1 = 7) THEN
		_retorno = 'Julho';
	ELSIF ($1 = 8) THEN
		_retorno = 'Agosto';
	ELSIF ($1 = 9) THEN
		_retorno = 'Setembro';
	ELSIF ($1 = 10) THEN
		_retorno = 'Outubro';
	ELSIF ($1 = 11) THEN
		_retorno = 'Novembro';
	ELSE
		_retorno = 'Dezembro';
	END IF;

	RETURN _retorno;
END; $$
LANGUAGE plpgsql;

CREATE VIEW escola.frequencias AS
select 
	_chamada_id,
	_caderno_id,
	_aluno_id,
	nome_aluno,

	(select c.descricao as nome_curso from escola.gradecurricular g
	inner join public.cursos c on c.curso_id = g.curso_id
	left join public.turmas t on t.turma_id = g.turma_id
	where g.gradecurricular_id = (select gradecurricular_id from escola.cadernos where caderno_id = _caderno_id)) as nome_curso,

	(select t.descricao as nome_turma from escola.gradecurricular g
	inner join public.cursos c on c.curso_id = g.curso_id
	left join public.turmas t on t.turma_id = g.turma_id
	where g.gradecurricular_id = (select gradecurricular_id from escola.cadernos where caderno_id = _caderno_id)) as nome_turma,

	c.descricao as nome_caderno,
	_um,_dois,_tres,_quatro,_cinco,_seis,_sete,_oito,_nove,_dez,_onze,_doze,_treze,_quatorze,_quinze,_dezesseis,_dezessete,_dezoito,_dezenove,
	_vinte,_vinteum,_vintedois,_vintetres,_vintequatro,_vintecinco,_vinteseis,_vintesete,_vinteoito,_vintenove,_trinta,_trintaeum,
	_mes,mes_por_extenso(_mes),
	_ano	
	
   from escola.chamadas_report_view
	inner join escola.cadernos c on c.caderno_id = _caderno_id;
