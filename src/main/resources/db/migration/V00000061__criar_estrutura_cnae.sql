CREATE SCHEMA monitoramento;
CREATE SCHEMA contabil;
CREATE SCHEMA temporario;

CREATE TABLE erros.importacao_cnae(
	importacao_cnae_id						SERIAL NOT NULL PRIMARY KEY,
	rotina								VARCHAR(50),
	erro								VARCHAR(255)
);

CREATE TABLE public.empresa(
	empresa_id							SERIAL NOT NULL PRIMARY KEY
);

CREATE TABLE contabil.atividades(
	atividade_id						SERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(100) NOT NULL UNIQUE,
	classificacao_id					BIGINT
);

ALTER TABLE public.colaborador ADD COLUMN atividade_id			BIGINT;
ALTER TABLE public.colaborador ADD CONSTRAINT colaborador_atividade_fk FOREIGN KEY(atividade_id) REFERENCES contabil.atividades(atividade_id);

INSERT INTO contabil.atividades(descricao) VALUES('ESCOLA'),('ACADEMIA'),('FARMÁCIA'),('RESTAURANTE'),('LOJA/MERCADO');
INSERT INTO contabil.atividades(descricao) VALUES('TRANSPORTADORA'),('LOJA MATERIAIS'),('SALÃO BELESA');
INSERT INTO contabil.atividades(descricao) VALUES('DISTRIBUIDORA'),('LOJA ROUPA/CALÇADO'),('MECÂNICA'),('IMOBILIÁRIA');
INSERT INTO contabil.atividades(descricao) VALUES('REVENDA VEICULO'),('PADARIA'),('CONSTRUTORA'),('ESTACIONAMENTO');
INSERT INTO contabil.atividades(descricao) VALUES('HOTEL'),('LANCHONETE'),('PROVEDORES INTERNET/SISTEMA');
INSERT INTO contabil.atividades(descricao) VALUES('CONSERTO COMPUTADOR'),('PRODUTOR'),('FABRICA'),('LOCADORA'),('SEGURADORA');

CREATE TABLE contabil.classificacoes(
	classificacao_id					SERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(60) NOT NULL UNIQUE
);

ALTER TABLE contabil.atividades ADD CONSTRAINT classificacao_fk FOREIGN KEY(classificacao_id) REFERENCES contabil.classificacoes(classificacao_id);

INSERT INTO contabil.classificacoes(descricao) VALUES('INDUSTRIA'),('COMÉRCIO'),('SERVIÇO');

UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'ESCOLA';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'ACADEMIA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'FARMÁCIA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'RESTAURANTE';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'LOJA/MERCADO';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'TRANSPORTADORA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'LOJA MATERIAIS';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'SALÃO BELESA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'DISTRIBUIDORA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'LOJA ROUPA/CALÇADO';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'MECÂNICA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'IMOBILIÁRIA';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'REVENDA VEICULO';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'PADARIA';
UPDATE contabil.atividades SET classificacao_id = 1 WHERE descricao = 'CONSTRUTORA';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'ESTACIONAMENTO';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'HOTEL';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'LANCHONETE';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'PROVEDORES INTERNET/SISTEMA';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'CONSERTO COMPUTADOR';
UPDATE contabil.atividades SET classificacao_id = 2 WHERE descricao = 'PRODUTOR';
UPDATE contabil.atividades SET classificacao_id = 1 WHERE descricao = 'FABRICA';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'LOCADORA';
UPDATE contabil.atividades SET classificacao_id = 3 WHERE descricao = 'SEGURADORA';

CREATE TABLE temporario.cnae(
	secao_id					SERIAL NOT NULL PRIMARY KEY,
	secao						VARCHAR(1),
	divisao						VARCHAR(2),
	grupo						VARCHAR(6),
	classe						VARCHAR(12),
	denominacao					VARCHAR(500)
);

CREATE TABLE contabil.cnae(
	cnae_id						SERIAL NOT NULL PRIMARY KEY,
	cnae_composto_id				BIGINT REFERENCES contabil.cnae(cnae_id),
	descricao					VARCHAR(500),
	codigo						VARCHAR(12),
	secao						VARCHAR(1)
);

CREATE TABLE monitoramento.importando_cnae(
	importando_cnae_id				SERIAL NOT NULL PRIMARY KEY,
	total_registros					BIGINT,
	percentual_concluido			NUMERIC(16,3),
	executando						BOOLEAN,
	preparando						BOOLEAN,
	falha							BOOLEAN,
	concluido						BOOLEAN,
	start							TIMESTAMP,
	finish							TIMESTAMP
);

CREATE TABLE action.importar_cnae(
	importar_cnae					SERIAL NOT NULL PRIMARY KEY,
	arquivo							TEXT
);

CREATE OR REPLACE FUNCTION action.importar_cnae_executar() RETURNS trigger AS $$
DECLARE
	existe				BOOLEAN;
	id				BIGINT;
	id_pai				BIGINT[];
	monitor_id			BIGINT;
	registros_processar		BIGINT;
	import				temporario.cnae%ROWTYPE;
	sql				VARCHAR;
	linha				BIGINT;

	contador			BIGINT;
BEGIN
	LOCK TABLE temporario.cnae IN SHARE MODE;
	INSERT INTO monitoramento.importando_cnae(start,falha,concluido,preparando) VALUES(now(),false,false,true) RETURNING importando_cnae_id INTO monitor_id;

	-- SELECT verificar se a tabela temporária já existe;
--	SELECT count(1) > 0 INTO existe FROM pg_catalog.pg_tables WHERE tablename = 'cnae_t';

--	sql = 'COPY temporario.cnae(secao,divisao,grupo,classe,denominacao) FROM "' || NEW.arquivo || '" USING delimiters;';
	BEGIN 
		sql = NEW.arquivo;
		EXECUTE 'COPY temporario.cnae(secao,divisao,grupo,classe,denominacao) FROM ''/var/tmp/cnae.csv'' USING delimiters '';''';
--		EXECUTE sql;
	EXCEPTION WHEN OTHERS THEN
		--ROLLBACK;
		INSERT INTO erros.importacao_cnae(rotina,erro) VALUES('Importação Arquivo','Não foi possível carregar o arquivo para importação ');
		UPDATE monitoramento.importando_cnae 
			SET total_registros = 0, percentual_concluido = 0, executando = FALSE, preparando = FALSE, falha = TRUE, concluido = FALSE, finesh = now()
				WHERE importando_cnae_id = monitor_id;
		--RETURN NEW;
	END;

	SELECT count(1) INTO registros_processar FROM temporario.cnae;
	UPDATE monitoramento.importando_cnae
		SET total_registros = registros_processar
			WHERE importando_cnae_id = monitor_id;
	
	UPDATE monitoramento.importando_cnae
		SET executando = TRUE, preparando = FALSE
			WHERE importando_cnae_id = monitor_id;
	
	linha = 0;
	
	SELECT count(1) INTO contador FROM temporario.cnae;
	
--	BEGIN 
		FOR import IN 
			SELECT * FROM temporario.cnae
		LOOP
			RAISE NOTICE 'PASSEI AQUI %',contador;		
			IF import.secao != '' THEN
				INSERT INTO contabil.cnae(secao,descricao) VALUES(import.secao,import.denominacao);
				SELECT MAX(cnae_id) INTO id FROM contabil.cnae;
				id_pai[1] = id;
			END IF;
			IF (import.secao = '') AND (import.divisao != '') THEN
				INSERT INTO contabil.cnae(secao,cnae_composto_id,descricao,codigo) VALUES(
					(SELECT secao FROM contabil.cnae WHERE cnae_id = id_pai[1]),
					id_pai[1],
					import.denominacao,
					'0'||CAST(import.divisao AS VARCHAR)
				);
				SELECT MAX(cnae_id) INTO id FROM contabil.cnae;
				id_pai[2] = id;
			END IF;
			SELECT MAX(cnae_id) INTO id FROM contabil.cnae;
			IF (import.secao = '') AND (import.divisao = '') AND (import.grupo != '') THEN
				INSERT INTO contabil.cnae(secao,cnae_composto_id,descricao,codigo) VALUES(
					(SELECT secao FROM contabil.cnae WHERE cnae_id = (SELECT cnae_id FROM contabil.cnae WHERE cnae_composto_id = id_pai[2] LIMIT 1)),
					id_pai[2],
					import.denominacao,
					'0'||CAST(import.grupo AS VARCHAR)
				);
				SELECT MAX(cnae_id) INTO id FROM contabil.cnae;
				id_pai[3] = id;
			END IF;
			IF (import.secao = '') AND (import.divisao = '') AND (import.grupo = '') AND (import.classe != '') THEN 
				INSERT INTO contabil.cnae(secao,cnae_composto_id,descricao,codigo) VALUES(
					(SELECT secao FROM contabil.cnae WHERE cnae_id = (SELECT cnae_id FROM contabil.cnae WHERE cnae_composto_id = id_pai[3] LIMIT 1)),
					id_pai[3],
					import.denominacao,
					'0'||CAST(import.classe AS VARCHAR)
				);
			END IF;
			
			linha = linha + 1;
			
			UPDATE monitoramento.importando_cnae 
				SET percentual_concluido = ((total_registros - linha) / total_registros) * 100
					WHERE importando_cnae_id = monitor_id;
					

		END LOOP;
		--COMMIT;
		
		UPDATE monitoramento.importando_cnae
			SET finish = now() 
				WHERE importando_cnae_id = monitor_id;

		DELETE FROM temporario.cnae;
	RETURN NEW;
--	EXCEPTION WHEN OTHERS THEN
		--ROLLBACK;
--		INSERT INTO erros.importacao_cnae(rotina,erro) VALUES('Importação CNAE','Importação apresentou erro na linha '||linha);
--		UPDATE monitoramento.importando_cnae 
--			SET total_registros = 0, percentual_concluido = 0, executando = FALSE, preparando = FALSE, falha = TRUE, concluido = FALSE, finesh = now()
--				WHERE importando_cnae_id = monitor_id;
--		DELETE FROM temporario.cnae;
--	END;
	
	
END;$$
LANGUAGE plpgsql;

CREATE TRIGGER importar_cnae_trigger AFTER INSERT ON action.importar_cnae FOR EACH ROW
	EXECUTE PROCEDURE action.importar_cnae_executar();

CREATE VIEW arvore_cnae_view AS 
	WITH RECURSIVE cnae_recursivo(codigo,nivel,descricao,cnae_id,cnae_composto_id) 
		AS (SELECT codigo,
				   1 AS nivel,
				   descricao,
				   cnae_id,	
				   cnae_composto_id
				   
			FROM contabil.cnae 
			WHERE COALESCE(cnae_composto_id,0) = 0
			
			UNION ALL
			
			SELECT a.codigo,
				   r.nivel + 1 AS nivel,
				   a.descricao,
				   a.cnae_id,
				   a.cnae_composto_id	
				   
			FROM contabil.cnae a
			INNER JOIN cnae_recursivo r 
				ON r.cnae_id = a.cnae_composto_id
		) SELECT codigo,nivel,descricao,cnae_id,cnae_composto_id FROM cnae_recursivo ORDER BY codigo;







