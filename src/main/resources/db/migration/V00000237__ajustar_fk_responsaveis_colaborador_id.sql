ALTER TABLE public.responsaveis_colaboradores ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.responsaveis_colaboradores
	ADD CONSTRAINT responsaveis_colaboradores_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_responsaveis_colaboradores_fk
	ON public.responsaveis_colaboradores
	USING btree(colaborador_id,composicao_colaborador_id);


DROP VIEW responsavel_view;
CREATE OR REPLACE VIEW responsavel_view AS 
 SELECT w.responsavel_id,
	w.nome,
	w.composicao_responsavel_id
   FROM ( SELECT r.responsavel_id,
            ( SELECT c1.nome
                   FROM colaborador c1
                  WHERE c1.colaborador_id = rc.responsavel_id) AS nome,
	    c.composicao_id AS composicao_responsavel_id
           FROM responsaveis r
             JOIN responsaveis_colaboradores rc ON rc.responsavel_id = r.responsavel_id 
             JOIN colaborador c ON c.colaborador_id = rc.colaborador_id and c.composicao_id = rc.composicao_colaborador_id
           WHERE c.status = true) w          
   GROUP BY w.responsavel_id, w.nome, w.composicao_responsavel_id;
