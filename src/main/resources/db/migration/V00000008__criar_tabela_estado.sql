CREATE TABLE estados(
	estado_id					BIGINT,
	descricao					VARCHAR(80),
	sigla						VARCHAR(3),
	regiao_id					BIGINT,
	pais_id						BIGINT			
);

ALTER TABLE estados ADD CONSTRAINT estado_pk PRIMARY KEY(estado_id);
ALTER TABLE estados ADD CONSTRAINT pais_fk FOREIGN KEY(pais_id) REFERENCES paises(pais_id);
ALTER TABLE regiao ADD CONSTRAINT regiao_fk FOREIGN KEY(regiao_id) REFERENCES regiao(regiao_id);