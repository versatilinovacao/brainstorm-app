INSERT INTO permissao(nome) VALUES('ROLE_TIPOSEMPRESAS_CONTEUDO'),('ROLE_TIPOSEMPRESAS_SALVAR'),('ROLE_TIPOSEMPRESAS_DELETAR');
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,95),(1,96),(1,97);

INSERT INTO permissao(nome) VALUES('ROLE_PORTESEMPRESAS_CONTEUDO'),('ROLE_PORTESEMPRESAS_SALVAR'),('ROLE_PORTESEMPRESAS_DELETAR');
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,98),(1,99),(1,100);

INSERT INTO permissao(nome) VALUES('ROLE_CLASSIFICACAOEMPRESAS_CONTEUDO'),('ROLE_CLASSIFICACAOEMPRESAS_SALVAR'),('ROLE_CLASSIFICACAOEMPRESAS_DELETAR');
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,101),(1,102),(1,103);

INSERT INTO permissao(nome) VALUES('ROLE_SEDEEMPRESAS_CONTEUDO'),('ROLE_SEDEEMPRESAS_SALVAR'),('ROLE_SEDEEMPRESAS_DELETAR');
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,104),(1,105),(1,106);

CREATE SCHEMA empresa;

CREATE TABLE empresa.tipos_empresa(
	tipo_empresa_id							SERIAL NOT NULL PRIMARY KEY,
	classificacao							VARCHAR(100) NOT NULL UNIQUE,
	descricao								TEXT
);

INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('Empresário Individual','Exerce em nome próprio uma atividade empresarial. Atua individualmente, sem sociedade. Sua responsabilidade é ilimitada (responde com seus bens pessoais pelas obrigações assumidas com a atividade empresarial). O empresário pode exercer atividade industrial, comercial ou prestação de serviços, exceto serviços de profissão intelectual.');
INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('MEI - Microempreendedor Individual','MEI - Microempreendedor Individual – é o empresário individual com receita bruta anual até R$ 60.000,00, e a partir de 2018, R$ 81.000,00, optante pelo Simples Nacional e SIMEI.
													 O Simples Nacional estabelece valores fixos mensais para o MEI, que não seja sócio, titular ou administrador de outra empresa, que possua no máximo 01 (um) empregado que receba exclusivamente o piso da categoria profissional, não tenha mais de um estabelecimento (não ter filial) e entre outros requisitos. Ver artigo 18-A da Lei Complementar n. 123, de 14 de dezembro de 2006.
													 O MEI paga os seus tributos na forma do SIMEI por valores fixos mensais (5% de um salário mínimo, relativo ao INSS do Empresário + R$ 1,00 relativo ao ICMS (indústria, comércio ou serviço de transporte intermunicipal ou interestadual) + R$ 5,00 relativos ao ISS (prestação de serviços). Está dispensado de escrituração contábil e é segurado da Previdência social - Contribuinte Individual (tem direito a alguns benefícios previdenciários, entre eles, a aposentadoria por idade).
													 O registro do MEI é gratuito e pode ser efetuado pela Internet através do site www.portaldoempreendedor.gov.br, onde é possível verificar as atividades permitidas e obter maiores informações. Vale lembrar que no caso de início de atividades no próprio ano-calendário, o limite de receita bruta acima mencionado será proporcional ao número de meses de atividade.');
INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('Empresa Individual de Responsabilidade Limitada – EIRELI','Atuação individual - sem sócios. Responsabilidade do empresário é limitada ao capital social (valor do investimento, em dinheiro ou bens). Obrigatoriedade de capital social integralizado de no mínimo 100 salários mínimos. A EIRELI possibilita a atuação individual – sem sócios – porém, com responsabilidade limitada. Protege o patrimônio pessoal do empresário através da separação patrimonial. A EIRELI é uma pessoa jurídica, com patrimônio próprio, não se confundindo com a pessoa física do empreendedor e seu respectivo patrimônio.
                                                     O empresário titular da EIRELI poderá responder com seu patrimônio pessoal por obrigações da empresa nas mesmas hipóteses previstas para as Sociedades Limitadas.');
INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('Sociedade Empresária','Neste tipo de empresa é possível a atuação coletiva entre dois ou mais sócios, sendo sua responsabilidade limitada ao capital social. Deverá adotar uma das espécies de sociedade existentes (S/A, Sociedade Limitada - LTDA, etc.). A espécie de sociedade empresária mais adotada no Brasil é a Sociedade Limitada (LTDA.), por ser mais simples e pela proteção ao patrimônio pessoal dos sócios.
                                                     Sociedade para o exercício da atividade própria de empresário (produção, circulação de bens e prestação de serviços, exceto profissão intelectual de natureza científica, literária ou artística). A Responsabilidade dos sócios é limitada ao capital social (os sócios não respondem com seus bens pessoais pelas obrigações da empresa após a integralização do capital social).
                                                     A Sociedade Empresária Limitada é pessoa jurídica que possui patrimônio próprio, não se confundindo com a pessoa física do dos sócios e seus respectivos patrimônios.
                                                     Os sócios podem responder com seus bens pessoais nos casos de comprovação de má-fé, sonegação fiscal, confusão patrimonial, estelionato, fraude contra credores e etc. Dívidas trabalhistas: A Justiça do Trabalho, recorrentemente, condena os sócios ao pagamento da dívida trabalhista com o patrimônio pessoal, no caso de os bens da empresa não serem suficientes.');
INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('Sociedade Simples','Empresa com atuação Coletiva, ou seja, 02 (dois) ou mais sócios. A responsabilidade dos sócios é ilimitada. Porém, poderá adotar a espécie societária de Sociedade Limitada - Sociedade Simples Ltda., passando a responsabilidade dos sócios a ser limitada ao capital social, não respondendo com seus bens pessoais pelas obrigações da sociedade, exceto nas hipóteses mencionadas no item anterior (sociedade empresária limitada).
                                                     A Sociedade Simples é uma pessoa jurídica para a prestação de serviços de profissão intelectual, de natureza científica, artística ou literária, sem elemento de empresa (ex. médicos, dentistas, engenheiros, arquitetos, etc.).');
INSERT INTO empresa.tipos_empresa(classificacao,descricao) VALUES('Sociedade Anônima (SA)','SAs são empreendimentos com capital social dividido em ações, diferente dos sistema de quotas utilizados por outros tipos de empresas.
                                                     A Sociedade Anônima é dividida ainda em dois subtipos:
                                                     SA de capital aberto: é a organização que vende ações na bolsa de valores ao público geral por intermediação de instituições financeiras, como bancos e corretoras;
                                                     SA de capital fechado: também tem o capital dividido em ações internamente entre os sócios e outros interessados ou convidados. Mas não conta com capital aberto ao público em bolsa de valores.');

CREATE TABLE empresa.portes(
	porte_id								SERIAL NOT NULL PRIMARY KEY,
	classificacao							VARCHAR(100) NOT NULL UNIQUE,
	descricao								TEXT,
	de										NUMERIC(16,4),
	ate										NUMERIC(16,4)
);

INSERT INTO empresa.portes(classificacao,descricao,de,ate) VALUES('Microempresa (ME)','Conforme a Lei Complementar 123, de 2006, o porte micro diz respeito às empresas que faturam no máximo R$ 360 mil por ano.
                                                                     Elas podem, desde que não exerçam atividade impeditiva, optarem pelo Simples Nacional.',0,360000);
INSERT INTO empresa.portes(classificacao,descricao,de,ate) VALUES('Empresa de Pequeno Porte (EPP)','É a empresa que fatura acima de R$ 360 mil por ano até o limite de R$ 3,6 milhões anuais. Como a ME, pode estar enquadrada no Simples se não desenvolver alguma atividade que o regime não permita.',360000,3600000);
INSERT INTO empresa.portes(classificacao,descricao,de,ate) VALUES('Empresas de médio e grande porte','Para a classificação de portes de empresas maiores, os órgãos públicos e de fiscalização utilizam diferentes critérios, como de número de funcionários. O Banco Nacional do Desenvolvimento (BNDES), por exemplo, usa o critério de faturamento:
                                                                       Acima de R$ 16 milhões até R$ 90 milhões por ano: média;
                                                                       Acima de R$ 90 milhões até R$ 300 milhões anualmente: média-grande;
                                                                       Após os R$ 300 milhões anuais: grande.
                                                                       Conforme o BNDES, negócios que faturam entre R$ 3,6 milhões e R$ 16 milhões anuais ainda são EPP. Mesmo assim, pela receita, não são autorizadas a optar pelo Simples.',16000000,0);

CREATE TABLE empresa.classificacao_empresa(
	classificacao_empresa_id				SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.classificacao_empresa(descricao) VALUES('Fabrica'),('Restaurante'),('Mercado'),('Loja'),('Locadora'),
							   ('Mecanica'),('Farmácia'),('Manutenção');

CREATE TABLE empresa.sede(
	sede_id									SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(100) NOT NULL UNIQUE,
	area_construida							NUMERIC(16,4),
	area_externa							NUMERIC(16,4),
	area_estacionamento						NUMERIC(16,4),
	area_total								NUMERIC(16,4)
);

CREATE TABLE empresa.empresa(
	empresa_id								SERIAL NOT NULL PRIMARY KEY,
	filial_id								BIGINT REFERENCES empresa.empresa(empresa_id),
	descricao								VARCHAR(100) NOT NULL UNIQUE,
	tipo_empresa_id							BIGINT NOT NULL,
	sede_id									BIGINT REFERENCES empresa.sede(sede_id),
	cnpj									VARCHAR(14) NOT NULL 
);

CREATE TABLE empresa.tipos_ambiente(
	tipo_ambiente_id						SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.tipos_ambiente(descricao) VALUES('SALA'),('COSINHA'),('DEPOSITO'),('PAVILHÃO'),('BANHEIRO'),
                                                    ('AREA'),('QUARTO'),('ESTACIONAMENTO'),('LOJA'),('GALERIA'),
                                                    ('TERRAÇO'),('SALÃO'),('ALMOXARIFADO'),('PATIO');

CREATE TABLE empresa.status_ambientes(
	status_ambiente_id						SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(100)
);

INSERT INTO empresa.status_ambientes(descricao) VALUES('DISPONÍVEL'),('OCUPADO'),('RESERVADO'),('INDISPONÍVEL'),
                                                      ('AGENDADO');
--Disponível   = Liberado para qualquer um alocar tempo de uso na sala
--Ocupado 	   = Situação em que a sala esta sendo utilizada naquele momento.
--Reservado	   = A sala foi reservada, mas ainda não tem um horário definido, em caso de conflito será fator de desempate. ????
--Indisponível = Sala não pode ser usada e nem agendada, esta fora do circuito de utilização.
--Agendado	   = Não esta em uso, mas quando chegar a data e hora definidos, será utilizada. Ainda não foi ocupada.

CREATE TABLE empresa.modelos_ambiente(
	modelo_ambiente_id						SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.modelos_ambiente(descricao) VALUES('PERMANENTE'),('ROTATIVO');
--Permanente são ambientes que não podem ser alocados para uso, eles tem finalidades previamente definidas.
--Rotativo são os ambientes que estão disponíveis para agendamento de curto, medio e longo prazo.

CREATE TABLE empresa.ambientes(
	ambiente_id								SERIAL NOT NULL PRIMARY KEY,
	sede_id									BIGINT NOT NULL REFERENCES empresa.sede(sede_id),
	descricao								VARCHAR(100) NOT NULL UNIQUE,
	tipo_ambiente_id						BIGINT NOT NULL REFERENCES empresa.tipos_ambiente(tipo_ambiente_id),
	modelo_ambiente_id						BIGINT NOT NULL REFERENCES empresa.modelos_ambiente(modelo_ambiente_id),
	metragem_quadrada						NUMERIC(16,4),
	metragem_cubica							NUMERIC(16,4),
	lotacao									NUMERIC(16,4),
	vagas									NUMERIC(16,4),
	tempo_maximo							TIME			--Tempo máximo de permanência no ambiente, apartir da sua alocação de horário, a ausência desta informação, torna a utilização por tempo indeterminado.
);

CREATE TABLE empresa.aloca_ambiente(
	evento									DATE NOT NULL,
	ambiente_id								BIGINT NOT NULL REFERENCES empresa.ambientes(ambiente_id),
	responsavel_id							BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id), --Quem alocou o ambiente
	aloca_horario							TIME NOT NULL,
	libera_horario							TIME
);


