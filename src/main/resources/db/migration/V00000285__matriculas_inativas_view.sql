DROP VIEW escola.matriculas_inativas_cursos_view;
							  
CREATE OR REPLACE VIEW escola.matriculas_inativas_cursos_view AS 
 SELECT m.matricula_id,
    m.codigo,
    m.colaborador_id,
    m.composicao_colaborador_id,
    m.situacao_matricula_id,
    m.curso_id,
    m.competencia_inicial,
    m.competencia_final,
    m.status,
    c.nome AS aluno,
    s.descricao AS situacao,
    cs.descricao AS curso
   FROM matriculas m
     JOIN colaborador c ON c.colaborador_id = m.colaborador_id
     LEFT JOIN situacao_matricula s ON s.situacao_matricula_id = m.situacao_matricula_id
     LEFT JOIN cursos cs ON cs.curso_id = m.curso_id
  WHERE m.status = false;