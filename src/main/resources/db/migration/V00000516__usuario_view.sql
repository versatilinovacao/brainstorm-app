DROP VIEW usuario_view;

CREATE OR REPLACE VIEW usuario_view AS 
 SELECT u.usuario_id,
    u.empresa_id,
    u.nome,
    u.status,
    u.email,
    c.colaborador_id,
    c.composicao_id AS composicao_colaborador_id,
    i.cpf,
    i.data_nascimento,
    c.email AS email_colaborador,
    i.foto_thumbnail,
    u.isserver,
    u.isclient,
    u.iscounter,
    u.issistema,
    (SELECT COUNT(1) > 0 FROM parceiros WHERE parceiro_id = c.parceiro_id AND cargo_id IN (511,512,514,517,617,820,1018)) as isprofessor
   FROM usuario u
     LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
     LEFT JOIN parceiros p ON p.parceiro_id = c.parceiro_id
  WHERE u.status = true;
