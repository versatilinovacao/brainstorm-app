CREATE TYPE record_enderecos	AS ( cep VARCHAR, logradouro VARCHAR, tipo_logradouro VARCHAR, complemento VARCHAR, local VARCHAR, cidade_id BIGINT, bairro_id BIGINT, tipologradouro_id BIGINT);

CREATE OR REPLACE FUNCTION dblink_enderecos() RETURNS SETOF record_enderecos AS $$
DECLARE
	reg	record_enderecos%ROWTYPE;
BEGIN

	FOR reg IN 
		SELECT cep, logradouro, tipo_logradouro, complemento, local, id_cidade AS cidade_id, id_bairro AS bairro_id, tipologradouro_id
		FROM( 
		SELECT * FROM public.dblink ('cep_server','select cep, logradouro, tipo_logradouro, complemento, local, id_cidade, id_bairro, tipologradouro_id from public.qualocep_endereco')  AS DATA(cep VARCHAR, logradouro VARCHAR,tipo_logradouro CHARACTER VARYING, complemento VARCHAR, local VARCHAR, id_cidade BIGINT, id_bairro BIGINT, tipologradouro_id BIGINT)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.enderecos_view AS 
select cep, logradouro, tipo_logradouro, complemento, local, cidade_id, bairro_id, tipologradouro_id from dblink_enderecos();
