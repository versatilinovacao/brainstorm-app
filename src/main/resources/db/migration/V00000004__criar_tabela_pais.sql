CREATE TABLE paises(
	pais_id					BIGINT NOT NULL,
	sigla					VARCHAR(3),
	descricao				VARCHAR(60),
	dt_ini					VARCHAR(10),
	dt_fim					VARCHAR(10)
);

ALTER TABLE paises ADD CONSTRAINT pais_pk PRIMARY KEY(pais_id);