DROP VIEW remoto.estados_view;
CREATE OR REPLACE VIEW remoto.estados_view AS 
 SELECT dblink_estados.estado_id,
    dblink_estados.sigla,
    dblink_estados.descricao,
    dblink_estados.codigo_ibge,
    dblink_estados.pais_id
   FROM dblink_estados() dblink_estados(estado_id, sigla, descricao, codigo_ibge, pais_id);