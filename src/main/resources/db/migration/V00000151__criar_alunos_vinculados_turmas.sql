CREATE VIEW escola.alunos_vinculados_turmas AS
	SELECT random() AS alunovinculadoturma_id,at.turma_id,at.colaborador_id,c.nome FROM alunos_turmas at
	INNER JOIN colaborador c ON c.colaborador_id = at.colaborador_id;
