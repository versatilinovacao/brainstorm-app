DROP TABLE public.classificacao_colaborador;
CREATE TABLE public.pill_colaborador(
	pill_colaborador_id				SERIAL NOT NULL PRIMARY KEY,
	label							VARCHAR(20) NOT NULL UNIQUE
); 
												     
INSERT INTO public.pill_colaborador(label) VALUES('Empresa'),('Cliente'),('Tercerizado'),('Parceiro');

CREATE TABLE public.pill_selecionado_colaborador(
	colaborador_id					BIGINT NOT NULL,
	pill_colaborador_id				BIGINT NOT NULL
);

ALTER TABLE public.pill_selecionado_colaborador	ADD CONSTRAINT pill_selecionado_colaborador_pk PRIMARY KEY(pill_colaborador_id,colaborador_id);
ALTER TABLE public.pill_selecionado_colaborador	ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);
ALTER TABLE public.pill_selecionado_colaborador	ADD CONSTRAINT pill_colaborador_fk FOREIGN KEY(pill_colaborador_id) REFERENCES public.pill_colaborador(pill_colaborador_id);

--Criar uma view 
CREATE VIEW pill_colaborador_view AS 
	SELECT c.pill_colaborador_id, c.label, cl.colaborador_id
		FROM pill_colaborador c
		INNER JOIN pill_selecionado_colaborador cl ON cl.pill_colaborador_id = c.pill_colaborador_id

