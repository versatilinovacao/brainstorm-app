ALTER TABLE administracao.contratos		ADD COLUMN formapagamento_id				BIGINT;
ALTER TABLE administracao.contratos		ADD CONSTRAINT formapagamento_fk	FOREIGN KEY(formapagamento_id)	REFERENCES financeiro.formas_pagamentos(formapagamento_id);
CREATE INDEX contrato_formapagamento_idx	ON administracao.contratos	USING btree(formapagamento_id);