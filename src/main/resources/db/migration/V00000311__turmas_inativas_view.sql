DROP VIEW escola.turmas_inativas_view;

CREATE OR REPLACE VIEW escola.turmas_inativas_view AS 
 SELECT t.turma_id,
    t.descricao,
    (SELECT MIN(periodo_inicial) FROM escola.periodos_letivos_itens WHERE periodo_letivo_id = p.periodo_letivo_id) AS inicio,
    (SELECT MAX(periodo_final) FROM escola.periodos_letivos_itens WHERE periodo_letivo_id = p.periodo_letivo_id) AS fim,
    t.status
   FROM turmas t
   INNER JOIN escola.gradecurricular g ON g.turma_id = t.turma_id
   INNER JOIN cursos c ON c.curso_id = g.curso_id
   INNER JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
  WHERE t.status = false;
