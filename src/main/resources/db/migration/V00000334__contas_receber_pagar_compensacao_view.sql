CREATE OR REPLACE VIEW financeiro.contas_receber_compensacao_view AS
SELECT c.contareceber_id,
       c.composicao_id,
       c.empresa_id,
       c.composicao_empresa_id,
       c1.nome AS nomeempresa,
       c.vencimento,
       c.emissao,
       c.pagamento,
       c.quitacao,
       c.devedor_id,
       c.composicao_devedor_id,
       c2.nome AS nomedevedor,
       c.desconto,
       c.valorpago,
       c.saldo,
       c.total,
       c.negociacao_id,
       n.descricao	AS descricaonegociacao,
       c.formapagamento_id,
       f.descricao	AS descricaoformapagamento,
       c.tipopagamento_id,
       p.descricao	AS descricaotipopagamento,
       c.status
FROM financeiro.contas_receber c
INNER JOIN public.colaborador c1 ON c1.colaborador_id = c.empresa_id AND c1.composicao_id = c.composicao_empresa_id
INNER JOIN public.colaborador c2 ON c2.colaborador_id = c.devedor_id AND c2.composicao_id = c.composicao_devedor_id
LEFT JOIN financeiro.negociacoes n ON n.negociacao_id = c.negociacao_id
LEFT JOIN financeiro.formas_pagamentos f ON f.formapagamento_id = c.formapagamento_id
LEFT JOIN financeiro.tipos_pagamentos p ON p.tipopagamento_id = c.tipopagamento_id
WHERE COALESCE(c.status,true) = true
  AND c.quitacao IS null;


CREATE OR REPLACE VIEW financeiro.contas_pagar_compensacao_view AS
SELECT c.contapagar_id,
       c.composicao_id,
       c.empresa_id,
       c.composicao_empresa_id,
       c1.nome AS nomeempresa,
       c.vencimento,
       c.emissao,
       c.pagamento,
       c.quitacao,
       c.credor_id,
       c.composicao_credor_id,
       c2.nome AS nomedevedor,
       c.desconto,
       c.valorpago,
       c.saldo,
       c.total,
       c.negociacao_id,
       n.descricao	AS descricaonegociacao,
       c.formapagamento_id,
       f.descricao	AS descricaoformapagamento,
       c.tipopagamento_id,
       p.descricao	AS descricaotipopagamento,
       c.status
FROM financeiro.contas_pagar c
INNER JOIN public.colaborador c1 ON c1.colaborador_id = c.empresa_id AND c1.composicao_id = c.composicao_empresa_id
INNER JOIN public.colaborador c2 ON c2.colaborador_id = c.credor_id AND c2.composicao_id = c.composicao_credor_id
LEFT JOIN financeiro.negociacoes n ON n.negociacao_id = c.negociacao_id
LEFT JOIN financeiro.formas_pagamentos f ON f.formapagamento_id = c.formapagamento_id
LEFT JOIN financeiro.tipos_pagamentos p ON p.tipopagamento_id = c.tipopagamento_id
WHERE COALESCE(c.status,true) = true
  AND c.quitacao IS null;
