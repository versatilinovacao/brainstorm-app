DROP TABLE administracao.contratos CASCADE;

CREATE TABLE administracao.contratos(
		contrato_id					SERIAL NOT NULL PRIMARY KEY,
        composicao_id				BIGINT REFERENCES administracao.contratos(contrato_id),
		contratada_id				BIGINT NOT NULL,
		composicao_contratada_id	BIGINT,
		contratante_id				BIGINT NOT NULL,
		composicao_contratante_id	BIGINT,
		artefato_id					BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
		inicio_contrato				DATE NOT NULL,
		duracao_inicial				INTEGER NOT NULL,
		renovacoes					INTEGER,
		mensalidade					NUMERIC(16,4),
		indicereajuste_id			BIGINT NOT NULL REFERENCES administracao.indices(indice_id),
		tiporenovacao_id			BIGINT NOT NULL REFERENCES administracao.tipos_renovacoes(tiporenovacao_id),
		parcelas_isentas			BIGINT,
		desconto					NUMERIC(16,4),
		ciclo_desconto				INTEGER,
		dia_pagamento				INTEGER NOT NULL,
		total						NUMERIC(16,4)
);

-- Index: administracao.contratos_contratado_id_composicao_contratado_id_idx

-- DROP INDEX administracao.contratos_contratado_id_composicao_contratado_id_idx;

CREATE INDEX contratos_contratado_id_composicao_contratado_id_idx
  ON administracao.contratos
  USING btree
  (contratada_id, composicao_contratada_id);

-- Index: administracao.contratos_contratante_id_composicao_contratante_id_idx

-- DROP INDEX administracao.contratos_contratante_id_composicao_contratante_id_idx;

CREATE INDEX contratos_contratante_id_composicao_contratante_id_idx
  ON administracao.contratos
  USING btree
  (contratante_id, composicao_contratante_id);

-- Index: administracao.contratos_contrato_id_empresa_id_composicao_empresa_id_idx

-- DROP INDEX administracao.contratos_contrato_id_empresa_id_composicao_empresa_id_idx;

CREATE INDEX contratos_contrato_id_empresa_id_composicao_empresa_id_idx
  ON administracao.contratos
  USING btree
  (contrato_id, contratada_id, composicao_contratada_id);

-- Index: administracao.contratos_empresa_id_composicao_empresa_id_idx

-- DROP INDEX administracao.contratos_empresa_id_composicao_empresa_id_idx;

CREATE INDEX contratos_empresa_id_composicao_empresa_id_idx
  ON administracao.contratos
  USING btree
  (contratada_id, composicao_contratada_id);

