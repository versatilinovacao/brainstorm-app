ALTER TABLE administracao.contratos		ADD COLUMN contratomodelo_id		BIGINT;
ALTER TABLE administracao.contratos		ADD CONSTRAINT contratomodelo_fk	FOREIGN KEY(contratomodelo_id)	REFERENCES contratos_modelos(contratomodelo_id);

CREATE INDEX contrato_contratomodelo_idx	ON administracao.contratos	USING btree(contratomodelo_id);