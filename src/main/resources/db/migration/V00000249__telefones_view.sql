DROP VIEW telefones_view;

CREATE OR REPLACE VIEW telefones_view AS 
 SELECT t.telefone_id,
    t.numero,
    t.ramal,
    t.tipo_telefone_id,
    tc.colaborador_id,
    tc.composicao_colaborador_id
   FROM telefones t
     LEFT JOIN telefones_colaboradores tc ON tc.telefone_id = t.telefone_id;