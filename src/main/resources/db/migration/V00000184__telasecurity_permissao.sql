CREATE TABLE public.telasecurity_permissao(
	telasistema_id								BIGINT NOT NULL REFERENCES public.telas_sistema(telasistema_id),
	permissao_id								BIGINT NOT NULL REFERENCES public.permissao(permissao_id)
);