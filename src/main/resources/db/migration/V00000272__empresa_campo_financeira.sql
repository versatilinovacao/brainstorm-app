ALTER TABLE empresa.empresa ADD COLUMN financeira_id			BIGINT;
ALTER TABLE empresa.empresa ADD COLUMN composicaofinanceira_id	BIGINT;
ALTER TABLE empresa.empresa ADD CONSTRAINT financeira_fk	FOREIGN KEY(financeira_id,composicaofinanceira_id)  REFERENCES colaborador(colaborador_id,composicao_id);

CREATE INDEX fki_financeira_colaborador_fk
	ON empresa.empresa
	USING btree(financeira_id,composicaofinanceira_id);
