ALTER TABLE escola.chamadas ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE escola.chamadas
	ADD CONSTRAINT colaborador_fk FOREIGN KEY (aluno_id,composicao_colaborador_id)
		REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
		ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_colaborador_fk
	ON escola.chamadas
	USING btree(aluno_id,composicao_colaborador_id);