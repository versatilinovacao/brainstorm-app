CREATE VIEW telefones_view AS
	SELECT t.telefone_id,t.numero,t.ramal,t.tipo_telefone_id, tc.colaborador_id FROM telefones t
		 LEFT JOIN telefones_colaboradores tc ON tc.telefone_id = t.telefone_id;
