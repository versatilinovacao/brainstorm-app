CREATE TABLE action.deletar_telefones(
	telefone_id			BIGINT NOT NULL PRIMARY KEY
);

CREATE TABLE critica.telefones(
	critica_id			SERIAL NOT NULL PRIMARY KEY,
	telefone_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE TABLE msg.telefones(
	mensagem_id			SERIAL NOT NULL PRIMARY KEY,
	telefone_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE OR REPLACE FUNCTION action.deletar_telefones_execute() RETURNS trigger AS $$
DECLARE
	temTelefone	BOOLEAN;
BEGIN 
	SELECT COUNT(1) > 0 INTO temTelefone FROM critica.telefones WHERE telefone_id = NEW.telefone_id;

	IF NOT temTelefone THEN
		DELETE FROM public.telefones WHERE telefone_id = NEW.telefone_id;
	ELSE
		INSERT INTO msg.telefones(telefone_id,mensagem,ocorrencia) SELECT telefone_id,mensagem,ocorrencia FROM critica.telefones WHERE telefone_id = NEW.telefone_id;
	END IF;

	DELETE FROM action.deletar_telefones WHERE telefone_id = NEW.telefone_id;

	RETURN NEW;

END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION action.cancelar_critica_telefones_execute() RETURNS trigger AS $$
BEGIN
	DELETE FROM critica.telefones WHERE telefone_id = OLD.telefone_id ; 
	RETURN OLD;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER deletar_telefone_trigger AFTER INSERT ON action.deletar_telefones FOR EACH ROW
	EXECUTE PROCEDURE action.deletar_telefones_execute();

CREATE OR REPLACE FUNCTION action.telefones_colaboradores_criticar() RETURNS trigger AS $$
BEGIN
	INSERT INTO critica.telefones(telefone_id,mensagem,ocorrencia) VALUES(NEW.telefone_id,'Existe um colaborador vinculado ao telefone, desvincule-o antes de tentar excluir!',now());
	
	RETURN NEW;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER telefones_colaboradores_trigger AFTER INSERT ON public.telefones_colaboradores FOR EACH ROW
	EXECUTE PROCEDURE action.telefones_colaboradores_criticar();

CREATE TRIGGER cancelar_critica_telefone_trigger BEFORE DELETE ON public.telefones_colaboradores FOR EACH ROW
	EXECUTE PROCEDURE action.cancelar_critica_telefones_execute(); 

	