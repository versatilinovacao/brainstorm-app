DROP VIEW administracao.contratos_parcelas_view;

CREATE OR REPLACE VIEW administracao.contratos_parcelas_view AS 
 SELECT round(random() * c.devedor_id::double precision + date_part('month'::text, c.vencimento) + date_part('year'::text, c.vencimento)) AS id,
    p.contrato_id,
    c.vencimento,
    c.emissao,
    c.pagamento,
    c.quitacao,
    c.devedor_id,
    c.composicao_devedor_id,
    c.desconto,
    c.valorpago,
    c.saldo,
    c.total,
    c.negociacao_id,
    n.descricao AS descricaonegociacao,
    c.formapagamento_id,
    f.descricao AS descricaoformapagamento,
    c.parcela,
    c.protesto,
    c.tipopagamento_id,
    t.descricao AS descricaotipopagamento,
    c.descricao,
    c.status
   FROM administracao.parcelas_por_contratos p
     JOIN financeiro.contas_receber c ON c.contareceber_id = p.contareceber_id
     LEFT JOIN financeiro.negociacoes n ON n.negociacao_id = c.negociacao_id
     LEFT JOIN financeiro.tipos_pagamentos t ON t.tipopagamento_id = c.tipopagamento_id
     LEFT JOIN financeiro.formas_pagamentos f ON f.formapagamento_id = c.formapagamento_id
  WHERE COALESCE(c.status, true) = true
  ORDER BY c.vencimento;