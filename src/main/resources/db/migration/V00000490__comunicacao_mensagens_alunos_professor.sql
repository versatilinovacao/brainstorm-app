CREATE SCHEMA comunicacao;

CREATE TABLE comunicacao.comunicacao_aluno_professor(
	comunicacao_id						BIGSERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO comunicacao.comunicacao_aluno_professor(descricao) VALUES('PROFESSOR'),('ALUNO');

CREATE TABLE comunicacao.mensagens_alunos_professor(	
	mensagemaluno_id					BIGSERIAL NOT NULL PRIMARY KEY,
	aluno_id							BIGINT,
	professor_id						BIGINT,
	comunicacao_id						BIGINT,
	mensagem							TEXT,
	registro							TIMESTAMP
);

CREATE INDEX conversa_idx				ON comunicacao.mensagens_alunos_professor		USING btree(professor_id,aluno_id);
CREATE INDEX conversa_registro_idx		ON comunicacao.mensagens_alunos_professor		USING btree(registro,professor_id,aluno_id);

ALTER TABLE comunicacao.mensagens_alunos_professor		ADD CONSTRAINT professor_pk	FOREIGN KEY(professor_id)	REFERENCES usuario(usuario_id);
ALTER TABLE comunicacao.mensagens_alunos_professor		ADD CONSTRAINT aluno_fk		FOREIGN KEY(aluno_id)		REFERENCES usuario(usuario_id);

CREATE INDEX professor_idx				ON comunicacao.mensagens_alunos_professor		USING btree(professor_id);
CREATE INDEX aluno_idx					ON comunicacao.mensagens_alunos_professor		USING btree(aluno_id);