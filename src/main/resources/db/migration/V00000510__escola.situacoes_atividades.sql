ALTER TABLE escola.situacoes_atividades			ADD COLUMN prioridade					INTEGER;

UPDATE escola.situacoes_atividades	SET prioridade = 1 WHERE situacaoatividade_id = 2;
UPDATE escola.situacoes_atividades	SET prioridade = 2 WHERE situacaoatividade_id = 4;
UPDATE escola.situacoes_atividades	SET prioridade = 3 WHERE situacaoatividade_id = 3;
UPDATE escola.situacoes_atividades	SET prioridade = 4 WHERE situacaoatividade_id = 1;