CREATE TABLE configuracoes_contratos(
	configuracaocontrato_id				BIGSERIAL NOT NULL PRIMARY KEY,
	
	desconto_pagamento				NUMERIC(16,4), --Determina um percentual de desconto para pagamentos até a data.
	desconto_sazonal				NUMERIC(16,4), --Determina um percentual de desconto para um período pré-determinado com inicio e fim.
	desconto_bolsista				NUMERIC(16,4), --Desconto utilizado nas escolas para alunos com bolsas de estudo.
	desconto_soma					BOOLEAN        --Se for true, todos os percentuais serão somados em um unico percentual gerando um desconto maior, senão os descontos serão tratados separadamente.
	
);

ALTER TABLE empresa.empresa		ADD COLUMN configuracaocontrato_id	BIGINT;
ALTER TABLE empresa.empresa		ADD CONSTRAINT empresacontrato_fk	FOREIGN KEY(configuracaocontrato_id)	REFERENCES configuracoes_contratos(configuracaocontrato_id);
CREATE INDEX configuracaocliente_idx		ON empresa.empresa	USING btree(configuracaocontrato_id);

ALTER TABLE cliente		ADD COLUMN configuracaocontrato_id	BIGINT;
ALTER TABLE cliente		ADD CONSTRAINT clientecontrato_fk	FOREIGN KEY(configuracaocontrato_id)	REFERENCES configuracoes_contratos(configuracaocontrato_id);
CREATE INDEX configuracaocliente_idx		ON cliente	USING btree(configuracaocontrato_id);

ALTER TABLE tercerizados	ADD COLUMN configuracaocontrato_id	BIGINT;
ALTER TABLE tercerizados	ADD CONSTRAINT tercerizadocontrato_fk	FOREIGN KEY(configuracaocontrato_id)	REFERENCES configuracoes_contratos(configuracaocontrato_id);
CREATE INDEX configuracaotercerizado_idx	ON tercerizados	USING btree(configuracaocontrato_id);
