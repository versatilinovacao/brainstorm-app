DROP VIEW colaborador_view;

CREATE OR REPLACE VIEW colaborador_view AS 
 SELECT c.colaborador_id,c.composicao_id,
    c.nome,
    c.fantasia,
    c.referencia,
    c.cep,
    c.email,
    c.status,
    i.foto_thumbnail,
    t.numero AS telefone
   FROM colaborador c
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
     LEFT JOIN telefones t ON t.telefone_id = c.telefone_id
  WHERE c.status = true
  ORDER BY c.nome;

ALTER TABLE colaborador_view
  OWNER TO postgres;
