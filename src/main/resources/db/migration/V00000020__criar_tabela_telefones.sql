CREATE TABLE public.telefones(
	telefone_id					SERIAL NOT NULL,
	numero						VARCHAR(15),
	ramal						VARCHAR(6),
	tipo_telefone_id			BIGINT
);

ALTER TABLE public.telefones ADD CONSTRAINT telefone_pk PRIMARY KEY(telefone_id);
ALTER TABLE public.telefones ADD CONSTRAINT tipo_telefone_fk FOREIGN KEY(tipo_telefone_id) REFERENCES tipos_telefones(tipo_telefone_id);

ALTER TABLE public.colaborador ADD CONSTRAINT telefone_fk FOREIGN KEY(telefone_id) REFERENCES public.telefones(telefone_id);

CREATE TABLE public.telefones_colaboradores(
	telefone_id					BIGINT,
	colaborador_id				BIGINT
);

ALTER TABLE public.telefones_colaboradores ADD CONSTRAINT telefone_colaborador_pk PRIMARY KEY(telefone_id,colaborador_id);
ALTER TABLE public.telefones_colaboradores ADD CONSTRAINT telefone_fk FOREIGN KEY(telefone_id) REFERENCES public.telefones(telefone_id);
ALTER TABLE public.telefones_colaboradores ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);
