ALTER TABLE almoxarifado.artefatos	ADD COLUMN planoconta_id		BIGINT;
ALTER TABLE almoxarifado.artefatos	ADD CONSTRAINT planoconta_fk	FOREIGN KEY(planoconta_id)	REFERENCES contabil.planos_contas(planoconta_id);
CREATE INDEX planoconta_idx	ON almoxarifado.artefatos(planoconta_id);