DROP VIEW escola.cadernos_inconsistencias_find;
CREATE VIEW escola.cadernos_inconsistencias_find AS
	SELECT i.caderno_inconsistencia_id,
	cw.nome AS resumo,
	cw.descricao,
	i.caderno_id,
	i.consistencia_id,
	i.status
   FROM escola.cadernos_inconsistencias i
	inner join escola.consistencias_view cw on cw.caderno_inconsistencia_id = i.caderno_inconsistencia_id ;
