UPDATE servico.servicos SET status = true;

DROP VIEW contabil.servicos_ativos_view;
CREATE OR REPLACE VIEW contabil.servicos_ativos_view AS 
 SELECT c.servico_id,
        c.referencia,
        c.descricao,
        c.aliquota,
        c.municipio,
        c.status
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = true
  ORDER BY c.referencia;

DROP VIEW contabil.servicos_inativos_view;
CREATE OR REPLACE VIEW contabil.servicos_inativos_view AS 
 SELECT c.servico_id,
        c.referencia,
        c.descricao,
        c.aliquota,
        c.municipio,
        c.status
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = false
  ORDER BY c.referencia;

DROP VIEW servico.servicos_ativos_cachoeirinha_view;
CREATE OR REPLACE VIEW servico.servicos_ativos_cachoeirinha_view AS 
 SELECT c.servico_id,
    c.referencia,
    c.descricao,
    c.aliquota,
    c.status,
    c.municipio
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = true
    AND c.municipio = 4303103
    ORDER BY c.referencia;

DROP VIEW servico.servicos_inativos_cachoeirinha_view;
CREATE OR REPLACE VIEW servico.servicos_inativos_cachoeirinha_view AS 
 SELECT c.servico_id,
    c.referencia,
    c.descricao,
    c.aliquota,
    c.status,
    c.municipio
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = false
    AND c.municipio = 4303103
  ORDER BY c.referencia;

DROP VIEW servico.servicos_ativos_portoalegre_view;
CREATE OR REPLACE VIEW servico.servicos_ativos_portoalegre_view AS 
 SELECT c.servico_id,
    c.referencia,
    c.descricao,
    c.aliquota,
    c.status,
    c.municipio,
    c.codigo_procempa,
    c.inicio,
    c.fim
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = true
    AND c.municipio = 4314902
  ORDER BY c.referencia;

DROP VIEW servico.servicos_inativos_portoalegre_view;
CREATE OR REPLACE VIEW servico.servicos_inativos_portoalegre_view AS 
 SELECT c.servico_id,
    c.referencia,
    c.descricao,
    c.aliquota,
    c.status,
    c.municipio,
    c.codigo_procempa,
    c.inicio,
    c.fim
   FROM servico.servicos c
  WHERE COALESCE(c.status, true) = false
    AND c.municipio = 4314902
  ORDER BY c.referencia;