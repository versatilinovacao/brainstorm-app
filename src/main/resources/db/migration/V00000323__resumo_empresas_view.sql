DROP VIEW resumo_empresas_view;

CREATE OR REPLACE VIEW resumo_empresas_view AS 
SELECT c.colaborador_id AS empresa_id,
       c.composicao_id AS composicao_empresa_id,
       c.nome,
       s.cidade_id
FROM colaborador c
INNER JOIN logradouro l ON l.cep = c.cep
INNER JOIN bairros b ON b.bairro_id = l.bairro_id
INNER JOIN cidades s ON s.cidade_id = b.cidade_id
WHERE COALESCE(c.empresa_id, 0::bigint) > 0;
