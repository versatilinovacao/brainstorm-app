CREATE TABLE contabil.plano_contas(
	planoconta_id							BIGSERIAL NOT NULL,
	composicao_id							BIGINT REFERENCES contabil.plano_contas(planoconta_id),
	nivel									INTEGER NOT NULL,
	codigo									INTEGER NOT NULL,
	numeral									INTEGER NOT NULL,
	descricao								VARCHAR(60) NOT NULL,
	status									BOOLEAN,
	PRIMARY KEY(planoconta_id)
);



INSERT INTO contabil.plano_contas(nivel,codigo,numeral,descricao,status) VALUES(0,1,1,'COMERCIO',true),(0,2,1,'INDUSTRIA',true),(0,3,1,'SERVIÇO',true);
INSERT INTO contabil.plano_contas(composicao_id,nivel,codigo,numeral,descricao,status) VALUES(1,1,1,1,'ATIVO',true),(1,1,2,1,'PASSIVO',true),(1,1,3,1,'DESPESAS',true),(1,1,4,1,'RECEITAS',true),(1,1,5,1,'RESULTADO',true);
INSERT INTO contabil.plano_contas(composicao_id,nivel,codigo,numeral,descricao,status) VALUES(2,1,1,1,'ATIVO',true),(2,1,2,1,'PASSIVO',true),(2,1,3,1,'DESPESAS',true),(2,1,4,1,'CUSTOS',true),(2,1,5,1,'RECEITAS',true),(2,1,6,1,'RESULTADO',true);
INSERT INTO contabil.plano_contas(composicao_id,nivel,codigo,numeral,descricao,status) VALUES(3,1,1,1,'ATIVO',true),(3,1,2,1,'PASSIVO',true),(3,1,3,1,'DESPESAS',true),(3,1,4,1,'CUSTOS',true),(3,1,5,1,'RECEITAS',true),(3,1,6,1,'RESULTADO',true);

CREATE OR REPLACE VIEW contabil.plano_contas_ativas_view AS 
SELECT * FROM contabil.plano_contas WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW contabil.plano_contas_inativas_view AS 
SELECT * FROM contabil.plano_contas WHERE COALESCE(status,true) = false;

ALTER TABLE financeiro.contas_receber	ADD COLUMN planoconta_id	BIGINT;
ALTER TABLE financeiro.contas_pagar		ADD COLUMN planoconta_id	BIGINT;

ALTER TABLE financeiro.contas_receber	ADD CONSTRAINT planoconta_fk	FOREIGN KEY(planoconta_id)	REFERENCES contabil.plano_contas(planoconta_id);
ALTER TABLE financeiro.contas_pagar		ADD CONSTRAINT planoconta_fk	FOREIGN KEY(planoconta_id)	REFERENCES contabil.plano_contas(planoconta_id);