DROP VIEW telefones_ativos_view;
CREATE OR REPLACE VIEW telefones_ativos_view AS 
 SELECT t.telefone_id,
    t.numero,
    t.ramal,
    t.tipo_telefone_id,
    t.status
   FROM telefones t
  WHERE t.status = true;

DROP VIEW telefones_inativos_view;
CREATE OR REPLACE VIEW telefones_inativos_view AS 
 SELECT t.telefone_id,
    t.numero,
    t.ramal,
    t.tipo_telefone_id,
    t.status
   FROM telefones t
  WHERE t.status = false;
