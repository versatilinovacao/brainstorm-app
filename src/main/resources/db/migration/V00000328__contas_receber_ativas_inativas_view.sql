DROP VIEW financeiro.contas_receber_ativas_view;

CREATE OR REPLACE VIEW financeiro.contas_receber_ativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contareceber_id,
	    c.composicao_id,
        c.total,
        c.devedor_id,
        c.composicao_devedor_id,
	r.nome AS nomedevedor,
        c.parcela,
        c.status
   FROM financeiro.contas_receber c
INNER JOIN public.colaborador r ON r.colaborador_id = c.devedor_id AND r.composicao_id = c.composicao_devedor_id
  WHERE COALESCE(c.status, true) = true;

DROP VIEW financeiro.contas_receber_inativas_view;

CREATE OR REPLACE VIEW financeiro.contas_receber_inativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contareceber_id,
	    c.composicao_id,
        c.total,
        c.devedor_id,
        c.composicao_devedor_id,
	r.nome AS nomedevedor,
        c.parcela,
        c.status
   FROM financeiro.contas_receber c
INNER JOIN public.colaborador r ON r.colaborador_id = c.devedor_id AND r.composicao_id = c.composicao_devedor_id
  WHERE COALESCE(c.status, true) = false;  

