CREATE TABLE tipos_criptografia(
	tipocriptografia_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(60) NOT NULL UNIQUE,
	sigla							VARCHAR(10) NOT NULL UNIQUE
);

INSERT INTO tipos_criptografia(nome,sigla)	VALUES('Transport Layer Security','TLS'); 

ALTER TABLE empresa.empresa ADD COLUMN smtp 			VARCHAR(60);
ALTER TABLE empresa.empresa ADD COLUMN porta_smtp		INTEGER;
ALTER TABLE empresa.empresa ADD COLUMN criptografia_id	BIGINT;
ALTER TABLE empresa.empresa ADD COLUMN autenticado		BOOLEAN;
ALTER TABLE empresa.empresa ADD COLUMN login			VARCHAR(100);
ALTER TABLE empresa.empresa ADD COLUMN senha			VARCHAR(255);

ALTER TABLE empresa.empresa ADD CONSTRAINT criptografia_fk FOREIGN KEY(criptografia_id)	REFERENCES tipos_criptografia(tipocriptografia_id);

CREATE INDEX tipo_criptografia_idx	ON empresa.empresa USING btree(criptografia_id);