CREATE OR REPLACE VIEW public.usuariosview AS
	SELECT * FROM usuario WHERE status = TRUE;
	
CREATE OR REPLACE VIEW public.usuariosdeletadosview AS
	SELECT * FROM usuario WHERE status = FALSE;
