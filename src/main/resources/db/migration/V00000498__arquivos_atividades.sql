CREATE TABLE escola.arquivos_atividades(
	arquivo_id									BIGINT NOT NULL REFERENCES arquivos.hd(arquivo_id),
	atividade_id								BIGINT NOT NULL REFERENCES escola.atividades(atividade_id)
);

CREATE INDEX arquivos_atividades_idx			ON escola.arquivos_atividades	USING btree(arquivo_id);
CREATE INDEX atividades_arquivos_idx			ON escola.arquivos_atividades	USING btree(atividade_id);