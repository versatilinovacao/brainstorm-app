--CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
CREATE SERVER seguranca_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'seguranca_db');
CREATE USER MAPPING FOR postgres SERVER seguranca_server OPTIONS (user 'postgres', password 'default123');
GRANT USAGE ON FOREIGN SERVER seguranca_server TO postgres;

CREATE TYPE record_permissoes	AS ( permissao_id BIGINT, nome VARCHAR, label VARCHAR, descricao VARCHAR);

CREATE OR REPLACE FUNCTION dblink_permissoes() RETURNS SETOF record_permissoes AS $$
DECLARE
	reg	record_permissoes%ROWTYPE;
BEGIN
	FOR reg IN 
		SELECT permissao_id,nome,label,descricao
		FROM( 
		SELECT * FROM public.dblink ('seguranca_server','select permissao_id,nome,label,descricao from public.permissao')  AS DATA(permissao_id BIGINT, nome VARCHAR, label VARCHAR, descricao VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.permissoes_view AS 
select permissao_id, nome, label, descricao from dblink_permissoes(); 