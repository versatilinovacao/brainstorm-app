ALTER TABLE almoxarifado.artefatos	ADD COLUMN servico_id					BIGINT;

ALTER TABLE almoxarifado.artefatos	ADD CONSTRAINT servico_fk	FOREIGN KEY(servico_id)		REFERENCES servico.servicos(servico_id);

DROP VIEW contabil.servicos_ativos_view;
DROP VIEW contabil.servicos_inativos_view;

CREATE OR REPLACE VIEW contabil.servicos_ativos_view AS 
SELECT servico_id,
       referencia,
       descricao,
       aliquota,
       municipio,
       status
FROM
(SELECT c.servico_id,
       c.referencia,
       c.descricao,
       c.aliquota,
       c.municipio,
       c.status
FROM servico.cachoeirinha c
WHERE COALESCE(c.status,true) = true
UNION ALL
SELECT p.servico_id,
       p.referencia,
       p.descricao,
       p.aliquota,
       p.municipio,
       p.status
FROM servico.portoalegre p
WHERE COALESCE(p.status,true) = true) servicos
ORDER BY descricao;

CREATE OR REPLACE VIEW contabil.servicos_inativos_view AS 
SELECT servico_id,
       referencia,
       descricao,
       aliquota,
       municipio,
       status
FROM
(SELECT c.servico_id,
       c.referencia,
       c.descricao,
       c.aliquota,
       c.municipio,
       c.status
FROM servico.cachoeirinha c
WHERE COALESCE(c.status,true) = false
UNION ALL
SELECT p.servico_id,
       p.referencia,
       p.descricao,
       p.aliquota,
       p.municipio,
       p.status
FROM servico.portoalegre p
WHERE COALESCE(p.status,true) = false) servicos
ORDER BY descricao;