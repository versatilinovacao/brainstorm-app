DROP VIEW escola.atividades_alunos_view;
DROP TABLE escola.alunos_atividades CASCADE;
CREATE TABLE escola.alunos_atividades(
	alunoatividade_id		BIGSERIAL NOT NULL PRIMARY KEY,
	aluno_id			BIGINT NOT NULL,
	composicao_aluno_id		BIGINT,
	atividade_id			BIGINT NOT NULL REFERENCES escola.atividades(atividade_id)
);

ALTER TABLE escola.alunos_atividades	ADD CONSTRAINT aluno_atividade_fk FOREIGN KEY(aluno_id,composicao_aluno_id) REFERENCES escola.alunos(colaborador_id,composicao_id);

CREATE OR REPLACE VIEW escola.atividades_alunos_view AS 
SELECT alunoatividade_id,
       at.aluno_id, 
       at.composicao_aluno_id,
       a.atividade_id,
       a.gradecurricular_id,
       a.data_atividade,
       a.titulo,
       a.descricao,
       a.avaliacao,
       a.nota,
       a.conceito,
       a.entrega,
       a.concluido,
       a.status        
FROM escola.alunos_atividades at
INNER JOIN escola.atividades a ON a.atividade_id = at.atividade_id
INNER JOIN escola.alunos e ON e.colaborador_id = at.aluno_id AND e.composicao_id = at.composicao_aluno_id;
