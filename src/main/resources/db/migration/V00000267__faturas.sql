CREATE TABLE empresa.tipos_fatura(
	tipofatura_id					BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.tipos_fatura(descricao) VALUES('Receita'),('Despesa');

CREATE TABLE empresa.tipos_pagamento(
	tipopagamento_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.tipos_pagamento(descricao) VALUES('A Vista'),('A Prazo'),('Fragmentado');

CREATE TABLE empresa.parcelamentos(
	parcelamento_id					BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO empresa.parcelamentos(descricao) VALUES('Cartão'),('Boleto'),('Promissória'),('Financiamento'),('Cheque');

CREATE TABLE empresa.formas_pagamentos(
	formapagamento_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
); 

INSERT INTO empresa.formas_pagamentos(descricao) VALUES('Dinheiro'),('Cheque'),('Cartão'),('Boleto'),('Promissória'),('Vale Refeição'),('Vale Alimentação');

CREATE TABLE empresa.faturas(
	fatura_id						BIGSERIAL NOT NULL PRIMARY KEY,
	composicao_id					BIGINT,
	
	emissao							DATE NOT NULL,
	vencimento						DATE NOT NULL,
	pagamento						DATE,
	compensado						DATE,
	protesto						DATE,
	
	provisor_id						BIGINT NOT NULL,
	composicao_provisor_id			BIGINT DEFAULT 0,
	financiador_id					BIGINT DEFAULT 0,
	composicao_financiador_id		BIGINT DEFAULT 0,
	fiador_id						BIGINT DEFAULT 0,
	composicao_fiador_id			BIGINT DEFAULT 0,
	
	tipofatura_id					BIGINT NOT NULL REFERENCES empresa.tipos_fatura(tipofatura_id),
	tipopagamento_id				BIGINT NOT NULL REFERENCES empresa.tipos_pagamento(tipopagamento_id),
	parcelamento_id					BIGINT NOT NULL REFERENCES empresa.parcelamentos(parcelamento_id),
	formapagamento_id				BIGINT NOT NULL REFERENCES empresa.formas_pagamentos(formapagamento_id),
	
	taxa							NUMERIC(16,4) DEFAULT 0,
	juros							NUMERIC(16,4) DEFAULT 0,
	mora							NUMERIC(16,4) DEFAULT 0,
	
	acrescimo						NUMERIC(16,4) DEFAULT 0,
	desconto						NUMERIC(16,4) DEFAULT 0,
	valor							NUMERIC(16,4) DEFAULT 0,
	valorcorrigido					NUMERIC(16,4) DEFAULT 0,
	total							NUMERIC(16,4) NOT NULL DEFAULT 0,
	FOREIGN KEY(provisor_id,composicao_provisor_id) REFERENCES colaborador(colaborador_id,composicao_id),
	FOREIGN KEY(financiador_id,composicao_financiador_id) REFERENCES colaborador(colaborador_id,composicao_id),
	FOREIGN KEY(fiador_id,composicao_fiador_id) REFERENCES colaborador(colaborador_id,composicao_id)
	
	
);

CREATE INDEX fatura_emissao ON empresa.faturas USING btree(emissao);
CREATE INDEX fatura_vencimento ON empresa.faturas USING btree(vencimento);
CREATE INDEX fatura_pagamento ON empresa.faturas USING btree(pagamento);
CREATE INDEX fatura_emissao_tipo ON empresa.faturas USING btree(emissao,tipofatura_id);
CREATE INDEX colaborador_provisor_idx ON empresa.faturas USING btree(provisor_id,composicao_provisor_id);
CREATE INDEX colaborador_financiador ON empresa.faturas USING btree(financiador_id,composicao_id);
CREATE INDEX colaborador_fiador ON empresa.faturas USING btree(fiador_id,composicao_fiador_id);



