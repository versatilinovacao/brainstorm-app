CREATE TABLE public.tiposmaterias(
	tipo_materia_id					SERIAL NOT NULL,
	descricao						VARCHAR(100) UNIQUE
);

ALTER TABLE public.tiposmaterias ADD CONSTRAINT tipo_materia_pk PRIMARY KEY(tipo_materia_id);

INSERT INTO public.tiposmaterias(descricao) VALUES('OPTATIVA'),('OBRIGATÓRIA'); 

CREATE TABLE public.materias(
	materia_id						SERIAL NOT NULL,
	descricao						VARCHAR(100) UNIQUE,
	tipo_materia_id					BIGINT
);

ALTER TABLE public.materias ADD CONSTRAINT materia_pk PRIMARY KEY(materia_id);
ALTER TABLE public.materias ADD CONSTRAINT tipo_materia_fk FOREIGN KEY(tipo_materia_id) REFERENCES public.tiposmaterias(tipo_materia_id);