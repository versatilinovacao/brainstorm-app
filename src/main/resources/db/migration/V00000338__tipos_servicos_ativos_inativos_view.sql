CREATE TABLE contabil.tipos_servicos(
	tiposervico_id							BIGSERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(60) NOT NULL UNIQUE,
	status									BOOLEAN
);

CREATE OR REPLACE VIEW contabil.tipos_servicos_ativos_view AS 
SELECT * FROM contabil.tipos_servicos WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW contabil.tipos_servicos_inativos_view AS 
SELECT * FROM contabil.tipos_servicos WHERE COALESCE(status,true) = false;

INSERT INTO contabil.tipos_servicos(descricao,status) VALUES('Presencial',true),('Virtual',true);