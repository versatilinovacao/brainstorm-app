DROP MATERIALIZED VIEW escola.fichas_alunos_view;
ALTER TABLE cliente		DROP COLUMN outraescolatempo;
ALTER TABLE cliente		ADD COLUMN qualoutraescolatempo		VARCHAR(40);
ALTER TABLE cliente		DROP COLUMN outraescolaadaptacao;
ALTER TABLE cliente		ADD COLUMN qualoutraescolaadaptacao	BOOLEAN;

CREATE MATERIALIZED VIEW escola.fichas_alunos_view	AS
SELECT  a.colaborador_id AS aluno_id,
	a.composicao_id AS composicao_aluno_id,
	c.nome,
        i.data_nascimento,
	COALESCE(s.descricao,'Não informado'),
	c.cep,
	l.logradouro || ', ' || c.numero AS endereco,
	b.descricao AS bairro,
	m.descricao AS cidade,
	e.descricao AS estado,
	e.estado_id,
	c.responsavel_id,
	r.nome AS responsavel,
	CASE WHEN cl.guarda_alternada THEN 'X' ELSE '' END AS guarda_alternada,
	CASE WHEN cl.guarda_compartilhada THEN 'X' ELSE '' END AS guarda_compartilhada,
	CASE WHEN cl.guarda_nidal THEN 'X' ELSE '' END AS guarda_nidal,
	CASE WHEN cl.guarda_unilateral THEN 'X' ELSE '' END AS guarda_unilateral,
	CASE WHEN cl.outraescola THEN 'SIM' ELSE 'NÃO' END AS outraescola,
	CASE WHEN cl.outraescola THEN cl.qualoutraescola ELSE '' END AS qualoutraescola,
	CASE WHEN cl.outraescola THEN cl.qualoutraescolatempo ELSE '' END AS qualoutraescolatempo,
	CASE WHEN cl.outraescola THEN CASE WHEN cl.qualoutraescolaadaptacao THEN 'SIM' ELSE 'NÃO' END ELSE '' END AS qualoutraescolaadaptacao
FROM escola.alunos a
INNER JOIN colaborador c ON c.colaborador_id = a.colaborador_id AND c.composicao_id = a.composicao_id
INNER JOIN identificador i ON i.identificador_id = c.identificador_id
INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id
LEFT JOIN sexos s ON s.sexo_id = i.sexo_id
LEFT JOIN remoto.logradouros_view l ON l.cep = c.cep
LEFT JOIN remoto.bairros_view b ON b.bairro_id = l.bairro_id
LEFT JOIN remoto.cidades_view m ON m.cidade_id = b.cidade_id
LEFT JOIN remoto.estados_view e ON e.estado_id = m.estado_id
LEFT JOIN colaborador r ON r.colaborador_id = c.responsavel_id;
