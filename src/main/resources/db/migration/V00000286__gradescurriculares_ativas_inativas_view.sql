ALTER TABLE escola.gradecurricular ADD COLUMN status				BOOLEAN; 

CREATE OR REPLACE VIEW escola.grades_curriculares_view AS
	SELECT g.gradecurricular_id,
	       g.turma_id,
	       t.descricao AS turma,
	       g.professor_id,
	       p.nome AS professor,
	       g.materia_id,
	       m.descricao AS materia,
	       g.curso_id,
	       c.descricao AS curso,
	       g.cursoperiodo_id,
	       g.composicao_professor_id,
	       g.status
	  FROM escola.gradecurricular g
	  LEFT JOIN cursos c ON c.curso_id = g.curso_id	
	  LEFT JOIN materias m ON m.materia_id = g.materia_id
	  LEFT JOIN turmas t ON t.turma_id = g.turma_id
	  LEFT JOIN colaborador p ON p.colaborador_id = g.professor_id AND p.composicao_id = g.composicao_professor_id;
	 

CREATE OR REPLACE VIEW escola.grades_curriculares_ativas_view AS
	SELECT * FROM escola.grades_curriculares_view
	 WHERE status = true;
	

CREATE OR REPLACE VIEW escola.grades_curriculares_inativas_view AS
	SELECT * FROM escola.grades_curriculares_view
	 WHERE status = false;
