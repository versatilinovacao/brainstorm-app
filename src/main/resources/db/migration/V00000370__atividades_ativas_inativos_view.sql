CREATE OR REPLACE VIEW escola.atividades_ativas_view AS
SELECT a.atividade_id,
	   a.gradecurricular_id,
	   a.data_atividade,
	   a.titulo,
	   a.descricao,
	   a.objetivo,
	   a.orientacoes,
	   a.avaliacao,
	   a.tipoatividade_id,
	   t.descricao AS descricao_atividade,
	   a.nota,
	   a.conceito,
	   a.entrega,
	   a.status
FROM escola.atividades a
LEFT JOIN escola.tipos_atividades t ON t.tipoatividade_id = a.tipoatividade_id	   
WHERE COALESCE(a.status,true) = true;	   

CREATE OR REPLACE VIEW escola.atividades_inativas_view AS
SELECT a.atividade_id,
	   a.gradecurricular_id,
	   a.data_atividade,
	   a.titulo,
	   a.descricao,
	   a.objetivo,
	   a.orientacoes,
	   a.avaliacao,
	   a.tipoatividade_id,
	   t.descricao AS descricao_atividade,
	   a.nota,
	   a.conceito,
	   a.entrega,
	   a.status
FROM escola.atividades a
LEFT JOIN escola.tipos_atividades t ON t.tipoatividade_id = a.tipoatividade_id	   
WHERE COALESCE(a.status,true) = false;	  