DROP TRIGGER periodoletivo_consistencia_tgi ON escola.periodos_letivos;
DROP TRIGGER periodoletivo_consistencia_tgu ON escola.periodos_letivos;

DROP FUNCTION critica.periodoletivo_consistencia();
CREATE OR REPLACE FUNCTION critica.periodoletivo_consistencia() RETURNS trigger AS $$
DECLARE
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	isExiste						BOOLEAN;
	cadernos1						escola.cadernos%ROWTYPE;
	cadernos2						escola.cadernos%ROWTYPE;
	ano_v							VARCHAR(4);
BEGIN
	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT count(1) > 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) = 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF IsPeriodoLetivoNotFind = true THEN
		FOR cadernos1 IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE status = true AND ano = ano_v)
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 1 AND caderno_id = cadernos1.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos1.caderno_id,1,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = cadernos1.caderno_id;
			END IF;
		END LOOP;
	END IF;			
	
	IF isPeriodoLetivoFind = true THEN
		FOR cadernos2 IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE status = true AND ano = ano_v)
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 3 AND caderno_id = cadernos2.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos2.caderno_id,3,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 3 and caderno_id = cadernos2.caderno_id;
			END IF;
		END LOOP;
	END IF;

	RETURN NULL;
END $$ LANGUAGE plpgsql;

CREATE TRIGGER periodoletivo_consistencia_tgi AFTER INSERT ON escola.periodos_letivos FOR EACH STATEMENT EXECUTE PROCEDURE critica.periodoletivo_consistencia();
CREATE TRIGGER periodoletivo_consistencia_tgu AFTER UPDATE ON escola.periodos_letivos FOR EACH STATEMENT EXECUTE PROCEDURE critica.periodoletivo_consistencia();
