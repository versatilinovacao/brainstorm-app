ALTER TABLE escola.atividades_upload		ADD COLUMN evento			TIMESTAMP;
CREATE INDEX evento_idx	ON escola.atividades_upload	USING btree(evento);

INSERT INTO escola.tipos_arquivos(nome,sigla)	VALUES('Linguagem SQL','SQL');