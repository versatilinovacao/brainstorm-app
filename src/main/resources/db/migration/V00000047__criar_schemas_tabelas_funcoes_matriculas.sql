CREATE SCHEMA action;
CREATE SCHEMA critica;
CREATE SCHEMA erros;

CREATE TABLE action.deletar_matricula(
	matricula_id					BIGINT NOT NULL PRIMARY KEY,
	usuario_id						BIGINT
);

CREATE TABLE critica.delete_matricula(
	matricula_id					BIGINT NOT NULL PRIMARY KEY
);	

CREATE TABLE erros.deletar_matriculas(
	deletar_matricula_id				SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(500) 
);

CREATE OR REPLACE FUNCTION action.deletar_matricula_execute() RETURNS trigger AS $$
DECLARE
	pesquisa	BOOLEAN;
BEGIN
	SELECT COUNT(1) > 0 INTO pesquisa FROM critica.delete_matricula WHERE matricula_id = NEW.matricula_id;

	IF NOT pesquisa THEN
		DELETE FROM public.matriculas WHERE matricula_id = NEW.matricula_id;
	ELSE
		INSERT INTO erros.deletar_matriculas(descricao) VALUES('A Matricula % não pode mais ser excluida, pois ela possui movimentação e vinculos com outros registros!');
	END IF;

	DELETE FROM action.deletar_matricula WHERE matricula_id = NEW.matricula_id;

	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER deletar_matricula_trigger AFTER INSERT ON action.deletar_matricula FOR EACH ROW
	EXECUTE PROCEDURE action.deletar_matricula_execute();
	
