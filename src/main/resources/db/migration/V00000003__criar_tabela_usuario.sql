CREATE TABLE public.usuario(
	usuario_id						SERIAL NOT NULL,
	empresa_id						BIGINT,							
	nome							VARCHAR(100),
	nascimento						DATE,
	senha							VARCHAR(255),
	status							BOOLEAN,
	email							VARCHAR(200)
);

ALTER TABLE public.usuario ADD CONSTRAINT usuario_pk PRIMARY KEY(usuario_id);
ALTER TABLE public.usuario ADD CONSTRAINT usuario_empresa_fk FOREIGN KEY(empresa_id) REFERENCES public.colaborador(colaborador_id);

CREATE TABLE public.permissao(
	permissao_id					SERIAL NOT NULL,
	nome							VARCHAR(100)
);

ALTER TABLE public.permissao ADD CONSTRAINT permissao_pk PRIMARY KEY(permissao_id);

CREATE TABLE public.usuario_permissao(
	usuario_id						BIGINT,
	permissao_id					BIGINT
);

ALTER TABLE public.usuario_permissao ADD CONSTRAINT usuario_permissao_fk FOREIGN KEY(usuario_id) REFERENCES public.usuario(usuario_id);
ALTER TABLE public.usuario_permissao ADD CONSTRAINT permissao_usuario_fk FOREIGN KEY(permissao_id) REFERENCES public.permissao(permissao_id);


INSERT INTO permissao(nome) VALUES('ROLE_COLABORADOR_ALTERAR'),('ROLE_COLABORADOR_INCLUIR'),('ROLE_COLABORADOR_DELETAR'),('ROLE_COLABORADOR_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_USUARIO_ALTERAR'),('ROLE_USUARIO_INCLUIR'),('ROLE_USUARIO_DELETAR'),('ROLE_USUARIO_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_PRODUTO_ALTERAR'),('ROLE_PRODUTO_INCLUIR'),('ROLE_PRODUTO_DELETAR'),('ROLE_USUARIO_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_CURSO_ALTERAR'),('ROLE_CURSO_INCLUIR'),('ROLE_CURSO_DELETAR'),('ROLE_CURSO_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_MATERIA_ALTERAR'),('ROLE_MATERIA_INCLUIR'),('ROLE_MATERIA_DELETAR'),('ROLE_MATERIA_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_TURMA_ALTERAR'),('ROLE_TURMA_INCLUIR'),('ROLE_TURMA_DELETAR'),('ROLE_TURMA_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_SECRETARIA_ALTERAR'),('ROLE_SECRETARIA_INCLUIR'),('ROLE_SECRETARIA_DELETAR'),('ROLE_SECRETARIA_VISUALIZAR');
INSERT INTO permissao(nome) VALUES('ROLE_PAIS_CONTEUDO'),('ROLE_PAIS_SALVAR'),('ROLE_PAIS_DELETAR'),('ROLE_ESTADO_CONTEUDO'),('ROLE_ESTADO_SALVAR');
INSERT INTO permissao(nome) VALUES('ROLE_PAIS_DELETAR'),('ROLE_CIDADE_CONTEUDO'),('ROLE_CIDADE_SALVAR'),('ROLE_CIDADE_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_BAIRRO_CONTEUDO'),('ROLE_BAIRRO_SALVAR'),('ROLE_BAIRRO_DELETAR'),('ROLE_LOGRADOURO_CONTEUDO');
INSERT INTO permissao(nome) VALUES('ROLE_LOGRADOURO_SALVAR'),('ROLE_LOGRADOURO_DELETAR'),('ROLE_CONTATO_SALVAR'),('ROLE_CONTATO_CONTEUDO'),('ROLE_CONTATO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_RESPONSAVEL_CONTEUDO'),('ROLE_RESPONSAVEL_SALVAR'),('ROLE_RESPONSAVEL_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_TELEFONE_CONTEUDO'),('ROLE_TELEFONE_ALTERAR'),('ROLE_TELEFONE_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_CARGO_CONTEUDO'),('ROLE_CARGO_SALVAR'),('ROLE_CARGO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_ESPECIALIDADE_CONTEUDO'),('ROLE_ESPECIALIDADE_SALVAR'),('ROLE_ESPECIALIDADE_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_CURSO_CONTEUDO'),('ROLE_CURSO_SALVAR'),('ROLE_CURSO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_MATERIA_CONTEUDO'),('ROLE_MATERIA_SALVAR'),('ROLE_MATERIA_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_TURMA_CONTEUDO'),('ROLE_TURMA_SALVAR'),('ROLE_TURMA_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_TURNO_CONTEUDO'),('ROLE_TURNO_SALVAR'),('ROLE_TURNO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_AMBIENTE_CONTEUDO'),('ROLE_AMBIENTE_SALVAR'),('ROLE_AMBIENTE_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_DIASEMANA_CONTEUDO'),('ROLE_DIASEMANA_SALVAR'),('ROLE_DIASEMANA_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_GRADEMATERIA_CONTEUDO'),('ROLE_GRADEMATERIA_SALVAR'),('ROLE_GRADEMATERIA_DELETAR');

INSERT INTO usuario(nome,nascimento,senha,status,email) VALUES('root',null,'$2a$10$gd3VN8QnT24dpVl2z5SgjuNTg7TYA8S7JKq6akSk84Up0WRr6BvKC',true,'administrador@insideincloud.com.br');
INSERT INTO usuario(nome,nascimento,senha,status,email) VALUES('apresentacao',null,'$2a$10$OieNjDcdmZnMSYvl3T1NW.tOhEwFdxhTNq9CC53qdIa3CqUeVVc/y',true,'suporte.duvida@mundodasnuvens.com.br');
INSERT INTO usuario(nome,nascimento,senha,status,email) VALUES('demonstracao',null,'$2a$10$OieNjDcdmZnMSYvl3T1NW.tOhEwFdxhTNq9CC53qdIa3CqUeVVc/y',true,'suporte.atendimento@mundodasnuvens.com.br');
INSERT INTO usuario(nome,nascimento,senha,status,email) VALUES('simone',null,'$2a$10$p1KxCcERmv1gJUonhhuUoO.Vh1.S4e50woJtmh2TzPd9W.2cMYNWq',true,'simone@escolaprosaber.com.br');

INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,1);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,2);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,3);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,4);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,5);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,6);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,7);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,8);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,9);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,10);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,11);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,12);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,13);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,14);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,15);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,16);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,17);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,18);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,19);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,20);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,21);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,22);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,23);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,24);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,25);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,26);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,27);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,28);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,29);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,30);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,31);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,32);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,33);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,34);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,35);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,36);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,37);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,38);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,39);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,40);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,41);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,42);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,43);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,44);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,45);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,46);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,47);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,48);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,49);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,50);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,51);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,52);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,53);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,54);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,55);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,56);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,57);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,58);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,59);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,60);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,61);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,62);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,63);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,64);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,65);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,66);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,67);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,68);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,69);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,70);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,71);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,72);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,73);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,74);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,75);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,76);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,77);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,78);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,79);



--SIMONE
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,1);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,2);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,3);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,4);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,5);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,6);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,7);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,8);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,9);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,10);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,11);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,12);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,13);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,14);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,15);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,16);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,17);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,18);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,19);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,20);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,21);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,22);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,23);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,24);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,25);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,26);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,27);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,28);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,29);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,30);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,31);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,32);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,33);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,34);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,35);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,36);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,37);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,38);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,39);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,40);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,41);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,42);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,43);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,44);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,45);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,46);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,47);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,48);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,49);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,50);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,51);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,52);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,53);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,54);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,55);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,56);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,57);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,58);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,59);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,60);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,61);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,62);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,63);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,64);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,65);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,66);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,67);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,68);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,69);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,70);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,71);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,72);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,73);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,74);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,75);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,76);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,77);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,78);
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(4,79);

