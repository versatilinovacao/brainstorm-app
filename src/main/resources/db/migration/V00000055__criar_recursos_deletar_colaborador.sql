CREATE TABLE critica.colaboradores(
	critica_id			SERIAL NOT NULL PRIMARY KEY,
	colaborador_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE TABLE msg.colaboradores(
	mensagem_id			SERIAL NOT NULL PRIMARY KEY,
	colaborador_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE TABLE action.deletar_colaborador(
	colaborador_id			BIGINT NOT NULL PRIMARY KEY
);


CREATE OR REPLACE FUNCTION action.deletar_colaboradores_execute() RETURNS trigger AS $$
DECLARE
	temCritica	boolean;
BEGIN
	SELECT COUNT(1) > 0 INTO temCritica FROM critica.colaboradores WHERE colaborador_id = NEW.colaborador_id;

	IF NOT temCritica THEN
		DELETE FROM public.colaborador WHERE colaborador_id = NEW.colaborador_id;
	ELSE
		UPDATE public.colaborador SET deletado = true, status = false WHERE colaborador_id = NEW.colaborador_id;
	END IF;

	DELETE FROM action.deletar_colaboradores WHERE colaborador_id = NEW.colaborador_id;

	RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER deletar_colaboradores_trigger AFTER INSERT ON action.deletar_colaborador
	FOR EACH ROW
		EXECUTE PROCEDURE action.deletar_colaboradores_execute();

CREATE OR REPLACE FUNCTION critica_alunos_turmas()
  RETURNS trigger AS
$BODY$
DECLARE
	matricula		BIGINT;
	colaborador		BIGINT;
BEGIN
	SELECT matricula_id INTO matricula FROM matriculas WHERE colaborador_id = NEW.colaborador_id AND status = TRUE;

	INSERT INTO critica.matriculas(matricula_id,mensagem,ocorrencia) VALUES(matricula,'Existe uma aluno vinculado a uma turma!',now());	
	INSERT INTO critica.colaboradores(colaborador_id,mensagem,ocorrencia) VALUES(NEW.colaborador_id,'Existem vinculos com turma(s), remova o vinculo antes de excluir o colaborador.',now());

	RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql