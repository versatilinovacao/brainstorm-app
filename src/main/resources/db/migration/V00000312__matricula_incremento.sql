CREATE OR REPLACE FUNCTION matricula_incremento()
  RETURNS trigger AS
$BODY$
DECLARE
	autoinc			BIGINT;
	sortInc1		CHAR(1);
	sortInc2		CHAR(1);
BEGIN
	IF COALESCE(NEW.codigo,'') = '' THEN
		SELECT count(1) + 1 INTO autoinc FROM matriculas;
		SELECT sort INTO sortInc1 FROM (select chr(65 + round( CAST(random()*(90-65) AS NUMERIC) ,0)::int) sort) sort1;
		SELECT sort INTO sortInc2 FROM (select chr(97 + round( CAST(random()*(122-97) AS NUMERIC) ,0)::int) sort) sort2;

		NEW.codigo = sortInc1 || to_hex(autoinc) || sortInc2;
	END IF;

	RETURN NEW;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;