CREATE TRIGGER atualiar_aluno_atividade
  AFTER INSERT OR UPDATE OR DELETE
  ON escola.atividades
  FOR EACH STATEMENT
  EXECUTE PROCEDURE escola.alunos_materializados();