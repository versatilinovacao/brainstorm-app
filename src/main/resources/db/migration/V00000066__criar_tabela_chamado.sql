CREATE TABLE public.time_line(
	time_line_id				SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.time_line(descricao) VALUES('AGUARDANDO TRIAGEM'),('EM AVALIAÇÃO'),('AGUARDANDO ANÁLISE'),('ANALISANDO'),
					      ('AGUARDANDO ATUAÇÃO'),('EM PRODUÇÃO'),('SUSPENSO'),('CANCELADO'),('PRONTO PARA TESTE'),
					      ('TESTANDO'),('AGUARDANDO HOMOLOGAÇÃO'),('HOMOLOGADO'),('AGUARDANDO IMPLANTAÇÃO'),
					      ('IMPLANTADO');

CREATE TABLE public.chamados(
	chamado_id				SERIAL NOT NULL PRIMARY KEY,
	chamado_composto_id			BIGINT REFERENCES public.chamados(chamado_id),
	empresa_id				BIGINT REFERENCES public.colaborador(colaborador_id),
	colaborador_id				BIGINT REFERENCES public.colaborador(colaborador_id),
	abertura				TIMESTAMP NOT NULL,
	fechamento				TIMESTAMP,
	titulo					VARCHAR(150),
	resumo					VARCHAR(500),
	descricao				TEXT,
	solucao					TEXT,
	time_line_id				BIGINT NOT NULL REFERENCES public.time_line(time_line_id)
);

CREATE TABLE action.deletar_chamados(
	chamado_id					BIGINT NOT NULL PRIMARY KEY
);

CREATE TABLE msg.chamados(
	mensagem_id					SERIAL NOT NULL PRIMARY KEY,
	chamado_id					BIGINT,
	mensagem					VARCHAR(500),
	ocorrencia					TIMESTAMP
);

CREATE TABLE erros.deletar_chamados(
	deletar_matricula_id		SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(500)
);

CREATE TABLE critica.chamados(
	critica_id					SERIAL NOT NULL,
	chamado_id					BIGINT,
	mensagem					VARCHAR(500),
	ocorrencia					TIMESTAMP
);

CREATE OR REPLACE FUNCTION action.deletar_chamados_execute() RETURNS trigger AS $$
DECLARE
	temCritica			BOOLEAN;
BEGIN
	SELECT COUNT(1) > 0 INTO temCritica FROM critica.chamados WHERE chamado_id = NEW.chamado_id;
	
	IF NOT temCritica THEN
		DELETE FROM public.chamados WHERE chamado_id = NEW.chamado_id;
	ELSE
		INSERT INTO msg.chamados(chamado_id,mensagem,ocorrencia) SELECT chamado_id, mensagem, ocorrencia FROM critica.chamados WHERE chamado_id = NEW.chamado_id;
	END IF;
	
	DELETE FROM action.deletar_chamados WHERE chamado_id = NEW.chamado_id;
	
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER deletar_chamados_trigger 
	AFTER INSERT ON action.deletar_chamados
		FOR EACH ROW
			EXECUTE PROCEDURE action.deletar_chamados_execute();


