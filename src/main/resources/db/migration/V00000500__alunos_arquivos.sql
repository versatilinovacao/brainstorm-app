CREATE TABLE escola.alunos_arquivos(
	aluno_id									BIGINT NOT NULL,
	composicao_aluno_id							BIGINT,
	arquivo_id									BIGINT NOT NULL REFERENCES arquivos.hd(arquivo_id)
);

ALTER TABLE escola.alunos_arquivos		ADD CONSTRAINT aluno_fk	FOREIGN KEY(aluno_id,composicao_aluno_id)	REFERENCES colaborador(colaborador_id,composicao_id);

CREATE INDEX arquivo_arquivo_idx	ON escola.alunos_arquivos	USING btree(arquivo_id);
CREATE INDEX arquivos_aluno_idx		ON escola.alunos_arquivos	USING btree(aluno_id,composicao_aluno_id);