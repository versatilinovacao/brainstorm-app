CREATE SCHEMA insideincloud;

CREATE TABLE insideincloud.sistemas(
	sistema_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	url					VARCHAR(500) NOT NULL UNIQUE
);

INSERT INTO insideincloud.sistemas(nome,url) VALUES('Desenvolvimento','localhost:8080');
INSERT INTO insideincloud.sistemas(nome,url) VALUES('Ambiente de Teste','http://brainstormteste.com.br:8280');
INSERT INTO insideincloud.sistemas(nome,url) VALUES('Ambiente de Homologação','http://timetechrs.com.br');
INSERT INTO insideincloud.sistemas(nome,url) VALUES('Insideincloud','http://insideincloud.com.br:8080');
INSERT INTO insideincloud.sistemas(nome,url) VALUES('Escola Pro-Saber','http://escolaprosaber.com.br');
