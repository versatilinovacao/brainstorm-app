CREATE TABLE escola.composicao_materia(
	composicao_materia_id		SERIAL NOT NULL PRIMARY KEY,
	materia_id			BIGINT NOT NULL,
	materia_composta_id		BIGINT NOT NULL
);

CREATE VIEW escola.materias_composta_view AS 
SELECT materia_id,descricao,aowner FROM (
	SELECT cm.materia_id,m.descricao, 0 AS aowner FROM escola.composicao_materia cm
		INNER JOIN public.materias m ON m.materia_id = cm.materia_id
		GROUP BY cm.materia_id,m.descricao
	UNION ALL
	SELECT materia_composta_id AS materia_id,descricao, cm.materia_id AS aowner FROM escola.composicao_materia cm
		LEFT JOIN public.materias m ON m.materia_id = cm.materia_composta_id

) AS materias_tree ORDER BY aowner,descricao;
