DROP TABLE escola.chamadas_alunos;
DROP TABLE escola.chamadas;

CREATE TABLE escola.periodos_letivos
(
	periodo_letivo_id 			SERIAL NOT NULL PRIMARY KEY,
	descricao 					VARCHAR(100) NOT NULL,
	ano 						VARCHAR(4) NOT NULL UNIQUE,
	status 						BOOLEAN
);

CREATE TABLE escola.periodos_letivos_itens
(
	periodo_letivo_item_id 		SERIAL NOT NULL PRIMARY KEY,
	periodo_letivo_id 			BIGINT NOT NULL REFERENCES escola.periodos_letivos(periodo_letivo_id),
	descricao 					VARCHAR(100) NOT NULL,
	periodo_inicial 			DATE NOT NULL,
	periodo_final 				DATE NOT NULL
);

CREATE TABLE escola.chamadas(
	chamada_aowner_id			SERIAL NOT NULL PRIMARY KEY,				
	evento						TIMESTAMP NOT NULL,
	chamada_id					BIGINT NOT NULL REFERENCES escola.periodos_letivos_itens(periodo_letivo_item_id), --usar aqui o código id da tabela periodos_letivos_itens. 
	caderno_id					BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	aluno_id					BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	conceito					VARCHAR(1),
	nota						NUMERIC(16,4),
	avaliacao					TEXT,
	status						BOOLEAN
);

--ALTER TABLE escola.chamadas ADD CONSTRAINT chamada_pk PRIMARY KEY(evento,chamada_id,caderno_id,aluno_id);
ALTER TABLE escola.chamadas ADD CONSTRAINT chamada_unica UNIQUE(caderno_id,aluno_id,evento);

CREATE TABLE escola.avaliacoes(
	avaliacao_id				SERIAL NOT NULL PRIMARY KEY,
	caderno_id					BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	aluno_id					BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	chamada_id					BIGINT NOT NULL REFERENCES escola.periodos_letivos_itens(periodo_letivo_item_id),
	aproveitamento				NUMERIC(16,4), --Percentual que indica a quantidade de participação em aula.
	conceito					VARCHAR(1), --Média no formato de conceito
	nota						NUMERIC(16,4), --Média no formato de nota
	avaliacao					TEXT --Avaliação geral no formato descritivo
);

ALTER TABLE escola.avaliacoes ADD CONSTRAINT avaliacao_unica UNIQUE(caderno_id,aluno_id,chamada_id);

CREATE VIEW escola.chamadas_view AS
	SELECT ch.chamada_aowner_id,
		   ch.evento,
	       ch.chamada_id,
	       ch.caderno_id,
	       ch.aluno_id,
	       c.nome as nome_aluno,
	       ch.conceito,
	       ch.nota,
           ch.avaliacao,
           ch.status
	FROM escola.chamadas ch
		LEFT JOIN public.colaborador c ON c.colaborador_id = ch.aluno_id;

CREATE VIEW escola.avaliacoes_view AS
	SELECT  a.avaliacao_id,
		a.chamada_id,
		a.caderno_id,
		a.aluno_id,
		c.nome as nome_aluno,
		p.descricao as periodo_letivo_nome,
		a.aproveitamento,
		a.conceito,
		a.nota,
		a.avaliacao
	FROM escola.avaliacoes a
		LEFT JOIN public.colaborador c ON c.colaborador_id = a.aluno_id
		LEFT JOIN escola.periodos_letivos_itens p ON p.periodo_letivo_item_id = a.chamada_id;
		
		--AJUSTAR A CRIAÇÃO AUTOMÁTICA DO CADERNO

--REVERTER TODO O CÓDIGO...
DROP TRIGGER criar_caderno_tgi ON escola.cadernos;
DROP FUNCTION escola.criar_caderno();		
CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodoLetivoItem				BIGINT;
	chamada							BIGINT;
	
	inicio_turma					DATE;
	fim_turma						DATE;

	periodos						escola.periodos_letivos_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	eventos							TIMESTAMP%TYPE;			
BEGIN
	SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
	SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;

	FOR alunos IN
		SELECT * FROM public.alunos_turmas WHERE turma_id = turma
	LOOP
		FOR eventos IN
			SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
		LOOP
			SELECT periodo_letivo_item_id INTO periodoLetivoItem FROM escola.periodos_letivos_itens 
									    WHERE eventos::date BETWEEN periodo_inicial AND periodo_final
									      AND status = true;	

			RAISE NOTICE 'PERIODO: %',periodoLetivoItem;
		
			INSERT INTO escola.chamadas(chamada_id,caderno_id,aluno_id,evento,status)
							VALUES(periodoLetivoItem,NEW.caderno_id,alunos.colaborador_id,eventos,false);
		END LOOP;

		INSERT INTO escola.avaliacoes(chamada_id,caderno_id,aluno_id)
			VALUES(periodoLetivoItem,NEW.caderno_id,alunos.colaborador_id);
		
	END LOOP;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER criar_caderno_tgi AFTER INSERT 
	ON escola.cadernos 
		FOR EACH ROW 
			EXECUTE PROCEDURE escola.criar_caderno();

		
		
		--AJUSTAR FUNÇÃO DELETAR CADERNO

--Precisa dropar a trigger e a função aqui...		
CREATE OR REPLACE FUNCTION action.deletar_caderno_execute() RETURNS trigger AS $$
DECLARE 
	isDelete			BOOLEAN;
	isCritica			BOOLEAN;
BEGIN
	IF NEW.deletar THEN
		UPDATE action.grupo_cadernos_delete SET deletar = NEW.deletar WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	END IF;
	
	SELECT deletar INTO isDelete FROM action.grupo_cadernos_delete WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	IF isDelete THEN
		SELECT COUNT(1) > 0 INTO isCritica FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
	
		IF NOT isCritica THEN
			DELETE FROM escola.avaliacoes 	WHERE caderno_id = NEW.caderno_id;
			DELETE FROM escola.chamadas 	WHERE caderno_id = NEW.caderno_id;
			DELETE FROM escola.cadernos 	WHERE caderno_id = NEW.caderno_id;
		ELSE
			INSERT INTO msg.cadernos(caderno_id,mensagem,ocorrencia) 
				SELECT caderno_id, mensagem, ocorrencia FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
		END IF;
		
		DELETE FROM action.deletar_cadernos WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	END IF; 

	RETURN NEW;
END;$$
LANGUAGE plpgsql;

DROP TRIGGER deletar_caderno_tgi ON action.deletar_cadernos; 
CREATE TRIGGER deletar_caderno_tgi AFTER INSERT
	ON action.deletar_cadernos
		FOR EACH ROW
			EXECUTE PROCEDURE action.deletar_caderno_execute();
		