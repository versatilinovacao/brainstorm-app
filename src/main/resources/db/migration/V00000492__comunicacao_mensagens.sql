CREATE TABLE comunicacao.dispositivos(
	dispositivo_id					BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO comunicacao.dispositivos(nome) VALUES('Empresa'),('Clientes'),('Tercerizados'),('Colaboradores'),('Ambiente Escola'),('Web');

CREATE TABLE comunicacao.mensagens(
	mensagem_id						BIGSERIAL NOT NULL PRIMARY KEY,
	locutor_id						BIGINT NOT NULL REFERENCES usuario(usuario_id),
	receptor_id						BIGINT NOT NULL REFERENCES usuario(usuario_id),
	mensagem						TEXT NOT NULL,
	evento							TIMESTAMP DEFAULT now(),
	dispositivo_id					BIGINT NOT NULL REFERENCES comunicacao.dispositivos(dispositivo_id)
);

CREATE INDEX mensagem_locutor_idx	ON comunicacao.mensagens	USING btree(locutor_id);
CREATE INDEX mensagem_receptor_idx	ON comunicacao.mensagens	USING btree(receptor_id);