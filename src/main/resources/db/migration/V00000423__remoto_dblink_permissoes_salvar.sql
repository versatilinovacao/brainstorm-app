CREATE OR REPLACE FUNCTION dblink_permissoes_salvar() RETURNS trigger AS $$
DECLARE
	comando		VARCHAR(250);
	id		BIGINT;
BEGIN
	IF (TG_OP = 'UPDATE') THEN
		SELECT permissao_id INTO id
		FROM ( 
		SELECT permissao_id FROM public.dblink ('seguranca_server','select permissao_id from public.permissao where nome = '''||OLD.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||NEW.label||''', descricao = '''||NEW.descricao||''' WHERE permissao_id = '||id||';';

	ELSE
		comando = 'INSERT INTO permissao(nome,label,descricao) VALUES('''||NEW.nome||''','''||NEW.label||''','''||NEW.descricao||''');';
	END IF;
	PERFORM dblink_exec('seguranca_server',comando);
	

	RETURN NEW;
END; $$
LANGUAGE 'plpgsql';
