ALTER TABLE colaborador DROP CONSTRAINT colaborador_composicao_fk;
ALTER TABLE colaborador DROP CONSTRAINT colaborador_pk CASCADE;
UPDATE colaborador SET composicao_id = 0 WHERE COALESCE(composicao_id,0) = 0;


ALTER TABLE colaborador ADD CONSTRAINT colaborador_pk PRIMARY KEY (colaborador_id, composicao_id);

