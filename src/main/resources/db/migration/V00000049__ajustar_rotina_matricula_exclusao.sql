﻿CREATE SCHEMA msg;
CREATE SCHEMA notificacao;
CREATE SCHEMA alerta;

CREATE TABLE msg.matriculas(
	mensagem_id			SERIAL NOT NULL PRIMARY KEY,
	matricula_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

DROP TABLE critica.delete_matricula;
CREATE TABLE critica.matriculas(
	criticas_id			SERIAL NOT NULL PRIMARY KEY,
	matricula_id			BIGINT,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE OR REPLACE FUNCTION public.critica_alunos_turmas() RETURNS trigger AS $$
DECLARE
	matricula		BIGINT;
BEGIN
	SELECT matricula_id INTO matricula FROM matriculas WHERE colaborador_id = NEW.colaborador_id AND status = TRUE;
	INSERT INTO critica.matriculas(matricula_id,mensagem,ocorrencia) VALUES(matricula,'Existe uma aluno vinculado a uma turma!',now());	
	
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER critica_alunos_turmas_trigger AFTER INSERT ON public.alunos_turmas FOR EACH ROW
	EXECUTE PROCEDURE public.critica_alunos_turmas();

DROP TRIGGER deletar_matricula_trigger ON action.deletar_matricula;
DROP FUNCTION action.deletar_matricula_execute();
CREATE OR REPLACE FUNCTION action.deletar_matricula_execute() RETURNS trigger AS $$
DECLARE 
	temCritica	BOOLEAN;
BEGIN
	SELECT COUNT(1) > 0 INTO temCritica FROM critica.matriculas WHERE matricula_id = NEW.matricula_id;

	IF NOT temCritica THEN
		DELETE FROM public.matriculas WHERE matricula_id = NEW.matricula_id;
	ELSE
		INSERT INTO msg.matriculas(matricula_id,mensagem,ocorrencia) SELECT matricula_id,mensagem,ocorrencia FROM critica.matriculas WHERE matricula_id = NEW.matricula_id;
	END IF;
	
	DELETE FROM action.deletar_matricula WHERE matricula_id = NEW.matricula_id;

	RETURN NEW;

END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER deletar_matricula_trigger AFTER INSERT ON action.deletar_matricula FOR EACH ROW
	EXECUTE PROCEDURE action.deletar_matricula_execute(); 