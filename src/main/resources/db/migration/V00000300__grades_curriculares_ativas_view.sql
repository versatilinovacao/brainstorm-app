DROP VIEW escola.grades_curriculares_ativas_view;

CREATE OR REPLACE VIEW escola.grades_curriculares_ativas_view AS 
 SELECT g.gradecurricular_id,
        g.turma_id,
        g.turma,
        g.professor_id,
        g.professor,
        g.materia_id,
        g.materia,
        g.curso_id,
        g.curso,
        g.cursoperiodo_id,
        g.composicao_professor_id,
        g.status
   FROM escola.grades_curriculares_view g
  WHERE g.status = true
  ORDER BY g.turma, g.professor, g.materia;
  
DROP VIEW escola.grades_curriculares_inativas_view;

CREATE OR REPLACE VIEW escola.grades_curriculares_inativas_view AS 
 SELECT g.gradecurricular_id,
        g.turma_id,
        g.turma,
        g.professor_id,
        g.professor,
        g.materia_id,
        g.materia,
        g.curso_id,
        g.curso,
        g.cursoperiodo_id,
        g.composicao_professor_id,
        g.status
   FROM escola.grades_curriculares_view g
  WHERE g.status = false
  ORDER BY g.turma, g.professor, g.materia;