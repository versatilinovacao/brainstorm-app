CREATE TABLE public.tipos_acessos(
	tipoacesso_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(40) NOT NULL UNIQUE
);

INSERT INTO public.tipos_acessos(nome) VALUES('Server'),('Client'),('Counter');

INSERT INTO public.usuario(nome,nascimento,senha,status,email) VALUES('owner',null,'$2a$10$zP53hD3PIrYLcP2b6GboMOVWP0cKYVZeo4IrZ4H8y9IDc5ddCxrrm',true,'owner@insideincloud.com.br');
CREATE TABLE public.acessos_usuarios(
	acessousuario_id			SERIAL NOT NULL PRIMARY KEY,
	tipoacesso_id                           BIGINT NOT NULL REFERENCES public.tipos_acessos(tipoacesso_id),
	usuario_id				BIGINT NOT NULL REFERENCES public.usuario(usuario_id)
);

INSERT INTO public.acessos_usuarios(tipoacesso_id,usuario_id) VALUES(1,(SELECT usuario_id FROM usuario WHERE nome = 'root'));
INSERT INTO public.acessos_usuarios(tipoacesso_id,usuario_id) VALUES(1,(SELECT usuario_id FROM usuario WHERE nome = 'owner'));