DROP VIEW responsavel_view;

CREATE OR REPLACE VIEW responsavel_view AS 
 SELECT c.colaborador_id AS responsavel_id,
		c.composicao_id AS composicao_colaborador_id,
		c.nome,
		( SELECT t.numero
            FROM telefones t
           WHERE t.telefone_id = c.telefone_id) AS telefone
   FROM colaborador c
  WHERE c.status = true AND COALESCE(c.empresa_id, 0::bigint) = 0 AND (COALESCE(c.cliente_id, 0::bigint) <> 0 OR COALESCE(c.parceiro_id,0) <> 0 )
  ORDER BY c.nome;