DROP VIEW escola.arquivos_atividades_view;

CREATE OR REPLACE VIEW escola.arquivos_atividades_view AS 
 SELECT aa.atividade_id,
    ah.arquivo_id,
    ah.nome,
    ah.extensao,
    ah.tipo,
    ah.tamanho,
    ah.registro,
    ah.url,
    ah.arquivo,
    ah.principal
   FROM escola.arquivos_atividades aa
     JOIN arquivos.hd ah ON ah.arquivo_id = aa.arquivo_id;
