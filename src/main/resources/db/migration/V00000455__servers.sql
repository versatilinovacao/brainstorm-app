CREATE TABLE servers(
	server_id					BIGSERIAL NOT NULL PRIMARY KEY,
	server_host					TEXT,
	server_user					TEXT,
	server_password				TEXT,
	server_port					TEXT,
	server_database				TEXT
);

INSERT INTO servers(server_host,server_user,server_password,server_port,server_database) VALUES('127.0.0.1','postgres','default123','5432','cep_db');
INSERT INTO servers(server_host,server_user,server_password,server_port,server_database) VALUES('127.0.0.1','postgres','default123','5432','seguranca_db');