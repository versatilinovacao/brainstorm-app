CREATE OR REPLACE FUNCTION escola.composicao_chamadas() RETURNS TABLE(
	_chamada_id 				INTEGER,
	_caderno_id				BIGINT,
	_aluno_id				BIGINT,
	_um					BOOLEAN,
	_dois					BOOLEAN,
	_tres					BOOLEAN,
	_quatro					BOOLEAN,
	_cinco					BOOLEAN,
	_seis					BOOLEAN,
	_sete					BOOLEAN,
	_oito					BOOLEAN,
	_nove					BOOLEAN,
	_dez					BOOLEAN,
	_onze					BOOLEAN,
	_doze					BOOLEAN,
	_treze					BOOLEAN,
	_quatorze				BOOLEAN,
	_quinze					BOOLEAN,
	_dezesseis				BOOLEAN,
	_dezessete				BOOLEAN,
	_dezoito				BOOLEAN,
	_dezenove				BOOLEAN,
	_vinte					BOOLEAN,
	_vinteum				BOOLEAN,
	_vintedois				BOOLEAN,
	_vintetres				BOOLEAN,
	_vintequatro				BOOLEAN,
	_vintecinco				BOOLEAN,
	_vinteseis				BOOLEAN,
	_vintesete				BOOLEAN,
	_vinteoito				BOOLEAN,
	_vintenove				BOOLEAN,
	_trinta					BOOLEAN,
	_trintaeum				BOOLEAN,
	_mes					DOUBLE PRECISION,
	_ano					DOUBLE PRECISION
) AS $$
DECLARE
	chamadas_in					escola.chamadas%ROWTYPE;
	aluno_corrente					BIGINT;
	dia_corrente					DOUBLE PRECISION;
	mes_corrente					DOUBLE PRECISION;
	ano_corrente					DOUBLE PRECISION;

	existe						BOOLEAN;
	existe_temp					BOOLEAN;
BEGIN 
	SELECT count(1) > 0 INTO existe_temp FROM pg_class WHERE relname = '_retorno' AND relkind = 'r';	
	IF existe_temp THEN
		BEGIN
			DROP TABLE _retorno;
		EXCEPTION WHEN OTHERS THEN
			RAISE NOTICE 'Tabela temporária de retorno não existe.';
		END;
	END IF;
	
	CREATE TEMP TABLE _retorno(
		chamada_id				INTEGER,
		caderno_id				BIGINT,
		aluno_id				BIGINT,
		um					BOOLEAN,
		dois					BOOLEAN,
		tres					BOOLEAN,
		quatro					BOOLEAN,
		cinco					BOOLEAN,
		seis					BOOLEAN,
		sete					BOOLEAN,
		oito					BOOLEAN,
		nove					BOOLEAN,
		dez					BOOLEAN,
		onze					BOOLEAN,
		doze					BOOLEAN,
		treze					BOOLEAN,
		quatorze				BOOLEAN,
		quinze					BOOLEAN,
		dezesseis				BOOLEAN,
		dezessete				BOOLEAN,
		dezoito					BOOLEAN,
		dezenove				BOOLEAN,
		vinte					BOOLEAN,
		vinteum					BOOLEAN,
		vintedois				BOOLEAN,
		vintetres				BOOLEAN,
		vintequatro				BOOLEAN,
		vintecinco				BOOLEAN,
		vinteseis				BOOLEAN,
		vintesete				BOOLEAN,
		vinteoito				BOOLEAN,
		vintenove				BOOLEAN,
		trinta					BOOLEAN,
		trintaeum				BOOLEAN,
		mes					DOUBLE PRECISION,
		ano					DOUBLE PRECISION		
	);

	FOR chamadas_in IN
		SELECT * FROM escola.chamadas
	LOOP	
		dia_corrente = date_part('DAY',chamadas_in.evento);
		
		SELECT count(1) > 0 INTO existe FROM _retorno r WHERE aluno_id = chamadas_in.aluno_id AND r.mes = date_part('MONTH',chamadas_in.evento) AND r.ano = date_part('YEAR',chamadas_in.evento);
		IF (existe) THEN
			IF (dia_corrente = 1) THEN
				UPDATE _retorno 
					SET um         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 2) THEN
				UPDATE _retorno 
					SET dois         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 3) THEN
				UPDATE _retorno 
					SET tres         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 4) THEN
				UPDATE _retorno 
					SET quatro       = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 5) THEN
				UPDATE _retorno 
					SET cinco        = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 6) THEN
				UPDATE _retorno 
					SET seis         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 7) THEN
				UPDATE _retorno 
					SET sete         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 8) THEN
				UPDATE _retorno 
					SET oito         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 9) THEN
				UPDATE _retorno 
					SET nove         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 10) THEN
				UPDATE _retorno 
					SET dez          = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 11) THEN
				UPDATE _retorno 
					SET onze         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 12) THEN
				UPDATE _retorno 
					SET doze         = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 13) THEN
				UPDATE _retorno 
					SET treze        = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 14) THEN
				UPDATE _retorno 
					SET quatorze     = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 15) THEN
				UPDATE _retorno 
					SET quinze       = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 16) THEN
				UPDATE _retorno 
					SET dezesseis    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 17) THEN
				UPDATE _retorno 
					SET dezessete    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 18) THEN
				UPDATE _retorno 
					SET dezoito      = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 19) THEN
				UPDATE _retorno 
					SET dezenove     = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 20) THEN
				UPDATE _retorno 
					SET vinte        = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 21) THEN
				UPDATE _retorno 
					SET vinteum      = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 22) THEN
				UPDATE _retorno 
					SET vintedois    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 23) THEN
				UPDATE _retorno 
					SET vintetres    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 24) THEN
				UPDATE _retorno 
					SET vintequatro  = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 25) THEN
				UPDATE _retorno 
					SET vintecinco   = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 26) THEN
				UPDATE _retorno 
					SET vinteseis    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 27) THEN --SELECT * FROM _retorno WHERE caderno_id = 1 AND aluno_id = 5 AND mes = 2 AND ano = 2019;
				UPDATE _retorno 
					SET vintesete    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 28) THEN
				UPDATE _retorno 
					SET vinteoito    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 29) THEN
				UPDATE _retorno 
					SET vintenove    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 30) THEN
				UPDATE _retorno 
					SET trinta       = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 31) THEN
				UPDATE _retorno 
					SET trintaeum    = chamadas_in.status
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			END IF;
		ELSE
			aluno_corrente = chamadas_in.aluno_id;
			mes_corrente = date_part('MONTH',chamadas_in.evento); ano_corrente = date_part('YEAR',chamadas_in.evento);
			IF dia_corrente = 1 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,um,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 2 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 3 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,tres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 4 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 5 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,cinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 6 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,seis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 7 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,sete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 8 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,oito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 9 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,nove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 10 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dez,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 11 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,onze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 12 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,doze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 13 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,treze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 14 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatorze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 15 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quinze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 16 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezesseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 17 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezessete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 18 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 19 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 20 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinte,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 21 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 22 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintedois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 23 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintetres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 24 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintequatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 25 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintecinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 26 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 27 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintesete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 28 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 29 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 30 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trinta,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			ELSIF dia_corrente = 31 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trintaeum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,chamadas_in.status,mes_corrente,ano_corrente);
			END IF;
		
		END IF;
	END LOOP;
	
	RETURN QUERY SELECT * FROM _retorno;
END; $$
LANGUAGE plpgsql;


CREATE OR REPLACE VIEW escola.chamadas_report_view AS
	select * from escola.composicao_chamadas();

