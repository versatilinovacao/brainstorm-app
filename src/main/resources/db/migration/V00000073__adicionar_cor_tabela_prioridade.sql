ALTER TABLE public.prioridades			ADD COLUMN	cor				VARCHAR(15);

DELETE FROM public.prioridades;
INSERT INTO public.prioridades(descricao,cor) VALUES('Crítico','black'),('Emergêncial','red'),('Moderado','yellow'),('Leve','blue'),('Neutro','green');