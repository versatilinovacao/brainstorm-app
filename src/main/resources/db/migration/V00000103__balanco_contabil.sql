CREATE TABLE contabil.tipos(
	tipo_id								SERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(100) NOT NULL 
);

INSERT INTO contabil.tipos(nome) VALUES('Fixa');
INSERT INTO contabil.tipos(nome) VALUES('Variável');

CREATE TABLE contabil.operacoes(
	operacao_id							SERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO contabil.operacoes(nome) VALUES('Ativo');
INSERT INTO contabil.operacoes(nome) VALUES('Passivo');
INSERT INTO contabil.operacoes(nome) VALUES('Investimento');
INSERT INTO contabil.operacoes(nome) VALUES('Custo');
INSERT INTO contabil.operacoes(nome) VALUES('Despesas');
INSERT INTO contabil.operacoes(nome) VALUES('Receita');
--INSERT INTO contabil.operacoes(nome) VALUES('Operacional'); //Verificar
--INSERT INTO contabil.operacoes(nome) VALUES('Desembolso');  //Verificar

CREATE TABLE contabil.conta(
	conta_id							SERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(100) NOT NULL,
	tipo_id								BIGINT NOT NULL REFERENCES contabil.tipos(tipo_id)
);

/*
INSERT INTO contabil.conta(nome,tipo_id) VALUES('Caixa',1);
INSERT INTO contabil.conta(nome,tipo_id) VALUES('Investimento',2);
INSERT INTO contabil.conta(nome,tipo_id) VALUES('Equivalentes a Caixa',1);
INSERT INTO contabil.conta(nome,tipo_id) VALUES('Contas a Receber',2);
INSERT INTO contabil.conta(nome,tipo_id) VALUES('Contas a Pagar',2);
*/

CREATE TABLE contabil.contas(
	conta_id							SERIAL NOT NULL PRIMARY KEY,
	colaborador_id						BIGINT REFERENCES public.colaborador(colaborador_id),
	nome								VARCHAR(100),
	descricao							VARCHAR(500),
	operacao_id							BIGINT REFERENCES contabil.operacoes(operacao_id),
	conta_composta_id					BIGINT REFERENCES contabil.contas(conta_id),
	conta_fk_id							BIGINT REFERENCES contabil.conta(conta_id)
);

--select * from contabil.contas
/*
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Ativo',null,null,null);                    --1 
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Circulante',1,null,null);                  --2
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Investimento a Curto Prazo',2,null,null);  --3
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES(null,3,1,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES(null,3,2,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES(null,3,3,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Investimento a Longo Prazo',2,null,null);  --7
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES(null,7,4,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Não Circulante',2,null,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('',,);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Passivo',null,null,null);                   --11
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Circulante',11,null,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Não Circulante',11,null,null);
INSERT INTO contabil.contas(nome,conta_composta_id,conta_fk_id,operacao_id) VALUES('Receita',null,null,4);
*/
--select * from contabil.contas


CREATE TABLE contabil.balanco_patrimonial(
	balanco_patrimonial_id				SERIAL NOT NULL PRIMARY KEY,
	colaborador_id						BIGINT REFERENCES public.colaborador(colaborador_id),
	conta_id							BIGINT NOT NULL REFERENCES contabil.contas(conta_id),
	evento								TIMESTAMP NOT NULL,
	valor								NUMERIC(16,4),
	fechado								BOOLEAN
);

CREATE TABLE contabil.balanco_patrimonial_detalhe(
	balanco_patrimonial_detalhe_id		SERIAL NOT NULL PRIMARY KEY,
	balanco_patrimonial_id				BIGINT NOT NULL REFERENCES contabil.balanco_patrimonial(balanco_patrimonial_id),
	colaborador_id						BIGINT REFERENCES public.colaborador(colaborador_id),
	evento								TIMESTAMP NOT NULL,
	conta_id							BIGINT NOT NULL REFERENCES contabil.contas(conta_id),
	valor								NUMERIC(16,4)
);