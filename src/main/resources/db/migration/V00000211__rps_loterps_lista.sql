CREATE TABLE nfse.loterps_rps(
	loterps_id				BIGINT NOT NULL REFERENCES nfse.loterps(loterps_id),
	rps_id					BIGINT NOT NULL REFERENCES nfse.rps(rps_id)
);
