CREATE TABLE administracao.clausulas(
	clausula_id				BIGSERIAL NOT NULL PRIMARY KEY,
	contrato_id				BIGINT NOT NULL,
	descricao				TEXT NOT NULL
);

ALTER TABLE administracao.clausulas	ADD CONSTRAINT contrato_fk FOREIGN KEY(contrato_id)	REFERENCES administracao.contratos(contrato_id);
CREATE INDEX contrato_idx	ON administracao.clausulas USING btree(contrato_id);

CREATE TABLE administracao.paragrafos(
	paragrafo_id				BIGSERIAL NOT NULL PRIMARY KEY,
	clausula_id				BIGINT NOT NULL,
	descricao				TEXT,
	simbulo					VARCHAR(1),
	unico					BOOLEAN

);

ALTER TABLE administracao.paragrafos	ADD CONSTRAINT clausula_fk	FOREIGN KEY(clausula_id)	REFERENCES administracao.clausulas(clausula_id);
CREATE INDEX clausula_idx	ON administracao.paragrafos USING btree(clausula_id);
