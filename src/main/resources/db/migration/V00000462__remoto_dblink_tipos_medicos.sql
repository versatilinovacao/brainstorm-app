CREATE OR REPLACE FUNCTION dblink_tipos_medicos() RETURNS SETOF record_tipos_medicos AS $$
DECLARE
	reg	record_tipos_medicos%ROWTYPE;
	conexao	text;
BEGIN
	SELECT findbyserver INTO conexao FROM findbyserver('saude_db');
	FOR reg IN 
		SELECT tipomedico_id,nome
		FROM( 
		SELECT * FROM public.dblink (conexao,'select tipomedico_id,nome from public.tipos_medicos')  AS DATA(tipomedico_id BIGINT, nome VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 