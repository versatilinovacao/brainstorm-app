DROP TRIGGER criar_caderno_tgi ON escola.cadernos;
DROP FUNCTION escola.criar_caderno();
CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodoLetivoItem				BIGINT;
	chamada							BIGINT;
	
	inicio_turma					DATE;
	fim_turma						DATE;

	periodos						escola.periodos_letivos_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	eventos							TIMESTAMP%TYPE;			
BEGIN
	SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
	SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;

	FOR alunos IN
		SELECT * FROM public.alunos_turmas WHERE turma_id = turma
	LOOP
		FOR eventos IN
			SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
		LOOP
			SELECT i.periodo_letivo_item_id INTO periodoLetivoItem FROM escola.periodos_letivos_itens i 
								 	  LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = i.periodo_letivo_id	
								 	      WHERE eventos::date BETWEEN periodo_inicial AND periodo_final
									        AND p.status = true;	

			RAISE NOTICE 'PERIODO: %',periodoLetivoItem;
		
			INSERT INTO escola.chamadas(chamada_id,caderno_id,aluno_id,evento,status)
							VALUES(periodoLetivoItem,NEW.caderno_id,alunos.colaborador_id,eventos,false);
		END LOOP;

		INSERT INTO escola.avaliacoes(chamada_id,caderno_id,aluno_id)
			VALUES(periodoLetivoItem,NEW.caderno_id,alunos.colaborador_id);
		
	END LOOP;

	RETURN NEW;
END; $$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER criar_caderno_tgi AFTER INSERT 
	ON escola.cadernos 
		FOR EACH ROW 
			EXECUTE PROCEDURE escola.criar_caderno();
