CREATE TABLE public.prioridades(
	prioridade_id					SERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.prioridades(descricao) VALUES('Preto'),('Vermelho'),('Amarelo'),('Azul'),('Verde');

--LEGENDA
	--Preto 	= Critico
	--Vermelho 	= Urgente
	--Amarelo	= Moderado
	--Verde		= Neutro
	
ALTER TABLE public.chamados ADD COLUMN prioridade_id 			BIGINT;
ALTER TABLE public.chamados	ADD CONSTRAINT prioridade_fk	FOREIGN KEY(prioridade_id) REFERENCES public.prioridades(prioridade_id);
