ALTER TABLE escola.gradecurricular ADD COLUMN composicao_professor_id BIGINT;

ALTER TABLE escola.gradecurricular
	ADD CONSTRAINT professor_fk FOREIGN KEY(professor_id,composicao_professor_id)
		REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
		ON UPDATE NO ACTION ON DELETE NO ACTION;
		
CREATE INDEX fki_gradecurricular_professor_fk
	ON escola.gradecurricular
	USING btree(professor_id,composicao_professor_id);
