ALTER TABLE empresa.empresa ADD COLUMN  	formato_caderno_id			BIGINT;

CREATE TABLE empresa.formato_caderno(
	formato_caderno_id						SERIAL NOT NULL PRIMARY KEY,
	descricao								VARCHAR(60) NOT NULL UNIQUE
);

ALTER TABLE empresa.empresa ADD CONSTRAINT formato_caderno_fk FOREIGN KEY(formato_caderno_id) REFERENCES empresa.formato_caderno(formato_caderno_id);

INSERT INTO empresa.formato_caderno(descricao) VALUES('Continuo');
INSERT INTO empresa.formato_caderno(descricao) VALUES('Individual');

ALTER TABLE escola.cadernos	ADD COLUMN		escola_id				BIGINT;
ALTER TABLE escola.cadernos ADD CONSTRAINT  escola_fk FOREIGN KEY(escola_id) REFERENCES public.colaborador(colaborador_id);		  