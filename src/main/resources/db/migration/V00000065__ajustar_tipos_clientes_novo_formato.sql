UPDATE public.tipos_clientes SET descricao = 'CONSUMIDOR' WHERE tipo_cliente_id = 1;
INSERT INTO public.tipos_clientes(descricao) VALUES('EMPRESA');

CREATE TABLE public.tipos_filiacao(
	tipo_filiacao_id						SERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.tipos_filiacao(descricao) VALUES('PAI'),('MÃE'),('AVÔ'),('AVÓ'),('IRMÃO'),('IRMÃ'),
						   ('TIO'),('TIA'),('SÓCIO(A)'),('FUNCIONÁRIO(A)');
												   
CREATE TABLE public.tipos_responsavel(
	tipo_responsavel_id    						SERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.tipos_responsavel(descricao) VALUES('RESPONSAVEL LEGAL'),('FIADOR'),('PARCEIRO');

CREATE TABLE public.tipos_clientes_tipos_filiacao(
	tipo_cliente_id						BIGINT NOT NULL,
	tipo_filiacao_id					BIGINT NOT NULL
);

ALTER TABLE public.tipos_clientes_tipos_filiacao ADD CONSTRAINT tipo_cliente_tipo_filiacao_pk PRIMARY KEY(tipo_cliente_id,tipo_filiacao_id);
ALTER TABLE public.tipos_clientes_tipos_filiacao ADD CONSTRAINT tipo_cliente_fk FOREIGN KEY(tipo_cliente_id) REFERENCES public.tipos_clientes(tipo_cliente_id);
ALTER TABLE public.tipos_clientes_tipos_filiacao ADD CONSTRAINT tipo_filiacao_fk FOREIGN KEY(tipo_filiacao_id) REFERENCES public.tipos_filiacao(tipo_filiacao_id);

INSERT INTO public.tipos_clientes_tipos_filiacao(tipo_cliente_id,tipo_filiacao_id) 
	VALUES(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),
		  (2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),
		  (3,9),(3,10);
		  
CREATE TABLE public.tipos_filiacao_tipos_responsavel(
	tipo_filiacao_id					BIGINT NOT NULL,
	tipo_responsavel_id					BIGINT NOT NULL
);		  

ALTER TABLE public.tipos_filiacao_tipos_responsavel ADD CONSTRAINT tipo_filiacao_tipo_responsavel_pk PRIMARY KEY(tipo_filiacao_id,tipo_responsavel_id);
ALTER TABLE public.tipos_filiacao_tipos_responsavel ADD CONSTRAINT tipo_filiacao_fk FOREIGN KEY(tipo_filiacao_id) REFERENCES tipos_filiacao(tipo_filiacao_id);
ALTER TABLE public.tipos_filiacao_tipos_responsavel ADD CONSTRAINT tipo_responsavel_fk FOREIGN KEY(tipo_responsavel_id) REFERENCES tipos_responsavel(tipo_responsavel_id);

INSERT INTO public.tipos_filiacao_tipos_responsavel(tipo_filiacao_id,tipo_responsavel_id) 
	VALUES(1,1),(1,2),(2,1),(2,2),(3,1),(3,2),(4,1),(4,2),(5,1),(5,2),(6,1),(6,2),(7,1),(7,2),(8,1),(8,2),
		  (9,1),(9,3),(10,3);
		  
CREATE VIEW  tipos_filiacao_view AS
	SELECT 	f.tipo_filiacao_id,f.descricao,c.tipo_cliente_id	FROM tipos_filiacao f
		INNER JOIN tipos_clientes_tipos_filiacao c ON c.tipo_filiacao_id = f.tipo_filiacao_id;

--SELECT * FROM tipos_filiacao_view WHERE tipo_cliente_id = 3
		
CREATE VIEW tipos_responsavel_view AS
	SELECT	r.tipo_responsavel_id,r.descricao,f.tipo_filiacao_id	FROM tipos_responsavel r
		INNER JOIN tipos_filiacao_tipos_responsavel f ON f.tipo_responsavel_id = r.tipo_responsavel_id;


		





