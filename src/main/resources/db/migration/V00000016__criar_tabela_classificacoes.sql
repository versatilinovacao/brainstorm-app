CREATE TABLE classificacoes(
	classificacao_id				SERIAL NOT NULL,
	descricao						VARCHAR(100)
);

ALTER TABLE classificacoes ADD CONSTRAINT classificacao_pk PRIMARY KEY(classificacao_id);


INSERT INTO classificacoes(descricao) VALUES('EMPRESA'),('CLIENTE'),('TERCEIRIZADO'),('PARCEIRO');

CREATE TABLE classificacao_colaborador(
	colaborador_id						BIGINT,
	classificacao_id					BIGINT
);

ALTER TABLE classificacao_colaborador ADD CONSTRAINT classificacao_colaborador_pk PRIMARY KEY(colaborador_id,classificacao_id);
ALTER TABLE classificacao_colaborador ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES colaborador(colaborador_id);
ALTER TABLE classificacao_colaborador ADD CONSTRAINT classificacao_fk FOREIGN KEY(classificacao_id) REFERENCES classificacoes(classificacao_id);