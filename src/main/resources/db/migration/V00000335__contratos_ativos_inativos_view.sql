ALTER TABLE administracao.contratos		ADD COLUMN status	BOOLEAN;

CREATE OR REPLACE VIEW administracao.contratos_ativos_view	AS
SELECT c.contrato_id,
       c.empresa_id,
       c.composicao_empresa_id,
       c1.nome AS nomeempresa,
       c.contratante_id,
       c.composicao_contratante_id,
       c2.nome AS nomecontratante,
       c.contratado_id,
       c.composicao_contratado_id,
       c3.nome AS nomecontratado,
       p.parcelamento_id,
       p.descricao AS descricaoparcelamento,
       d.pagamento_id,
       d.dia,
       c.assinatura,
       c.renovado_automaticamente,
       c.validade,
       c.total,
       c.status
FROM administracao.contratos c
INNER JOIN public.colaborador c1 ON c1.colaborador_id = c.empresa_id AND c1.composicao_id = c.composicao_empresa_id
INNER JOIN public.colaborador c2 ON c2.colaborador_id = c.contratante_id AND c2.composicao_id = c.composicao_contratante_id
INNER JOIN public.colaborador c3 ON c3.colaborador_id = c.contratado_id AND c3.composicao_id = c.composicao_contratado_id
INNER JOIN administracao.parcelamento p ON p.parcelamento_id = c.parcelamento_id
INNER JOIN administracao.pagamentos d ON d.pagamento_id = c.pagamento_id
WHERE COALESCE(c.status,true) = true;

CREATE OR REPLACE VIEW administracao.contratos_inativos_view	AS
SELECT c.contrato_id,
       c.empresa_id,
       c.composicao_empresa_id,
       c1.nome AS nomeempresa,
       c.contratante_id,
       c.composicao_contratante_id,
       c2.nome AS nomecontratante,
       c.contratado_id,
       c.composicao_contratado_id,
       c3.nome AS nomecontratado,
       p.parcelamento_id,
       p.descricao AS descricaoparcelamento,
       d.pagamento_id,
       d.dia,
       c.assinatura,
       c.renovado_automaticamente,
       c.validade,
       c.total,
       c.status
FROM administracao.contratos c
INNER JOIN public.colaborador c1 ON c1.colaborador_id = c.empresa_id AND c1.composicao_id = c.composicao_empresa_id
INNER JOIN public.colaborador c2 ON c2.colaborador_id = c.contratante_id AND c2.composicao_id = c.composicao_contratante_id
INNER JOIN public.colaborador c3 ON c3.colaborador_id = c.contratado_id AND c3.composicao_id = c.composicao_contratado_id
INNER JOIN administracao.parcelamento p ON p.parcelamento_id = c.parcelamento_id
INNER JOIN administracao.pagamentos d ON d.pagamento_id = c.pagamento_id
WHERE COALESCE(c.status,true) = false;
