CREATE TABLE servico.ordensservicos(
	ordemservico_id			BIGSERIAL NOT NULL PRIMARY KEY,
	abertura			TIMESTAMP NOT NULL,
	emissao				TIMESTAMP,
	fechamento			TIMESTAMP,
	issqn				NUMERIC(16,4),
	retencaoissqn			NUMERIC(16,4),
	desconto_valor			NUMERIC(16,4),
	desconto_percentual		NUMERIC(16,4),
	total				NUMERIC(16,4),
	discriminador			VARCHAR(50)
);

CREATE TABLE servico.itensordensservicos(
	itemordemservico_id		BIGSERIAL 	NOT NULL PRIMARY KEY,
	ordemservico_id			BIGINT		NOT NULL REFERENCES servico.ordensservicos(ordemservico_id),
	artefato_id			BIGINT	 	NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
	cnae_id				BIGINT		NOT NULL REFERENCES contabil.cnaes(cnae_id),
	descricao			VARCHAR(100) 	NOT NULL,
	valor				NUMERIC(16,4) 	NOT NULL,
	issqn				NUMERIC(16,4),
	basecalculo			NUMERIC(16,4),
	desconto			NUMERIC(16,4),
	valorliquido			NUMERIC(16,4),
	discriminador			VARCHAR(50)
);
