CREATE TABLE public.responsaveis(
	responsavel_id						BIGINT NOT NULL,
	nome								VARCHAR(100)
);

ALTER TABLE public.responsaveis ADD CONSTRAINT responsavel_pk PRIMARY KEY(responsavel_id);

CREATE TABLE public.responsaveis_colaboradores(
	responsavel_id						BIGINT,
	colaborador_id						BIGINT
);

ALTER TABLE public.responsaveis_colaboradores ADD CONSTRAINT responsaveis_colaboradores_pk PRIMARY KEY(responsavel_id,colaborador_id);
ALTER TABLE public.responsaveis_colaboradores ADD CONSTRAINT responsavel_fk FOREIGN KEY(responsavel_id) REFERENCES public.responsaveis(responsavel_id);
ALTER TABLE public.responsaveis_colaboradores ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);

--CREATE VIEW contatos_view AS 	
--		select t.contato_id,
--		       (select nome from colaborador c1 where colaborador_id = t.contato_id) as nome, '' as telefone, '' as ramal, '' as tipo, '' as email, c.colaborador_id as colaborador_id 
--				from contatos t
--					inner join contatos_colaboradores cc on cc.contato_id = t.contato_id
--					inner join colaborador c on c.colaborador_id = cc.colaborador_id;
