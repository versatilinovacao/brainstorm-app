DROP VIEW escola.is_cadernos_inconsistencias_view;
DROP VIEW escola.cadernos_inconsistencias_find;
--DROP VIEW escola.consistencias_view;
DROP TABLE escola.cadernos_inconsistencias;
--DROP TABLE escola.consistencias;

/* Cadastro com as consistencias que serão montadas pelo caderno para o caderno */
CREATE TABLE escola.consistencias(
	consistencia_id					SERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(60) NOT NULL,
	descricao						VARCHAR(200) NOT NULL,
	link							VARCHAR(200)
);

INSERT INTO escola.consistencias(nome,descricao) VALUES('Cadastro Período Letivo','Não existe período letivo valido, você precisa acessar o cadastro colaboradores e ir até a ABA empresa para esse ajuste');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Forma do Caderno','Entre no cadastro de colaboradores e va até a ABA empresa e escolha o formato que o caderno deve ter');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Período Letivos','Existe mais de um cadastro de períodos letivos para uma mesma empresa com status de ativo. Você precisara deixar apenas um período por empresa ativo.');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Matricula','Não foram localizados, alunos matriculados e vinculados a uma turma');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Classificação da Empresa','Você precisa configurar o cadastro da sua empresa, antes de continuar');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Critério de Conselho de classe','O critério máximo para liberar a aprovação do aluno por conselho, ainda não foi configurado.');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Cadastro de Curso','Não existe nenhum curso cadastrado para esta escola, cadastre-o antes de continuar.');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Grades do curso','Não existe uma grade formatada corretamente para este curso.');
INSERT INTO escola.consistencias(nome,descricao) VALUES('Cadastro de Turma','Antes de criar o caderno, deve ser montada a turma.');

--DROP TABLE escola.cadernos_inconsistencias;
CREATE TABLE escola.cadernos_inconsistencias(
	caderno_inconsistencia_id		SERIAL NOT NULL PRIMARY KEY,
	caderno_id						BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	consistencia_id					BIGINT NOT NULL REFERENCES escola.consistencias(consistencia_id),
	status							BOOLEAN DEFAULT FALSE
);

CREATE VIEW escola.is_cadernos_inconsistencias_view AS 
	SELECT caderno_id,
	       (SELECT (count(1) > 0) AS isErro FROM escola.cadernos_inconsistencias b WHERE b.caderno_id = a.caderno_id and status = false) AS isErro
	FROM escola.cadernos_inconsistencias a group by caderno_id;


CREATE VIEW escola.consistencias_view AS 
	SELECT i.caderno_inconsistencia_id,
		   c.nome,
		   c.descricao,
		   c.link,
		   i.caderno_id
	FROM escola.cadernos_inconsistencias i
	INNER JOIN escola.consistencias c ON c.consistencia_id = i.consistencia_id;

CREATE VIEW escola.cadernos_inconsistencias_find AS
	SELECT * FROM escola.cadernos_inconsistencias;

