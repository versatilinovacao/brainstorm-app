ALTER TABLE public.telefones_colaboradores ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.telefones_colaboradores
	ADD CONSTRAINT telefones_colaboradores_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_telefones_colaboradores_fk
	ON public.telefones_colaboradores
	USING btree(colaborador_id,composicao_colaborador_id);
	