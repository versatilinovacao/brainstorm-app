CREATE TYPE record_cidades	AS ( cidade_id INTEGER, descricao VARCHAR, uf VARCHAR, cep VARCHAR, codigo_ibge VARCHAR, area NUMERIC(16,4), municipiosubordinado_id INTEGER, estado_id BIGINT );

CREATE OR REPLACE FUNCTION dblink_cidades() RETURNS SETOF record_cidades AS $$
DECLARE
	reg	record_cidades%ROWTYPE;
BEGIN
	BEGIN
		CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE SERVER cep_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'cep_db');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE USER MAPPING FOR postgres SERVER cep_server OPTIONS (user 'postgres', password 'default123');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		GRANT USAGE ON FOREIGN SERVER cep_server TO postgres;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;

	FOR reg IN 
		SELECT id_cidade AS cidade_id, cidade AS descricao, uf, cep, cod_ibge AS codigo_ibge, area, id_municipio_subordinado AS municipiosubordinado_id, estado_id FROM public.dblink ('cep_server','select id_cidade, cidade, uf, cep, cod_ibge, area, id_municipio_subordinado, estado_id from public.qualocep_cidade')  AS DATA( id_cidade INTEGER, cidade VARCHAR, uf VARCHAR, cep VARCHAR, cod_ibge VARCHAR, area NUMERIC(16,4), id_municipio_subordinado INTEGER, estado_id BIGINT )	
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.cidades_view AS 
select cidade_id, descricao, uf, cep, codigo_ibge, area, municipiosubordinado_id, estado_id from dblink_cidades() 
