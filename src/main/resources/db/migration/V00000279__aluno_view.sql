DROP VIEW alunos_view;

CREATE OR REPLACE VIEW alunos_view AS 
 SELECT c.colaborador_id,
 		c.composicao_id AS composicao_aluno_id,
    	c.nome,
    	cl.matricula
   FROM colaborador c
     JOIN cliente cl ON cl.cliente_id = c.cliente_id
  WHERE cl.tipo_cliente_id = (( SELECT tipos_clientes.tipo_cliente_id
           FROM tipos_clientes
          WHERE tipos_clientes.descricao::text = 'ALUNO'::text)) AND c.status = true;