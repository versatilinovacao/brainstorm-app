CREATE TABLE public.tiposambientes(
	tipo_ambiente_id				SERIAL NOT NULL,
	descricao						VARCHAR(100) UNIQUE
);

ALTER TABLE public.tiposambientes ADD CONSTRAINT tipo_ambiente_pk PRIMARY KEY(tipo_ambiente_id);

INSERT INTO public.tiposambientes(descricao) VALUES('PRÉDIO'),('SALA'),('MESANINO'),('COBERTURA'),('SALÃO'),('ESCRITÓRIO'),('PAVILHÃO'),('BANHEIRO'),('QUARTO'),('COZINHA'),('REFEITÓRIO'),('ESTACIONAMENTO'),('PATIO'),('GINÁSIO'),('PRACINHA'),('LANCHERIA');

CREATE TABLE public.ambientes(
	ambiente_id						SERIAL NOT NULL,
	composicao_id					BIGINT,
	descricao						VARCHAR(100),
	largura							NUMERIC(12,4),
	altura							NUMERIC(12,4),
	comprimento						NUMERIC(12,4),
	capacidade_pessoas				BIGINT,
	tipo_ambiente_id				BIGINT						
);

ALTER TABLE public.ambientes ADD CONSTRAINT ambiente_pk PRIMARY KEY(ambiente_id);
ALTER TABLE public.ambientes ADD CONSTRAINT tipo_ambiente_fk FOREIGN KEY(tipo_ambiente_id) REFERENCES public.tiposambientes(tipo_ambiente_id);

INSERT INTO public.ambientes(descricao,capacidade_pessoas,tipo_ambiente_id) VALUES('SALA 1',20,2),('SALA 2',20,2),('SALA 3',20,2),('SALA 4',20,2),('SALA 5',20,2);
