ALTER TABLE administracao.contratos			ADD COLUMN negociacao_id				BIGINT;

ALTER TABLE administracao.contratos			ADD CONSTRAINT negociacao_fk	FOREIGN KEY(negociacao_id)	REFERENCES financeiro.negociacoes(negociacao_id);

CREATE INDEX contrato_negociacao_idx	ON administracao.contratos	USING btree(negociacao_id);

INSERT INTO financeiro.negociacoes(descricao) VALUES('Carne');