CREATE TYPE record_estados	AS (estado_id BIGINT, sigla VARCHAR, descricao VARCHAR, codigo_ibge INTEGER, pais_id BIGINT);

CREATE OR REPLACE FUNCTION dblink_estados() RETURNS SETOF record_estados AS $$
DECLARE
	reg	record_estados%ROWTYPE;
BEGIN
	BEGIN
		CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE SERVER cep_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'cep_db');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE USER MAPPING FOR postgres SERVER cep_server OPTIONS (user 'postgres', password 'default123');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		GRANT USAGE ON FOREIGN SERVER cep_server TO postgres;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;

	FOR reg IN 
		SELECT estado_id,uf AS sigla, estado AS descricao, cod_ibge AS codigo_ibge, pais_id
		FROM( 
		SELECT * FROM public.dblink ('cep_server','select estado_id, uf,estado, cod_ibge, pais_id from public.qualocep_estado')  AS DATA(estado_id BIGINT, uf VARCHAR,estado CHARACTER VARYING, cod_ibge integer, pais_id bigint)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.estados_view AS 
select estado_id, sigla, descricao, codigo_ibge from dblink_estados(); 