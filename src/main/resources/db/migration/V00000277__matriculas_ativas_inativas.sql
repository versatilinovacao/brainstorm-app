CREATE OR REPLACE VIEW escola.matriculas_ativas_cursos_view				AS
	SELECT m.matricula_id,
		   m.codigo,
		   m.colaborador_id,
		   m.composicao_colaborador_id,
		   m.situacao_matricula_id,
		   m.curso_id,
		   m.competencia_inicial,
		   m.competencia_final,
		   m.status
		FROM matriculas m
		WHERE m.status = true;
		
CREATE OR REPLACE VIEW escola.matriculas_inativas_cursos_view				AS
	SELECT m.matricula_id,
		   m.codigo,
		   m.colaborador_id,
		   m.composicao_colaborador_id,
		   m.situacao_matricula_id,
		   m.curso_id,
		   m.competencia_inicial,
		   m.competencia_final,
		   m.status
		FROM matriculas m
		WHERE m.status = false;		