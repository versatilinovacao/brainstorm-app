DROP INDEX escola.atividades_alunos_idx;
DROP INDEX escola.atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx;
DROP MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view;

CREATE MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view AS 
 SELECT a.colaborador_id AS aluno_id,
    a.composicao_id AS composicao_aluno_id,
    a.colaborador_id,
    a.composicao_id,
    c.nome AS nomealuno,
    aa.atividade_id,
    at.avaliacaoalunoatividade_id,
    at.nota,
    at.conceito,
    at.avaliacao_descritiva,
    aa.alunoatividade_id,
    at.situacaoatividade_id,
    s.nome AS nomesituacaoatividade,
    at.revisar,
    false AS concluido
   FROM escola.alunos a
     JOIN escola.alunos_atividades aa ON aa.aluno_id = a.colaborador_id AND aa.composicao_aluno_id = a.composicao_id
     LEFT JOIN colaborador c ON c.colaborador_id = a.colaborador_id AND c.composicao_id = a.composicao_id
     LEFT JOIN escola.avaliacao_aluno_atividade at ON at.aluno_id = a.colaborador_id AND at.composicao_aluno_id = a.composicao_id AND at.atividade_id = aa.atividade_id
     LEFT JOIN escola.situacoes_atividades s ON s.situacaoatividade_id = at.situacaoatividade_id
WITH DATA;

ALTER TABLE escola.atividades_alunos_avaliacao_view
  OWNER TO postgres;

-- Index: escola.atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx

-- DROP INDEX escola.atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx;

CREATE UNIQUE INDEX atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx
  ON escola.atividades_alunos_avaliacao_view
  USING btree
  (aluno_id, composicao_aluno_id, atividade_id);

-- Index: escola.atividades_alunos_idx

-- DROP INDEX escola.atividades_alunos_idx;

CREATE INDEX atividades_alunos_idx
  ON escola.atividades_alunos_avaliacao_view
  USING hash
  (nomealuno COLLATE pg_catalog."default");
  
  
DROP VIEW escola.atividades_alunos_view;

CREATE OR REPLACE VIEW escola.atividades_alunos_view AS 
 SELECT at.alunoatividade_id,
    at.aluno_id,
    at.composicao_aluno_id,
    a.atividade_id,
    ta.descricao AS descricaotipoatividade,
    a.gradecurricular_id,
    a.data_atividade,
    a.titulo,
    a.descricao,
    a.orientacoes,
    aa.avaliacao_descritiva AS avaliacao,
    aa.nota,
    aa.conceito,
    a.entrega,
    a.concluido,
    t.descricao AS descricaoturma,
    g.materia_id,
    m.descricao AS descricaomateria,
    c.colaborador_id AS professor_id,
    c.composicao_id AS composicao_professor_id,
    c.nome AS nomeprofessor,
    aa.revisar,
    aa.situacaoatividade_id,
    s.nome AS nomesituacaoatividade,
    a.status
   FROM escola.alunos_atividades at
     JOIN escola.atividades a ON a.atividade_id = at.atividade_id
     JOIN escola.alunos e ON e.colaborador_id = at.aluno_id AND e.composicao_id = at.composicao_aluno_id
     JOIN escola.gradecurricular g ON g.gradecurricular_id = a.gradecurricular_id
     JOIN turmas t ON t.turma_id = g.turma_id
     LEFT JOIN colaborador c ON c.colaborador_id = g.professor_id AND c.composicao_id = g.composicao_professor_id
     LEFT JOIN materias m ON m.materia_id = g.materia_id
     LEFT JOIN escola.tipos_atividades ta ON ta.tipoatividade_id = a.tipoatividade_id
     LEFT JOIN escola.avaliacao_aluno_atividade aa ON aa.aluno_id = at.aluno_id AND aa.composicao_aluno_id = at.composicao_aluno_id AND aa.atividade_id = a.atividade_id
     LEFT JOIN escola.situacoes_atividades s ON s.situacaoatividade_id = aa.situacaoatividade_id;

