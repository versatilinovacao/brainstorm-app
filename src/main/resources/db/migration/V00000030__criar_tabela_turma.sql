CREATE TABLE public.turmas(
	turma_id					SERIAL NOT NULL,
	descricao					VARCHAR(100) UNIQUE,
	inicio						DATE,
	fim							DATE
);

ALTER TABLE public.turmas ADD CONSTRAINT turma_pk PRIMARY KEY(turma_id);