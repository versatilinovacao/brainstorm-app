DROP FUNCTION escola.alunos_materializados() CASCADE;

CREATE OR REPLACE FUNCTION escola.materializar_alunos()
  RETURNS trigger AS $$
BEGIN
	REFRESH MATERIALIZED VIEW CONCURRENTLY escola.atividades_alunos_avaliacao_view;
	RETURN NULL;
END; $$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER atualiar_aluno_atividade
  AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
  ON escola.alunos_atividades
  FOR EACH STATEMENT
  EXECUTE PROCEDURE escola.materializar_alunos();