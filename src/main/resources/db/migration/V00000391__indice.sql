CREATE TABLE administracao.indices(
	indice_id				SERIAL NOT NULL PRIMARY KEY,
	sigla					VARCHAR(6),
	descricao				VARCHAR(100)
);

INSERT INTO administracao.indices(sigla,descricao) VALUES('IGPM','Indice Geral de Preços do Mercado');
