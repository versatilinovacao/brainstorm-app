CREATE OR REPLACE VIEW escola.arquivos_por_atividades_view AS
SELECT apa.arquivoporatividade_id,
       apa.aluno_id,
       apa.composicao_aluno_id,
       apa.arquivo_id,
       apa.atividade_id,
       hd.nome,
       hd.extensao,
       hd.tipo,
       hd.tamanho,
       hd.registro,
       hd.url,
       hd.arquivo
FROM escola.arquivos_por_atividades apa
INNER JOIN arquivos.hd hd ON hd.arquivo_id = apa.arquivo_id;