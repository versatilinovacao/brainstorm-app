DROP VIEW especializacoes_ativas_view;
DROP VIEW especializacoes_inativas_view;

CREATE OR REPLACE VIEW especializacoes_ativas_view AS
SELECT e.especializacao_id,
       e.descricao,
       e.status
FROM especializacoes e
WHERE e.status = true;

CREATE OR REPLACE VIEW especializacoes_inativas_view AS
SELECT e.especializacao_id,
       e.descricao,
       e.status
FROM especializacoes e
WHERE e.status = false;
