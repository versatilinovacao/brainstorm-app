DROP VIEW usuariosview;
CREATE OR REPLACE VIEW usuariosview AS 
 SELECT u.usuario_id,
    u.empresa_id,
    u.nome,
    i.data_nascimento as nascimento,
    u.senha,
    u.status,
    u.email,
    u.isserver,
    u.isclient,
    u.iscounter,
    i.foto_thumbnail
   FROM usuario u 
     LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
  WHERE u.status = true;


