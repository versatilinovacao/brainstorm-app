CREATE TABLE public.cargos(
	cargo_id					SERIAL NOT NULL,
	descricao					VARCHAR(100)
);

ALTER TABLE public.cargos ADD CONSTRAINT cargo_pk PRIMARY KEY(cargo_id);

ALTER TABLE public.colaborador ADD CONSTRAINT cargo_fk FOREIGN KEY(cargo_id) REFERENCES public.cargos(cargo_id);