CREATE TABLE public.composicao_turnos(
	turno_id			BIGSERIAL NOT NULL PRIMARY KEY,
	descricao			VARCHAR(50) NOT NULL UNIQUE,
	inicio				TIME NOT NULL,
	fim					TIME NOT NULL
);

INSERT INTO public.composicao_turnos(descricao,inicio,fim) VALUES('Madrugada','00:00','07:59');
INSERT INTO public.composicao_turnos(descricao,inicio,fim) VALUES('Manha','08:00','11:59');
INSERT INTO public.composicao_turnos(descricao,inicio,fim) VALUES('tarde','12:00','17:59');
INSERT INTO public.composicao_turnos(descricao,inicio,fim) VALUES('Noite','18:00','23:59');