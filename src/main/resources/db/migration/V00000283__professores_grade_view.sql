DROP VIEW escola.professores_grade_view;
CREATE OR REPLACE VIEW escola.professores_grade_view AS 
 SELECT c.colaborador_id,
        c.nome,
		a.cbo
   FROM colaborador c
  INNER JOIN parceiros p ON p.parceiro_id = c.parceiro_id
  INNER JOIN cargos a ON a.cargo_id = p.cargo_id
	     AND a.cbo IN (SELECT codigo FROM filtro.cbos_parceiros WHERE classificacao_id = (SELECT classificacaocbo_id FROM filtro.classificacoes_cbos WHERE descricao = 'Professor'))
  WHERE COALESCE(c.parceiro_id, 0::bigint) > 0 
    AND c.status = true;
