DROP VIEW telefones_colaboradores_view;

CREATE OR REPLACE VIEW telefones_colaboradores_view AS 
 SELECT visualizacao_telefones.telefone_id,
    visualizacao_telefones.numero,
    visualizacao_telefones.ramal,
    visualizacao_telefones.tipo,
    visualizacao_telefones.colaborador_id,
    visualizacao_telefones.composicao_colaborador_id
   FROM ( SELECT t.telefone_id,
            t.numero,
            t.ramal,
            tt.descricao AS tipo,
            tc.colaborador_id,
            tc.composicao_colaborador_id
           FROM telefones_colaboradores tc
             JOIN telefones t ON t.telefone_id = tc.telefone_id
             JOIN tipos_telefones tt ON tt.tipo_telefone_id = t.tipo_telefone_id) visualizacao_telefones
  GROUP BY visualizacao_telefones.telefone_id, visualizacao_telefones.numero, visualizacao_telefones.ramal, visualizacao_telefones.tipo, visualizacao_telefones.colaborador_id,visualizacao_telefones.composicao_colaborador_id;
