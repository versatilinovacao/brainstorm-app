CREATE OR REPLACE VIEW responsavel_view AS 
SELECT responsavel_id,nome FROM (
SELECT r.responsavel_id,
( SELECT c1.nome
          FROM colaborador c1
          WHERE c1.colaborador_id = rc.responsavel_id) AS nome
   FROM responsaveis r
     JOIN responsaveis_colaboradores rc ON rc.responsavel_id = r.responsavel_id
     JOIN colaborador c ON c.colaborador_id = rc.colaborador_id) AS responsavel_view
   GROUP BY responsavel_id,nome;

