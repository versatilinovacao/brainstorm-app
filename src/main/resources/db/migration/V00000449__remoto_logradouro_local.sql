CREATE OR REPLACE VIEW remoto.logradouros_view AS 
 SELECT e.cep,
    e.logradouro,
    e.complemento,
    e.local,
    e.bairro_id,
    c.cidade_id,
    u.estado_id,
    p.pais_id,
    b.descricao AS descricaobairro,
    c.descricao AS descricaocidade,
    u.sigla AS siglauf,
    u.descricao AS descricaoestado,
    p.sigla AS siglapais,
    p.descricao AS descricaopais
   FROM remoto.enderecos_view e
     LEFT JOIN remoto.bairros_view b ON b.bairro_id = e.bairro_id
     LEFT JOIN remoto.cidades_view c ON c.cidade_id = b.cidade_id
     LEFT JOIN remoto.estados_view u ON u.estado_id = c.estado_id
     LEFT JOIN remoto.pais_view p ON p.pais_id = u.pais_id;

--DROP TABLE remoto.logradouros;
CREATE TABLE remoto.logradouros(
	cep					VARCHAR(08) NOT NULL PRIMARY KEY,
	logradouro				VARCHAR(100),
	complemento				VARCHAR(100),
	local					VARCHAR(100),
	bairro_id				BIGINT,
	cidade_id				BIGINT,
	estado_id				BIGINT,
	pais_id					BIGINT,
	descricaobairro				VARCHAR(100),
	descricaocidade				VARCHAR(100),
	descricaoestado				VARCHAR(100),
	descricaopais				VARCHAR(100),
	siglauf					VARCHAR(3),
	siglapais				VARCHAR(3)
);
