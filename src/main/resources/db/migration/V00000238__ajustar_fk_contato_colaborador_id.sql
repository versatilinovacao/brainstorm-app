ALTER TABLE public.contatos_colaboradores ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.contatos_colaboradores
	ADD CONSTRAINT contatos_colaboradores_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_contatos_colaboradores_fk
	ON public.contatos_colaboradores
	USING btree(colaborador_id,composicao_colaborador_id);
	