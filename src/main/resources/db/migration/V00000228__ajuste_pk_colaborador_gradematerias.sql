ALTER TABLE public.gradematerias ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.gradematerias
	ADD CONSTRAINT colaborador_fk FOREIGN KEY (colaborador_id,composicao_colaborador_id)
		REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
		ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_gradematerias_colaborador_fk
	ON public.gradematerias
	USING btree(colaborador_id,composicao_colaborador_id);
