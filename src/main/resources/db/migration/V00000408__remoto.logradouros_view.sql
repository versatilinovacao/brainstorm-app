CREATE OR REPLACE VIEW remoto.logradouros_view AS
SELECT e.cep,
       e.logradouro,
       e.complemento,
       e.local,
       e.bairro_id,
       c.cidade_id,
       u.estado_id,
       p.pais_id,
       b.descricao AS descricaobairro, c.descricao AS descricaocidade,
       u.sigla AS siglauf, u.descricao AS descricaoestado,
       p.sigla AS siglapais, p.descricao AS descricaopais FROM remoto.enderecos_view e
INNER JOIN remoto.bairros_view b ON b.bairro_id = e.bairro_id
INNER JOIN remoto.cidades_view c ON c.cidade_id = b.cidade_id
INNER JOIN remoto.estados_view u ON u.estado_id = c.estado_id
INNER JOIN remoto.pais_view p ON p.pais_id = u.pais_id;