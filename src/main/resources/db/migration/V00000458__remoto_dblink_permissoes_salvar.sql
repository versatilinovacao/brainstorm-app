CREATE OR REPLACE FUNCTION dblink_permissoes_salvar()
  RETURNS trigger AS
$BODY$
DECLARE
	conexao		TEXT;
	comando		VARCHAR(250);
	id		BIGINT;
BEGIN
	select findbyserver into conexao from findbyserver('seguranca_db');
	IF (TG_OP = 'UPDATE') THEN
		SELECT permissao_id INTO id
		FROM ( 
		SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||OLD.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||COALESCE(NEW.label,'')||''', descricao = '''||COALESCE(NEW.descricao,'')||''' WHERE permissao_id = '||id||';';

	ELSE
		comando = 'INSERT INTO permissao(nome,label,descricao) VALUES('''||NEW.nome||''','''||COALESCE(NEW.label,'')||''','''||NEW.descricao||''');';
	END IF;
	PERFORM dblink_exec(conexao,comando);
	

	RETURN NEW;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dblink_permissoes_salvar()
  OWNER TO postgres;
