DROP VIEW escola.avaliacoes_view;
DROP VIEW escola.chamadas_view;
CREATE VIEW escola.chamadas_view AS
	SELECT c.chamada_id,
		   c.caderno_id,
		   s.descricao AS nome_caderno,
		   c.evento,
		   c.aluno_id, 
		   a.nome AS nome_aluno,
		   c.periodo_letivo_item_id,
		   c.conceito,c.nota,
		   c.avaliacao,
		   c.status,
		   c.acompanhamento
		FROM escola.chamadas c
		INNER JOIN public.colaborador a ON a.colaborador_id = c.aluno_id
		INNER JOIN escola.cadernos s ON s.caderno_id = c.caderno_id;
		
CREATE OR REPLACE VIEW escola.avaliacoes_view AS 
 SELECT a.avaliacao_id,
    a.periodo_letivo_item_id,
    a.caderno_id,
    a.aluno_id,
    c.nome AS nome_aluno,
    p.descricao AS periodo_letivo_nome,
    a.aproveitamento,
    a.conceito,
    ( SELECT sum(chamadas_view.nota) AS sum
           FROM escola.chamadas_view
          WHERE chamadas_view.chamada_id = a.periodo_letivo_item_id AND chamadas_view.aluno_id = chamadas_view.aluno_id) AS nota,
    a.avaliacao
   FROM escola.avaliacoes a
     LEFT JOIN colaborador c ON c.colaborador_id = a.aluno_id
     LEFT JOIN escola.periodos_letivos_itens p ON p.periodo_letivo_item_id = a.periodo_letivo_item_id;