DELETE FROM public.usuario_permissao;
DELETE FROM public.permissao;

UPDATE public.usuario SET senha = '$2a$10$jvvqI58e2naN149yt7iPZOoUlvv4uwnnDclx1Se3TQwEuYhxvS26e' WHERE usuario_id = 1;
ALTER TABLE public.permissao ADD CONSTRAINT permissao_unica UNIQUE(nome);
