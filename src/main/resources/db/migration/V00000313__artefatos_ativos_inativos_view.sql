ALTER TABLE almoxarifado.artefatos	ADD COLUMN status						BOOLEAN;
ALTER TABLE almoxarifado.artefatos	ADD COLUMN composicao_colaborador_id	BIGINT;

ALTER TABLE almoxarifado.artefatos	ADD CONSTRAINT artefato_colaborador_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)	 REFERENCES colaborador(colaborador_id,composicao_id);
 
CREATE OR REPLACE VIEW almoxarifado.artefatos_ativos_view AS
	SELECT a.artefato_id,
	       a.nome,
	       a.descricao,
	       a.preco,
	       c.nome AS classificacao,
	       t.nome AS tipo,
	       s.nome AS artigo,
               a.colaborador_id,
               a.composicao_colaborador_id,
	       a.status
	 FROM almoxarifado.artefatos a
   INNER JOIN almoxarifado.classificacoes c ON c.classificacao_id = a.classificacao_id
   INNER JOIN almoxarifado.tipos t ON t.tipo_id = a.tipo_id
   INNER JOIN almoxarifado.artigos s ON s.artigo_id = a.artigo_id
 	WHERE a.status = true;


CREATE OR REPLACE VIEW almoxarifado.artefatos_inativos_view AS
	SELECT a.artefato_id,
	       a.nome,
	       a.descricao,
	       a.preco,
	       c.nome AS classificacao,
	       t.nome AS tipo,
	       s.nome AS artigo,
	       a.colaborador_id,
	       a.composicao_colaborador_id,
	       a.status
	 FROM almoxarifado.artefatos a
   INNER JOIN almoxarifado.classificacoes c ON c.classificacao_id = a.classificacao_id
   INNER JOIN almoxarifado.tipos t ON t.tipo_id = a.tipo_id
   INNER JOIN almoxarifado.artigos s ON s.artigo_id = a.artigo_id
 	WHERE a.status = false;
