CREATE TABLE public.permissao_por_tela(
	telasistema_id								BIGINT NOT NULL REFERENCES public.telas_sistema(telasistema_id),
	permissao_id								BIGINT NOT NULL REFERENCES public.permissao(permissao_id)
);