DROP VIEW alunos_turmas_view;

CREATE OR REPLACE VIEW alunos_turmas_view AS 
SELECT aluno_id,
       composicao_aluno_id,
       colaborador_id,
       composicao_id,
       nome,
       matricula,
       vinculo,
       status,
       status_periodo_letivo,
       status_grade
FROM ( SELECT t.colaborador_id AS aluno_id,
	    a.composicao_id AS composicao_aluno_id,
	    a.colaborador_id,
	    a.composicao_id,
	    a.nome,
	    m.codigo AS matricula,
	    t.turma_id AS vinculo,
	    s.descricao AS status,
	    p.status AS status_periodo_letivo,
	    g.status AS status_grade
	   FROM alunos_turmas t
	     LEFT JOIN colaborador a ON a.colaborador_id = t.colaborador_id
	     LEFT JOIN matriculas m ON m.colaborador_id = t.colaborador_id
	     LEFT JOIN escola.gradecurricular g ON g.turma_id = t.turma_id AND g.curso_id = m.curso_id
	     LEFT JOIN situacao_matricula s ON s.situacao_matricula_id = m.situacao_matricula_id
	     LEFT JOIN cursos c ON c.curso_id = g.curso_id
	     LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
	  WHERE p.status = true AND COALESCE(c.periodo_letivo_id, 0::bigint) <> 0
	    AND COALESCE(g.status,true) = true) AS al
  GROUP BY aluno_id,composicao_aluno_id,colaborador_id,composicao_id,nome,matricula,vinculo,status,status_periodo_letivo,status_grade
  ORDER BY nome;
