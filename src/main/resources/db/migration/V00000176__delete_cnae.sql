ALTER TABLE contabil.cnaes ADD COLUMN deletado				BOOLEAN;

CREATE TABLE action.deletar_cnae(
	cnae_id				BIGINT NOT NULL PRIMARY KEY
);

CREATE TABLE critica.cnaes(
	critica_id			SERIAL NOT NULL PRIMARY KEY,
	mensagem			VARCHAR(500),
	ocorrencia			TIMESTAMP
);

CREATE OR REPLACE FUNCTION action.deletar_cnae_execute() RETURNS trigger AS $$
DECLARE
	temCritica boolean;
BEGIN	
	SELECT COUNT(1) > 0 INTO temCritica FROM critica.cnaes WHERE cnae_id = NEW.cnae_id;
	
	IF NOT temCritica THEN
		DELETE FROM contabil.cnaes WHERE cnae_id = NEW.cnae_id;
	ELSE
		UPDATE contabil.cnaes SET deletado = true WHERE cnae_id = NEW.cnae_id;
	END IF;

	DELETE FROM action.deletar_cnae WHERE cnae_id = NEW.cnae_id;

	RETURN NEW;
END; $$
	LANGUAGE plpgsql;

CREATE TRIGGER deletar_cnaes_tgi
	AFTER INSERT 
		ON action.deletar_cnae FOR EACH ROW
			EXECUTE PROCEDURE action.deletar_cnae_execute();

CREATE OR REPLACE VIEW contabil.cnaesview AS
	SELECT * FROM contabil.cnaes WHERE COALESCE(deletado,false) = false;
	
CREATE OR REPLACE VIEW contabil.cnaesignoradosview AS
	SELECT * FROM contabil.cnaes WHERE deletado = true;