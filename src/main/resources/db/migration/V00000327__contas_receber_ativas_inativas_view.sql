DROP VIEW financeiro.contas_receber_ativas_view;

CREATE OR REPLACE VIEW financeiro.contas_receber_ativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contareceber_id,
	    c.composicao_id,
        c.total,
        c.devedor_id,
        c.composicao_devedor_id,
        c.parcela,
        c.status
   FROM financeiro.contas_receber c
  WHERE COALESCE(c.status, true) = true;

DROP VIEW financeiro.contas_receber_inativas_view;

CREATE OR REPLACE VIEW financeiro.contas_receber_inativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contareceber_id,
   	    c.composicao_id,
        c.total,
        c.devedor_id,
        c.composicao_devedor_id,
        c.parcela,
        c.status
   FROM financeiro.contas_receber c
  WHERE COALESCE(c.status, true) = false;
  