ALTER TABLE administracao.contratos ADD COLUMN status		BOOLEAN;

CREATE OR REPLACE VIEW administracao.contratos_ativos_view AS
SELECT c.contrato_id,
       c.composicao_id,
       c.contratada_id,
       c.composicao_contratada_id,
       ca.nome AS nomecontratada,
       ca.fantasia AS nomefantasiacontratada,
       c.contratante_id,
       c.composicao_contratante_id,
       ce.nome AS nomecontratante,
       ce.fantasia AS nomefantasiacontratante,
       c.artefato_id,
       a.nome AS nomeartefato,
       c.inicio_contrato,
       c.duracao_inicial,
       c.renovacoes,
       c.mensalidade,
       c.indicereajuste_id,
       i.sigla AS siglaindice,
       i.descricao AS descricaoindice,
       c.tiporenovacao_id,
       r.descricao AS descricaorenovacao,
       c.status
FROM administracao.contratos c
INNER JOIN colaborador ca ON ca.colaborador_id = c.contratada_id AND ca.composicao_id = c.composicao_contratada_id
INNER JOIN colaborador ce ON ce.colaborador_id = c.contratante_id AND ce.composicao_id = c.composicao_contratante_id
INNER JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
LEFT JOIN administracao.indices i ON i.indice_id = c.indicereajuste_id 
LEFT JOIN administracao.tipos_renovacoes r ON r.tiporenovacao_id = c.tiporenovacao_id
WHERE COALESCE(c.status,true) = true;


CREATE OR REPLACE VIEW administracao.contratos_inativos_view AS
SELECT c.contrato_id,
       c.composicao_id,
       c.contratada_id,
       c.composicao_contratada_id,
       ca.nome AS nomecontratada,
       ca.fantasia AS nomefantasiacontratada,
       c.contratante_id,
       c.composicao_contratante_id,
       ce.nome AS nomecontratante,
       ce.fantasia AS nomefantasiacontratante,
       c.artefato_id,
       a.nome AS nomeartefato,
       c.inicio_contrato,
       c.duracao_inicial,
       c.renovacoes,
       c.mensalidade,
       c.indicereajuste_id,
       i.sigla AS siglaindice,
       i.descricao AS descricaoindice,
       c.tiporenovacao_id,
       r.descricao AS descricaorenovacao,
       c.status
FROM administracao.contratos c
INNER JOIN colaborador ca ON ca.colaborador_id = c.contratada_id AND ca.composicao_id = c.composicao_contratada_id
INNER JOIN colaborador ce ON ce.colaborador_id = c.contratante_id AND ce.composicao_id = c.composicao_contratante_id
INNER JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
LEFT JOIN administracao.indices i ON i.indice_id = c.indicereajuste_id 
LEFT JOIN administracao.tipos_renovacoes r ON r.tiporenovacao_id = c.tiporenovacao_id
WHERE COALESCE(c.status,true) = false;
