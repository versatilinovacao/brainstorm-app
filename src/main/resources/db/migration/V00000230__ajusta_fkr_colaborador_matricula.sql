ALTER TABLE matriculas ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE matriculas
	ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
		REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
		ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_matricula_colaborador_fk
	ON matriculas
	USING btree(colaborador_id,composicao_colaborador_id);
