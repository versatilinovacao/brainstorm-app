ALTER TABLE escola.cadernos ADD COLUMN status		BOOLEAN;
UPDATE escola.cadernos SET status = true;

CREATE OR REPLACE VIEW escola.cadernos_ativos_view AS
	SELECT c.caderno_id,
	       c.descricao,
	       c.gradecurricular_id,
	       c.conceito,
	       c.nota,
	       c.periodo_letivo_id,
	       c.escola_id,
	       c.composicao_escola_id,
	       t.turma_id,
	       t.descricao AS nome_turma,
	       c.status
	  FROM escola.cadernos c
		LEFT JOIN escola.gradecurricular g ON g.gradecurricular_id = c.gradecurricular_id
		LEFT JOIN turmas t ON t.turma_id = g.turma_id AND t.status = true
	WHERE c.status = true;


CREATE OR REPLACE VIEW escola.cadernos_inativos_view AS
	SELECT c.caderno_id,
	       c.descricao,
	       c.gradecurricular_id,
	       c.conceito,
	       c.nota,
	       c.periodo_letivo_id,
	       c.escola_id,
	       c.composicao_escola_id,
	       t.turma_id,
	       t.descricao AS nome_turma,
	       c.status
	  FROM escola.cadernos c
		LEFT JOIN escola.gradecurricular g ON g.gradecurricular_id = c.gradecurricular_id
		LEFT JOIN turmas t ON t.turma_id = g.turma_id AND t.status = false
	WHERE c.status = false;