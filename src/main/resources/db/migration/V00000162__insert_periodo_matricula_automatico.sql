CREATE OR REPLACE FUNCTION matricula_competencia() RETURNS trigger AS $$
DECLARE 
	_competencia_inicial			DATE;
	_competencia_final				DATE;
BEGIN
	SELECT competencia_inicial,competencia_final INTO _competencia_inicial,_competencia_final FROM cursos WHERE curso_id = NEW.curso_id;

	NEW.competencia_inicial = _competencia_inicial;
	NEW.competencia_final   = _competencia_final;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER matricula_competencia_tgi
	BEFORE INSERT 
		ON matriculas FOR EACH ROW
			EXECUTE PROCEDURE matricula_competencia();
