CREATE TABLE public.especializacoes(
	especializacao_id							SERIAL NOT NULL,
	descricao									VARCHAR(100)
);

ALTER TABLE public.especializacoes ADD CONSTRAINT especializacao_pk PRIMARY KEY(especializacao_id);

ALTER TABLE public.colaborador ADD CONSTRAINT especializacao_fk FOREIGN KEY(especializacao_id) REFERENCES public.especializacoes(especializacao_id) ;