CREATE SCHEMA recursos;

--DROP TABLE recursos.unidades
CREATE TABLE recursos.unidades(
    unidade_id                          SERIAL NOT NULL PRIMARY KEY,
    unidade                             VARCHAR(8) NOT NULL UNIQUE,
    descricao                           VARCHAR(40) NOT NULL UNIQUE
);

INSERT INTO recursos.unidades(unidade,descricao) VALUES('AMPOLA','AMPOLA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BALDE','BALDE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BANDEJ','BANDEJA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BARRA','BARRA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BISNAG','BISNAGA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BLOCO','BLOCO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BOBINA','BOBINA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('BOMB','BOMBONA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CAPS','CAPSULA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CART','CARTELA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CENTO','CENTO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CJ','CONJUNTO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CM','CENTIMETRO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CM2','CENTIMETRO QUADRADO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX','CAIXA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX2','CAIXA COM 2 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX3','CAIXA COM 3 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX5','CAIXA COM 5 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX10','CAIXA COM 10 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX15','CAIXA COM 15 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX20','CAIXA COM 20 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX25','CAIXA COM 25 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX50','CAIXA COM 50 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('CX100','CAIXA COM 100 UNIDADES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('DISP','DISPLAY');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('DUZIA','DUZIA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('EMBAL','EMBALAGEM');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('FARDO','FARDO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('FOLHA','FOLHA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('FRASCO','FRASCO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('GALAO','GALÃO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('GF','GARRAFA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('GRAMAS','GRAMAS');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('JOGO','JOGO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('KG','QUILOGRAMA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('KIT','KIT');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('LATA','LATA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('LITRO','LITRO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('M','METRO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('M2','METRO QUADRADO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('M3','METRO CÚBICO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('MILHEI','MILHEIRO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('ML','MILILITRO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('MWH','MEGAWATT HORA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('PACOTE','PACOTE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('PALETE','PALETE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('PARES','PARES');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('PC','PEÇA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('POTE','POTE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('K','QUILATE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('RESMA','RESMA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('ROLO','ROLO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('SACO','SACO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('SACOLA','SACOLA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('TAMBOR','TAMBOR');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('TANQUE','TANQUE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('TON','TONELADA');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('TUBO','TUBO');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('UNID','UNIDADE');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('VASIL','VASILHAME');
INSERT INTO recursos.unidades(unidade,descricao) VALUES('VIDRO','VIDRO');

--SELECT * FROM recursos.unidades

CREATE TABLE recursos.tipos_produtos(
    tipo_produto_id                     SERIAL NOT NULL PRIMARY KEY,
    descricao                           VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO recursos.tipos_produtos(descricao) VALUES('Embalagem'),('Materia Prima'),('Ferramenta'),('Veículo'),('Produto'),('Serviço'),('Insumo');

CREATE TABLE recursos.produto_ean(
    produto_ean_id                      SERIAL NOT NULL PRIMARY KEY,
    codigo_ean                          VARCHAR(11) NOT NULL UNIQUE,
    descricao                           VARCHAR(150)
);

CREATE TABLE recursos.ncm(
    ncm_id                              SERIAL NOT NULL PRIMARY KEY,
    ncm                                 VARCHAR(8) NOT NULL UNIQUE,
    categoria                           VARCHAR(150),
    descricao                           VARCHAR(150),
    ipi                                 NUMERIC(16,4) DEFAULT 0,
    inicio_vigencia                     DATE,
    fim_vigencia                        DATE,
    unidade_tributaria                  VARCHAR(8),
    descricao_unidade                   VARCHAR(20),
    cronograma_gtin                     DATE,
    observacao                          VARCHAR(250)
);

CREATE TABLE recursos.estoque(
    estoque_id                          SERIAL NOT NULL PRIMARY KEY,
    quantidade                          NUMERIC(16,4),
    unidade_id                          BIGINT REFERENCES recursos.unidades(unidade_id),
    estoque_reservado                   NUMERIC(16,4) DEFAULT 0,
    estoque_provisionado                NUMERIC(16,4) DEFAULT 0, --Conforme a utilização do estoque, mediante uma série de regras, a quantidade alocada aqui, será utilizada como a provisão necessária para compra afim de reduzir problemas com falta ou sobra.
    custo_medio                         NUMERIC(16,4) DEFAULT 0,
    custo_reposicao                     NUMERIC(16,4) DEFAULT 0
    
);

--DROP TABLE recursos.grupos
CREATE TABLE recursos.grupos(
    grupo_id                            SERIAL NOT NULL PRIMARY KEY,
    grupo_composto_id                   BIGINT REFERENCES recursos.grupos(grupo_id),
    descricao                           VARCHAR(100)
);

CREATE TABLE recursos.produtos_compostos(
    produto_composto_id                 SERIAL NOT NULL PRIMARY KEY,
    produto_vinculado_id                BIGINT REFERENCES recursos.produtos_compostos(produto_composto_id),
    produto_id                          BIGINT NOT NULL,
    quantidade                          NUMERIC(16,4)
);

CREATE TABLE recursos.produtos(
    produto_id                          SERIAL NOT NULL PRIMARY KEY,
    produto_composto_id                 BIGINT REFERENCES recursos.produtos_compostos(produto_composto_id), 
    referencia                          VARCHAR(10) UNIQUE,
    descricao                           VARCHAR(200) UNIQUE,
    descricao_reduzida                  VARCHAR(60) UNIQUE,
    tipo_produto_id                     BIGINT NOT NULL REFERENCES recursos.tipos_produtos(tipo_produto_id)
);

CREATE TABLE recursos.produtos_por_grupos(
    grupo_id                            BIGINT NOT NULL REFERENCES recursos.grupos(grupo_id),
    produto_id                          BIGINT NOT NULL REFERENCES recursos.produtos(produto_id),
    tipo_produto_id                     BIGINT NOT NULL REFERENCES recursos.tipos_produtos(tipo_produto_id),
    estoque_id                          BIGINT NOT NULL REFERENCES recursos.estoque(estoque_id),
    ncm_id                              BIGINT NOT NULL REFERENCES recursos.ncm(ncm_id),
    produto_ean_id                      BIGINT NOT NULL REFERENCES recursos.produto_ean(produto_ean_id)
);

ALTER TABLE recursos.produtos_por_grupos ADD CONSTRAINT produto_por_grupo_pk PRIMARY KEY(grupo_id,produto_id);

INSERT INTO recursos.grupos(descricao) VALUES('VENDA');
INSERT INTO recursos.grupos(descricao) VALUES('LOCAÇÃO');
INSERT INTO recursos.grupos(descricao) VALUES('PRODUÇÂO');
INSERT INTO recursos.grupos(descricao) VALUES('MANUTENÇÃO');

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('PATRIMONIO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('REVENDA',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('CONSUMO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('EXPORTAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('DISTRIBUIÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('INDUSTRIALIZAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('DOAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('BRINDE',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('EMPRESTIMO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 1)); --VENDA

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('DISPONÍVEL',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 2)); --LOCAÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('INDISPONÍVEL',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 2)); --LOCAÇÃO

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ARMAZENAMENTO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('PRODUÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ACABADO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AVARIA',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ROUBO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('DESGASTE',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 3)); --PRODUÇÃO

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('TRIAGEM',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO ANALISE',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ANALISANDO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO ATUAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ATUANDO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO TESTE',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('TESTANDO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO LIBERAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('LIBERADO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO RETIRADA/FINALIZAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ENTREGUE/FINALIZADO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('SUSPENSO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('CANCELADO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AGUARDANDO HOMOLOGAÇÃO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('HOMOLOGADO',(SELECT grupo_id FROM recursos.grupos WHERE grupo_id = 4)); --MANUTENÇÃO

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('INSUMO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('MATERIAL',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('MATERIA PRIMA',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('FERRAMENTA',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('PRODUTO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('VEICULO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('SERVIÇO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --REVENDA
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('AVARIA',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('ROUBO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --PRODUÇÃO
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('DESGASTE',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'REVENDA')); --PRODUÇÃO

INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('INSUMO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('MATERIAL',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('MATERIA PRIMA',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('FERRAMENTA',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('PRODUTO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));
INSERT INTO recursos.grupos(descricao,grupo_composto_id) VALUES('VEICULO',(SELECT grupo_id FROM recursos.grupos WHERE descricao = 'ARMAZENAMENTO'));

