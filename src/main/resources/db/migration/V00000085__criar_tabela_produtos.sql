CREATE SCHEMA recurso;

CREATE TABLE recurso.produtos(
	produto_id					SERIAL NOT NULL PRIMARY KEY,
	produto_composto_id			BIGINT REFERENCES recurso.produtos(produto_id),
	produto_vinculado_id		BIGINT REFERENCES recurso.produtos(produto_id),
	nome						VARCHAR(60) NOT NULL UNIQUE,
	descricao					VARCHAR(200)
);
