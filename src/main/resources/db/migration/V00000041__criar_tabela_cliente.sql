CREATE TABLE public.cliente(
	cliente_id					SERIAL NOT NULL,
	matricula					VARCHAR(30),
	fichamedica					BIGINT,
	consumocalorico				BIGINT
);


ALTER TABLE public.cliente ADD CONSTRAINT cliente_id PRIMARY KEY(cliente_id);

ALTER TABLE public.colaborador ADD COLUMN cliente_id         BIGINT;
ALTER TABLE public.colaborador ADD CONSTRAINT cliente_fk FOREIGN KEY(cliente_id) REFERENCES public.cliente(cliente_id);    			 