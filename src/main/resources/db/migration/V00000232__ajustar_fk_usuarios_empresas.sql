ALTER TABLE public.usuarios_empresas ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.usuarios_empresas
	ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_usuarios_empresas_colaborador_fk
	ON public.usuarios_empresas
	USING btree(colaborador_id,composicao_colaborador_id);
