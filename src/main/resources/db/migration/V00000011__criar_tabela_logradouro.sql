CREATE TABLE public.logradouro(
	cep							VARCHAR(8),
	descricao					VARCHAR(100),
	bairro_id					BIGINT							
);

ALTER TABLE logradouro ADD CONSTRAINT logradouro_pk PRIMARY KEY(cep);
ALTER TABLE logradouro ADD CONSTRAINT bairro_fk FOREIGN KEY(bairro_id) REFERENCES bairros(bairro_id);
ALTER TABLE public.colaborador ADD CONSTRAINT colaborador_logradouro_fk FOREIGN KEY(cep) REFERENCES public.logradouro(cep);