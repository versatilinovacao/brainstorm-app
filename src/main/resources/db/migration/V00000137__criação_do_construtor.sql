CREATE SCHEMA construtor;

CREATE TABLE construtor.tipos(
	tipo_id				SERIAL NOT NULL PRIMARY KEY,
	nome				VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO construtor.tipos(nome) VALUES('Entity'),('Resource'),('Service'),('Repository'),('View');

CREATE TABLE construtor.objetos(
	objeto_id			SERIAL NOT NULL PRIMARY KEY,
	nome				VARCHAR(100) NOT NULL,
	identificacao			VARCHAR(40) NOT NULL,
	tipo_id				BIGINT NOT NULL REFERENCES construtor.tipos(tipo_id)
);

INSERT INTO construtor.objetos(nome,identificacao,tipo_id) VALUES('Cadastro de Usuários','Usuario.class',1);