CREATE OR REPLACE VIEW securityview AS 
	SELECT s.gruposecurity_id,g.nome as nomegrupo,s.telasistema_id,t.descricao,s.permissao_id,p.nome as nomepermissao,s.telasistemapermissao_id
	FROM public.security s
	INNER JOIN public.telas_sistema t ON t.telasistema_id = s.telasistema_id
	INNER JOIN public.gruposecurity g ON g.gruposecurity_id = s.gruposecurity_id
	INNER JOIN public.permissao p ON p.permissao_id = s.permissao_id;