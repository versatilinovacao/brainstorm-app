CREATE OR REPLACE VIEW escola.arquivos_atividades_view AS
SELECT aa.atividade_id,ah.arquivo_id,ah.nome,ah.extensao,ah.tipo,ah.tamanho,ah.registro,ah.url,ah.arquivo from escola.arquivos_atividades aa
inner join arquivos.hd ah ON ah.arquivo_id = aa.arquivo_id;