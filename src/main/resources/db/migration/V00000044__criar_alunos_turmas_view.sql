CREATE VIEW alunos_turmas_view AS
	SELECT a.colaborador_id, a.nome, c.matricula, t.turma_id as vinculo FROM colaborador a
		INNER JOIN alunos_turmas t ON t.colaborador_id = a.colaborador_id
		LEFT JOIN cliente c ON c.cliente_id = a.cliente_id;
