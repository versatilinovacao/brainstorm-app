DROP VIEW usuario_view;
CREATE OR REPLACE VIEW usuario_view AS 
 SELECT u.usuario_id,
    u.empresa_id,
    u.nome,
    u.status,
    u.email,
    i.cpf,
    i.data_nascimento,
    c.email as email_colaborador,
    i.foto_thumbnail
   FROM usuario u
   LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
   LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
  WHERE u.status = true;

