ALTER TABLE empresa.empresa		ADD COLUMN contratomodelo_id				BIGINT;
ALTER TABLE empresa.empresa		ADD CONSTRAINT contratomodelo_fk	FOREIGN KEY(contratomodelo_id) REFERENCES contratos_modelos(contratomodelo_id);
CREATE INDEX empresa_contratomodelo_idx	ON empresa.empresa		USING btree(contratomodelo_id);