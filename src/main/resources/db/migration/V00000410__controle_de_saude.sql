CREATE SCHEMA medicina;

CREATE TABLE medicina.tipos_doencas(
	tipodoenca_id			BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	descricao				VARCHAR(255)
);

CREATE INDEX tipo_doenca_nome_idx	ON medicina.tipos_doencas	USING btree(nome);

INSERT INTO medicina.tipos_doencas(nome,descricao)	VALUES('Congênita','Doença adquirida com o nascimento'),('Genética','Doença produzida por alterações no DNA'),('Comorbidades','Coexistência de doenças'),('Viral','Doença contraida por virus'),('Bacteriana','Doença desenvolvida por ação de bactérias'),('Cronica','São doenças de longa duração e de progressão lenta, e muitas, infelizmente,  ainda não tem cura');

CREATE TABLE medicina.doencas(
	doença_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	tipodoenca_id			BIGINT NOT NULL REFERENCES medicina.tipos_doencas(tipodoenca_id)
);

CREATE INDEX tipodoenca_idx	ON medicina.doencas	USING btree(tipodoenca_id);

INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Deficiência mental',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Nariz achatado ou ausente',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Fissura labial',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Planda dos pés arredondados',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Rosto muito alongado',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Orelhas muito baixas',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Congênita'));

INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Epilético',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Hemofilico',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('hipertenso',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Asmático',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Diabético',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));
INSERT INTO medicina.doencas(nome,tipodoenca_id) VALUES('Cardiáco',(SELECT tipodoenca_id FROM medicina.tipos_doencas WHERE nome = 'Cronica'));

CREATE TABLE medicina.tipos_medicos(
	tipomedico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(60) NOT NULL UNIQUE
);

INSERT INTO medicina.tipos_medicos(nome) VALUES('alopata'),('homeopata');

CREATE TABLE medicina.necessidades_especiais(
	necessidadeespecial_id			BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(100) UNIQUE
);

INSERT INTO medicina.necessidades_especiais(nome) VALUES('Cego'),('Surdo'),('Mudo'),('Hemiplégico'),('Paraplégico'),('Tetraplégico'),('Distúrbio de aprendizagem'),
							('Transtorno de déficit de atenção com hiperatividade'),('Síndrome de Down'),('Miopia'),('Hipermetropia'),
							('Astigmatismo'),('Presbiopia'),('Estrabismo'),('Glaucoma'),('Catarata'),('Descolamento retina'),
							('Ausencia de olfato');

CREATE TABLE medicina.tipos_alergicos(
	tipoalergico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO medicina.tipos_alergicos(nome)	VALUES('Medicamentos'),('Alimentos'),('Embalagens');

CREATE TABLE medicina.alergenicos(
	alergenico_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE,
	descricao					VARCHAR(500),
	tipoalergico_id				BIGINT NOT NULL REFERENCES medicina.tipos_alergicos(tipoalergico_id)
);

CREATE INDEX tipoalergico_idx	ON medicina.alergenicos		USING btree(tipoalergico_id);

INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Crustáceos','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Ovos','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Peixes','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Amendoim','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Soja','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Leites','Origem animal',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Amêndoa','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Avelãs','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Castanha-de-caju','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Castanha-do-pará','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Trigo','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Centeio','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Cevada','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Aveia','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Macadâmias','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Nozes','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Pecãs','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Pistaches','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
INSERT INTO medicina.alergenicos(nome,descricao,tipoalergico_id) VALUES('Pinoli','',(SELECT tipoalergico_id FROM medicina.tipos_alergicos WHERE nome = 'Alimentos'));
