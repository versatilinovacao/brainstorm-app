--DROP VIEW escola.atividades_minor_view;

CREATE OR REPLACE VIEW escola.atividades_minor_view AS 
 SELECT a.atividade_id,
    aa.aluno_id,
    aa.composicao_aluno_id,
    a.gradecurricular_id,
    a.data_atividade,
    a.titulo,
    a.descricao,
    a.objetivo,
    a.avaliacao,
    a.orientacoes,
    a.tipoatividade_id,
    a.nota,
    a.conceito,
    a.entrega,
    a.status,
    a.concluido,
    a.urlvideoatividade,
    at.transcricao
   FROM escola.atividades a
     JOIN escola.alunos_atividades aa ON aa.atividade_id = a.atividade_id
     LEFT JOIN escola.avaliacao_aluno_atividade at ON at.aluno_id = aa.aluno_id AND at.composicao_aluno_id = aa.composicao_aluno_id AND at.atividade_id = aa.atividade_id;