CREATE OR REPLACE VIEW escola.periodos_letivos_ativos_view AS
	SELECT p.periodo_letivo_id,
		   p.descricao,
		   p.ano,
		   p.empresa_id, 
		   p.status FROM escola.periodos_letivos p
		   			WHERE p.status = true;
		   			
CREATE OR REPLACE VIEW escola.periodos_letivos_inativos_view AS
	SELECT p.periodo_letivo_id,
		   p.descricao, 
		   p.ano, 
		   p.empresa_id, 
		   p.status FROM escola.periodos_letivos p
		   			WHERE p.status = false;
		   			