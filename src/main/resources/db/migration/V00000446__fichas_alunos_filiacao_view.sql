CREATE OR REPLACE VIEW escola.fichas_alunos_filiacao_view AS
SELECT responsavel_id,aluno_id,composicao_aluno_id,idade,tipo_id,tipo,nome,email,cpf,rg,data_nascimento,profissao,telefonepessoal,telefonecomercial FROM (
SELECT 	r.responsavel_id,
	a.colaborador_id AS aluno_id, a.composicao_id AS composicao_aluno_id,
	(SELECT t.tiporesponsavel_id FROM responsaveis tr LEFT JOIN tipos_responsaveis t ON t.tiporesponsavel_id = tr.tiporesponsavel_id WHERE tr.responsavel_id = r.responsavel_id) AS tipo_id,
	(SELECT t.descricao FROM responsaveis tr LEFT JOIN tipos_responsaveis t ON t.tiporesponsavel_id = tr.tiporesponsavel_id WHERE tr.responsavel_id = r.responsavel_id) AS tipo,
	(SELECT nome FROM colaborador WHERE colaborador_id = r.responsavel_id) AS nome,
	(SELECT email FROM colaborador WHERE colaborador_id = r.responsavel_id) AS email,
	(SELECT i.cpf FROM colaborador c INNER JOIN identificador i ON i.identificador_id = c.identificador_id WHERE c.colaborador_id = r.responsavel_id) AS cpf,
	(SELECT i.rg FROM colaborador c INNER JOIN identificador i ON i.identificador_id = c.identificador_id WHERE c.colaborador_id = r.responsavel_id) AS rg,
	cast((SELECT i.data_nascimento FROM colaborador c INNER JOIN identificador i ON i.identificador_id = c.identificador_id WHERE c.colaborador_id = r.responsavel_id) AS VARCHAR) AS data_nascimento,
	(cast(extract(year from age( (SELECT i.data_nascimento FROM colaborador c INNER JOIN identificador i ON i.identificador_id = c.identificador_id WHERE c.colaborador_id = r.responsavel_id) )) AS integer)) AS idade,
	(SELECT cl.profissao FROM colaborador c INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id WHERE c.colaborador_id = r.responsavel_id) AS profissao,
	(SELECT fp.numero FROM telefones fp WHERE fp.telefone_id = (select t2.telefone_id from telefones_colaboradores t1 inner join telefones t2 ON t2.telefone_id = t1.telefone_id inner join tipos_telefones t3 ON t3.tipo_telefone_id = t2.tipo_telefone_id where t1.colaborador_id = r.responsavel_id AND t3.tipo_telefone_id = 1 limit 1)) AS telefonepessoal,
	(SELECT fc.numero FROM telefones fc WHERE fc.telefone_id = (select t21.telefone_id from telefones_colaboradores t11 inner join telefones t21 ON t21.telefone_id = t11.telefone_id inner join tipos_telefones t31 ON t31.tipo_telefone_id = t21.tipo_telefone_id where t11.colaborador_id = r.responsavel_id AND t31.tipo_telefone_id = 2 limit 1)) AS telefonecomercial
FROM escola.alunos a
INNER JOIN responsaveis_colaboradores r ON r.colaborador_id = a.colaborador_id AND r.composicao_colaborador_id = a.composicao_id
INNER JOIN responsaveis rt ON rt.responsavel_id = r.responsavel_id
) AS responsaveis
ORDER BY tipo_id
