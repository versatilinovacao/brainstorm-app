CREATE TABLE public.telas_sistema(
	telasistema_id						SERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(100) NOT NULL,
	label								VARCHAR(40),
	descricao							VARCHAR(500)	
);

--INSERT INTO public.telassistema(nome) VALUES('colaborador'),('ususarios'),('gruposusuarios');

CREATE TABLE public.gruposecurity(
	gruposecurity_id					SERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.gruposecurity(nome) VALUES('OWNER'),('ROOT');

CREATE TABLE public.security(
	telasistemapermissao_id				SERIAL NOT NULL PRIMARY KEY,
	telasistema_id						BIGINT NOT NULL REFERENCES public.telas_sistema(telasistema_id),
	gruposecurity_id					BIGINT NOT NULL REFERENCES public.gruposecurity(gruposecurity_id),
	permissao_id						BIGINT NOT NULL REFERENCES public.permissao(permissao_id)
);
 
CREATE UNIQUE INDEX security_unique_idx ON public.security(telasistema_id,gruposecurity_id,permissao_id);  
CREATE INDEX security_permissao_idx ON public.security(telasistema_id,gruposecurity_id,permissao_id);

/*
Criar a função que a partir da tabela de public.security ele crie os vinculos de todas as permissões do usuário dentro
da tabela usuario_permissao;
*/

DELETE FROM public.usuario_permissao;
DELETE FROM public.permissao;
DELETE FROM public.acessos_usuarios;
DELETE FROM public.usuario;

SELECT setval('public.usuario_usuario_id_seq', 1, true);
SELECT setval('public.permissao_permissao_id_seq', 1, true);
SELECT setval('public.acessos_usuarios_acessousuario_id_seq', 1, true);

INSERT INTO public.usuario(usuario_id,nome,nascimento,senha,status,email) VALUES(1,'owner',null,'$2a$10$zP53hD3PIrYLcP2b6GboMOVWP0cKYVZeo4IrZ4H8y9IDc5ddCxrrm',true,'owner@insideincloud.com.br');

INSERT INTO public.telas_sistema(nome,label,descricao) VALUES('colaborador','Colaborador','Cadastro de Colaboradores');
INSERT INTO public.telas_sistema(nome,label,descricao) VALUES('usuario','Usuarios','Cadastro de Usuários');
INSERT INTO public.telas_sistema(nome,label,descricao) VALUES('gruposecurity','Grupo de Usuário','Cadastro de Grupo de Usuários');
INSERT INTO public.telas_sistema(nome,label,descricao) VALUES('telasistema','Telas do Sistema','Cadastro de Telas do Sistema');
INSERT INTO public.telas_sistema(nome,label,descricao) VALUES('permissoes','Permissões','Cadastro de Permissões');

INSERT INTO permissao(nome) VALUES('ROLE_COLABORADOR_CONTEUDO'),('ROLE_COLABORADOR_SALVAR'),('ROLE_COLABORADOR_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_USUARIO_CONTEUDO'),('ROLE_USUARIO_SALVAR'),('ROLE_USUARIO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_TELASISTEMA_CONTEUDO'),('ROLE_TELASISTEMA_SALVAR'),('ROLE_TELASISTEMA_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_GRUPOSECURITY_CONTEUDO'),('ROLE_GRUPOSECURITY_SALVAR'),('ROLE_GRUPOSECURITY_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_PERMISSAO_CONTEUDO'),('ROLE_PERMISSAO_SALVAR'),('ROLE_PERMISSAO_DELETAR');
INSERT INTO permissao(nome) VALUES('ROLE_INICIALIZAPERMISSAO_EXECUTAR'); --Permite executar a inicialização da criação de novas permissões para cada tela nova;

INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'COLABORADOR'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_COLABORADOR_CONTEUDO'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'COLABORADOR'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_COLABORADOR_SALVAR'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'COLABORADOR'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_COLABORADOR_DELETAR'));

INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'USUARIO'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_USUARIO_CONTEUDO'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'USUARIO'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_USUARIO_SALVAR'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'USUARIO'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_USUARIO_DELETAR'));

INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'GRUPOSECURITY'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_GRUPOSECURITY_CONTEUDO'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'GRUPOSECURITY'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_GRUPOSECURITY_SALVAR'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'GRUPOSECURITY'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_GRUPOSECURITY_DELETAR'));

INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'TELASISTEMA'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_TELASISTEMA_CONTEUDO'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'TELASISTEMA'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_TELASISTEMA_SALVAR'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'TELASISTEMA'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_TELASISTEMA_DELETAR'));

INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'PERMISSOES'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_PERMISSAO_CONTEUDO'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'PERMISSOES'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_PERMISSAO_SALVAR'));
INSERT INTO public.security(telasistema_id,gruposecurity_id,permissao_id) VALUES((select telasistema_id from public.telas_sistema where UPPER(nome) = 'PERMISSOES'),(select gruposecurity_id from public.gruposecurity where upper(nome) = 'OWNER'),(select permissao_id from public.permissao where upper(nome) = 'ROLE_PERMISSAO_DELETAR'));

INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_COLABORADOR_CONTEUDO'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_COLABORADOR_SALVAR'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_COLABORADOR_DELETAR'));

INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_USUARIO_CONTEUDO'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_USUARIO_SALVAR'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_USUARIO_DELETAR'));

INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_TELASISTEMA_CONTEUDO'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_TELASISTEMA_SALVAR'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_TELASISTEMA_DELETAR'));

INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_PERMISSAO_CONTEUDO'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_PERMISSAO_SALVAR'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_PERMISSAO_DELETAR'));

INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_GRUPOSECURITY_CONTEUDO'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_GRUPOSECURITY_SALVAR'));
INSERT INTO public.usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM public.permissao WHERE nome = 'ROLE_GRUPOSECURITY_DELETAR'));

