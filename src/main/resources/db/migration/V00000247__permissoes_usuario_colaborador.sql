INSERT INTO permissao(nome,label,descricao) VALUES('ROLE_LOGRADOURO_CONTEUDO','','CONTEUDO');
INSERT INTO permissao(nome,label,descricao) VALUES('ROLE_LOGRADOURO_SALVAR','','SALVAR');
INSERT INTO permissao(nome,label,descricao) VALUES('ROLE_LOGRADOURO_DELETAR','','DELETAR');

INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_LOGRADOURO_CONTEUDO'));
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_LOGRADOURO_SALVAR'));
INSERT INTO usuario_permissao(usuario_id,permissao_id) VALUES(1,(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_LOGRADOURO_DELETAR'));