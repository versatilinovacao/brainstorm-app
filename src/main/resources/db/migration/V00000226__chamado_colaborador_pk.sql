ALTER TABLE public.chamados ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.chamados 	
  ADD CONSTRAINT colaborador_fk FOREIGN KEY (colaborador_id,composicao_colaborador_id)
      REFERENCES colaborador(colaborador_id, composicao_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_colaborador_fk
  ON public.chamados
  USING btree(colaborador_id,composicao_colaborador_id);
  
ALTER TABLE public.chamados ADD COLUMN composicao_empresa_id BIGINT;
ALTER TABLE public.chamados
  ADD CONSTRAINT colaborador_empresa_fk FOREIGN KEY(colaborador_id,composicao_empresa_id)
      REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
CREATE INDEX fki_empresa_fk
  ON public.chamados
  USING btree(colaborador_id,composicao_empresa_id);  
