CREATE TABLE public.competencia_mensal(
	competencia_mensal_id		SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE,
	mes							SMALLINT
);

CREATE TABLE public.periodo_agrupado( --Trimestre / Semestre / etc...
	periodo_agrupado_id		SERIAL NOT NULL PRIMARY KEY,
	descricao				VARCHAR(100) NOT NULL
);

INSERT INTO public.competencia_mensal(descricao,mes) VALUES('JANEIRO',1),('FEVEREIRO',2),('MARÇO',3),('ABRIL',4),('MAIO',5),('JUNHO',6),
   							   ('JULHO',7),('AGOSTO',8),('SETEMBRO',9),('OUTUBRO',10),('NOVEMBRO',11),('DEZEMBRO',12);

CREATE TABLE public.cadernos(
	caderno_id				SERIAL NOT NULL PRIMARY KEY,
	descricao				VARCHAR(100) NOT NULL,
	periodo_letivo_id		BIGINT NOT NULL REFERENCES public.periodos_letivo(periodo_letivo_id),
	grademateria_id			BIGINT NOT NULL REFERENCES public.gradematerias(grademateria_id),
	periodo_inicial			DATE,
	periodo_final			DATE,
	status					BOOLEAN --Aberto, Fechado...
);

CREATE TABLE public.chamadas(
	chamada_id				SERIAL NOT NULL PRIMARY KEY,
	caderno_id				BIGINT NOT NULL REFERENCES public.cadernos(caderno_id),
	competencia_mensal_id	BIGINT NOT NULL REFERENCES public.competencia_mensal(competencia_mensal_id),
	periodo_agrupado		VARCHAR(30),
	periodo					VARCHAR(4)
);

CREATE TABLE public.chamadas_aluno(
	chamada_aluno_id		SERIAL NOT NULL PRIMARY KEY,
	chamada_id				BIGINT NOT NULL REFERENCES public.chamadas(chamada_id),
	colaborador_id			BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	avaliacao				TEXT
);

CREATE TABLE public.registro_chamadas(
	registro_chamada_id		SERIAL NOT NULL PRIMARY KEY,
	chamada_aluno_id		BIGINT NOT NULL REFERENCES public.chamadas_aluno(chamada_aluno_id),
	evento					TIMESTAMP,
	nota					SMALLINT,
	conceito				VARCHAR(1),
	avaliacao				TEXT,
	status					BOOLEAN
);

CREATE OR REPLACE FUNCTION abrir_caderno() RETURNS trigger AS $$
DECLARE
	periodo				public.periodos_letivo_itens%ROWTYPE;
	alunos				public.colaborador%ROWTYPE;
	g_turma_id			BIGINT;
	chamada_inicio			SMALLINT;
	chamada_fim			SMALLINT;
	registro			TIMESTAMP%TYPE;
	chamada_codigo			BIGINT;
	chamada_aluno_codigo		BIGINT;
BEGIN 
	FOR periodo IN
		SELECT * FROM public.periodos_letivo_itens WHERE periodo_letivo_id = NEW.periodo_letivo_id
	LOOP
		SELECT inicio INTO chamada_inicio FROM Extract('Month' FROM periodo.periodo_inicial) AS inicio;
		SELECT fim    INTO chamada_fim	  FROM Extract('Month' FROM periodo.periodo_final) AS fim;

		WHILE chamada_inicio <= chamada_fim LOOP
			INSERT INTO public.chamadas(caderno_id,competencia_mensal_id,periodo_agrupado,periodo) 
				VALUES(NEW.caderno_id,
				       chamada_inicio,
				       periodo.descricao,
				       (SELECT ano FROM Extract('MONTH' FROM periodo.periodo_inicial) AS ano)
				      ) RETURNING chamada_id INTO chamada_codigo;

			SELECT turma_id INTO g_turma_id FROM public.gradematerias gm WHERE gm.grademateria_id = NEW.gradegrademateria_id; 

			FOR alunos IN
				SELECT colaborador_id FROM alunos_turmas WHERE turma_id = g_turma_id
			LOOP 
				INSERT INTO public.chamadas_aluno(chamada_id,colaborador_id) 
					VALUES( chamada_codigo,alunos.colaborador_id) RETURNING chamada_aluno_id INTO chamada_aluno_codigo; --(SELECT MAX(chamada_id) FROM public.chamadas LIMIT 1)				
				FOR registro IN 
					SELECT data FROM generate_series(periodo.periodo_inicial,periodo.periodo_final,INTERVAL'1 day') AS data
				LOOP 
					INSERT INTO public.registro_chamadas(chamada_aluno_id,evento)
						VALUES(chamada_aluno_codigo,registro);
				END LOOP;		
			END LOOP; 
			
			chamada_inicio := chamada_inicio + 1;
		END LOOP;
		
	END LOOP;
		
	RETURN NEW;
EXCEPTION WHEN OTHERS THEN
	RAISE NOTICE 'ESTA ACONTECENDO UM ERRO AQUI... % ...';

END; $$
LANGUAGE plpgsql;

CREATE TRIGGER abrir_caderno_trigger 
	AFTER INSERT 
		ON public.cadernos FOR EACH ROW
			EXECUTE PROCEDURE abrir_caderno();


/*
SELECT * FROM DATE_TRUNC('month',current_date)

SELECT *
 FROM generate_series(DATE'2012-12-01',DATE'2012-12-31',INTERVAL'1 day');
    generate_series
    */
