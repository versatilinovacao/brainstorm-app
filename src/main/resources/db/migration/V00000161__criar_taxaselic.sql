CREATE SCHEMA tributacao;

CREATE TABLE tributacao.taxasselic(
    taxaselic_id                    SERIAL NOT NULL PRIMARY KEY,
    movimento                       DATE,
    taxa                            NUMERIC(16,4),
    fator                           NUMERIC(16,4),
    financeiro                      NUMERIC(16,4),
    operacoes                       INTEGER,
    media                           NUMERIC(16,4),
    mediana                         NUMERIC(16,4),
    moda                            NUMERIC(16,4),
    desvio                          NUMERIC(16,4),
    curtose                         NUMERIC(16,4)
);

