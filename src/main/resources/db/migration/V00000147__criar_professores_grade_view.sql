CREATE VIEW escola.professores_grade_view AS
SELECT colaborador_id AS professore_id, nome FROM public.colaborador 
	WHERE COALESCE(parceiro_id,0) > 0 AND status = true;
