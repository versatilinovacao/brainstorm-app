/*
	Se a empresa estiver como contratante ela paga
	Se a empresa estiver como contatada ela recebe
*/

CREATE SCHEMA administracao;

CREATE TABLE administracao.parcelamento(
	parcelamento_id						BIGSERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(100) NOT NULL UNIQUE,
	fator								INTEGER
);

INSERT INTO administracao.parcelamento(descricao,fator) VALUES('ANUAL',1),('SEMESTRAL',2),('TRIMESTRAL',4),('MENSAL',12),('SEMANAL',52);

CREATE TABLE administracao.pagamentos(
	pagamento_id						BIGSERIAL NOT NULL PRIMARY KEY,
	dia									INTEGER
);

INSERT INTO administracao.pagamentos(dia)	VALUES(5),(10),(15),(20),(25);

CREATE TABLE administracao.contratos(
	contrato_id							BIGSERIAL NOT NULL PRIMARY KEY,
	empresa_id							BIGINT,
	composicao_empresa_id				BIGINT,
	contratante_id						BIGINT,
	composicao_contratante_id			BIGINT,
	contratado_id						BIGINT,
	composicao_contratado_id			BIGINT,
	parcelamento_id						BIGINT NOT NULL REFERENCES administracao.parcelamento(parcelamento_id),
	pagamento_id						BIGINT NOT NULL REFERENCES administracao.pagamentos(pagamento_id),
	assinatura							TIMESTAMP,
	renovado_automaticamente			BOOLEAN,
	validade							INTEGER,
	total								NUMERIC(16,4)
	
);

ALTER TABLE administracao.contratos		ADD CONSTRAINT empresa_fk		FOREIGN KEY(empresa_id,composicao_empresa_id)			REFERENCES public.colaborador(colaborador_id,composicao_id);
ALTER TABLE administracao.contratos		ADD CONSTRAINT contratante_fk	FOREIGN KEY(contratante_id,composicao_contratante_id)	REFERENCES public.colaborador(colaborador_id,composicao_id);
ALTER TABLE administracao.contratos		ADD CONSTRAINT contratado_fk	FOREIGN KEY(contratado_id,composicao_contratado_id)		REFERENCES public.colaborador(colaborador_id,composicao_id);

CREATE INDEX ON administracao.contratos(contrato_id,empresa_id,composicao_empresa_id);
CREATE INDEX ON administracao.contratos(empresa_id,composicao_empresa_id);
CREATE INDEX ON administracao.contratos(contratante_id,composicao_contratante_id);
CREATE INDEX ON administracao.contratos(contratado_id,composicao_contratado_id);
CREATE INDEX ON administracao.contratos(parcelamento_id);

CREATE TABLE administracao.testemunhas(
	testemunha_id						BIGSERIAL NOT NULL,
	contrato_id							BIGINT,
	colaborador_id						BIGINT,
	composicao_colaborador_id			BIGINT
);

ALTER TABLE administracao.testemunhas		ADD CONSTRAINT testemunha_pk	PRIMARY KEY(testemunha_id,contrato_id);
ALTER TABLE administracao.testemunhas		ADD CONSTRAINT colaborador_fk	FOREIGN KEY(colaborador_id,composicao_colaborador_id)	REFERENCES public.colaborador(colaborador_id,composicao_id);
ALTER TABLE administracao.testemunhas		ADD CONSTRAINT contrato_fk		FOREIGN KEY(contrato_id)								REFERENCES administracao.contratos(contrato_id);

CREATE TABLE administracao.objetos(
	objeto_id							BIGSERIAL NOT NULL PRIMARY KEY,
	contrato_id							BIGINT NOT NULL,
	artefato_id							BIGINT NOT NULL,
	descricao							VARCHAR(100) NOT NULL,
	aliquota							NUMERIC(16,4),
	valor								NUMERIC(16,4)
);

CREATE INDEX ON administracao.objetos(contrato_id);
CREATE INDEX ON administracao.objetos(artefato_id);
CREATE INDEX ON administracao.objetos(contrato_id,artefato_id);
CREATE INDEX ON administracao.objetos(descricao);

