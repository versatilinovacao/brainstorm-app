CREATE TABLE financeiro.tipos_pagamentos(
	tipopagamento_id			BIGSERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(40) NOT NULL UNIQUE
);

INSERT INTO financeiro.tipos_pagamentos(descricao)	VALUES('A VISTA'),('A PRAZO');

DELETE FROM financeiro.negociacoes;
DELETE FROM financeiro.formas_pagamentos;
INSERT INTO financeiro.negociacoes(descricao) VALUES('Dinheiro'),('Cartão'),('Boleto'),('Promissória'),('Cheque'),('Transferência'),('Vale Alimentação'),('Vale Refeição'),('Promessa');
INSERT INTO financeiro.formas_pagamentos(descricao) VALUES('Dinheiro'),('Débito'),('Crédito'),('Boleto'),('Transferência'),('Cheque');

