CREATE TABLE escola.cadernos_inconsistencias(
	caderno_inconsistencia_id		SERIAL NOT NULL PRIMARY KEY,
	caderno_id						BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	resumo							VARCHAR(40) NOT NULL,
	descricao						VARCHAR(200) NOT NULL,
	status							BOOLEAN DEFAULT FALSE
);


CREATE VIEW escola.is_cadernos_inconsistencias_view AS 
	SELECT caderno_id,
	       (SELECT (count(1) > 0) AS isErro FROM escola.cadernos_inconsistencias b WHERE b.caderno_id = a.caderno_id and status = false) AS isErro
	FROM escola.cadernos_inconsistencias a group by caderno_id;
