CREATE INDEX idx_avaliacoes_cadernoid ON escola.avaliacoes USING hash(caderno_id);
CREATE INDEX idx_avaliacoes ON escola.avaliacoes USING btree(caderno_id,aluno_id,periodo_letivo_item_id);
CREATE INDEX idx_chamadas_cadenoid ON escola.chamadas USING hash(caderno_id);
CREATE INDEX idx_chamadas ON escola.chamadas USING btree(caderno_id,evento,aluno_id,chamada_id);

CREATE TABLE action.cancelar_caderno(
	caderno_id			BIGINT NOT NULL PRIMARY KEY
);

CREATE OR REPLACE FUNCTION action.cancelar_caderno() RETURNS trigger AS $$
BEGIN
	DELETE FROM escola.cadernos_inconsistencias WHERE caderno_id = NEW.caderno_id;
	DELETE FROM escola.avaliacoes WHERE caderno_id = NEW.caderno_id;
	DELETE FROM escola.chamadas WHERE caderno_id = NEW.caderno_id;
	DELETE FROM escola.cadernos WHERE caderno_id = NEW.caderno_id;
	RETURN NEW;
	
END; $$ 
	LANGUAGE plpgsql;

CREATE TRIGGER cancelar_cadernos_tgi 
	AFTER INSERT 
		ON action.cancelar_caderno
		FOR EACH ROW
		EXECUTE PROCEDURE action.cancelar_caderno();

-- Function: action.deletar_caderno_execute()

-- DROP FUNCTION action.deletar_caderno_execute();

CREATE OR REPLACE FUNCTION action.deletar_caderno_execute()
  RETURNS trigger AS
$$
DECLARE 
	isDelete			BOOLEAN;
	isCritica			BOOLEAN;
BEGIN
--	IF NEW.deletar THEN
--		UPDATE action.grupo_cadernos_delete SET deletar = NEW.deletar WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
--	END IF;
	
--	SELECT deletar INTO isDelete FROM action.grupo_cadernos_delete WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
--	IF isDelete THEN
		SELECT COUNT(1) > 0 INTO isCritica FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
	
		IF NOT isCritica THEN
			DELETE FROM escola.cadernos 	WHERE caderno_id = NEW.caderno_id;
		ELSE
			INSERT INTO msg.cadernos(caderno_id,mensagem,ocorrencia) 
				SELECT caderno_id, mensagem, ocorrencia FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
		END IF;
		
		--DELETE FROM action.deletar_cadernos WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
--	END IF; 

	RETURN NEW;
END;$$
  LANGUAGE plpgsql VOLATILE;
