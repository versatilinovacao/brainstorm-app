CREATE TABLE public.tipo_colaborador(
	tipo_colaborador_id				SERIAL NOT NULL,
	descricao						VARCHAR(100)
);

ALTER TABLE public.tipo_colaborador ADD CONSTRAINT tipo_colaborador_pk PRIMARY KEY(tipo_colaborador_id);

INSERT INTO public.tipo_colaborador(descricao) VALUES('EMPRESA'),('CLIENTE'),('TERCEIRO'),('PARCEIRO');


CREATE TABLE public.colaborador(
	colaborador_id					SERIAL NOT NULL,
	composicao_id					BIGINT,
	empresa_id						BIGINT,
	nome							VARCHAR(100) UNIQUE,
	fantasia						VARCHAR(60),
	usuario_id						BIGINT,
	referencia						VARCHAR(10),
	cep								VARCHAR(8),
	identificador_id				BIGINT,
	pessoajuridica_id				BIGINT,
	responsavel_id					BIGINT,
	telefone_id						BIGINT,
	cargo_id						BIGINT,
	especializacao_id				BIGINT,
	numero							VARCHAR(15),
	complemento						VARCHAR(15),
	aluno							BOOLEAN,
	responsavel						BOOLEAN,	
	tipo_colaborador				VARCHAR(1) -- 1. Empresa 2. Cliente 3. Terceiro 4. Parceiro
);

ALTER TABLE public.colaborador ADD CONSTRAINT colaborador_pk PRIMARY KEY(colaborador_id);
ALTER TABLE public.colaborador ADD CONSTRAINT colaborador_composicao_fk FOREIGN KEY(composicao_id) REFERENCES public.colaborador(colaborador_id);
ALTER TABLE public.colaborador ADD CONSTRAINT colaborador_empresa_fk FOREIGN KEY(empresa_id) REFERENCES public.colaborador(colaborador_id);

CREATE TABLE public.identificador(
	identificador_id				SERIAL NOT NULL,
	cpf								VARCHAR(11),
	rg								VARCHAR(20),
	data_nascimento					DATE,
	certidao						VARCHAR(33),
	registro_profissional			VARCHAR(30), 	
	registro						DATE, 			
	habilitacao						VARCHAR(20), 	
	habilitacao_modelo				VARCHAR(5), 	
	habilitacao_registro			DATE, 			
	habilitacao_vencimento			DATE, 			
	titulo_numero					VARCHAR(20),	
	titulo_zona						VARCHAR(5),
	titulo_secao					VARCHAR(5),
	certificado_reservista			VARCHAR(20),
	assinatura						BYTEA, 			
	foto							BYTEA,
	polegar							BYTEA,
	indicador						BYTEA,
	medio							BYTEA,
	anelar							BYTEA,
	minguinho						BYTEA,
	idade							BIGINT		
);

ALTER TABLE public.identificador ADD CONSTRAINT identificador_pk PRIMARY KEY(identificador_id);
ALTER TABLE public.colaborador ADD CONSTRAINT identificador_fk FOREIGN KEY(identificador_id) REFERENCES public.identificador(identificador_id);


