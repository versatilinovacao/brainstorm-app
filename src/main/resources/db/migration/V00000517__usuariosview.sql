DROP VIEW usuariosview;

CREATE OR REPLACE VIEW usuariosview AS 
 SELECT u.usuario_id,
    ( SELECT c_1.colaborador_id AS empresa_id
           FROM colaborador c_1
          WHERE COALESCE(c_1.empresa_id, 0::bigint) > 0) AS empresa_id,
    ( SELECT c_1.composicao_id AS composicao_empresa_id
           FROM colaborador c_1
          WHERE COALESCE(c_1.empresa_id, 0::bigint) > 0) AS composicao_empresa_id,
    u.nome,
    i.data_nascimento AS nascimento,
    u.senha,
    u.status,
    u.email,
    u.isserver,
    u.isclient,
    u.iscounter,
    u.issistema,
    i.foto_thumbnail,
    c.cargo_id,
    ca.descricao AS descricao_cargo,
    c.colaborador_id,
    c.composicao_id,
    (SELECT COUNT(1) > 0 FROM parceiros WHERE parceiro_id = c.parceiro_id AND cargo_id IN (511,512,514,517,617,820,1018)) as isprofessor
   FROM usuario u
     LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
     LEFT JOIN cargos ca ON ca.cargo_id = c.cargo_id
  WHERE u.status = true;
  