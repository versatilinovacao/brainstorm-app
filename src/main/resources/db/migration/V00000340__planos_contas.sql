ALTER TABLE financeiro.contas_receber DROP CONSTRAINT planoconta_fk;
ALTER TABLE financeiro.contas_pagar DROP CONSTRAINT planoconta_fk;

ALTER TABLE contabil.plano_contas 	DROP CONSTRAINT plano_contas_pkey CASCADE;
ALTER TABLE contabil.plano_contas 	ALTER COLUMN composicao_id DROP NOT NULL;
ALTER TABLE contabil.plano_contas	ADD CONSTRAINT plano_contas_pkey PRIMARY KEY(planoconta_id);

--ALTER TABLE financeiro.contas_receber	DROP COLUMN composicao_planoconta_id;
ALTER TABLE financeiro.contas_receber	ADD COLUMN composicao_planoconta_id		BIGINT;
ALTER TABLE financeiro.contas_receber
  ADD CONSTRAINT planoconta_fk FOREIGN KEY (planoconta_id)
      REFERENCES contabil.plano_contas (planoconta_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

--ALTER TABLE financeiro.contas_pagar	DROP COLUMN composicao_planoconta_id;
ALTER TABLE financeiro.contas_pagar	ADD COLUMN composicao_planoconta_id		BIGINT;
ALTER TABLE financeiro.contas_pagar
  ADD CONSTRAINT planoconta_fk FOREIGN KEY (planoconta_id)
      REFERENCES contabil.plano_contas (planoconta_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE contabil.plano_contas	ADD CONSTRAINT recursiva_fk	FOREIGN KEY(composicao_id) REFERENCES contabil.plano_contas(planoconta_id);

ALTER TABLE contabil.plano_contas	ADD COLUMN ordenacao	VARCHAR(1);
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel1		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel2		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel3		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel4		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel5		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel6		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel7		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel8		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel9		INTEGER;
ALTER TABLE contabil.plano_contas	ADD COLUMN nivel10		INTEGER;

--DROP VIEW contabil.planos_contas_arvores_view;
CREATE OR REPLACE VIEW contabil.planos_contas_arvores_view AS
SELECT cod AS codigo,descricao,planoconta_id,composicao_id,status FROM (
	WITH RECURSIVE arvore AS (
		SELECT  CAST(nivel AS VARCHAR) AS cod, planoconta_id,composicao_id,descricao,nivel,codigo,numeral,status,CAST(ordenacao AS TEXT) AS ordenacao 
			FROM contabil.plano_contas WHERE composicao_id IS null
		UNION ALL
		SELECT	(CASE
			   WHEN (not p.nivel1 IS null) AND (p.nivel2 IS null) AND (p.nivel3 IS null) AND (p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) 
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (p.nivel3 IS null) AND (p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (p.nivel4 IS null) AND (nivel5 IS null) AND (nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (nivel7 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (nivel8 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (not nivel8 IS null) AND (nivel9 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR) || '.' || CAST(p.nivel9 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (not nivel8 IS null) AND (not nivel9 IS null) AND (nivel10 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR) || '.' || CAST(p.nivel9 AS VARCHAR) || '.' || CAST(p.nivel10 AS VARCHAR)
			END) AS cod,p.planoconta_id,p.composicao_id,p.descricao,p.nivel,p.codigo,p.numeral,p.status,CAST(a.ordenacao || ' > ' || p.ordenacao AS TEXT) AS ordenacao 
			FROM contabil.plano_contas p
		INNER JOIN arvore a ON p.composicao_id = a.planoconta_id
		--WHERE p.nivel = 2
	) SELECT cod,planoconta_id,composicao_id,descricao,nivel,codigo,numeral,status,arvore.ordenacao FROM arvore order by arvore.ordenacao,arvore.codigo

) AS planos_contas_arvore 
ORDER BY codigo;

--DROP VIEW contabil.planos_contas_arvores_ativas_view
CREATE OR REPLACE VIEW contabil.planos_contas_arvores_ativas_view AS
SELECT * FROM contabil.planos_contas_arvores_view
WHERE COALESCE(status,true) = true ;

--DROP VIEW contabil.planos_contas_arvores_inativas_view
CREATE OR REPLACE VIEW contabil.planos_contas_arvores_inativas_view AS
SELECT * FROM contabil.planos_contas_arvores_view
WHERE COALESCE(status,true) = false;

--select * from contabil.planos_contas_arvores_ativas_view
--Inicializar Plano Conta 
--select * from contabil.plano_contas
DELETE FROM contabil.plano_contas;
INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel) VALUES(1,null,1,1,'COMERCIO',true,'A',1);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(2,1,1,1,'ATIVO',true,'A',1,1);
		INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1,nivel2) VALUES(3,2,1,1,'Ativo Circulante',true,'A',1,1,1);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(4,1,2,1,'PASSIVO',true,'A',1,2);
		INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1,nivel2) VALUES(5,4,1,1,'Passivo Cirtulante',true,'A',1,2,1);
			INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1,nivel2,nivel3) VALUES(6,5,1,1,'Fornecedores',true,'A',1,2,1,1);
			INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1,nivel2,nivel3) VALUES(7,5,2,1,'Contas a Pagar',true,'A',1,2,1,2);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(8,1,2,1,'DESPESA',true,'A',1,3);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(9,1,2,1,'RECEITA',true,'A',1,4);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(10,1,2,1,'RESULTADO',true,'A',1,5);
INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel) VALUES(11,null,2,1,'INDUSTRIA',true,'B',2);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES(13,11,1,1,'ATIVO',true,'A',2,1); 
		INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1,nivel2) VALUES((select max(planoconta_id)+1 from contabil.plano_contas),11,1,1,'Ativo Circulante',true,'A',2,1,1);
	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel,nivel1) VALUES((select max(planoconta_id)+1 from contabil.plano_contas),11,2,1,'PASSIVO',true,'A',2,2);


	INSERT INTO contabil.plano_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status,ordenacao,nivel) VALUES(12,null,2,1,'SERVIÇO',true,'C',3);

