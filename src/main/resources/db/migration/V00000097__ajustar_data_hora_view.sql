CREATE SCHEMA biblioteca;

DROP VIEW data_hora_view;

CREATE VIEW biblioteca.data_hora_view AS
	SELECT 'SP' AS indice,now() AT TIME ZONE 'America/Sao_Paulo'
		UNION ALL
	SELECT 'MST' AS indice,now() AT TIME ZONE 'MST'
		UNION ALL
	SELECT 'UTC' as indice, now() AT TIME ZONE 'UTC';
