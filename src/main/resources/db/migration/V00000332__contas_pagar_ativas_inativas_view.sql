CREATE OR REPLACE VIEW financeiro.contas_pagar_ativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contapagar_id,
	    c.composicao_id,
        c.total,
        c.credor_id,
        c.composicao_credor_id,
	    r.nome AS nomecredor,
        c.parcela,
        c.status
   FROM financeiro.contas_pagar c
INNER JOIN public.colaborador r ON r.colaborador_id = c.credor_id AND r.composicao_id = c.composicao_credor_id
  WHERE COALESCE(c.status, true) = true;

CREATE OR REPLACE VIEW financeiro.contas_pagar_inativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contapagar_id,
	    c.composicao_id,
        c.total,
        c.credor_id,
        c.composicao_credor_id,
	    r.nome AS nomecredor,
        c.parcela,
        c.status
   FROM financeiro.contas_pagar c
INNER JOIN public.colaborador r ON r.colaborador_id = c.credor_id AND r.composicao_id = c.composicao_credor_id
  WHERE COALESCE(c.status, true) = false;  

