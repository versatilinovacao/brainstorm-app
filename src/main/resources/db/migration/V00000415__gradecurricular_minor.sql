DROP VIEW escola.gradecurricular_minor;

CREATE OR REPLACE VIEW escola.gradecurricular_minor AS 
 SELECT g.gradecurricular_id,
        g.turma_id,
        t.descricao AS nometurma,
        g.professor_id,
	c.nome AS nomeprofessor,
        g.materia_id,
        m.descricao AS nomemateria,
        g.curso_id,
        g.cursoperiodo_id,
        g.composicao_professor_id,
        g.status
   FROM escola.gradecurricular g
   INNER JOIN turmas t ON t.turma_id = g.turma_id
   INNER JOIN materias m ON m.materia_id = g.materia_id
   INNER JOIN colaborador c ON c.colaborador_id = g.professor_id AND c.composicao_id = g.composicao_professor_id
WHERE COALESCE(g.status,true) = true;
