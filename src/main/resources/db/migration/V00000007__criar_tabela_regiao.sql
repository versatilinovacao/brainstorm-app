CREATE TABLE regiao(
	regiao_id				SERIAL NOT NULL,
	descricao				VARCHAR(100)
);

ALTER TABLE regiao ADD CONSTRAINT regiao_pk PRIMARY KEY(regiao_id);