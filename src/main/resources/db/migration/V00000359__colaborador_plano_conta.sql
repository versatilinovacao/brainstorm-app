ALTER TABLE colaborador		ADD COLUMN	planoconta_id			BIGINT;
ALTER TABLE colaborador		ADD CONSTRAINT planoconta_fk	FOREIGN KEY(planoconta_id)		REFERENCES contabil.planos_contas(planoconta_id);
CREATE INDEX planoconta_idx	ON colaborador(planoconta_id);