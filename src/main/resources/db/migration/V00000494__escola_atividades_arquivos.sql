CREATE TABLE escola.atividades_arquivos(
	atividadearquivo_id								BIGSERIAL NOT NULL PRIMARY KEY,
	atividade_id									BIGINT NOT NULL REFERENCES escola.atividades(atividade_id),
	arquivomp4_id									BIGINT NOT NULL REFERENCES arquivos.mp4(arquivo_id)
);

CREATE INDEX atividades_arquivos_atividade_idx		ON escola.atividades_arquivos		USING btree(atividade_id);