CREATE TABLE administracao.parcelas_por_contratos(
	contrato_id							BIGINT NOT NULL REFERENCES administracao.contratos(contrato_id),
	contareceber_id						BIGINT NOT NULL REFERENCES financeiro.contas_receber(contareceber_id),
	PRIMARY KEY(contrato_id,contareceber_id)
);

CREATE INDEX parcelas_contratos_idx	ON administracao.parcelas_por_contratos	USING btree(contrato_id,contareceber_id);

