 DROP VIEW responsaveis_view;
 CREATE OR REPLACE VIEW responsaveis_view AS 
 SELECT r.responsavel_id,
    ( SELECT c1.nome
           FROM colaborador c1
          WHERE c1.colaborador_id = rc.responsavel_id) AS nome,
    (SELECT t.numero FROM telefones t WHERE t.telefone_id = c.telefone_id) AS telefone,
    rc.colaborador_id AS vinculo_id
   FROM responsaveis r
     JOIN responsaveis_colaboradores rc ON rc.responsavel_id = r.responsavel_id
     JOIN colaborador c ON c.colaborador_id = rc.colaborador_id;
 