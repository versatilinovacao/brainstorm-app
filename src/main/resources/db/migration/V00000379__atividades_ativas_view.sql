DROP VIEW escola.atividades_ativas_view;

CREATE OR REPLACE VIEW escola.atividades_ativas_view AS 
 SELECT a.atividade_id,
    a.gradecurricular_id,
    a.data_atividade,
    a.titulo,
    a.descricao,
    a.objetivo,
    a.orientacoes,
    a.avaliacao,
    a.tipoatividade_id,
    t.descricao AS descricao_atividade,
    a.nota,
    a.conceito,
    a.entrega,
    a.status,
    g.materia_id,
    g.professor_id,
    g.turma_id,
    tu.descricao AS descricaoturma,
    m.descricao AS descricaomateria,
    c.composicao_id AS composicao_professor_id
   FROM escola.atividades a
     LEFT JOIN escola.tipos_atividades t ON t.tipoatividade_id = a.tipoatividade_id
     LEFT JOIN escola.gradecurricular g ON g.gradecurricular_id = a.gradecurricular_id
     LEFT JOIN turmas tu ON tu.turma_id = g.turma_id
     LEFT JOIN materias m ON m.materia_id = g.materia_id 
     LEFT JOIN colaborador c ON c.colaborador_id = g.professor_id AND c.composicao_id = g.composicao_professor_id
  WHERE COALESCE(a.status, true) = true;
