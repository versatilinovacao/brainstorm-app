CREATE TABLE public.confirmacadastros(
	confirmacadastro_id						BIGINT NOT NULL PRIMARY KEY,
	codigo									VARCHAR(200) NOT NULL UNIQUE,
	registro								DATE,
	ativo									TIME,
	validade								INTEGER
);