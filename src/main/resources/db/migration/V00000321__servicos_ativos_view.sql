DROP VIEW contabil.servicos_ativos_view;
DROP VIEW contabil.servicos_inativos_view;

CREATE OR REPLACE VIEW contabil.servicos_ativos_view AS 
SELECT c.servico_id,
       c.referencia,
       c.descricao,
       c.aliquota,
       c.municipio,
       c.status
FROM servico.cachoeirinha c
WHERE COALESCE(c.status,true) = true
UNION ALL
SELECT p.servico_id,
       p.referencia,
       p.descricao,
       p.aliquota,
       p.municipio,
       p.status
FROM servico.portoalegre p
WHERE COALESCE(p.status,true) = true;

CREATE OR REPLACE VIEW contabil.servicos_inativos_view AS 
SELECT c.servico_id,
       c.referencia,
       c.descricao,
       c.aliquota,
       c.municipio,
       c.status
FROM servico.cachoeirinha c
WHERE COALESCE(c.status,true) = false
UNION ALL
SELECT p.servico_id,
       p.referencia,
       p.descricao,
       p.aliquota,
       p.municipio,
       p.status
FROM servico.portoalegre p
WHERE COALESCE(p.status,true) = false;
