CREATE TABLE escola.tipos_atividades(
	tipoatividade_id			SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO escola.tipos_atividades(descricao) VALUES('CONTEUDO'),('TRABALHO'),('PROVA');

CREATE TABLE escola.atividades(
	atividade_id				SERIAL NOT NULL PRIMARY KEY,
	gradecurricular_id			BIGINT NOT NULL REFERENCES escola.gradecurricular(gradecurricular_id),
	data_atividade				DATE,
	titulo						VARCHAR(90) NOT NULL,
	descricao					TEXT,
	objetivo					TEXT,
	orientacoes					TEXT,
	avaliacao					BOOLEAN,
	tipoatividade_id			BIGINT NOT NULL REFERENCES escola.tipos_atividades(tipoatividade_id),
	nota						INTEGER,
	conceito					VARCHAR(3),
	entrega						TIMESTAMP,
	status						BOOLEAN
);
