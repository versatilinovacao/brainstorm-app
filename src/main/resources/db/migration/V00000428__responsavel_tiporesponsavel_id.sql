ALTER TABLE responsaveis	ADD tiporesponsavel_id			BIGINT;
ALTER TABLE responsaveis	ADD CONSTRAINT tiporesponsavel_fk	FOREIGN KEY(tiporesponsavel_id)	REFERENCES tipos_responsaveis(tiporesponsavel_id);
DROP INDEX tiporesponsavel_idx CASCADE;
CREATE INDEX tiporesponsavel_idx	ON responsaveis	USING btree(tiporesponsavel_id);
