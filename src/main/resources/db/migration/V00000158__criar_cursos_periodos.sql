CREATE TABLE escola.cursosperiodos(
	cursoperiodo_id					SERIAL NOT NULL PRIMARY KEY,
	curso_id					    BIGINT NOT NULL REFERENCES cursos(curso_id),
	periodoletivo_id				BIGINT NOT NULL REFERENCES escola.periodos_letivos(periodo_letivo_id),
	inicio_curso					DATE NOT NULL,
	fim_curso					    DATE,
	status						    BOOLEAN
);