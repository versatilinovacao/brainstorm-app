CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item					BIGINT;
	chamada							BIGINT;
	
	inicio_turma					DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	eventos							TIMESTAMP%TYPE;
	IsPeriodoLetivoNotFind			BOOLEAN;
	IsPeriodoLetivoFind				BOOLEAN;
	IsFormaCaderno					BOOLEAN;
	IsMatriculas					BOOLEAN;
	IsClassificacaoEmpresa 			BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio					BOOLEAN;
	IsGrade						BOOLEAN;
BEGIN
	IsError = false;

	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,1,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,3,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,2,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,5,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,6,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,7,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,9,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,8,false);
	INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,4,false);

	SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
	SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

	/* Consistência */
	SELECT count(1) = 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) > 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF (NOT IsPeriodoLetivoNotFind OR isPeriodoLetivoFind) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_inconsistencia_id = 1;
		IsError = true;
	END IF;	
		
	SELECT COALESCE(formato_caderno,0) < 1, COALESCE(classificacao_empresa_id,0) < 1 INTO IsFormaCaderno, IsClassificacaoEmpresa, IsCriterio FROM empresa.empresa LIMIT 1;
	IF IsFormaCaderno THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_inconsistencia_id = 2;
		IsError = true;
	END IF;
	
	IF IsClassificacaoEmpresa THEN 
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_inconsistencia_id = 5;
		IsError = true;
	END IF;
	
	IF IsCriterio THEN
		UPDATE escola.cadernos_incosnsistencias SET status = true WHERE caderno_inconsistencia_id = 6;
		IsError = true;
	END IF;

	SELECT count(1) < 1 INTO IsMatriculas FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsMatriculas) THEN
		UPDATE escola.cadenos_inconsistencias SET status = true WHERE cadernos_inconsistencia_id = 4;	
		IsError = true;
	END IF;
	
	SELECT count(1) < 1 INTO IsGrade FROM escola.gradecurricular WHERE turma_id = turma;
	IF (IsGrade) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE cadernos_inconsistencia_id = 8;
		IsError = true;
	END IF;
	
	IF NOT IsError THEN
		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP
			INSERT INTO escola.chamadas(caderno_id,aluno_id)
				VALUES(NEW.caderno_id,alunos.colaborador_id)
					RETURNING chamada_id INTO chamada;
			
			
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
			LOOP
				INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
								VALUES(eventos,NEW.caderno_id,chamada,false);
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

/*
CREATE OR REPLACE FUNCION escola.alterar_caderno() RETURN trigger AS $$
BEGIN

END; $$ LANGUAGE plpgsql;


CREATE TRIGGER alterar_caderno_tgi AFTER UPDATE
	ON escola.cadernos
		FOR EACH ROW
			EXECUTE PROCEDURE escola.alterar_caderno();

*/