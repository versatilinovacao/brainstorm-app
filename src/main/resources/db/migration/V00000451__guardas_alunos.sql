DROP MATERIALIZED VIEW escola.fichas_alunos_view;

CREATE TABLE escola.guardas_alunos(
	guardaaluno_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao				    VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO escola.guardas_alunos(descricao) VALUES('Compartilhada'),('Nidal'),('Unilateral'),('Alternada');

ALTER TABLE cliente		DROP COLUMN guarda_compartilhada;
ALTER TABLE cliente		DROP COLUMN guarda_nidal;
ALTER TABLE cliente		DROP COLUMN guarda_unilateral;
ALTER TABLE cliente		DROP COLUMN guarda_alternada;

ALTER TABLE cliente		ADD COLUMN guardaaluno_id	BIGINT;
ALTER TABLE cliente		ADD CONSTRAINT guardaaluno_fk	FOREIGN KEY(guardaaluno_id)	REFERENCES escola.guardas_alunos(guardaaluno_id);
CREATE INDEX guardaaluno_idx	ON cliente	USING btree(guardaaluno_id);

CREATE MATERIALIZED VIEW escola.fichas_alunos_view	AS
SELECT  a.colaborador_id AS aluno_id,
	a.composicao_id AS composicao_aluno_id,
	c.nome,
        CAST(i.data_nascimento AS VARCHAR) AS data_nascimento,
	COALESCE(s.descricao,'Não informado') AS genero,
	c.cep,
	l.logradouro || ', ' || c.numero AS endereco,
	b.descricao AS bairro,
	m.descricao AS cidade,
	e.descricao AS estado,
	e.estado_id,
	c.responsavel_id,
	0 AS idade,
	r.nome AS responsavel,
	u.codigo AS matricula,
	''::varchar AS data_matricula,
	c.observacao AS observacao,
	CASE WHEN cl.guardaaluno_id = 1 THEN 'X' ELSE '' END AS guarda_alternada,
	CASE WHEN cl.guardaaluno_id = 2 THEN 'X' ELSE '' END AS guarda_nidal,
	CASE WHEN cl.guardaaluno_id = 3 THEN 'X' ELSE '' END AS guarda_unilateral,
	CASE WHEN cl.guardaaluno_id = 4 THEN 'X' ELSE '' END AS guarda_compartilhada,
	CASE WHEN cl.outraescola THEN 'SIM' ELSE 'NÃO' END AS outraescola,
	CASE WHEN cl.outraescola THEN cl.qualoutraescola ELSE '' END AS qualoutraescola,
	CASE WHEN cl.outraescola THEN cl.qualoutraescolatempo ELSE '' END AS qualoutraescolatempo,
	CASE WHEN cl.outraescola THEN CASE WHEN cl.qualoutraescolaadaptacao THEN 'SIM' ELSE 'NÃO' END ELSE '' END AS qualoutraescolaadaptacao
FROM escola.alunos a
INNER JOIN colaborador c ON c.colaborador_id = a.colaborador_id AND c.composicao_id = a.composicao_id
INNER JOIN identificador i ON i.identificador_id = c.identificador_id
INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id
LEFT JOIN sexos s ON s.sexo_id = i.sexo_id
LEFT JOIN remoto.logradouros_view l ON l.cep = c.cep
LEFT JOIN remoto.bairros_view b ON b.bairro_id = l.bairro_id
LEFT JOIN remoto.cidades_view m ON m.cidade_id = b.cidade_id
LEFT JOIN remoto.estados_view e ON e.estado_id = m.estado_id
LEFT JOIN colaborador r ON r.colaborador_id = c.responsavel_id
LEFT JOIN matriculas u ON u.colaborador_id = a.colaborador_id AND u.composicao_colaborador_id = a.composicao_id
WHERE COALESCE(u.status,true) = true;

