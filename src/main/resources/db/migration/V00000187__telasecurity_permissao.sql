DROP TABLE public.telasecurity_permissao CASCADE;
CREATE TABLE public.telasecurity_permissao(
	telasecurity_id	    						BIGINT NOT NULL REFERENCES public.telas_security(telasecurity_id),
	permissao_id								BIGINT NOT NULL REFERENCES public.permissao(permissao_id)
);
