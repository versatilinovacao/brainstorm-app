CREATE SCHEMA tesouraria;

INSERT INTO almoxarifado.artigos(nome) VALUES('Rendimento');
INSERT INTO almoxarifado.classificacoes(nome) VALUES('Papel');

CREATE TABLE tesouraria.carteiras(
	carteira_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO tesouraria.carteiras(nome)	VALUES('AÇÕES'),('IMOBILIARIA');

CREATE TABLE contabil.posicoes(
	posicao_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao				VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO contabil.posicoes(descricao) VALUES('COMPRA'),('VENDA');

CREATE TABLE tesouraria.ordens(
	ordem_id				BIGSERIAL NOT NULL PRIMARY KEY,
	codigo					VARCHAR(30),
	carteira_id				BIGINT NOT NULL REFERENCES tesouraria.carteiras(carteira_id),
	artefato_id				BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
	corretora_id				BIGINT NOT NULL,
	composicao_corretora_id			BIGINT,
	posicao_id				BIGINT NOT NULL REFERENCES contabil.posicoes(posicao_id),
	quantidade				NUMERIC(16,4) NOT NULL,
	preco					NUMERIC(16,4) NOT NULL,
	evento					TIMESTAMP
);

ALTER TABLE tesouraria.ordens		ADD CONSTRAINT corretora_fk	FOREIGN KEY(corretora_id,composicao_corretora_id) REFERENCES colaborador(colaborador_id,composicao_id);
CREATE INDEX corretora_idx	ON tesouraria.ordens	USING btree(corretora_id,composicao_corretora_id);
