CREATE TABLE public.diasemanas(
	diasemana_id					SERIAL NOT NULL,
	descricao						VARCHAR(100)
);

ALTER TABLE public.diasemanas ADD CONSTRAINT diasemana_pk PRIMARY KEY(diasemana_id);

INSERT INTO public.diasemanas(descricao) VALUES('SEGUNDA'),('TERÇA'),('QUARTA'),('QUINTA'),('SEXTA'),('SÁBADO'),('DOMINGO');

CREATE TABLE public.gradematerias(
	grademateria_id					SERIAL NOT NULL,
	composicao_id					BIGINT,
	materia_id						BIGINT,
	ambiente_id						BIGINT,
	diasemana_id					BIGINT,
	turno_id						BIGINT,
	colaborador_id					BIGINT,
	turma_id						BIGINT,
	inicio							TIME,
	fim								TIME,
	justificativa					VARCHAR(150)
);

ALTER TABLE public.gradematerias ADD CONSTRAINT grademateria_id PRIMARY KEY(grademateria_id);
ALTER TABLE public.gradematerias ADD CONSTRAINT diasemana_fk    FOREIGN KEY(diasemana_id) REFERENCES public.diasemanas(diasemana_id);
ALTER TABLE public.gradematerias ADD CONSTRAINT materia_fk      FOREIGN KEY(materia_id)   REFERENCES public.materias(materia_id); 
ALTER TABLE public.gradematerias ADD CONSTRAINT ambiente_fk     FOREIGN KEY(ambiente_id)  REFERENCES public.ambientes(ambiente_id);
ALTER TABLE public.gradematerias ADD CONSTRAINT turma_fk		FOREIGN KEY(turma_id)	  REFERENCES public.turmas(turma_id);	

CREATE TABLE public.turmas_por_materias(
	turma_id						BIGINT,
	grademateria_id					BIGINT
);

ALTER TABLE public.turmas_por_materias ADD CONSTRAINT turma_por_materia_pk PRIMARY KEY(turma_id,grademateria_id);
ALTER TABLE public.turmas_por_materias ADD CONSTRAINT turma_fk FOREIGN KEY(turma_id) REFERENCES public.turmas(turma_id);
ALTER TABLE public.turmas_por_materias ADD CONSTRAINT grademateria_fk FOREIGN KEY(grademateria_id) REFERENCES public.gradematerias(grademateria_id);


