ALTER TABLE public.parceiros ADD COLUMN cargo_id		BIGINT;
ALTER TABLE public.parceiros ADD COLUMN especializacao_id	BIGINT;
ALTER TABLE public.parceiros ADD CONSTRAINT cargo_fk		FOREIGN KEY(cargo_id) 		REFERENCES public.cargos(cargo_id);
ALTER TABLE public.parceiros ADD CONSTRAINT especializacao_fk	FOREIGN KEY(especializacao_id)  REFERENCES public.especializacoes(especializacao_id);
