DROP TRIGGER deletar_caderno_tgi ON action.deletar_cadernos;
DROP FUNCTION action.deletar_caderno_execute();

CREATE OR REPLACE FUNCTION action.deletar_caderno_execute() RETURNS trigger AS $$
DECLARE 
	isCritica			BOOLEAN;
BEGIN
	SELECT COUNT(1) > 0 INTO isCritica FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;

	IF NOT isCritica THEN
		DELETE FROM escola.chamadas_alunos WHERE caderno_id = NEW.caderno_id;
		DELETE FROM escola.chamadas WHERE caderno_id = NEW.caderno_id;
		DELETE FROM escola.cadernos WHERE caderno_id = NEW.caderno_id;
	ELSE
		INSERT INTO msg.cadernos(caderno_id,mensagem,ocorrencia) 
			SELECT caderno_id, mensagem, ocorrencia FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
	END IF;

	DELETE FROM action.deletar_cadernos WHERE caderno_id = NEW.caderno_id;

	RETURN NEW;
END;$$
LANGUAGE plpgsql;
 
CREATE TRIGGER deletar_caderno_tgi AFTER INSERT
	ON action.deletar_cadernos
		FOR EACH ROW
			EXECUTE PROCEDURE action.deletar_caderno_execute();