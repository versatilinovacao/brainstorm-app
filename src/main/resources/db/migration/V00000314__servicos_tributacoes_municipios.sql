CREATE TABLE tributacao.servicos_iss_municipios(
	servicoissmunicipio_id				BIGSERIAL NOT NULL PRIMARY KEY,
	servico_id							BIGINT NOT NULL,
	cidade_id							BIGINT NOT NULL REFERENCES cidades(cidade_id),
	aliquota							NUMERIC(16,4)
);

ALTER TABLE tributacao.servicos_iss_municipios	ADD CONSTRAINT servico_unico	UNIQUE(servico_id,cidade_id);

CREATE TABLE contabil.servicos(
	servico_id							BIGSERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(500) NOT NULL,
	referencia							VARCHAR(6) NOT NULL,
	portoalegre							VARCHAR(9)
);

ALTER TABLE tributacao.servicos_iss_municipios	ADD CONSTRAINT servicos_fk FOREIGN KEY(servico_id) REFERENCES contabil.servicos(servico_id);