CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item						BIGINT;
	chamada							BIGINT;
	
	inicio_turma						DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	consistencias						public.consistencias%rowtype;
	eventos							TIMESTAMP%TYPE;
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	IsFormaCaderno						BOOLEAN;
	IsMatriculas						BOOLEAN;
	IsClassificacaoEmpresa 					BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio						BOOLEAN;
	IsGrade							BOOLEAN;
	IsTurmaFormada						BOOLEAN;
	existe							BOOLEAN;
BEGIN
--	IF NEW.status THEN
--		RETURN NEW;
		/*...Caderno de Chamada foi fechado, não pode mais ser alterado...*/
--	END IF;

	FOR consistencias IN
		SELECT * FROM public.consistencias WHERE grupo_consistencia_id = 1
	LOOP
		SELECT count(1) < 1 INTO existe FROM escola.cadernos_inconsistencias WHERE consistencia_id = consistencias.consistencia_id and caderno_id = NEW.caderno_id;
		IF existe THEN
			INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
				VALUES(NEW.caderno_id,consistencias.consistencia_id,false);
		END IF;
	END LOOP;

	UPDATE escola.cadernos_inconsistencias SET status = false WHERE caderno_id = NEW.caderno_id;

	/* Consistência */
	SELECT count(1) = 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) > 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF (NOT IsPeriodoLetivoNotFind OR isPeriodoLetivoFind) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = NEW.caderno_id;
	END IF;	
		
	SELECT COALESCE(formato_caderno_id,0) < 1, COALESCE(classificacao_empresa_id,0) < 1 INTO IsFormaCaderno, IsClassificacaoEmpresa, IsCriterio FROM empresa.empresa LIMIT 1;
	IF IsFormaCaderno THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 2 and caderno_id = NEW.caderno_id;
	END IF;
	
	IF IsClassificacaoEmpresa THEN 
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 5 and caderno_id = NEW.caderno_id;
	END IF;
	
	IF IsCriterio THEN
		UPDATE escola.cadernos_incosnsistencias SET status = true WHERE consistencia_id = 6 and caderno_id = NEW.caderno_id;
	END IF;

	SELECT count(1) > 0 INTO IsMatriculas FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsMatriculas) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 4 and caderno_id = NEW.caderno_id;	
	END IF;
	
	SELECT count(1) > 0 INTO IsGrade FROM escola.gradecurricular WHERE turma_id = turma;
	IF (IsGrade) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 8 and caderno_id = NEW.caderno_id;
	END IF;
	
	SELECT count(1) > 0 INTO IsTurmaFormada FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsTurmaFormada) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 9 and caderno_id = NEW.caderno_id;
	END IF;

	IF COALESCE(NEW.gradecurricular_id,0) = 0 THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 10 and caderno_id = NEW.caderno_id;
	END IF;

	SELECT count(1) > 0 INTO IsError FROM escola.cadernos_inconsistencias  WHERE caderno_id = NEW.caderno_id and status = false;
	IF NOT IsError THEN
		SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
		SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
	--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP
			INSERT INTO escola.chamadas(caderno_id,aluno_id)
				VALUES(NEW.caderno_id,alunos.colaborador_id)
					RETURNING chamada_id INTO chamada;
			
			
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
			LOOP
				INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
								VALUES(eventos,NEW.caderno_id,chamada,false);
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION escola.alterar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item						BIGINT;
	chamada							BIGINT;
	
	inicio_turma						DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	consistencias						public.consistencias%rowtype;
	eventos							TIMESTAMP%TYPE;
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	IsFormaCaderno						BOOLEAN;
	IsMatriculas						BOOLEAN;
	IsClassificacaoEmpresa 					BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio						BOOLEAN;
	IsGrade							BOOLEAN;
	IsTurmaFormada						BOOLEAN;
	existe							BOOLEAN;
BEGIN
--	IF NEW.status THEN
--		RETURN NEW;
		/*...Caderno de Chamada foi fechado, não pode mais ser alterado...*/
--	END IF;

	FOR consistencias IN
		SELECT * FROM public.consistencias WHERE grupo_consistencia_id = 1
	LOOP
		SELECT count(1) < 1 INTO existe FROM escola.cadernos_inconsistencias WHERE consistencia_id = consistencias.consistencia_id and caderno_id = NEW.caderno_id;
		IF existe THEN
			INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
				VALUES(NEW.caderno_id,consistencias.consistencia_id,false);
		END IF;
	END LOOP;

	UPDATE escola.cadernos_inconsistencias SET status = false WHERE caderno_id = NEW.caderno_id;

	/* Consistência */
	SELECT count(1) = 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) > 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF (NOT IsPeriodoLetivoNotFind OR isPeriodoLetivoFind) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = NEW.caderno_id;
	END IF;	
		
	SELECT COALESCE(formato_caderno_id,0) < 1, COALESCE(classificacao_empresa_id,0) < 1 INTO IsFormaCaderno, IsClassificacaoEmpresa, IsCriterio FROM empresa.empresa LIMIT 1;
	IF IsFormaCaderno THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 2 and caderno_id = NEW.caderno_id;
	END IF;
	
	IF IsClassificacaoEmpresa THEN 
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 5 and caderno_id = NEW.caderno_id;
	END IF;
	
	IF IsCriterio THEN
		UPDATE escola.cadernos_incosnsistencias SET status = true WHERE consistencia_id = 6 and caderno_id = NEW.caderno_id;
	END IF;

	SELECT count(1) > 0 INTO IsMatriculas FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsMatriculas) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 4 and caderno_id = NEW.caderno_id;	
	END IF;
	
	SELECT count(1) > 0 INTO IsGrade FROM escola.gradecurricular WHERE turma_id = turma;
	IF (IsGrade) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 8 and caderno_id = NEW.caderno_id;
	END IF;
	
	SELECT count(1) > 0 INTO IsTurmaFormada FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsTurmaFormada) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 9 and caderno_id = NEW.caderno_id;
	END IF;

	IF COALESCE(NEW.gradecurricular_id,0) = 0 THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 10 and caderno_id = NEW.caderno_id;
	END IF;

	SELECT count(1) > 0 INTO IsError FROM escola.cadernos_inconsistencias  WHERE caderno_id = NEW.caderno_id and status = false;
	IF NOT IsError THEN
		SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
		SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
	--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP
			INSERT INTO escola.chamadas(caderno_id,aluno_id)
				VALUES(NEW.caderno_id,alunos.colaborador_id)
					RETURNING chamada_id INTO chamada;
			
			
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
			LOOP
				INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
								VALUES(eventos,NEW.caderno_id,chamada,false);
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
	
END; $$ LANGUAGE plpgsql;
