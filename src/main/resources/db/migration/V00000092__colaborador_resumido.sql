CREATE TABLE public.tercerizados(
	tercerizado_id				SERIAL NOT NULL PRIMARY KEY
);

ALTER TABLE public.colaborador 		ADD COLUMN tercerizado_id		BIGINT;
ALTER TABLE public.tercerizados  	ADD CONSTRAINT colaborador_fk FOREIGN KEY(tercerizado_id) REFERENCES public.tercerizados(tercerizado_id); 

CREATE VIEW public.colaborador_geral_view AS
	SELECT c.colaborador_id,
	       c.nome,
               c.referencia,
               t.numero,
	       CASE WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) = 0 THEN 'Não Classificado'
		    WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) = 1 THEN 'Empresa'
		    WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) = 2 THEN 'Cliente'
		    WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) = 3 THEN 'Tercerizado'
		    WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) = 4 THEN 'Parceiro'
		    WHEN COALESCE(c.empresa_id,0)+COALESCE(c.cliente_id,0)+COALESCE(c.parceiro_id,0)+COALESCE(c.tercerizado_id,0) > 4 THEN 'Multipla Classificação'
	       END AS classificacao	
	FROM colaborador c
		LEFT JOIN telefones_colaboradores tc ON tc.colaborador_id = c.colaborador_id
		LEFT JOIN telefones t ON t.telefone_id = tc.telefone_id
		LEFT JOIN empresa e ON e.empresa_id = c.empresa_id
		LEFT JOIN cliente l ON l.cliente_id = c.cliente_id
		LEFT JOIN parceiros p ON p.parceiro_id = c.parceiro_id
		LEFT JOIN tercerizados s ON s.tercerizado_id = c.tercerizado_id
