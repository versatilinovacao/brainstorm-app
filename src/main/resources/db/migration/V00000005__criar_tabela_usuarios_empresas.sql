CREATE TABLE usuarios_empresas(
	usuario_id					BIGINT,
	colaborador_id				BIGINT
);

ALTER TABLE usuarios_empresas ADD CONSTRAINT user_emp_pk PRIMARY KEY(usuario_id,colaborador_id);

ALTER TABLE usuarios_empresas ADD CONSTRAINT usuario_fk FOREIGN KEY(usuario_id) REFERENCES usuario(usuario_id);
ALTER TABLE usuarios_empresas ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES colaborador(colaborador_id);