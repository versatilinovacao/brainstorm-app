CREATE TABLE financeiro.contas_pagar(
	contapagar_id				BIGSERIAL NOT NULL PRIMARY KEY,
	composicao_id				BIGINT,
	
	empresa_id					BIGINT,
	composicao_empresa_id		BIGINT,
	
	vencimento					DATE NOT NULL,
	emissao						DATE NOT NULL,
	pagamento					DATE,
	quitacao					DATE,
	
	credor_id					BIGINT NOT NULL,
	composicao_credor_id		BIGINT,

	desconto					NUMERIC(16,4),
	valorpago					NUMERIC(16,4),
	saldo						NUMERIC(16,4),
	total						NUMERIC(16,4),
	
	tipopagamento_id			BIGINT NOT NULL REFERENCES financeiro.tipos_pagamentos(tipopagamento_id),
	negociacao_id				BIGINT NOT NULL REFERENCES financeiro.negociacoes(negociacao_id),
	formapagamento_id			BIGINT REFERENCES financeiro.formas_pagamentos(formapagamento_id),
	
	parcela						INTEGER,
	protesto					BOOLEAN,
	
	status						BOOLEAN
		
);

ALTER TABLE financeiro.contas_receber		ADD COLUMN tipopagamento_id			BIGINT;
ALTER TABLE financeiro.contas_receber		ADD CONSTRAINT tipo_pagamento_fk	FOREIGN KEY(tipopagamento_id) REFERENCES financeiro.tipos_pagamentos(tipopagamento_id);

CREATE INDEX ON financeiro.contas_pagar(vencimento);
CREATE INDEX ON financeiro.contas_pagar(pagamento);
CREATE INDEX ON financeiro.contas_pagar(credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(vencimento,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(pagamento,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(emissao,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(quitacao,credor_id,composicao_credor_id);

