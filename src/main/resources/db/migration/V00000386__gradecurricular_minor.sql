CREATE OR REPLACE VIEW escola.gradecurricular_minor AS
SELECT gradecurricular_id,
       turma_id,
       professor_id,
       materia_id,
       curso_id,
       cursoperiodo_id,
       composicao_professor_id,
       status
       FROM escola.gradecurricular;