ALTER TABLE almoxarifado.artefatos	ADD COLUMN composicao_id				BIGINT;

ALTER TABLE almoxarifado.artefatos	ADD CONSTRAINT composicao_fk	FOREIGN KEY(composicao_id) REFERENCES almoxarifado.artefatos(artefato_id);

CREATE INDEX composicao_artefato_idx ON almoxarifado.artefatos(composicao_id,artefato_id);
CREATE INDEX composicao_idx ON almoxarifado.artefatos(composicao_id);