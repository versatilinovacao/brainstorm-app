CREATE OR REPLACE VIEW telefones_colaboradores_view AS 
SELECT telefone_id, numero, ramal, tipo, vinculo FROM (
	SELECT t.telefone_id, t.numero, t.ramal, tt.descricao as tipo, colaborador_id as vinculo FROM telefones_colaboradores tc
		INNER JOIN telefones t ON t.telefone_id = tc.telefone_id
		INNER JOIN tipos_telefones tt ON tt.tipo_telefone_id = t.tipo_telefone_id) visualizacao_telefones
	GROUP BY telefone_id,numero,ramal,tipo,vinculo;
