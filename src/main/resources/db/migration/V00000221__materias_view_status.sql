ALTER TABLE public.materias ADD COLUMN status	BOOLEAN DEFAULT true;

CREATE OR REPLACE VIEW escola.materias_view AS 
	SELECT m.materia_id, m.descricao, m.tipo_materia_id, m.status FROM materias m
	WHERE m.status = true;

CREATE OR REPLACE VIEW escola.materias_inativas_view AS 
	SELECT m.materia_id, m.descricao, m.tipo_materia_id, m.status FROM materias m
	WHERE m.status = false;
	
