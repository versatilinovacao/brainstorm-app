CREATE VIEW nfse.prestador_view AS
	SELECT c.colaborador_id,p.cnpj,p.inscricao_municipal, a.codigo FROM public.pessoajuridica p
	INNER JOIN colaborador c ON c.pessoajuridica_id = p.pessoajuridica_id
	LEFT JOIN contabil.cnaes a ON a.cnae_id = p.cnae_id 
	WHERE c.status = true
	  AND COALESCE(a.codigo,'') != '';


CREATE TABLE nfse.rps(
	rps_id				BIGSERIAL NOT NULL PRIMARY KEY,
	tomador_id			BIGINT NOT NULL REFERENCES nfse.tomador(tomador_id),
	prestador_id			BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	status				BOOLEAN,
	dataemissao			DATE NOT NULL,
	incentivadorcultural		BOOLEAN,
	optantesimplesnacional		BOOLEAN
	
);
