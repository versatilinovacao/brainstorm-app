DROP VIEW colaborador_view;

CREATE OR REPLACE VIEW colaborador_view AS 
 SELECT c.colaborador_id,
    c.composicao_id,
    c.nome,
    c.fantasia,
    c.referencia,
    c.cep,
    c.email,
    c.status,
    i.foto_thumbnail,
    t.numero AS telefone,
    ( SELECT count(1) > 0 FROM empresa.empresa ep WHERE ep.empresa_id = c.empresa_id LIMIT 1 ) AS isempresa,
    ( SELECT count(1) > 0 FROM cliente cl WHERE cl.cliente_id = c.cliente_id LIMIT 1 ) AS iscliente,
    ( SELECT count(1) > 0 FROM tercerizados tc WHERE tc.tercerizado_id = c.tercerizado_id LIMIT 1 ) AS isterceiro,
    ( SELECT count(1) > 0 FROM parceiros pc WHERE pc.parceiro_id = c.parceiro_id LIMIT 1 ) AS isparceiro
   FROM colaborador c
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
     LEFT JOIN telefones t ON t.telefone_id = c.telefone_id
  WHERE c.status = true
  ORDER BY c.nome;
