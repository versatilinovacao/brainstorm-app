CREATE OR REPLACE FUNCTION dblink_permissoes_salvar() RETURNS trigger AS $$
DECLARE
	comando		VARCHAR(250);
BEGIN
	IF (COALESCE(NEW.permissao_id,0) > 0) THEN
		comando = 'UPDATE permissao permissao_id = '||NEW.permissao_id||',nome = '''||NEW.nome||''',label = '''||NEW.label||''',descricao = '''||NEW.descricao||''' WHERE permissao_id = '||NEW.permissao_id||';';
	ELSE
		comando = 'INSERT INTO permissao(nome,label,descricao) VALUES('''||NEW.nome||''','''||NEW.label||''','''||NEW.descricao||''');';
	END IF;
	
	SELECT dblink_exec('seguranca_server',comando);

	RETURN NEW;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER dblink_permissoes_gatilho AFTER INSERT OR UPDATE ON permissao
	FOR EACH ROW EXECUTE PROCEDURE dblink_permissoes_salvar();

