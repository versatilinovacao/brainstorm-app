DROP VIEW usuariosview;

CREATE OR REPLACE VIEW usuariosview AS 
 SELECT u.usuario_id,
	    (SELECT c.colaborador_id AS empresa_id FROM colaborador c WHERE COALESCE(c.empresa_id, 0::bigint) > 0) AS empresa_id,
	    (SELECT c.composicao_id AS composicao_empresa_id FROM colaborador c WHERE COALESCE(c.empresa_id, 0::bigint) > 0) AS composicao_empresa_id,
        u.nome,
        i.data_nascimento AS nascimento,
        u.senha,
        u.status,
        u.email,
        u.isserver,
        u.isclient,
        u.iscounter,
        i.foto_thumbnail
   FROM usuario u
     LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
  WHERE u.status = true;
