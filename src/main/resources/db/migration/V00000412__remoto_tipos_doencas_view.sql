CREATE TYPE record_tiposdoencas	AS ( tipodoenca_id BIGINT, nome VARCHAR, descricao VARCHAR );

CREATE OR REPLACE FUNCTION dblink_tiposdoencas() RETURNS SETOF record_tiposdoencas AS $$
DECLARE
	reg	record_tiposdoencas%ROWTYPE;
BEGIN
	CREATE EXTENSION IF NOT EXISTS dblink; 
	BEGIN
		CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE SERVER saude_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'saude_db');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE USER MAPPING FOR postgres SERVER saude_server OPTIONS (user 'postgres', password 'default123');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		GRANT USAGE ON FOREIGN SERVER saude_server TO postgres;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;

	FOR reg IN 
		SELECT tipodoenca_id,nome,descricao
		FROM( 
		SELECT * FROM public.dblink ('saude_server','select tipodoenca_id,nome,descricao from public.tipos_doencas')  AS DATA(tipodoenca_id BIGINT, nome VARCHAR, descricao VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.tiposdoencas_view AS 
select tipodoenca_id, nome, descricao from dblink_tiposdoencas(); 
