CREATE TABLE escola.tipos_arquivos(
	tipoarquivo_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(60) NOT NULL,
	sigla					VARCHAR(6) NOT NULL UNIQUE
);

INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Arquivo Texto','TXT');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Formato Portatil de Documento','PDF');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('WORD','DOC');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('WORD','DOCX');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Arquivo Compactardo','ZIP');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Excel','XLSM');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Excel','XLSX');
INSERT INTO escola.tipos_arquivos(nome,sigla) VALUES('Excel','XLS');

CREATE TABLE escola.atividades_upload(
	atividadeupload_id			BIGSERIAL NOT NULL PRIMARY KEY,
	atividade_id				BIGINT NOT NULL,
	nome					VARCHAR(100) NOT NULL,
	path					VARCHAR(255) NOT NULL,
	tipoarquivo_id				BIGINT,
	size					BIGINT
);

ALTER TABLE escola.atividades_upload	ADD CONSTRAINT atividade_fk	FOREIGN KEY(atividade_id)	REFERENCES escola.atividades(atividade_id);
ALTER TABLE escola.atividades_upload	ADD CONSTRAINT tipoarquivo_fk	FOREIGN KEY(tipoarquivo_id)	REFERENCES escola.tipos_arquivos(tipoarquivo_id);
CREATE INDEX atividade_upload_idx	ON escola.atividades		USING btree(atividade_id);
CREATE INDEX tipoarquivo_idx		ON escola.tipos_arquivos	USING btree(tipoarquivo_id);
