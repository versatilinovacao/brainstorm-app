DROP VIEW escola.turmas_ativas_view;

CREATE OR REPLACE VIEW escola.turmas_ativas_view AS 
 SELECT t.turma_id,
    t.descricao,
    ( SELECT min(periodos_letivos_itens.periodo_inicial) AS min
           FROM escola.periodos_letivos_itens
          WHERE periodos_letivos_itens.periodo_letivo_id = p.periodo_letivo_id) AS inicio,
    ( SELECT max(periodos_letivos_itens.periodo_final) AS max
           FROM escola.periodos_letivos_itens
          WHERE periodos_letivos_itens.periodo_letivo_id = p.periodo_letivo_id) AS fim,
    t.status
   FROM turmas t
     LEFT JOIN escola.gradecurricular g ON g.turma_id = t.turma_id
     LEFT JOIN cursos c ON c.curso_id = g.curso_id
     LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
  WHERE t.status = true;


DROP VIEW escola.turmas_inativas_view;

CREATE OR REPLACE VIEW escola.turmas_inativas_view AS 
 SELECT t.turma_id,
    t.descricao,
    ( SELECT min(periodos_letivos_itens.periodo_inicial) AS min
           FROM escola.periodos_letivos_itens
          WHERE periodos_letivos_itens.periodo_letivo_id = p.periodo_letivo_id) AS inicio,
    ( SELECT max(periodos_letivos_itens.periodo_final) AS max
           FROM escola.periodos_letivos_itens
          WHERE periodos_letivos_itens.periodo_letivo_id = p.periodo_letivo_id) AS fim,
    t.status
   FROM turmas t
     LEFT JOIN escola.gradecurricular g ON g.turma_id = t.turma_id
     LEFT JOIN cursos c ON c.curso_id = g.curso_id
     LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
  WHERE t.status = false;
