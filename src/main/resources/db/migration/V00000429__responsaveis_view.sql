DROP VIEW responsaveis_view;

CREATE OR REPLACE VIEW responsaveis_view AS 
 SELECT r.responsavel_id,
    ( SELECT c1.nome
           FROM colaborador c1
          WHERE c1.colaborador_id = rc.responsavel_id) AS nome,
    ( SELECT t.numero
           FROM telefones t
          WHERE t.telefone_id = c.telefone_id) AS telefone,
    rc.colaborador_id,
    c.composicao_id AS composicao_colaborador_id,
    r.tiporesponsavel_id,
    t.descricao AS descricaotiporesponsavel
   FROM responsaveis r
     JOIN responsaveis_colaboradores rc ON rc.responsavel_id = r.responsavel_id
     JOIN colaborador c ON c.colaborador_id = rc.responsavel_id AND c.status = true
     LEFT JOIN tipos_responsaveis t ON t.tiporesponsavel_id = r.tiporesponsavel_id;
     
 DROP VIEW responsavel_view;

CREATE OR REPLACE VIEW responsavel_view AS 
 SELECT c.colaborador_id AS responsavel_id,
    c.composicao_id AS composicao_colaborador_id,
    c.nome,
    ( SELECT t.numero
           FROM telefones t
          WHERE t.telefone_id = c.telefone_id) AS telefone,
    r.tiporesponsavel_id,
    t.descricao AS descricaotiporesponsavel
   FROM colaborador c
   LEFT JOIN responsaveis r ON r.responsavel_id = c.responsavel_id
   LEFT JOIN tipos_responsaveis t ON t.tiporesponsavel_id = r.tiporesponsavel_id
  WHERE c.status = true AND COALESCE(c.empresa_id, 0::bigint) = 0 AND (COALESCE(c.cliente_id, 0::bigint) <> 0 OR COALESCE(c.parceiro_id, 0::bigint) <> 0)
  ORDER BY c.nome;