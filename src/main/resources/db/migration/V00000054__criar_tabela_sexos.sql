CREATE TABLE public.sexos(
	sexo_id					SERIAL NOT NULL PRIMARY KEY,
	descricao				VARCHAR(50) UNIQUE
);

INSERT INTO public.sexos(descricao) VALUES('Masculino'),('Feminino');