DROP VIEW alunos_matriculados_view;
CREATE OR REPLACE VIEW alunos_matriculados_view AS 
 SELECT c.colaborador_id,
    c.nome,
    m.codigo,
    m.curso_id
   FROM colaborador c
     JOIN cliente cl ON cl.cliente_id = c.cliente_id
     JOIN matriculas m ON m.colaborador_id = c.colaborador_id
  WHERE cl.tipo_cliente_id = (( SELECT tipos_clientes.tipo_cliente_id
           FROM tipos_clientes 
          WHERE tipos_clientes.descricao::text = 'ALUNO'::text)) AND (EXISTS ( SELECT 1
           FROM matriculas 
          WHERE matriculas.colaborador_id = c.colaborador_id AND matriculas.status = true));
