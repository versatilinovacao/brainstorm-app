CREATE SCHEMA financeiro;

CREATE TABLE financeiro.negociacoes(
	negociacao_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO financeiro.negociacoes(descricao) VALUES('Dinheiro'),('Cartão'),('Boleto'),('Promissória'),('Cheque');

CREATE TABLE financeiro.formas_pagamentos(
	formapagamento_id			BIGSERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO financeiro.formas_pagamentos(descricao) VALUES('Dinheiro'),('Cartao'),('Boleto'),('Promissória'),('Cheque');

CREATE TABLE financeiro.contas_receber(
	contareceber_id				BIGSERIAL NOT NULL PRIMARY KEY,
	composicao_id				BIGINT,
	
	empresa_id					BIGINT,
	composicao_empresa_id		BIGINT,
	
	vencimento					DATE NOT NULL,
	emissao						DATE NOT NULL,
	pagamento					DATE,
	quitacao					DATE,
	
	devedor_id					BIGINT NOT NULL,
	composicao_devedor_id		BIGINT,

	desconto					NUMERIC(16,4),
	valorpago					NUMERIC(16,4),
	saldo						NUMERIC(16,4),
	total						NUMERIC(16,4),
	
	negociacao_id				BIGINT NOT NULL REFERENCES financeiro.negociacoes(negociacao_id),
	formapagamento_id			BIGINT REFERENCES financeiro.formas_pagamentos(formapagamento_id),
	
	parcela						INTEGER,
	protesto					BOOLEAN,
	
	status						BOOLEAN
		
);

ALTER TABLE financeiro.contas_receber	ADD CONSTRAINT devedor_fk	FOREIGN KEY(devedor_id,composicao_devedor_id)	REFERENCES public.colaborador(colaborador_id,composicao_id);

CREATE OR REPLACE VIEW financeiro.contas_receber_ativas_view AS
SELECT vencimento,emissao,pagamento,quitacao,total,devedor_id,composicao_devedor_id,parcela,status
FROM financeiro.contas_receber
WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW financeiro.contas_receber_inativas_view AS
SELECT vencimento,emissao,pagamento,quitacao,total,devedor_id,composicao_devedor_id,parcela,status
FROM financeiro.contas_receber
WHERE COALESCE(status,true) = false;
