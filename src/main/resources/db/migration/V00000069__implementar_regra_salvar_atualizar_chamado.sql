CREATE OR REPLACE FUNCTION informar_data_abertura_chamado() RETURNS trigger AS $$
BEGIN	
	NEW.abertura = now();

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER informar_data_abertura_chamado_trigger 
	BEFORE INSERT 
		ON public.chamados FOR EACH ROW
			EXECUTE PROCEDURE informar_data_abertura_chamado();

CREATE OR REPLACE FUNCTION informar_data_fechamento_chamado() RETURNS trigger AS $$
BEGIN
	IF NEW.time_line_id in (8,14) THEN 
		NEW.fechamento = now();
	END IF;
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER informar_data_fechamento_chamado_trigger
	BEFORE UPDATE
		ON public.chamados FOR EACH ROW
			EXECUTE PROCEDURE informar_data_fechamento_chamado();
