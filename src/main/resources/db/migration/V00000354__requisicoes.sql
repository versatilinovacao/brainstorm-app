CREATE SCHEMA comercial;
CREATE TABLE comercial.tipos_requisicoes(
	tiporequisicao_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(50) NOT NULL UNIQUE
);

CREATE INDEX tiporequisicao_idx ON comercial.tipos_requisicoes(nome);

INSERT INTO comercial.tipos_requisicoes(nome) VALUES('Pedido'),('Orçamento'),('Contrato'),('Ordem Serviço'),('Manutenção'),('Transferencia'),('Agendamento');

/*
	Pedido 			(Compra - Venda)
	Orçamento 		(Compra - Venda)
	Contrato		(Locação - Serviço - Venda - Trabalho)
	Ordem Serviço	(Orçamento - Atendimento - Garantia)
		Orçamento: Pode ou não ser o primeiro atendimento, sendo o inicio que determinara se o problema pode ser reparado e qual o valor para isso, também pode ser um orçamento de construtividade
			Construtividade: é quando o orçamento trata de um atendimento parta resolver uma questão que não existe, por exemplo um orçamento para desenvolver um sistema novo ou aplicativo 
							 de celular para um cliente, para construir uma impressão de um caderno de chamadas para uma escola, ou uma aula particular de uma escola para um aluno ou grupo de alunos 
		Atendimento: Trata do serviço que será, esta ou foi prestado após a conclusão e aceite do orçamento.
		Garantia: É uma requisição de outra requisição já atendida e concluida, que retornou com reclamação de não conformidade, podendo ou não ser aceita, mas que deve obrigatoriamente estar vinculada
				  a uma composição da requisição.
	
	A composição da requisição, pode existir sempre que uma garantia for acionada ou que precise ser feito novos serviços que não foram previstos ou que a garantia não cobre sobre o serviço já prestado.
		Ex: Uma parede que foi construida mas não foi solicitado o reboco, mas no meio do processo o cliente muda de ideia e quer acrescentar mais este serviço e antes do final o mesmo cliente resolve 
			que a parede deve ser pintada.
			
			Tudo faz parte do mesmo serviço mas não foi incluido no planejamento inicial, assim sendo precisa ser adicionado e deve refletir no valor total e final do trabalho.
	
	Composição de um contrato:
		Identificação da Partes(Contratante / Contratado)
		Obrigações:
			Contratante
			Contratado
		Pagamento:
			Condicoes de pagamento;
			Inadimplencia e multas;
			Recisão;
			
	Garantia		
	
	Incluir termo de garantia
	
	Personalização: Implica em mudanças no sistema que precisaram ser atendidas separadamente, aonde vão criar uma linha alternativa de uma ou mais partes do sistema, impedindo que estas partes
					possam ser utilizadas por outras empresas que anteriormente à utilizavam ou poderiam utilizar, deixando o uso praticamente exclusivo ao requerente, atendendo sua vontade de forma ampla
					e praticamente irrestrita.
	
	
	Especialização: É quando o sistema precisa que seja adcionado a ele, algum item ou recurso que não pode ser feito de forma a atender a todos ou grande maioria, e não afeta nenhuma parte do sistema
					que já exista ou que podera vir a existir, tornando este recurso de uso exclusido do requerente.
					
					É quando temos uma situação aonde o usuário precisa que seja implementado ao sistema um recurso que ainda não existe, mas este recurso não afeta o funcionamento de todos os recursos
					que já existem e não ira conflitar com recursos novos, sendo utilizado unica e exclusivamente pelo solicitante, de forma a não criar uma linha de desenvolvimento que necessite de 
					atenção especial para este cliente, e importante ressaltar que se esta modificação em qualquer momento entrar em conflito com a personalização e vier a tornar-se uma personalização,
					ela será cobrada como tal.
					
					Exemplo de Especialização: Contratos ou Cadernos de Chamada.
						Contrato é um item que sua impressão costuma ter particularidades singulares a cada empresa, porém depois de prontas não costumam sofrer modificações, tendem a perdurar por tempo
						indeterminado e não afetam o funcionamento do sistema como um todo, pois podem ser feitos outros formatos sem precisar mudar o sistema.
						
					Tudo que for compreendido como especialização, será cobrado um valor com pagamento único, não vai fazer parte da mensalidade, porém em contra partida não fara parte do contrato, 
					desta forma sempre que precisar de alguma intervençção para algum ajuste, melhoria ou modificação por qualquer motivo, será cobrado valor a parte para cada caso, ao contrario da 
					personalização que para cada item de personalização, fara com que o valor cobrado passe a compor o valor da mensalidade e não possa ser desfeito sem um novo acordo prévio entre as
					partes.
	
	
*/

CREATE TABLE administracao.indices_precos(
	indicepreco_id					BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(5) NOT NULL UNIQUE,
	descricao						VARCHAR(80) NOT NULL
);

CREATE INDEX indice_nome_idx ON administracao.indices_precos(nome);

INSERT INTO administracao.indices_precos(nome,descricao) VALUES('IGPM','Indide de Preços por Atacado'),('IPC','Índice de Preços ao Consumidor'),('INCC','Índice Nacional da Contrução Civil');

CREATE TABLE contabil.igpm(
	igpm_id							BIGSERIAL NOT NULL PRIMARY KEY,
	indicepreco_id					BIGINT NOT NULL REFERENCES administracao.indices_precos(indicepreco_id),
	evento							TIMESTAMP NOT NULL,
	valor							NUMERIC(16,4),
	mes								INTEGER,
	ano								INTEGER
);

CREATE TABLE contabil.ipc(
	ipc_id							BIGSERIAL NOT NULL PRIMARY KEY,
	indicepreco_id					BIGINT NOT NULL REFERENCES administracao.indices_precos(indicepreco_id),
	evento							TIMESTAMP NOT NULL,
	valor							NUMERIC(16,4),
	mes								INTEGER,
	ano								INTEGER
);

CREATE TABLE contabil.incc(
	incc_id							BIGSERIAL NOT NULL PRIMARY KEY,
	indicepreco_id					BIGINT NOT NULL REFERENCES administracao.indices_precos(indicepreco_id),
	evento							TIMESTAMP NOT NULL,
	valor							NUMERIC(16,4),
	mes								INTEGER,
	ano								INTEGER
);

CREATE TABLE comercial.requisicoes(
	requisicao_id					BIGSERIAL NOT NULL PRIMARY KEY,
	
	composicao_id					BIGINT REFERENCES comercial.requisicoes(requisicao_id),
	
	evento							TIMESTAMP NOT NULL,
	tiporequisicao_id				BIGINT, --Pedido, Orçamento, Contrato, Ordem Serviço, Manutenção

	empresa_id						BIGINT NOT NULL,
	composicao_empresa_id			BIGINT,
	cliente_id						BIGINT,
	composicao_cliente_id			BIGINT,
	fornecedor_id					BIGINT,
	composicao_fornecedor_id		BIGINT,
	parcelamento_id					BIGINT,
	pagamento_id					BIGINT,

	objetocontratado_id				BIGINT REFERENCES almoxarifado.artefatos(artefato_id),

	assinatura						TIMESTAMP,
	renovacaoautomatica				BOOLEAN,
	pagamentodia					INTEGER, --Dia que foi definido o pagamento de eventuais parcelas
	validade_mes					INTEGER, --Validade do contrato em meses.

	multa							NUMERIC(16,4),
	mora							NUMERIC(16,4),
	indicereajuste_id				BIGINT REFERENCES administracao.indices_precos(indicepreco_id),

	basecalculoipi					NUMERIC(16,4),
	aliquotaipi						NUMERIC(16,4),
	valoripi						NUMERIC(16,4),

	basecalculoicms					NUMERIC(16,4),
	aliquotaicms					NUMERIC(16,4),
	valoricms						NUMERIC(16,4),

	basecalculopis					NUMERIC(16,4),
	aliquotapis						NUMERIC(16,4),
	valorpis						NUMERIC(16,4),

	basecalculocofins				NUMERIC(16,4),
	aliquotacofins					NUMERIC(16,4),
	valorcofins						NUMERIC(16,4),

	basecalculoissqn				NUMERIC(16,4),
	aliquotaissqn					NUMERIC(16,4),
	aliquotaretencaoissqn			NUMERIC(16,4),
	valorretencaoissqn				NUMERIC(16,4),
	valorissqn						NUMERIC(16,4),

	descontopercentual				NUMERIC(16,4),
	valordesconto					NUMERIC(16,4),

	total							NUMERIC(16,4)
);


CREATE TABLE comercial.servicos(
	servico_id						BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(200) NOT NULL,
	requisicao_id					BIGINT REFERENCES comercial.requisicoes(requisicao_id),
	artefato_id						BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
	servicomunicipio_id				BIGINT REFERENCES servico.servicos(servico_id),
	naturezaopservico_id			BIGINT NOT NULL REFERENCES contabil.naturezaopservicos(naturezaopservico_id),
	basecalculo						NUMERIC(16,4),
	aliquotaissqn					NUMERIC(16,4),
	retencao						NUMERIC(16,4),
	valorretido						NUMERIC(16,4),
	valor							NUMERIC(16,4),
	
	aceite							BOOLEAN --Indica que o cliente aceitou o serviço, o processo define que o cliente deve atraves de seu aparelho dar o aceite ou por meio de equipamento do tecnico, mas neste caso precisara pedir senha do usuário para dar aceite
);

--Ser for um pedido, dai devemos ter os itens do pedido.
CREATE TABLE comercial.requisicao_itens(
	requisicaoitem_id				BIGSERIAL NOT NULL PRIMARY KEY,
	requisicao_id					BIGINT NOT NULL REFERENCES comercial.requisicoes(requisicao_id),
	
	artefato_id						BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
	
	quantidade						NUMERIC(16,4),
	
	custo_medio						NUMERIC(16,4),
	custo_transporte				NUMERIC(16,4),
	custo_producao					NUMERIC(16,4),
	
	preco							NUMERIC(16,4),
	desconto						NUMERIC(16,4),
	acrescimo						NUMERIC(16,4),
	total							NUMERIC(16,4),
	
	basecalculo_ipi					NUMERIC(16,4),
	aliquota_ipi					NUMERIC(16,4),
	valor_ipi						NUMERIC(16,4),
	
	basecalculo_icms				NUMERIC(16,4),
	aliquota_icms					NUMERIC(16,4),
	valor_icms						NUMERIC(16,4),
	
	basecalculo_icmsst				NUMERIC(16,4),
	aliquota_icmsst					NUMERIC(16,4),
	valor_icmsst					NUMERIC(16,4),
	
	basecalculo_pis					NUMERIC(16,4),
	aliquota_pis					NUMERIC(16,4),
	valor_pis						NUMERIC(16,4),
	
	basecalculo_cofins				NUMERIC(16,4),
	aliquota_cofins					NUMERIC(16,4),
	valor_cofins					NUMERIC(16,4),
	
	total_item						NUMERIC(16,4)
);

/*
	Relatos é uma tabela que guarda informações sobre o problema relatado pelo cliente.
*/
CREATE TABLE comercial.relatos(
	relato_id						BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						TEXT NOT NULL
	
);

CREATE TABLE comercial.testemunhas(
	testemunha_id					BIGSERIAL NOT NULL PRIMARY KEY,
	requisicao_id					BIGINT NOT NULL REFERENCES comercial.requisicoes(requisicao_id), --Contrato
	colaborador_id					BIGINT NOT NULL,
	composicao_colaborador_id		BIGINT,
	assinatura						TEXT
);

ALTER TABLE comercial.testemunhas	ADD CONSTRAINT colaborador_fk	FOREIGN KEY(colaborador_id,composicao_colaborador_id) REFERENCES public.colaborador(colaborador_id,composicao_id);
