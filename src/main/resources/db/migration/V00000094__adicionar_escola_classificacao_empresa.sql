INSERT INTO empresa.classificacao_empresa(descricao) VALUES('Escola');

ALTER TABLE empresa.empresa  ADD COLUMN classificacao_empresa_id  BIGINT;
ALTER TABLE empresa.empresa  ADD CONSTRAINT classificacao_fk FOREIGN KEY(classificacao_empresa_id) REFERENCES empresa.classificacao_empresa(classificacao_empresa_id);