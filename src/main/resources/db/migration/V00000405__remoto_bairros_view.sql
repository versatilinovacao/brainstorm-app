CREATE TYPE record_bairros	AS ( bairro_id INTEGER, descricao VARCHAR, cidade_id BIGINT );

CREATE OR REPLACE FUNCTION dblink_bairros() RETURNS SETOF record_bairros AS $$
DECLARE
	reg	record_bairros%ROWTYPE;
BEGIN
	BEGIN
		CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE SERVER cep_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'cep_db');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		CREATE USER MAPPING FOR postgres SERVER cep_server OPTIONS (user 'postgres', password 'default123');
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;
	
	BEGIN
		GRANT USAGE ON FOREIGN SERVER cep_server TO postgres;
	EXCEPTION WHEN OTHERS THEN
		RAISE NOTICE '';
	END;

	FOR reg IN 
		SELECT id_bairro AS bairro_id, bairro AS descricao, id_cidade AS cidade_id FROM public.dblink ('cep_server','select id_bairro, bairro, id_cidade from public.qualocep_bairro')  AS DATA( id_bairro INTEGER, bairro VARCHAR, id_cidade BIGINT )	
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.bairros_view AS 
select bairro_id, descricao, cidade_id from dblink_bairros();
