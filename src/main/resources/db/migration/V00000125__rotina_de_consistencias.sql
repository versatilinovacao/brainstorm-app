DROP TRIGGER reprocessar_consistencias_tgi ON public.reprocessar_consistencias;
DROP FUNCTION public.reprocessar_consistencias();
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Cadastro de Materias','Será necessário cadastrar as matérias antes de criar os cadernos',1);
CREATE OR REPLACE FUNCTION public.reprocessar_consistencias() RETURNS trigger AS $$
DECLARE
	IsPeriodoLetivoNotFind				BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	isExiste							BOOLEAN;
	cadernos1							escola.cadernos%ROWTYPE;
	cadernos2							escola.cadernos%ROWTYPE;
	cadernos_for						escola.cadernos%ROWTYPE;
	cadernos_for1						escola.cadernos%ROWTYPE;
	classificacao_empresa_v				BIGINT;
	ano_v								VARCHAR(4);
	periodo_letivo_v					BIGINT;
	unidade_v							BIGINT;
	formato_caderno_v					BIGINT;
	isExisteCurso						BOOLEAN;	
	isExisteTurmas						BOOLEAN;
	isExisteMaterias					BOOLEAN;
	isExisteGradeSelecionada			BOOLEAN;
	cadernos_grades						escola.cadernos%ROWTYPE;
BEGIN
	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT count(1) > 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) = 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF IsPeriodoLetivoNotFind = true THEN
		FOR cadernos1 IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE status = true AND ano = ano_v)
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 1 AND caderno_id = cadernos1.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos1.caderno_id,1,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = cadernos1.caderno_id;
			END IF;
		END LOOP;
	END IF;			
	
	IF isPeriodoLetivoFind = true THEN
		FOR cadernos2 IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE status = true AND ano = ano_v)
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 3 AND caderno_id = cadernos2.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos2.caderno_id,3,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 3 and caderno_id = cadernos2.caderno_id;
			END IF;
		END LOOP;
	END IF;
	
	SELECT count(1) > 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE status = false;
	IF isExiste THEN
		SELECT COALESCE(classificacao_empresa_id,0) INTO classificacao_empresa_v FROM empresa.empresa_matriz LIMIT 1;
		IF classificacao_empresa_v = 9 THEN 
			SELECT DATE_PART('year',now()) INTO ano_v;
			SELECT COALESCE(periodo_letivo_id,0) INTO periodo_letivo_v	FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;
			IF  periodo_letivo_v > 0 THEN
				FOR cadernos_for IN
					SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v 
				LOOP
					UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
												   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Classificação da Empresa');
				END LOOP;
			END IF;
		END IF;
		
	END IF;

	--select * from empresa.empresa_matriz
	SELECT COALESCE(unidade_id,0) INTO unidade_v FROM empresa.empresa_matriz LIMIT 1;
	IF unidade_v = 1 THEN --Unidade da empresa
		SELECT DATE_PART('year',now()) INTO ano_v;
		SELECT COALESCE(periodo_letivo_id,0) INTO periodo_letivo_v	FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;
		IF periodo_letivo_v > 0 THEN
			FOR cadernos_for IN
				SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v 
			LOOP
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
											   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Unidade da empresa');
			END LOOP;

		END IF;
		
	END IF;

	SELECT COALESCE(formato_caderno_id,0) INTO formato_caderno_v FROM empresa.empresa_matriz LIMIT 1;
	IF formato_caderno_v > 0 THEN --Unidade da empresa
		SELECT DATE_PART('year',now()) INTO ano_v;
		SELECT COALESCE(periodo_letivo_id,0) INTO periodo_letivo_v	FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;
		RAISE NOTICE 'PASSEI AQUI %.',periodo_letivo_v;
		IF periodo_letivo_v > 0 THEN
			FOR cadernos_for1 IN --SELECT * FROM escola.cadernos WHERE periodo_letivo_id = 1
				SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v 
			LOOP
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for1.caderno_id --select * from consistencias 		
											   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Forma do Caderno');
			END LOOP;

		END IF;
		
	END IF;
	
	SELECT count(1) < 1 INTO isExisteCurso FROM public.cursos LIMIT 2;

	IF isExisteCurso THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for1.caderno_id --select * from consistencias 		
									   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro de Curso');
		
	END IF;

	SELECT count(1) > 0 INTO isExisteTurmas FROM public.turmas LIMIT 2;

	IF isExisteTurmas THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for1.caderno_id --select * from consistencias 		
									   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro de Turma');
	END IF;

	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT COALESCE(periodo_letivo_id,0) INTO periodo_letivo_v	FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;
	RAISE NOTICE 'PASSEI AQUI %.',periodo_letivo_v;
	IF periodo_letivo_v > 0 THEN
		FOR cadernos_grades IN
			SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v 
		LOOP --select * from escola.cadernos
			SELECT COALESCE(gradecurricular_id,0) > 0 INTO isExiste FROM escola.cadernos WHERE caderno_id = cadernos_grades.caderno_id;
			IF isExiste THEN 
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for1.caderno_id --select * from consistencias 		
							   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Forma do Caderno');
			END IF;
		END LOOP;
	END IF;

	DELETE FROM public.reprocessar_consistencias WHERE reprocessar_consistencia_id = NEW.reprocessar_consistencia_id;
	RETURN NEW;
END; $$ LANGUAGE plpgsql;
														     
CREATE TRIGGER reprocessar_consistencias_tgi AFTER INSERT ON public.reprocessar_consistencias FOR EACH ROW EXECUTE PROCEDURE public.reprocessar_consistencias();
