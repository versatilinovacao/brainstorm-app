CREATE VIEW contatos_view AS 	
		select t.contato_id,
		       (select nome from colaborador c1 where colaborador_id = t.contato_id) as nome, '' as telefone, '' as ramal, '' as tipo, '' as email, c.colaborador_id as colaborador_id 
				from contatos t
					inner join contatos_colaboradores cc on cc.contato_id = t.contato_id
					inner join colaborador c on c.colaborador_id = cc.colaborador_id;
