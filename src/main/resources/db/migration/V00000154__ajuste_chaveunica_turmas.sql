ALTER TABLE public.turmas DROP CONSTRAINT turmas_descricao_key;
ALTER TABLE public.turmas ADD CONSTRAINT turmas_descricao_key UNIQUE (descricao, inicio, fim);
