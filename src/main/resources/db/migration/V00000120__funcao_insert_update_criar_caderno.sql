DROP TRIGGER criar_caderno_tgi ON escola.cadernos;
DROP TRIGGER alterar_caderno_tgu ON escola.cadernos;
DROP FUNCTION escola.criar_caderno();
DROP FUNCTION escola.alterar_caderno();

CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item						BIGINT;
	chamada							BIGINT;
	
	inicio_turma						DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	consistencias						public.consistencias%rowtype;
	eventos							TIMESTAMP%TYPE;
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	IsFormaCaderno						BOOLEAN;
	IsMatriculas						BOOLEAN;
	IsClassificacaoEmpresa 					BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio						BOOLEAN;
	IsGrade							BOOLEAN;
	IsTurmaFormada						BOOLEAN;
	existe							BOOLEAN;
BEGIN
--	IF NEW.status THEN
--		RETURN NEW;
		/*...Caderno de Chamada foi fechado, não pode mais ser alterado...*/
--	END IF;

	FOR consistencias IN
		SELECT * FROM public.consistencias WHERE grupo_consistencia_id = 1
	LOOP
		SELECT count(1) < 1 INTO existe FROM escola.cadernos_inconsistencias WHERE consistencia_id = consistencias.consistencia_id and caderno_id = NEW.caderno_id;
		IF existe THEN
			INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
				VALUES(NEW.caderno_id,consistencias.consistencia_id,false);
		END IF;
	END LOOP;

	SELECT count(1) > 0 INTO IsError FROM escola.cadernos_inconsistencias  WHERE caderno_id = NEW.caderno_id and status = false;
	IF NOT IsError THEN
		SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
		SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
	--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP
			INSERT INTO escola.chamadas(caderno_id,aluno_id)
				VALUES(NEW.caderno_id,alunos.colaborador_id)
					RETURNING chamada_id INTO chamada;
			
			
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
			LOOP
				INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
								VALUES(eventos,NEW.caderno_id,chamada,false);
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER criar_caderno_tgi AFTER INSERT ON escola.cadernos FOR EACH ROW EXECUTE PROCEDURE escola.criar_caderno();
CREATE TRIGGER criar_caderno_tgu AFTER UPDATE ON escola.cadernos FOR EACH ROW EXECUTE PROCEDURE escola.criar_caderno(); 
