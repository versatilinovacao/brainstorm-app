DROP TABLE action.deletar_periodos_letivo;

CREATE TABLE action.deletar_periodos_letivo(
	periodo_letivo_id			BIGINT NOT NULL PRIMARY KEY
);

DROP TABLE critica.periodos_letivo;

CREATE TABLE critica.periodos_letivo(
	critica_id				SERIAL NOT NULL PRIMARY KEY,
	periodo_letivo_id			BIGINT,
	mensagem				VARCHAR(500),
	ocorrencia				TIMESTAMP
);

DROP TABLE msg.periodo_letivo;
CREATE TABLE msg.periodo_letivo(
	mensagem_id				SERIAL NOT NULL PRIMARY KEY,
	perido_letivo_id			BIGINT,
	mensagem				VARCHAR(500),
	ocorrencia				TIMESTAMP
);

CREATE OR REPLACE FUNCTION action.deletar_periodos_letivo_execute() RETURNS trigger AS $$
DECLARE
	temPeriodo_Letivo	BOOLEAN;
BEGIN 
	SELECT COUNT(1) > 0 INTO temPeriodo_Letivo FROM critica.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;

	IF NOT temPeriodo_Letivo THEN
		DELETE FROM escola.periodos_letivo_itens WHERE periodo_letivo_id = NEW.periodo_letivo_id;
		DELETE FROM escola.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;
		
	ELSE
		INSERT INTO msg.periodos_letivo(periodo_letivo_id,mensagem,ocorrencia) SELECT periodo_letivo_id,mensagem,ocorrencia FROM critica.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;
	END IF;

	DELETE FROM action.deletar_periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;

	RETURN NEW;

END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION action.cancelar_critica_periodos_letivo_execute() RETURNS trigger AS $$
BEGIN
	DELETE FROM critica.periodos_letivo WHERE periodo_letivo_id = OLD.periodo_letivo_id; 
	RETURN OLD;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER deletar_periodos_letivo_trigger AFTER INSERT ON action.deletar_periodos_letivo FOR EACH ROW
	EXECUTE PROCEDURE action.deletar_periodos_letivo_execute();

CREATE OR REPLACE FUNCTION action.periodos_letivo_criticar() RETURNS trigger AS $$
BEGIN
	INSERT INTO critica.periodos_letivo(periodo_letivo_id,mensagem,ocorrencia) VALUES(NEW.periodo_letivo_id,'Existe um ou mais vinculos ao periodo letivo, desvincule-o antes de tentar excluir!',now());
	
	RETURN NEW;
END; $$
LANGUAGE 'plpgsql';

DROP TRIGGER periodos_letivo_trigger ON public.periodos_letivo;
CREATE TRIGGER periodos_letivo_trigger AFTER INSERT ON public.periodos_letivo FOR EACH ROW
	EXECUTE PROCEDURE action.periodos_letivo_criticar();
	
	