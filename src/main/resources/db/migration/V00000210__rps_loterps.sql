CREATE TABLE nfse.loterps(
	loterps_id			BIGSERIAL NOT NULL PRIMARY KEY,
	numerolote			VARCHAR(15) NOT NULL,
	cnpj				VARCHAR(14) NOT NULL,
	inscricaomunicipal		VARCHAR(20) NOT NULL,
	quantidaderps			INTEGER NOT NULL
);
