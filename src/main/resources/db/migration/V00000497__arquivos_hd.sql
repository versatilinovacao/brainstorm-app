DROP SCHEMA arquivos CASCADE;

CREATE SCHEMA arquivos;

CREATE TABLE arquivos.hd(
	arquivo_id						BIGSERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(100) NOT NULL,
	extensao						VARCHAR(5) NOT NULL,
	tipo							VARCHAR(10),
	tamanho							BIGINT,
	registro						TIMESTAMP NOT NULL,						
	url								VARCHAR(1000),
	arquivo							TEXT
);
