CREATE VIEW escola.periodo_letivo_caderno_view AS
	SELECT avaliacoes_view.periodo_letivo_item_id,
	       avaliacoes_view.periodo_letivo_nome,
	       avaliacoes_view.caderno_id
	FROM escola.avaliacoes_view
	GROUP BY avaliacoes_view.periodo_letivo_item_id, avaliacoes_view.periodo_letivo_nome, avaliacoes_view.caderno_id
	ORDER BY avaliacoes_view.periodo_letivo_item_id;
