CREATE TABLE tipos_tercerizados(
	tipotercerizado_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO tipos_tercerizados(descricao) VALUES('Fornecedor'),('Representante'),('Financeira'),('Contador'),('Banco');

ALTER TABLE tercerizados
   ADD COLUMN tipotercerizado_id bigint REFERENCES tipos_tercerizados(tipotercerizado_id);
