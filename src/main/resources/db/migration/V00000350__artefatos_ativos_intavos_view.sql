DROP VIEW almoxarifado.artefatos_ativos_view;

CREATE OR REPLACE VIEW almoxarifado.artefatos_ativos_view AS 
 SELECT a.artefato_id,
	a.composicao_id,
	a.nome,
	a.descricao,
	a.preco,
	c.nome AS classificacao,
	t.nome AS tipo,
	s.nome AS artigo,
	a.colaborador_id,
	a.composicao_colaborador_id,
	a.status
   FROM almoxarifado.artefatos a
     JOIN almoxarifado.classificacoes c ON c.classificacao_id = a.classificacao_id
     JOIN almoxarifado.tipos t ON t.tipo_id = a.tipo_id
     JOIN almoxarifado.artigos s ON s.artigo_id = a.artigo_id
  WHERE COALESCE(a.status,true) = true;
  
DROP VIEW almoxarifado.artefatos_inativos_view;

CREATE OR REPLACE VIEW almoxarifado.artefatos_inativos_view AS 
 SELECT a.artefato_id,
	a.composicao_id,
	a.nome,
	a.descricao,
	a.preco,
	c.nome AS classificacao,
	t.nome AS tipo,
	s.nome AS artigo,
	a.colaborador_id,
	a.composicao_colaborador_id,
	a.status
   FROM almoxarifado.artefatos a
     JOIN almoxarifado.classificacoes c ON c.classificacao_id = a.classificacao_id
     JOIN almoxarifado.tipos t ON t.tipo_id = a.tipo_id
     JOIN almoxarifado.artigos s ON s.artigo_id = a.artigo_id
  WHERE COALESCE(a.status,true) = false;