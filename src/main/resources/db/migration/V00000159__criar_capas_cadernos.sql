CREATE TABLE escola.capas_cadernos(
    capacaderno_id                      SERIAL NOT NULL PRIMARY KEY,
    caderno_id                          BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
    titulo                              VARCHAR(100) NOT NULL,
    descricao                           VARCHAR(200),
    apresentacao                        VARCHAR(200)
);