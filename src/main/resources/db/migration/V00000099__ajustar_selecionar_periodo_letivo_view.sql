DROP VIEW public.selecionar_periodo_letivo_view;
CREATE OR REPLACE VIEW escola.selecionar_periodo_letivo_view AS 
 SELECT p.periodo_letivo_id,
    p.descricao,
    p.ano,
    p.status,
    p.empresa_id
   FROM escola.periodos_letivos p
  WHERE p.status = true;