CREATE TABLE escola.orientacoes_avaliacoes(
	orientacao_id					BIGSERIAL NOT NULL PRIMARY KEY,
	avaliacaoalunoatividade_id		BIGINT NOT NULL REFERENCES escola.avaliacao_aluno_atividade(avaliacaoalunoatividade_id),
	descricao						TEXT	
);

CREATE INDEX avaliacoes_orientacoes_idx	ON escola.orientacoes_avaliacoes	USING btree(avaliacaoalunoatividade_id);

DROP TABLE escola.orientacoes_atividades;