ALTER TABLE cursos	ADD COLUMN escola_id				BIGINT;
ALTER TABLE cursos	ADD COLUMN composicao_escola_id		BIGINT;

ALTER TABLE cursos	ADD CONSTRAINT colaborador_fk	FOREIGN KEY(escola_id,composicao_escola_id)	REFERENCES colaborador(colaborador_id,composicao_id);

CREATE INDEX escola_idx ON cursos(escola_id,composicao_escola_id);