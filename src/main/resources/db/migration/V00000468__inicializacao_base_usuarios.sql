DROP TRIGGER dblink_permissoes_gatilho ON permissao;
delete from permissao_por_tela;
delete from gruposecurity_telasecurity;
delete from telasecurity_permissao;
delete from usuario_permissao;
delete from security;
delete from permissao;
delete from grupos_usuarios;
delete from gruposecurity;

SELECT dblink_exec((select findbyserver from findbyserver('seguranca_db')),'DELETE FROM permissao');
SELECT * FROM dblink((select findbyserver from findbyserver('seguranca_db')),'SELECT setval(''permissao_permissao_id_seq'', (SELECT COALESCE(MAX(permissao_id),1) FROM permissao))') AS t(setval BIGINT);

INSERT INTO servers(server_host,server_user,server_password,server_port,server_database) VALUES('127.0.0.1','postgres','default123','5432','contabilidade_db');

CREATE TRIGGER dblink_permissoes_gatilho
  AFTER INSERT OR UPDATE OR DELETE 
  ON permissao
  FOR EACH ROW
  EXECUTE PROCEDURE dblink_permissoes_salvar();

SELECT setval('public.permissao_permissao_id_seq', 1, true);

INSERT INTO permissao(nome,descricao) VALUES('ROLE_COLABORADOR_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_COLABORADOR_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_COLABORADOR_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_USUARIO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_USUARIO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_USUARIO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELASISTEMA_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELASISTEMA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELASISTEMA_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRUPOSECURITY_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRUPOSECURITY_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRUPOSECURITY_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERMISSAO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERMISSAO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERMISSAO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_INICIALIZAPERMISSAO_EXECUTAR','EXECUTAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_LOGRADOURO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_LOGRADOURO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_LOGRADOURO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CNAE_CONTEUDO','COMTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CNAE_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CNAE_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATERIA_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATERIA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATERIA_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARGO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARGO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARGO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_RESPONSAVEL_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_RESPONSAVEL_DELETE','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_RESPONSAVEL_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIDADE_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIDADE_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIDADE_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELEFONE_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELEFONE_DELETE','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TELEFONE_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CADERNO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CADERNO_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CADERNO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CHAMADA_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CHAMADA_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CHAMADA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERIODO_LETIVO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERIODO_LETIVO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PERIODO_LETIVO_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TURMA_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TURMA_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TURMA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CURSO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CURSO_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CURSO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATRICULA_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATRICULA_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_MATRICULA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRADECURRICULAR_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRADECURRICULAR_DELETAR','DELETE');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_GRADECURRICULAR_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ARTEFATO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ARTEFATO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ARTEFATO_DELETE','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_SERVICO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_SERVICO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_SERVICO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTARECEBER_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTARECEBER_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTARECEBER_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARDAPIO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARDAPIO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARDAPIO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PLANOCONTA_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PLANOCONTA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_PLANOCONTA_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARNEPAGAMENTO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARNEPAGAMENTO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CARNEPAGAMENTO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ALUNO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ALUNO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ALUNO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ORDEMSERVICO_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ORDEMSERVICO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ORDEMSERVICO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ATIVIDADE_CONTEUDO','CONTEÚDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ATIVIDADE_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ATIVIDADE_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTRATO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTRATO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_CONTRATO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPORENOVACAO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPORENOVACAO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPORENOVACAO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_INDICE_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_INDICE_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_INDICE_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPODOENCA_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPODOENCA_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPODOENCA_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPOMEDICO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPOMEDICO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_TIPOMEDICO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_COMPOSICAOTURNO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_COMPOSICAOTURNO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_COMPOSICAOTURNO_DELETAR','DELETAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIZACAO_CONTEUDO','CONTEUDO');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIZACAO_SALVAR','SALVAR');
INSERT INTO permissao(nome,descricao) VALUES('ROLE_ESPECIALIZACAO_DELETAR','DELETAR');

--Verifica se usuário owner já esta cadastrado, caso não esteja ele cadastra.
--select * from usuario
INSERT INTO usuario
SELECT * FROM (SELECT 1 AS usuario_id, null::INTEGER AS empresa_id, 'owner'::VARCHAR AS nome, null::date AS nascimento, '$2a$10$zP53hD3PIrYLcP2b6GboMOVWP0cKYVZeo4IrZ4H8y9IDc5ddCxrrm'::VARCHAR AS senha, true AS status, 'owner@insideincloud.com.br'::VARCHAR AS email, true AS isserver) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM usuario WHERE nome = 'owner'
);

--Verifica quais os grupos não existem e cria;
INSERT INTO gruposecurity
SELECT * FROM (SELECT 1,'OWNER'::VARCHAR) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM gruposecurity	WHERE nome = 'OWNER'
);

INSERT INTO gruposecurity
SELECT * FROM (SELECT 2,'ROOT'::VARCHAR) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM gruposecurity	WHERE nome = 'ROOT'
);

INSERT INTO gruposecurity(nome)
SELECT * FROM (SELECT 'CLIENTE'::VARCHAR) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM gruposecurity	WHERE nome = 'CLIENTE'
);

INSERT INTO gruposecurity(nome)
SELECT * FROM (SELECT 'SERVIDOR'::VARCHAR) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM gruposecurity	WHERE nome = 'SERVIDOR'
);

--select * from gruposecurity
INSERT INTO gruposecurity(nome)
SELECT * FROM (SELECT 'CONTABILIDADE'::VARCHAR) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM gruposecurity	WHERE nome = 'CONTABILIDADE'
);

--Vincula o usuário owner ao grupo owner caso ainda não esteja vinculado
INSERT INTO grupos_usuarios 
SELECT * FROM (SELECT 1,1) AS tmp
WHERE NOT EXISTS (
	SELECT usuario_id FROM grupos_usuarios WHERE usuario_id = 1 AND gruposecurity_id = 1
);

--Vincular as permissões por tela
INSERT INTO permissao_por_tela 
SELECT t.telasistema_id,p.permissao_id FROM telas_sistema t
LEFT JOIN permissao p ON p.permissao_id IN (SELECT permissao_id FROM permissao WHERE nome LIKE '%'||UPPER( CASE WHEN t.nome = 'periodo letivo' THEN 'PERIODO%LETIVO' WHEN t.nome = 'grade curricular' THEN 'grade%curricular' WHEN t.nome = 'contas receber' THEN 'conta%receber' WHEN t.nome = 'carnes pagamento' THEN 'carne%pagamento' WHEN t.nome = 'planos de contas' THEN 'planoconta' WHEN t.nome = 'ordem servico' THEN 'ORDEMSERVICO' WHEN t.nome = 'permissoes' THEN 'PERMISSAO' ELSE t.nome END )||'%') 
ORDER BY t.telasistema_id, p.permissao_id;

--SELECT * FROM telasecurity_permissao
INSERT INTO telasecurity_permissao
SELECT t.telasecurity_id, p.permissao_id
FROM telas_security t 
LEFT JOIN telas_sistema ts ON ts.telasistema_id = t.telasistema_id
INNER JOIN permissao p ON p.permissao_id IN (SELECT permissao_id FROM permissao WHERE nome LIKE '%'||UPPER( CASE WHEN ts.nome = 'periodo letivo' THEN 'PERIODO%LETIVO' WHEN ts.nome = 'grade curricular' THEN 'grade%curricular' WHEN ts.nome = 'contas receber' THEN 'conta%receber' WHEN ts.nome = 'carnes pagamento' THEN 'carne%pagamento' WHEN ts.nome = 'planos de contas' THEN 'planoconta' WHEN ts.nome = 'ordem servico' THEN 'ORDEMSERVICO' WHEN ts.nome = 'permissoes' THEN 'PERMISSAO' ELSE ts.nome END )||'%') 
ORDER BY t.telasecurity_id,p.nome;

--select * from security
INSERT INTO security(telasistema_id,permissao_id,gruposecurity_id) 
SELECT t.telasistema_id,permissao_id, 1 AS gruposecurity_id
FROM permissao_por_tela t
WHERE telasistema_id < 6;

INSERT INTO security(telasistema_id,permissao_id,gruposecurity_id) 
SELECT t.telasistema_id,permissao_id, 2 AS gruposecurity_id
FROM permissao_por_tela t
WHERE telasistema_id > 5 OR telasistema_id = 1;

INSERT INTO usuario_permissao(permissao_id,usuario_id)
SELECT permissao_id, 1 AS usuario_id from security where gruposecurity_id = 1;

INSERT INTO gruposecurity_telasecurity(gruposecurity_id,telasecurity_id) 
SELECT (SELECT gruposecurity_id FROM gruposecurity WHERE gruposecurity_id = 2) as gruposecurity_id,
	telasecurity_id 
FROM telas_security
WHERE telasecurity_id > 5 OR telasecurity_id = 1;

INSERT INTO gruposecurity_telasecurity(gruposecurity_id,telasecurity_id) 
SELECT (SELECT gruposecurity_id FROM gruposecurity WHERE gruposecurity_id = 1) as gruposecurity_id,
	telasecurity_id 
FROM telas_security
WHERE telasecurity_id < 6 OR telasecurity_id = 1;

