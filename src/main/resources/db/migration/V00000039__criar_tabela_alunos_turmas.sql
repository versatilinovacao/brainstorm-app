CREATE TABLE public.alunos_turmas(
	turma_id			BIGINT,
	colaborador_id		BIGINT
);

ALTER TABLE public.alunos_turmas ADD CONSTRAINT alunos_turmas_pk PRIMARY KEY(turma_id,colaborador_id);

ALTER TABLE public.alunos_turmas ADD CONSTRAINT turma_fk FOREIGN KEY(turma_id) REFERENCES public.turmas(turma_id);
ALTER TABLE public.alunos_turmas ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);


