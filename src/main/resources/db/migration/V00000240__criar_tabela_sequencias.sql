CREATE TABLE public.sequencias(
	sequencia					VARCHAR(50) NOT NULL PRIMARY KEY,
	sequencia_id				BIGINT NOT NULL
);

CREATE INDEX sequencia_idx
	ON public.sequencias
	USING hash(sequencia);