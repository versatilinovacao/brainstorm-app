CREATE TABLE tipos_responsaveis(
	tiporesponsavel_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO tipos_responsaveis(descricao)	VALUES('Mãe'),('Pai'),('Avó'),('Avô'),('Tio'),('Tia'),('Irmão'),('Irmã'),('Tutor'),('Responsável');

ALTER TABLE responsaveis	ADD tiporesponsavel_id			BIGINT;
ALTER TABLE responsaveis	ADD CONSTRAINT tiporesponsavel_fk	FOREIGN KEY(tiporesponsavel_id)	REFERENCES tipos_responsaveis(tiporesponsavel_id);
CREATE INDEX tiporesponsavel_idx	ON responsaveis	USING btree(tiporesponsavel_id);
