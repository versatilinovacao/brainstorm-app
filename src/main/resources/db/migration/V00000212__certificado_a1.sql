CREATE TABLE empresa.certificados_a1(
	certificado_id				BIGSERIAL NOT NULL,
	empresa_id				BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	path					VARCHAR(500) NOT NULL,
	senha					VARCHAR(250),
	PRIMARY KEY(certificado_id,empresa_id)
);
