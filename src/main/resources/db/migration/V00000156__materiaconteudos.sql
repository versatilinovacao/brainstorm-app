CREATE TABLE escola.materiaconteudos(
	materia_conteudo_id				SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL,
	carga_horaria					NUMERIC(3,2),
	materia_id					BIGINT NOT NULL REFERENCES public.materias(materia_id)
);