ALTER TABLE escola.cadernos ADD COLUMN periodo_letivo_id	BIGINT;
ALTER TABLE escola.cadernos ADD CONSTRAINT periodo_letivo_fk 	FOREIGN KEY(periodo_letivo_id) REFERENCES escola.periodos_letivos(periodo_letivo_id);

CREATE OR REPLACE FUNCTION escola.vinculo_caderno_periodo_letivo() RETURNS trigger AS $$
DECLARE 
	periodo_letivo_v					BIGINT;
	ano_v							VARCHAR(4);
BEGIN
	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT periodo_letivo_id INTO periodo_letivo_v FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;

	NEW.periodo_letivo_id = periodo_letivo_v;
	
	RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER vinculo_caderno_periodo_letivo_tgi BEFORE INSERT ON escola.cadernos FOR EACH ROW EXECUTE PROCEDURE escola.vinculo_caderno_periodo_letivo();
CREATE TRIGGER vinculo_caderno_periodo_letivo_tgu BEFORE UPDATE ON escola.cadernos FOR EACH ROW EXECUTE PROCEDURE escola.vinculo_caderno_periodo_letivo();

