ALTER TABLE public.identificador ADD COLUMN passaporte_numero			VARCHAR(8);
ALTER TABLE public.identificador ADD COLUMN passaporte_datavalidade 	DATE;
ALTER TABLE public.identificador ADD COLUMN naturalidade				VARCHAR(40);
ALTER TABLE public.identificador ADD COLUMN nacionalidade				VARCHAR(40);
ALTER TABLE public.identificador ADD COLUMN militar_numero				VARCHAR(40);
ALTER TABLE public.identificador ADD COLUMN sexo_id						BIGINT;
ALTER TABLE public.identificador ADD COLUMN militar_status				BIGINT;
ALTER TABLE public.identificador ADD COLUMN militar_patente				BIGINT;
ALTER TABLE public.identificador ADD COLUMN militar_unidade				VARCHAR(15);
ALTER TABLE public.identificador ADD COLUMN militar_datadispensa		DATE;
ALTER TABLE public.identificador ADD COLUMN orgaoemissor				VARCHAR(8);
ALTER TABLE public.identificador ADD COLUMN habilitacao_exerceatividade BOOLEAN;
ALTER TABLE public.identificador ADD COLUMN habilitacao_lentecorretiva	BOOLEAN;


