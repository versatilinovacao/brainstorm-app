CREATE OR REPLACE VIEW cargos_ativos_view AS
	SELECT c.* FROM public.cargos c
			  WHERE c.status = true;