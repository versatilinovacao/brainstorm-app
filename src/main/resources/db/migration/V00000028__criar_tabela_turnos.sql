CREATE TABLE public.turnos(
	turno_id					SERIAL NOT NULL,
	composicao_id					BIGINT,
	descricao					VARCHAR(100)
);

ALTER TABLE public.turnos ADD CONSTRAINT turno_pk PRIMARY KEY(turno_id);
ALTER TABLE public.turnos ADD CONSTRAINT turno_composto_fk FOREIGN KEY(composicao_id) REFERENCES public.turnos(turno_id);

INSERT INTO public.turnos(descricao) VALUES('INTEGRAL'),('INTEGRAL'),('INTEGRAL'),('MANHA'),('TARDE'),('NOITE');
INSERT INTO public.turnos(composicao_id,descricao) VALUES(1,'MANHA'),(1,'TARDE'),(1,'NOITE');
INSERT INTO public.turnos(composicao_id,descricao) VALUES(2,'MANHA'),(2,'TARDE');
INSERT INTO public.turnos(composicao_id,descricao) VALUES(3,'TARDE'),(3,'NOITE');
