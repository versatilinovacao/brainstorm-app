DROP VIEW responsaveis_view;

CREATE OR REPLACE VIEW responsaveis_view AS 
 SELECT r.responsavel_id,
    ( SELECT c1.nome
           FROM colaborador c1
          WHERE c1.colaborador_id = rc.responsavel_id) AS nome,
    ( SELECT t_1.numero
           FROM telefones t_1
          WHERE t_1.telefone_id = c.telefone_id) AS telefone,
    rc.colaborador_id,
    c.composicao_id AS composicao_colaborador_id,
    r.tiporesponsavel_id,
    t.descricao AS descricaotiporesponsavel,
    c.email
   FROM responsaveis r
     JOIN responsaveis_colaboradores rc ON rc.responsavel_id = r.responsavel_id
     JOIN colaborador c ON c.colaborador_id = rc.responsavel_id AND c.status = true
     LEFT JOIN tipos_responsaveis t ON t.tiporesponsavel_id = r.tiporesponsavel_id;
