DROP MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view;
				
CREATE MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view AS 
 SELECT a.colaborador_id AS aluno_id,
    a.composicao_id AS composicao_aluno_id,
    c.nome AS nomealuno,
    aa.atividade_id,
    at.avaliacaoalunoatividade_id,
    at.nota,
    at.conceito,
    at.avaliacao_descritiva,
    aa.alunoatividade_id,
    false AS concluido
   FROM escola.alunos a
     JOIN escola.alunos_atividades aa ON aa.aluno_id = a.colaborador_id AND aa.composicao_aluno_id = a.composicao_id
     LEFT JOIN colaborador c ON c.colaborador_id = a.colaborador_id AND c.composicao_id = a.composicao_id
     LEFT JOIN escola.avaliacao_aluno_atividade at ON at.aluno_id = a.colaborador_id AND at.composicao_aluno_id = a.composicao_id AND at.atividade_id = aa.atividade_id
WITH DATA;

ALTER TABLE escola.atividades_alunos_avaliacao_view
  OWNER TO postgres;


--select * from escola.alunos_atividades order by aluno_id,atividade_id
--update escola.alunos_atividades set gradecurricular_id = 18

-- Index: escola.atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx

-- DROP INDEX escola.atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx;

CREATE UNIQUE INDEX atividades_alunos_avaliacao_v_aluno_id_composicao_aluno_id__idx
  ON escola.atividades_alunos_avaliacao_view
  USING btree
  (aluno_id, composicao_aluno_id, atividade_id);

-- Index: escola.atividades_alunos_idx

-- DROP INDEX escola.atividades_alunos_idx;

CREATE INDEX atividades_alunos_idx
  ON escola.atividades_alunos_avaliacao_view
  USING hash
  (nomealuno COLLATE pg_catalog."default");
