DROP VIEW escola.periodo_letivo_caderno_view;
DROP VIEW escola.avaliacoes_view;
DROP VIEW escola.chamadas_view;

CREATE OR REPLACE VIEW escola.chamadas_view AS 
 SELECT c.chamada_id,
    c.caderno_id,
    s.descricao AS nome_caderno,
    c.evento,
    c.aluno_id,
    a.composicao_id AS composicao_aluno_id,
    a.nome AS nome_aluno,
    c.periodo_letivo_item_id,
    c.conceito,
    c.nota,
    c.avaliacao,
    c.status,
    c.acompanhamento
   FROM escola.chamadas c
     JOIN colaborador a ON a.colaborador_id = c.aluno_id
     JOIN escola.cadernos s ON s.caderno_id = c.caderno_id
   ORDER BY a.nome;

     
CREATE OR REPLACE VIEW escola.avaliacoes_view AS 
 SELECT a.avaliacao_id,
    a.periodo_letivo_item_id,
    a.caderno_id,
    a.aluno_id,
    c.composicao_id AS composicao_aluno_id,
    c.nome AS nome_aluno,
    p.descricao AS periodo_letivo_nome,
    escola.faltas(a.caderno_id, a.periodo_letivo_item_id, a.aluno_id) AS faltas,
    escola.frequencia(a.caderno_id, a.periodo_letivo_item_id, a.aluno_id) AS frequencia,
    a.aproveitamento,
    a.conceito,
    ( SELECT sum(chamadas_view.nota) AS sum
           FROM escola.chamadas_view
          WHERE chamadas_view.chamada_id = a.periodo_letivo_item_id AND chamadas_view.aluno_id = chamadas_view.aluno_id) AS nota,
    a.avaliacao
   FROM escola.avaliacoes a
     LEFT JOIN colaborador c ON c.colaborador_id = a.aluno_id
     LEFT JOIN escola.periodos_letivos_itens p ON p.periodo_letivo_item_id = a.periodo_letivo_item_id
   ORDER BY c.nome;

CREATE OR REPLACE VIEW escola.periodo_letivo_caderno_view AS 
 SELECT avaliacoes_view.periodo_letivo_item_id,
    avaliacoes_view.periodo_letivo_nome,
    avaliacoes_view.caderno_id
   FROM escola.avaliacoes_view
  GROUP BY avaliacoes_view.periodo_letivo_item_id, avaliacoes_view.periodo_letivo_nome, avaliacoes_view.caderno_id
  ORDER BY avaliacoes_view.periodo_letivo_item_id;     