CREATE TABLE inscricoes_estaduais(
	inscricaoestadual_id			BIGSERIAL NOT NULL PRIMARY KEY,
	colaborador_id				BIGINT NOT NULL,
	composicao_colaborador_id		BIGINT NOT NULL,
	codigo					VARCHAR(20) NOT NULL UNIQUE,
	FOREIGN KEY(colaborador_id,composicao_colaborador_id) REFERENCES colaborador(colaborador_id,composicao_id)
		ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE VIEW provisor_view AS
SELECT c.colaborador_id AS provisor_id,
       c.composicao_id AS composicao_provisor_id,
       c.nome,
       c.fantasia,
       p.cnpj,
       p.inscricao_municipal,
       i.codigo AS inscricaoestadual,
       id.rg,
       id.cpf
FROM colaborador c
	LEFT JOIN pessoajuridica p ON p.pessoajuridica_id = c.pessoajuridica_id
	LEFT JOIN tercerizados t ON t.tercerizado_id = c.tercerizado_id
	LEFT JOIN inscricoes_estaduais i ON i.inscricaoestadual_id = p.inscricao_estadual_id
	LEFT JOIN identificador id ON id.identificador_id = c.identificador_id
WHERE c.status = true;	