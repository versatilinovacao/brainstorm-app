CREATE TRIGGER atualizar_alunos_valiacoes_tg
  AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
  ON escola.avaliacao_aluno_atividade
  FOR EACH STATEMENT
  EXECUTE PROCEDURE escola.materializar_alunos();