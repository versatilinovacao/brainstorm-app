INSERT INTO regiao(descricao) VALUES('Região Norte'),
									('Região Nordeste'),
									('Região Sudeste'),
									('Região Sul'),
									('Região Centro-Oeste');

--Região Norte  
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(11,'Rondônia','RO',1,1058);
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(12,'Acre','AC',1,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(13,'Amazonas','AM',1,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(14,'Roraima','RR',1,1058);
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(15,'Pará','PA',1,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(16,'Amapá','AP',1,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(17,'Tocantins','TO',1,1058); 

--Região Nordeste 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(21,'Maranhão','MA',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(22,'Piauí','PI',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(23,'Ceará','CE',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(24,'Rio Grande do Norte','RN',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(25,'Paraíba','PB',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(26,'Pernambuco','PE',2,1058);
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(27,'Alagoas','AL',2,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(28,'Sergipe','SE',2,1058);
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(29,'Bahia','BA',2,1058); 

--Região Sudeste 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(31,'Minas Gerais','MG',3,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(32,'Espírito Santo','ES',3,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(33,'Rio de Janeiro','RJ',3,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(35,'São Paulo','SP',3,1058); 

--Região Sul 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(41,'Paraná','PR',4,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(42,'Santa Catarina','SC',4,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(43,'Rio Grande do Sul','RS',4,1058); 

--Região Centro-Oeste 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(50,'Mato Grosso do Sul','MS',5,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(51,'Mato Grosso','MT',5,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(52,'Goiás','GO',5,1058); 
INSERT INTO estados(estado_id,descricao,sigla,regiao_id,pais_id) VALUES(53,'Distrito Federal','DF',5,1058); 


/*
ICMS no Acre – 17%
ICMS em Alagoas – 18%
ICMS no Amazonas – 18%
ICMS no Amapá – 18%
ICMS na Bahia – 18%
ICMS no Ceará – 18%
ICMS no Distrito Federal – 18%
ICMS no Espírito Santo – 17%
ICMS em Goiás -17%
ICMS no Maranhão – 18%
ICMS no Mato Grosso – 17%
ICMS no Mato Grosso do Sul – 17%
ICMS em Minas Gerais – 18%
ICMS no Pará – 17%
ICMS na Paraíba – 18%
ICMS no Paraná – 18%;
ICMS em Pernambuco – 18%
ICMS no Piauí – 18%;
ICMS no Rio Grande do Norte – 18%
ICMS no Rio Grande do Sul – 18%
ICMS no Rio de Janeiro – 20%
ICMS em Rondônia – 17,5%
ICMS em Roraima – 17%
ICMS em Santa Catarina – 17%
ICMS em São Paulo – 18%
ICMS em Sergipe – 18%
ICMS no Tocantins – 18%
*/