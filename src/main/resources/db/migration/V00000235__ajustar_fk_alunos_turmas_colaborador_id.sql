ALTER TABLE public.alunos_turmas ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.alunos_turmas
	ADD CONSTRAINT alunos_colaborador_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_alunos_turmas_colaborador_fk
	ON public.alunos_turmas
	USING btree(colaborador_id,composicao_colaborador_id);
