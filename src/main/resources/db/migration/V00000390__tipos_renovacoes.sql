CREATE TABLE administracao.tipos_renovacoes(
	tiporenovacao_id			SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO administracao.tipos_renovacoes(descricao) VALUES('Automatica'),('Condicionada');