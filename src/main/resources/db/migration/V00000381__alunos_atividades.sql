CREATE TABLE escola.alunos_atividades(
	aluno_id			BIGINT NOT NULL,
	composicao_aluno_id		BIGINT,
	atividade_id			BIGINT NOT NULL REFERENCES escola.atividades(atividade_id),
	PRIMARY KEY(aluno_id,atividade_id)
);

ALTER TABLE escola.alunos_atividades	ADD CONSTRAINT aluno_atividade_fk FOREIGN KEY(aluno_id,composicao_aluno_id) REFERENCES escola.alunos(colaborador_id,composicao_id);
