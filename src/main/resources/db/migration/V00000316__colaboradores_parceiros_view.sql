CREATE OR REPLACE VIEW colaboradores_parceiros_view AS
SELECT 	p.parceiro_id AS id,
	    c.colaborador_id AS parceiro_id,
	    c.composicao_id AS composicao_parceiros_id,
	    c.nome,
	    c.status
FROM parceiros p
INNER JOIN colaborador c ON c.parceiro_id = p.parceiro_id
WHERE c.status = true;