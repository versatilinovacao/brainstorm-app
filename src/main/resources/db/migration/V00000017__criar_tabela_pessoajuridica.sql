CREATE TABLE public.pessoajuridica(
	pessoajuridica_id					SERIAL NOT NULL,
	cnpj								VARCHAR(14),
	inscricao_estadual_id				BIGINT,
	inscricao_municipal					VARCHAR(15),
	cnae_id								BIGINT,
	cae_id								BIGINT,
	colaborador_id						BIGINT
);

ALTER TABLE public.pessoajuridica	ADD CONSTRAINT pessoajuridica_pk	PRIMARY KEY(pessoajuridica_id);
ALTER TABLE public.colaborador 		ADD CONSTRAINT colaborador_fk		FOREIGN KEY(pessoajuridica_id)		REFERENCES	public.pessoajuridica(pessoajuridica_id);

