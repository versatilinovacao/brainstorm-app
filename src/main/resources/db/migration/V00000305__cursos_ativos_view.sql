DROP VIEW escola.cursos_ativos_view;

CREATE OR REPLACE VIEW escola.cursos_ativos_view AS 
 SELECT c.curso_id,
    c.descricao,
    c.competencia_inicial,
    c.competencia_final,
    c.status,
    c.periodo_letivo_id	
   FROM cursos c
  WHERE c.status = true;
