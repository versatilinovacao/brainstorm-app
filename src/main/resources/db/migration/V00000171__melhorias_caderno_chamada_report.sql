CREATE TABLE action.historico_controle(
	historico_controle_id		SERIAL NOT NULL PRIMARY KEY,
	ano							VARCHAR(4)
);

CREATE SCHEMA resultado;
CREATE TABLE resultado.historico(
		chamada_id				INTEGER,
		caderno_id				BIGINT,
		aluno_id				BIGINT,
		um						BOOLEAN,
		dois					BOOLEAN,
		tres					BOOLEAN,
		quatro					BOOLEAN,
		cinco					BOOLEAN,
		seis					BOOLEAN,
		sete					BOOLEAN,
		oito					BOOLEAN,
		nove					BOOLEAN,
		dez						BOOLEAN,
		onze					BOOLEAN,
		doze					BOOLEAN,
		treze					BOOLEAN,
		quatorze				BOOLEAN,
		quinze					BOOLEAN,
		dezesseis				BOOLEAN,
		dezessete				BOOLEAN,
		dezoito					BOOLEAN,
		dezenove				BOOLEAN,
		vinte					BOOLEAN,
		vinteum					BOOLEAN,
		vintedois				BOOLEAN,
		vintetres				BOOLEAN,
		vintequatro				BOOLEAN,
		vintecinco				BOOLEAN,
		vinteseis				BOOLEAN,
		vintesete				BOOLEAN,
		vinteoito				BOOLEAN,
		vintenove				BOOLEAN,
		trinta					BOOLEAN,
		trintaeum				BOOLEAN,
		media					INT,
		conceito				CHAR(3),
		avaliacao				TEXT,
		mes						DOUBLE PRECISION,
		ano						DOUBLE PRECISION		
);

CREATE INDEX idx_historico_caderno_id ON resultado.historico USING hash(caderno_id);
CREATE INDEX idx_historico_aluno_id ON resultado.historico USING hash(aluno_id);
CREATE INDEX idx_historico_chamad_id ON resultado.historico USING hash(chamada_id);
CREATE INDEX idx_unico ON resultado.historico USING btree(chamada_id,caderno_id,aluno_id,mes,ano);

CREATE MATERIALIZED VIEW escola.frequenciasreport AS
	SELECT h.chamada_id,h.caderno_id,h.aluno_id,h.um,h.dois,h.tres,h.quatro,h.cinco,h.seis,h.sete,h.oito,h.nove,h.dez,h.onze,h.doze,h.treze,h.quatorze,h.quinze,h.dezesseis,h.dezessete,h.dezoito,h.dezenove,
	       h.vinte,h.vinteum,h.vintedois,h.vintetres,h.vintequatro,h.vintecinco,h.vinteseis,h.vintesete,h.vinteoito,h.vintenove,h.trinta,h.trintaeum,h.media,h.conceito,h.avaliacao,h.mes,h.ano,
	       s.descricao AS nome_curso,
	       t.descricao AS nome_turma,
	       c.descricao AS nome_caderno,
	       a.nome AS nome_aluno,	
	       mes_por_extenso(h.mes) AS mes_por_extenso
	  FROM resultado.historico h
	  INNER JOIN escola.cadernos c ON c.caderno_id = h.caderno_id
	  INNER JOIN escola.gradecurricular g ON g.gradecurricular_id = c.gradecurricular_id
	  INNER JOIN public.cursos s ON s.curso_id = g.curso_id
	  INNER JOIN public.turmas t ON t.turma_id = g.turma_id
	  INNER JOIN public.colaborador a ON a.colaborador_id = h.aluno_id
	  ORDER BY h.ano,h.mes,nome_curso,nome_turma,nome_aluno;


CREATE INDEX frequenciasreport_ano_idx ON escola.frequenciasreport (ano);
CREATE INDEX frequenciasreport_mes_idx ON escola.frequenciasreport (mes);
CREATE INDEX frequenciasreport_nome_curso_idx ON escola.frequenciasreport (nome_curso);
CREATE INDEX frequenciasreport_nome_turma_idx ON escola.frequenciasreport (nome_turma);
CREATE INDEX frequenciasreport_nome_aluno_idx ON escola.frequenciasreport (nome_aluno);
CREATE INDEX frequenciasreport_caderno_id_idx ON escola.frequenciasreport (caderno_id);
CREATE INDEX frequenciasreport_aluno_id_idx ON escola.frequenciasreport (aluno_id);
CREATE INDEX frequenciasreport_idx ON escola.frequenciasreport (ano,mes,nome_curso,nome_turma,nome_aluno);

CREATE OR REPLACE FUNCTION resultado.inicializar_historico_caderno() RETURNS trigger AS $$
DECLARE 
	cadernos_loop			escola.cadernos%ROWTYPE;
	chamadas_loop			escola.chamadas%ROWTYPE;
	existe					BOOLEAN;
BEGIN 
	DELETE FROM resultado.historico WHERE ano = CAST(NEW.ano AS DOUBLE PRECISION);
	FOR cadernos_loop IN
		SELECT caderno_id,descricao,gradecurricular_id,conceito,nota,escola_id,periodo_letivo_id FROM escola.cadernos 
	LOOP

		FOR chamadas_loop IN
			SELECT chamada_id,caderno_id,evento,aluno_id,periodo_letivo_item_id,nota,avaliacao,status,acompanhamento 
				FROM escola.chamadas WHERE caderno_id = cadernos_loop.caderno_id
		LOOP 
			SELECT count(1) > 0 INTO existe FROM resultado.historico WHERE caderno_id = chamadas_loop.caderno_id
																	   AND aluno_id   = chamadas_loop.aluno_id  
																	   AND mes        = date_part('MONTH',chamadas_loop.evento)
																	   AND ano        = date_part('YEAR',chamadas_loop.evento);
			IF NOT existe THEN														   
				INSERT INTO resultado.historico(chamada_id,caderno_id,aluno_id,mes,ano) VALUES(chamadas_loop.chamada_id,chamadas_loop.caderno_id,chamadas_loop.aluno_id,date_part('MONTH',chamadas_loop.evento),date_part('YEAR',chamadas_loop.evento));
			END IF;
		END LOOP;

	END LOOP;
	
	REFRESH MATERIALIZED VIEW escola.frequenciasreport;
	DELETE FROM action.historico_controle;
	RETURN NEW;
EXCEPTION
	WHEN OTHERS  THEN
		RAISE NOTICE 'ERRO'; 	
END; $$ 
	LANGUAGE plpgsql;

CREATE TRIGGER historico_controle_tgi AFTER INSERT
	ON action.historico_controle
		FOR EACH ROW
			EXECUTE PROCEDURE resultado.inicializar_historico_caderno();

/*

CREATE OR REPLACE FUNCTION resultado.inicializar_historico() RETURNS trigger AS $$
DECLARE
	existe			BOOLEAN;
	executado		BOOLEAN;
BEGIN
	SELECT executar INTO executado FROM controle.historico_controle WHERE historico_controle_id = 1;
	IF executar THEN
		execute inicializar_historico_caderno(date_part('YEAR',NEW.evento));
	END IF;
 
	SELECT count(1) > 0 INTO existe FROM resultado.historico WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	IF NOT existe THEN
		INSERT INTO resultado.historico(caderno_id,aluno_id,mes,ano) VALUES (NEW.caderno_id,NEW.aluno_id,date_part('MONTH',NEW.evento),date_part('YEAR',NEW.evento));
		REFRESH MATERIALIZED VIEW escola.frequenciasreport;
	END IF;

END; $$
	LANGUAGE plpgsql;


CREATE TRIGGER inicializa_historico_tgi AFTER INSERT
	ON escola.chamadas 
		FOR EACH ROW
			EXECUTE PROCEDURE resultado.inicializar_historico();
*/


CREATE OR REPLACE FUNCTION resultado.atualizar_historico_insert() RETURNS trigger AS $$
DECLARE
	existe			BOOLEAN;
	dia_corrente		INTEGER;
	media			INTEGER;
	conceito		CHAR(3);
	avaliacao		TEXT;
BEGIN
	dia_corrente = date_part('DAY',NEW.evento);
	SELECT count(1) > 0 INTO existe FROM resultado.historico WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	IF NOT existe THEN
		INSERT INTO resultado.historico(chamada_id,caderno_id,aluno_id,mes,ano) VALUES (NEW.chamada_id,NEW.caderno_id,NEW.aluno_id,date_part('MONTH',NEW.evento),date_part('YEAR',NEW.evento));
	END IF;
	
	RETURN NEW;
END; $$
	LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resultado.atualizar_historico_update() RETURNS trigger AS $$
DECLARE
	existe			BOOLEAN;
	dia_corrente		INTEGER;
	media			INTEGER;
	conceito		CHAR(3);
	avaliacao		TEXT;
BEGIN
	dia_corrente = date_part('DAY',NEW.evento);
	SELECT count(1) > 0 INTO existe FROM resultado.historico WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	IF NOT existe THEN
		INSERT INTO resultado.historico(chamada_id,caderno_id,aluno_id,mes,ano) VALUES (NEW.chamada_id,NEW.caderno_id,NEW.aluno_id,date_part('MONTH',NEW.evento),date_part('YEAR',NEW.evento));
	END IF;
	
	IF dia_corrente = 1 THEN
		UPDATE resultado.historico SET um = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 2 THEN
		UPDATE resultado.historico SET dois = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 3 THEN
		UPDATE resultado.historico SET tres = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 4 THEN
		UPDATE resultado.historico SET quatro = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 5 THEN
		UPDATE resultado.historico SET cinco = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 6 THEN
		UPDATE resultado.historico SET seis = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 7 THEN
		UPDATE resultado.historico SET sete = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 8 THEN
		UPDATE resultado.historico SET oito = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 9 THEN
		UPDATE resultado.historico SET nove = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 10 THEN
		UPDATE resultado.historico SET dez = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 11 THEN
		UPDATE resultado.historico SET onze = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 12 THEN
		UPDATE resultado.historico SET doze = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 13 THEN
		UPDATE resultado.historico SET treze = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 14 THEN
		UPDATE resultado.historico SET quatorze = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 15 THEN
		UPDATE resultado.historico SET quinze = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 16 THEN
		UPDATE resultado.historico SET dezesseis = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 17 THEN
		UPDATE resultado.historico SET dezessete = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 18 THEN
		UPDATE resultado.historico SET dezoito = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 19 THEN
		UPDATE resultado.historico SET dezenove = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 20 THEN
		UPDATE resultado.historico SET vinte = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 21 THEN
		UPDATE resultado.historico SET vinteum = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 22 THEN
		UPDATE resultado.historico SET vintedois = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 23 THEN
		UPDATE resultado.historico SET vintetres = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 24 THEN
		UPDATE resultado.historico SET vintequatro = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 25 THEN
		UPDATE resultado.historico SET vintecinco = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 26 THEN
		UPDATE resultado.historico SET vinteseis = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 27 THEN
		UPDATE resultado.historico SET vintesete = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 28 THEN
		UPDATE resultado.historico SET vinteoito = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 29 THEN
		UPDATE resultado.historico SET vintenove = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 30 THEN
		UPDATE resultado.historico SET trinta = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	ELSIF dia_corrente = 31 THEN
		UPDATE resultado.historico SET trintaeum = NEW.status WHERE caderno_id = NEW.caderno_id AND aluno_id = NEW.aluno_id AND mes = date_part('MONTH',NEW.evento) AND ano = date_part('YEAR',NEW.evento);
	END IF;
		
	REFRESH MATERIALIZED VIEW escola.frequenciasreport;	
		
	RETURN NEW;
END; $$
	LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION resultado.refresh_atualizar_historico() RETURNS trigger AS $$
BEGIN
	REFRESH MATERIALIZED VIEW escola.frequenciasreport;	
	RETURN NEW;
END; $$ 
	LANGUAGE plpgsql;

CREATE TRIGGER atualizar_historico_tgi AFTER INSERT
	ON escola.chamadas
		FOR EACH ROW
			EXECUTE PROCEDURE resultado.atualizar_historico_insert();

CREATE TRIGGER atualizar_historico_tgu AFTER UPDATE
	ON escola.chamadas
		FOR EACH ROW
			EXECUTE PROCEDURE resultado.atualizar_historico_update();

CREATE TRIGGER refresh_historico_tgi AFTER INSERT OR UPDATE OR DELETE
	ON escola.cadernos
		FOR EACH STATEMENT
			EXECUTE PROCEDURE resultado.refresh_atualizar_historico();

