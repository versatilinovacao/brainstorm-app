INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_CONTRATOMODELO_SALVAR'::VARCHAR AS nome, 'SALVAR'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_SALVAR'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_CONTRATOMODELO_CONTEUDO'::VARCHAR AS nome, 'CONTEUDO'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_CONTEUDO'
);

INSERT INTO permissao(nome,descricao) 
SELECT * FROM (SELECT 'ROLE_CONTRATOMODELO_DELETE'::VARCHAR AS nome, 'DELETE'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_DELETE'
);

INSERT INTO telas_sistema(nome,label,descricao)
SELECT * FROM (SELECT 'contratomodelo'::VARCHAR AS nome, 'Contrato Modelo'::VARCHAR AS label, 'Modelo de Documentos de Contratos'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM telas_sistema WHERE nome = 'contratomodelo'
);






