DROP TABLE escola.atividades_arquivos;
DROP TABLE arquivos.mp4;
CREATE TABLE arquivos.mp4(
	arquivomp4_id					BIGSERIAL NOT NULL PRIMARY KEY,
	usuario_id						BIGINT NOT NULL REFERENCES usuario(usuario_id),
	nome							VARCHAR(100) NOT NULL,
	extensao						VARCHAR(5) NOT NULL,
	tamanho							INTEGER,
	tipo							VARCHAR(40),
	url								VARCHAR(1000),
	arquivo							TEXT
);


CREATE INDEX arquivo_mp4_usuario_idx	ON arquivos.mp4		USING btree(usuario_id);