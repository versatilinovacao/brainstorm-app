INSERT INTO sequencias(sequencia,sequencia_id)	
SELECT 'COLABORADOR'::VARCHAR AS sequencia, 0::BIGINT AS sequencia_id
WHERE NOT EXISTS (
	SELECT sequencia FROM sequencias WHERE sequencia = 'COLABORADOR'
);