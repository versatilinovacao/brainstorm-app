ALTER TABLE escola.atividades ADD COLUMN concluido		BOOLEAN;

CREATE OR REPLACE VIEW escola.atividades_alunos_view AS 
SELECT at.aluno_id, 
       at.composicao_aluno_id,
       a.atividade_id,
       a.gradecurricular_id,
       a.data_atividade,
       a.titulo,
       a.descricao,
       a.avaliacao,
       a.nota,
       a.conceito,
       a.entrega,
       a.concluido,
       a.status        
FROM escola.alunos_atividades at
INNER JOIN escola.atividades a ON a.atividade_id = at.atividade_id
INNER JOIN escola.alunos e ON e.colaborador_id = at.aluno_id AND e.composicao_id = at.composicao_aluno_id;
