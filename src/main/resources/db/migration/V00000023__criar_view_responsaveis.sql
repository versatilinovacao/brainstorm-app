CREATE VIEW responsaveis_view AS 
select 	r.responsavel_id as responsavel_id,
	(select c1.nome from colaborador c1 where c1.colaborador_id = rc.responsavel_id) as nome,
	'' as telefone, rc.colaborador_id as vinculo_id
		from responsaveis r 
			inner join responsaveis_colaboradores rc on rc.responsavel_id = r.responsavel_id
			inner join colaborador c on c.colaborador_id = rc.colaborador_id
