DROP VIEW alunos_view;
CREATE OR REPLACE VIEW alunos_view AS 
SELECT aluno_id,
       composicao_aluno_id,
       colaborador_id,
       composicao_id,
       nome,
       matricula,
       curso_id
FROM (SELECT c.colaborador_id AS aluno_id,
	     c.composicao_id AS composicao_aluno_id,
	     c.colaborador_id,
	     c.composicao_id,
	     c.nome,
	     m.codigo AS matricula,
	     m.curso_id
      FROM colaborador c
      LEFT JOIN cliente cl ON cl.cliente_id = c.cliente_id
      LEFT JOIN matriculas m ON m.colaborador_id = c.colaborador_id and m.composicao_colaborador_id = c.composicao_id
	
      WHERE COALESCE(m.status,true) = true
        AND COALESCE(c.status,true) = true
        AND cl.tipo_cliente_id = (( SELECT tipos_clientes.tipo_cliente_id FROM tipos_clientes WHERE tipos_clientes.descricao::text = 'ALUNO'::text))
) AS alunos_matricula
ORDER BY nome;