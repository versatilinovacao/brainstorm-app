CREATE OR REPLACE VIEW telefones_ativos_view AS 
 SELECT t.telefone_id,
    t.numero,
    t.ramal,
    t.tipo_telefone_id,
    tc.colaborador_id,
    tc.composicao_colaborador_id,
    t.status
   FROM telefones t
     LEFT JOIN telefones_colaboradores tc ON tc.telefone_id = t.telefone_id
  WHERE t.status = true;

CREATE OR REPLACE VIEW telefones_inativos_view AS 
 SELECT t.telefone_id,
    t.numero,
    t.ramal,
    t.tipo_telefone_id,
    tc.colaborador_id,
    tc.composicao_colaborador_id,
    t.status
   FROM telefones t
     LEFT JOIN telefones_colaboradores tc ON tc.telefone_id = t.telefone_id
  WHERE t.status = false;