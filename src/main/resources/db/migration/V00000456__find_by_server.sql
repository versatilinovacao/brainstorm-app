CREATE OR REPLACE FUNCTION findbyserver(VARCHAR) RETURNS VARCHAR AS $$
DECLARE
	conexao		VARCHAR;
BEGIN 
	SELECT 'host='||server_host||' user='||server_user||' password='||server_password||' dbname='||server_database INTO conexao FROM servers WHERE server_database = $1;

	RETURN conexao;
	
END; $$
LANGUAGE plpgsql;