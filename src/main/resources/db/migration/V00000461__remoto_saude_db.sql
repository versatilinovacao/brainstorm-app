INSERT INTO servers(server_host,server_user,server_password,server_port,server_database)
VALUES('127.0.0.1','postgres','default123','5432','saude_db');

CREATE OR REPLACE FUNCTION dblink_doencas() RETURNS SETOF record_doencas AS $$
DECLARE
	reg	record_doencas%ROWTYPE;
	conexao	text;
BEGIN
	SELECT findbyserver INTO conexao FROM findbyserver('cep_db');
	FOR reg IN 
		SELECT doenca_id,nome,tipodoenca_id
		FROM( 
		SELECT * FROM public.dblink (conexao,'select doenca_id,nome,tipodoenca_id from public.doencas')  AS DATA(doenca_id BIGINT, nome VARCHAR, tipodoenca_id BIGINT)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.doencas_view AS 
select doenca_id, nome, tipodoenca_id from dblink_doencas(); 

CREATE OR REPLACE FUNCTION dblink_tipos_medicos() RETURNS SETOF record_tipos_medicos AS $$
DECLARE
	reg	record_tipos_medicos%ROWTYPE;
	conexao	text;
BEGIN
	SELECT findbyserver INTO conexao FROM findbyservers('saude_db');
	FOR reg IN 
		SELECT tipomedico_id,nome
		FROM( 
		SELECT * FROM public.dblink (conexao,'select tipomedico_id,nome from public.tipos_medicos')  AS DATA(tipomedico_id BIGINT, nome VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.tiposmedicos_view AS 
select tipomedico_id, nome from dblink_tipos_medicos(); 
