DROP VIEW escola.caderno_mes_view;
DROP VIEW escola.frequencias;

CREATE OR REPLACE FUNCTION escola.composicao_chamadas()
  RETURNS TABLE(_chamada_id integer, _caderno_id bigint, _aluno_id bigint, _um integer, _dois integer, _tres integer, _quatro integer, _cinco integer, _seis integer, _sete integer, _oito integer, _nove integer, _dez integer, _onze integer, _doze integer, _treze integer, _quatorze integer, _quinze integer, _dezesseis integer, _dezessete integer, _dezoito integer, _dezenove integer, _vinte integer, _vinteum integer, _vintedois integer, _vintetres integer, _vintequatro integer, _vintecinco integer, _vinteseis integer, _vintesete integer, _vinteoito integer, _vintenove integer, _trinta integer, _trintaeum integer, _mes double precision, _ano double precision) AS
$$
DECLARE
	chamadas_in					escola.chamadas%ROWTYPE;
	aluno_corrente					BIGINT;
	dia_corrente					DOUBLE PRECISION;
	mes_corrente					DOUBLE PRECISION;
	ano_corrente					DOUBLE PRECISION;

	existe						BOOLEAN;
	existe_temp					BOOLEAN;
BEGIN 
	SELECT count(1) > 0 INTO existe_temp FROM pg_class WHERE relname = '_retorno' AND relkind = 'r';	
	IF existe_temp THEN
		BEGIN
			DROP TABLE _retorno;
		EXCEPTION WHEN OTHERS THEN
			RAISE NOTICE 'Tabela temporária de retorno não existe.';
		END;
	END IF;
	
	CREATE TEMP TABLE _retorno(
		chamada_id				INTEGER,
		caderno_id				BIGINT,
		aluno_id				BIGINT,
		um					INT,
		dois					INT,
		tres					INT,
		quatro					INT,
		cinco					INT,
		seis					INT,
		sete					INT,
		oito					INT,
		nove					INT,
		dez					INT,
		onze					INT,
		doze					INT,
		treze					INT,
		quatorze				INT,
		quinze					INT,
		dezesseis				INT,
		dezessete				INT,
		dezoito					INT,
		dezenove				INT,
		vinte					INT,
		vinteum					INT,
		vintedois				INT,
		vintetres				INT,
		vintequatro				INT,
		vintecinco				INT,
		vinteseis				INT,
		vintesete				INT,
		vinteoito				INT,
		vintenove				INT,
		trinta					INT,
		trintaeum				INT,
		mes					DOUBLE PRECISION,
		ano					DOUBLE PRECISION		
	);

	CREATE INDEX index_1 ON _retorno USING btree (aluno_id,mes,ano);
	CREATE INDEX index_2 ON _retorno USING btree (caderno_id,aluno_id,mes,ano);

	FOR chamadas_in IN
		SELECT * FROM escola.chamadas
	LOOP	
		dia_corrente = date_part('DAY',chamadas_in.evento);
		
		SELECT count(1) > 0 INTO existe FROM _retorno r WHERE aluno_id = chamadas_in.aluno_id AND r.mes = date_part('MONTH',chamadas_in.evento) AND r.ano = date_part('YEAR',chamadas_in.evento);
		IF (existe) THEN
			IF (dia_corrente = 1) THEN
				UPDATE _retorno 
					SET um         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 2) THEN
				UPDATE _retorno 
					SET dois         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 3) THEN
				UPDATE _retorno 
					SET tres         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 4) THEN
				UPDATE _retorno 
					SET quatro       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 5) THEN
				UPDATE _retorno 
					SET cinco        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 6) THEN
				UPDATE _retorno 
					SET seis         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 7) THEN
				UPDATE _retorno 
					SET sete         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 8) THEN
				UPDATE _retorno 
					SET oito         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 9) THEN
				UPDATE _retorno 
					SET nove         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 10) THEN
				UPDATE _retorno 
					SET dez          = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 11) THEN
				UPDATE _retorno 
					SET onze         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 12) THEN
				UPDATE _retorno 
					SET doze         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 13) THEN
				UPDATE _retorno 
					SET treze        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 14) THEN
				UPDATE _retorno 
					SET quatorze     = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 15) THEN
				UPDATE _retorno 
					SET quinze       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 16) THEN
				UPDATE _retorno 
					SET dezesseis    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 17) THEN
				UPDATE _retorno 
					SET dezessete    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 18) THEN
				UPDATE _retorno 
					SET dezoito      = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 19) THEN
				UPDATE _retorno 
					SET dezenove     = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 20) THEN
				UPDATE _retorno 
					SET vinte        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 21) THEN
				UPDATE _retorno 
					SET vinteum      = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 22) THEN
				UPDATE _retorno 
					SET vintedois    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 23) THEN
				UPDATE _retorno 
					SET vintetres    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 24) THEN
				UPDATE _retorno 
					SET vintequatro  = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 25) THEN
				UPDATE _retorno 
					SET vintecinco   = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 26) THEN
				UPDATE _retorno 
					SET vinteseis    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 27) THEN --SELECT * FROM _retorno WHERE caderno_id = 1 AND aluno_id = 5 AND mes = 2 AND ano = 2019;
				UPDATE _retorno 
					SET vintesete    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 28) THEN
				UPDATE _retorno 
					SET vinteoito    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 29) THEN
				UPDATE _retorno 
					SET vintenove    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 30) THEN
				UPDATE _retorno 
					SET trinta       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 31) THEN
				UPDATE _retorno 
					SET trintaeum    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			END IF;
		ELSE
			aluno_corrente = chamadas_in.aluno_id;
			mes_corrente = date_part('MONTH',chamadas_in.evento); ano_corrente = date_part('YEAR',chamadas_in.evento);
			IF dia_corrente = 1 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,um,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 2 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 3 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,tres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 4 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 5 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,cinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 6 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,seis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 7 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,sete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 8 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,oito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 9 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,nove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 10 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dez,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 11 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,onze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 12 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,doze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 13 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,treze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 14 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatorze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 15 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quinze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 16 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezesseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 17 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezessete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 18 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 19 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 20 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinte,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 21 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 22 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintedois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 23 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintetres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 24 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintequatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 25 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintecinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 26 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 27 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintesete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 28 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 29 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 30 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trinta,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 31 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trintaeum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			END IF;
		
		END IF;
	END LOOP;
	
	RETURN QUERY SELECT chamada_id,
			    caderno_id,
			    aluno_id,
			    COALESCE(um,0),
			    COALESCE(dois,0),
			    COALESCE(tres,0),
			    COALESCE(quatro,0),
			    COALESCE(cinco,0),
			    COALESCE(seis,0),
			    COALESCE(sete,0),
			    COALESCE(oito,0),
			    COALESCE(nove,0),
			    COALESCE(dez,0),
			    COALESCE(onze,0),
			    COALESCE(doze,0),
			    COALESCE(treze,0),
			    COALESCE(quatorze,0),
			    COALESCE(quinze,0),
			    COALESCE(dezesseis,0),
			    COALESCE(dezessete,0),
			    COALESCE(dezoito,0),
			    COALESCE(dezenove,0),
			    COALESCE(vinte,0),
			    COALESCE(vinteum,0),
			    COALESCE(vintedois,0),
			    COALESCE(vintetres,0),
			    COALESCE(vintequatro,0),
			    COALESCE(vintecinco,0),
			    COALESCE(vinteseis,0),
			    COALESCE(vintesete,0),
			    COALESCE(vinteoito,0),
			    COALESCE(vintenove,0),
			    COALESCE(trinta,0),
			    COALESCE(trintaeum,0),
			    mes,
			    ano FROM _retorno;
END; $$
  LANGUAGE plpgsql VOLATILE;

CREATE VIEW escola.frequencias AS
SELECT * FROM 
( SELECT chamadas_report_view._chamada_id,
    chamadas_report_view._caderno_id,
    chamadas_report_view._aluno_id,
    chamadas_report_view.nome_aluno,
    ( SELECT c_1.descricao AS nome_curso
           FROM escola.gradecurricular g
             JOIN cursos c_1 ON c_1.curso_id = g.curso_id
             LEFT JOIN turmas t ON t.turma_id = g.turma_id
          WHERE g.gradecurricular_id = (( SELECT cadernos.gradecurricular_id
                   FROM escola.cadernos
                  WHERE cadernos.caderno_id = chamadas_report_view._caderno_id))) AS nome_curso,
    ( SELECT t.descricao AS nome_turma
           FROM escola.gradecurricular g
             JOIN cursos c_1 ON c_1.curso_id = g.curso_id
             LEFT JOIN turmas t ON t.turma_id = g.turma_id
          WHERE g.gradecurricular_id = (( SELECT cadernos.gradecurricular_id
                   FROM escola.cadernos
                  WHERE cadernos.caderno_id = chamadas_report_view._caderno_id))) AS nome_turma,
    c.descricao AS nome_caderno,
    chamadas_report_view._um,
    chamadas_report_view._dois,
    chamadas_report_view._tres,
    chamadas_report_view._quatro,
    chamadas_report_view._cinco,
    chamadas_report_view._seis,
    chamadas_report_view._sete,
    chamadas_report_view._oito,
    chamadas_report_view._nove,
    chamadas_report_view._dez,
    chamadas_report_view._onze,
    chamadas_report_view._doze,
    chamadas_report_view._treze,
    chamadas_report_view._quatorze,
    chamadas_report_view._quinze,
    chamadas_report_view._dezesseis,
    chamadas_report_view._dezessete,
    chamadas_report_view._dezoito,
    chamadas_report_view._dezenove,
    chamadas_report_view._vinte,
    chamadas_report_view._vinteum,
    chamadas_report_view._vintedois,
    chamadas_report_view._vintetres,
    chamadas_report_view._vintequatro,
    chamadas_report_view._vintecinco,
    chamadas_report_view._vinteseis,
    chamadas_report_view._vintesete,
    chamadas_report_view._vinteoito,
    chamadas_report_view._vintenove,
    chamadas_report_view._trinta,
    chamadas_report_view._trintaeum,
    chamadas_report_view._mes,
    mes_por_extenso(chamadas_report_view._mes) AS mes_por_extenso,
    chamadas_report_view._ano
   FROM escola.chamadas_report_view
     JOIN escola.cadernos c ON c.caderno_id = chamadas_report_view._caderno_id) AS frequencias_result
   ORDER BY _ano,_mes,nome_curso,nome_turma,nome_aluno	;

CREATE OR REPLACE VIEW escola.caderno_mes_view AS 
 SELECT DISTINCT frequencias._caderno_id,
    frequencias.nome_curso,
    frequencias.nome_turma,
    frequencias.nome_caderno,
    frequencias._mes,
    frequencias.mes_por_extenso,
    frequencias._ano
   FROM escola.frequencias;
