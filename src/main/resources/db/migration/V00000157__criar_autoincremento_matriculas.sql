CREATE OR REPLACE FUNCTION matricula_incremento() RETURNS trigger AS $$
DECLARE
	autoinc			BIGINT;
BEGIN
	IF COALESCE(NEW.codigo,'') = '' THEN
		SELECT count(1) + 1 INTO autoinc FROM matriculas;
		NEW.codigo = to_hex(autoinc);
	END IF;

	RETURN NEW;
END;$$ LANGUAGE plpgsql;

CREATE TRIGGER matricula_incremento_tgi BEFORE INSERT ON matriculas FOR EACH ROW EXECUTE PROCEDURE matricula_incremento();