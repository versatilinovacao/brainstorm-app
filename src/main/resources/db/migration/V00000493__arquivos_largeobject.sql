CREATE SCHEMA arquivos;

--DROP TABLE mp4.arquivos
CREATE TABLE arquivos.mp4(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

--DROP TABLE arquivos.png
CREATE TABLE arquivos.png(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

--select * from arquivos.pdf
--DROP TABLE arquivos.pdf
CREATE TABLE arquivos.pdf(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

CREATE TABLE arquivos.word(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

CREATE TABLE arquivos.excel(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

CREATE TABLE arquivos.corel(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

CREATE TABLE arquivos.photoshop(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

CREATE TABLE arquivos.txt(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	arquivo					bytea
);

--DROP FUNCTION remoto.salvar_arquivo(bigint,bigint,varchar,varchar,bytea)
/*
CREATE OR REPLACE FUNCTION arquivos.salvar_arquivo(bigint,bigint,varchar,varchar,varchar) RETURNS bigint AS $$
DECLARE
	conexao		text;
	empresa		BIGINT;
	usuario		BIGINT;
	nome		VARCHAR;
	tipo		VARCHAR;
	arquivo_id	BIGINT;

	comando		TEXT;
BEGIN
	
--	SELECT id INTO arquivo_id FROM ( SELECT extract(day from now()) + extract(month from now()) + extract(year from now()) + extract(hour from now()) + extract(minute from now()) + extract(second from now()) + extract(dow from now()) + COALESCE(empresa,0) + COALESCE(usuario,0) AS id) AS tmp;
	SELECT id INTO arquivo_id FROM ( SELECT extract(day from now()) + extract(month from now()) + extract(year from now()) + extract(hour from now()) + extract(minute from now()) + extract(dow from now()) + COALESCE(empresa,0) + COALESCE(usuario,0) AS id) AS tmp;

	IF UPPER($3) = 'MP4' THEN
		comando = 'INSERT INTO arquivos.mp4(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'DOC' THEN
		comando = 'INSERT INTO arquivos.word(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'EXCEL' THEN
		comando = 'INSERT INTO arquivos.excel(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'PDF' THEN
		comando = 'INSERT INTO arquivos.PDF(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'PNG' THEN
		comando = 'INSERT INTO arquivos.png(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'JPG' THEN
		comando = 'INSERT INTO arquivos.jpg(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	ELSIF UPPER($3) = 'corel' THEN
		comando = 'INSERT INTO arquivos.corel(arquivo_id,nome,registro,extensao,arquivo) VALUES('||arquivo_id||','''||$4||''','''||now()||''','''||$3||''','||lo_import($5)||');';
	END IF;
	
	execute(comando);

	RETURN arquivo_id;
END; $$ LANGUAGE plpgsql;
*/