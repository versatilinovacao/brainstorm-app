CREATE TABLE nfse.identificacao_rps(
	identificacaorps_id			BIGSERIAL NOT NULL PRIMARY KEY,
	numero					VARCHAR(15),
	serie					VARCHAR(5),
	tipo					VARCHAR(1)
);
