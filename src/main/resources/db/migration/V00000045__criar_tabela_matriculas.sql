CREATE TABLE public.situacao_matricula(
	situacao_matricula_id				SERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(50) UNIQUE
);

INSERT INTO public.situacao_matricula(descricao) VALUES ('Matriculado'),('Cancelado'),('Desistencia'),('Transferencia');

CREATE TABLE public.matriculas(
	matricula_id						SERIAL NOT NULL,
	codigo								VARCHAR(30) UNIQUE,
	colaborador_id						BIGINT,
	situacao_matricula_id				BIGINT DEFAULT 1,
	curso_id							BIGINT,
	competencia_inicial					DATE,
	competencia_final					DATE,	
	status								BOOLEAN DEFAULT TRUE--Define se a matricula esta ou não ativa;
);

ALTER TABLE public.matriculas ADD CONSTRAINT matricula_pk PRIMARY KEY(matricula_id);
CREATE UNIQUE INDEX matricula_unica ON public.matriculas(colaborador_id,curso_id,competencia_inicial,competencia_final);

DROP VIEW alunos_matriculados_view;
CREATE VIEW alunos_matriculados_view AS 
	SELECT c.colaborador_id,c.nome,cl.matricula
		FROM colaborador c
		INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id
	WHERE cl.tipo_cliente_id = (SELECT tipo_cliente_id FROM tipos_clientes WHERE descricao = 'ALUNO')
	  AND EXISTS(SELECT 1 FROM public.matriculas WHERE colaborador_id = c.colaborador_id AND status = true);


DROP VIEW alunos_turmas_view;
CREATE VIEW alunos_turmas_view AS
	SELECT a.colaborador_id, a.nome, m.codigo as matricula, t.turma_id as vinculo, s.descricao as status FROM colaborador a
		INNER JOIN alunos_turmas t ON t.colaborador_id = a.colaborador_id
		INNER JOIN public.matriculas m ON m.colaborador_id = a.colaborador_id AND m.status = true
		INNER JOIN public.situacao_matricula s ON s.situacao_matricula_id = m.situacao_matricula_id;