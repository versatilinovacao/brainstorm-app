ALTER TABLE turmas ADD COLUMN status	BOOLEAN DEFAULT true;

CREATE OR REPLACE VIEW escola.turmas_ativas_view AS
	SELECT t.turma_id,
		   t.descricao,
		   t.inicio,
		   t.fim,
		   t.status
		FROM turmas t
		WHERE status = true;
		
CREATE OR REPLACE VIEW escola.turmas_inativas_view AS 
	SELECT t.turma_id,
	       t.descricao,
	       t.inicio,
	       t.fim,
	       t.status
		FROM turmas t 
		WHERE status = false;
		