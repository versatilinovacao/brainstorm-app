CREATE OR REPLACE FUNCTION escola.frequencia(Bigint,Bigint,Bigint) RETURNS NUMERIC AS $$
DECLARE
	caderno			ALIAS FOR $1;
	periodo			ALIAS FOR $2;
	aluno			ALIAS FOR $3;

	presenca		NUMERIC;
	faltas			NUMERIC; 
BEGIN

	SELECT count(1) INTO faltas FROM escola.chamadas WHERE status = false AND caderno_id = caderno AND periodo_letivo_item_id = periodo AND aluno_id = aluno;	
	SELECT count(1) INTO presenca FROM escola.chamadas WHERE caderno_id = caderno AND periodo_letivo_item_id = periodo AND aluno_id = aluno;

	RAISE NOTICE 'TESTE % - % - %',presenca,faltas,(faltas / presenca);
	RETURN ROUND((100-((faltas/presenca)*100)),2);
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION escola.faltas(BigInt,BigInt,Bigint) RETURNS Integer AS $$
DECLARE
	caderno			ALIAS FOR $1;
	periodo			ALIAS FOR $2;
	aluno			ALIAS FOR $3;

	faltas			INTEGER;
BEGIN
	--select * from escola.chamadas
	SELECT count(1) INTO faltas FROM escola.chamadas WHERE status = false AND caderno_id = caderno AND periodo_letivo_item_id = periodo AND aluno_id = aluno;

	RETURN faltas;
END; $$ LANGUAGE plpgsql;

DROP VIEW escola.avaliacoes_view CASCADE;

CREATE OR REPLACE VIEW escola.avaliacoes_view AS
 SELECT a.avaliacao_id,
    a.periodo_letivo_item_id,
    a.caderno_id,
    a.aluno_id,
    c.nome AS nome_aluno,
    p.descricao AS periodo_letivo_nome,
    (escola.faltas(a.caderno_id,a.periodo_letivo_item_id,a.aluno_id)) AS faltas,
    (escola.frequencia(a.caderno_id,a.periodo_letivo_item_id,a.aluno_id) ) AS Frequencia,
    a.aproveitamento,
    a.conceito,
    ( SELECT sum(chamadas_view.nota) AS sum
           FROM escola.chamadas_view
          WHERE chamadas_view.chamada_id = a.periodo_letivo_item_id AND chamadas_view.aluno_id = chamadas_view.aluno_id) AS nota,
    a.avaliacao
   FROM escola.avaliacoes a
     LEFT JOIN colaborador c ON c.colaborador_id = a.aluno_id
     LEFT JOIN escola.periodos_letivos_itens p ON p.periodo_letivo_item_id = a.periodo_letivo_item_id;

CREATE OR REPLACE VIEW escola.periodo_letivo_caderno_view AS 
 SELECT avaliacoes_view.periodo_letivo_item_id,
    avaliacoes_view.periodo_letivo_nome,
    avaliacoes_view.caderno_id
   FROM escola.avaliacoes_view
  GROUP BY avaliacoes_view.periodo_letivo_item_id, avaliacoes_view.periodo_letivo_nome, avaliacoes_view.caderno_id
  ORDER BY avaliacoes_view.periodo_letivo_item_id;

