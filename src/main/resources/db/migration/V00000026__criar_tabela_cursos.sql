CREATE TABLE public.cursos(
	curso_id					SERIAL NOT NULL,
	descricao					VARCHAR(100) UNIQUE
);

ALTER TABLE public.cursos ADD CONSTRAINT curso_pk PRIMARY KEY(curso_id);


