DROP TABLE nfse.rps CASCADE;
CREATE TABLE nfse.rps(
	rps_id						BIGSERIAL NOT NULL,
	empresa_id					BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	filial_id					BIGINT REFERENCES public.colaborador(colaborador_id),	
	tomador_id					BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	prestador_id				BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	dataemissao					DATE NOT NULL,

	numero						VARCHAR(20),
	serie						VARCHAR(20),
	tipo						VARCHAR(20),

	incentivadorcultural		BOOLEAN DEFAULT FALSE,
	optantesimplesnacional		BOOLEAN DEFAULT TRUE,

	codigoobra					VARCHAR(15),
	art							VARCHAR(15),

	status						BOOLEAN DEFAULT FALSE,

	xml_valido					XML,
	xml_cancelado				XML,

	discriminador				VARCHAR(50),
	PRIMARY KEY(rps_id,empresa_id,filial_id)
); 
