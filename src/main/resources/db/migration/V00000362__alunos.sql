CREATE TABLE escola.alunos(
	colaborador_id							BIGINT NOT NULL,
	composicao_id							BIGINT
);

ALTER TABLE escola.alunos	ADD CONSTRAINT aluno_pk	PRIMARY KEY(colaborador_id,composicao_id);

INSERT INTO escola.alunos(colaborador_id,composicao_id)
SELECT colaborador_id,composicao_colaborador_id FROM alunos_turmas;

ALTER TABLE alunos_turmas	DROP CONSTRAINT alunos_colaborador_fk;
ALTER TABLE alunos_turmas	ADD   CONSTRAINT alunos_colaborador_fk FOREIGN KEY (colaborador_id, composicao_colaborador_id)
					REFERENCES escola.alunos (colaborador_id, composicao_id);




