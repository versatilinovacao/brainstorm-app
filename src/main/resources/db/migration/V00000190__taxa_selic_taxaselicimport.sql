CREATE SCHEMA mercadofinanceiro;

CREATE TABLE mercadofinanceiro.taxa_selic_import(
	taxaselicimport_id				SERIAL NOT NULL PRIMARY KEY,
	evento							DATE NOT NULL,				
	taxaanual						NUMERIC(16,4) NOT NULL,
	fatordiario						DOUBLE PRECISION NOT NULL,
	basefinanceiro					DOUBLE PRECISION NOT NULL,
	baseoperacoes					NUMERIC(16,4) NOT NULL,
	media							DOUBLE PRECISION,
	mediana							DOUBLE PRECISION,
	moda							DOUBLE PRECISION,
	desviopadrao					DOUBLE PRECISION,
	indicecurtose					DOUBLE PRECISION
);