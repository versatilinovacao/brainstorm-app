ALTER TABLE public.cursos ADD COLUMN periodo_letivo_id				BIGINT;
ALTER TABLE public.cursos ADD CONSTRAINT periodo_letivo_fk FOREIGN KEY(periodo_letivo_id) REFERENCES public.periodos_letivo(periodo_letivo_id);
 