CREATE SCHEMA kanban;
CREATE SCHEMA suporte;

CREATE TABLE public.parceiros(
    parceiro_id           		SERIAL NOT NULL PRIMARY KEY,
    cbo_id                		BIGINT,
    empresa_id            		BIGINT REFERENCES public.colaborador(colaborador_id) --Indica qual a empresa o colaborador esta ligado
);

ALTER TABLE public.colaborador ADD COLUMN parceiro_id        BIGINT;
ALTER TABLE public.colaborador ADD CONSTRAINT parceiro_fk    FOREIGN KEY(parceiro_id) REFERENCES public.parceiros(parceiro_id);
ALTER TABLE public.cliente ADD COLUMN empresa_id BIGINT; --Indica qual a empresa o colaborador esta ligado

CREATE VIEW kanban.parceiros AS
    SELECT c.colaborador_id,
        c.composicao_id,
        c.nome,
        c.usuario_id,
        c.cep,
        c.identificador_id,
        c.responsavel_id,
        c.telefone_id,
        c.numero,
        c.complemento,
        c.email,
        c.parceiro_id,
        p.cbo_id,
        c.status,
        c.deletado
        FROM public.colaborador c
            INNER JOIN public.parceiros p ON p.parceiro_id = c.parceiro_id;

CREATE TABLE kanban.equipes(
    equipe_id            		SERIAL NOT NULL PRIMARY KEY,
    nome                		VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE kanban.integrantes(
    integrante_id            	SERIAL NOT NULL PRIMARY KEY,
    equipe_id            		BIGINT NOT NULL REFERENCES kanban.equipes(equipe_id),
    parceiro_id            		BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id)
);

CREATE TABLE kanban.administradores(
    administrador_id        	SERIAL NOT NULL PRIMARY KEY,
    nome                		VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE kanban.boards(
    board_id            		SERIAL NOT NULL PRIMARY KEY,
    nome                		VARCHAR(100) NOT NULL UNIQUE,
    descricao            		VARCHAR(500)
);

CREATE TABLE kanban.cards(
    card_id                		SERIAL NOT NULL PRIMARY KEY,
    board_id            		BIGINT NOT NULL REFERENCES kanban.boards(board_id),
    nome                		VARCHAR(100)
);
INSERT INTO kanban.boards(nome,descricao) VALUES('Brainstorm','Controle e desenvolvimento de tarefas voltadas para a plataforama de sistemas On-Line.');
INSERT INTO kanban.cards(board_id,nome) VALUES(1,'Backlog'),(1,'TO DO'),(1,'DO'),(1,'DONE');

CREATE TABLE kanban.prioridades(
    prioridade_id            	SERIAL NOT NULL PRIMARY KEY,
    nome                		VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO kanban.prioridades(nome) VALUES('Critica'),('Urgente'),('Alta'),('Normal'),('Baixa'),('Neutra');

CREATE TABLE kanban.categorias(
    categoria_id            	SERIAL NOT NULL PRIMARY KEY,
    descricao            		VARCHAR(60) NOT NULL
);

INSERT INTO kanban.categorias(descricao) VALUES('Analise'),('Desenvolvimento'),('Teste'),('Homologação'),('Implantação');

CREATE TABLE suporte.chamados(
    chamado_id            		SERIAL NOT NULL PRIMARY KEY,
    chamado_composto_id        	BIGINT REFERENCES suporte.chamados(chamado_id),
    solicitante_id            	BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
    evento_abertura            	TIMESTAMP NOT NULL,
    evento_fechamento        	TIMESTAMP,
    descricao            		TEXT
);

CREATE TABLE suporte.eventos(
    evento_id            		SERIAL NOT NULL PRIMARY KEY,
    chamado_id            		BIGINT NOT NULL REFERENCES suporte.chamados(chamado_id),
    evento                		TIMESTAMP NOT NULL,
    colaborador_id            	BIGINT NOT NULL REFERENCES colaborador(colaborador_id),
    mensagem            		VARCHAR(600) NOT NULL,
    print                		TEXT,
    arquivo                		TEXT
);

CREATE TABLE kanban.tarefas(
    tarefa_id            		SERIAL NOT NULL PRIMARY KEY,
    tarefa_composta_id        	BIGINT REFERENCES kanban.tarefas(tarefa_id),
    tarefa_vinculada_id        	BIGINT REFERENCES kanban.tarefas(tarefa_id),
    card_id                		BIGINT NOT NULL REFERENCES kanban.cards(card_id),
    suporte_id            		BIGINT NOT NULL REFERENCES suporte.chamados(chamado_id),
    categoria_id            	BIGINT NOT NULL REFERENCES kanban.categorias(categoria_id),
    arquivo                		TEXT,
    parceiro_id            		BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
    evento_inicio            	TIMESTAMP NOT NULL,
    evento_fim            		TIMESTAMP,
    prioridade_id            	BIGINT NOT NULL REFERENCES kanban.prioridades(prioridade_id),
    depende_aprovacao        	BOOLEAN
);

CREATE TABLE kanban.tipos(
	tipo_id						SERIAL NOT NULL PRIMARY KEY,
	descricao					VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO kanban.tipos(descricao) VALUES('Aguardando'),('Atendimento'),('Pausa'),('Congelado');

CREATE TABLE kanban.eventos(
	evento_id					SERIAL NOT NULL PRIMARY KEY,
	tarefa_id					BIGINT NOT NULL REFERENCES kanban.tarefas(tarefa_id),
	parceiro_id					BIGINT NOT NULL REFERENCES public.parceiros(parceiro_id),
	tipo_id						BIGINT NOT NULL REFERENCES kanban.tipos(tipo_id), -- Aguardando - Atendimento - Pausa - Congelado - 
	situacao_id					BIGINT, -- | Aguardando Atuação | Aguardando Tarefa Vinculada | Aguardando Analise | Produzindo | Intervalo | Carga Horária Encerrada | Executando Outra Tarefa |
	print						TEXT,
	arquivo						TEXT,
	status						BOOLEAN -- | True | Aberto || False | Fechado |
);

CREATE TABLE kanban.logs(
	log_id						SERIAL NOT NULL PRIMARY KEY,
	tarefa_id					BIGINT NOT NULL REFERENCES kanban.tarefas(tarefa_id),
	evento						TIMESTAMP NOT NULL,
	parceiro_id					BIGINT NOT NULL REFERENCES public.parceiros(parceiro_id),
	descricao					TEXT,
	arquivo						TEXT
);

CREATE TABLE kanban.iniciativas(
	iniciativa_id				SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE,
	descricao					TEXT,
	total_projetos				INT,
	total_concluido				NUMERIC(16,4)
);

CREATE TABLE kanban.times(
	time_id						SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE,
	descricao					VARCHAR(200),
	equipe_id					BIGINT NOT NULL REFERENCES kanban.equipes(equipe_id),
	administrador_id			BIGINT NOT NULL REFERENCES kanban.administradores(administrador_id),
	iniciativa_id				BIGINT REFERENCES kanban.iniciativas(iniciativa_id)
);

CREATE TABLE kanban.projetos(
	projeto_id					SERIAL NOT NULL PRIMARY KEY,
	projeto_composto_id			BIGINT REFERENCES kanban.projetos(projeto_id),
	iniciativa_id				BIGINT NOT NULL REFERENCES kanban.iniciativas(iniciativa_id),
	produto_id					BIGINT REFERENCES recurso.produtos(produto_id), --Vincula o produto ou serviço que deve ser tratado neste projeto
	time_id						BIGINT REFERENCES kanban.times(time_id),
	total_concluido				NUMERIC(16,4)
);