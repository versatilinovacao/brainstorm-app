CREATE VIEW escola.gradecurricular_view AS
SELECT gradecurricular_id,g.turma_id,t.descricao as nome_turma,professor_id,c.nome as nome_professor,g.materia_id, m.descricao as nome_materia,curso_id FROM escola.gradecurricular g
	LEFT JOIN public.colaborador c ON c.colaborador_id = g.professor_id
	LEFT JOIN public.turmas t ON t.turma_id = g.turma_id
	LEFT JOIN public.materias m ON m.materia_id = g.materia_id;
