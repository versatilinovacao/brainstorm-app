CREATE SCHEMA cepaberto;

CREATE TABLE cepaberto.estados(
	estado_id							BIGSERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(100) NOT NULL UNIQUE,
	sigla								VARCHAR(3) NOT NULL UNIQUE
);

INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(1, 'Acre','AC');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(2, 'Alagoas','AL');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(3, 'Amazonas','AM');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(4, 'Amapá','AP');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(5, 'Bahia','BA');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(6, 'Ceara','CE');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(7, 'Distrito Federal','DF');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(8, 'Espirito Santo','ES');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(9, 'Goiás','GO');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(10,'Maranhão','MA');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(11,'Minas Gerais','MG');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(12,'Mato Grosso do Sul','MS');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(13,'Mato Grosso','MT');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(14,'Pará','PA');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(15,'Paraíba','PB');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(16,'Pernambuco','PE');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(17,'Piauí','PI');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(18,'Paraná','PR');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(19,'Rio de Janeiro','RJ');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(20,'Rio Grande do Norte','RN');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(21,'Rondônia','RO');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(22,'Roraima','RR');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(23,'Rio Grande do Sul','RS');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(24,'Santa Catarina','SC');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(25,'Sergipe','SE');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(26,'São Paulo','SP');
INSERT INTO cepaberto.estados(estado_id,descricao,sigla) VALUES(27,'Tocantins','TO');
