CREATE TYPE record_tipos_medicos	AS ( tipomedico_id BIGINT, nome VARCHAR );

CREATE OR REPLACE FUNCTION dblink_tipos_medicos() RETURNS SETOF record_tipos_medicos AS $$
DECLARE
	reg	record_tipos_medicos%ROWTYPE;
BEGIN
	FOR reg IN 
		SELECT tipomedico_id,nome
		FROM( 
		SELECT * FROM public.dblink ('saude_server','select tipomedico_id,nome from public.tipos_medicos')  AS DATA(tipomedico_id BIGINT, nome VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE OR REPLACE VIEW remoto.tiposmedicos_view AS 
select tipomedico_id, nome from dblink_tipos_medicos(); 
