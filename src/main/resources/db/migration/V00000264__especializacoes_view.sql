UPDATE especializacoes 
SET status = true;

CREATE OR REPLACE VIEW especializacoes_ativas_view AS
SELECT *
FROM especializacoes
WHERE status = true;

CREATE OR REPLACE VIEW especializacoes_inativas_view AS
SELECT *
FROM especializacoes
WHERE status = false;