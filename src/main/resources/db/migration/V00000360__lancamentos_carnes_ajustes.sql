/* EXEMPLO:
Ativo 
	Ativo Circulante
		Caixa
			Clientes
				Responsavel 1
				Responsavel 2
				Responsavel 3
		Banco
		Estoque
		Serviços
			Oficina de Futebol
			


Carne
Contas a Receber
Vencimento	Emissao		Pagamento	Devedor		Negociação	PlanoConta
05/03/2020	02/02/2020			Responsavel 2	Carne		


Carnes
Vencimento	Emissao		Devedor		Artefato		PlanoConta		Parcelas	 Valor	  Quitação	R$ Quitação	Escola
05/03/2020	02/02/2020	Responsavel 2	Oficina de Futebol	Oficina de Futebol	      10	100,00	  /  /    			Prosaber Ltda
	Criar tabela auxiliar para registrar AJUSTES de multa, mora, desconto ou abatimento
	Carne ID	Emissão		Escola		Tipo(Multa,Mora,Abatimento,Desconto)

*/

CREATE TABLE financeiro.carnes(
	carne_id							BIGSERIAL NOT NULL PRIMARY KEY,
	vencimento							DATE NOT NULL,
	emissao								DATE NOT NULL,
	pagamento							DATE,
	empresa_id							BIGINT,
	composicao_empresa_id				BIGINT,
	responsavel_id						BIGINT,
	composicao_responsavel_id			BIGINT, 
	artefato_id							BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),
	planoconta_id						BIGINT NOT NULL REFERENCES contabil.planos_contas(planoconta_id),
	parcelas							INTEGER NOT NULL,
	valor								NUMERIC(16,4),
	quitacao							DATE,
	valor_quitacao						NUMERIC(16,4),
	status								BOOLEAN
);

ALTER TABLE financeiro.carnes	ADD CONSTRAINT colaborador_fk	FOREIGN KEY(empresa_id,composicao_empresa_id) REFERENCES colaborador(colaborador_id,composicao_id);
CREATE INDEX empresa_idx			ON financeiro.carnes(empresa_id,composicao_empresa_id);
CREATE INDEX responsavel_idx	ON financeiro.carnes(responsavel_id,composicao_responsavel_id);
CREATE INDEX artefato_idx		ON financeiro.carnes(artefato_id);
CREATE INDEX vencimento_idx		ON financeiro.carnes(vencimento);

CREATE OR REPLACE VIEW financeiro.carnes_ativos_view AS
SELECT c.carne_id,
	   c.vencimento,
	   c.emissao,
	   c.pagamento,
	   c.quitacao,
	   c.parcelas,
	   c.valor,
	   c.valor_quitacao,
	   e.colaborador_id AS empresa_id,
	   e.composicao_id AS composicao_empresa_id,
	   e.nome AS nomeempresa,
	   r.colaborador_id AS responsavel_id,
	   r.composicao_id AS composicao_responsavel_id,
	   r.nome AS nomeresponsavel,
	   a.artefato_id,
	   a.nome AS nomeartefato,
	   p.planoconta_id,
	   p.descricao AS descricaoplanoconta,
	   c.status 

FROM financeiro.carnes c
INNER JOIN colaborador e ON e.colaborador_id = c.empresa_id AND e.composicao_id = c.composicao_empresa_id
INNER JOIN colaborador r ON r.colaborador_id = c.responsavel_id AND e.composicao_id = c.composicao_responsavel_id
INNER JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
LEFT JOIN contabil.planos_contas p ON p.planoconta_id = c.planoconta_id
WHERE COALESCE(c.status,true) = true;

CREATE OR REPLACE VIEW financeiro.carnes_inativos_view AS
SELECT c.carne_id,
	   c.vencimento,
	   c.emissao,
	   c.pagamento,
	   c.quitacao,
	   c.parcelas,
	   c.valor,
	   c.valor_quitacao,
	   e.colaborador_id AS empresa_id,
	   e.composicao_id AS composicao_empresa_id,
	   e.nome AS nomeempresa,
	   r.colaborador_id AS responsavel_id,
	   r.composicao_id AS composicao_responsavel_id,
	   r.nome AS nomeresponsavel,
	   a.artefato_id,
	   a.nome AS nomeartefato,
	   p.planoconta_id,
	   p.descricao AS descricaoplanoconta,
	   c.status 

FROM financeiro.carnes c
INNER JOIN colaborador e ON e.colaborador_id = c.empresa_id AND e.composicao_id = c.composicao_empresa_id
INNER JOIN colaborador r ON r.colaborador_id = c.responsavel_id AND e.composicao_id = c.composicao_responsavel_id
INNER JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
LEFT JOIN contabil.planos_contas p ON p.planoconta_id = c.planoconta_id
WHERE COALESCE(c.status,true) = false;


CREATE TABLE financeiro.tipos_ajustes_carnes(
	tipoajustecarne_id					BIGSERIAL NOT NULL PRIMARY KEY,
	nome								VARCHAR(50) NOT NULL UNIQUE,
	planoconta_id						BIGINT REFERENCES contabil.planos_contas(planoconta_id),
	status							BOOLEAN
);

INSERT INTO financeiro.tipos_ajustes_carnes(nome) VALUES('Multa'),('Mora'),('Desconto'),('Abatimento');

CREATE OR REPLACE VIEW financeiro.tipos_ajustes_carnes_ativos_view AS
SELECT * FROM financeiro.tipos_ajustes_carnes WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW financeiro.tipos_ajustes_carnes_inativos_view AS
SELECT * FROM financeiro.tipos_ajustes_carnes WHERE COALESCE(status,true) = true;

CREATE TABLE financeiro.carnes_ajustes(
	carneajuste_id						BIGSERIAL NOT NULL PRIMARY KEY,
	carne_id							BIGINT NOT NULL REFERENCES financeiro.carnes(carne_id),
	emissao								DATE NOT NULL,
	tipoajustecarne_id					BIGINT NOT NULL REFERENCES financeiro.tipos_ajustes_carnes(tipoajustecarne_id),
	planoconta_id						BIGINT NOT NULL REFERENCES contabil.planos_contas(planoconta_id),
	valor								NUMERIC(16,4),
	status							BOOLEAN
);

CREATE INDEX carne_idx				ON financeiro.carnes_ajustes(carne_id);
CREATE INDEX tipoajustecarne_idx	ON financeiro.carnes_ajustes(tipoajustecarne_id);
CREATE INDEX planoconta_idx			ON financeiro.carnes_ajustes(planoconta_id);
CREATE INDEX emissao_idx			ON financeiro.carnes_ajustes(emissao);

CREATE OR REPLACE VIEW financeiro.carnes_ajustes_ativos_view AS
SELECT a.carneajuste_id,
       a.carne_id,
       a.emissao,
       a.valor,
       t.tipoajustecarne_id,
       t.nome AS nometipoajustecarne,
       p.planoconta_id,
       p.descricao AS descricaoplanoconta,
       a.status
FROM financeiro.carnes_ajustes a
INNER JOIN financeiro.tipos_ajustes_carnes t ON t.tipoajustecarne_id = a.tipoajustecarne_id
LEFT JOIN contabil.planos_contas p ON p.planoconta_id = a.planoconta_id
WHERE COALESCE(a.status,true) = true;

CREATE OR REPLACE VIEW financeiro.carnes_ajustes_inativos_view AS
SELECT a.carneajuste_id,
       a.carne_id,
       a.emissao,
       a.valor,
       t.tipoajustecarne_id,
       t.nome AS nometipoajustecarne,
       p.planoconta_id,
       p.descricao AS descricaoplanoconta,
       a.status
FROM financeiro.carnes_ajustes a
INNER JOIN financeiro.tipos_ajustes_carnes t ON t.tipoajustecarne_id = a.tipoajustecarne_id
LEFT JOIN contabil.planos_contas p ON p.planoconta_id = a.planoconta_id
WHERE COALESCE(a.status,true) = false;