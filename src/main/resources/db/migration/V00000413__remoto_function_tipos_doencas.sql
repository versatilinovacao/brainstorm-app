CREATE OR REPLACE FUNCTION dblink_tiposdoencas()
  RETURNS SETOF record_tiposdoencas AS
$BODY$
DECLARE
	reg	record_tiposdoencas%ROWTYPE;
BEGIN
	FOR reg IN 
		SELECT tipodoenca_id,nome,descricao
		FROM( 
		SELECT * FROM public.dblink ('saude_server','select tipodoenca_id,nome,descricao from public.tipos_doencas')  AS DATA(tipodoenca_id BIGINT, nome VARCHAR, descricao VARCHAR)) AS dados
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION dblink_tiposdoencas()
  OWNER TO postgres;