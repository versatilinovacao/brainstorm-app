ALTER TABLE escola.avaliacoes ADD COLUMN composicao_aluno_id BIGINT;

ALTER TABLE escola.avaliacoes 
	ADD CONSTRAINT aluno_fk FOREIGN KEY(aluno_id,composicao_aluno_id) 
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_avaliacoes_aluno_fk
	ON escola.avaliacoes
	USING btree(aluno_id,composicao_aluno_id);
