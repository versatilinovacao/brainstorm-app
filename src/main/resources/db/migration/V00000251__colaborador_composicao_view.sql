CREATE OR REPLACE VIEW empresa.empresas_composicao_view AS
SELECT c.colaborador_id AS empresa_id,
    c.composicao_id,
    c.nome,
    c.fantasia AS nomefantasia,
    p.cnpj,
    t.numero
   FROM colaborador c
     INNER JOIN empresa.empresa e ON e.empresa_id = c.empresa_id
     LEFT JOIN pessoajuridica p ON p.pessoajuridica_id = c.pessoajuridica_id
     LEFT JOIN telefones t ON t.telefone_id = c.telefone_id
  WHERE c.status = true;
