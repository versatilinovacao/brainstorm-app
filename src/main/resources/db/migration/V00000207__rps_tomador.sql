CREATE TABLE nfse.tomador(
	tomador_id		SERIAL NOT NULL PRIMARY KEY,
	cpf			VARCHAR(11),
	cnpj			VARCHAR(14),
	razaosocial		VARCHAR(115) NOT NULL,
	endereco		VARCHAR(125) NOT NULL,
	numero			VARCHAR(10) NOT NULL,
	complemento		VARCHAR(60),
	bairro			VARCHAR(60) NOT NULL,
	codigomunicipio		VARCHAR(10) NOT NULL,
	uf			VARCHAR(2) NOT NULL,
	cep			VARCHAR(8) NOT NULL,
	telefone		VARCHAR(11),
	email			VARCHAR(80)
);
