DROP VIEW escola.gradecurricular_view;

CREATE OR REPLACE VIEW escola.gradecurricular_view AS 
 SELECT g.gradecurricular_id,
    g.turma_id,
    t.descricao AS nome_turma,
    g.professor_id,
    c.nome AS nome_professor,
    g.materia_id,
    m.descricao AS nome_materia,
    g.curso_id,
    s.descricao AS nome_curso
   FROM escola.gradecurricular g
     LEFT JOIN colaborador c ON c.colaborador_id = g.professor_id
     LEFT JOIN turmas t ON t.turma_id = g.turma_id
     LEFT JOIN materias m ON m.materia_id = g.materia_id
     LEFT JOIN cursos s ON s.curso_id = g.curso_id
     LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = s.periodo_letivo_id
  WHERE COALESCE(s.periodo_letivo_id, 0::bigint) <> 0 AND p.status = true
    AND COALESCE(g.status,true) = true
  ORDER BY s.descricao, t.descricao, m.descricao, c.nome;