SELECT setval('public.telas_sistema_telasistema_id_seq', (SELECT MAX(telasistema_id) FROM telas_sistema), true);

CREATE OR REPLACE FUNCTION dblink_permissoes_salvar()
  RETURNS trigger AS
$BODY$
DECLARE
	conexao		TEXT;
	comando		VARCHAR(250);
	id		BIGINT;
	existe		BOOLEAN;
BEGIN
	select findbyserver into conexao from findbyserver('seguranca_db');

	IF (TG_OP = 'UPDATE') THEN
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			SELECT permissao_id INTO id
			FROM ( 
			SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||OLD.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
			
			comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||COALESCE(NEW.label,'')||''', descricao = '''||COALESCE(NEW.descricao,'')||''' WHERE permissao_id = '||id||';';
		ELSE
			comando = 'INSERT INTO permissao(permissao_id,nome,label,descricao) VALUES('||NEW.permissao_id||','''||NEW.nome||''','''||COALESCE(NEW.label,'')||''','''||NEW.descricao||''');';
		END IF;

	ELSIF (TG_OP = 'INSERT') THEN
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||COALESCE(NEW.label,'')||''', descricao = '''||COALESCE(NEW.descricao,'')||''' WHERE permissao_id = '||id||';';
		ELSE
			comando = 'INSERT INTO permissao(permissao_id,nome,label,descricao) VALUES('||NEW.permissao_id||','''||NEW.nome||''','''||COALESCE(NEW.label,'')||''','''||NEW.descricao||''');';
		END IF;
	ELSE
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			comando = 'DELETE FROM permissao WHERE permissao_id = '||NEW.permissao_id;
		END IF;
	END IF;
	PERFORM dblink_exec(conexao,comando);

	RETURN NEW;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION dblink_permissoes_salvar()
  OWNER TO postgres;

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_AMBIENTEALUNO_CONTEUDO'::VARCHAR AS nome, 'CONTEÚDO'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_AMBIENTEALUNO_CONTEUDO'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_AMBIENTEALUNO_SALVAR'::VARCHAR AS nome, 'SALVAR'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_AMBIENTEALUNO_SALVAR'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_AMBIENTEALUNO_DELETAR'::VARCHAR AS nome, 'DELETAR'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_AMBIENTEALUNO_DELETAR'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_CLIENTE_CONTEUDO'::VARCHAR AS nome, 'CONTEÚDO'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CLIENTE_CONTEUDO'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_CLIENTE_SALVAR'::VARCHAR AS nome, 'SALVAR'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CLIENTE_SALVAR'
);

INSERT INTO permissao(nome,descricao)
SELECT * FROM (SELECT 'ROLE_CLIENTE_DELETAR'::VARCHAR AS nome, 'DELETAR'::VARCHAR AS conteudo) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM permissao WHERE nome = 'ROLE_CLIENTE_DELETAR'
);

INSERT INTO telas_sistema(nome,label,descricao)
SELECT * FROM (SELECT 'ambientealuno'::VARCHAR AS nome ,'Ambiente Aluno'::VARCHAR AS label, 'Ambiente de Acesso Escolar'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM telas_sistema WHERE nome = 'ambientealuno'
);

INSERT INTO telas_sistema(nome,label,descricao)
SELECT * FROM (SELECT 'meucadastro'::VARCHAR AS nome, 'Meu Cadastro'::VARCHAR AS label, 'Informações do Usuário'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS (
	SELECT nome FROM telas_sistema WHERE nome = 'meucadastro'
);
