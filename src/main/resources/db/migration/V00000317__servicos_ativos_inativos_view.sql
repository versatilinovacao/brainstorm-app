ALTER TABLE contabil.servicos	ADD COLUMN status				BOOLEAN;

CREATE OR REPLACE VIEW contabil.servicos_ativos_view AS 
SELECT servico_id,
       descricao,
       referencia
FROM contabil.servicos
WHERE status = true;

CREATE OR REPLACE VIEW contabil.servicos_inativos_view AS 
SELECT servico_id,
       descricao,
       referencia
FROM contabil.servicos
WHERE status = false;