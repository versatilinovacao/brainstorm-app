--DROP TRIGGER contareceber_serial_tg ON financeiro.contas_receber;
DROP TABLE financeiro.contas_pagar CASCADE;
CREATE TABLE financeiro.contas_pagar(
	contapagar_id				BIGSERIAL NOT NULL PRIMARY KEY,
	composicao_id				BIGINT,
	
	empresa_id					BIGINT,
	composicao_empresa_id		BIGINT,
	
	vencimento					DATE NOT NULL,
	emissao						DATE NOT NULL,
	pagamento					DATE,
	quitacao					DATE,
	
	credor_id					BIGINT NOT NULL,
	composicao_credor_id		BIGINT,

	desconto					NUMERIC(16,4),
	valorpago					NUMERIC(16,4),
	saldo						NUMERIC(16,4),
	total						NUMERIC(16,4),
	
	tipopagamento_id			BIGINT NOT NULL REFERENCES financeiro.tipos_pagamentos(tipopagamento_id),
	negociacao_id				BIGINT NOT NULL REFERENCES financeiro.negociacoes(negociacao_id),
	formapagamento_id			BIGINT REFERENCES financeiro.formas_pagamentos(formapagamento_id),
	
	parcela						INTEGER,
	protesto					BOOLEAN,
	
	status						BOOLEAN
		
);

CREATE INDEX ON financeiro.contas_pagar(vencimento);
CREATE INDEX ON financeiro.contas_pagar(pagamento);
CREATE INDEX ON financeiro.contas_pagar(credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(vencimento,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(pagamento,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(emissao,credor_id,composicao_credor_id);
CREATE INDEX ON financeiro.contas_pagar(quitacao,credor_id,composicao_credor_id);

CREATE OR REPLACE VIEW financeiro.contas_pagar_ativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contapagar_id,
	    c.composicao_id,
        c.total,
        c.credor_id,
        c.composicao_credor_id,
	    r.nome AS nomecredor,
        c.parcela,
        c.status
   FROM financeiro.contas_pagar c
INNER JOIN public.colaborador r ON r.colaborador_id = c.credor_id AND r.composicao_id = c.composicao_credor_id
  WHERE COALESCE(c.status, true) = true;

CREATE OR REPLACE VIEW financeiro.contas_pagar_inativas_view AS 
 SELECT c.vencimento,
        c.emissao,
        c.pagamento,
        c.quitacao,
	    c.contapagar_id,
	    c.composicao_id,
        c.total,
        c.credor_id,
        c.composicao_credor_id,
	    r.nome AS nomecredor,
        c.parcela,
        c.status
   FROM financeiro.contas_pagar c
INNER JOIN public.colaborador r ON r.colaborador_id = c.credor_id AND r.composicao_id = c.composicao_credor_id
  WHERE COALESCE(c.status, true) = false;  

CREATE OR REPLACE FUNCTION contareceber_serial() RETURNS trigger AS $$
DECLARE
	maior			BIGINT;
BEGIN
	SELECT COALESCE(MAX(contareceber_id),0) INTO maior FROM financeiro.contas_receber;
	
	NEW.contareceber_id = (maior+1);
	
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER contareceber_serial_tg
	BEFORE INSERT
	ON financeiro.contas_receber
	FOR EACH ROW
	EXECUTE PROCEDURE contareceber_serial();

CREATE OR REPLACE FUNCTION contapagar_serial() RETURNS trigger AS $$
DECLARE
	maior			BIGINT;
BEGIN
	SELECT COALESCE(MAX(contapagar_id),0) INTO maior FROM financeiro.contas_pagar;
	
	NEW.contapagar_id = (maior+1);
	
	RETURN NEW;
END; $$
LANGUAGE plpgsql;

CREATE TRIGGER contapagar_serial_tg
	BEFORE INSERT
	ON financeiro.contas_pagar
	FOR EACH ROW
	EXECUTE PROCEDURE contapagar_serial();


