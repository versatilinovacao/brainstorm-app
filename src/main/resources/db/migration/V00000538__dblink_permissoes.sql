--select * from permissao order by permissao_id desc (select max(permissao_id) from permissao)
INSERT INTO permissao(permissao_id,nome,descricao)
SELECT permissao_id, nome, descricao FROM (SELECT (select max(permissao_id)+1 from permissao)::BIGINT AS permissao_id, 'ROLE_CONTRATOMODELO_CONTEUDO'::VARCHAR AS nome, 'CONTEUDO'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS( 
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_CONTEUDO'
);

INSERT INTO permissao(permissao_id,nome,descricao)
SELECT permissao_id, nome, descricao FROM (SELECT (select max(permissao_id)+1 from permissao)::BIGINT AS permissao_id, 'ROLE_CONTRATOMODELO_SALVAR'::VARCHAR AS nome, 'SALVAR'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS( 
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_SALVAR'
);

INSERT INTO permissao(permissao_id,nome,descricao)
SELECT permissao_id, nome, descricao FROM (SELECT (select max(permissao_id)+1 from permissao)::BIGINT AS permissao_id, 'ROLE_CONTRATOMODELO_DELETAR'::VARCHAR AS nome, 'DELETAR'::VARCHAR AS descricao) AS tmp
WHERE NOT EXISTS( 
	SELECT nome FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_DELETAR'
);