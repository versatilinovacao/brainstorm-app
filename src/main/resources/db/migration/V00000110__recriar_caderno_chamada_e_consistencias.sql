CREATE TABLE public.grupos_consistencias(
	grupo_consistencia_id					SERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO public.grupos_consistencias(descricao) VALUES('Cadernos Escola');

CREATE TABLE public.consistencias(
	consistencia_id						SERIAL NOT NULL PRIMARY KEY,
	nome							VARCHAR(60) NOT NULL,
	descricao						VARCHAR(500) NOT NULL,
	link							VARCHAR(200),
	grupo_consistencia_id					BIGINT NOT NULL REFERENCES public.grupos_consistencias(grupo_consistencia_id)
);

INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Cadastro Período Letivo','Não existe período letivo valido, você precisa acessar o cadastro colaboradores e ir até a ABA empresa para esse ajuste',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Forma do Caderno','Entre no cadastro de colaboradores e va até a ABA empresa e escolha o formato que o caderno deve ter',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Período Letivos','Existe mais de um cadastro de períodos letivos para uma mesma empresa com status de ativo. Você precisara deixar apenas um período por empresa ativo.',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Matricula','Não foram localizados, alunos matriculados e vinculados a uma turma',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Classificação da Empresa','Você precisa configurar o cadastro da sua empresa, antes de continuar',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Critério de Conselho de classe','O critério máximo para liberar a aprovação do aluno por conselho, ainda não foi configurado.',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Cadastro de Curso','Não existe nenhum curso cadastrado para esta escola, cadastre-o antes de continuar.',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Grades do curso','Não existe uma grade formatada corretamente para este curso.',1);
INSERT INTO public.consistencias(nome,descricao,grupo_consistencia_id) VALUES('Cadastro de Turma','Antes de criar o caderno, deve ser montada a turma.',1);


CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item					BIGINT;
	chamada							BIGINT;
	
	inicio_turma					DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	consistencias						public.consistencias%rowtype;
	eventos							TIMESTAMP%TYPE;
	IsPeriodoLetivoNotFind			BOOLEAN;
	IsPeriodoLetivoFind				BOOLEAN;
	IsFormaCaderno					BOOLEAN;
	IsMatriculas					BOOLEAN;
	IsClassificacaoEmpresa 			BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio					BOOLEAN;
	IsGrade						BOOLEAN;
	IsTurmaFormada					BOOLEAN;
BEGIN
	IsError = false;
	
	FOR consistencias IN
		SELECT * FROM public.consistencias WHERE grupo_consistencia_id = 1
	LOOP
		INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
			VALUES(NEW.caderno_id,consistencias.consistencia_id,false);
	END LOOP;

	SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
	SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

	/* Consistência */
	SELECT count(1) = 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) > 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF (NOT IsPeriodoLetivoNotFind OR isPeriodoLetivoFind) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = NEW.caderno_id;
		IsError = true;
	END IF;	
		
	SELECT COALESCE(formato_caderno_id,0) < 1, COALESCE(classificacao_empresa_id,0) < 1 INTO IsFormaCaderno, IsClassificacaoEmpresa, IsCriterio FROM empresa.empresa LIMIT 1;
	IF IsFormaCaderno THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 2 and caderno_id = NEW.caderno_id;
		IsError = true;
	END IF;
	
	IF IsClassificacaoEmpresa THEN 
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 5 and caderno_id = NEW.caderno_id;
		IsError = true;
	END IF;
	
	IF IsCriterio THEN
		UPDATE escola.cadernos_incosnsistencias SET status = true WHERE consistencia_id = 6 and caderno_id = NEW.caderno_id;
		IsError = true;
	END IF;

	SELECT count(1) < 1 INTO IsMatriculas FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsMatriculas) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 4 and caderno_id = NEW.caderno_id;	
		IsError = true;
	END IF;
	
	SELECT count(1) < 1 INTO IsGrade FROM escola.gradecurricular WHERE turma_id = turma;
	IF (IsGrade) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 8 and caderno_id = NEW.caderno_id;
		IsError = true;
	END IF;
	
	SELECT count(1) < 1 INTO IsTurmaFormada FROM public.alunos_turmas WHERE turma_id = turma;
	IF (IsTurmaFormada) THEN
		UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 9 and caderno_id = NEW.caderno_id;
	END IF;

	IF NOT IsError THEN
		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP
			INSERT INTO escola.chamadas(caderno_id,aluno_id)
				VALUES(NEW.caderno_id,alunos.colaborador_id)
					RETURNING chamada_id INTO chamada;
			
			
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
			LOOP
				INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
								VALUES(eventos,NEW.caderno_id,chamada,false);
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;

/*
CREATE OR REPLACE FUNCION escola.alterar_caderno() RETURN trigger AS $$
BEGIN

END; $$ LANGUAGE plpgsql;


CREATE TRIGGER alterar_caderno_tgi AFTER UPDATE
	ON escola.cadernos
		FOR EACH ROW
			EXECUTE PROCEDURE escola.alterar_caderno();

*/