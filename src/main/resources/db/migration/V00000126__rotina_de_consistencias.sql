DROP TRIGGER reprocessar_consistencias_tgi ON public.reprocessar_consistencias;
DROP FUNCTION public.reprocessar_consistencias();
CREATE OR REPLACE FUNCTION public.reprocessar_consistencias() RETURNS trigger AS $$
DECLARE
	isExiste			BOOLEAN;
	unidade_v			BIGINT;
	formato_caderno_v		BIGINT;
	materia_v			BIGINT;
	turma_v				BIGINT;
	curso_v				BIGINT;
	criterio_v			DOUBLE PRECISION;
	cadernos_for			escola.cadernos%ROWTYPE;
	ano_v				VARCHAR(4);
	periodo_letivo_v		BIGINT;
	isExistePeriodoLetivo		INTEGER;
BEGIN
	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT count(1), periodo_letivo_id INTO isExistePeriodoLetivo,periodo_letivo_v FROM escola.periodos_letivos WHERE status = true AND ano = ano_v GROUP BY periodo_letivo_id;
	IF isExistePeriodoLetivo > 0 AND isExistePeriodoLetivo = 1 THEN
		FOR cadernos_for IN
			SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v
		LOOP
			UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
					AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro Período Letivo');
			UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
					AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Período Letivos');

			SELECT COALESCE(unidade_id,0),COALESCE(formato_caderno_id,0),COALESCE(criterio,0) INTO unidade_v,formato_caderno_v,criterio_v FROM empresa.empresa_matriz LIMIT 1;
			IF unidade_v > 0 AND formato_caderno_v > 0 THEN
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
						AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Classificação da Empresa');
			END IF;
			IF criterio_v THEN
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
						AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Critério de Conselho de classe');
			END IF;

			/* Tratar inconsistencias escola */
			IF cadernos_for.gradecurricular_id > 0 THEN
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
						AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Grades do curso');
				SELECT COALESCE(turma_id,0),COALESCE(materia_id,0),COALESCE(curso_id,0) INTO turma_v,materia_v,curso_v FROM escola.gradecurricular WHERE gradecurricular_id = cadernos_for.gradecurricular_id;
				IF curso_v > 0 THEN
					UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
							AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro de Curso');
					--SELECT * FROM public.matriculas
					SELECT count(1) > 0 INTO isExiste FROM public.matriculas WHERE curso_id = curso_v;
					IF isExiste THEN
						UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
							AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Matricula');
					END IF;
				END IF;
				IF turma_v > 0 THEN
					UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
							AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro de Turma');
				END IF;
				IF materia_v > 0 THEN
					UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
							AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Cadastro de Materias');
				END IF;
			END IF;
			
		END LOOP;

	--ELSE	
		/* Adicionar uma inconsistencia para avisar que existe uma das duas situações que ainda não foi resolvida. */
		--IF isExistePeriodoLetivo > 0 AND isExistePeriodo > 0 THEN 
		--END IF;
	END IF;
	
	DELETE FROM public.reprocessar_consistencias WHERE reprocessar_consistencia_id = NEW.reprocessar_consistencia_id;
	RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER reprocessar_consistencias_tgi AFTER INSERT ON public.reprocessar_consistencias FOR EACH ROW EXECUTE PROCEDURE public.reprocessar_consistencias();
