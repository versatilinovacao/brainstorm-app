DROP VIEW alunos_view;

CREATE OR REPLACE VIEW alunos_view AS 
 SELECT alunos_matricula.aluno_id,
    alunos_matricula.composicao_aluno_id,
    alunos_matricula.colaborador_id,
    alunos_matricula.composicao_id,
    alunos_matricula.nome,
    alunos_matricula.cargo_id,
    alunos_matricula.descricao_cargo,
    alunos_matricula.matricula,
    alunos_matricula.curso_id
   FROM ( SELECT c.colaborador_id AS aluno_id,
            c.composicao_id AS composicao_aluno_id,
            c.colaborador_id,
            c.composicao_id,
            c.nome,
	    c.cargo_id,
	    ca.descricao AS descricao_cargo,
            m.codigo AS matricula,
            m.curso_id
           FROM colaborador c
             LEFT JOIN cliente cl ON cl.cliente_id = c.cliente_id
             LEFT JOIN matriculas m ON m.colaborador_id = c.colaborador_id AND m.composicao_colaborador_id = c.composicao_id
	     LEFT JOIN cargos ca ON ca.cargo_id = c.cargo_id
          WHERE COALESCE(c.status, true) = true AND cl.tipo_cliente_id = (( SELECT tipos_clientes.tipo_cliente_id
                   FROM tipos_clientes
                  WHERE tipos_clientes.descricao::text = 'ALUNO'::text))) alunos_matricula
  ORDER BY alunos_matricula.nome;