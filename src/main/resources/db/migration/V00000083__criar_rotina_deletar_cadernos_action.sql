CREATE TABLE critica.cadernos(
	critica_id							SERIAL NOT NULL,
	caderno_id							BIGINT, 
	mensagem							VARCHAR(500),
	ocorrencia							TIMESTAMP		
);

CREATE TABLE msg.cadernos(
	mensagem_id							SERIAL NOT NULL PRIMARY KEY,
	caderno_id							BIGINT,
	mensagem							VARCHAR(500),
	ocorrencia							TIMESTAMP
);

CREATE TABLE action.grupo_cadernos_delete(
	grupo_caderno_delete_id				SERIAL NOT NULL PRIMARY KEY,
	deletar								BOOLEAN
);

CREATE TABLE action.deletar_cadernos(
	caderno_id					BIGINT NOT NULL PRIMARY KEY,
	grupo_caderno_delete_id				BIGINT NOT NULL REFERENCES action.grupo_cadernos_delete(grupo_caderno_delete_id),
	deletar						BOOLEAN
);

CREATE OR REPLACE FUNCTION action.deletar_caderno_execute() RETURNS trigger AS $$
DECLARE 
	isDelete			BOOLEAN;
	isCritica			BOOLEAN;
BEGIN
	IF NEW.deletar THEN
		UPDATE action.grupo_cadernos_delete SET deletar = NEW.deletar WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	END IF;
	
	SELECT deletar INTO isDelete FROM action.grupo_cadernos_delete WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	IF isDelete THEN
		SELECT COUNT(1) > 0 INTO isCritica FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
	
		IF NOT isCritica THEN
			DELETE FROM escola.chamadas_alunos WHERE caderno_id = NEW.caderno_id;
			DELETE FROM escola.chamadas WHERE caderno_id = NEW.caderno_id;
			DELETE FROM escola.cadernos WHERE caderno_id = NEW.caderno_id;
		ELSE
			INSERT INTO msg.cadernos(caderno_id,mensagem,ocorrencia) 
				SELECT caderno_id, mensagem, ocorrencia FROM critica.cadernos WHERE caderno_id = NEW.caderno_id;
		END IF;
		
		DELETE FROM action.deletar_cadernos WHERE grupo_caderno_delete_id = NEW.grupo_caderno_delete_id;
	END IF; 

	RETURN NEW;
END;$$
LANGUAGE plpgsql;

CREATE TRIGGER deletar_caderno_tgi AFTER INSERT
	ON action.deletar_cadernos
		FOR EACH ROW
			EXECUTE PROCEDURE action.deletar_caderno_execute();