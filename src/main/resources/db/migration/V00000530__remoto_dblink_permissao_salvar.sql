CREATE OR REPLACE FUNCTION dblink_permissoes_salvar()
  RETURNS trigger AS
$BODY$
DECLARE
	conexao		TEXT;
	comando		VARCHAR(250);
	id		BIGINT;
	existe		BOOLEAN;
BEGIN
	select findbyserver into conexao from findbyserver('seguranca_db');

	IF (TG_OP = 'UPDATE') THEN
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||OLD.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			SELECT permissao_id INTO id
			FROM ( 
			SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||OLD.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
			
			comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||COALESCE(NEW.label,'')||''', descricao = '''||COALESCE(NEW.descricao,'')||''' WHERE permissao_id = '||id||';';
		ELSE
			comando = 'INSERT INTO permissao(permissao_id,nome,label,descricao) VALUES('||NEW.permissao_id||','''||NEW.nome||''','''||COALESCE(NEW.label,'')||''','''||NEW.descricao||''');';
		END IF;
		
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''' or permissao_id = ''' || NEW.permissao_id || ''';') AS DATA(permissao_id BIGINT)) AS dados;
--		RAISE NOTICE '************************************************************************************************** %, %',NEW.nome, existe;
		IF NOT existe  THEN
			PERFORM dblink_exec(conexao,comando);
		END IF;

	ELSIF (TG_OP = 'INSERT') THEN
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			comando = 'UPDATE permissao SET nome = '''||NEW.nome||''' , label = '''||COALESCE(NEW.label,'')||''', descricao = '''||COALESCE(NEW.descricao,'')||''' WHERE permissao_id = '||id||';';
		ELSE
			comando = 'INSERT INTO permissao(permissao_id,nome,label,descricao) VALUES('||NEW.permissao_id||','''||NEW.nome||''','''||COALESCE(NEW.label,'')||''','''||NEW.descricao||''');';
		END IF;
		PERFORM dblink_exec(conexao,comando);
	ELSE
		SELECT COUNT(permissao_id) > 0 INTO existe FROM (SELECT permissao_id FROM public.dblink (conexao,'select permissao_id from public.permissao where nome = '''||NEW.nome||''';') AS DATA(permissao_id BIGINT)) AS dados;
		
		IF existe THEN
			comando = 'DELETE FROM permissao WHERE permissao_id = '||NEW.permissao_id;
		END IF;
		PERFORM dblink_exec(conexao,comando);
	END IF;

	RETURN NEW;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
