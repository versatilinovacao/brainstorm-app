ALTER TABLE contabil.cnaes ADD COLUMN status 			BOOLEAN DEFAULT true;

CREATE OR REPLACE VIEW contabil.cnaes_secundarios_view AS 
	SELECT * FROM contabil.cnaes 
		WHERE TRIM(codigo) != ''
		AND status = true;
