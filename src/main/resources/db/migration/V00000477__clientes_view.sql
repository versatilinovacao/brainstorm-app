DROP VIEW clientes_view;
CREATE OR REPLACE VIEW clientes_view AS
SELECT c.colaborador_id,
	c.composicao_id,
	c.nome,
	c.foto_real,
	c.foto_thunbnail,
	l.logradouro,
	c.numero,
	c.complemento,
	c.cep,
	l.descricaobairro,
	l.descricaocidade,
	l.descricaoestado,
	l.descricaopais,
	i.cpf,
	i.rg,
	i.certidao,
	i.data_nascimento,
	c.email
	FROM colaborador c
	LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
	LEFT JOIN remoto.logradouros_view l ON l.cep = c.cep;