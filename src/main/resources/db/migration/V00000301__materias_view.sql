DROP VIEW escola.materias_view;

CREATE OR REPLACE VIEW escola.materias_view AS 
 SELECT m.materia_id,
        m.descricao,
        m.tipo_materia_id,
        m.status
   FROM materias m
  WHERE m.status = true
  ORDER BY m.descricao,m.tipo_materia_id;
