ALTER TABLE public.identificador ADD COLUMN foto_thumbnail 							TEXT;

CREATE OR REPLACE VIEW public.colaborador_view AS 
SELECT c.colaborador_id,c.nome,c.fantasia,c.referencia,c.cep,c.email,c.status, i.foto_thumbnail, t.numero as telefone
  FROM colaborador c
INNER JOIN public.identificador i ON i.identificador_id = c.identificador_id
INNER JOIN public.telefones t ON t.telefone_id = c.telefone_id
ORDER BY nome;