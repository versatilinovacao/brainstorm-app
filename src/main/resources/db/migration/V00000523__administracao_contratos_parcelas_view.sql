create or replace view administracao.contratos_parcelas_view AS
select  round(random() * c.devedor_id+date_part('month', c.vencimento)+date_part('year',c.vencimento)) as id,
	c.vencimento,
	c.emissao,
	c.pagamento,
	c.quitacao,
	c.devedor_id,
	c.composicao_devedor_id,
	c.desconto,
	c.valorpago,
	c.saldo,
	c.total,
	c.negociacao_id,
	n.descricao as descricaonegociacao,
	c.formapagamento_id,
	c.parcela,
	c.protesto,
	c.tipopagamento_id,
	t.descricao as descricaotipopagamento,
	c.descricao,
	c.status
from administracao.parcelas_por_contratos p
inner join financeiro.contas_receber c on c.contareceber_id = p.contareceber_id
left join financeiro.negociacoes n on n.negociacao_id = c.negociacao_id
left join financeiro.tipos_pagamentos t on t.tipopagamento_id = c.tipopagamento_id
left join financeiro.formas_pagamentos f on f.formapagamento_id = c.formapagamento_id
where coalesce(c.status,true) = true
order by 2;