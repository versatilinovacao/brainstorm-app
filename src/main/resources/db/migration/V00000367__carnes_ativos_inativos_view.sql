DROP VIEW financeiro.carnes_ativos_view;

CREATE OR REPLACE VIEW financeiro.carnes_ativos_view AS 
 SELECT c.carne_id,
    c.vencimento,
    c.emissao,
    c.pagamento,
    c.quitacao,
    c.parcelas,
    c.valor,
    c.valor_quitacao,
    e.colaborador_id AS empresa_id,
    e.composicao_id AS composicao_empresa_id,
    e.nome AS nomeempresa,
    r.colaborador_id AS responsavel_id,
    r.composicao_id AS composicao_responsavel_id,
    r.nome AS nomeresponsavel,
    a.artefato_id,
    a.nome AS nomeartefato,
    p.planoconta_id,
    p.descricao AS descricaoplanoconta,
    c.status
   FROM financeiro.carnes c
     JOIN colaborador e ON e.colaborador_id = c.empresa_id AND e.composicao_id = c.composicao_empresa_id
     JOIN colaborador r ON r.colaborador_id = c.responsavel_id AND r.composicao_id = c.composicao_responsavel_id
     JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
     LEFT JOIN contabil.planos_contas p ON p.planoconta_id = c.planoconta_id
  WHERE COALESCE(c.status, true) = true;

DROP VIEW financeiro.carnes_inativos_view;

CREATE OR REPLACE VIEW financeiro.carnes_inativos_view AS 
 SELECT c.carne_id,
    c.vencimento,
    c.emissao,
    c.pagamento,
    c.quitacao,
    c.parcelas,
    c.valor,
    c.valor_quitacao,
    e.colaborador_id AS empresa_id,
    e.composicao_id AS composicao_empresa_id,
    e.nome AS nomeempresa,
    r.colaborador_id AS responsavel_id,
    r.composicao_id AS composicao_responsavel_id,
    r.nome AS nomeresponsavel,
    a.artefato_id,
    a.nome AS nomeartefato,
    p.planoconta_id,
    p.descricao AS descricaoplanoconta,
    c.status
   FROM financeiro.carnes c
     JOIN colaborador e ON e.colaborador_id = c.empresa_id AND e.composicao_id = c.composicao_empresa_id
     JOIN colaborador r ON r.colaborador_id = c.responsavel_id AND r.composicao_id = c.composicao_responsavel_id
     JOIN almoxarifado.artefatos a ON a.artefato_id = c.artefato_id
     LEFT JOIN contabil.planos_contas p ON p.planoconta_id = c.planoconta_id
  WHERE COALESCE(c.status, true) = false;
 