DROP TRIGGER criar_caderno_tgi ON escola.cadernos;
DROP TRIGGER criar_caderno_tgu ON escola.cadernos;
DROP TRIGGER refresh_historico_tgi ON escola.cadernos;

DROP FUNCTION escola.criar_caderno();

CREATE OR REPLACE FUNCTION escola.criar_caderno()
  RETURNS trigger AS
$BODY$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item						BIGINT;
	chamada							BIGINT;
	ano_v							VARCHAR(4);
	periodo_letivo_v					BIGINT;
	periodo_letivo_item_v					BIGINT;
	
	inicio_turma						DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	consistencias						public.consistencias%rowtype;
	eventos							TIMESTAMP%TYPE;
	isNotExiste						BOOLEAN;
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	IsFormaCaderno						BOOLEAN;
	IsMatriculas						BOOLEAN;
	IsClassificacaoEmpresa 					BOOLEAN;
	IsError							BOOLEAN;
	IsCriterio						BOOLEAN;
	IsGrade							BOOLEAN;
	IsTurmaFormada						BOOLEAN;
	existe							BOOLEAN;
BEGIN 
	FOR consistencias IN
		SELECT * FROM public.consistencias WHERE grupo_consistencia_id = 1
	LOOP
		SELECT count(1) < 1 INTO existe FROM escola.cadernos_inconsistencias WHERE consistencia_id = consistencias.consistencia_id and caderno_id = NEW.caderno_id;
		IF existe THEN
			INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status)
				VALUES(NEW.caderno_id,consistencias.consistencia_id,true);
		END IF;
	END LOOP;

	IF (TG_OP = 'INSERT') THEN
		INSERT INTO public.reprocessar_consistencias(executar) VALUES(true);
	END IF;

	--select * from escola.cadernos
	--SELECT * FROM escola.cadernos_inconsistencias UPDATE escola.cadernos_inconsistencias SET status = true
	SELECT count(1) > 0 INTO IsError FROM escola.cadernos_inconsistencias  WHERE caderno_id = NEW.caderno_id and status = false;
	IF NOT IsError THEN
		select turma_id, 
		       professor_id, 
		       materia_id,
		       (select MIN(i.periodo_inicial) from escola.periodos_letivos_itens i where i.periodo_letivo_id = p.periodo_letivo_id) as periodo_inicial, 
		       (select MAX(i.periodo_final) from escola.periodos_letivos_itens i where i.periodo_letivo_id = p.periodo_letivo_id) as periodo_final 
		  into turma,professor,materia,inicio_turma,fim_turma
		  from escola.gradecurricular g
		 inner join cursos c on c.curso_id = g.curso_id
		 inner join escola.periodos_letivos p on p.periodo_letivo_id = c.periodo_letivo_id
		 where g.gradecurricular_id = NEW.gradecurricular_id;
		
--		SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
--		SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
		
		DELETE FROM escola.chamadas WHERE caderno_id = NEW.caderno_id;
		DELETE FROM escola.avaliacoes WHERE caderno_id = 14;

		SELECT DATE_PART('year',now()) INTO ano_v;
		FOR alunos IN
			SELECT * FROM public.alunos_turmas WHERE turma_id = turma
		LOOP	
			FOR eventos IN
				SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data 
			LOOP --(SELECT periodo_letivo_item_id FROM escola.periodos_letivos_itens WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE ano = '2018' AND status = true) AND '20181005' between periodo_inicial AND periodo_final),
				SELECT periodo_letivo_item_id INTO periodo_letivo_item_v FROM escola.periodos_letivos_itens WHERE periodo_letivo_id = (SELECT periodo_letivo_id FROM escola.periodos_letivos WHERE periodo_letivo_id = NEW.periodo_letivo_id) AND eventos between periodo_inicial AND periodo_final;
				
				IF (COALESCE(periodo_letivo_item_v,0) > 0) THEN
					INSERT INTO escola.chamadas(evento,caderno_id,aluno_id,periodo_letivo_item_id,status)
									VALUES(eventos,
									       NEW.caderno_id,
									       alunos.colaborador_id,
									       periodo_letivo_item_v,
									       true); --select * from escola.periodos_letivos_itens

					SELECT count(1) < 1 INTO isNotExiste FROM escola.avaliacoes WHERE caderno_id = NEW.caderno_id AND aluno_id = alunos.colaborador_id AND periodo_letivo_item_id = periodo_letivo_item_v;
					IF isNotExiste THEN
						INSERT INTO escola.avaliacoes(caderno_id,aluno_id,periodo_letivo_item_id) VALUES(NEW.caderno_id,alunos.colaborador_id,periodo_letivo_item_v);
					END IF;
				END IF;
			END LOOP;
		END LOOP;
	END IF;

	RETURN NEW;
END; $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION escola.criar_caderno()
  OWNER TO postgres;
  
CREATE TRIGGER criar_caderno_tgi
  AFTER INSERT
  ON escola.cadernos
  FOR EACH ROW
  EXECUTE PROCEDURE escola.criar_caderno();

CREATE TRIGGER criar_caderno_tgu
  AFTER UPDATE
  ON escola.cadernos
  FOR EACH ROW
  EXECUTE PROCEDURE escola.criar_caderno();
  
CREATE TRIGGER refresh_historico_tgi
  AFTER INSERT OR UPDATE OR DELETE
  ON escola.cadernos
  FOR EACH STATEMENT
  EXECUTE PROCEDURE resultado.refresh_atualizar_historico();


