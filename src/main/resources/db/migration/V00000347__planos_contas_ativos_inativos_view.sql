DROP VIEW contabil.planos_contas_arvores_ativas_view;
DROP VIEW contabil.planos_contas_arvores_inativas_view;
DROP VIEW contabil.plano_contas_inativas_view;
DROP VIEW contabil.planos_contas_arvores_view;
DROP VIEW contabil.plano_contas_ativas_view;

ALTER TABLE financeiro.contas_receber DROP CONSTRAINT planoconta_fk;
ALTER TABLE financeiro.contas_pagar DROP CONSTRAINT planoconta_fk;

DROP TABLE contabil.plano_contas ;

CREATE TABLE contabil.planos_contas(
	planoconta_id				BIGSERIAL NOT NULL PRIMARY KEY,
	composicao_id				BIGINT REFERENCES contabil.planos_contas(planoconta_id),
	grupo					VARCHAR(1) UNIQUE,
	descricao				VARCHAR(255) NOT NULL,
	codigo					VARCHAR(8) NOT NULL,
	numeral					INTEGER,
	status					BOOLEAN
);

CREATE INDEX composicao_idx	ON contabil.planos_contas(composicao_id,planoconta_id);

ALTER TABLE financeiro.contas_receber	DROP COLUMN composicao_planoconta_id;
ALTER TABLE financeiro.contas_receber
  ADD CONSTRAINT planoconta_fk FOREIGN KEY (planoconta_id)
      REFERENCES contabil.planos_contas (planoconta_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE financeiro.contas_pagar	DROP COLUMN composicao_planoconta_id;
ALTER TABLE financeiro.contas_pagar
  ADD CONSTRAINT planoconta_fk FOREIGN KEY (planoconta_id)
      REFERENCES contabil.planos_contas (planoconta_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE OR REPLACE VIEW contabil.planos_contas_ativas_view AS 
SELECT * FROM contabil.planos_contas WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW contabil.planos_contas_inativas_view AS 
SELECT * FROM contabil.planos_contas WHERE COALESCE(status,true) = false;

--DROP VIEW contabil.planos_contas_arvores_view; 
/*
CREATE OR REPLACE VIEW contabil.planos_contas_arvores_view AS
SELECT cod AS codigo,descricao,planoconta_id,composicao_id,status FROM (
	WITH RECURSIVE arvore AS (
		SELECT  CAST(nivel AS VARCHAR) AS cod, planoconta_id,composicao_id,descricao,nivel,codigo,numeral,status,CAST(ordenacao AS TEXT) AS ordenacao 
			FROM contabil.plano_contas WHERE composicao_id IS null
		UNION ALL
		SELECT	(CASE
			   WHEN (not p.nivel1 IS null) AND (p.nivel2 IS null) AND (p.nivel3 IS null) AND (p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) 
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (p.nivel3 IS null) AND (p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (p.nivel4 IS null) AND (nivel5 IS null) AND (nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (p.nivel5 IS null) AND (p.nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (nivel7 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (nivel8 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (not nivel8 IS null) AND (nivel9 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR) || '.' || CAST(p.nivel9 AS VARCHAR)
			   WHEN (not p.nivel1 IS null) AND (not p.nivel2 IS null) AND (not p.nivel3 IS null) AND (not p.nivel4 IS null) AND (not nivel5 IS null) AND (not nivel6 IS null) AND (not nivel7 IS null) AND (not nivel8 IS null) AND (not nivel9 IS null) AND (nivel10 IS null) THEN 
				CAST(p.nivel AS VARCHAR) || '.' || CAST(p.nivel1 AS VARCHAR) || '.' || CAST(p.nivel2 AS VARCHAR) || '.' || CAST(p.nivel3 AS VARCHAR) || '.' || CAST(p.nivel4 AS VARCHAR) || '.' || CAST(p.nivel5 AS VARCHAR) || '.' || CAST(p.nivel6 AS VARCHAR) || '.' || CAST(p.nivel7 AS VARCHAR) || '.' || CAST(p.nivel8 AS VARCHAR) || '.' || CAST(p.nivel9 AS VARCHAR) || '.' || CAST(p.nivel10 AS VARCHAR)
			END) AS cod,p.planoconta_id,p.composicao_id,p.descricao,p.nivel,p.codigo,p.numeral,p.status,CAST(a.ordenacao || ' > ' || p.ordenacao AS TEXT) AS ordenacao 
			FROM contabil.plano_contas p
		INNER JOIN arvore a ON p.composicao_id = a.planoconta_id
		--WHERE p.nivel = 2
	) SELECT cod,planoconta_id,composicao_id,descricao,nivel,codigo,numeral,status,arvore.ordenacao FROM arvore order by arvore.ordenacao,arvore.codigo

) AS planos_contas_arvore 
ORDER BY codigo;
*/
--DROP VIEW contabil.planos_contas_arvores_ativas_view
--CREATE OR REPLACE VIEW contabil.planos_contas_arvores_ativas_view AS
--SELECT * FROM contabil.planos_contas_arvores_view
--WHERE COALESCE(status,true) = true ;

--DROP VIEW contabil.planos_contas_arvores_inativas_view
--CREATE OR REPLACE VIEW contabil.planos_contas_arvores_inativas_view AS
--SELECT * FROM contabil.planos_contas_arvores_view
--WHERE COALESCE(status,true) = false;

--select * from contabil.planos_contas_arvores_ativas_view
--Inicializar Plano Conta 
--select * from contabil.planos_contas
--DELETE FROM contabil.planos_contas
INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(1,null,1,1,'COMERCIO',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(2,1,1,1,'ATIVO',true);
		INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(3,2,1,1,'Ativo Circulante',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(4,1,2,1,'PASSIVO',true);
		INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(5,4,1,1,'Passivo Cirtulante',true);
			INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(6,5,1,1,'Fornecedores',true);
			INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(7,5,2,1,'Contas a Pagar',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(8,1,2,1,'DESPESA',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(9,1,2,1,'RECEITA',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(10,1,2,1,'RESULTADO',true);
INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(11,null,2,1,'INDUSTRIA',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(13,11,1,1,'ATIVO',true); 
		INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES((select max(planoconta_id)+1 from contabil.planos_contas),11,1,1,'Ativo Circulante',true);
	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES((select max(planoconta_id)+1 from contabil.planos_contas),11,2,1,'PASSIVO',true);


	INSERT INTO contabil.planos_contas(planoconta_id,composicao_id,codigo,numeral,descricao,status) VALUES(12,null,2,1,'SERVIÇO',true);
