CREATE OR REPLACE VIEW escola.cursos_ativos_view AS
	SELECT c.curso_id,
		   c.descricao,
		   c.competencia_inicial,
		   c.competencia_final,
		   c.status
		FROM cursos c
		WHERE c.status = true;
		
CREATE OR REPLACE VIEW escola.cursos_inativos_view AS
	SELECT c.curso_id,
		   c.descricao,
		   c.competencia_inicial,
		   c.competencia_final,
		   c.status
		FROM cursos c
		WHERE c.status = false;