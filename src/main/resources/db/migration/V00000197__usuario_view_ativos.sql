CREATE OR REPLACE VIEW usuario_view AS 
 SELECT usuario.usuario_id,
    usuario.empresa_id,
    usuario.nome,
    usuario.status,
    usuario.email
   FROM usuario
WHERE usuario.status = true;
