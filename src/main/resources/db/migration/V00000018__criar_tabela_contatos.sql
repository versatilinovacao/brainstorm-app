CREATE TABLE public.tipos_telefones(
	tipo_telefone_id			SERIAL NOT NULL,
	descricao					VARCHAR(60)
);

ALTER TABLE public.tipos_telefones ADD CONSTRAINT tipo_telefone_pk PRIMARY KEY(tipo_telefone_id);

INSERT INTO public.tipos_telefones(descricao) VALUES('PESSOAL'),('PROFISSIONAL'),('RECADO');

CREATE TABLE public.contatos(
	contato_id					SERIAL NOT NULL
);

ALTER TABLE public.contatos ADD CONSTRAINT contato_pk PRIMARY KEY(contato_id);

CREATE TABLE public.contatos_colaboradores(
	contato_id					BIGINT,
	colaborador_id				BIGINT
);

ALTER TABLE public.contatos_colaboradores ADD CONSTRAINT contatos_colaboradores_pk PRIMARY KEY(contato_id,colaborador_id);
ALTER TABLE public.contatos_colaboradores ADD CONSTRAINT contato_fk FOREIGN KEY(contato_id) REFERENCES public.contatos(contato_id);
ALTER TABLE public.contatos_colaboradores ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);
