CREATE OR REPLACE VIEW escola.alunos_view AS
SELECT a.colaborador_id AS aluno_id,
       a.composicao_id AS composicao_aluno_id,
       c.colaborador_id,
       c.composicao_id,
       c.nome,
       c.email,
       i.cpf,
       i.data_nascimento,
       t.numero,
       i.foto_thumbnail,
       i.foto
FROM escola.alunos a
INNER JOIN public.colaborador c ON c.colaborador_id = a.colaborador_id AND c.composicao_id = a.composicao_id
LEFT JOIN public.identificador i ON i.identificador_id = c.identificador_id
LEFT JOIN public.telefones t ON t.telefone_id = c.telefone_id
WHERE COALESCE(c.status,true) = true 
ORDER BY nome
