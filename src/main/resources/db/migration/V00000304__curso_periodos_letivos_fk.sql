ALTER TABLE cursos DROP CONSTRAINT periodo_letivo_fk;
ALTER TABLE cursos
  ADD CONSTRAINT periodo_letivo_fk FOREIGN KEY (periodo_letivo_id)
      REFERENCES escola.periodos_letivos (periodo_letivo_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
