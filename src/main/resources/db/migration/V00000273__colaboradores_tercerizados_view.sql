DROP VIEW colaboradores_tercerizados_view;
CREATE OR REPLACE VIEW colaboradores_tercerizados_view AS
SELECT c.colaborador_id,
	   c.composicao_id,
       c.nome,
       c.fantasia,
       p.cnpj,
       i.cpf
  FROM tipos_tercerizados tt 
 INNER JOIN tercerizados t ON t.tipotercerizado_id = tt.tipotercerizado_id
 INNER JOIN colaborador c ON c.tercerizado_id = t.tercerizado_id
  LEFT JOIN pessoajuridica p ON p.pessoajuridica_id = c.pessoajuridica_id
  LEFT JOIN identificador i ON i.identificador_id = c.identificador_id  
 WHERE tt.tipotercerizado_id = 3 OR tt.tipotercerizado_id = 5;