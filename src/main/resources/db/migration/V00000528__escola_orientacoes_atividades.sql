CREATE TABLE escola.orientacoes_atividades(
	orientacao_id					BIGSERIAL NOT NULL PRIMARY KEY,
	atividade_id					BIGINT NOT NULL REFERENCES escola.atividades(atividade_id),
	descricao						TEXT
);

CREATE INDEX orientacao_atividade_idx		ON escola.orientacoes_atividades	USING btree(atividade_id);