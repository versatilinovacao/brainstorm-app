ALTER TABLE escola.alunos_atividades	ADD COLUMN gradecurricular_id		BIGINT;
ALTER TABLE escola.alunos_atividades	ADD CONSTRAINT gradecurricular_idx	FOREIGN KEY(gradecurricular_id)	REFERENCES escola.gradecurricular(gradecurricular_id);
CREATE INDEX gradecurricular_idx	ON escola.alunos_atividades	USING btree(gradecurricular_id);
