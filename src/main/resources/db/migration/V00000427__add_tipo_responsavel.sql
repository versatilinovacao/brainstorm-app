ALTER TABLE responsaveis DROP tiporesponsavel_id;
ALTER TABLE responsaveis_colaboradores		ADD tiporesponsavel_id		BIGINT;
ALTER TABLE responsaveis_colaboradores		ADD CONSTRAINT tiporesponsavel_fk	FOREIGN KEY(tiporesponsavel_id) 	REFERENCES tipos_responsaveis(tiporesponsavel_id);
CREATE INDEX tiporesponsavel_idx	ON responsaveis_colaboradores	USING btree(tiporesponsavel_id);
