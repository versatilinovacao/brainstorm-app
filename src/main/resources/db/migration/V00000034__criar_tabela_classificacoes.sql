CREATE SCHEMA configuracao;
CREATE TABLE configuracao.classificacoes(
	classificacao_id						SERIAL NOT NULL,
	descricao								VARCHAR(40) UNIQUE,
	valor									BOOLEAN DEFAULT FALSE
);

ALTER TABLE configuracao.classificacoes ADD CONSTRAINT classificacao_pk PRIMARY KEY(classificacao_id);

INSERT INTO configuracao.classificacoes(descricao) VALUES('Empresa'),('Cliente'),('Terceirizado'),('Parceiro');

CREATE TABLE configuracao.colaboradores_classificacoes(
	colaborador_id							BIGINT,
	classificacao_id						BIGINT
);

ALTER TABLE configuracao.colaboradores_classificacoes ADD CONSTRAINT colaboradores_classificacoes_pk PRIMARY KEY(colaborador_id,classificacao_id);
ALTER TABLE configuracao.colaboradores_classificacoes ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id) REFERENCES public.colaborador(colaborador_id);
ALTER TABLE configuracao.colaboradores_classificacoes ADD CONSTRAINT classificacao_fk FOREIGN KEY(classificacao_id) REFERENCES configuracao.classificacoes(classificacao_id);