CREATE OR REPLACE FUNCTION public.status(BOOLEAN) RETURNS INTEGER AS $$
DECLARE 
	resultado			INTEGER;
BEGIN 
	IF ($1 = TRUE) THEN
		resultado = 1;
	ELSIF ($1 = FALSE) THEN
		resultado = 2;
	ELSE 
		resultado = 0;
	END IF;

	RETURN resultado;
END;$$ 
LANGUAGE 'plpgsql';

DROP VIEW escola.chamadas_report_view;
DROP FUNCTION escola.composicao_chamadas();

CREATE OR REPLACE FUNCTION escola.composicao_chamadas()
  RETURNS TABLE(_chamada_id integer, _caderno_id bigint, _aluno_id bigint, _um int, _dois int, _tres int, _quatro int, _cinco int, _seis int, _sete int, _oito int, _nove int, _dez int, _onze int, _doze int, _treze int, _quatorze int, _quinze int, _dezesseis int, _dezessete int, _dezoito int, _dezenove int, _vinte int, _vinteum int, _vintedois int, _vintetres int, _vintequatro int, _vintecinco int, _vinteseis int, _vintesete int, _vinteoito int, _vintenove int, _trinta int, _trintaeum int, _mes double precision, _ano double precision) AS
$BODY$
DECLARE
	chamadas_in					escola.chamadas%ROWTYPE;
	aluno_corrente					BIGINT;
	dia_corrente					DOUBLE PRECISION;
	mes_corrente					DOUBLE PRECISION;
	ano_corrente					DOUBLE PRECISION;

	existe						BOOLEAN;
	existe_temp					BOOLEAN;
BEGIN 
	SELECT count(1) > 0 INTO existe_temp FROM pg_class WHERE relname = '_retorno' AND relkind = 'r';	
	IF existe_temp THEN
		BEGIN
			DROP TABLE _retorno;
		EXCEPTION WHEN OTHERS THEN
			RAISE NOTICE 'Tabela temporária de retorno não existe.';
		END;
	END IF;
	
	CREATE TEMP TABLE _retorno(
		chamada_id				INTEGER,
		caderno_id				BIGINT,
		aluno_id				BIGINT,
		um					INT,
		dois					INT,
		tres					INT,
		quatro					INT,
		cinco					INT,
		seis					INT,
		sete					INT,
		oito					INT,
		nove					INT,
		dez					INT,
		onze					INT,
		doze					INT,
		treze					INT,
		quatorze				INT,
		quinze					INT,
		dezesseis				INT,
		dezessete				INT,
		dezoito					INT,
		dezenove				INT,
		vinte					INT,
		vinteum					INT,
		vintedois				INT,
		vintetres				INT,
		vintequatro				INT,
		vintecinco				INT,
		vinteseis				INT,
		vintesete				INT,
		vinteoito				INT,
		vintenove				INT,
		trinta					INT,
		trintaeum				INT,
		mes					DOUBLE PRECISION,
		ano					DOUBLE PRECISION		
	);

	FOR chamadas_in IN
		SELECT * FROM escola.chamadas
	LOOP	
		dia_corrente = date_part('DAY',chamadas_in.evento);
		
		SELECT count(1) > 0 INTO existe FROM _retorno r WHERE aluno_id = chamadas_in.aluno_id AND r.mes = date_part('MONTH',chamadas_in.evento) AND r.ano = date_part('YEAR',chamadas_in.evento);
		IF (existe) THEN
			IF (dia_corrente = 1) THEN
				UPDATE _retorno 
					SET um         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 2) THEN
				UPDATE _retorno 
					SET dois         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 3) THEN
				UPDATE _retorno 
					SET tres         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 4) THEN
				UPDATE _retorno 
					SET quatro       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 5) THEN
				UPDATE _retorno 
					SET cinco        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 6) THEN
				UPDATE _retorno 
					SET seis         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 7) THEN
				UPDATE _retorno 
					SET sete         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 8) THEN
				UPDATE _retorno 
					SET oito         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 9) THEN
				UPDATE _retorno 
					SET nove         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 10) THEN
				UPDATE _retorno 
					SET dez          = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 11) THEN
				UPDATE _retorno 
					SET onze         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 12) THEN
				UPDATE _retorno 
					SET doze         = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 13) THEN
				UPDATE _retorno 
					SET treze        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 14) THEN
				UPDATE _retorno 
					SET quatorze     = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 15) THEN
				UPDATE _retorno 
					SET quinze       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 16) THEN
				UPDATE _retorno 
					SET dezesseis    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 17) THEN
				UPDATE _retorno 
					SET dezessete    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 18) THEN
				UPDATE _retorno 
					SET dezoito      = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 19) THEN
				UPDATE _retorno 
					SET dezenove     = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 20) THEN
				UPDATE _retorno 
					SET vinte        = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 21) THEN
				UPDATE _retorno 
					SET vinteum      = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 22) THEN
				UPDATE _retorno 
					SET vintedois    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 23) THEN
				UPDATE _retorno 
					SET vintetres    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 24) THEN
				UPDATE _retorno 
					SET vintequatro  = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 25) THEN
				UPDATE _retorno 
					SET vintecinco   = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 26) THEN
				UPDATE _retorno 
					SET vinteseis    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 27) THEN --SELECT * FROM _retorno WHERE caderno_id = 1 AND aluno_id = 5 AND mes = 2 AND ano = 2019;
				UPDATE _retorno 
					SET vintesete    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 28) THEN
				UPDATE _retorno 
					SET vinteoito    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 29) THEN
				UPDATE _retorno 
					SET vintenove    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 30) THEN
				UPDATE _retorno 
					SET trinta       = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			ELSIF (dia_corrente = 31) THEN
				UPDATE _retorno 
					SET trintaeum    = public.status(chamadas_in.status)
					WHERE caderno_id = chamadas_in.caderno_id AND aluno_id = chamadas_in.aluno_id AND mes = mes_corrente AND ano = ano_corrente;
			END IF;
		ELSE
			aluno_corrente = chamadas_in.aluno_id;
			mes_corrente = date_part('MONTH',chamadas_in.evento); ano_corrente = date_part('YEAR',chamadas_in.evento);
			IF dia_corrente = 1 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,um,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 2 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 3 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,tres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 4 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 5 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,cinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 6 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,seis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 7 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,sete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 8 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,oito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 9 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,nove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 10 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dez,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 11 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,onze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 12 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,doze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 13 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,treze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 14 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quatorze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 15 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,quinze,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 16 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezesseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 17 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezessete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 18 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 19 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,dezenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 20 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinte,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 21 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 22 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintedois,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 23 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintetres,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 24 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintequatro,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 25 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintecinco,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 26 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteseis,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 27 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintesete,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 28 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vinteoito,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 29 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,vintenove,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 30 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trinta,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			ELSIF dia_corrente = 31 THEN
				INSERT INTO _retorno(chamada_id,caderno_id,aluno_id,trintaeum,mes,ano) VALUES(chamadas_in.chamada_id,chamadas_in.caderno_id,chamadas_in.aluno_id,public.status(chamadas_in.status),mes_corrente,ano_corrente);
			END IF;
		
		END IF;
	END LOOP;
	
	RETURN QUERY SELECT chamada_id,
			    caderno_id,
			    aluno_id,
			    COALESCE(um,0),
			    COALESCE(dois,0),
			    COALESCE(tres,0),
			    COALESCE(quatro,0),
			    COALESCE(cinco,0),
			    COALESCE(seis,0),
			    COALESCE(sete,0),
			    COALESCE(oito,0),
			    COALESCE(nove,0),
			    COALESCE(dez,0),
			    COALESCE(onze,0),
			    COALESCE(doze,0),
			    COALESCE(treze,0),
			    COALESCE(quatorze,0),
			    COALESCE(quinze,0),
			    COALESCE(dezesseis,0),
			    COALESCE(dezessete,0),
			    COALESCE(dezoito,0),
			    COALESCE(dezenove,0),
			    COALESCE(vinte,0),
			    COALESCE(vinteum,0),
			    COALESCE(vintedois,0),
			    COALESCE(vintetres,0),
			    COALESCE(vintequatro,0),
			    COALESCE(vintecinco,0),
			    COALESCE(vinteseis,0),
			    COALESCE(vintesete,0),
			    COALESCE(vinteoito,0),
			    COALESCE(vintenove,0),
			    COALESCE(trinta,0),
			    COALESCE(trintaeum,0),
			    mes,
			    ano FROM _retorno;
END; $BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE VIEW escola.chamadas_report_view AS
	select _chamada_id,_caderno_id,_aluno_id,c.nome as nome_aluno,_um,_dois,_tres,_quatro,_cinco,_seis,_sete,_oito,_nove,_dez,_onze,_doze,_treze,_quatorze,_quinze,_dezesseis,_dezessete,_dezoito,_dezenove,_vinte,_vinteum,_vintedois,_vintetres,_vintequatro,_vintecinco,_vinteseis,_vintesete,_vinteoito,_vintenove,_trinta,_trintaeum,_mes,_ano from escola.composicao_chamadas()
		inner join colaborador c on c.colaborador_id = _aluno_id;
