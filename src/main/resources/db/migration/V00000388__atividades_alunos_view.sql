DROP VIEW escola.atividades_alunos_view;

CREATE OR REPLACE VIEW escola.atividades_alunos_view AS 
 SELECT at.alunoatividade_id,
    at.aluno_id,
    at.composicao_aluno_id,
    a.atividade_id,
    ta.descricao AS descricaotipoatividade,
    a.gradecurricular_id,
    a.data_atividade,
    a.titulo,
    a.descricao,
    a.avaliacao,
    a.orientacoes,
    a.nota,
    a.conceito,
    a.entrega,
    a.concluido,
    t.descricao AS descricaoturma,
    a.status
   FROM escola.alunos_atividades at
     JOIN escola.atividades a ON a.atividade_id = at.atividade_id
     JOIN escola.alunos e ON e.colaborador_id = at.aluno_id AND e.composicao_id = at.composicao_aluno_id
     JOIN escola.gradecurricular g ON g.gradecurricular_id = a.gradecurricular_id
     JOIN turmas t ON t.turma_id = g.turma_id
     LEFT JOIN escola.tipos_atividades ta ON ta.tipoatividade_id = a.tipoatividade_id;
