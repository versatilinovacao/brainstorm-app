DROP VIEW colaborador_user_view;

CREATE OR REPLACE VIEW colaborador_user_view AS 
 SELECT c.colaborador_id,
    c.composicao_id,
    c.nome,
    i.data_nascimento AS nascimento,
    i.foto_thumbnail,
    c.email
   FROM colaborador c
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
   WHERE COALESCE(c.status,true) = true;