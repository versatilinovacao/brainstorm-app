ALTER TABLE almoxarifado.artefatos		ADD COLUMN servicocachoeirinha_id			BIGINT;
ALTER TABLE almoxarifado.artefatos		ADD COLUMN servicoportoalegre_id			BIGINT;

ALTER TABLE servico.cachoeirinha		ADD CONSTRAINT cachoeirinha_pk	PRIMARY KEY(servico_id);
ALTER TABLE servico.portoalegre			ADD CONSTRAINT portoalegre_pk	PRIMARY KEY(servico_id);

ALTER TABLE almoxarifado.artefatos		ADD CONSTRAINT cachoeirinha_fk	FOREIGN KEY(servicocachoeirinha_id)	REFERENCES servico.cachoeirinha(servico_id);
ALTER TABLE almoxarifado.artefatos		ADD CONSTRAINT portoalegre_fk	FOREIGN KEY(servicoportoalegre_id)	REFERENCES servico.portoalegre(servico_id);
