--Prefeitura Municipal de Cachoeirinha
--Secretaria Municipal da Fazenda

CREATE TABLE contabil.naturezaopservicos(
	naturezaopservico_id			BIGSERIAL NOT NULL PRIMARY KEY,
	referencia						DOUBLE PRECISION NOT NULL UNIQUE,
	titulo							VARCHAR(200) NOT NULL,
	descricao						VARCHAR(500),
	status							BOOLEAN
);

CREATE TABLE contabil.opservicos_cidades(
	naturezaopservico_id			BIGINT NOT NULL REFERENCES contabil.naturezaopservicos(naturezaopservico_id),
	cidade_id						BIGINT NOT NULL REFERENCES cidades(cidade_id)
);

CREATE INDEX op_cidades_idx ON contabil.opservicos_cidades(naturezaopservico_id,cidade_id);

INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(5.1,'Imposto devido em Cachoeirinha, com obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado na cidade de Cachoeirinha e cuja obrigação de recolhimento do ISS seja do contratante do serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(5.2,'Imposto devido em Cachoeirinha, sem obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado na cidade de Cachoeirinha e cuja obrigação de recolhimento do ISS seja do prestador de serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(5.8,'Não tributável','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado na cidade de Cachoeirinha e cuja operação esteja abrangida por imunidade ou isenção tributária.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(5.9,'Imposto recolhido pelo regime único de arrecadação','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado na cidade de Cachoeirinha e cuja operação e prestador do serviço esteja enquadrado no Simples Nacional.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.0,'PRESTAÇÃO DE SERVIÇOS PARA OUTROS MUNICÍPIOS DA FEDERAÇÃO','Classificam-se neste grupo as prestações de serviços efetuadas para estabelecimentos situados em municípios diversos do município de Cachoeirinha.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.1,'Imposto devido em Cachoeirinha, com obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha, cujo imposto seja devido em Cachoeirinha e a obrigação de recolhimento do ISS seja do contratante do serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.2,'Imposto devido em Cachoeirinha, sem obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha, cujo imposto seja devido em Cachoeirinha e a obrigação de recolhimento do ISS seja do prestador do serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.3,'Imposto devido fora de Cachoeirinha, com obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha, cujo imposto seja devido no município onde o serviço foi prestado e a obrigação de recolhimento do ISS seja do contratante do serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.4,'Imposto devido fora de Cachoeirinha, sem obrigação de retenção na fonte','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha, cujo imposto seja devido no município onde o serviço foi prestado e a obrigação de recolhimento do ISS seja do prestador do serviço.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.8,'Não tributável','Classificam-se neste código as prestações de serviço em que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha e cuja operação esteja abrangida por imunidade ou isenção tributária.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(6.9,'Imposto recolhido pelo regime único de arrecadação','Classificam-se neste código as prestações de serviços que o estabelecimento tomador esteja localizado fora da cidade de Cachoeirinha e cuja operação e prestador do serviço esteja enquadrado no Simples Nacional.');
INSERT INTO contabil.naturezaopservicos(referencia,titulo,descricao) VALUES(7.0,'PRESTAÇÃO DE SERVIÇOS PARA O EXTERIOR','Classificam-se neste grupo as prestações de serviços que o estabelecimento contratado esteja localizado fora do Brasil.');

INSERT INTO contabil.opservicos_cidades(naturezaopservico_id,cidade_id) 
SELECT naturezaopservico_id, (SELECT cidade_id FROM cidades WHERE descricao LIKE '%Cachoeirinha%' and estado_id = 43)
FROM contabil.naturezaopservicos;

CREATE OR REPLACE VIEW contabil.naturezas_servicos_ativas AS
SELECT c.naturezaopservico_id,
	   c.referencia,
	   c.titulo,
	   c.descricao 
FROM contabil.naturezaopservicos c 
WHERE COALESCE(c.status,true) = true;

CREATE OR REPLACE VIEW contabil.naturezas_servicos_inativas AS
SELECT c.naturezaopservico_id,
	   c.referencia,
	   c.titulo,
	   c.descricao 
FROM contabil.naturezaopservicos c 
WHERE COALESCE(c.status,true) = false;
