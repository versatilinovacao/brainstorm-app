CREATE TABLE public.telas_security(
	telasecurity_id								SERIAL NOT NULL PRIMARY KEY,
	gruposecurity_id							BIGINT NOT NULL REFERENCES public.gruposecurity(gruposecurity_id),
	telasistema_id								BIGINT NOT NULL REFERENCES public.telas_sistema(telasistema_id)
);


ALTER TABLE public.gruposecurity_telasecurity ADD COLUMN telasecurity_id				BIGINT REFERENCES public.telas_security(telasecurity_id);
