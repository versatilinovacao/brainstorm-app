CREATE TABLE cidades(
	cidade_id					BIGINT,
	reduzido					BIGINT,
	descricao					VARCHAR(150),
	estado_id					BIGINT
);

ALTER TABLE cidades ADD CONSTRAINT cidade_pk PRIMARY KEY(cidade_id);
ALTER TABLE cidades ADD CONSTRAINT estado_fk FOREIGN KEY(estado_id) REFERENCES estados(estado_id);