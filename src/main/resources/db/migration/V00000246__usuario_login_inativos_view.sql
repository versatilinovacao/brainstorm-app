DROP VIEW usuariosinativosview;

CREATE OR REPLACE VIEW usuariosinativosview AS 
 SELECT u.usuario_id,
    u.empresa_id,
    u.nome,
    u.status,
    u.email,
    c.colaborador_id,
    c.composicao_id AS composicao_colaborador_id,
    i.cpf,
    i.data_nascimento,
    c.email AS email_colaborador,
    i.foto_thumbnail,
    u.isserver,
    u.isclient,
    u.iscounter
   FROM usuario u
     LEFT JOIN colaborador c ON c.colaborador_id = u.colaborador_id
     LEFT JOIN identificador i ON i.identificador_id = c.identificador_id
  WHERE u.status = false;

ALTER TABLE usuariosinativosview
  OWNER TO postgres;
