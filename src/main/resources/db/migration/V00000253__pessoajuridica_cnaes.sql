CREATE TABLE empresa.pessoajuridica_cnaes(
	pessoajuridica_id							BIGINT NOT NULL REFERENCES pessoajuridica(pessoajuridica_id),
	cnae_id										BIGINT NOT NULL REFERENCES contabil.cnaes(cnae_id)
);

CREATE INDEX fki_pessoajuridica_cnae_fk
	ON empresa.pessoajuridica_cnaes
	USING btree(pessoajuridica_id,cnae_id);
