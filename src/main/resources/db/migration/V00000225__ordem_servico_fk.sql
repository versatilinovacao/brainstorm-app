ALTER TABLE servico.ordensservicos ADD COLUMN composicao_colaborador_id bigint;

ALTER TABLE servico.ordensservicos
  ADD CONSTRAINT colaborador_fk FOREIGN KEY (colaborador_id, composicao_colaborador_id)
      REFERENCES colaborador (colaborador_id, composicao_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_colaborador_fk
  ON servico.ordensservicos
  USING btree
  (colaborador_id, composicao_colaborador_id);
