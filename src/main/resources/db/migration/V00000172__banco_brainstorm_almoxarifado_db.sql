--	EXECUTE 'CREATE DATABASE brainstorm_nfse_db WITH ENCODING = ' || quote_literal('UTF8') || ' OWNER = postgres TEMPLATE = template0 LC_COLLATE = ' || quote_literal('pt_BR.UTF-8') || ' LC_CTYPE = ' || quote_literal('pt_BR.UTF-8') || ' CONNECTION LIMIT = -1 TABLESPACE = pg_default;';

CREATE SCHEMA almoxarifado;

CREATE TABLE almoxarifado.classificacoes(
	classificacao_id			SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO almoxarifado.classificacoes(nome) VALUES('Produto'),('Serviço');

CREATE TABLE almoxarifado.tipos(
	tipo_id						SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO almoxarifado.tipos(nome) VALUES('Item'),('Insumo'),('Materia Prima'),('Material'),('Equipamento'),('Ferramenta'),('Mão de Obra'),('Veículo'),('Investimento'),('Humano'),('Edificação');

CREATE TABLE almoxarifado.artigos(
	artigo_id					SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO almoxarifado.artigos(nome) VALUES('Produção'),('Comercialização'),('Consumo'),('Patrimonial'),('Construção');

CREATE TABLE almoxarifado.artefatos(
	artefato_id					SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(60) NOT NULL UNIQUE,
	descricao					VARCHAR(200),
	classificacao_id			BIGINT NOT NULL REFERENCES almoxarifado.classificacoes(classificacao_id),
	tipo_id						BIGINT NOT NULL REFERENCES almoxarifado.tipos(tipo_id),
	artigo_id					BIGINT NOT NULL REFERENCES almoxarifado.artigos(artigo_id),
	colaborador_id				BIGINT REFERENCES public.colaborador(colaborador_id),
	

	preco						DOUBLE PRECISION
);

CREATE TABLE almoxarifado.fotos(
	foto_id						SERIAL NOT NULL PRIMARY KEY,
	composicao_id				BIGINT REFERENCES almoxarifado.fotos(foto_id),
	artefato_id					BIGINT NOT NULL REFERENCES almoxarifado.artefatos(artefato_id),					
	imagem_tumbs				TEXT,
	imagem_original				TEXT
);