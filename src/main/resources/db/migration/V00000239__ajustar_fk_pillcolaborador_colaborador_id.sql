ALTER TABLE configuracao.colaboradores_classificacoes ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE configuracao.colaboradores_classificacoes
	ADD CONSTRAINT colaboradores_classificacoes_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;
	
CREATE INDEX fki_colaboradores_classificacoes_fk
	ON configuracao.colaboradores_classificacoes
	USING btree(colaborador_id,composicao_colaborador_id);
