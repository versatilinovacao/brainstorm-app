SELECT setval('public.telas_sistema_telasistema_id_seq', (SELECT MAX(telasistema_id) FROM telas_sistema), true);
SELECT setval('public.permissao_permissao_id_seq',(SELECT MAX(permissao_id) FROM permissao),true);

INSERT INTO permissao_por_tela(telasistema_id,permissao_id) VALUES((SELECT telasistema_id FROM telas_sistema WHERE nome = 'contratomodelo'),(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_SALVAR'));
INSERT INTO permissao_por_tela(telasistema_id,permissao_id) VALUES((SELECT telasistema_id FROM telas_sistema WHERE nome = 'contratomodelo'),(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_CONTEUDO'));
INSERT INTO permissao_por_tela(telasistema_id,permissao_id) VALUES((SELECT telasistema_id FROM telas_sistema WHERE nome = 'contratomodelo'),(SELECT permissao_id FROM permissao WHERE nome = 'ROLE_CONTRATOMODELO_DELETAR'));
