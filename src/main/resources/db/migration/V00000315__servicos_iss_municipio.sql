DROP TABLE tributacao.servicos_iss_municipios;

CREATE TABLE tributacao.servicos_iss_municipios
(
  servicoissmunicipio_id 	bigserial NOT NULL,
  servico_id 				bigint,
  cidade_id 				bigint,
  aliquota 					numeric(16,4),
  CONSTRAINT servicos_iss_municipios_pkey PRIMARY KEY (servicoissmunicipio_id),
  CONSTRAINT servicos_fk FOREIGN KEY (servico_id)
      REFERENCES contabil.servicos (servico_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT servicos_iss_municipios_cidade_id_fkey FOREIGN KEY (cidade_id)
      REFERENCES cidades (cidade_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT servico_unico UNIQUE (servico_id, cidade_id)
)