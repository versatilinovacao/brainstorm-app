INSERT INTO financeiro.formas_pagamentos(descricao)
SELECT descricao FROM (
		SELECT 'Isento'::VARCHAR descricao
	) AS tmp
	WHERE NOT EXISTS(
		SELECT descricao FROM financeiro.formas_pagamentos WHERE TRIM(UPPER(descricao)) = 'ISENTO'
);
