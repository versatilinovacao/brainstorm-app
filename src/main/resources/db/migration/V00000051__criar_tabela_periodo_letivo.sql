﻿--DROP TABLE public.periodos_letivo
CREATE TABLE public.periodos_letivo(
	periodo_letivo_id			SERIAL NOT NULL PRIMARY KEY,
	descricao				VARCHAR(100) NOT NULL,
	ano					VARCHAR(4) NOT NULL UNIQUE,
	status					BOOLEAN
);

--DROP TABLE public.periodos_letivo_itens;
CREATE TABLE public.periodos_letivo_itens(
	periodo_letivo_item_id			SERIAL NOT NULL,
	periodo_letivo_id			BIGINT NOT NULL,
	descricao				VARCHAR(100) NOT NULL,
	periodo_inicial				DATE NOT NULL,
	periodo_final				DATE NOT NULL
);

ALTER TABLE public.periodos_letivo_itens ADD CONSTRAINT periodo_letivo_item_pk PRIMARY KEY(periodo_letivo_item_id);
ALTER TABLE public.periodos_letivo_itens ADD CONSTRAINT periodo_letivo_fk FOREIGN KEY(periodo_letivo_id) REFERENCES public.periodos_letivo(periodo_letivo_id);

CREATE TABLE action.deletar_periodos_letivo(
	periodo_letivo_id			BIGINT NOT NULL PRIMARY KEY
);

CREATE TABLE critica.periodos_letivo(
	critica_id				SERIAL NOT NULL PRIMARY KEY,
	periodo_letivo_id			BIGINT,
	mensagem				VARCHAR(500),
	ocorrencia				TIMESTAMP
);

CREATE TABLE msg.periodo_letivo(
	mensagem_id				SERIAL NOT NULL PRIMARY KEY,
	perido_letivo_id			BIGINT,
	mensagem				VARCHAR(500),
	ocorrencia				TIMESTAMP
);

CREATE OR REPLACE FUNCTION action.deletar_periodos_letivo_execute() RETURNS trigger AS $$
DECLARE
	temPeriodo_Letivo	BOOLEAN;
BEGIN 
	SELECT COUNT(1) > 0 INTO temPeriodo_Letivo FROM critica.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;

	IF NOT temPeriodo_Letivo THEN
		DELETE FROM public.periodos_letivo_itens WHERE periodo_letivo_id = NEW.periodo_letivo_id;
		DELETE FROM public.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;
		
	ELSE
		INSERT INTO msg.periodos_letivo(periodo_letivo_id,mensagem,ocorrencia) SELECT periodo_letivo_id,mensagem,ocorrencia FROM critica.periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;
	END IF;

	DELETE FROM action.deletar_periodos_letivo WHERE periodo_letivo_id = NEW.periodo_letivo_id;

	RETURN NEW;

END; $$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION action.cancelar_critica_periodos_letivo_execute() RETURNS trigger AS $$
BEGIN
	DELETE FROM critica.periodos_letivo WHERE periodo_letivo_id = OLD.periodo_letivo_id; 
	RETURN OLD;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER deletar_periodos_letivo_trigger AFTER INSERT ON action.deletar_periodos_letivo FOR EACH ROW
	EXECUTE PROCEDURE action.deletar_periodos_letivo_execute();

CREATE OR REPLACE FUNCTION action.periodos_letivo_criticar() RETURNS trigger AS $$
BEGIN
	INSERT INTO critica.periodos_letivo(periodo_letivo_id,mensagem,ocorrencia) VALUES(NEW.periodo_letivo_id,'Existe um ou mais vinculos ao periodo letivo, desvincule-o antes de tentar excluir!',now());
	
	RETURN NEW;
END; $$
LANGUAGE 'plpgsql';

CREATE TRIGGER periodos_letivo_trigger AFTER INSERT ON public.periodos_letivo FOR EACH ROW
	EXECUTE PROCEDURE action.periodos_letivo_criticar();