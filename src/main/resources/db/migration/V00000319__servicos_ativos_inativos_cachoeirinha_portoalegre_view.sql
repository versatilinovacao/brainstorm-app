CREATE OR REPLACE VIEW servico.servicos_ativos_cachoeirinha_view AS
SELECT *
FROM servico.cachoeirinha c
WHERE c.status = true;

CREATE OR REPLACE VIEW servico.servicos_inativos_cachoeirinha_view AS
SELECT *
FROM servico.cachoeirinha c
WHERE c.status = false;


CREATE OR REPLACE VIEW servico.servicos_ativos_portoalegre_view AS
SELECT *
FROM servico.portoalegre c
WHERE c.status = true;

CREATE OR REPLACE VIEW servico.servicos_inativos_portoalegre_view AS
SELECT *
FROM servico.portoalegre c
WHERE c.status = false;