CREATE OR REPLACE VIEW contabil.arvore_planos_contas_ativas_view AS
WITH RECURSIVE arvore AS (
	SELECT p.codigo,p.planoconta_id,p.composicao_id,p.grupo,p.descricao,p.status FROM contabil.planos_contas p WHERE p.composicao_id IS null
	UNION ALL
		SELECT CAST((a.codigo || '.' || pr.codigo) AS VARCHAR(8)) AS codigo ,pr.planoconta_id,pr.composicao_id,pr.grupo,pr.descricao,pr.status
		FROM contabil.planos_contas pr
		INNER JOIN arvore a ON pr.composicao_id = a.planoconta_id
) SELECT codigo,planoconta_id,composicao_id,descricao,grupo,status 
  FROM arvore 
  WHERE COALESCE(status,true) = true
  ORDER BY codigo;


CREATE OR REPLACE VIEW contabil.arvore_planos_contas_inativas_view AS
WITH RECURSIVE arvore AS (
	SELECT p.codigo,p.planoconta_id,p.composicao_id,p.grupo,p.descricao,p.status FROM contabil.planos_contas p WHERE p.composicao_id IS null
	UNION ALL
		SELECT CAST((a.codigo || '.' || pr.codigo) AS VARCHAR(8)) AS codigo ,pr.planoconta_id,pr.composicao_id,pr.grupo,pr.descricao,pr.status
		FROM contabil.planos_contas pr
		INNER JOIN arvore a ON pr.composicao_id = a.planoconta_id
) SELECT codigo,planoconta_id,composicao_id,descricao,grupo,status 
  FROM arvore 
  WHERE COALESCE(status,true) = false
  ORDER BY codigo;
