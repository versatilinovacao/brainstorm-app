DROP TABLE escola.chamadas CASCADE;
CREATE TABLE escola.chamadas
(
  chamada_id serial NOT NULL,
  caderno_id bigint NOT NULL,
  evento timestamp without time zone NOT NULL,
  aluno_id bigint NOT NULL,
  periodo_letivo_item_id bigint NOT NULL,
  conceito character varying(3),
  nota numeric(16,4),
  avaliacao text,
  status boolean,
  CONSTRAINT chamadas_pkey PRIMARY KEY (chamada_id),
  CONSTRAINT chamadas_aluno_id_fkey FOREIGN KEY (aluno_id)
      REFERENCES colaborador (colaborador_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT chamadas_caderno_id_fkey FOREIGN KEY (caderno_id)
      REFERENCES escola.cadernos (caderno_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT chamadas_periodo_letivo_item_id_fkey FOREIGN KEY (periodo_letivo_item_id)
      REFERENCES escola.periodos_letivos_itens (periodo_letivo_item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE VIEW escola.chamada_view AS
	SELECT c.chamada_id,c.caderno_id,s.descricao AS nome_caderno,c.evento,c.aluno_id, a.nome AS nome_aluno,c.periodo_letivo_item_id,c.conceito,c.nota,c.avaliacao,c.status 
		FROM escola.chamadas c
		INNER JOIN public.colaborador a ON a.colaborador_id = c.aluno_id
		INNER JOIN escola.cadernos s ON s.caderno_id = c.caderno_id;