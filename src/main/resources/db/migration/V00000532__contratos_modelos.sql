CREATE TABLE contratos_modelos(
	contratomodelo_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL,
	documento						TEXT
);
