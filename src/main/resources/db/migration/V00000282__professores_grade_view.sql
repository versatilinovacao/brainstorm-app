ALTER TABLE cargos ADD COLUMN cbo	VARCHAR(8);

CREATE SCHEMA filtro;
CREATE TABLE filtro.classificacoes_cbos(
	classificacaocbo_id				BIGSERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(60) NOT NULL UNIQUE
);

INSERT INTO filtro.classificacoes_cbos(descricao) VALUES('Professor');

CREATE TABLE filtro.cbos_parceiros(
	cboparceiro_id					BIGSERIAL NOT NULL PRIMARY KEY,
	codigo							VARCHAR(8) NOT NULL UNIQUE,
	classificacao_id				BIGINT REFERENCES filtro.classificacoes_cbos(classificacaocbo_id)
);


INSERT INTO cargos(descricao,cbo,status) VALUES('Professor de língua e literatura brasileira no ensino médio','232145',true);
INSERT INTO cargos(descricao,cbo,status) VALUES('Professor de língua estrangeira moderna no ensino médio','232150',true);
INSERT INTO filtro.cbos_parceiros(codigo,classificacao_id) VALUES('232145',(SELECT classificacaocbo_id FROM filtro.classificacoes_cbos WHERE descricao = 'Professor'));
INSERT INTO filtro.cbos_parceiros(codigo,classificacao_id) VALUES('232150',(SELECT classificacaocbo_id FROM filtro.classificacoes_cbos WHERE descricao = 'Professor'));

DROP VIEW escola.professores_grade_view;

CREATE OR REPLACE VIEW escola.professores_grade_view AS 
 SELECT c.colaborador_id AS professor_id,
        c.nome,
		a.cbo
   FROM colaborador c
  INNER JOIN parceiros p ON p.parceiro_id = c.parceiro_id
  INNER JOIN cargos a ON a.cargo_id = p.cargo_id
	     AND a.cbo IN (SELECT codigo FROM filtro.cbos_parceiros WHERE classificacao_id = (SELECT classificacaocbo_id FROM filtro.classificacoes_cbos WHERE descricao = 'Professor'))
  WHERE COALESCE(c.parceiro_id, 0::bigint) > 0 
    AND c.status = true;

