CREATE TABLE empresa.unidades(
	unidade_id			SERIAL NOT NULL PRIMARY KEY,
	descricao			VARCHAR(50) NOT NULL UNIQUE
);

INSERT INTO empresa.unidades(descricao) VALUES('Matriz'),('Filial');

ALTER TABLE empresa.empresa ADD COLUMN unidade_id			BIGINT;
ALTER TABLE empresa.empresa ADD CONSTRAINT unidade_fk FOREIGN KEY(unidade_id) REFERENCES empresa.unidades(unidade_id);

CREATE VIEW empresa.empresa_matriz AS
	SELECT c.colaborador_id,e.* FROM empresa.empresa e 
	INNER JOIN public.colaborador c ON c.empresa_id = e.empresa_id
	WHERE e.unidade_id = (SELECT unidade_id FROM empresa.unidades WHERE UPPER(descricao) = 'MATRIZ');

--Classificação da Empresa. 	|Não foi localizado nenhuma Empresa com a classificação configurada...Classifique antes de continuar...
CREATE OR REPLACE FUNCTION empresa.verifica_classificacao() RETURNS trigger AS $$
DECLARE
	ano_v				VARCHAR(4);
	periodo_letivo_v		BIGINT;
	cadernos_for			escola.cadernos%ROWTYPE;

	classificacao_empresa_v	BIGINT;
BEGIN	--select * from classificacao_empresa_id FROM 
	SELECT COALESCE(classificacao_empresa_id,0) INTO classificacao_empresa_v FROM empresa.empresa_matriz LIMIT 1;
	IF classificacao_empresa_v = 9 THEN --select * from escola.cadernos_inconsistencias
		SELECT DATE_PART('year',now()) INTO ano_v;
		SELECT COALESCE(periodo_letivo_id,0) INTO periodo_letivo_v	FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;
		IF  periodo_letivo_v > 0 THEN
			FOR cadernos_for IN
				SELECT * FROM escola.cadernos WHERE periodo_letivo_id = periodo_letivo_v 
			LOOP
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE caderno_id = cadernos_for.caderno_id
											   AND consistencia_id = (SELECT consistencia_id FROM public.consistencias WHERE nome = 'Classificação da Empresa');
			END LOOP;
		END IF;
	END IF;
	
	RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER verifica_classificacao_tgi AFTER INSERT ON empresa.empresa FOR EACH ROW EXECUTE PROCEDURE empresa.verifica_classificacao();
CREATE TRIGGER verifica_classificacao_tgu AFTER UPDATE ON empresa.empresa FOR EACH ROW EXECUTE PROCEDURE empresa.verifica_classificacao();
