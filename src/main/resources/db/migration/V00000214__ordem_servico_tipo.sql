CREATE SCHEMA servico;

CREATE TABLE servico.ordemservicotipo(
	ordemservicotipo_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100)
);

INSERT INTO servico.ordemservicotipo(nome) VALUES('Reparo'),('Garantia'),('Atendimento'),('Venda');
