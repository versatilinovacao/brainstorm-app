DROP VIEW escola.avaliacoes_view;
CREATE OR REPLACE VIEW escola.avaliacoes_view AS 
 SELECT a.avaliacao_id,
    a.chamada_id,
    a.caderno_id,
    a.aluno_id,
    c.nome AS nome_aluno,
    p.descricao AS periodo_letivo_nome,
    a.aproveitamento,
    a.conceito,
    (SELECT SUM(nota) FROM escola.chamadas_view WHERE chamada_aowner_id = a.chamada_id and aluno_id = aluno_id) AS nota,
    a.avaliacao
   FROM escola.avaliacoes a
     LEFT JOIN colaborador c ON c.colaborador_id = a.aluno_id
     LEFT JOIN escola.periodos_letivos_itens p ON p.periodo_letivo_item_id = a.chamada_id;

ALTER TABLE empresa.empresa ADD COLUMN criterio			NUMERIC(16,4);
