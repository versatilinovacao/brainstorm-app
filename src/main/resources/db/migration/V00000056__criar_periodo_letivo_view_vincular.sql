CREATE OR REPLACE VIEW selecionar_periodo_letivo_view AS
	SELECT p.periodo_letivo_id, p.descricao, p.ano, p.status
		FROM public.periodos_letivo p
		WHERE p.status = true;
