CREATE MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view AS
SELECT  a.colaborador_id AS aluno_id,a.composicao_id AS composicao_aluno_id,
		aa.atividade_id,
		at.avaliacaoalunoatividade_id,
		nota,
		conceito,
		avaliacao_descritiva,
		false	AS concluido
FROM escola.alunos a
INNER JOIN escola.alunos_atividades aa ON aa.aluno_id = a.colaborador_id AND aa.composicao_aluno_id = a.composicao_id
LEFT JOIN escola.avaliacao_aluno_atividade at ON at.aluno_id = a.colaborador_id AND at.composicao_aluno_id = a.composicao_id;

CREATE UNIQUE INDEX ON escola.atividades_alunos_avaliacao_view(aluno_id,composicao_aluno_id,atividade_id);

CREATE OR REPLACE FUNCTION escola.alunos_materializados()	RETURNS trigger AS $$
BEGIN
	REFRESH MATERIALIZED VIEW escola.atividades_alunos_avaliacao_view;
	RETURN NEW;
END;$$
LANGUAGE 'plpgsql';

CREATE TRIGGER atualiar_aluno AFTER INSERT OR UPDATE OR DELETE ON escola.alunos
	FOR EACH STATEMENT 
		EXECUTE PROCEDURE escola.alunos_materializados(); 
	
CREATE TRIGGER atualiar_aluno AFTER INSERT OR UPDATE OR DELETE ON escola.avaliacao_aluno_atividade
	FOR EACH STATEMENT 
		EXECUTE PROCEDURE escola.alunos_materializados(); 
	 