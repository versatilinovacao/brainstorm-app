ALTER TABLE escola.gradecurricular	ADD COLUMN curso_id		BIGINT;
ALTER TABLE escola.gradecurricular	ADD CONSTRAINT curso_fk		FOREIGN KEY(curso_id)	REFERENCES public.cursos(curso_id);