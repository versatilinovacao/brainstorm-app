CREATE EXTENSION IF NOT EXISTS dblink;
CREATE TYPE record_paises	AS (pais_id BIGINT, sigla VARCHAR, descricao VARCHAR);

CREATE FOREIGN DATA WRAPPER dbrnd VALIDATOR postgresql_fdw_validator;
CREATE SERVER cep_server FOREIGN DATA WRAPPER dbrnd OPTIONS (hostaddr '127.0.0.1', dbname 'cep_db');
CREATE USER MAPPING FOR postgres SERVER cep_server OPTIONS (user 'postgres', password 'default123');
GRANT USAGE ON FOREIGN SERVER cep_server TO postgres;

CREATE OR REPLACE FUNCTION dblink_paises() RETURNS SETOF record_paises AS $$
DECLARE
	reg	record_paises%ROWTYPE;
BEGIN
	FOR reg IN 
		SELECT pais_id, sigla, nome AS descricao FROM public.dblink('cep_server','select pais_id,sigla,nome from public.qualocep_paises') AS DATA(pais_id BIGINT, sigla VARCHAR, nome VARCHAR )
	LOOP
		RETURN NEXT reg;
	END LOOP;

	RETURN;
END; $$
LANGUAGE 'plpgsql'; 


CREATE SCHEMA remoto;
CREATE OR REPLACE VIEW remoto.pais_view AS 
select pais_id,sigla,descricao from dblink_paises() ;
