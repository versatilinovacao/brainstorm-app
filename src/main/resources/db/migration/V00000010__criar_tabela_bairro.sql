CREATE TABLE bairros(
	bairro_id						SERIAL NOT NULL,
	descricao						VARCHAR(100),
	cidade_id						BIGINT
);

ALTER TABLE bairros ADD CONSTRAINT bairro_pk PRIMARY KEY(bairro_id);
ALTER TABLE bairros ADD CONSTRAINT cidade_fk FOREIGN KEY(cidade_id) REFERENCES cidades(cidade_id);