ALTER TABLE public.cliente ADD COLUMN tipo_cliente_id		BIGINT;

CREATE TABLE tipos_clientes(
	tipo_cliente_id					SERIAL NOT NULL,
	descricao						VARCHAR(100) UNIQUE,
	status							BOOLEAN
);

ALTER TABLE public.tipos_clientes ADD CONSTRAINT tipo_cliente_pk PRIMARY KEY(tipo_cliente_id);
ALTER TABLE public.cliente ADD CONSTRAINT tipo_cliente_fk FOREIGN KEY(tipo_cliente_id) REFERENCES public.tipos_clientes(tipo_cliente_id);

INSERT INTO public.tipos_clientes(descricao) VALUES ('COMUM'),('ALUNO');

CREATE VIEW alunos_matriculados_view AS 
	SELECT c.colaborador_id,c.nome,cl.matricula
		FROM colaborador c
		INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id
			WHERE cl.tipo_cliente_id = (SELECT tipo_cliente_id FROM tipos_clientes WHERE descricao = 'ALUNO');


