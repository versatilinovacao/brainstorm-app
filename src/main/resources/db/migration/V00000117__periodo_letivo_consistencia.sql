CREATE OR REPLACE FUNCTION critica.periodoletivo_consistencia() RETURNS trigger AS $$
DECLARE
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	isExiste						BOOLEAN;
	cadernos						escola.cadernos%ROWTYPE;
BEGIN

	SELECT count(1) > 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) <= 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF IsPeriodoLetivoNotFind = true THEN
		FOR cadernos IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = NEW.periodo_letivo_id
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 1 AND caderno_id = cadernos.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos.caderno_id,1,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = cadernos.caderno_id;
			END IF;
		END LOOP;
	END IF;			
	
	IF isPeriodoLetivoFind = true THEN
		FOR cadernos IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = NEW.periodo_letivo_id
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 3 AND caderno_id = cadernos.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos.caderno_id,3,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 3 and caderno_id = cadernos.caderno_id;
			END IF;
		END LOOP;
	END IF;

	RETURN NEW;
END $$ LANGUAGE plpgsql;

CREATE TRIGGER periodoletivo_consistencia_tgi AFTER INSERT ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE critica.periodoletivo_consistencia();
CREATE TRIGGER periodoletivo_consistencia_tgu AFTER UPDATE ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE critica.periodoletivo_consistencia();
