DROP TABLE escola.atividades_arquivos;

DROP TABLE arquivos.mp4;
CREATE TABLE arquivos.mp4(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.png;
CREATE TABLE arquivos.png(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

--select * from arquivos.pdf
DROP TABLE arquivos.pdf;
CREATE TABLE arquivos.pdf(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.word;
CREATE TABLE arquivos.word(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.excel;
CREATE TABLE arquivos.excel(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.corel;
CREATE TABLE arquivos.corel(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.photoshop;
CREATE TABLE arquivos.photoshop(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

DROP TABLE arquivos.txt;
CREATE TABLE arquivos.txt(
	arquivo_id				BIGINT NOT NULL PRIMARY KEY,
	nome					VARCHAR(255) NOT NULL,
	registro				TIMESTAMP,
	extensao				VARCHAR(5),
	tamanho					BIGINT,
	url						VARCHAR(1024),
	tipo					VARCHAR(40),
	arquivo					TEXT
);

CREATE TABLE escola.atividades_arquivos(
	atividadearquivo_id								BIGSERIAL NOT NULL PRIMARY KEY,
	atividade_id									BIGINT NOT NULL REFERENCES escola.atividades(atividade_id),
	arquivomp4_id									BIGINT NOT NULL REFERENCES arquivos.mp4(arquivo_id)
);

CREATE INDEX atividades_arquivos_atividade_idx		ON escola.atividades_arquivos		USING btree(atividade_id);
