CREATE TABLE escola.cardapios_semanas(
	cardapiosemana_id						BIGSERIAL NOT NULL PRIMARY KEY,
	cardapio_id								BIGINT NOT NULL REFERENCES escola.cardapios(cardapio_id),
	evento									DATE NOT NULL,
	cafemanha								VARCHAR(500),
	lanchemanha								VARCHAR(500),
	almoco									VARCHAR(500),
	lanchetarde								VARCHAR(500),
	janta									VARCHAR(500)
);

CREATE INDEX ON escola.cardapios_semanas(cardapiosemana_id,cardapio_id);
CREATE INDEX ON escola.cardapios_semanas(evento,cardapiosemana_id,cardapio_id);
CREATE INDEX ON escola.cardapios_semanas(cardapio_id);
CREATE INDEX ON escola.cardapios_semanas(evento,cardapio_id);