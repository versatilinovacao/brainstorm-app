ALTER TABLE public.usuario ADD COLUMN composicao_colaborador_id BIGINT;

ALTER TABLE public.usuario
	ADD CONSTRAINT colaborador_fk FOREIGN KEY(colaborador_id,composicao_colaborador_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_usuario_colaborador_fk
	ON public.usuario
	USING btree(colaborador_id,composicao_colaborador_id);
