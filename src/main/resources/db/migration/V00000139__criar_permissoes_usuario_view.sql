ALTER TABLE public.permissao ADD COLUMN label VARCHAR(100);
--UPDATE public.permissao SET label = 'Permissão para visualiza Conteudo do Colaborador' WHERE permissao_id = 107
--UPDATE public.permissao SET label = 'Permissão para salvar Conteudo no Colaborador' WHERE permissao_id = 108
--UPDATE public.permissao SET label = 'Permissão para deletar Conteudo do Colaborador' WHERE permissao_id = 109

CREATE VIEW public.permissoes_usuario_view AS
SELECT CAST(CAST(a.usuario_id AS VARCHAR)||CAST(a.permissao_id AS VARCHAR) AS BIGINT) AS permissaousuarioview_id,
	   a.usuario_id, 
	   a.permissao_id, 
	   b.nome,b.label 
	FROM usuario_permissao a
	INNER JOIN permissao b ON b.permissao_id = a.permissao_id;
