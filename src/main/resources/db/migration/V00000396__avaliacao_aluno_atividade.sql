CREATE TABLE escola.avaliacao_aluno_atividade(
	avaliacaoalunoatividade_id				BIGSERIAL NOT NULL PRIMARY KEY,
	aluno_id								BIGINT NOT NULL,
	composicao_aluno_id						BIGINT,
	atividade_id							BIGINT NOT NULL,
	nota									INTEGER,
	conceito								VARCHAR(3),
	avaliacao_descritiva					TEXT
);

ALTER TABLE escola.avaliacao_aluno_atividade	ADD CONSTRAINT aluno_fk		FOREIGN KEY(aluno_id,composicao_aluno_id)		REFERENCES escola.alunos(colaborador_id,composicao_id);
ALTER TABLE escola.avaliacao_aluno_atividade	ADD CONSTRAINT atividade_fk	FOREIGN KEY(atividade_id)						REFERENCES escola.atividades(atividade_id);

CREATE INDEX aluno_idx		ON escola.avaliacao_aluno_atividade	USING btree(aluno_id);
CREATE INDEX atividade_idx	ON escola.avaliacao_aluno_atividade	USING btree(atividade_id);
