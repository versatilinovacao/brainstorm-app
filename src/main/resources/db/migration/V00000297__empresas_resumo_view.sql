DROP VIEW resumo_empresas_view;

CREATE OR REPLACE VIEW resumo_empresas_view AS 
 SELECT colaborador.colaborador_id AS empresa_id,
    colaborador.nome
   FROM colaborador
  WHERE COALESCE(colaborador.empresa_id, 0::bigint) > 0;