CREATE VIEW alunos_view AS 
	SELECT c.colaborador_id,c.nome,cl.matricula
		FROM colaborador c
		INNER JOIN cliente cl ON cl.cliente_id = c.cliente_id
	WHERE cl.tipo_cliente_id = (SELECT tipo_cliente_id FROM tipos_clientes WHERE descricao = 'ALUNO');