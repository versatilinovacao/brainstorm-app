CREATE TABLE escola.arquivos_por_atividades(
	arquivoporatividade_id								BIGSERIAL NOT NULL PRIMARY KEY,
	aluno_id											BIGINT NOT NULL,
	composicao_aluno_id									BIGINT,
	arquivo_id											BIGINT NOT NULL REFERENCES arquivos.hd(arquivo_id),
	atividade_id										BIGINT NOT NULL REFERENCES escola.atividades(atividade_id)
);

ALTER TABLE escola.arquivos_por_atividades		ADD CONSTRAINT aluno_fk		FOREIGN KEY(aluno_id,composicao_aluno_id)		REFERENCES escola.alunos(colaborador_id,composicao_id);

CREATE INDEX arq_por_ativ_aluno_idx			ON escola.arquivos_por_atividades		USING btree(aluno_id,composicao_aluno_id);
CREATE INDEX arq_por_ativ_arquivo_idx		ON escola.arquivos_por_atividades		USING btree(arquivo_id);
CREATE INDEX arq_por_ativ_atividade_idx		ON escola.arquivos_por_atividades		USING btree(atividade_id);