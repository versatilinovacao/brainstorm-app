CREATE OR REPLACE VIEW cargos_inativos_view AS
	SELECT c.* FROM public.cargos c
			  WHERE c.status = false;