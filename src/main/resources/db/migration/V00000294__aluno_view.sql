DROP VIEW alunos_view;

CREATE OR REPLACE VIEW alunos_view AS 
 SELECT c.colaborador_id AS aluno_id,
    c.composicao_id AS composicao_aluno_id,
    c.colaborador_id,
    c.composicao_id,
    c.nome,
    m.codigo AS matricula,
    m.curso_id	
   FROM colaborador c
     LEFT JOIN cliente cl ON cl.cliente_id = c.cliente_id
     LEFT JOIN matriculas m ON m.colaborador_id = c.colaborador_id
  WHERE cl.tipo_cliente_id = (( SELECT tipos_clientes.tipo_cliente_id
           FROM tipos_clientes
          WHERE tipos_clientes.descricao::text = 'ALUNO'::text)) AND c.status = true
  ORDER BY c.nome;