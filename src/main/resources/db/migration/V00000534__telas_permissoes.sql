--Criar a tela security
INSERT INTO telas_security(telasistema_id) 
SELECT telasistema_id FROM telas_sistema WHERE UPPER(nome) = 'CONTRATOMODELO';

--Vincular as permissoes_por_tela
INSERT INTO permissao_por_tela
SELECT t.telasistema_id,p.permissao_id FROM telas_sistema t
LEFT JOIN permissao p ON p.permissao_id IN (SELECT permissao_id FROM permissao WHERE nome LIKE '%'||UPPER( 'CONTRATOMODELO' )||'%')
WHERE t.nome = 'contratomodelo'
ORDER BY t.telasistema_id, p.permissao_id;

--SELECT * FROM telasecurity_permissao
INSERT INTO telasecurity_permissao
SELECT t.telasecurity_id,p.permissao_id
FROM telas_security t
LEFT JOIN telas_sistema ts ON ts.telasistema_id = t.telasistema_id
INNER JOIN permissao p ON p.permissao_id IN (SELECT permissao_id FROM permissao WHERE nome = 'Contrato Modelo')
WHERE t.telasistema_id = (SELECT telasistema_id FROM telas_sistema WHERE UPPER(nome) = 'CONTRATOMODELO')
ORDER BY t.telasecurity_id,p.nome;
