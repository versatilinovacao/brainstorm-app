CREATE TABLE escola.situacoes_atividades(
	situacaoatividade_id				BIGSERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);

INSERT INTO escola.situacoes_atividades(nome) VALUES('Concluido'),('Ajuste'),('Entregue'),('Aberto');

ALTER TABLE escola.avaliacao_aluno_atividade		ADD COLUMN situacaoatividade_id				BIGINT;
ALTER TABLE escola.avaliacao_aluno_atividade		ADD CONSTRAINT situacaoatividade_fk	FOREIGN KEY(situacaoatividade_id)		REFERENCES escola.situacoes_atividades(situacaoatividade_id);
CREATE INDEX situacaoatividade_idx	ON escola.avaliacao_aluno_atividade	USING btree(situacaoatividade_id);

