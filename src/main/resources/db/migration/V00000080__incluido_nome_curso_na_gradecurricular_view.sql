DROP VIEW escola.gradecurricular_view;
CREATE VIEW escola.gradecurricular_view AS
SELECT  gradecurricular_id,
	g.turma_id,
	t.descricao AS nome_turma,
	professor_id,
	c.nome AS nome_professor,
	g.materia_id, 
	m.descricao AS nome_materia,
	g.curso_id, 
	s.descricao AS nome_curso 
	FROM escola.gradecurricular g
	LEFT JOIN public.colaborador c ON c.colaborador_id = g.professor_id
	LEFT JOIN public.turmas t ON t.turma_id = g.turma_id
	LEFT JOIN public.materias m ON m.materia_id = g.materia_id
	LEFT JOIN public.cursos s ON s.curso_id = g.curso_id;
