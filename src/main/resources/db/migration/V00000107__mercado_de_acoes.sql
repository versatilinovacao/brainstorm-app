CREATE SCHEMA investidor;
CREATE TABLE investidor.acao(
	acao_id						SERIAL NOT NULL PRIMARY KEY,
	sigla						VARCHAR(20) NOT NULL,
	nome						VARCHAR(50) NOT NULL
);

CREATE TABLE investidor.cotacao(
	cotacao_id					SERIAL NOT NULL PRIMARY KEY,
	acao_id						BIGINT NOT NULL REFERENCES investidor.acao(acao_id),
	data						TIMESTAMP NOT NULL,
	abertura					NUMERIC(15,2) NOT NULL,
	minima						NUMERIC(15,2) NOT NULL,
	maxima						NUMERIC(15,2) NOT NULL,
	media						NUMERIC(15,2) NOT NULL,
	ultima						NUMERIC(15,2) NOT NULL,
	volume						DOUBLE PRECISION,
	negociado					BIGINT
);

CREATE TABLE investidor.ponderado(
	ponderado_id				SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL,
	descricao					VARCHAR(500)
);

INSERT INTO investidor.ponderado(nome) VALUES('Média 2 Períodos');
INSERT INTO investidor.ponderado(nome) VALUES('Média 9 Períodos');
INSERT INTO investidor.ponderado(nome) VALUES('Média 26 Períodos');
INSERT INTO investidor.ponderado(nome) VALUES('Média 200 Períodos');

CREATE TABLE investidor.medias(
	media_id					SERIAL NOT NULL PRIMARY KEY,
	cotacao_id					BIGINT NOT NULL REFERENCES investidor.cotacao(cotacao_id),
	ponderado_id				BIGINT NOT NULL REFERENCES investidor.ponderado(ponderado_id),	
	valor						NUMERIC(15,2) NOT NULL
);

CREATE TABLE investidor.movimentos(
	movimento_id				SERIAL NOT NULL PRIMARY KEY,
	evento						DATE NOT NULL,
	ocorrencia					TIME NOT NULL,
	negociado					BIGINT NOT NULL, --Compra, Retenção, Devolução, Custódia, Venda
	
	cotacao						NUMERIC(15,2) NOT NULL
);

CREATE TABLE investidor.saldo(
	saldo_id					SERIAL NOT NULL PRIMARY KEY,
	acao_id						BIGINT NOT NULL REFERENCES investidor.acao(acao_id),
	quantidade					BIGINT NOT NULL,
	preco						NUMERIC(15,2) NOT NULL
);

CREATE TABLE investidor.indexador(
	indexador_id				SERIAL NOT NULL PRIMARY KEY,
	codigo_bacen				BIGINT NOT NULL,
	nome						VARCHAR(20) NOT NULL,
	valor						DOUBLE PRECISION NOT NULL
);