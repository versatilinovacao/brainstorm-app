CREATE TABLE escola.cardapios(
	cardapio_id							BIGSERIAL NOT NULL PRIMARY KEY,
	descricao							VARCHAR(60) NOT NULL,
	mes									INTEGER,
	ano									INTEGER,
	status								BOOLEAN
);

CREATE OR REPLACE VIEW escola.cardapios_ativos_view AS
SELECT * FROM escola.cardapios WHERE COALESCE(status,true) = true;

CREATE OR REPLACE VIEW escola.cardapios_inativos_view AS
SELECT * FROM escola.cardapios WHERE COALESCE(status,true) = false;