CREATE TABLE action.login(
	login_id					SERIAL NOT NULL PRIMARY KEY,
	usuario_id					BIGINT NOT NULL UNIQUE
);

CREATE UNIQUE INDEX login_unico ON action.login(usuario_id);

CREATE OR REPLACE FUNCTION criar_sessao() RETURNS trigger AS $$
BEGIN
	DELETE FROM action.login WHERE usuario_id = NEW.usuario_id;
	
	RETURN NEW;	
END;$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION action.logar_execute() RETURNS trigger AS $$
DECLARE
	existe			BOOLEAN;
BEGIN
	--Temporário...
	SELECT count(1) > 0 INTO existe FROM pg_catalog.pg_tables WHERE tablename = 'sessao';

	IF existe THEN
		DROP TABLE sessao;
	END IF;

	--Verificar aqui se existe a tabela criada e senão tiver criar
	CREATE TEMP TABLE sessao AS SELECT * FROM action.login WHERE usuario_id = NEW.usuario_id;

	CREATE TRIGGER criar_sessao_trigger AFTER INSERT ON sessao
		FOR EACH ROW
			EXECUTE PROCEDURE criar_sessao();
	
	RETURN NEW;
END;$$
LANGUAGE plpgsql;

--CRIAR A TRIGGER logar_tg aqui

CREATE TRIGGER logar_usuario_trigger AFTER INSERT ON action.login
	FOR EACH ROW
		EXECUTE PROCEDURE action.logar_execute();


