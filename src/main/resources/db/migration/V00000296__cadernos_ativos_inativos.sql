DROP VIEW escola.cadernos_ativos_view;
CREATE OR REPLACE VIEW escola.cadernos_ativos_view AS
	SELECT c.caderno_id,
	       c.descricao,
	       c.gradecurricular_id,
	       c.conceito,
	       c.nota,
	       c.periodo_letivo_id,
	       c.escola_id,
	       c.composicao_escola_id,
	       t.turma_id,
	       t.descricao AS nome_turma,
	       p.ano,
	       c.status
	  FROM escola.cadernos c
		LEFT JOIN escola.gradecurricular g ON g.gradecurricular_id = c.gradecurricular_id
		LEFT JOIN turmas t ON t.turma_id = g.turma_id AND t.status = true
		LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
	WHERE c.status = true
	ORDER BY c.descricao;

DROP VIEW escola.cadernos_inativos_view;
CREATE OR REPLACE VIEW escola.cadernos_inativos_view AS
	SELECT c.caderno_id,
	       c.descricao,
	       c.gradecurricular_id,
	       c.conceito,
	       c.nota,
	       c.periodo_letivo_id,
	       c.escola_id,
	       c.composicao_escola_id,
	       t.turma_id,
	       t.descricao AS nome_turma,
	       p.ano,
	       c.status
	  FROM escola.cadernos c
		LEFT JOIN escola.gradecurricular g ON g.gradecurricular_id = c.gradecurricular_id
		LEFT JOIN turmas t ON t.turma_id = g.turma_id AND t.status = false
		LEFT JOIN escola.periodos_letivos p ON p.periodo_letivo_id = c.periodo_letivo_id
	WHERE c.status = false
	ORDER BY c.descricao;