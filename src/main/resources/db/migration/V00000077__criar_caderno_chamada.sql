CREATE SCHEMA escola;

CREATE TABLE escola.gradecurricular(
	gradecurricular_id				SERIAL NOT NULL PRIMARY KEY,
	turma_id						BIGINT NOT NULL REFERENCES public.turmas(turma_id),
	professor_id					BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	materia_id						BIGINT NOT NULL REFERENCES public.materias(materia_id)	
);

CREATE TABLE escola.cadernos(
	caderno_id						SERIAL NOT NULL PRIMARY KEY,
	descricao						VARCHAR(100) NOT NULL,
	gradecurricular_id				BIGINT NOT NULL REFERENCES escola.gradecurricular(gradecurricular_id),
	conceito						VARCHAR(1), --Determina o conceito minimo para aprovação
	nota							NUMERIC(16,4) --Determina a nota média para aprovação
);

CREATE TABLE escola.chamadas(
	chamada_id						SERIAL NOT NULL PRIMARY KEY,
	caderno_id						BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	aluno_id						BIGINT NOT NULL REFERENCES public.colaborador(colaborador_id),
	presenca						NUMERIC(16,4), --Percentual de presença do aluno em relação ao total de aulas.
	conceito						VARCHAR(1),
	nota							NUMERIC(16,4),
	avaliacao						TEXT
);

ALTER TABLE escola.chamadas			ADD CONSTRAINT chamada_unica 	UNIQUE(caderno_id,aluno_id);

CREATE TABLE escola.chamadas_alunos(
	chamada_aluno_id				SERIAL NOT NULL PRIMARY KEY,
	caderno_id 						BIGINT NOT NULL REFERENCES escola.cadernos(caderno_id),
	chamada_id						BIGINT NOT NULL REFERENCES escola.chamadas(chamada_id),
	evento							TIMESTAMP NOT NULL,
	conceito						VARCHAR(1),
	nota							NUMERIC(16,4),
	avaliacao						TEXT,
	presente						BOOLEAN
);

ALTER TABLE escola.chamadas_alunos		ADD CONSTRAINT chamada_aluno_unica UNIQUE(evento,caderno_id,chamada_id);

--Agendar AULA
CREATE TABLE escola.aulas(
	aula_id							SERIAL NOT NULL PRIMARY KEY,
	turma_id						BIGINT NOT NULL REFERENCES public.turmas(turma_id),
	evento							DATE NOT NULL,
	sala_id							BIGINT REFERENCES public.ambientes(ambiente_id),
	aloca_horario					TIME,
	libera_horario					TIME,
	aula_executada					BOOLEAN --Se for verdadeiro, a aula foi dada, se for falso, não teve aula.
);

--Reservar sala 
--Mantem registro das salas para uso conjunto a sala principal, que acontecera na mesma data dentro do horário da aula vinculada a outra sala. Ex: A sala vinculada na aula é a sala 1 e tem uma sala 2(laboratorio) para o segundo momento do turno de encino.
CREATE TABLE escola.sala_auxiliar(
	sala_auxiliar_id				SERIAL NOT NULL PRIMARY KEY,
	aula_id							BIGINT NOT NULL REFERENCES escola.aulas(aula_id),
	sala_id							BIGINT NOT NULL REFERENCES public.ambientes(ambiente_id),
	aloca_horario					TIME NOT NULL,
	libera_horario					TIME NOT NULL
);

--Ate aqui funcionou...
CREATE OR REPLACE FUNCTION escola.criar_caderno() RETURNS trigger AS $$
DECLARE
	turma							BIGINT;
	professor						BIGINT;
	materia							BIGINT;
	periodo							BIGINT;
	periodo_item					BIGINT;
	chamada							BIGINT;
	
	inicio_turma					DATE;
	fim_turma						DATE;

	periodos						public.periodos_letivo_itens%rowtype;
	alunos							public.alunos_turmas%rowtype;
	eventos							TIMESTAMP%TYPE;			
BEGIN
	SELECT turma_id, professor_id,materia_id INTO turma,professor,materia FROM escola.gradecurricular WHERE gradecurricular_id = NEW.gradecurricular_id;
	SELECT inicio,fim INTO inicio_turma,fim_turma FROM public.turmas WHERE turma_id = turma;
--	SELECT periodo_letivo_id INTO periodo_item FROM escola.cadernos where caderno_id = NEW.caderno_id;

	FOR alunos IN
		SELECT * FROM public.alunos_turmas WHERE turma_id = turma
	LOOP
		INSERT INTO escola.chamadas(caderno_id,aluno_id)
			VALUES(NEW.caderno_id,alunos.colaborador_id)
				RETURNING chamada_id INTO chamada;
		
		
		FOR eventos IN
			SELECT data FROM generate_series(inicio_turma,fim_turma,INTERVAL '1 day') AS data
		LOOP
			INSERT INTO escola.chamadas_alunos(evento,caderno_id,chamada_id,presente)
							VALUES(eventos,NEW.caderno_id,chamada,false);
		END LOOP;
	END LOOP;

	RETURN NEW;
END; $$
LANGUAGE plpgsql;


CREATE TRIGGER criar_caderno_tgi AFTER INSERT 
	ON escola.cadernos 
		FOR EACH ROW 
			EXECUTE PROCEDURE escola.criar_caderno();

