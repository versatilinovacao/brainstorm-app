CREATE OR REPLACE VIEW escola.alunos_arquivos_hd_view AS 
SELECT a.arquivo_id,
       aa.aluno_id,
       aa.composicao_aluno_id,
       a.nome,
       a.extensao,
       a.tipo,
       a.tamanho,
       a.registro,
       a.url,
       a.arquivo
FROM escola.alunos_arquivos aa
INNER JOIN arquivos.hd a ON a.arquivo_id = aa.arquivo_id;
