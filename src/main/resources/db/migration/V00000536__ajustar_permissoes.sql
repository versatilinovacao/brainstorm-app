--select * from permissao order by permissao_id desc
update permissao set descricao = t.descricao
from (select tmp.*descricao from (
SELECT * FROM public.dblink ('seguranca_server','select permissao_id,nome,label,descricao from public.permissao')  AS DATA(permissao_id BIGINT, nome VARCHAR, label VARCHAR, descricao VARCHAR)
) as tmp 
left join permissao p on tmp.permissao_id = p.permissao_id) as t
where t.nome = permissao.nome