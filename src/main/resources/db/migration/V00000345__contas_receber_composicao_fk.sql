ALTER TABLE financeiro.contas_pagar	ADD CONSTRAINT composicao_fk	FOREIGN KEY(composicao_id)	REFERENCES financeiro.contas_pagar(contapagar_id);

CREATE INDEX id_composicao_fk	ON financeiro.contas_pagar(contapagar_id,composicao_id);