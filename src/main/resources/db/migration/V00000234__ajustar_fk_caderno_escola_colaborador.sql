ALTER TABLE escola.cadernos ADD COLUMN composicao_escola_id BIGINT;

ALTER TABLE escola.cadernos
	ADD CONSTRAINT escola_fk FOREIGN KEY(escola_id,composicao_escola_id)
	REFERENCES colaborador(colaborador_id,composicao_id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE INDEX fki_caderno_escola_fk
	ON escola.cadernos
	USING btree(escola_id,composicao_escola_id);
