CREATE VIEW data_hora_view AS
	SELECT 'SP' AS sao_paulo,now() AT TIME ZONE 'America/Sao_Paulo'
		UNION ALL
	SELECT 'MST' AS mst,now() AT TIME ZONE 'MST'
		UNION ALL
	SELECT 'UTC' as utc, now() AT TIME ZONE 'UTC';
