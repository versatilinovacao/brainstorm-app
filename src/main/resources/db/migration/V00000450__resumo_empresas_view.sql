INSERT INTO remoto.logradouros(cep,logradouro,complemento,local,bairro_id,cidade_id,pais_id,descricaobairro,descricaocidade,descricaoestado,descricaopais,siglauf,siglapais)
SELECT cep,logradouro,complemento,local,bairro_id,cidade_id,pais_id,descricaobairro,descricaocidade,descricaoestado,descricaopais,siglauf,siglapais FROM remoto.logradouros_view;

ALTER TABLE colaborador		DROP CONSTRAINT colaborador_logradouro_fk;
ALTER TABLE colaborador		ADD CONSTRAINT colaborador_logradouro_fk FOREIGN KEY(cep) REFERENCES remoto.logradouros(cep);

CREATE INDEX bairro_materializado_idx		ON remoto.logradouros	USING btree(bairro_id);
CREATE INDEX cidade_materializado_idx		ON remoto.logradouros	USING btree(cidade_id);
CREATE INDEX estado_materializado_idx		ON remoto.logradouros	USING btree(estado_id);
CREATE INDEX pais_materializado_idx		ON remoto.logradouros	USING btree(pais_id);
CREATE INDEX localidade_materializado_idx	ON remoto.logradouros	USING btree(pais_id,estado_id,cidade_id);
CREATE INDEX rua_materializada_idx		ON remoto.logradouros	USING hash(logradouro);
CREATE INDEX rua_materializada_idx1		ON remoto.logradouros	USING btree(logradouro);
DROP VIEW resumo_empresas_view;

CREATE OR REPLACE VIEW resumo_empresas_view AS 
 SELECT c.colaborador_id AS empresa_id,
    c.composicao_id AS composicao_empresa_id,
    c.nome,
    c.fantasia,
    s.cidade_id,
    c.empresa_id AS empresa_type,
    e.classificacao_empresa_id,
    cf.descricao AS classificacao
   FROM colaborador c
     INNER JOIN empresa.empresa e ON e.empresa_id = c.empresa_id
     LEFT JOIN empresa.classificacao_empresa cf ON cf.classificacao_empresa_id = e.classificacao_empresa_id
     LEFT JOIN logradouro l ON l.cep::text = c.cep::text
     LEFT JOIN bairros b ON b.bairro_id = l.bairro_id
     LEFT JOIN cidades s ON s.cidade_id = b.cidade_id
  WHERE COALESCE(c.empresa_id, 0::bigint) > 0
  ORDER BY c.empresa_id;
