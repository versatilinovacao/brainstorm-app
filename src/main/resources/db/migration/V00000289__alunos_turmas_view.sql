DROP VIEW alunos_turmas_view;

CREATE OR REPLACE VIEW alunos_turmas_view AS 
 SELECT t.colaborador_id,
    a.composicao_id AS composicao_aluno_id,
    a.nome,
    m.codigo AS matricula,
    t.turma_id AS vinculo,
    s.descricao AS status
   FROM alunos_turmas t
     LEFT JOIN colaborador a ON a.colaborador_id = t.colaborador_id
     LEFT JOIN matriculas m ON m.colaborador_id = t.colaborador_id
     LEFT JOIN escola.gradecurricular g ON g.turma_id = t.turma_id AND g.curso_id = m.curso_id
     LEFT JOIN situacao_matricula s ON s.situacao_matricula_id = m.situacao_matricula_id
  WHERE t.turma_id = 6;
