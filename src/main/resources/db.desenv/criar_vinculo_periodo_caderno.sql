﻿CREATE OR REPLACE FUNCTION escola.vinculo_periodo_letivo_caderno() RETURNS trigger AS $$
DECLARE 
	periodo_letivo_v					BIGINT;
	ano_v							VARCHAR(4);
	cadernos						escola.cadernos%ROWTYPE;
BEGIN
	SELECT DATE_PART('year',now()) INTO ano_v;
	SELECT periodo_letivo_id INTO periodo_letivo_v FROM escola.periodos_letivos WHERE ano = ano_v AND status = true;

	FOR cadernos IN 
		SELECT caderno_id FROM escola.cadernos WHERE COALESCE(periodo_letivo_id,0) = 0
	LOOP
		UPDATE escola.cadernos SET periodo_letivo_id = periodo_letivo_v WHERE caderno_id = cadernos_caderno_id;
	END LOOP;
	
	RETURN NEW;
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER vinculo_periodo_letivo_caderno_tgi AFTER INSERT ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE escola.vinculo_periodo_letivo_caderno();
CREATE TRIGGER vinculo_periodo_letivo_caderno_tgu AFTER UPDATE ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE escola.vinculo_periodo_letivo_caderno();
