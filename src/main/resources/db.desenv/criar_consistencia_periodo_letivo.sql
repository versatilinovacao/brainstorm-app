﻿CREATE OR REPLACE FUNCTION critica.periodoletivo_consistencia() RETURNS trigger AS $$
DECLARE
	IsPeriodoLetivoNotFind					BOOLEAN;
	IsPeriodoLetivoFind					BOOLEAN;
	isExiste						BOOLEAN;
	cadernos						escola.cadernos%ROWTYPE;
BEGIN

	SELECT count(1) > 0 INTO IsPeriodoLetivoNotFind FROM escola.periodos_letivos WHERE status = true;
	SELECT count(1) <= 1 INTO IsPeriodoLetivoFind FROM escola.periodos_letivos WHERE status = true;
	IF IsPeriodoLetivoNotFind = true THEN
		FOR cadernos IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = NEW.periodo_letivo_id
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 1 AND caderno_id = cadernos.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos.caderno_id,1,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 1 and caderno_id = cadernos.caderno_id;
			END IF;
		END LOOP;
	END IF;			
	
	IF isPeriodoLetivoFind = true THEN
		FOR cadernos IN
			SELECT caderno_id FROM escola.cadernos WHERE periodo_letivo_id = NEW.periodo_letivo_id
		LOOP
			SELECT COUNT(1) = 0 INTO isExiste FROM escola.cadernos_inconsistencias WHERE consistencia_id = 3 AND caderno_id = cadernos.caderno_id;
			IF isExiste THEN
				INSERT INTO escola.cadernos_inconsistencias(caderno_id,consistencia_id,status) VALUES(cadernos.caderno_id,3,true);
			ELSE
				UPDATE escola.cadernos_inconsistencias SET status = true WHERE consistencia_id = 3 and caderno_id = cadernos.caderno_id;
			END IF;
		END LOOP;
	END IF;

	RETURN NEW;
END $$ LANGUAGE plpgsql;

CREATE TRIGGER periodoletivo_consistencia_tgi AFTER INSERT ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE critica.periodoletivo_consistencia();
CREATE TRIGGER periodoletivo_consistencia_tgu AFTER UPDATE ON escola.periodos_letivos FOR EACH ROW EXECUTE PROCEDURE critica.periodoletivo_consistencia();


/*
	Não pode deixar um periodo letivo ser inativado enquanto existir um ou mais cadernos abertos.
	Quando fechar o caderno, verificar, senão existir mais nenhum caderno do periodo letivo aberto, fechar o periodo letivo.
	Se precisar abrir um caderno fechado e o periodo letivo estiver fechado, abrir o periodo letivo junto com a abertura do caderno, quando fechar o caderno senão tiver outro caderno aberto, fechar o caderno e o periodo novamente
	Se o periodo letivo estiver fechado, não pode mais imprimir nenhum dos caderno do referido periodo letivo


	Quando o caderno precisar ser reaberto e o periodo estiver fechado, se o periodo for do ano atual, usar regras acima
	se o periodo for anterior ao atual, usar rotina para reabertura do periodo e liberação do caderno apenas para reimpressao. 

	Colocar no modal do periodo letivo, fazer aparecer um botão com a descrição caderno que mude o visual todo para um novo formato aonde tera uma grid que list todos os cadernos daquele periodo com um checkbox para
	marcar quais os cadernos serão reimpressos, podendo ser um ou mais de um, talves até mesmo todos do periodo.

	+++++++++++++++
	Criar area para abertura de perido anterior quando precisar abrir um periodo para reimpressao de um ou mais cadernos

	Quando for logar no sistema, verificar se existe um periodo do ano anterior que tenha ficado aberto e tenha sido aberto pelo usuário que esta logando e fechar o período.
	Quando for abrir um periodo letivo anterior verificar se não existe outro usuário com o período aberto, só um usuário pode abrir um período por vez.

*/