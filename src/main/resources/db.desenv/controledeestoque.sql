﻿CREATE TABLE recurso.seguimentos(
	seguimento_id				SERIAL NOT NULL PRIMARY KEY,
	codigo					VARCHAR(1) NOT NULL UNIQUE,
	nome					VARCHAR(100)
);

CREATE TABLE recurso.familias(
	familia_id				SERIAL NOT NULL PRIMARY KEY,
	codigo					VARCHAR(3),
	nome					VARCHAR(100),
	descricao				VARCHAR(500)	
);

CREATE TABLE recurso.classes(
	classe_id				SERIAL NOT NULL PRIMARY KEY,
	codigo					VARCHAR(5),
	nome					VARCHAR(100),
	descricao				VARCHAR(500)
);

DROP TABLE recurso.subclasses;
CREATE TABLE recurso.subclasses(
	subclasse_id				SERIAL NOT NULL PRIMARY KEY,
	codigo					VARCHAR(5),
	nome					VARCHAR(100),
	descricao				VARCHAR(500)
);

CREATE TABLE recurso.marcas(
	marca_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE recurso.unidades_medidas(
	unidade_medida_id			SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	sigla					VARCHAR(5) NOT NULL,
	sped					VARCHAR(5) --sigla da unidade para utilizar no sped
);

CREATE TABLE recurso.finalidades( --Consumo Próprio, Revenda, Produção, Brind, Doação, Locação
	finalidade_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE,
	descricao				VARCHAR(500)
); -- Verificar quais os impostos incidem para cada situação cadastradas aqui dentro.

CREATE TABLE recurso.tipos( --Produzido, Adquirido, Montado
	tipo_id					SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(60) NOT NULL UNIQUE
);

CREATE TABLE recurso.producao( --Própria, Tercerizada
	producao_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(60) NOT NULL UNIQUE
);

CREATE TABLE recurso.estoques(
	estoque_id				SERIAL NOT NULL PRIMARY KEY,
	quantidade				NUMERIC(16,4)
);

CREATE TABLE recurso.tipos_materiais( --Madeira, Ferro, Aluminio, Latão, Concreto, Couro, Terra, Pedra, Cimento, Ouro, Prata, Plastico, Não se aplica
	tipo_material_id			SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE recurso.modelos( --Produto, Serviço, Material, Insumo, Ferramenta, Veiculo, Embalagem
	modelo_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE recurso.grupos( --Normal,Consignado,Comodato,Alugado,Serviço
	grupo_id				SERIAL NOT NULL PRIMARY KEY,
	nome					VARCHAR(100) NOT NULL UNIQUE
);

DROP TABLE recurso.produtos CASCADE;
CREATE TABLE recurso.produtos(
	produto_id				SERIAL NOT NULL PRIMARY KEY,
	produto_composto_id			BIGINT REFERENCES recurso.produtos(produto_id),
	nome					VARCHAR(60) NOT NULL,
	descricao				VARCHAR(200),
	seguimento_id				BIGINT REFERENCES recurso.seguimentos(seguimento_id),
	familia_id				BIGINT REFERENCES recurso.familias(familia_id),
	classe_id				BIGINT REFERENCES recurso.classes(classe_id),
	subclasse_id				BIGINT REFERENCES recurso.subclasses(subclasse_id),
	marca_id				BIGINT REFERENCES recurso.marcas(marca_id),
	unidade_base_id				BIGINT REFERENCES recurso.unidades_medidas(unidade_medida_id),
	finalidade_id				BIGINT REFERENCES recurso.finalidades(finalidade_id),
	tipo_id					BIGINT REFERENCES recurso.tipos(tipo_id),
	producao_id				BIGINT REFERENCES recurso.producao(producao_id),
	estoque_id				BIGINT REFERENCES recurso.estoques(estoque_id),
	tipo_material_id			BIGINT REFERENCES recurso.tipos_materiais(tipo_material_id),
	modelo_id				BIGINT REFERENCES recurso.modelos(modelo_id),
	grupo_id				BIGINT REFERENCES recurso.grupos(grupo_id)
);
