﻿CREATE SCHEMA tempo;
CREATE SCHEMA interface; --Permite definir tabelas com informações de varias tabelas centralizadas em um única, permitindo criar um objeto no java também unico 
						--para dar entrada disparando trigger que será responsável por direcionar as informações para as tabelas separadamente, aproveitando
						--uma única transação.

CREATE TABLE public.dias(
	dia_id						SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE,
	referencia					INTEGER
);
INSERT INTO public.dias(nome,referencia) VALUES('Domingo',1),('Segunda Feira',2),('Terça Feira',3),('Quarta Feira',4),('Quinta Feira',5),('Sexta Feira',6),('Sabado',7);

CREATE TABLE tempo.regimes(
	regime_id					SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);
INSERT INTO tempo.regimes(nome) VALUES('Normal'),('Banco de Horas');

CREATE TABLE tempo.tipo_turnos(
	tipo_turno_id					SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);
INSERT INTO tempo.tipo_turnos(nome) VALUES('Manha'),('Tarde'),('Noite'),('Madrugada');

CREATE TABLE tempo.tipos(
	tipo_id						SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);
INSERT INTO tempo.tipos(nome) VALUES('Normal'),('Sobreaviso'),('Rotativo'),('Escala');

CREATE TABLE tempo.direcionamentos(
	direcionamento_id				SERIAL NOT NULL PRIMARY KEY,
	nome						VARCHAR(100) NOT NULL UNIQUE
);
INSERT INTO tempo.direcionamentos(nome) VALUES('Entrada'),('Saida');
--************************************************************************************

CREATE TABLE tempo.horarios(
	horario_id					SERIAL NOT NULL PRIMARY KEY,
	executar					BOOLEAN
);

CREATE TABLE tempo.quadro_horarios(
	quadro_horario_id				SERIAL NOT NULL PRIMARY KEY,
	horario_id					BIGINT NOT NULL REFERENCES tempo.horarios(horario_id),
	direcionamento_id				BIGINT NOT NULL REFERENCES tempo.direcionamentos(direcionamento_id),
	horario						TIME
	
);

CREATE TABLE tempo.turnos(
	turno_id					SERIAL NOT NULL PRIMARY KEY,
	executar					BOOLEAN
);

CREATE TABLE tempo.quadro_turnos(
	quadro_turno_id					SERIAL NOT NULL PRIMARY KEY,
	tipo_turno_id					BIGINT NOT NULL REFERENCES tempo.tipo_turnos(tipo_turno_id),
	horario_id					BIGINT REFERENCES tempo.horarios(horario_id)
);

CREATE TABLE tempo.semana(
	semana_id					SERIAL NOT NULL PRIMARY KEY,
	executar					BOOLEAN
);

CREATE TABLE tempo.dias(
	dia_id						SERIAL NOT NULL PRIMARY KEY,
	semana_id					BIGINT NOT NULL REFERENCES tempo.semana(semana_id),
	dia_semana_id					BIGINT NOT NULL REFERENCES public.dias(dia_id),
	turno_id					BIGINT REFERENCES tempo.turnos(turno_id)
);

--CRIAR INDICE POR dia_semana_id+turno_id

/*
	Estruturar as VIEWs
*/

--Inicio e Fim, quando preenchidos, permitem definir um prazo de inicio e fim para a jornada de trabalho, tornando possível programar o tempo que ficara ativo.
CREATE TABLE tempo.jornadas(
	jornada_id					SERIAL NOT NULL PRIMARY KEY,
	regime_id					BIGINT NOT NULL REFERENCES tempo.regimes(regime_id),
	tipo_id						BIGINT NOT NULL REFERENCES tempo.tipos(tipo_id),
	nome 						VARCHAR(100) NOT NULL,
	semana_id					BIGINT REFERENCES tempo.semana(semana_id),
	tolerancia_entrada				TIME,
	tolerancia_saida				TIME,
	inicio						DATE,
	fim						DATE
);

CREATE TABLE interface.jornadas(
    jornada_id                     			SERIAL NOT NULL PRIMARY KEY,
    regime_id                      			BIGINT,
    tipo_id                        			BIGINT,
    nome                           			VARCHAR(100),
    tolerancia_entrada					TIME,
    tolerancia_saida					TIME,
    inicio						DATE,
    fim							DATE

    segunda                        BOOLEAN,
    terca                          BOOLEAN,
    quarta                         BOOLEAN,
    quinta                         BOOLEAN,
    sexta                          BOOLEAN,
    sabado                         BOOLEAN,
    domingo                        BOOLEAN,

    segunda_manha                  BOOLEAN,
    segunda_tarde                  BOOLEAN,
    segunda_noite                  BOOLEAN,
    segunda_madrugada              BOOLEAN,

    terca_manha                    BOOLEAN,
    terca_tarde                    BOOLEAN,
    terca_noite                    BOOLEAN,
    terca_madrugada                BOOLEAN,

    quarta_manha                   BOOLEAN,
    quarta_tarde                   BOOLEAN,
    quarta_noite                   BOOLEAN,
    quarta_madrugada               BOOLEAN,

    quinta_manha                   BOOLEAN,
    quinta_tarde                   BOOLEAN,
    quinta_noite                   BOOLEAN,
    quinta_madrugada               BOOLEAN,

    sexta_manha                    BOOLEAN,
    sexta_tarde                    BOOLEAN,
    sexta_noite                    BOOLEAN,
    sexta_madrugada                BOOLEAN,

    sabado_manha                   BOOLEAN,
    sabado_tarde                   BOOLEAN,
    sabado_noite                   BOOLEAN,
    sabado_madrugada               BOOLEAN,

    domingo_manha                  BOOLEAN,
    domingo_tarde                  BOOLEAN,
    domingo_noite                  BOOLEAN,
    domingo_madrugada              BOOLEAN,

    segunda_manha_entrada          BOOLEAN,
    segunda_tarde_entrada          BOOLEAN,
    segunda_noite_entrada          BOOLEAN,
    segunda_madrugada_entrada      BOOLEAN,

    segunda_manha_saida            BOOLEAN,
    segunda_tarde_saida            BOOLEAN,
    segunda_noite_saida            BOOLEAN,
    segunda_madrugada_saida        BOOLEAN,

    terca_manha_entrada            BOOLEAN,
    terca_tarde_entrada            BOOLEAN,
    terca_noite_entrada            BOOLEAN,
    terca_madrugada_entrada        BOOLEAN,

    terca_manha_saida              BOOLEAN,
    terca_tarde_saida              BOOLEAN,
    terca_noite_saida              BOOLEAN,
    terca_madrugada_saida          BOOLEAN,

    quarta_manha_entrada           BOOLEAN,
    quarta_tarde_entrada           BOOLEAN,
    quarta_noite_entrada           BOOLEAN,
    quarta_madrugada_entrada       BOOLEAN,

    quarta_manha_saida             BOOLEAN,
    quarta_tarde_saida             BOOLEAN,
    quarta_noite_saida             BOOLEAN,
    quarta_madrugada_saida         BOOLEAN,

    quinta_manha_entrada           BOOLEAN,
    quinta_tarde_entrada           BOOLEAN,
    quinta_noite_entrada           BOOLEAN,
    quinta_madrugada_entrada       BOOLEAN,

    quinta_manha_saida             BOOLEAN,
    quinta_tarde_saida             BOOLEAN,
    quinta_noite_saida             BOOLEAN,
    qiunta_madrugada_saida         BOOLEAN,

    sexta_manha_entrada            BOOLEAN,
    sexta_tarde_entrada            BOOLEAN,
    sexta_noite_entrada            BOOLEAN,
    sexta_madrugada_entrada        BOOLEAN,

    sexta_manha_saida              BOOLEAN,
    sexta_tarde_saida              BOOLEAN,
    sexta_noite_saida              BOOLEAN,
    sexta_madrugada_saida          BOOLEAN,

    sabado_manha_entrada           BOOLEAN,
    sabado_tarde_entrada           BOOLEAN,
    sabado_noite_entrada           BOOLEAN,
    sabado_madrugada_entrada       BOOLEAN,

    sabado_manha_saida             BOOLEAN,
    sabado_tarde_saida             BOOLEAN,
    sabado_noite_saida             BOOLEAN,
    sabado_madrugada_saida         BOOLEAN,

    domingo_manha_entrada          BOOLEAN,
    domingo_tarde_entrada          BOOLEAN,
    domingo_noite_entrada          BOOLEAN,
    domingo_madrugada_entrada      BOOLEAN,

    domingo_manha_saida            BOOLEAN,
    domingo_tarde_saida            BOOLEAN,
    domingo_noite_saida            BOOLEAN,
    domingo_madrugada_saida        BOOLEAN

);

/*
http://pgdocptbr.sourceforge.net/pg80/ecpg-errors.html
http://pgdocptbr.sourceforge.net/pg80/errcodes-appendix.html
COMANDO PARA PEGAR VALORES SIMILARES
https://postgres.cz/wiki/PostgreSQL_SQL_Tricks
*/

CREATE OR REPLACE FUNCTION interface.executar_jornada() RETURNS trigger AS $$
DECLARE
    not_existe                    BOOLEAN;
    jornada_v                     BIGINT;
    semana_v                      BIGINT;
    turno_v                       BIGINT;
    horario_v                     BIGINT;

                        
BEGIN
    BEGIN
	
	INSERT INTO tempo.semana(executar)   VALUES(true) RETURNING semana_id IN semana_v;
	INSERT INTO tempo.turnos(executar)   VALUES(true) RETURNING turno_id IN turno_v;
	INSERT INTO tempo.horarios(executar) VALUES(true) RETURNING horario_id IN horario_v;

	INSERT INTO tempo.jornadas(nome,regime_id,tipo_id,tolerancia_entrada,tolerancia_saida,inicio,fim,semana_id) 
	VALUES(NEW.nome,NEW.regime_id,NEW.tolerancia_entrada,NEW.tolerancia_saida,NEW.inicio,NEW.fim,semana_v)
	RETURNING jornada_id IN jornada_v;
	
	CREATE TEMP TABLE semanas_t(
		semana_t_id			SERIAL NOT NULL PRIMARY KEY,
		identificador			BIGINT NOT NULL UNIQUE
	);

	CREATE TEMP TABLE turnos_t(
		turnos_t_id			SERIAL NOT NULL PRIMARY KEY,
		identificador_semana		BIGINT NOT NULL,
		identificador			BIGINT
	);

	IF domingo THEN
		INSERT INTO semanas_t(identificador) VALUES(1);
	END IF;
	IF segunda THEN
		INSERT INTO semanas_t(identificador) VALUES(2);
	END IF;
	IF terca THEN
		INSERT INTO semanas_t(identificador) VALUES(3);
	END IF;
	IF quarta THEN
		INSERT INTO semanas_t(identificador) VALUES(4);
	END IF;
	IF quinta THEN
		INSERT INTO semanas_t(identificador) VALUES(5);
	END IF;
	IF sexta THEN
		INSERT INTO semanas_t(identificador) VALUES(6);
	END IF;
	IF sabado THEN
		INSERT INTO semanas_t(identificador) VALUES(7);
	END IF;
	select * from tempo.tipo_turnos

	IF NEW.domingo_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(1,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.domingo_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(1,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.domingo_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(1,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.domingo_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(1,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;
	
	IF NEW.segunda_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(2,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.segunda_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(2,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.segunda_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(2,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.segunda_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(2,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	IF NEW.terca_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(3,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.terca_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(3,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.terca_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(3,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.terca_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(3,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	IF NEW.quarta_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(4,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.quarta_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(4,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.quarta_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(4,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.quarta_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(4,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	IF NEW.quinta_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(5,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.quinta_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(5,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.quinta_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(5,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.quinta_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(5,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	IF NEW.sexta_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(6,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.sexta_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(6,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.sexta_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(6,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.sexta_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(6,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	IF NEW.sabado_manha THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(7,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MANHA');
	END IF;
	IF NEW.sabado_tarde THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(7,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'TARDE');
	END IF;
	IF NEW.sabado_noite THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(7,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'NOITE');
	END IF;
	IF NEW.sabado_madrugada THEN
		INSERT INTO turnos_t(identificador_semana,identificador) VALUES(7,SELECT tipo_turno_id FROM tempo.tipo_turnos WHERE UPPER(nome) = 'MADRUGADA');
	END IF;

	FOR semanas_for IN
		SELECT semana_t_id,descricao FROM semanas_t
	LOOP 
		INSERT INTO tempo.dias(semana_id,dia_semana_id,turno_id) VALUES(semana_v,NEW.dia_id,turno_v);
		FOR turnos_for IN 
			SELECT turnos_t_id,
			       identificador_semana,
		               identificador
			FROM turnos_t
		LOOP
			INSERT INTO tempo.quadro_turnos(tipo_turno_id,horario_id) VALUES(NEW.turnos_t_id,horario_v);
			FOR horarios_for IN 
				SELECT * FROM horarios_t
			LOOP
				--INSERT INTO tempo.horarios(horario_id,direcionamento_id,horario) VALUES(horario_v,horarios_for.direcionamento_id,horarios_for.horario);
			END LOOP;
		END LOOP;
		
	END LOOP;
	
    EXCEPTION 
        WHEN OTHERS THEN
            RAISE EXCEPTION "Erro na criação da jornada de trabalho!";
            RETURN NEW;    
    END;

    DELETE interface.jornadas WHERE joranada_id = NEW.joranada_id;
    RETURN NEW;    
END; $$ LANGUAGE plpgsql;

CREATE TRIGGER executar_jornada_tgi ON interface.executar_jornada 
    FOR EACH 













