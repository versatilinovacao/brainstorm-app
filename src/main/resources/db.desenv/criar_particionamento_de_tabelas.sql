DROP SCHEMA IF EXISTS teste;
CREATE SCHEMA teste;

DROP SCHEMA IF EXISTS servidor;
CREATE SCHEMA servidor;

DROP SCHEMA IF EXISTS particao_colaborador;
CREATE SCHEMA particao_colaborador;

DROP TABLE IF EXISTS configuracao.parametro_particao;
CREATE TABLE configuracao.parametro_particao(
	nome_tabela_esquema			VARCHAR(64) NOT NULL,
	nome_tabela				VARCHAR(64) NOT NULL,
	nome_coluna_chave			VARCHAR(64) NOT NULL,
	nome_tablespace				VARCHAR(64) NOT NULL DEFAULT 'pg_default',
	qt_chave_ativa				INTEGER DEFAULT 5,
	qt_chave_excluir			INTEGER,
	qt_chave_desfilhar			INTEGER,
	ordem_criacao				INTEGER,
	CONSTRAINT parametro_particao_pk  PRIMARY KEY(nome_tabela_esquema, nome_tabela),
	CONSTRAINT parametro_particao_ck1 CHECK(qt_chave_ativa > 1),
	CONSTRAINT parametro_particao_ck2 CHECK(qt_chave_excluir   > qt_chave_ativa AND qt_chave_excluir   < 100),
	CONSTRAINT parametro_particao_ck3 CHECK(qt_chave_desfilhar > qt_chave_ativa AND qt_chave_desfilhar < 100)
);

ALTER TABLE configuracao.parametro_particao OWNER TO postgres;

COMMENT ON TABLE configuracao.parametro_particao
  IS 'Cadastro de tabelas particionadas e parâmetros de ajuste de manutenção das mesmas';
COMMENT ON COLUMN configuracao.parametro_particao.nome_tabela_esquema IS 'Nome do esquema da tabela particionada';
COMMENT ON COLUMN configuracao.parametro_particao.nome_tabela         IS 'Nome da tabela particionada';
COMMENT ON COLUMN configuracao.parametro_particao.nome_coluna_chave   IS 'Nome do campo da tabela que será utilizado como chave para o particionamento';
COMMENT ON COLUMN configuracao.parametro_particao.nome_tablespace     IS 'Nome do tablespace onde a partição será criada';
COMMENT ON COLUMN configuracao.parametro_particao.qt_chave_ativa      IS 'Quantidade de chaves ativas no gatilho de INSERT da tabela mãe';
COMMENT ON COLUMN configuracao.parametro_particao.qt_chave_excluir    IS 'Quantidade de partições a serem mantidas. Serão mantidas "qt_chaves_apagar" com "no_coluna_chave" maiores, as demais serão excluídas. O valor NULL desativa a esclusão de partições';
COMMENT ON COLUMN configuracao.parametro_particao.qt_chave_desfilhar  IS 'Quantidade de partições a serem desfilhadas (deixar de ser uma partição filha da tabela mãe). Serão mantidas "qt_chaves_desfilhar" com "no_coluna_chave" maiores, as demais serão desfilhadas. O valor NULL desativa a desfilhação.';
COMMENT ON COLUMN configuracao.parametro_particao.ordem_criacao       IS 'Órdem de processamento das tabelas. Utilizado para respeitar as FKs das partições.';

--DELETE FROM configuracao.parametro_particao
--INSERT INTO configuracao.parametro_particao VALUES ('teste', 'pedido', 'ano_pedido', 'pedido', 5, 99, 50, 1);
--INSERT INTO configuracao.parametro_particao VALUES ('teste', 'pedido_detalhe', 'ano_pedido', 'pedido_detalhe', 5, 99, 50, 2);

INSERT INTO configuracao.parametro_particao VALUES ('particao_colaborador', 'colaborador', 'colaborador_id', 'colaborador', 5, 99, 50, 1);
--SELECT * FROM configuracao.parametro_particao

-- Function: public.cria_particao_tabela(integer, boolean, boolean)
DROP FUNCTION IF EXISTS servidor.cria_particao_tabela_colaborador(integer, boolean, boolean);
CREATE OR REPLACE FUNCTION servidor.criar_particao_tabela_colaborador(
		IN p_next_partition_key integer, --Número da nova partição
		IN p_archive boolean,            --Colocar como NO INHERIT tabelas marcadas com 'qt_chaves_desfiliar'
		IN p_drop boolean,               --Apagar partições mais antigas que 'qt_chaves_apagar'
		OUT v_cod_erro text,             --Mensagem de erro
		OUT v_pos_erro integer)          --Posição do erro no código
	RETURNS record AS $$
--
-- Descrição: Cria novas partições nas tabelas cadastradas em 'parametro_particao'
-- Autor: Fábio Telles Rodriguez
--
DECLARE
	v_sql                       text;
	v_tmp                       integer;
	c_pp                        record;
	c_part                      record;

	-- Pegar FKs de tabelas não particionadas que apontam para tabelas particionadas
	c_loop_const_fora           CURSOR FOR SELECT tc.conname AS const FROM pg_constraint AS tc,
									       pg_class      AS c,
									       pg_class      AS r
									  WHERE tc.contype   = 'f'
									    AND tc.conrelid  = c.oid
									    AND tc.confrelid = r.oid
									    AND c.relname NOT IN (SELECT nome_tabela FROM configuracao.parametro_particao)
									    AND r.relname IN     (SELECT nome_tabela FROM configuracao.parametro_particao)
									    AND NOT EXISTS       (SELECT 1 FROM pg_inherits i WHERE  i.inhrelid = c.oid);
	
	-- Criar FKs de tabelas particionadas que apontam para outras tabelas particionadas
	
					


	c_particao_a			INTEGER;
	c_particao_b			INTEGER;
	c_particao_c			INTEGER;
	c_particao_d			INTEGER;
	c_particao_e			INTEGER;
	c_particao_f			INTEGER;
	c_particao_g			INTEGER;
	c_particao_h			INTEGER;
	c_particao_i			INTEGER;
	c_particao_j			INTEGER;
	c_particao_l			INTEGER;
	c_particao_m			INTEGER;
	c_particao_n			INTEGER;
	c_particao_o			INTEGER;
	c_particao_p			INTEGER;
	c_particao_q			INTEGER;
	c_particao_r			INTEGER;
	c_particao_s			INTEGER;
	c_particao_t			INTEGER;
	c_particao_u			INTEGER;
	c_particao_v			INTEGER;
	c_particao_x			INTEGER;
	c_particao_z			INTEGER;
	c_particao_w			INTEGER;
	c_particao_y			INTEGER;
	c_particao_k			INTEGER;

BEGIN
	v_cod_erro := 0;
	v_pos_erro := 0;
	RAISE NOTICE 'cria_particao_tabela|Begin';

	--Verifica quais as partições deve criar, com base na existencia de um nome que começe com cada letra...
	SELECT count(1) INTO c_particao_a FROM colaborador WHERE UPPER(nome) LIKE 'A%';
	SELECT count(1) INTO c_particao_b FROM colaborador WHERE UPPER(nome) LIKE 'B%';
	SELECT count(1) INTO c_particao_c FROM colaborador WHERE UPPER(nome) LIKE 'C%';
	SELECT count(1) INTO c_particao_d FROM colaborador WHERE UPPER(nome) LIKE 'D%';

	SELECT count(1) INTO c_particao_e FROM colaborador WHERE UPPER(nome) LIKE 'E%';
	SELECT count(1) INTO c_particao_f FROM colaborador WHERE UPPER(nome) LIKE 'F%';
	SELECT count(1) INTO c_particao_g FROM colaborador WHERE UPPER(nome) LIKE 'G%';
	SELECT count(1) INTO c_particao_h FROM colaborador WHERE UPPER(nome) LIKE 'H%';

	SELECT count(1) INTO c_particao_i FROM colaborador WHERE UPPER(nome) LIKE 'I%';
	SELECT count(1) INTO c_particao_j FROM colaborador WHERE UPPER(nome) LIKE 'J%';
	SELECT count(1) INTO c_particao_l FROM colaborador WHERE UPPER(nome) LIKE 'L%';
	SELECT count(1) INTO c_particao_m FROM colaborador WHERE UPPER(nome) LIKE 'M%';

	SELECT count(1) INTO c_particao_n FROM colaborador WHERE UPPER(nome) LIKE 'N%';
	SELECT count(1) INTO c_particao_o FROM colaborador WHERE UPPER(nome) LIKE 'O%';
	SELECT count(1) INTO c_particao_p FROM colaborador WHERE UPPER(nome) LIKE 'P%';
	SELECT count(1) INTO c_particao_q FROM colaborador WHERE UPPER(nome) LIKE 'Q%';

	SELECT count(1) INTO c_particao_r FROM colaborador WHERE UPPER(nome) LIKE 'R%';
	SELECT count(1) INTO c_particao_s FROM colaborador WHERE UPPER(nome) LIKE 'S%';
	SELECT count(1) INTO c_particao_t FROM colaborador WHERE UPPER(nome) LIKE 'T%';
	SELECT count(1) INTO c_particao_u FROM colaborador WHERE UPPER(nome) LIKE 'U%';

	SELECT count(1) INTO c_particao_v FROM colaborador WHERE UPPER(nome) LIKE 'V%';
	SELECT count(1) INTO c_particao_x FROM colaborador WHERE UPPER(nome) LIKE 'X%';
	SELECT count(1) INTO c_particao_z FROM colaborador WHERE UPPER(nome) LIKE 'Z%';
	SELECT count(1) INTO c_particao_w FROM colaborador WHERE UPPER(nome) LIKE 'W%';

	SELECT count(1) INTO c_particao_y FROM colaborador WHERE UPPER(nome) LIKE 'Y%';
	SELECT count(1) INTO c_particao_k FROM colaborador WHERE UPPER(nome) LIKE 'K%';

	-- Verifica se alguma FK de tabela não particionada aponta para uma tabela particionada
	FOR c_part IN c_loop_const_fora LOOP
		v_pos_erro := 10;
		RAISE EXCEPTION 'cria_particao_tabela| A constraint % aponta para uma tabela particionada mas não será particionada também!!!', c_part.const;
	END LOOP;

	IF c_particao_a	> 0 AND NOT EXISTS particao_colaborador.colaborador_a THEN		
		CREATE TABLE particao_colaborador.colaborador_a AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'A%';
	END IF;

	IF c_particao_b	> 0 AND NOT EXISTS particao_colaborador.colaborador_b  THEN
		CREATE TABLE particao_colaborador.colaborador_b AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'B%';
	END IF;

	IF c_particao_c	> 0 AND NOT EXISTS particao_colaborador.colaborador_c THEN			
		CREATE TABLE particao_colaborador.colaborador_c AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'C%';
	END IF;

	IF c_particao_d	> 0 AND NOT EXISTS particao_colaborador.colaborador_d THEN			
		CREATE TABLE particao_colaborador.colaborador_d AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'D%';
	END IF;

	IF c_particao_e	> 0 AND NOT EXISTS particao_colaborador.colaborador_e THEN			
		CREATE TABLE particao_colaborador.colaborador_e AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'E%';
	END IF;

	IF c_particao_f	> 0 AND NOT EXISTS particao_colaborador.colaborador_f THEN			
		CREATE TABLE particao_colaborador.colaborador_f AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'F%';
	END IF;

	IF c_particao_g	> 0 AND NOT EXISTS particao_colaborador.colaborador_g THEN			
		CREATE TABLE particao_colaborador.colaborador_g AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'G%';
	END IF;

	IF c_particao_h	> 0 AND NOT EXISTS particao_colaborador.colaborador_h THEN			
		CREATE TABLE particao_colaborador.colaborador_h AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'H%';
	END IF;

	IF c_particao_i	> 0 AND NOT EXISTS particao_colaborador.colaborador_i THEN			
		CREATE TABLE particao_colaborador.colaborador_i AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'I%';
	END IF;

	IF c_particao_j	> 0 AND NOT EXISTS particao_colaborador.colaborador_j THEN			
		CREATE TABLE particao_colaborador.colaborador_j AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'J%';
	END IF;

	IF c_particao_l	> 0 AND NOT EXISTS particao_colaborador.colaborador_l THEN			
		CREATE TABLE particao_colaborador.colaborador_l AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'L%';
	END IF;

	IF c_particao_m	> 0 AND NOT EXISTS particao_colaborador.colaborador_m THEN			
		CREATE TABLE particao_colaborador.colaborador_m AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'M%';
	END IF;

	IF c_particao_n	> 0 AND NOT EXISTS particao_colaborador.colaborador_n THEN			
		CREATE TABLE particao_colaborador.colaborador_n AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'N%';
	END IF;

	IF c_particao_o	> 0 AND NOT EXISTS particao_colaborador.colaborador_o THEN			
		CREATE TABLE particao_colaborador.colaborador_o AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'O%';
	END IF;

	IF c_particao_p	> 0 AND NOT EXISTS particao_colaborador.colaborador_p THEN			
		CREATE TABLE particao_colaborador.colaborador_p AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'P%';
	END IF;

	IF c_particao_q	> 0 AND NOT EXISTS particao_colaborador.colaborador_q THEN			
		CREATE TABLE particao_colaborador.colaborado_q AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'Q%';
	END IF;

	IF c_particao_r	> 0 AND NOT EXISTS particao_colaborador.colaborador_r THEN			
		CREATE TABLE particao_colaborador.colaborador_r AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'R%';
	END IF;

	IF c_particao_s	> 0 AND NOT EXISTS particao_colaborador.colaborador_s THEN			
		CREATE TABLE particao_colaborador.colaborador_s AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'S%';
	END IF;

	IF c_particao_t	> 0 AND NOT EXISTS particao_colaborador.colaborador_t THEN			
		CREATE TABLE particao_colaborador.colaborador_t AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'T%';
	END IF;

	IF c_particao_u	> 0 AND NOT EXISTS particao_colaborador.colaborador_u THEN			
		CREATE TABLE particao_colaborador.colaborador_u AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'U%';
	END IF;

	IF c_particao_v	> 0 AND NOT EXISTS particao_colaborador.colaborador_v THEN			
		CREATE TABLE particao_colaborador.colaborador_v AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'V%';
	END IF;

	IF c_particao_x	> 0 AND NOT EXISTS particao_colaborador.colaborador_x THEN			
		CREATE TABLE particao_colaborador.colaborador_x AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'X%';
	END IF;

	IF c_particao_z	> 0 AND NOT EXISTS particao_colaborador.colaborador_z THEN			
		CREATE TABLE particao_colaborador.colaborador_z AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'Z%';
	END IF;

	IF c_particao_w	> 0 AND NOT EXISTS particao_colaborador.colaborador_w THEN			
		CREATE TABLE particao_colaborador.colaborador_w AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'W%';
	END IF;

	IF c_particao_y	> 0 AND NOT EXISTS particao_colaborador.colaborador_y THEN			
		CREATE TABLE particao_colaborador.colaborador_y AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'Y%';
	END IF;

	IF c_particao_k	> 0 AND NOT EXISTS particao_colaborador.colaborador_k THEN
		CREATE TABLE particao_colaborador.colaborador_k AS SELECT * FROM colaborador WHERE UPPER(nome) LIKE 'K%';
	END IF;
			
	

EXCEPTION
	WHEN OTHERS            THEN
		v_cod_erro  := SUBSTR(SQLERRM,1,2000);
		RAISE WARNING 'cria_particao_tabela|ERRO!!! POS: %, COD: %, MSG: %', v_pos_erro, SQLSTATE, v_cod_erro;
END; $$
LANGUAGE plpgsql VOLATILE SECURITY DEFINER
COST 100000;








CREATE OR REPLACE FUNCTION public.cria_particao_tabela(
    IN p_next_partition_key integer, --Número da nova partição
    IN p_archive boolean,            --Colocar como NO INHERIT tabelas marcadas com 'qt_chaves_desfiliar'
    IN p_drop boolean,               --Apagar partições mais antigas que 'qt_chaves_apagar'
    OUT v_cod_erro text,             --Mensagem de erro
    OUT v_pos_erro integer)          --Posição do erro no código
  RETURNS record AS
$BODY$
--
-- Descrição: Cria novas partições nas tabelas cadastradas em 'parametro_particao'
-- Autor: Fábio Telles Rodriguez
--
DECLARE
    v_sql                       text;
    v_tmp                       integer;
    c_pp                        record;
    c_part                      record;
 
    -- Pegar FKs de tabelas não particionadas que apontam para tabelas particionadas
    c_loop_const_fora           CURSOR FOR SELECT tc.conname AS const
        FROM
            pg_constraint AS tc,
            pg_class      AS c,
            pg_class      AS r
        WHERE
                tc.contype   = 'f'
            AND tc.conrelid  = c.oid
            AND tc.confrelid = r.oid
            AND c.relname NOT IN (SELECT nome_tabela FROM parametro_particao)
            AND r.relname IN     (SELECT nome_tabela FROM parametro_particao)
            AND NOT EXISTS       (SELECT 1 FROM pg_inherits i WHERE  i.inhrelid = c.oid);
 
    -- Criar FKs de tabelas particionadas que apontam para outras tabelas particionadas
    c_loop_const_part           CURSOR FOR
        SELECT 'ALTER TABLE ' || pp.nome_tabela_esquema || '.' || pp.nome_tabela || '_' || p_next_partition_key ||
            ' ADD CONSTRAINT ' || conname || '_' || p_next_partition_key || ' ' ||
            replace(pg_get_constraintdef(tc.oid),
                r.relname, r.relname || '_' || p_next_partition_key) ||
            ';' AS alter_c
        FROM
            pg_constraint AS tc,
            pg_class      AS c,
            pg_class      AS r,
            pg_namespace  AS nc,
            pg_namespace  AS nr,
            parametro_particao AS pp
        WHERE
                tc.contype   = 'f'
            AND tc.conrelid  = c.oid
            AND nc.oid       = c.relnamespace
            AND c.relname    = pp.nome_tabela
            AND nc.nspname   = pp.nome_tabela_esquema
            AND tc.confrelid = r.oid
            AND nr.oid       = r.relnamespace
            AND EXISTS (SELECT 1 FROM parametro_particao rpp
                                WHERE
                                    nr.nspname = rpp.nome_tabela_esquema AND
                                    r.relname  = rpp.nome_tabela);
 
    -- Criar FKs de tabelas particionadas que apontam para tabelas não particionadas
    c_loop_constraint           CURSOR (
                                    p_table_schema varchar,
                                    p_table_name varchar) FOR
        SELECT 'ALTER TABLE ' || p_table_schema || '.' || p_table_name || '_' || p_next_partition_key ||
            ' ADD CONSTRAINT ' || conname || '_' || p_next_partition_key || ' ' || pg_get_constraintdef(tc.oid) || ';' AS alter_c
        FROM
            pg_constraint AS tc,
            pg_class      AS c,
            pg_class      AS r,
            pg_namespace  AS nc,
            pg_namespace  AS nr,
            parametro_particao AS pp
        WHERE
                tc.contype   = 'f'
            AND tc.conrelid  = c.oid
            AND nc.nspname   = p_table_schema
            AND nc.nspname   = pp.nome_tabela_esquema
            AND nc.oid       = c.relnamespace
            AND c.relname    = p_table_name
            AND c.relname    = pp.nome_tabela
            AND tc.confrelid = r.oid
            AND nr.oid       = r.relnamespace
            AND NOT EXISTS (SELECT 1 FROM parametro_particao rpp
                                WHERE
                                    nr.nspname = rpp.nome_tabela_esquema AND
                                    r.relname  = rpp.nome_tabela);
 
    -- Criar condições na função do gatilho para demais partições já existentes
    c_loop_partitions           CURSOR (
                                    p_part_col     varchar,
                                    p_table_schema varchar,
                                    p_table_name   varchar,
                                    p_key_active   integer ) FOR
        SELECT 'ELSIF ( NEW.' || p_part_col || ' = ' || replace(r.relname, c.relname || '_', '') || ') THEN ' || chr(10)
            || '    INSERT INTO ' || p_table_schema || '.' || r.relname || ' VALUES (NEW.*);' || chr(10) AS sql
            FROM
                pg_inherits i
                JOIN pg_class c     ON i.inhparent = c.oid
                JOIN pg_namespace n ON c.relnamespace = n.oid
                JOIN pg_class r     ON i.inhrelid    = r.oid
            WHERE
                c.relname = p_table_name AND
                n.nspname = p_table_schema AND
                r.relname != (p_table_name || '_' || p_next_partition_key)
            ORDER BY to_number(replace(r.relname, c.relname || '_', ''),'999999') DESC
            LIMIT p_key_active -1;
    c_loop_grant          CURSOR (
                                    p_table_schema varchar,
                                    p_table_name   varchar) FOR
        SELECT 'GRANT ' || privilege_type || ' ON ' || table_schema || '.' || table_name || '_' || p_next_partition_key || ' TO ' || grantee ||
            CASE is_grantable WHEN 'YES' THEN ' WITH GRANT OPTION' ELSE '' END || ';' sql
            FROM information_schema.table_privileges
            WHERE
                table_schema = p_table_schema AND
                table_name = p_table_name;
 
    -- Desfiliar partições com mais de 'qt_chave_desfiliar' além da última
    c_loop_arc            CURSOR (
                                    p_table_schema varchar,
                                    p_table_name   varchar,
                                    p_key_arc     integer) FOR
        SELECT 'ALTER TABLE  ' || p_table_schema || '.' || r.relname || ' NO INHERIT ' || p_table_schema || '.' || p_table_name || ';' AS sql
            FROM
                pg_inherits i
                JOIN pg_class c     ON i.inhparent = c.oid
                JOIN pg_namespace n ON c.relnamespace = n.oid
                JOIN pg_class r     ON i.inhrelid    = r.oid
            WHERE
                c.relname = p_table_name AND
                n.nspname = p_table_schema
            ORDER BY to_number(replace(r.relname, c.relname || '_', ''),'999999') DESC
            OFFSET p_key_arc;
 
    -- Apagar partições com mais de 'qt_chave_excluir' além da última
    c_loop_drop           CURSOR (
                                    p_table_schema varchar,
                                    p_table_name   varchar,
                                    p_key_drop     integer) FOR
        SELECT 'DROP TABLE IF EXISTS ' || p_table_schema || '.' || r.relname || ';' AS sql
            FROM
                pg_inherits i
                JOIN pg_class c     ON i.inhparent = c.oid
                JOIN pg_namespace n ON c.relnamespace = n.oid
                JOIN pg_class r     ON i.inhrelid    = r.oid
            WHERE
                c.relname = p_table_name AND
                c.relkind = 'r' AND
                n.nspname = p_table_schema
            ORDER BY to_number(replace(r.relname, c.relname || '_', ''),'999999') DESC
            OFFSET p_key_drop;
 
    -- Carregar dados de parametro_particao
    c_tabelas           CURSOR FOR SELECT
            nome_tabela_esquema  AS tbl_sch,
            nome_tabela          AS tbl_nom,
            nome_coluna_chave    AS par_col,
            nome_tablespace      AS tbs_def,
            qt_chave_ativa       AS key_act,
            qt_chave_excluir     AS key_drp,
            qt_chave_desfilhar   AS key_arc,
            ordem_criacao        AS tbl_ord
        FROM parametro_particao
        ORDER BY ordem_criacao;
 
-- Chega de declarações, vamos trabalhar!!!
BEGIN
    v_cod_erro := 0;
    v_pos_erro := 0;
    RAISE NOTICE 'cria_particao_tabela|Begin';
 
    -- Verifica se alguma FK de tabela não particionada aponta para uma tabela particionada
    FOR c_part IN c_loop_const_fora LOOP
        v_pos_erro := 10;
        RAISE EXCEPTION 'cria_particao_tabela| A constraint % aponta para uma tabela particionada mas não será particionada também!!!', c_part.const;
    END LOOP;
 
    -- Carrega dados no cadastro de particionamento
    v_pos_erro := 20;
    FOR c_pp IN c_tabelas LOOP
 
        -- A tabela mãe existe?
        v_pos_erro := 30;
        SELECT 1
            INTO v_tmp
            FROM information_schema.tables
            WHERE
                table_catalog   = current_database() AND
                table_schema    = c_pp.tbl_sch  AND
                table_name      = c_pp.tbl_nom;
        IF NOT FOUND THEN
            v_pos_erro := 40;
            RAISE EXCEPTION 'cria_particao_tabela|A Tabela Mãe % configurada em tbom_parametro_particao não existe', c_pp.tbl_nom;
        END IF;
 
        -- Verifica se o tablespace existe
        v_pos_erro := 50;
        SELECT 1
            INTO v_tmp
            FROM pg_catalog.pg_tablespace
            WHERE spcname = c_pp.tbs_def;
        IF NOT FOUND THEN
            v_pos_erro := 60;
            RAISE EXCEPTION 'cria_particao_tabela|A tablespace para a particao da tabela % não existe', c_pp.tbl_nom ;
        END IF;
 
        -- Verifica se a particao da tabela ja existe
        v_pos_erro := 70;
        SELECT 1
            INTO v_tmp
            FROM information_schema.tables
            WHERE
                table_name = c_pp.tbl_nom || '_' || p_next_partition_key AND
                table_schema = c_pp.tbl_sch;
        IF FOUND THEN
            v_pos_erro := 80;
            RAISE EXCEPTION 'cria_particao_tabela|A partição da tabela % com chave % de valor % já existe',
                c_pp.tbl_sch, c_pp.par_col, p_next_partition_key ;
        END IF;
 
        -- Cria a partição da tabela
        v_pos_erro := 90;
        RAISE WARNING 'cria_particao_tabela|Cria particao %', c_pp.tbl_nom || '_' || p_next_partition_key;
 
        v_sql := 'CREATE TABLE ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || '(LIKE ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom
            || ' INCLUDING STORAGE INCLUDING INDEXES INCLUDING DEFAULTS INCLUDING CONSTRAINTS ) INHERITS ('
            || c_pp.tbl_sch || '.' || c_pp.tbl_nom || ') TABLESPACE ' || c_pp.tbs_def;
        v_pos_erro := 100;
        EXECUTE v_sql;
 
        -- Ajusta parâmetros de STORAGE na partição criada
        v_pos_erro := 110;
        SELECT 'ALTER TABLE '  || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || ' SET (' || array_to_string(c.reloptions,',') || ');' AS sql
            INTO v_sql
            FROM
                pg_class c
                JOIN pg_namespace n ON c.relnamespace = n.oid
            WHERE
                c.relname = c_pp.tbl_nom AND
                n.nspname = c_pp.tbl_sch;
        IF FOUND AND v_sql IS NOT NULL THEN
            v_pos_erro := 120;
            EXECUTE v_sql;
        END IF;
 
        -- Remove restrição NO INHERIT da tabela filha;
        -- ### Só funciona a partir da versão 9.2 ###
        v_pos_erro := 130;
        SELECT 'ALTER TABLE '  || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || ' DROP CONSTRAINT ' || conname || ';'
            INTO v_sql
            FROM
                pg_constraint cc
                JOIN pg_class c ON
                    cc.conrelid = c.oid  AND
                    cc.connamespace = c.relnamespace
                JOIN pg_namespace n ON
                    c.relnamespace = n.oid
            WHERE
                connoinherit                AND
                contype      = 'c'          AND
                c.relname    = c_pp.tbl_nom AND
                n.nspname    = c_pp.tbl_sch;
 
        IF FOUND AND v_sql IS NOT NULL THEN
            v_pos_erro := 140;
            EXECUTE v_sql;
        END IF;
 
        -- Remove restrição NO INHERIT da tabela filha;
        v_pos_erro := 150;
        v_sql := 'INSERT INTO '  || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key ||
            ' SELECT * FROM ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || ' WHERE ' ||
            c_pp.par_col || ' = ' || p_next_partition_key || ';';
 
        IF FOUND AND v_sql IS NOT NULL THEN
            v_pos_erro := 160;
            EXECUTE v_sql;
            v_sql := 'DELETE FROM ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || ' WHERE ' || c_pp.par_col ||
                ' = ' || p_next_partition_key || ';';
            v_pos_erro := 170;
            EXECUTE v_sql;
        END IF;
 
        -- Cria CHECK CONSTRAINT para a chave da partição
        v_pos_erro := 180;
        v_sql := 'ALTER TABLE ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || ' ADD CONSTRAINT ' || c_pp.tbl_nom || '_' || p_next_partition_key || '_check_' || p_next_partition_key
            || ' CHECK (' || c_pp.par_col || ' = ' || p_next_partition_key || ');';
            EXECUTE v_sql;
 
        -- Ajusta o owner da partição criada
        v_pos_erro := 190;
        SELECT 'ALTER TABLE ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || ' OWNER TO ' || a.rolname || ';'
            INTO v_sql
            FROM
                pg_class c
                JOIN pg_namespace n ON c.relnamespace = n.oid
                JOIN pg_authid a ON c.relowner = a.oid
            WHERE
                c.relname = c_pp.tbl_nom AND
                n.nspname = c_pp.tbl_sch;
        v_pos_erro := 200;
        EXECUTE v_sql;
 
        -- Ajusta GRANTs para a partição criada
        v_pos_erro := 210;
        FOR c_part IN c_loop_grant(c_pp.tbl_sch, c_pp.tbl_nom) LOOP
            v_pos_erro := 220;
            EXECUTE c_part.sql;
        END LOOP;
 
        -- Cria ou substitui função do gatilho
        v_pos_erro := 230;
 
        -- Inicio da funcao
        v_pos_erro := 240;
        v_sql := 'CREATE OR REPLACE FUNCTION ' || c_pp.tbl_sch || '.fc_tg_' || c_pp.tbl_nom || '()' || chr(10)
            || 'RETURNS TRIGGER AS $$' || chr(10)
            || 'BEGIN' || chr(10) || chr(10);
 
        -- Regra para a nova particao
        v_pos_erro := 250;
        v_sql := v_sql || 'IF ( NEW.' || c_pp.par_col || ' = ' || p_next_partition_key || ') THEN ' || chr(10)
            || '    INSERT INTO ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || '_' || p_next_partition_key
            || ' VALUES (NEW.*);' || chr(10);
 
        -- Regra para as particoes ja existentes
        v_pos_erro := 260;
        IF c_pp.key_act IS NULL THEN
          c_pp.key_act = 100;
        END IF;
 
        v_pos_erro := 270;
        FOR c_part IN  c_loop_partitions (c_pp.par_col, c_pp.tbl_sch, c_pp.tbl_nom, c_pp.key_act) LOOP
            v_pos_erro := 280;
            v_sql := v_sql || c_part.sql;
        END LOOP;
 
        -- Final da funcao
        v_pos_erro := 290;
        v_sql := v_sql || 'ELSE'  || chr(10)
            || '    RAISE EXCEPTION ''Valor de chave inválida para o gatilho de INSERT na tabela %!'',' || c_pp.tbl_nom || ';'  || chr(10)
            || 'END IF;'  || chr(10)  || chr(10)
            || 'RETURN NULL;' || chr(10)
            || 'END;' || chr(10)
            || '$$' || chr(10)
            || 'LANGUAGE plpgsql;';
        v_pos_erro := 300;
        EXECUTE v_sql;
 
        -- Ajusta o owner função
        v_pos_erro := 310;
        SELECT 'ALTER FUNCTION ' || c_pp.tbl_sch || '.fc_tg_' || c_pp.tbl_nom || '()'
            || ' OWNER TO ' || a.rolname || ';'
            INTO v_sql
            FROM
                pg_class c
                JOIN pg_namespace n ON c.relnamespace = n.oid
                JOIN pg_authid a ON c.relowner = a.oid
            WHERE
                c.relname = c_pp.tbl_nom AND
                n.nspname = c_pp.tbl_sch;
        v_pos_erro := 320;
        EXECUTE v_sql;
 
        -- Verifica se o gatilho ja existe
        v_pos_erro := 330;
        SELECT 1
            INTO v_tmp
            FROM information_schema.triggers
            WHERE
                trigger_name = 'tg_' || c_pp.tbl_nom AND
                trigger_schema = c_pp.tbl_sch;
 
        IF NOT FOUND THEN
            -- Cria o gatilho na tabela mãe
            v_pos_erro := 340;
            v_sql := 'CREATE TRIGGER tg_' || c_pp.tbl_nom || chr(10)
                || 'BEFORE INSERT ON ' || c_pp.tbl_sch || '.' || c_pp.tbl_nom || chr(10)
                || '    FOR EACH ROW EXECUTE PROCEDURE ' || c_pp.tbl_sch || '.fc_tg_' || c_pp.tbl_nom  || '();';
            v_pos_erro := 350;
            EXECUTE v_sql;
        END IF;
 
        -- Cria constraints para tabelas não particionadas
        v_pos_erro := 360;
        FOR c_part IN c_loop_constraint(c_pp.tbl_sch, c_pp.tbl_nom) LOOP
            v_pos_erro := 360;
            EXECUTE c_part.alter_c;
        END LOOP;
 
        -- Arquiva partição antiga
        v_pos_erro := 370;
        IF p_archive AND c_pp.key_arc IS NOT NULL THEN
            FOR c_part IN c_loop_arc(c_pp.tbl_sch, c_pp.tbl_nom, c_pp.key_arc) LOOP
                v_pos_erro := 380;
                EXECUTE c_part.sql;
            END LOOP;
        END IF;
 
        -- Apaga partição antiga
        v_pos_erro := 390;
        IF p_drop AND c_pp.key_drp IS NOT NULL THEN
            FOR c_part IN c_loop_drop(c_pp.tbl_sch, c_pp.tbl_nom, c_pp.key_drp) LOOP
                v_pos_erro := 400;
                EXECUTE c_part.sql;
            END LOOP;
        END IF;
    END LOOP;
 
    v_pos_erro := 410;
    FOR c_part IN c_loop_const_part LOOP
        v_pos_erro := 420;
        EXECUTE c_part.alter_c;
    END LOOP;
 
    RAISE WARNING 'cria_particao_tabela|Fim';
 
EXCEPTION
   WHEN OTHERS            THEN
      v_cod_erro  := SUBSTR(SQLERRM,1,2000);
      RAISE WARNING 'cria_particao_tabela|ERRO!!! POS: %, COD: %, MSG: %', v_pos_erro, SQLSTATE, v_cod_erro;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100000;
