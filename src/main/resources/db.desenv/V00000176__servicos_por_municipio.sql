CREATE SCHEMA servico;

CREATE TABLE servico.servicos(
	servico_id				SERIAL NOT NULL PRIMARY KEY,
	servico_composto_id			BIGINT REFERENCES servico.servicos(servico_id),
	nomemclatura				VARCHAR(100) NOT NULL UNIQUE,
	descricao				TEXT
);

CREATE TABLE servico.servicos_por_municipios(
	servicopormunicipio_id			SERIAL NOT NULL PRIMARY KEY,
	servico_id				BIGINT NOT NULL REFERENCES servico.servicos(servico_id),
	cidade_id				BIGINT NOT NULL REFERENCES public.cidades(cidade_id),
	issqn					NUMERIC(16,4),
	irrf					NUMERIC(16,4),
	ccp					NUMERIC(16,4),
	inss					NUMERIC(16,4)
);
