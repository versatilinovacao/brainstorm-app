package br.com.insideincloud.brainstormapp.api.service.transport;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.OrdemServico;

public class LoteEnviarRps {
	private List<OrdemServico> ordens;

	public List<OrdemServico> getOrdens() {
		return ordens;
	}

	public void setOrdens(List<OrdemServico> ordens) {
		this.ordens = ordens;
	}

}
