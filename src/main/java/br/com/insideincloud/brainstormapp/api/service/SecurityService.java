package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.ConfirmarCadastro;
import br.com.insideincloud.brainstormapp.api.model.Usuario;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.ConfirmarCadastros;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;

@Service
public class SecurityService {

	@Autowired
	private Usuarios usuarios;
	
//	private UsuarioEmail email;
	
	@Autowired
	private ConfirmarCadastros confirmar;
	
//	@Autowired
//	private JavaMailSender mailSender;
	
	@Autowired
	private Colaboradores colaboradores;

	@Transactional
	public Usuario cadastrar(Usuario usuario) {
//		email = new UsuarioEmail();
		try {
			usuario = usuarios.saveAndFlush(usuario);
			
			Colaborador colaborador = new Colaborador();
			colaborador.setNome(usuario.getNomecompleto());
			colaboradores.saveAndFlush(colaborador);
			
//			email.setTo(usuario.getEmail());
//			email.setAssunto("Valide seu cadastro");
//			email.salvar(confirmar,usuario);
//			email.sendMail(mailSender);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return usuario;
	}
	
	@Transactional
	public void confirmarusuario(Long codigo) {
//		email = new UsuarioEmail();
		ConfirmarCadastro cadastro = confirmar.findOne(codigo);		
		Usuario usuario = usuarios.findOne(cadastro.getUsuario().getUsuario_id());
		usuario.setStatus(true);
		usuarios.save(usuario);
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		email.setTo(usuario.getEmail());
//		email.setAssunto("Cadastro Confirmado");
//		email.setConteudo("www.insideincloud.com.br:8090");
//		email.salvar(confirmar,usuario);
//		email.sendMail(mailSender);
	}
}
