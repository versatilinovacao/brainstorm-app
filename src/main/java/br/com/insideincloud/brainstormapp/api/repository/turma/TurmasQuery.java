package br.com.insideincloud.brainstormapp.api.repository.turma;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.repository.filter.TurmaFilter;

public interface TurmasQuery {
	public Page<Turma> filtrar(TurmaFilter filtro, Pageable page);
}
