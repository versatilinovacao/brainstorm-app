package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.minor.ClienteMinor;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Repository
public interface ClientesMinor extends JpaRepository<ClienteMinor, ColaboradorPK> {

}
