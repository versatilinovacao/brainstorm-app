package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="chamadas_view",schema="escola")
public class ChamadaView implements Serializable {
	private static final long serialVersionUID = 6627731965670919572L;

	@Id
	private Long chamada_id;
	private Long caderno_id;
	private String nome_caderno;
	private LocalDate evento;
	private Long aluno_id;
	@Column(name="composicao_aluno_id")
	private Long composicaoaluno_id;
	private String nome_aluno;
	private Long periodo_letivo_item_id;	
	private String conceito;
	private BigDecimal nota;
	private String avaliacao;
	private boolean status;
	private String acompanhamento;
	
	public Long getComposicaoaluno_id() {
		return composicaoaluno_id;
	}
	public void setComposicaoaluno_id(Long composicaoaluno_id) {
		this.composicaoaluno_id = composicaoaluno_id;
	}
	public String getAcompanhamento() {
		return acompanhamento;
	}
	public void setAcompanhamento(String acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	public String getNome_caderno() {
		return nome_caderno;
	}
	public void setNome_caderno(String nome_caderno) {
		this.nome_caderno = nome_caderno;
	}
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public LocalDate getEvento() {
		return evento;
	}
	public void setEvento(LocalDate evento) {
		this.evento = evento;
	}
	public Long getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(Long chamada_id) {
		this.chamada_id = chamada_id;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
