package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.servicos.ServicoCachoeirinha;
import br.com.insideincloud.brainstormapp.api.model.view.ServicoCachoeirinhaAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.ServicoCachoeirinhaInativoView;
import br.com.insideincloud.brainstormapp.api.repository.ServicosCachoeirinha;
import br.com.insideincloud.brainstormapp.api.repository.ServicosCachoeirinhaAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.ServicosCachoeirinhaInativosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/servicoscachoeirinha")
public class ServicoCachoeirinhaResource {
	
	@Autowired
	private ServicosCachoeirinha servicos;
	
	@Autowired
	private ServicosCachoeirinhaAtivosView servicosAtivos;
	
	@Autowired
	private ServicosCachoeirinhaInativosView servicosInativos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoCachoeirinhaAtivoView> conteudoAtivo() {
		RetornoWrapper<ServicoCachoeirinhaAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( servicosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações da lista de Serviços de Cachoeirinha, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoCachoeirinhaInativoView> conteudoInativo() {
		RetornoWrapper<ServicoCachoeirinhaInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( servicosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações da lista de Serviços inativos de Cachoeirinha, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoCachoeirinha> conteudoPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<ServicoCachoeirinha> retorno = new RetornoWrapper<>();
		
		retorno.setSingle( servicos.findOne(codigo) );
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_SERVICO_SALVAR')")
	public RetornoWrapper<ServicoCachoeirinha> salvar(@RequestBody ServicoCachoeirinha servico) {
		RetornoWrapper<ServicoCachoeirinha> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(servicos.saveAndFlush(servico));
		} catch(Exception e) {
			exception.setCodigo(2);
			exception.setMensagem("Não foi possível salvar o Serviço de Cachoeirinha na Lista, favor verificar e tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

}
