package br.com.insideincloud.brainstormapp.api.repository.filter;

public class GradeCurricularFilter {
	private String gradecurricular_id;
	private String turma_id;
	private String materia_id;
	private String colaborador_id;
	private String curso_id;	
	public String getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(String gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(String materia_id) {
		this.materia_id = materia_id;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(String curso_id) {
		this.curso_id = curso_id;
	}
	
	
}
