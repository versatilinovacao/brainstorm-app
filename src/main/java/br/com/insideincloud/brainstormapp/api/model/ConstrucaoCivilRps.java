package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CONSTRUCAOCIVILRPS")
public class ConstrucaoCivilRps extends Rps implements Serializable {
	private static final long serialVersionUID = 1L;

	private String codigoobra;
	private String art;
	public String getCodigoobra() {
		return codigoobra;
	}
	public void setCodigoobra(String codigoobra) {
		this.codigoobra = codigoobra;
	}
	public String getArt() {
		return art;
	}
	public void setArt(String art) {
		this.art = art;
	}
	
	
}
