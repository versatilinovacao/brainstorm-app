package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TipoAjusteCarne;

@Repository
public interface TiposAjustesCarnes extends JpaRepository<TipoAjusteCarne, Long> {

}
