package br.com.insideincloud.brainstormapp.api.repository.periodoletivoitem;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoItemFilter;

public class PeriodosLetivosItensImpl implements PeriodosLetivosItensQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<PeriodoLetivoItem> filtrar(PeriodoLetivoItemFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<PeriodoLetivoItem> criteria = builder.createQuery(PeriodoLetivoItem.class);
		Root<PeriodoLetivoItem> root = criteria.from(PeriodoLetivoItem.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<PeriodoLetivoItem> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(PeriodoLetivoItemFilter filtro, CriteriaBuilder builder, Root<PeriodoLetivoItem> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getPeriodoletivo_id())) {
				predicates.add(builder.equal(root.get("periodoletivo").get("periodo_letivo_id"), Long.parseLong(filtro.getPeriodoletivo_id())));
			}		

		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(PeriodoLetivoItemFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<PeriodoLetivoItem> root = criteria.from(PeriodoLetivoItem.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
