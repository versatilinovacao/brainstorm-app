package br.com.insideincloud.brainstormapp.api.repository.chamadareportview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaReportView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaReportViewFilter;

public interface ChamadasReportViewQuery {
	public Page<ChamadaReportView> filtrar(ChamadaReportViewFilter filtro,Pageable page);

}
