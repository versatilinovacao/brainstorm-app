package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Matricula;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaFilter;
import br.com.insideincloud.brainstormapp.api.repository.matricula.MatriculasQuery;

@Repository
public interface Matriculas extends JpaRepository<Matricula,Long>, MatriculasQuery {
	public Page<Matricula> filtrar(MatriculaFilter filtro, Pageable page);

}
