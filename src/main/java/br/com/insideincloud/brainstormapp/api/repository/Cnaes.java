package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Cnae;

@Repository
public interface Cnaes extends JpaRepository<Cnae,Long> {
//	@Query("FROM Cnae c WHERE c.cnae_id")
//	Page<Cnae> searchCODIGO(@Param("searchCodigo") String searchCodigo);
//	
//	@Query("FROM Cnae c WHERE c.secao = searchSecao")
//	Page<Cnae> searchSECAO(@Param("searchSecao") String searchSecao);
//	
//	@Query("FROM Cnae c WHERE c.divisao = searchDivisao")
//	Page<Cnae> searchDIVISAO(@Param("searchDivisao") String searchDivisao);
//	
//	@Query("FROM Cnae c WHERE c.grupo = searchGrupo")
//	Page<Cnae> searchGRUPO(@Param("searchGrupo") String searcGrupo);
//	
//	@Query("FROM Cnae c WHERE c.classe = searchClasse")
//	Page<Cnae> searchCLASSE(@Param("searchClasse") String searchClasse);
//	
//	@Query("FROM Cnae c WHERE LOWER(c.denominacao) LIKE %searchDenominacao%")
//	Page<Cnae> searchDENOMINACAO(@Param("searchDenominacao") String searchDenominacao);
}
