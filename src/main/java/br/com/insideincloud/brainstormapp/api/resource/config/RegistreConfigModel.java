package br.com.insideincloud.brainstormapp.api.resource.config;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class RegistreConfigModel implements Serializable {
	private static final long serialVersionUID = -3393342407773840891L;

	private String ip;
	private Long usuario;
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Long getUsuario() {
		return usuario;
	}
	public void setUsuario(Long usuario) {
		this.usuario = usuario;
	}
	
	
}
