package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tiposmedicos_view",schema="remoto")
public class TipoMedico implements Serializable {
	private static final long serialVersionUID = 1510273246793437095L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipomedico_id;
	private String nome;
	
	public Long getTipomedico_id() {
		return tipomedico_id;
	}
	public void setTipomedico_id(Long tipomedico_id) {
		this.tipomedico_id = tipomedico_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipomedico_id == null) ? 0 : tipomedico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoMedico other = (TipoMedico) obj;
		if (tipomedico_id == null) {
			if (other.tipomedico_id != null)
				return false;
		} else if (!tipomedico_id.equals(other.tipomedico_id))
			return false;
		return true;
	}
	
}
