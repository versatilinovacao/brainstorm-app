package br.com.insideincloud.brainstormapp.api.repository.grademateria;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.GradeMateria;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeMateriaFilter;

public class GradesMateriasImpl implements GradesMateriasQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<GradeMateria> filtrar(GradeMateriaFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<GradeMateria> criteria = builder.createQuery(GradeMateria.class);
		Root<GradeMateria> root = criteria.from(GradeMateria.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<GradeMateria> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(GradeMateriaFilter filtro, CriteriaBuilder builder, Root<GradeMateria> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getGrademateria_id())) {
				predicates.add(builder.equal(root.get("grademateria_id"), Long.parseLong(filtro.getGrademateria_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), Long.parseLong(filtro.getColaborador_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getDiasemana_id())) {
				predicates.add(builder.equal(root.get("diasemana_id"), Long.parseLong(filtro.getDiasemana_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getMateria_id())) {
				predicates.add(builder.equal(root.get("materia_id"), Long.parseLong(filtro.getMateria_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getSala_id())) {
				predicates.add(builder.equal(root.get("sala_id"), Long.parseLong(filtro.getSala_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getTurno_id())) {
				predicates.add(builder.equal(root.get("turno_id"), Long.parseLong(filtro.getTurno_id())));
			}		
			
//			if (!StringUtils.isEmpty(filtro.getDescricao())) {
//				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
//			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(GradeMateriaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<GradeMateria> root = criteria.from(GradeMateria.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
