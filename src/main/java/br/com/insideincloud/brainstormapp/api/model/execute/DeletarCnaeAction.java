package br.com.insideincloud.brainstormapp.api.model.execute;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_cnae", schema="action")
public class DeletarCnaeAction implements Serializable {
	private static final long serialVersionUID = -3175863774761947152L;

	@Id
	private Long cnae_id;

	public Long getCnae_id() {
		return cnae_id;
	}

	public void setCnae_id(Long cnae_id) {
		this.cnae_id = cnae_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnae_id == null) ? 0 : cnae_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeletarCnaeAction other = (DeletarCnaeAction) obj;
		if (cnae_id == null) {
			if (other.cnae_id != null)
				return false;
		} else if (!cnae_id.equals(other.cnae_id))
			return false;
		return true;
	}
}
