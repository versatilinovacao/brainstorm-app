//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.7 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2019.09.22 às 04:32:54 PM BRT 
//


package br.com.insideincloud.brainstormapp.api.soap.nfse.abrasf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3._2000._09.xmldsig_.SignatureType;


/**
 * <p>Classe Java de tcCancelamentoNfse complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tcCancelamentoNfse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Confirmacao" type="{http://www.abrasf.org.br/ABRASF/arquivos/nfse.xsd}tcConfirmacaoCancelamento"/>
 *         &lt;element ref="{http://www.w3.org/2000/09/xmldsig#}Signature"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tcCancelamentoNfse", propOrder = {
    "confirmacao",
    "signature"
})
public class TcCancelamentoNfse {

    @XmlElement(name = "Confirmacao", required = true)
    protected TcConfirmacaoCancelamento confirmacao;
    @XmlElement(name = "Signature", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
    protected SignatureType signature;

    /**
     * Obtém o valor da propriedade confirmacao.
     * 
     * @return
     *     possible object is
     *     {@link TcConfirmacaoCancelamento }
     *     
     */
    public TcConfirmacaoCancelamento getConfirmacao() {
        return confirmacao;
    }

    /**
     * Define o valor da propriedade confirmacao.
     * 
     * @param value
     *     allowed object is
     *     {@link TcConfirmacaoCancelamento }
     *     
     */
    public void setConfirmacao(TcConfirmacaoCancelamento value) {
        this.confirmacao = value;
    }

    /**
     * Obtém o valor da propriedade signature.
     * 
     * @return
     *     possible object is
     *     {@link SignatureType }
     *     
     */
    public SignatureType getSignature() {
        return signature;
    }

    /**
     * Define o valor da propriedade signature.
     * 
     * @param value
     *     allowed object is
     *     {@link SignatureType }
     *     
     */
    public void setSignature(SignatureType value) {
        this.signature = value;
    }

}
