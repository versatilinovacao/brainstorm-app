package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaReceberPK;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberAtivaView;

@Repository
public interface ContasReceberAtivasView extends JpaRepository<ContaReceberAtivaView, ContaReceberPK> {

}
