package br.com.insideincloud.brainstormapp.api.resource.fachada;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.insideincloud.brainstormapp.api.model.ComunicacaoAlunoProfessor;
import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;

public class MensagemAlunoProfessorFachada implements Serializable {
	private static final long serialVersionUID = -8925127433863407732L;
	
	private Long mensagemaluno_id;
	private ColaboradorMinor aluno;
	private ColaboradorMinor professor;
	private String mensagem;
	private LocalDateTime registro;
	private ComunicacaoAlunoProfessor emissor;
	
	public Long getMensagemaluno_id() {
		return mensagemaluno_id;
	}
	public void setMensagemaluno_id(Long mensagemaluno_id) {
		this.mensagemaluno_id = mensagemaluno_id;
	}
	public ColaboradorMinor getAluno() {
		return aluno;
	}
	public void setAluno(ColaboradorMinor aluno) {
		this.aluno = aluno;
	}
	public ColaboradorMinor getProfessor() {
		return professor;
	}
	public void setProfessor(ColaboradorMinor professor) {
		this.professor = professor;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public LocalDateTime getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDateTime registro) {
		this.registro = registro;
	}
	public ComunicacaoAlunoProfessor getEmissor() {
		return emissor;
	}
	public void setEmissor(ComunicacaoAlunoProfessor emissor) {
		this.emissor = emissor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mensagemaluno_id == null) ? 0 : mensagemaluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MensagemAlunoProfessorFachada other = (MensagemAlunoProfessorFachada) obj;
		if (mensagemaluno_id == null) {
			if (other.mensagemaluno_id != null)
				return false;
		} else if (!mensagemaluno_id.equals(other.mensagemaluno_id))
			return false;
		return true;
	}
	
	
	
	
}
