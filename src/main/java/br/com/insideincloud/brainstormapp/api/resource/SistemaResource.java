package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.Sistema;
import br.com.insideincloud.brainstormapp.api.repository.Sistemas;

@RestController
@RequestMapping("/sistemas")
public class SistemaResource {

	@Autowired
	private Sistemas sistemas;
	
	@GetMapping("{/codigo}")
	public ResponseEntity<Sistema> conteudo(@PathVariable Long codigo, HttpServletResponse response) {
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(codigo).toUri();
		response.setHeader("",uri.toASCIIString());
		return ResponseEntity.created(uri).body(sistemas.findOne(codigo));
	}
}