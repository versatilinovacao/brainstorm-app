package br.com.insideincloud.brainstormapp.api.service.nfse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Service
public class XmlNotaEletronicaService {
	protected String caminhoDoCertificado;
	protected String senhaDoCertificado;
	protected String arquivoXml;
	
	protected String pathOriginal;
	protected String pathAssinado;
	protected String arquivoName;
	
	@Autowired
	private RpsMonitor monitor;
	
	protected void assinarNFe(XMLSignatureFactory fac, ArrayList<Transformer> transformList, PrivateKey privateKey, KeyInfo ki, Document document, int indexNFe) throws Exception {
		NodeList elements = document.getElementsByTagName("infNFe");
		org.w3c.dom.Element el = (org.w3c.dom.Element) elements.item(indexNFe);
		el.setIdAttribute("Id",true);
		String id = el.getAttribute("Id");
		Reference ref = fac.newReference("#" + id, fac.newDigestMethod(DigestMethod.SHA1, null), transformList, null, null );
		SignedInfo si = fac.newSignedInfo(
					fac.newCanonicalizationMethod(
					CanonicalizationMethod.INCLUSIVE,
					(C14NMethodParameterSpec) null),
					fac.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
					Collections.singletonList(ref)
				);
		XMLSignature signature = fac.newXMLSignature(si, ki);
		DOMSignContext dsc = new DOMSignContext(privateKey,document.getDocumentElement().getElementsByTagName("NFE").item(indexNFe));
		signature.sign(dsc);
	}
	
	//Converte para o formato de documento XML.
	protected Document documentFactory(String xml) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document document = factory.newDocumentBuilder().parse( new ByteArrayInputStream(xml.getBytes()) );
		
		return document;
	}
	
	protected String outputXML(Document doc) throws TransformerException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		String xml = os.toString();
		if ((xml != null) && (!"".equals(xml))) {
			xml = xml.replace("\\r\\n","");
			xml = xml.replace(" standalone=\"no\"","");
		}
		
		return xml;
	}
	
	protected String lerXML() throws IOException { 
		String linha;
		StringBuilder xml = new StringBuilder();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(this.pathOriginal+"/"+this.arquivoName)));
		while ((linha = in.readLine()) != null) {
			xml.append(linha);
		}
		in.close();
		
		return xml.toString();
	}
	
	protected void salvarXml() throws IOException {
		Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.pathOriginal+"/"+this.arquivoName),"ISO-8859-1"));
		file.write(this.arquivoXml);
		file.close();
		
		monitor.log(2L);
	}

}
