package br.com.insideincloud.brainstormapp.api.repository.filter;

public class LogradouroFilter {
	private String cep;
	private String descricao;
	private String bairro_id;
	private String pais_id;
	private String estado_id;
	private String cidade_id;
	
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getBairro_id() {
		return bairro_id;
	}
	public void setBairro_id(String bairro_id) {
		this.bairro_id = bairro_id;
	}
	public String getPais_id() {
		return pais_id;
	}
	public void setPais_id(String pais_id) {
		this.pais_id = pais_id;
	}
	public String getEstado_id() {
		return estado_id;
	}
	public void setEstado_id(String estado_id) {
		this.estado_id = estado_id;
	}
	public String getCidade_id() {
		return cidade_id;
	}
	public void setCidade_id(String cidade_id) {
		this.cidade_id = cidade_id;
	}
	
}
