package br.com.insideincloud.brainstormapp.api.repository.matriculaalunoview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.deser.impl.ExternalTypeHandler.Builder;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoVinculadoTurmaView;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoViewFilter;

public class AlunosMatriculadosViewImpl implements AlunosMatriculadosViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<MatriculaAlunoView> filtrar(MatriculaAlunoViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<MatriculaAlunoView> criteria = builder.createQuery(MatriculaAlunoView.class);
		Root<MatriculaAlunoView> root = criteria.from(MatriculaAlunoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<MatriculaAlunoView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(MatriculaAlunoViewFilter filtro, CriteriaBuilder builder, Root<MatriculaAlunoView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
	
		
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getTurma_id())) {
				System.out.println("ESTE É UM TESTE INTERESSANTE...");
				predicates.add(builder.equal(root.get("turma_id"), Long.parseLong(filtro.getTurma_id())));
				System.out.println( getAlunosVinculados(Long.parseLong(filtro.getTurma_id())) + "{[...]}" );
				predicates.add( builder.not( getAlunosVinculados(Long.parseLong(filtro.getTurma_id())) ) );
				
			}		

			if (!StringUtils.isEmpty(filtro.getAluno_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), Long.parseLong(filtro.getAluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getMatricula())) {
				predicates.add(builder.equal(root.get("matricula"), Long.parseLong(filtro.getMatricula())));
			}		

			if (!StringUtils.isEmpty(filtro.getAlunonome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getAlunonome().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getCurso_id())) {
				predicates.add(builder.equal(root.get("curso_id"), Long.parseLong(filtro.getCurso_id())));
			}
			
			
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(MatriculaAlunoViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<MatriculaAlunoView> root = criteria.from(MatriculaAlunoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	private Predicate getAlunosVinculados(Long turma_id) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<AlunoVinculadoTurmaView> criteria = builder.createQuery(AlunoVinculadoTurmaView.class);
		Root<AlunoVinculadoTurmaView> root = criteria.from(AlunoVinculadoTurmaView.class);
		
		Predicate[] predicates = criarRestricoesVinculado(turma_id,builder,root);
//		Predicate predicates = builder.equal(root.get("turma_id"), turma_id);
		criteria.where(predicates);
		
		TypedQuery<AlunoVinculadoTurmaView> query = manager.createQuery(criteria);
		
		System.out.println(">>>>>>>>><<<<<<<<<"+query.getResultList().size());
		System.out.println("!>>>>>>>>><<<<<<<<<!"+turma_id);
		List<String> alunos = new ArrayList<String>();
		for (AlunoVinculadoTurmaView aluno : query.getResultList()) {
			System.out.println(aluno.getAlunovinculadoturma_id());
			System.out.println(aluno.getNome()+" [ --- ]");
			System.out.println(aluno.getColaborador_id());
			alunos.add( String.valueOf(aluno.getColaborador_id()) );
		}
		
//		Expression<String> parentExp = root.get("colaborador_id");
//		Predicate parentPred = parentExp.
		
		
		Path<AlunoVinculadoTurmaView> path = root.get("colaborador_id");
		Predicate resultado = path.in(alunos);
		
		return resultado;
		
	}
		
	private Predicate[] criarRestricoesVinculado(Long turma_id,CriteriaBuilder builder, Root<AlunoVinculadoTurmaView> root) {
		List<Predicate> predicates = new ArrayList<>(); 
		
		if (turma_id > 0) {
			predicates.add(builder.equal(root.get("colaborador_id"), turma_id));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
/*predicates.toArray(new Predicate[predicates.size()])
 * predicates.add(builder.equal(root.get("turma_id"), Long.parseLong(filtro.getTurma_id())));
 * Exemplo:
 *     final EntityManager em = dbService.getEntityManager();

    // INSERT new record
    em.getTransaction().begin();
    em.persist(new Account("A"));
    em.persist(new Account("B"));
    em.persist(new Account("C"));
    em.getTransaction().commit();



    CriteriaBuilder builder = em.getCriteriaBuilder();
    CriteriaQuery<Account> query = builder.createQuery(Account.class);
    Root<Account> acc = query.from(Account.class);

    Predicate cond = builder.gt(acc.get("id"), 1);
    query.where(cond);
    TypedQuery<Account> q = em.createQuery(query);
    List<Account> resultList = q.getResultList();
    System.out.println(resultList);

 * 
 * */
