package br.com.insideincloud.brainstormapp.api.repository.telefonecolaboradorview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.TelefoneColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneColaboradorViewFilter;

public class TelefonesColaboradoresViewImpl implements TelefonesColaboradoresViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<TelefoneColaboradorView> filtrar(TelefoneColaboradorViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<TelefoneColaboradorView> criteria = builder.createQuery(TelefoneColaboradorView.class);
		Root<TelefoneColaboradorView> root = criteria.from(TelefoneColaboradorView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<TelefoneColaboradorView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(TelefoneColaboradorViewFilter filtro, CriteriaBuilder builder, Root<TelefoneColaboradorView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getTelefone_id())) {
				predicates.add(builder.equal(root.get("telefone_id"), Long.parseLong(filtro.getTelefone_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getNumero())) {
				predicates.add(builder.like(builder.lower(root.get("numero")), "%" + filtro.getNumero() + "%"));
			}
			if (!StringUtils.isEmpty(filtro.getRamal())) {
				predicates.add(builder.like(builder.lower(root.get("ramal")), "%" + filtro.getRamal() + "%"));
			}
			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), Long.parseLong(filtro.getColaborador_id())));
			} else {
				predicates.add(builder.equal(root.get("colaborador_id"), 0));
			};		
			if (!StringUtils.isEmpty(filtro.getComposicao_colaborador_id())) {
				predicates.add(builder.equal(root.get("composicaocolaborador_id"), Long.parseLong(filtro.getComposicao_colaborador_id())));
			} else {
				predicates.add(builder.equal(root.get("composicaocolaborador_id"), 0));
			};		
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(TelefoneColaboradorViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<TelefoneColaboradorView> root = criteria.from(TelefoneColaboradorView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}


}
