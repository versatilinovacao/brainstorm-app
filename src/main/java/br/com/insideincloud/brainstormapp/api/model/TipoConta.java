package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_pagamentos",schema="financeiro")
public class TipoConta implements Serializable {
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private Long tipopagamento_id;
	private String descricao;
	
	public Long getTipopagamento_id() {
		return tipopagamento_id;
	}
	public void setTipopagamento_id(Long tipopagamento_id) {
		this.tipopagamento_id = tipopagamento_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipopagamento_id == null) ? 0 : tipopagamento_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoConta other = (TipoConta) obj;
		if (tipopagamento_id == null) {
			if (other.tipopagamento_id != null)
				return false;
		} else if (!tipopagamento_id.equals(other.tipopagamento_id))
			return false;
		return true;
	}

}
