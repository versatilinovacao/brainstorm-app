package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="seguimentos",schema="recurso")
public class Seguimento implements Serializable {
	private static final long serialVersionUID = 1126762293101165567L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long seguimento_id;
	@Column(name="codigo",length=1)
	private String codigo;
	@Column(name="nome",length=100)
	
	private String nome;
	public Long getSeguimento_id() {
		return seguimento_id;
	}
	public void setSeguimento_id(Long seguimento_id) {
		this.seguimento_id = seguimento_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((seguimento_id == null) ? 0 : seguimento_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seguimento other = (Seguimento) obj;
		if (seguimento_id == null) {
			if (other.seguimento_id != null)
				return false;
		} else if (!seguimento_id.equals(other.seguimento_id))
			return false;
		return true;
	}
	
	
}
