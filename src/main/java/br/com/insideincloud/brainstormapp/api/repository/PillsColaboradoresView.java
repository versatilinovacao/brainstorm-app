package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PillColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PillColaboradorViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.pillcolaboradorview.PillsColaboradoresViewQuery;

@Repository
public interface PillsColaboradoresView extends JpaRepository<PillColaboradorView,Long>, PillsColaboradoresViewQuery {
	public Page<PillColaboradorView> filtrar(PillColaboradorViewFilter filtro, Pageable page);

}
