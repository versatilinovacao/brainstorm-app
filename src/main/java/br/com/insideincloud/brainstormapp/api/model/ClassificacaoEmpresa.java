package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="classificacao_empresa",schema="empresa")
public class ClassificacaoEmpresa implements Serializable {
	private static final long serialVersionUID = -2916045559934350939L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classificacao_empresa_id;
	private String descricao;
	
	public Long getClassificacao_empresa_id() {
		return classificacao_empresa_id;
	}
	public void setClassificacao_empresa_id(Long classificacao_empresa_id) {
		this.classificacao_empresa_id = classificacao_empresa_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classificacao_empresa_id == null) ? 0 : classificacao_empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassificacaoEmpresa other = (ClassificacaoEmpresa) obj;
		if (classificacao_empresa_id == null) {
			if (other.classificacao_empresa_id != null)
				return false;
		} else if (!classificacao_empresa_id.equals(other.classificacao_empresa_id))
			return false;
		return true;
	}

}
