package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TaxaSelicImport;

@Repository
public interface TaxasSelicImport extends JpaRepository<TaxaSelicImport,Long> {

}
