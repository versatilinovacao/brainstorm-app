package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

import java.util.List;

public class Servico {
	private List<Valores> Valores;
	private String ItemListaServico;
	private String CodigoCnae;
	private String Discriminacao;
	private String CodigoMunicipio;

	public List<Valores> getValores() {
		return Valores;
	}
	public void setValores(List<Valores> valores) {
		Valores = valores;
	}
	public String getItemListaServico() {
		return ItemListaServico;
	}
	public void setItemListaServico(String itemListaServico) {
		ItemListaServico = itemListaServico;
	}
	public String getCodigoCnae() {
		return CodigoCnae;
	}
	public void setCodigoCnae(String codigoCnae) {
		CodigoCnae = codigoCnae;
	}
	public String getDiscriminacao() {
		return Discriminacao;
	}
	public void setDiscriminacao(String discriminacao) {
		Discriminacao = discriminacao;
	}
	public String getCodigoMunicipio() {
		return CodigoMunicipio;
	}
	public void setCodigoMunicipio(String codigoMunicipio) {
		CodigoMunicipio = codigoMunicipio;
	}	

}
