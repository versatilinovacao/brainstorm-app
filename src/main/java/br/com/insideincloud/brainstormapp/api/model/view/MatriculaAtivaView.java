package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="matriculas_ativas_cursos_view",schema="escola")
public class MatriculaAtivaView implements Serializable {
	private static final long serialVersionUID = -6963221566496051617L;
	
	@Id
	private Long matricula_id;
	private String codigo;
	private Long colaborador_id;
	private Long composicao_colaborador_id;
	private Long situacao_matricula_id;
	private Long curso_id;
	@Column(name="competencia_inicial")
	private LocalDate competenciainicial;
	@Column(name="competencia_final")
	private LocalDate competenciafinal;
	private Boolean status;
	private String aluno;
	private String situacao;
	private String curso;
	
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getAluno() {
		return aluno;
	}
	public void setAluno(String aluno) {
		this.aluno = aluno;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public Long getMatricula_id() {
		return matricula_id;
	}
	public void setMatricula_id(Long matricula_id) {
		this.matricula_id = matricula_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public Long getSituacao_matricula_id() {
		return situacao_matricula_id;
	}
	public void setSituacao_matricula_id(Long situacao_matricula_id) {
		this.situacao_matricula_id = situacao_matricula_id;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public LocalDate getCompetenciainicial() {
		return competenciainicial;
	}
	public void setCompetenciainicial(LocalDate competenciainicial) {
		this.competenciainicial = competenciainicial;
	}
	public LocalDate getCompetenciafinal() {
		return competenciafinal;
	}
	public void setCompetenciafinal(LocalDate competenciafinal) {
		this.competenciafinal = competenciafinal;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatriculaAtivaView other = (MatriculaAtivaView) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}

}
