package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.AvaliacaoAlunoAtividade;

@Repository
public interface AvaliacoesAlunosAtividades extends JpaRepository<AvaliacaoAlunoAtividade, Long> {
	
	@Query("select a from AvaliacaoAlunoAtividade a where a.atividade.atividade_id = ?1 ")
	public List<AvaliacaoAlunoAtividade> findByAlunos(Long atividade_id);
	
	@Query("select a from AvaliacaoAlunoAtividade a where a.atividade.atividade_id = ?1 and a.aluno.colaborador_id.colaborador_id = ?2 and a.aluno.colaborador_id.composicao_id = ?3")
	public AvaliacaoAlunoAtividade findByAvaliacaoAluno(Long atividade_id, Long aluno_id, Long composicao_aluno_id);

}
