package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="artefatos_inativos_view",schema="almoxarifado")
public class ArtefatoInativoView implements Serializable {
	private static final long serialVersionUID = 2665271509309209146L;
	
	@Id
	private Long artefato_id;
	private Long composicao_id;
	
	private String nome;
	
	private String descricao;
	private double preco;
	private String classificacao;
	private String tipo;
	private String artigo;
	private Long colaborador_id;
	private Long composicao_colaborador_id;
	private Boolean status;
	
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public Long getArtefato_id() {
		return artefato_id;
	}
	public void setArtefato_id(Long artefato_id) {
		this.artefato_id = artefato_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getArtigo() {
		return artigo;
	}
	public void setArtigo(String artigo) {
		this.artigo = artigo;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artefato_id == null) ? 0 : artefato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtefatoInativoView other = (ArtefatoInativoView) obj;
		if (artefato_id == null) {
			if (other.artefato_id != null)
				return false;
		} else if (!artefato_id.equals(other.artefato_id))
			return false;
		return true;
	}
}
