package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaPagarPK;

@Entity
@Table(name="contas_pagar_compensacao_view",schema="financeiro")
public class ContaPagarPendenteView implements Serializable {
	private static final long serialVersionUID = 5431538446562095156L;

	@Id
	private ContaPagarPK contapagar_id;
//	private Long contareceber_id;
//  private Long composicao_id;
    
    private Long empresa_id;
    @Column(name="composicao_empresa_id")
    private Long composicaoempresa_id;
    private String nomeempresa;
    private LocalDate vencimento;
    private LocalDate emissao;
    private LocalDate pagamento;
    private LocalDate quitacao;
    private Long credor_id;
    @Column(name="composicao_credor_id")
    private Long composicaocredor_id;
    private String nomedevedor;
    private Double desconto;
    private Double valorpago;
    private Double saldo;
    private Double total;
    private Long negociacao_id;
    private String descricaonegociacao;
    private Long formapagamento_id;
    private String descricaoformapagamento;
    private Long tipopagamento_id;
    private String descricaotipopagamento;
    private Boolean status;
    
	public ContaPagarPK getContapagar_id() {
		return contapagar_id;
	}
	public void setContapagar_id(ContaPagarPK contapagar_id) {
		this.contapagar_id = contapagar_id;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Long getComposicaoempresa_id() {
		return composicaoempresa_id;
	}
	public void setComposicaoempresa_id(Long composicaoempresa_id) {
		this.composicaoempresa_id = composicaoempresa_id;
	}
	public String getNomeempresa() {
		return nomeempresa;
	}
	public void setNomeempresa(String nomeempresa) {
		this.nomeempresa = nomeempresa;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public Long getCredor_id() {
		return credor_id;
	}
	public void setCredor_id(Long credor_id) {
		this.credor_id = credor_id;
	}
	public Long getComposicaocredor_id() {
		return composicaocredor_id;
	}
	public void setComposicaocredor_id(Long composicaocredor_id) {
		this.composicaocredor_id = composicaocredor_id;
	}
	public String getNomedevedor() {
		return nomedevedor;
	}
	public void setNomedevedor(String nomedevedor) {
		this.nomedevedor = nomedevedor;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Double getValorpago() {
		return valorpago;
	}
	public void setValorpago(Double valorpago) {
		this.valorpago = valorpago;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Long getNegociacao_id() {
		return negociacao_id;
	}
	public void setNegociacao_id(Long negociacao_id) {
		this.negociacao_id = negociacao_id;
	}
	public String getDescricaonegociacao() {
		return descricaonegociacao;
	}
	public void setDescricaonegociacao(String descricaonegociacao) {
		this.descricaonegociacao = descricaonegociacao;
	}
	public Long getFormapagamento_id() {
		return formapagamento_id;
	}
	public void setFormapagamento_id(Long formapagamento_id) {
		this.formapagamento_id = formapagamento_id;
	}
	public String getDescricaoformapagamento() {
		return descricaoformapagamento;
	}
	public void setDescricaoformapagamento(String descricaoformapagamento) {
		this.descricaoformapagamento = descricaoformapagamento;
	}
	public Long getTipopagamento_id() {
		return tipopagamento_id;
	}
	public void setTipopagamento_id(Long tipopagamento_id) {
		this.tipopagamento_id = tipopagamento_id;
	}
	public String getDescricaotipopagamento() {
		return descricaotipopagamento;
	}
	public void setDescricaotipopagamento(String descricaotipopagamento) {
		this.descricaotipopagamento = descricaotipopagamento;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contapagar_id == null) ? 0 : contapagar_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaPagarPendenteView other = (ContaPagarPendenteView) obj;
		if (contapagar_id == null) {
			if (other.contapagar_id != null)
				return false;
		} else if (!contapagar_id.equals(other.contapagar_id))
			return false;
		return true;
	}

}
