package br.com.insideincloud.brainstormapp.api.repository.responsavelview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.ResponsavelView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavelViewFilter;

public interface ResponsaveisViewQuery {
	
	public Page<ResponsavelView> filtrar(ResponsavelViewFilter filtro, Pageable page);

}
