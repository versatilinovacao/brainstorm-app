package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cnaes",schema="contabil")
public class Cnae implements Serializable {
	private static final long serialVersionUID = 1944603537758231163L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cnae_id;
	@Column(name="secao",length=6)
	private String secao;
	@Column(name="divisao",length=6)
	private String divisao;
	@Column(name="grupo",length=6)
	private String grupo;
	@Column(name="classe",length=8)
	private String classe;
	@Column(name="subclasse",length=10)
	private String subclasse;
	@Column(name="codigo",length=15)
	private String codigo;
	@Column(name="denominacao",length=500)
	private String denominacao;
	
	public Long getCnae_id() {
		return cnae_id;
	}
	public void setCnae_id(Long cnae_id) {
		this.cnae_id = cnae_id;
	}
	public String getSecao() {
		return secao;
	}
	public void setSecao(String secao) {
		this.secao = secao;
	}
	public String getDivisao() {
		return divisao;
	}
	public void setDivisao(String divisao) {
		this.divisao = divisao;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getSubclasse() {
		return subclasse;
	}
	public void setSubclasse(String subclasse) {
		this.subclasse = subclasse;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDenominacao() {
		return denominacao;
	}
	public void setDenominacao(String denominacao) {
		this.denominacao = denominacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnae_id == null) ? 0 : cnae_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cnae other = (Cnae) obj;
		if (cnae_id == null) {
			if (other.cnae_id != null)
				return false;
		} else if (!cnae_id.equals(other.cnae_id))
			return false;
		return true;
	}
	
	
}
