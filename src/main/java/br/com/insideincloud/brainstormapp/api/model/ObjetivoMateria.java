package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="materias",schema="public")
public class ObjetivoMateria  implements Serializable {
	private static final long serialVersionUID = -971505052047068645L;

	@Id
	private Long materia_id;
	private String descricao;
	
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materia_id == null) ? 0 : materia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjetivoMateria other = (ObjetivoMateria) obj;
		if (materia_id == null) {
			if (other.materia_id != null)
				return false;
		} else if (!materia_id.equals(other.materia_id))
			return false;
		return true;
	}
	
	
}
