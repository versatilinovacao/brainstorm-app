package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="selecionar_periodo_letivo_view",schema="escola")
public class PeriodoLetivoView implements Serializable {
	private static final long serialVersionUID = -7017602243532230826L;

	@Id
	private Long periodo_letivo_id;
	@Column(name="descricao")
	private String descricao;
	@Column(name="ano",length=4)
	private String ano;
	private boolean status;
	private Long empresa_id;
	
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((periodo_letivo_id == null) ? 0 : periodo_letivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoLetivoView other = (PeriodoLetivoView) obj;
		if (periodo_letivo_id == null) {
			if (other.periodo_letivo_id != null)
				return false;
		} else if (!periodo_letivo_id.equals(other.periodo_letivo_id))
			return false;
		return true;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	
	
}
