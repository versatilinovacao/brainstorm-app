package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="diasemanas")
public class DiaSemana implements Serializable {
	private static final long serialVersionUID = -5581330561145894013L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long diasemana_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	public Long getDiasemana_id() {
		return diasemana_id;
	}
	public void setDiasemana_id(Long diasemana_id) {
		this.diasemana_id = diasemana_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diasemana_id == null) ? 0 : diasemana_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiaSemana other = (DiaSemana) obj;
		if (diasemana_id == null) {
			if (other.diasemana_id != null)
				return false;
		} else if (!diasemana_id.equals(other.diasemana_id))
			return false;
		return true;
	}	
	
}
