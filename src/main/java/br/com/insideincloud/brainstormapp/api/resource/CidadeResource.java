package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Cidade;
import br.com.insideincloud.brainstormapp.api.repository.Cidades;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/cidades")
public class CidadeResource {
	@Autowired
	private Cidades cidades;
	
	@GetMapping("/{estado_id}")
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Cidade> conteudo(@PathVariable Long estado_id) {
		RetornoWrapper<Cidade> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper(); 
		
		try {
			retorno.setConteudo(cidades.findByEstado(estado_id));
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações das Cidades, favor tentar novamente mais tarde!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		} 
		
		return retorno;
	}
}
