package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;

public class GrupoSecurityDelete {
	private List<GrupoSecurity> gruposSecurity;

	public List<GrupoSecurity> getGruposSecurity() {
		return gruposSecurity;
	}

	public void setGruposSecurity(List<GrupoSecurity> gruposSecurity) {
		this.gruposSecurity = gruposSecurity;
	}
	
}
