package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="alunos",schema="escola")
public class Aluno implements Serializable {
	private static final long serialVersionUID = 1269933181777221799L;
	
	@EmbeddedId
	private ColaboradorPK colaborador_id;

//	@ManyToMany(fetch=FetchType.EAGER)
//	@JoinTable(name="alunos_atividades", schema="escola", joinColumns = {@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")}, 
//										 inverseJoinColumns = @JoinColumn(name="atividade_id"))
//	private List<Atividade> atividades;
//	
//	public List<Atividade> getAtividades() {
//		return atividades;
//	}
	
//	@Fetch(FetchMode.SELECT)
//	@ManyToMany(fetch=FetchType.EAGER)
//	@JoinTable(name="arquivos_por_atividades",schema="escola", joinColumns = {@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")},
//															   inverseJoinColumns = {@JoinColumn(name="arquivoporatividade_id")})
//	private List<ArquivoPorAtividade> arquivos;

//	public void setAtividades(List<Atividade> atividades) {
//		this.atividades = atividades;
//	}

	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}

	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}

	public void setAluno_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(colaborador_id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Aluno))
			return false;
		Aluno other = (Aluno) obj;
		return Objects.equals(colaborador_id, other.colaborador_id);
	}
	
	
}
