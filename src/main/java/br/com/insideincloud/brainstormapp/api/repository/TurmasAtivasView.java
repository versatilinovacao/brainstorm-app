package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.TurmaAtivaView;

@Repository
public interface TurmasAtivasView extends JpaRepository<TurmaAtivaView, Long> {

}
