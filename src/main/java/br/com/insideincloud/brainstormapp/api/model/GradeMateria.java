package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="gradematerias")
public class GradeMateria implements Serializable {
	private static final long serialVersionUID = 6779943940906345193L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long grademateria_id;
	@ManyToOne
	@JoinColumn(name="composicao_id")
	private GradeMateria composicao;
	@OneToOne
	@JoinColumn(name="materia_id")
	private Materia materia;
	@OneToOne
	@JoinColumn(name="diasemana_id") 
	private DiaSemana diasemana;
	@OneToOne
	@JoinColumn(name="ambiente_id")
	private Ambiente sala;
	@OneToOne
	@JoinColumn(name="turma_id")
	private Turma turma;
	@OneToOne
	@JoinColumn(name="turno_id")
	private Turno turno;
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador colaborador;
	private LocalTime inicio;
	private LocalTime fim;
	@Column(name="justificativa")	
	private String justificativa;
	
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public GradeMateria getComposicao() {
		return composicao;
	}
	public void setComposicao(GradeMateria composicao) {
		this.composicao = composicao;
	}
	public Turno getTurno() {
		return turno;
	}
	public void setTurno(Turno turno) {
		this.turno = turno;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public String getJustificativa() {
		return justificativa;
	}
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	public Long getGrademateria_id() {
		return grademateria_id;
	}
	public void setGrademateria_id(Long grademateria_id) {
		this.grademateria_id = grademateria_id;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	public DiaSemana getDiasemana() {
		return diasemana;
	}
	public void setDiasemana(DiaSemana diasemana) {
		this.diasemana = diasemana;
	}
	public Ambiente getSala() {
		return sala;
	}
	public void setSala(Ambiente sala) {
		this.sala = sala;
	}
	public LocalTime getInicio() {
		return inicio;
	}
	public void setInicio(LocalTime inicio) {
		this.inicio = inicio;
	}
	public LocalTime getFim() {
		return fim;
	}
	public void setFim(LocalTime fim) {
		this.fim = fim;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grademateria_id == null) ? 0 : grademateria_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GradeMateria other = (GradeMateria) obj;
		if (grademateria_id == null) {
			if (other.grademateria_id != null)
				return false;
		} else if (!grademateria_id.equals(other.grademateria_id))
			return false;
		return true;
	}

}
