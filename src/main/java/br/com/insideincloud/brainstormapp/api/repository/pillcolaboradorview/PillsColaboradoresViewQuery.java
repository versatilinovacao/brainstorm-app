package br.com.insideincloud.brainstormapp.api.repository.pillcolaboradorview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.PillColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PillColaboradorViewFilter;

public interface PillsColaboradoresViewQuery {
	public Page<PillColaboradorView> filtrar(PillColaboradorViewFilter filtro, Pageable page);

}
