package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="fichas_alunos_view",schema="escola")
public class FichaAlunoView implements Serializable {
	private static final long serialVersionUID = -5393276787175890571L;
	
	@Id
	private Long aluno_id;
	private Long composicao_aluno_id;
	private String nome;
	private String matricula;
	private String data_matricula;
	private String data_nascimento;
	private String genero;
	private String endereco;
	private String cep;
	private String bairro;
	private String cidade;
	private String responsavel;
	private Integer idade;
	@Column(name="guarda_alternada",length=1)
	private String guarda_alternada;
	@Column(name="guarda_compartilhada",length=1)
	private String guarda_compartilhada;
	@Column(name="guarda_nidal",length=1)
	private String guarda_nidal;
	@Column(name="guarda_unilateral")
	private String guarda_unilateral;
	private String observacao;
	private String outraescola;
	private String qualoutraescola;
	private String qualoutraescolatempo;
	private String qualoutraescolaadaptacao;
	@Transient
	private List<FichaAlunoView> fichas;
	@Transient
	private List<FichaAlunoFiliacaoView> filiacao;
	@Transient
	private String subReport;
	
	public String getSubReport() {
		subReport = "/var/local/ecosistema/repositorio/relatorios/escola/ficha_aluno_filiacao.jasper";
		return subReport;
	}
	public void setSubReport(String subReport) {
		this.subReport = subReport;
	}
	public List<FichaAlunoFiliacaoView> getFiliacao() {
		return filiacao;
	}
	public void setFiliacao(List<FichaAlunoFiliacaoView> filiacao) {
		this.filiacao = filiacao;
	}
	public String getData_matricula() {
		return data_matricula;
	}
	public void setData_matricula(String data_matricula) {
		this.data_matricula = data_matricula;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public String getQualoutraescolatempo() {
		return qualoutraescolatempo;
	}
	public void setQualoutraescolatempo(String qualoutraescolatempo) {
		this.qualoutraescolatempo = qualoutraescolatempo;
	}
	public String getQualoutraescolaadaptacao() {
		return qualoutraescolaadaptacao;
	}
	public void setQualoutraescolaadaptacao(String qualoutraescolaadaptacao) {
		this.qualoutraescolaadaptacao = qualoutraescolaadaptacao;
	}
	public List<FichaAlunoView> getFichas() {
		return fichas;
	}
	public void setFichas(List<FichaAlunoView> fichas) {
		this.fichas = fichas;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public String getGuarda_alternada() {
		return guarda_alternada;
	}
	public void setGuarda_alternada(String guarda_alternada) {
		this.guarda_alternada = guarda_alternada;
	}
	public String getGuarda_compartilhada() {
		return guarda_compartilhada;
	}
	public void setGuarda_compartilhada(String guarda_compartilhada) {
		this.guarda_compartilhada = guarda_compartilhada;
	}
	public String getGuarda_nidal() {
		return guarda_nidal;
	}
	public void setGuarda_nidal(String guarda_nidal) {
		this.guarda_nidal = guarda_nidal;
	}
	public String getGuarda_unilateral() {
		return guarda_unilateral;
	}
	public void setGuarda_unilateral(String guarda_unilateral) {
		this.guarda_unilateral = guarda_unilateral;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getOutraescola() {
		return outraescola;
	}
	public void setOutraescola(String outraescola) {
		this.outraescola = outraescola;
	}
	public String getQualoutraescola() {
		return qualoutraescola;
	}
	public void setQualoutraescola(String qualoutraescola) {
		this.qualoutraescola = qualoutraescola;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FichaAlunoView other = (FichaAlunoView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		return true;
	}
}
