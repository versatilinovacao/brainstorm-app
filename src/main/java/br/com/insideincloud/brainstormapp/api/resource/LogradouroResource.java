package br.com.insideincloud.brainstormapp.api.resource;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.dblink.Logradouro;
import br.com.insideincloud.brainstormapp.api.repository.LogradourosRemoto;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/logradouros")
public class LogradouroResource  {
	
	@Autowired
	private LogradourosRemoto logradouros;

	
	
	@GetMapping("/{pais_id}/{estado_id}/{cidade_id}")
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Logradouro> conteudo(@PathVariable Long pais_id, @PathVariable Long estado_id, @PathVariable Long cidade_id, HttpServletRequest request) {
		RetornoWrapper<Logradouro> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
				
		try {
			retorno.setConteudo( logradouros.findByEstadoCidade(pais_id,estado_id,cidade_id) );	
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações do endereço, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}

		System.out.println("EMPRESA >>>>>>>>>>>>>>>>> EMPRESA "+request.getHeader("Empresa-Type"));
		
		return retorno;
	}
	
	
	@GetMapping("/{pais_id}/{estado_id}/{cidade_id}/{rua}")
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Logradouro> conteudo(@PathVariable Long pais_id, @PathVariable Long estado_id, @PathVariable Long cidade_id, @PathVariable String rua) {
		RetornoWrapper<Logradouro> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<Logradouro> l = logradouros.findByEstadoCidade(pais_id, estado_id, cidade_id);
			l = l.stream()
					.sorted(Comparator.comparing(Logradouro::getNome))
					//.filter( value -> value.getNome().startsWith(rua))
					.filter( value -> value.getNome().toUpperCase().contains(rua.toUpperCase()))
					.collect(Collectors.toList());
			retorno.setConteudo( l );
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações do endereço, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
//		logradouros.filtrar(filtro, page);
		return retorno;
	}
	
	@GetMapping("/rua/{cep}")
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Logradouro> conteudoLogradouro(@PathVariable String cep) {
		RetornoWrapper<Logradouro> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( logradouros.findByEndereco(cep) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o endereço pelo cep fornecido.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}
