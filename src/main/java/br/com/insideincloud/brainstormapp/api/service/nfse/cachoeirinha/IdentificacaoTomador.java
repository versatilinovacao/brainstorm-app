package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class IdentificacaoTomador {
	private CpfCnpj CpfCnpj;

	public CpfCnpj getCpfCnpj() {
		return CpfCnpj;
	}

	public void setCpfCnpj(CpfCnpj cpfCnpj) {
		CpfCnpj = cpfCnpj;
	}

}
