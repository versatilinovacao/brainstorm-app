package br.com.insideincloud.brainstormapp.api.repository.filter;

import java.time.LocalTime;

public class GradeMateriaFilter {
	
	private String grademateria_id;
	private String turma_id;
	private String materia_id;
	private String colaborador_id;
	private String diasemana_id;
	private String sala_id;
	private String turno_id;
	private LocalTime inicio;
	private LocalTime fim;
	
	public String getGrademateria_id() {
		return grademateria_id;
	}
	public void setGrademateria_id(String grademateria_id) {
		this.grademateria_id = grademateria_id;
	}
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(String materia_id) {
		this.materia_id = materia_id;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getDiasemana_id() {
		return diasemana_id;
	}
	public void setDiasemana_id(String diasemana_id) {
		this.diasemana_id = diasemana_id;
	}
	public String getSala_id() {
		return sala_id;
	}
	public void setSala_id(String sala_id) {
		this.sala_id = sala_id;
	}
	public String getTurno_id() {
		return turno_id;
	}
	public void setTurno_id(String turno_id) {
		this.turno_id = turno_id;
	}
	public LocalTime getInicio() {
		return inicio;
	}
	public void setInicio(LocalTime inicio) {
		this.inicio = inicio;
	}
	public LocalTime getFim() {
		return fim;
	}
	public void setFim(LocalTime fim) {
		this.fim = fim;
	}
	
}
