package br.com.insideincloud.brainstormapp.api.service.lib;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailService {
	
	private String to;
	private String from;
	private String conteudo;
	private String assinatura;
	private String assunto;
	
	public String sendMail(JavaMailSender mailSender) {
		try {
//			MimeMessage mail = mailSender.createMimeMessage();
//			MimeMessageHelper helper = new MimeMessageHelper( mail );
//			helper.setTo(this.to);
//			helper.setSubject(this.assunto);
//			helper.setText(this.conteudo,true);
//			mailSender.send(mail);
			
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "Erro ao enviar e-mail";
		}
		
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getAssinatura() {
		return assinatura;
	}

	public void setAssinatura(String assinatura) {
		this.assinatura = assinatura;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

}
