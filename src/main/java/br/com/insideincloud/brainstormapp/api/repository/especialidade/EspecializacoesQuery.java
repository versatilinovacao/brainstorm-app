package br.com.insideincloud.brainstormapp.api.repository.especialidade;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Especialidade;
import br.com.insideincloud.brainstormapp.api.repository.filter.EspecialidadeFilter;

public interface EspecializacoesQuery {
	Page<Especialidade> filtrar(EspecialidadeFilter filtro, Pageable page);
}
