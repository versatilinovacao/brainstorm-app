package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jdom.Element;

@Entity
@Table(name="tomador",schema="nfse")
public class Tomador implements Serializable {
	private static final long serialVersionUID = 3974242718605535430L;

	@Id
	private Long tomador_id;
	private String cpf;
	private String cnpj;
	private String razaoSocial;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String codigomunicipio;
	private String uf;
	private String cep;
	private String telefone;
	private String email;

	private Element telefoneNode = new Element("Telefone");
	private Element emailNode = new Element("Email");
	private Element contatoNode = new Element("Contato");
	private Element logradouroNode = new Element("Endereco");
	private Element enderecoNode = new Element("Endereco");
	private Element numeroNode = new Element("Numero");
	private Element complementoNode = new Element("Complemento");
	private Element bairroNode = new Element("Bairro");
	private Element codigoMunicipioNode = new Element("CodigoMunicipio");
	private Element ufNode = new Element("Uf");
	private Element cepNode = new Element("Cep");
	private Element razaoSocialNode = new Element("RazaoSocial");
	private Element cnpjNode = new Element("Cnpj");
	private Element cpfNode = new Element("Cpf");
	private Element cpfCnpjNode = new Element("CpfCnpj");
	private Element identificacaoTomadorNode = new Element("IdentificacaoTomador");
	
	public void init() {		
		
		
	}
	
	public Element nodeXML() { 
		identificacaoTomadorNode.addContent(cpfCnpjNode);
		identificacaoTomadorNode.addContent(enderecoNode);
		identificacaoTomadorNode.addContent(contatoNode);
		
		return identificacaoTomadorNode;	
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		contatoNode.addContent(telefoneNode);
		telefoneNode.setText(telefone);
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		contatoNode.addContent(emailNode);
		emailNode.setText(email);
		this.email = email;
	}

	public Long getTomador_id() {
		return tomador_id;
	}

	public void setTomador_id(Long tomador_id) {
		this.tomador_id = tomador_id;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		enderecoNode.addContent(logradouroNode);
		logradouroNode.setText(endereco);
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		enderecoNode.addContent(numeroNode);
		numeroNode.setText(numero);
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		enderecoNode.addContent(complementoNode);
		complementoNode.setText(complemento);
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		enderecoNode.addContent(bairroNode);
		bairroNode.setText(bairro);
		this.bairro = bairro;
	}

	public String getCodigomunicipio() {
		return codigomunicipio;
	}

	public void setCodigomunicipio(String codigomunicipio) {
		enderecoNode.addContent(codigoMunicipioNode);
		codigoMunicipioNode.setText(codigomunicipio);
		this.codigomunicipio = codigomunicipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		enderecoNode.addContent(ufNode);
		ufNode.setText(uf);
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		enderecoNode.addContent(cepNode);
		cepNode.setText(cep);
		this.cep = cep;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		cpfCnpjNode.addContent(cpfNode);
		cpfNode.setText(cpf);
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		cpfCnpjNode.addContent(cnpjNode);
		cnpjNode.setText(cnpj);
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		identificacaoTomadorNode.addContent(razaoSocialNode);
		razaoSocialNode.setText(razaoSocial);
		this.razaoSocial = razaoSocial;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tomador_id == null) ? 0 : tomador_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tomador other = (Tomador) obj;
		if (tomador_id == null) {
			if (other.tomador_id != null)
				return false;
		} else if (!tomador_id.equals(other.tomador_id))
			return false;
		return true;
	}
	
	
}
