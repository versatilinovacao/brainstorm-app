package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoMedico;
import br.com.insideincloud.brainstormapp.api.repository.TiposMedicos;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposmedicos")
public class TipoMedicoResource {

	@Autowired
	private TiposMedicos tiposmedicos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIPOMEDICO_CONTEUDO')")
	public RetornoWrapper<TipoMedico> conteudo() {
		RetornoWrapper<TipoMedico> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposmedicos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de médico.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{tipomedico_id}")
	@PreAuthorize("hasAuthority('ROLE_TIPOMEDICO_CONTEUDO')")
	public RetornoWrapper<TipoMedico> conteudoPorId(@PathVariable Long tipomedico_id) {
		RetornoWrapper<TipoMedico> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( tiposmedicos.findOne( tipomedico_id ) );
		} catch(Exception e ) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar tipo de médico.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
 		
		return retorno;
	}
}
