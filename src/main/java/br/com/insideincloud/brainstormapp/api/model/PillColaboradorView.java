package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pill_colaborador_view")
public class PillColaboradorView implements Serializable {
	private static final long serialVersionUID = 6896276199745227011L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pill_colaborador_id;
	@Column(name="label")
	private String label;
	private Long colaborador_id;
	
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getPill_colaborador_id() {
		return pill_colaborador_id;
	}
	public void setPill_colaborador_id(Long pill_colaborador_id) {
		this.pill_colaborador_id = pill_colaborador_id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pill_colaborador_id == null) ? 0 : pill_colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PillColaboradorView other = (PillColaboradorView) obj;
		if (pill_colaborador_id == null) {
			if (other.pill_colaborador_id != null)
				return false;
		} else if (!pill_colaborador_id.equals(other.pill_colaborador_id))
			return false;
		return true;
	}
	
	
}
