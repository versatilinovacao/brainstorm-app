package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="taxasselic",schema="tributacao")
public class TaxaSelic implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long taxaselic_id;
    private LocalDate movimento;
    private BigDecimal taxa;
    private BigDecimal fator;
    private BigDecimal financeiro;
    private BigDecimal media;
    private BigDecimal mediana;
    private BigDecimal moda;
    private BigDecimal desvio;
    private BigDecimal curtose;
    private int operacoes;
    
	public void setFinanceiro(BigDecimal financeiro) { this.financeiro = financeiro; }
	public void setMedia(BigDecimal media) { this.media = media; }
	public void setMediana(BigDecimal mediana) { this.mediana = mediana; }
	public void setModa(BigDecimal moda) { this.moda = moda; }
	public void setDesvio(BigDecimal desvio) { this.desvio = desvio; }
	public void setCurtose(BigDecimal curtose) { this.curtose = curtose; }	
	public void setTaxaselic_id(Long taxaselic_id) { this.taxaselic_id = taxaselic_id; }
    public void setMovimento(LocalDate movimento) { this.movimento = movimento; }
    public void setTaxa(BigDecimal taxa) { this.taxa = taxa; }
    public void setFator(BigDecimal fator) { this.fator = fator; }
    public void setOperacoes(int operacoes) { this.operacoes = operacoes; }
    
    public BigDecimal getFinanceiro() {
		return financeiro;
	}
	public BigDecimal getMedia() {
		return media;
	}
	public BigDecimal getMediana() {
		return mediana;
	}
	public BigDecimal getModa() {
		return moda;
	}
	public BigDecimal getDesvio() {
		return desvio;
	}
	public BigDecimal getCurtose() {
		return curtose;
	}
    public Long getTaxaselic_id() {
        return this.taxaselic_id;
    }
    public LocalDate getMovimento() {
        return this.movimento;
    }
    public BigDecimal getTaxa() {
        return this.taxa;
    }
    public BigDecimal getFator() {
        return this.fator;
    }
	public int getOperacoes() {
		return operacoes;
	}
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( (taxaselic_id == 1) ? 0 : taxaselic_id.hashCode() );
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TaxaSelic other = (TaxaSelic) obj;
        if (taxaselic_id == null) {
            if (other.taxaselic_id != null) 
                return false;
        } else if (!taxaselic_id.equals(other.taxaselic_id)) 
            return false;
        
        return true;
    }

}