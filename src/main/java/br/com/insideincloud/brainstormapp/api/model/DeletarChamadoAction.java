package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_chamados",schema="action")
public class DeletarChamadoAction implements Serializable {
	private static final long serialVersionUID = 2450737775388289093L;

	@Id
	private Long chamado_id;

	public Long getChamado_id() {
		return chamado_id;
	}

	public void setChamado_id(Long chamado_id) {
		this.chamado_id = chamado_id;
	}
	
}
