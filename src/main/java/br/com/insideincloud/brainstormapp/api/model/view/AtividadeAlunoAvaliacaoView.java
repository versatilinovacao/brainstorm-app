package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.insideincloud.brainstormapp.api.model.pk.AtividadeAlunoAvaliacaoPK;

@Entity
@Table(name="atividades_alunos_avaliacao_view",schema="escola")
public class AtividadeAlunoAvaliacaoView implements Serializable {
	private static final long serialVersionUID = -2760247420522455853L;
	
	@Id
	private AtividadeAlunoAvaliacaoPK id;
	@Column(name="avaliacaoalunoatividade_id")
	private Long avaliacaoalunoatividade_id;
	@Column(name="colaborador_id")
	private Long aluno_id;
	@Column(name="composicao_id")
	private Long composicao_aluno_id;
	private String nomealuno;
	private Integer nota;
	private String conceito;
	@Column(name="avaliacao_descritiva")
	private String avaliacaodescritiva;
	private Long situacaoatividade_id;
	private String nomesituacaoatividade;
	private Boolean revisar;
	private String transcricao;
	private Boolean concluido;
	@Transient
	private Boolean isanexo;
	
	public Boolean getIsanexo() {
		return isanexo;
	}
	public void setIsanexo(Boolean isanexo) {
		this.isanexo = isanexo;
	}
	public String getTranscricao() {
		return transcricao;
	}
	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
	public Boolean getRevisar() {
		return revisar;
	}
	public void setRevisar(Boolean revisar) {
		this.revisar = revisar;
	}
	public Long getSituacaoatividade_id() {
		return situacaoatividade_id;
	}
	public void setSituacaoatividade_id(Long situacaoatividade_id) {
		this.situacaoatividade_id = situacaoatividade_id;
	}
	public String getNomesituacaoatividade() {
		return nomesituacaoatividade;
	}
	public void setNomesituacaoatividade(String nomesituacaoatividade) {
		this.nomesituacaoatividade = nomesituacaoatividade;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public Long getAvaliacaoalunoatividade_id() {
		return avaliacaoalunoatividade_id;
	}
	public void setAvaliacaoalunoatividade_id(Long avaliacaoalunoatividade_id) {
		this.avaliacaoalunoatividade_id = avaliacaoalunoatividade_id;
	}
	public String getNomealuno() {
		return nomealuno;
	}
	public void setNomealuno(String nomealuno) {
		this.nomealuno = nomealuno;
	}
	public AtividadeAlunoAvaliacaoPK getId() {
		return id;
	}
	public void setId(AtividadeAlunoAvaliacaoPK id) {
		this.id = id;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getAvaliacaodescritiva() {
		return avaliacaodescritiva;
	}
	public void setAvaliacaodescritiva(String avaliacaodescritiva) {
		this.avaliacaodescritiva = avaliacaodescritiva;
	}
	public Boolean getConcluido() {
		return concluido;
	}
	public void setConcluido(Boolean concluido) {
		this.concluido = concluido;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeAlunoAvaliacaoView other = (AtividadeAlunoAvaliacaoView) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
