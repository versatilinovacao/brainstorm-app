package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.CertificadoA1;
import br.com.insideincloud.brainstormapp.api.model.Rps;
import br.com.insideincloud.brainstormapp.api.model.pk.CertificadoA1PK;
import br.com.insideincloud.brainstormapp.api.repository.CertificadosA1;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.RpsCachoeirinhaService;

@RestController
@RequestMapping("/rps")
public class RpsCachoeirinhaResource {
	@Autowired
	private RpsCachoeirinhaService rpsService;
	
	@Autowired
	private CertificadosA1 certificados;
	
	@GetMapping("/certificados")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_CONTEUDO')")
	public List<CertificadoA1> certificados() {
		
		
		return certificados.findAll();
	}

	@GetMapping("/certificados/{id}/{empresa}")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_CONTEUDO')")
	public CertificadoA1 certificado(@PathVariable Long id, @PathVariable Long empresa) {
		CertificadoA1PK chave = new CertificadoA1PK();
		chave.setCertificado_id(id);
		chave.setEmpresa_id(empresa);
		
		System.out.println("TESTE "+id+ " <-----------> "+empresa);
		
		return certificados.findOne(chave);
	}
	
	@PostMapping("/salvar")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	public RetornoWrapper<CertificadoA1> salvarCertificado(@RequestBody CertificadoA1 certificado) {
		RetornoWrapper<CertificadoA1> retorno = new RetornoWrapper<CertificadoA1>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			CertificadoA1PK certificadoPk = new CertificadoA1PK();
			certificadoPk.setCertificado_id(1L);
			certificadoPk.setEmpresa_id(1L);
			certificado.setCertificado_id(certificadoPk);
			
			certificado = certificados.saveAndFlush(certificado);
			retorno.setSingle(certificado);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Erro ao gravar certificado A1");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
//		Object config = new DefaultClientConfig();
//		Client client = Client.create((ClientConfig)config);
//		WebResource request = client
		
		return retorno;
	}
	
	@PutMapping("/envio")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	public void envio() {
		
//		public void GravarXML() throws IOException {
//			XMLOutputter xout = new XMLOutputter();
//			FileWriter arquivo = new FileWriter(new File("/var/local/ecosistema/tmp/rps.xml"));
//			xout.output(documento, arquivo);
//		}
		
		
		
		Rps r = new Rps();
//		r.inicializa();
//		r.setRps_id(100L);
//		try {
//			r.GravarXML();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		/*
		try {
			//rpsService.gerarXML();
			rpsService.loadCertificates("", "",null);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
	}
}
