package br.com.insideincloud.brainstormapp.api.repository.filter;

public class UsuarioViewFilter {
	private String usuario_view;
	private String empresa_id;
	private String nome;
	private String status;
	private String email;
	
	public String getUsuario_view() {
		return usuario_view;
	}
	public void setUsuario_view(String usuario_view) {
		this.usuario_view = usuario_view;
	}
	public String getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(String empresa_id) {
		this.empresa_id = empresa_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
}
