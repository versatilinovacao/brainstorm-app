package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_telefones")
public class TipoTelefone implements Serializable {
	private static final long serialVersionUID = 8162474470432457925L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_telefone_id;
	@Column(name="descricao",length=60)
	private String descricao;
	
	public Long getTipo_contato_id() {
		return tipo_telefone_id;
	}
	public void setTipo_contato_id(Long tipo_contato_id) {
		this.tipo_telefone_id = tipo_contato_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_telefone_id == null) ? 0 : tipo_telefone_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoTelefone other = (TipoTelefone) obj;
		if (tipo_telefone_id == null) {
			if (other.tipo_telefone_id != null)
				return false;
		} else if (!tipo_telefone_id.equals(other.tipo_telefone_id))
			return false;
		return true;
	}	
	
}
