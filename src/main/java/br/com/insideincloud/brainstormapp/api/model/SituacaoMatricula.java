package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="situacao_matricula")
public class SituacaoMatricula implements Serializable {
	private static final long serialVersionUID = 1877317466269040771L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long situacao_matricula_id;
	@Column(name="descricao")
	private String descricao;
	
	public Long getSituacao_matricula_id() {
		return situacao_matricula_id;
	}
	public void setSituacao_matricula_id(Long situacao_matricula_id) {
		this.situacao_matricula_id = situacao_matricula_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((situacao_matricula_id == null) ? 0 : situacao_matricula_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SituacaoMatricula other = (SituacaoMatricula) obj;
		if (situacao_matricula_id == null) {
			if (other.situacao_matricula_id != null)
				return false;
		} else if (!situacao_matricula_id.equals(other.situacao_matricula_id))
			return false;
		return true;
	}	

}
