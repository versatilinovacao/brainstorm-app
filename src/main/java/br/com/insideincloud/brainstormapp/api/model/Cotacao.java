package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

public class Cotacao implements Serializable{
	private static final long serialVersionUID = -7567575213210311893L;

	private Long cotacao_id;
	private LocalDate data;
	private Acao acao;
	private BigDecimal abertura;
	private BigDecimal minima;
	private BigDecimal maxima;
	private BigDecimal media;
	private BigDecimal ultima;
	
	public Long getCotacao_id() {
		return cotacao_id;
	}
	public void setCotacao_id(Long cotacao_id) {
		this.cotacao_id = cotacao_id;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public Acao getAcao() {
		return acao;
	}
	public void setAcao(Acao acao) {
		this.acao = acao;
	}
	public BigDecimal getAbertura() {
		return abertura;
	}
	public void setAbertura(BigDecimal abertura) {
		this.abertura = abertura;
	}
	public BigDecimal getMinima() {
		return minima;
	}
	public void setMinima(BigDecimal minima) {
		this.minima = minima;
	}
	public BigDecimal getMaxima() {
		return maxima;
	}
	public void setMaxima(BigDecimal maxima) {
		this.maxima = maxima;
	}
	public BigDecimal getMedia() {
		return media;
	}
	public void setMedia(BigDecimal media) {
		this.media = media;
	}
	public BigDecimal getUltima() {
		return ultima;
	}
	public void setUltima(BigDecimal ultima) {
		this.ultima = ultima;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cotacao_id == null) ? 0 : cotacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cotacao other = (Cotacao) obj;
		if (cotacao_id == null) {
			if (other.cotacao_id != null)
				return false;
		} else if (!cotacao_id.equals(other.cotacao_id))
			return false;
		return true;
	}
	
	
}
