package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.Sede;
import br.com.insideincloud.brainstormapp.api.repository.Sedes;
import br.com.insideincloud.brainstormapp.api.repository.filter.SedeFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.SedeDelete;

@RestController
@RequestMapping("/sedesempresas")
public class SedeResource {
	
	@Autowired
	private Sedes sedes;

	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_SEDEEMPRESAS_CONTEUDO')")	
	public Page<Sede> conteudo(@RequestBody SedeFilter filtro, @PageableDefault(size=5) Pageable page) {
		return sedes.filtrar(filtro, page);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_SEDEEMPRESAS_SALVAR')")	
	public ResponseEntity<Sede> salvar(@RequestBody Sede sede, HttpServletResponse response) {
		sede = sedes.saveAndFlush(sede);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(sede.getSede_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(sede);
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_SEDEEMPRESAS_DELETAR')")	
	public Page<Sede> deletar(@RequestBody SedeDelete delete, @PageableDefault(size=10) Pageable page) {
		
//		for (Sede conteudo : delete.getSedes()) {
//			DeletarSedeAction deletarMatricula = new DeletarSedeAction();
//			deletarSede.setSede_id(conteudo.getSede_id());
//			
//			deletarSede.save(deletarSede);
//			
//		};
		
		return sedes.findAll(page);
	}
	
}
