package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="contratos_ativos_view",schema="administracao")
public class ContratoAtivoView implements Serializable {
	private static final long serialVersionUID = -5919737921760298372L;
	
	@Id
	private Long contrato_id;
    private Long composicao_id;
    private Long contratada_id;
    private Long composicao_contratada_id;
    private String nomecontratada;
    private String nomefantasiacontratada;
    private Long contratante_id;
    private Long composicao_contratante_id;
    private String nomecontratante;
    private String nomefantasiacontratante;
    private Long artefato_id;
    private String nomeartefato;
    @Column(name="inicio_contrato")
    private LocalDate iniciocontrato;
    @Column(name="duracao_inicial")
    private Integer duracaoinicial;
    private Integer renovacoes;
    private Double mensalidade;
    private Long indicereajuste_id;
    private String siglaindice;
    private String descricaoindice;
    private Long tiporenovacao_id;
    private String descricaorenovacao;
    private Boolean status;
    
	public Long getContrato_id() {
		return contrato_id;
	}
	public void setContrato_id(Long contrato_id) {
		this.contrato_id = contrato_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public Long getContratada_id() {
		return contratada_id;
	}
	public void setContratada_id(Long contratada_id) {
		this.contratada_id = contratada_id;
	}
	public Long getComposicao_contratada_id() {
		return composicao_contratada_id;
	}
	public void setComposicao_contratada_id(Long composicao_contratada_id) {
		this.composicao_contratada_id = composicao_contratada_id;
	}
	public String getNomecontratada() {
		return nomecontratada;
	}
	public void setNomecontratada(String nomecontratada) {
		this.nomecontratada = nomecontratada;
	}
	public String getNomefantasiacontratada() {
		return nomefantasiacontratada;
	}
	public void setNomefantasiacontratada(String nomefantasiacontratada) {
		this.nomefantasiacontratada = nomefantasiacontratada;
	}
	public Long getContratante_id() {
		return contratante_id;
	}
	public void setContratante_id(Long contratante_id) {
		this.contratante_id = contratante_id;
	}
	public Long getComposicao_contratante_id() {
		return composicao_contratante_id;
	}
	public void setComposicao_contratante_id(Long composicao_contratante_id) {
		this.composicao_contratante_id = composicao_contratante_id;
	}
	public String getNomecontratante() {
		return nomecontratante;
	}
	public void setNomecontratante(String nomecontratante) {
		this.nomecontratante = nomecontratante;
	}
	public String getNomefantasiacontratante() {
		return nomefantasiacontratante;
	}
	public void setNomefantasiacontratante(String nomefantasiacontratante) {
		this.nomefantasiacontratante = nomefantasiacontratante;
	}
	public Long getArtefato_id() {
		return artefato_id;
	}
	public void setArtefato_id(Long artefato_id) {
		this.artefato_id = artefato_id;
	}
	public String getNomeartefato() {
		return nomeartefato;
	}
	public void setNomeartefato(String nomeartefato) {
		this.nomeartefato = nomeartefato;
	}
	public LocalDate getIniciocontrato() {
		return iniciocontrato;
	}
	public void setIniciocontrato(LocalDate iniciocontrato) {
		this.iniciocontrato = iniciocontrato;
	}
	public Integer getDuracaoinicial() {
		return duracaoinicial;
	}
	public void setDuracaoinicial(Integer duracaoinicial) {
		this.duracaoinicial = duracaoinicial;
	}
	public Integer getRenovacoes() {
		return renovacoes;
	}
	public void setRenovacoes(Integer renovacoes) {
		this.renovacoes = renovacoes;
	}
	public Double getMensalidade() {
		return mensalidade;
	}
	public void setMensalidade(Double mensalidade) {
		this.mensalidade = mensalidade;
	}
	public Long getIndicereajuste_id() {
		return indicereajuste_id;
	}
	public void setIndicereajuste_id(Long indicereajuste_id) {
		this.indicereajuste_id = indicereajuste_id;
	}
	public String getSiglaindice() {
		return siglaindice;
	}
	public void setSiglaindice(String siglaindice) {
		this.siglaindice = siglaindice;
	}
	public String getDescricaoindice() {
		return descricaoindice;
	}
	public void setDescricaoindice(String descricaoindice) {
		this.descricaoindice = descricaoindice;
	}
	public Long getTiporenovacao_id() {
		return tiporenovacao_id;
	}
	public void setTiporenovacao_id(Long tiporenovacao_id) {
		this.tiporenovacao_id = tiporenovacao_id;
	}
	public String getDescricaorenovacao() {
		return descricaorenovacao;
	}
	public void setDescricaorenovacao(String descricaorenovacao) {
		this.descricaorenovacao = descricaorenovacao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contrato_id == null) ? 0 : contrato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContratoAtivoView other = (ContratoAtivoView) obj;
		if (contrato_id == null) {
			if (other.contrato_id != null)
				return false;
		} else if (!contrato_id.equals(other.contrato_id))
			return false;
		return true;
	}	
}
