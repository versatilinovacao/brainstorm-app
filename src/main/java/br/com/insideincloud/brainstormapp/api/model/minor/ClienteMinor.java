package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="clientes_view")
public class ClienteMinor implements Serializable {
	private static final long serialVersionUID = 3249083143031750509L;
	
	@Id
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_id")})
	private ColaboradorPK cliente_id;
	private String nome;
	@Column(name="data_nascimento")
	private LocalDate nascimento;
	private String foto_real;
	private String foto_thunbnail;
	private String logradouro;
	private String numero;
	private String complemento;
	private String cep;
	@Column(name="descricaobairro")
	private String bairro;
	@Column(name="descricaocidade")
	private String cidade;
	@Column(name="descricaoestado")
	private String estado;
	@Column(name="descricaopais")
	private String pais;
	private String cpf;
	private String rg;
	private String certidao;
	private String email;
	
	public ColaboradorPK getCliente_id() {
		return cliente_id;
	}
	public void setCliente_id(ColaboradorPK cliente_id) {
		this.cliente_id = cliente_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}
	public String getFoto_real() {
		return foto_real;
	}
	public void setFoto_real(String foto_real) {
		this.foto_real = foto_real;
	}
	public String getFoto_thunbnail() {
		return foto_thunbnail;
	}
	public void setFoto_thunbnail(String foto_thunbnail) {
		this.foto_thunbnail = foto_thunbnail;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCertidao() {
		return certidao;
	}
	public void setCertidao(String certidao) {
		this.certidao = certidao;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente_id == null) ? 0 : cliente_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClienteMinor other = (ClienteMinor) obj;
		if (cliente_id == null) {
			if (other.cliente_id != null)
				return false;
		} else if (!cliente_id.equals(other.cliente_id))
			return false;
		return true;
	}
	
	
	
}
