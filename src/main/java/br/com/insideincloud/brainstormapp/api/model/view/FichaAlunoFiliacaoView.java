package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="fichas_alunos_filiacao_view",schema="escola")
public class FichaAlunoFiliacaoView implements Serializable {
	private static final long serialVersionUID = -6556511142009291924L;
	
	@Id
	private Long responsavel_id;
	private Long aluno_id;
	private Long composicao_aluno_id;
	private Long tipo_id;
	private String tipo;
	private String nome;
	private Integer idade;
	private String email;
	private String cpf;
	private String rg;
	private String data_nascimento;
	private String profissao;
	private String telefonepessoal;
	private String telefonecomercial;
	
	public String getTelefonecomercial() {
		return telefonecomercial;
	}
	public void setTelefonecomercial(String telefonecomercial) {
		this.telefonecomercial = telefonecomercial;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public Long getResponsavel_id() {
		return responsavel_id;
	}
	public void setResponsavel_id(Long responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public Long getTipo_id() {
		return tipo_id;
	}
	public void setTipo_id(Long tipo_id) {
		this.tipo_id = tipo_id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public String getTelefonepessoal() {
		return telefonepessoal;
	}
	public void setTelefonepessoal(String telefonepessoal) {
		this.telefonepessoal = telefonepessoal;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		result = prime * result + ((responsavel_id == null) ? 0 : responsavel_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FichaAlunoFiliacaoView other = (FichaAlunoFiliacaoView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		if (responsavel_id == null) {
			if (other.responsavel_id != null)
				return false;
		} else if (!responsavel_id.equals(other.responsavel_id))
			return false;
		return true;
	}

}
