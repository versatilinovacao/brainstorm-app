package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.Chamada;
import br.com.insideincloud.brainstormapp.api.repository.Chamadas;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaNavegadorFilter;
import br.com.insideincloud.brainstormapp.api.resource.chamada.ChamadaNavegador;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/chamadas")
public class ChamadaResource {
	
	@Autowired
	private Chamadas chamadas;
	
//	@Autowired
//	private DatasHorasView datasHoras;	
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<Chamada> salvar(@RequestBody Chamada chamada, HttpServletResponse response) {
		RetornoWrapper<Chamada> retorno = new RetornoWrapper<>();
		
		chamada = chamadas.saveAndFlush(chamada);
		retorno.setSingle(chamada);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(chamada.getChamada_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());

		return retorno;		
	}
	
	@PutMapping("/navegador")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<ChamadaNavegador> navegador(@RequestBody ChamadaNavegadorFilter filtro, HttpServletResponse response) {
		RetornoWrapper<ChamadaNavegador> retorno = new RetornoWrapper<>();
		
		ChamadaNavegador navegador = instanciaNavegador(filtro);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{data}").buildAndExpand(filtro.getHoje()).toUri();
				
		retorno.setSingle(navegador);
		
		return retorno;
	}
	
	private ChamadaNavegador instanciaNavegador(ChamadaNavegadorFilter filtro) {		
		ChamadaNavegador navegador = new ChamadaNavegador();
		if (StringUtils.isEmpty(filtro.getHoje())) {
			DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			navegador.setHoje( LocalDate.now().format(formato)); //System.currentTimeMillis()			
		} else {
			navegador.setHoje(filtro.getHoje()); 
		}
		
		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		navegador.setAmanha( navegador.getHoje().plusDays(1).format(formato) );
		navegador.setOntem( navegador.getHoje().minusDays(1).format(formato) );		
		
		return navegador;
	}
	
//	@GetMapping("/navegador")
//	public List<DataHoraView> navegador() {
//		
//		return datasHoras.findAll();
//	}
	
}
