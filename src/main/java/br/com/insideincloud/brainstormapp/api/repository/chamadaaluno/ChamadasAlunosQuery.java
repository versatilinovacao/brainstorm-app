package br.com.insideincloud.brainstormapp.api.repository.chamadaaluno;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.ChamadaAluno;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaAlunoFilter;

public interface ChamadasAlunosQuery {
	public Page<ChamadaAluno> filtrar(ChamadaAlunoFilter filtro, Pageable page);
}
