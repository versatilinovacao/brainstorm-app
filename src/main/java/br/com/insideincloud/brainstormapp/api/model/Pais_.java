package br.com.insideincloud.brainstormapp.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pais.class)
public class Pais_ {

	public static volatile SingularAttribute<Pais, Long> pais_id;
	public static volatile SingularAttribute<Pais, Long> codigo;
	public static volatile SingularAttribute<Pais, String> sigla;
	public static volatile SingularAttribute<Pais, String> descricao;
	
}
