package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="telas_sistema",schema="public")
public class TelaSistema implements Serializable {
	private static final long serialVersionUID = -2083132104850827441L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long telasistema_id;
	private String nome;
	private String label;
	private String descricao;
		
	@Fetch(FetchMode.JOIN)
	//@Cascade(CascadeType.MERGE)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="permissao_por_tela", joinColumns = @JoinColumn(name="telasistema_id"), inverseJoinColumns = @JoinColumn(name="permissao_id"))
	private List<Permissao> permissoes;
	
	
	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public Long getTelasistema_id() {
		return telasistema_id;
	}
	public void setTelasistema_id(Long telasistema_id) {
		this.telasistema_id = telasistema_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telasistema_id == null) ? 0 : telasistema_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelaSistema other = (TelaSistema) obj;
		if (telasistema_id == null) {
			if (other.telasistema_id != null)
				return false;
		} else if (!telasistema_id.equals(other.telasistema_id))
			return false;
		return true;
	}
	
}
