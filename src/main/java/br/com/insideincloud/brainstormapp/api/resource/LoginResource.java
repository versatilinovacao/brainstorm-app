package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;

@RestController
@RequestMapping("/login")
public class LoginResource {
	
	@Autowired
	private Colaboradores colaboradores;

	@GetMapping
	public  List<Colaborador> conteudo() {
		return colaboradores.findAll();
	}
	
}
