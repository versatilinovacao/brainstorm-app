package br.com.insideincloud.brainstormapp.api.repository.telefone;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Telefone;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneFilter;

public class TelefonesImpl implements TelefonesQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Telefone> filtrar(TelefoneFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Telefone> criteria = builder.createQuery(Telefone.class);
		Root<Telefone> root = criteria.from(Telefone.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<Telefone> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(TelefoneFilter filtro, CriteriaBuilder builder, Root<Telefone> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getTelefone_id())) {
				predicates.add(builder.equal(root.get("telefone_id"), Long.parseLong(filtro.getTelefone_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getNumero())) {
				predicates.add(builder.equal(root.get("numero"), filtro.getNumero() ));
			}		
			if (!StringUtils.isEmpty(filtro.getRamal())) {
				predicates.add(builder.equal(root.get("ramal"), filtro.getRamal() ));
			}		
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(TelefoneFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Telefone> root = criteria.from(Telefone.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
