package br.com.insideincloud.brainstormapp.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Empresa;
import br.com.insideincloud.brainstormapp.api.model.view.AlunoAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.FichaAlunoView;
import br.com.insideincloud.brainstormapp.api.report.caderno.FichaAlunoReport;
import br.com.insideincloud.brainstormapp.api.repository.AlunosAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.CadastroAlunosView;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.FichasAlunosFiliacoesView;
import br.com.insideincloud.brainstormapp.api.repository.FichasAlunosView;

@Service
public class AlunoService {

	@Autowired
	private AlunosAtividadesView alunosAtividades;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private CadastroAlunosView alunos;
	
	private Empresa empresa;
	
	@Autowired
	private FichasAlunosView fichas;
	
	@Autowired
	private FichaAlunoReport fichaReport;
	
	@Autowired
	private FichasAlunosFiliacoesView filiacoes;
	
	public List<AlunoAtividadeView> findAlunosAtividades(Long codigo) {
		
		return alunosAtividades.alunosAtividades(codigo);
	}
	
	public List<AlunoAtividadeView> findAlunosAtividadesPendentes(Long aluno_id) {
		return alunosAtividades.alunosAtividadesPendentes(aluno_id);
	}
	
	public List<AlunoAtividadeView> findByAtividadesPendentes() {
		return alunosAtividades.findByAtividadesPendentes();
	}
	
	public byte[] fichaAluno(Long aluno_id,Long composicao_aluno_id,Empresa empresa) {
		byte[] ficha = null;
		this.empresa = empresa;
		System.out.println("PRINT >>>>>>>>>>>>>>> FICHA");

		try {
			ficha = fichaReport.reportFicha(fichaDoAluno(aluno_id,composicao_aluno_id), empresa);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return ficha;
	}
	
	public List<FichaAlunoView> fichaDoAluno(Long aluno_id, Long composicao_aluno_id) {
		List<FichaAlunoView> fichaResult = new ArrayList<>();
		
		if (aluno_id > 0) {
			fichaResult = fichas.findByAluno(aluno_id);
			fichaResult.forEach(conteudo -> {
				List<FichaAlunoView> aux = new ArrayList<>();
				conteudo.setFiliacao( filiacoes.findByResponsaveis(conteudo.getAluno_id(), conteudo.getComposicao_aluno_id()) );
				aux.add(conteudo);
				conteudo.setFichas(aux);
			});
			return fichaResult;			
		} else {
			fichaResult = fichas.findAll();
			
			fichaResult.forEach(conteudo -> {
				List<FichaAlunoView> aux = new ArrayList<>();
				conteudo.setFiliacao( filiacoes.findByResponsaveis(conteudo.getAluno_id(), conteudo.getComposicao_aluno_id()) );
				aux.add(conteudo);
				conteudo.setFichas(aux);
			});
			
			return fichaResult;
		}		
	}
	
}
