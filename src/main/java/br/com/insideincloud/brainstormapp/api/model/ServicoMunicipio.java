package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.minor.CidadeMinor;

@Entity
@Table(name="servicos",schema="servico")
public class ServicoMunicipio implements Serializable {
	private static final long serialVersionUID = -561902487952055611L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long servico_id;
	private Double referencia;
	private String codigo_procempa;
	private String descricao;
	private Double aliquota;
	private LocalDate inicio;
	private LocalDate fim;
	@OneToOne
	@JoinColumn(name="municipio")
	private CidadeMinor cidade;
	private Boolean status;
	
	public Long getServico_id() {
		return servico_id;
	}
	public void setServico_id(Long servico_id) {
		this.servico_id = servico_id;
	}
	public Double getReferencia() {
		return referencia;
	}
	public void setReferencia(Double referencia) {
		this.referencia = referencia;
	}
	public String getCodigo_procempa() {
		return codigo_procempa;
	}
	public void setCodigo_procempa(String codigo_procempa) {
		this.codigo_procempa = codigo_procempa;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAliquota() {
		return aliquota;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public LocalDate getInicio() {
		return inicio;
	}
	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}
	public LocalDate getFim() {
		return fim;
	}
	public void setFim(LocalDate fim) {
		this.fim = fim;
	}
	public CidadeMinor getCidade() {
		return cidade;
	}
	public void setCidade(CidadeMinor cidade) {
		this.cidade = cidade;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((servico_id == null) ? 0 : servico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicoMunicipio other = (ServicoMunicipio) obj;
		if (servico_id == null) {
			if (other.servico_id != null)
				return false;
		} else if (!servico_id.equals(other.servico_id))
			return false;
		return true;
	}

}
