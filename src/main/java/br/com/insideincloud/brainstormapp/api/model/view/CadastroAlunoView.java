package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="alunos_view",schema="escola")
public class CadastroAlunoView implements Serializable {
	private static final long serialVersionUID = -5572572939223133462L;
	
	@EmbeddedId
	private ColaboradorPK colaborador_id;
	
	private Long aluno_id;
	private Long composicao_aluno_id;
	
	private String nome;
	private String cpf;
	@Column(name="data_nascimento")
	private LocalDate datanascimento;
	@Column(name="numero")
	private String telefone;
	@Column(name="foto_thumbnail")
	private String fotothumbnail;
	private String foto;
	
	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public LocalDate getDatanascimento() {
		return datanascimento;
	}
	public void setDatanascimento(LocalDate datanascimento) {
		this.datanascimento = datanascimento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getFotothumbnail() {
		return fotothumbnail;
	}
	public void setFotothumbnail(String fotothumbnail) {
		this.fotothumbnail = fotothumbnail;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		result = prime * result + ((composicao_aluno_id == null) ? 0 : composicao_aluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadastroAlunoView other = (CadastroAlunoView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		if (composicao_aluno_id == null) {
			if (other.composicao_aluno_id != null)
				return false;
		} else if (!composicao_aluno_id.equals(other.composicao_aluno_id))
			return false;
		return true;
	}
		
}
