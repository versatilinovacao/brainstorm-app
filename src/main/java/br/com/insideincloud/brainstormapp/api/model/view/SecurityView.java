package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="securityview",schema="public")
public class SecurityView implements Serializable {
	private static final long serialVersionUID = -1822318462858194032L;

	@Id
	private Long telasistemapermissao_id;
	private Long gruposecurity_id;
	private String nomegrupo;
	private Long telasistema_id;
	private String descricao;
	private Long permissao_id;
	private String nomepermissao;
	
	public Long getTelasistemapermissao_id() {
		return telasistemapermissao_id;
	}
	public void setTelasistemapermissao_id(Long telasistemapermissao_id) {
		this.telasistemapermissao_id = telasistemapermissao_id;
	}
	public Long getGruposecurity_id() {
		return gruposecurity_id;
	}
	public void setGruposecurity_id(Long gruposecurity_id) {
		this.gruposecurity_id = gruposecurity_id;
	}
	public String getNomegrupo() {
		return nomegrupo;
	}
	public void setNomegrupo(String nomegrupo) {
		this.nomegrupo = nomegrupo;
	}
	public Long getTelasistema_id() {
		return telasistema_id;
	}
	public void setTelasistema_id(Long telasistema_id) {
		this.telasistema_id = telasistema_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getPermissao_id() {
		return permissao_id;
	}
	public void setPermissao_id(Long permissao_id) {
		this.permissao_id = permissao_id;
	}
	public String getNomepermissao() {
		return nomepermissao;
	}
	public void setNomepermissao(String nomepermissao) {
		this.nomepermissao = nomepermissao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telasistemapermissao_id == null) ? 0 : telasistemapermissao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SecurityView other = (SecurityView) obj;
		if (telasistemapermissao_id == null) {
			if (other.telasistemapermissao_id != null)
				return false;
		} else if (!telasistemapermissao_id.equals(other.telasistemapermissao_id))
			return false;
		return true;
	}
	
	
}
