package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Cargo;
import br.com.insideincloud.brainstormapp.api.repository.cargo.CargosQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.CargoFilter;

@Repository
public interface Cargos extends JpaRepository<Cargo,Long>, CargosQuery {
	Page<Cargo> filtrar(CargoFilter filtro, Pageable page);

}
