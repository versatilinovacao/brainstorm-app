package br.com.insideincloud.brainstormapp.api.repository.resumoempresaview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import br.com.insideincloud.brainstormapp.api.model.view.ResumoEmpresaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResumoEmpresaViewFilter;

public interface ResumosEmpresasViewQuery {
	public Page<ResumoEmpresaView> filtrar(ResumoEmpresaViewFilter filtro,@PageableDefault(size=8) Pageable page);

}
