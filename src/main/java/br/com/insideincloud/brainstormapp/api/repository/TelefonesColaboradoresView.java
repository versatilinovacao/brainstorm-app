package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.TelefoneColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneColaboradorViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.telefonecolaboradorview.TelefonesColaboradoresViewQuery;

@Repository
public interface TelefonesColaboradoresView extends JpaRepository<TelefoneColaboradorView,Long>, TelefonesColaboradoresViewQuery {
	public Page<TelefoneColaboradorView> filtrar(TelefoneColaboradorViewFilter filtro, Pageable page);

	@Query("select v from TelefoneColaboradorView v where v.colaborador_id = ?1 and v.composicao_colaborador_id = ?2")
	public List<TelefoneColaboradorView> findByVinculosColaborador(Long colaborador_id, Long composicao_colaborador_id);

}
