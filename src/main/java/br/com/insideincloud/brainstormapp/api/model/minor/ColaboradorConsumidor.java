package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="colaborador",schema="public")
public class ColaboradorConsumidor implements Serializable {
	private static final long serialVersionUID = 8878132106645405799L;
	
	@Id
	private ColaboradorPK colaborador_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="fantasia",length=100)
	private String fantasia;
	
	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorConsumidor other = (ColaboradorConsumidor) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}
	
}
