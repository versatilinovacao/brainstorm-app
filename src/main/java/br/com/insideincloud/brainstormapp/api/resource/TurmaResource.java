package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Ambiente;
import br.com.insideincloud.brainstormapp.api.model.DiaSemana;
import br.com.insideincloud.brainstormapp.api.model.Materia;
import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.model.Turno;
import br.com.insideincloud.brainstormapp.api.model.view.AlunoTurmaView;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.model.view.TurmaAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.TurmaInativaView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosTurmasView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosVinculadosTurmasView;
import br.com.insideincloud.brainstormapp.api.repository.Ambientes;
import br.com.insideincloud.brainstormapp.api.repository.DiasSemanas;
import br.com.insideincloud.brainstormapp.api.repository.Materias;
import br.com.insideincloud.brainstormapp.api.repository.Turmas;
import br.com.insideincloud.brainstormapp.api.repository.TurmasAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.TurmasInativasView;
import br.com.insideincloud.brainstormapp.api.repository.Turnos;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoTurmaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.TurmaFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.TurmaDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.TurmaService;

@RestController
@RequestMapping("/turmas")
public class TurmaResource {
	
	@Autowired 
	private Turmas turmas;
	
	@Autowired
	private Ambientes ambientes;
	
	@Autowired
	private Turnos turnos;
	
	@Autowired
	private Materias materias;
	
	@Autowired
	private DiasSemanas dias; 
	
	@Autowired
	private AlunosTurmasView alunos;
	
	@Autowired
	private TurmaService turmaservice;
	
	@Autowired		    
	private AlunosVinculadosTurmasView viculosTurmas;
	
	@Autowired
	private TurmasAtivasView turmasativas;
	
	@Autowired
	private TurmasInativasView turmasinativas;
	
	@GetMapping("/dias")
	@PreAuthorize("hasAuthority('ROLE_DIASEMANA_CONTEUDO')")
//	public List<DiaSemana> diaSemanaConteudo() {
	public RetornoWrapper<DiaSemana> diaSemanaConteudo() {
		RetornoWrapper<DiaSemana> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		dias.findAll()
		try {
			retorno.setConteudo(dias.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os dias da semana, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	} 
	
	@GetMapping("/turnos")
	@PreAuthorize("hasAuthority('ROLE_TURNO_CONTEUDO')")
	public RetornoWrapper<Turno> turnoConteudo() {
		RetornoWrapper<Turno> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		turnos.findAll()
		try {
			retorno.setConteudo(turnos.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os turnos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/salas")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTE_CONTEUDO')")
	public RetornoWrapper<Ambiente> salaConteudo() {
		RetornoWrapper<Ambiente> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		ambientes.findAll()
		try {
			retorno.setConteudo(ambientes.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível restornas as informações de ambientes, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	} 
	
	@GetMapping("/materias")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<Materia> materiaConteudo() {
		RetornoWrapper<Materia> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		materias.findAll()
		try {
			retorno.setConteudo(materias.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações das materias, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TURMA_CONTEUDO')")
	public RetornoWrapper<TurmaAtivaView> conteudo() {
		RetornoWrapper<TurmaAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(turmasativas.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as turmas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;		
	}
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_TURMA_CONTEUDO')")
	public RetornoWrapper<TurmaInativaView> conteudoInativo() {
		RetornoWrapper<TurmaInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( turmasinativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as turmas inativas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_TURMA_CONTEUDO')")
	public RetornoWrapper<Turma> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Turma> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( turmas.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Nao foi possível retornar a turma, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/materias")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<Materia> conteudo(@RequestBody MateriaFilter filtro) {
		RetornoWrapper<Materia> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		materias.filtrar(filtro,page)
		try {
			retorno.setConteudo(materias.findByMateriasNot( Long.parseLong(filtro.getNotmateria_id()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as materias solicitadas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_TURMA_SALVAR')")
	public RetornoWrapper<Turma> salvar(@RequestBody Turma turma, HttpServletResponse response) {
		RetornoWrapper<Turma> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			turma = turmas.saveAndFlush(turma);			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a turma, tente novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			System.out.println(e);
			retorno.setException(exception);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(turma.getTurma_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
//		return ResponseEntity.created(uri).body(turma);
	}
	
	@PutMapping("/alunosview")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<AlunoTurmaView> matricula(@RequestBody AlunoTurmaViewFilter filtro, HttpServletResponse response) {
		RetornoWrapper<AlunoTurmaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		alunos.filtrar(filtro, page)
		try {
			System.out.println(">>>>>>>>>>>>>>>> "+filtro.getVinculo());
			retorno.setConteudo(alunos.findByAlunosDaTurma( Long.parseLong(filtro.getVinculo()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os alunos matriculados, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/alunos")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<MatriculaAlunoView> conteudo(@RequestBody MatriculaAlunoViewFilter filtro) {
		RetornoWrapper<MatriculaAlunoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( turmaservice.findByAlunosMatriculados(Long.parseLong( filtro.getTurma_id() )) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os alunos matriculados, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}	
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_TURMA_DELETAR')")
	public Page<Turma> deletar(@RequestBody TurmaDelete deleta, @PageableDefault(size=8) Pageable page, HttpServletResponse response) {
		TurmaFilter filtro = new TurmaFilter();
		
		for (Turma turma : deleta.getTurmas()) {
			turmas.delete(turma);
		}
		
		return turmas.filtrar(filtro, page);
		
	}

}
