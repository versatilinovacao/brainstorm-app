package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Sede;
import br.com.insideincloud.brainstormapp.api.repository.filter.SedeFilter;
import br.com.insideincloud.brainstormapp.api.repository.sede.SedesQuery;

@Repository
public interface Sedes extends JpaRepository<Sede,Long>,SedesQuery {
	public Page<Sede> filtrar(SedeFilter filtro, Pageable page);
}
