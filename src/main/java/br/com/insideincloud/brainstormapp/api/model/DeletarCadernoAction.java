package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_cadernos",schema="action")
public class DeletarCadernoAction implements Serializable {
	private static final long serialVersionUID = 6071229553441689707L;
	
	@Id
	private Long caderno_id;
	private boolean deletar;

	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public boolean isDeletar() {
		return deletar;
	}
	public void setDeletar(boolean deletar) {
		this.deletar = deletar;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caderno_id == null) ? 0 : caderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeletarCadernoAction other = (DeletarCadernoAction) obj;
		if (caderno_id == null) {
			if (other.caderno_id != null)
				return false;
		} else if (!caderno_id.equals(other.caderno_id))
			return false;
		return true;
	}
	
	
	
}
