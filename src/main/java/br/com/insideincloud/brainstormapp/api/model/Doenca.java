package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.dblink.TipoDoenca;

@Entity
@Table(name="doencas_view",schema="remoto")
public class Doenca implements Serializable {
	private static final long serialVersionUID = -4217136036429029597L;

	@Id
	private Long doenca_id;
	private String nome;
	private TipoDoenca tipodoenca;
	
	public Long getDoenca_id() {
		return doenca_id;
	}
	public void setDoenca_id(Long doenca_id) {
		this.doenca_id = doenca_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public TipoDoenca getTipodoenca() {
		return tipodoenca;
	}
	public void setTipodoenca(TipoDoenca tipodoenca) {
		this.tipodoenca = tipodoenca;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doenca_id == null) ? 0 : doenca_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doenca other = (Doenca) obj;
		if (doenca_id == null) {
			if (other.doenca_id != null)
				return false;
		} else if (!doenca_id.equals(other.doenca_id))
			return false;
		return true;
	}

}
