package br.com.insideincloud.brainstormapp.api.resource;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Artefato;
import br.com.insideincloud.brainstormapp.api.model.ArtigoArtefato;
import br.com.insideincloud.brainstormapp.api.model.ClassificacaoArtefato;
import br.com.insideincloud.brainstormapp.api.model.TipoArtefato;
import br.com.insideincloud.brainstormapp.api.model.minor.ArtefatoMinor;
import br.com.insideincloud.brainstormapp.api.model.view.ArtefatoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.ArtefatoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.Artefatos;
import br.com.insideincloud.brainstormapp.api.repository.ArtefatosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.ArtefatosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.ArtefatosMinor;
import br.com.insideincloud.brainstormapp.api.repository.ArtigosArtefatos;
import br.com.insideincloud.brainstormapp.api.repository.ClassificacoesArtefatos;
import br.com.insideincloud.brainstormapp.api.repository.TiposArtefatos;
import br.com.insideincloud.brainstormapp.api.resource.delete.ArtefatoDeletar;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/artefatos")
public class ArtefatoResource {
	
	@Autowired
	private Artefatos artefatos;
	
	@Autowired
	private ArtigosArtefatos artigos;
	
	@Autowired
	private ClassificacoesArtefatos classificacoes;
	
	@Autowired
	private TiposArtefatos tipos;
	
	@Autowired
	private ArtefatosAtivosView artefatosAtivosView;
	
	@Autowired
	private ArtefatosInativosView artefatosInativosView;
	
	@Autowired
	private ArtefatosMinor artefatosMinor;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<ArtefatoAtivoView> conteudo() {
		RetornoWrapper<ArtefatoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ArtefatoAtivoView> conteudo = artefatosAtivosView.findAll();
			List<ArtefatoAtivoView> conteudoResult = new ArrayList<>();
			
			for (int x = 0; x <= conteudo.size() - 1; x++) {
				if (conteudo.get(x).getComposicao_id() == null) {
					conteudoResult.add(conteudo.get(x));
				}
			}
			
			retorno.setConteudo(conteudoResult);
			
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações relacionadas ao Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos") 
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<ArtefatoInativoView> conteudoInativo() {
		RetornoWrapper<ArtefatoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ArtefatoInativoView> conteudo = artefatosInativosView.findAll();
			
			retorno.setConteudo(conteudo);
			
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações relacionadas ao Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<Artefato> buscaPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Artefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( artefatos.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações do Artefato, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/minor/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<ArtefatoMinor> buscarPeloCodigoMinor(@PathVariable Long codigo) {
		RetornoWrapper<ArtefatoMinor> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( artefatosMinor.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o artefato, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_SALVAR')")
	@Transactional
	public RetornoWrapper<Artefato> salvar(@RequestBody Artefato artefato, HttpServletRequest request) {
		RetornoWrapper<Artefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
//			if (servico.getMunicipio().equals("4303103")) {
//			servicocachoeirinha.setServico_id(servico.getServico_id());
//		} else if (servico.getMunicipio().equals("4314902")) {
//			this.servicoportoalegre.setServico_id(servico.getServico_id());
//		}
//			if (artefato.getServico() != null) {
//				if ("4314902".equals( artefato.getServico().getMunicipio().toString() )) {
//					System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+artefato.getServico().getMunicipio());
//					ServicoPortoAlegre p = new ServicoPortoAlegre(); 
//					p.setServico_id( artefato.getServico().getServico_id() );
//					artefato.setServicoportoalegre( p );
//				}
//				
//			}
			artefatos.save(artefato);
			artefato = artefatos.findOne( artefato.getArtefato_id() );
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+artefato.getServico() == null);
			retorno.setSingle(artefato);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível salvar o arquivo relacionado ao Produto/Serviço");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_DELETAR')")
	@Transactional
	public RetornoWrapper<Artefato> deletar(@RequestBody ArtefatoDeletar artefato) {
		RetornoWrapper<Artefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			artefato.getArtefatos().forEach(conteudo -> {
				artefatos.delete(conteudo);
			});
			
			retorno.setConteudo(artefatos.findAll());
			
		} catch( Exception e ) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível excluir um ou mais de um registro relacionados a Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

	//Artigos
	@GetMapping("/artigos")
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<ArtigoArtefato> conteudoArtigos() {
		RetornoWrapper<ArtigoArtefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ArtigoArtefato> conteudo = artigos.findAll();
			retorno.setConteudo(conteudo);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível carregar as informações referente aos artigos dos Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	//Classificações
	@GetMapping("/classificacoes")
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<ClassificacaoArtefato> conteudoClassificacoes() {
		RetornoWrapper<ClassificacaoArtefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ClassificacaoArtefato> conteudo = classificacoes.findAll();
			retorno.setConteudo(conteudo);
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível carregar as informações referente as classificações dos Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	//Tipos
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_ARTEFATO_CONTEUDO')")
	public RetornoWrapper<TipoArtefato> conteudoTipos() {
		RetornoWrapper<TipoArtefato> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<TipoArtefato> conteudo = tipos.findAll();
			retorno.setConteudo(conteudo);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível carregar as informações referente aos tipos dos Produtos/Serviços");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
}
