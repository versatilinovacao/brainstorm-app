package br.com.insideincloud.brainstormapp.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.UsuarioLoginView;

@Repository
public interface UsuariosLoginView extends JpaRepository<UsuarioLoginView,Long> {
	public Optional<UsuarioLoginView> findByEmail(String email);
	
	@Query("select u from UsuarioLoginView u where u.colaborador_id = ?1 and u.composicao_id = ?2")
	public UsuarioLoginView findByUsuarioProfessor(Long professor_id, Long composicao_professor_id);
	
	@Query("select u from UsuarioLoginView u where u.colaborador_id = ?1 and u.composicao_id = ?2")
	public UsuarioLoginView findByUsuarioAluno_id(Long aluno_id, Long composicao_aluno_id);
	
	
}
