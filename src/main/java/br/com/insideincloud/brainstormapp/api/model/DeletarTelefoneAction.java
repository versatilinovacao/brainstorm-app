package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_telefones",schema="action")
public class DeletarTelefoneAction implements Serializable {
	private static final long serialVersionUID = 5274625536459201210L;

	@Id
	private Long telefone_id;

	public Long getTelefone_id() {
		return telefone_id;
	}

	public void setTelefone_id(Long telefone_id) {
		this.telefone_id = telefone_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telefone_id == null) ? 0 : telefone_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeletarTelefoneAction other = (DeletarTelefoneAction) obj;
		if (telefone_id == null) {
			if (other.telefone_id != null)
				return false;
		} else if (!telefone_id.equals(other.telefone_id))
			return false;
		return true;
	}
	
}
