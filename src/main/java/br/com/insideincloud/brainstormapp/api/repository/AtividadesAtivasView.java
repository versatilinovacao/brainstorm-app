package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AtividadeAtivaView;

@Repository
public interface AtividadesAtivasView extends JpaRepository<AtividadeAtivaView, Long> {
	
	@Query("select a from AtividadeAtivaView a where a.professor_id = ?1")
	public List<AtividadeAtivaView> findByAtividadePorProfessor(Long professor_id);

}
