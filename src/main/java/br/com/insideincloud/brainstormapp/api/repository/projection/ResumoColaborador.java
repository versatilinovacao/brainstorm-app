package br.com.insideincloud.brainstormapp.api.repository.projection;

public class ResumoColaborador {
	private Long colaborador_id;
	private String nome;
	private String fantasia;
		
	public ResumoColaborador(Long colaborador_id, String nome, String fantasia) {
		this.colaborador_id = colaborador_id;
		this.nome = nome;
		this.fantasia = fantasia;
	}
	
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	
	
}
