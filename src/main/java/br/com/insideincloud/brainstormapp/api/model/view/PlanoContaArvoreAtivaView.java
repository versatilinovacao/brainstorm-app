package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="planos_contas_arvores_ativas_view",schema="contabil")
public class PlanoContaArvoreAtivaView implements Serializable {
	private static final long serialVersionUID = 6215878475742228133L;
	
	@Id
	private Long planoconta_id;
	private Long composicao_id;
	private String codigo;
	private String descricao;
	private Boolean status;
	
	public Long getPlanoconta_id() {
		return planoconta_id;
	}
	public void setPlanoconta_id(Long planoconta_id) {
		this.planoconta_id = planoconta_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicao_id == null) ? 0 : composicao_id.hashCode());
		result = prime * result + ((planoconta_id == null) ? 0 : planoconta_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoContaArvoreAtivaView other = (PlanoContaArvoreAtivaView) obj;
		if (composicao_id == null) {
			if (other.composicao_id != null)
				return false;
		} else if (!composicao_id.equals(other.composicao_id))
			return false;
		if (planoconta_id == null) {
			if (other.planoconta_id != null)
				return false;
		} else if (!planoconta_id.equals(other.planoconta_id))
			return false;
		return true;
	}
	
}
