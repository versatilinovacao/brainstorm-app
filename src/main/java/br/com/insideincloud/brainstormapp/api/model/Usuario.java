package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 8896012136304111587L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long usuario_id;
	@Column(name="nome",length=100)
	private String nomecompleto;
	@Column(name="nascimento")
	private LocalDate nascimento;
	@Column(name="senha",length=255)
	private String senha;
	@Transient
	private String confirmacaoSenha;
	@Transient
	private String senhaatual;
	private Boolean status;
	@Column(name="email",length=255)
	private String email;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usuario_permissao",joinColumns = @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name="permissao_id"))
	private List<Permissao> permissoes;
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usuarios_empresas",joinColumns = @JoinColumn(name="usuario_id"), 
										inverseJoinColumns = {@JoinColumn(name="colaborador_id"),
															  @JoinColumn(name="composicao_colaborador_id")})
	private List<Colaborador> empresas;

	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="acessos_usuarios",joinColumns = @JoinColumn(name="tipoacesso_id"), inverseJoinColumns = @JoinColumn(name="usuario_id"))
	private List<TipoAcesso> tiposacessos;
//	@OneToOne
//	@JoinColumn(name="tipo_usuario")
//	private TipoUsuario tipo;

	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="grupos_usuarios",joinColumns = @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name="gruposecurity_id"))
	private List<GrupoSecurity> grupos;
	
	@Column(name="isserver")
	private Boolean isserver;
	@Column(name="isclient")
	private Boolean isclient;
	@Column(name="iscounter")
	private Boolean iscounter;
	@Column(name="issistema")
	private Boolean issistema;
	
	@OneToOne
	@Cascade(CascadeType.ALL)
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador colaborador;
	
	public Boolean getIssistema() {
		return issistema;
	}
	public void setIssistema(Boolean issistema) {
		this.issistema = issistema;
	}
	public String getSenhaatual() {
		return senhaatual;
	}
	public void setSenhaatual(String senhaatual) {
		this.senhaatual = senhaatual;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public Boolean getIsserver() {
		return isserver;
	}
	public void setIsserver(Boolean isserver) {
		this.isserver = isserver;
	}
	public Boolean getIsclient() {
		return isclient;
	}
	public void setIsclient(Boolean isclient) {
		this.isclient = isclient;
	}
	public Boolean getIscounter() {
		return iscounter;
	}
	public void setIscounter(Boolean iscounter) {
		this.iscounter = iscounter;
	}
	public List<GrupoSecurity> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<GrupoSecurity> grupos) {
		this.grupos = grupos;
	}
	public Long getUsuario_id() {
		return usuario_id;
	}
	public List<TipoAcesso> getTiposacessos() {
		return tiposacessos;
	}
	public void setTiposacessos(List<TipoAcesso> tiposacessos) {
		this.tiposacessos = tiposacessos;
	}
	public void setUsuario_id(Long usuario_id) {
		this.usuario_id = usuario_id;
	}
	public String getNomecompleto() {
		return nomecompleto;
	}
	public void setNomecompleto(String nomecompleto) {
		this.nomecompleto = nomecompleto;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}
	public String getSenha() {
		return this.senha;
	}
	public void setSenha(String senha) {
		System.out.println("<<<<<< "+senha+" >>>>>>>");
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		this.senha = encoder.encode(senha);
		System.out.println(">>>>>>>>>>>>>>>>><<<XXXXXXX>>>>>>>>>>>>>>>>>>>"+this.senha);
	}
	public void setSenhaUpdate(String senha) {
		this.senha = senha;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}
	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}
	public List<Colaborador> getEmpresas() {
		return empresas;
	}
	public void setEmpresas(List<Colaborador> empresas) {
		this.empresas = empresas;
	}
//	public TipoUsuario getTipo() {
//		return tipo;
//	}
//	public void setTipo(TipoUsuario tipo) {
//		this.tipo = tipo;
//	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((usuario_id == null) ? 0 : usuario_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (usuario_id == null) {
			if (other.usuario_id != null)
				return false;
		} else if (!usuario_id.equals(other.usuario_id))
			return false;
		return true;
	}
	
	
	
}
