package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="provisor_view")
public class ProvisorView implements Serializable {
	private static final long serialVersionUID = 7134807242404904723L;
	
	@Id
	@JoinColumn(name="provisor_id")
	private Long provisor_id;
	private Long composicao_id;
	private String nome;
	private String fantasia;
	private String cnpj;
	private String inscricaomunicipal;
	private String inscricaoestadual;
	private String cpf;
	private String rg;
	
	public Long getProvisor_id() {
		return provisor_id;
	}
	public void setProvisor_id(Long provisor_id) {
		this.provisor_id = provisor_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public String getInscricaoestadual() {
		return inscricaoestadual;
	}
	public void setInscricaoestadual(String inscricaoestadual) {
		this.inscricaoestadual = inscricaoestadual;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicao_id == null) ? 0 : composicao_id.hashCode());
		result = prime * result + ((provisor_id == null) ? 0 : provisor_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProvisorView other = (ProvisorView) obj;
		if (composicao_id == null) {
			if (other.composicao_id != null)
				return false;
		} else if (!composicao_id.equals(other.composicao_id))
			return false;
		if (provisor_id == null) {
			if (other.provisor_id != null)
				return false;
		} else if (!provisor_id.equals(other.provisor_id))
			return false;
		return true;
	}

}
