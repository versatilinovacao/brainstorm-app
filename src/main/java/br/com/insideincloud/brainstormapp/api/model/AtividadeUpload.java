package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="atividades_upload",schema="escola")
public class AtividadeUpload implements Serializable {
	private static final long serialVersionUID = 9015566279078899842L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long atividadeupload_id;
	@OneToOne
	@JoinColumn(name="atividade_id")
	private Atividade atividade;
	private String nome;
	private String path;
	@OneToOne
	@JoinColumn(name="tipoarquivo_id")
	private TipoArquivo tipoarquivo;
	private Long size;
	private LocalDateTime evento;
	
	public LocalDateTime getEvento() {
		return evento;
	}
	public void setEvento(LocalDateTime evento) {
		this.evento = evento;
	}
	public Long getAtividadeupload_id() {
		return atividadeupload_id;
	}
	public void setAtividadeupload_id(Long atividadeupload_id) {
		this.atividadeupload_id = atividadeupload_id;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public TipoArquivo getTipoarquivo() {
		return tipoarquivo;
	}
	public void setTipoarquivo(TipoArquivo tipoarquivo) {
		this.tipoarquivo = tipoarquivo;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividadeupload_id == null) ? 0 : atividadeupload_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeUpload other = (AtividadeUpload) obj;
		if (atividadeupload_id == null) {
			if (other.atividadeupload_id != null)
				return false;
		} else if (!atividadeupload_id.equals(other.atividadeupload_id))
			return false;
		return true;
	}

}
