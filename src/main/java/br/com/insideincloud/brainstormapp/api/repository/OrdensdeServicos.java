package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.OrdemServico;

@Repository
public interface OrdensdeServicos extends JpaRepository<OrdemServico, Long> {

}
