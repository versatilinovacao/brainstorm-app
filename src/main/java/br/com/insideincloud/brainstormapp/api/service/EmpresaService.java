package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Empresa;
import br.com.insideincloud.brainstormapp.api.repository.Empresas;

@Service
public class EmpresaService {
	@Autowired
	private Empresas empresas;
	
	public boolean existeEmpresa() {
		return false;
	}
	
	public boolean existeMatriz() {
		Empresa empresa = empresas.findOne(1l);
		return (empresa.getUnidade().getDescricao().equals("Matriz"));
	}

}
