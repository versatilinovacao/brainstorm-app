package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.ComunicacaoAlunoProfessor;

@Repository
public interface ComunicacoesAlunosProfessores extends JpaRepository<ComunicacaoAlunoProfessor, Long> {

}
