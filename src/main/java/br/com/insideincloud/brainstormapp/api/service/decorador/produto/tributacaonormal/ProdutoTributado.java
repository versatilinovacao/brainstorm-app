package br.com.insideincloud.brainstormapp.api.service.decorador.produto.tributacaonormal;

import java.math.BigDecimal;

import br.com.insideincloud.brainstormapp.api.service.decorador.produto.ProdutoComponente;
import br.com.insideincloud.brainstormapp.api.service.decorador.produto.ProdutoDecorado;

public class ProdutoTributado extends ProdutoDecorado {

	public ProdutoTributado(ProdutoComponente produto) {
		super(produto);
	}

	@Override
	public String getNome() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescricao() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCFOP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getNCM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTributacao() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getPreco() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getQuantidade() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getFrete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getOutrasDespesas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getFcp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getDesconto() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getPis() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getConfis() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BigDecimal getIpi() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
