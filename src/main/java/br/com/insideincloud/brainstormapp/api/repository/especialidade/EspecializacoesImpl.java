package br.com.insideincloud.brainstormapp.api.repository.especialidade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Especialidade;
import br.com.insideincloud.brainstormapp.api.repository.filter.EspecialidadeFilter;

public class EspecializacoesImpl implements EspecializacoesQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Especialidade> filtrar(EspecialidadeFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Especialidade> criteria = builder.createQuery(Especialidade.class);
		Root<Especialidade> root = criteria.from(Especialidade.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<Especialidade> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(EspecialidadeFilter filtro, CriteriaBuilder builder, Root<Especialidade> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getEspecializacao_id())) {
				predicates.add(builder.equal(root.get("especializacao_id"), Long.parseLong(filtro.getEspecializacao_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}

		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(EspecialidadeFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Especialidade> root = criteria.from(Especialidade.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
