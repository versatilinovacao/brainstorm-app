package br.com.insideincloud.brainstormapp.api.model.dblink;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tiposdoencas_view",schema="remoto")
public class TipoDoenca implements Serializable {
	private static final long serialVersionUID = 8004019828368932032L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipodoenca_id;
	private String nome;
	private String descricao;
	
	public Long getTipodoenca_id() {
		return tipodoenca_id;
	}
	public void setTipodoenca_id(Long tipodoenca_id) {
		this.tipodoenca_id = tipodoenca_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipodoenca_id == null) ? 0 : tipodoenca_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDoenca other = (TipoDoenca) obj;
		if (tipodoenca_id == null) {
			if (other.tipodoenca_id != null)
				return false;
		} else if (!tipodoenca_id.equals(other.tipodoenca_id))
			return false;
		return true;
	}

}
