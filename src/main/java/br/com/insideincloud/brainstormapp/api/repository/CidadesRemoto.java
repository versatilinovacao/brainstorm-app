package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.dblink.Cidade;

@Repository
public interface CidadesRemoto extends JpaRepository<Cidade, Long> {
	
	@Query("select c from Cidade c where c.estado.estado_id = ?1")
	public List<Cidade> findByEstado(Long estado_id);

}
