package br.com.insideincloud.brainstormapp.api.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Arquivo;
import br.com.insideincloud.brainstormapp.api.model.Atividade;
import br.com.insideincloud.brainstormapp.api.model.AvaliacaoAlunoAtividade;
import br.com.insideincloud.brainstormapp.api.model.SituacaoAtividade;
import br.com.insideincloud.brainstormapp.api.model.TipoAtividade;
import br.com.insideincloud.brainstormapp.api.model.view.ArquivoAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.ArquivoPorAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.AtividadeAlunoAvaliacaoView;
import br.com.insideincloud.brainstormapp.api.model.view.AtividadeAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.AtividadeInativaView;
import br.com.insideincloud.brainstormapp.api.repository.Arquivos;
import br.com.insideincloud.brainstormapp.api.repository.ArquivosAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.ArquivosPorAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.Atividades;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesAlunosAvaliacoesView;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesInativasView;
import br.com.insideincloud.brainstormapp.api.repository.AvaliacoesAlunosAtividades;
import br.com.insideincloud.brainstormapp.api.repository.SituacoesAtividades;
import br.com.insideincloud.brainstormapp.api.repository.TiposArquivos;
import br.com.insideincloud.brainstormapp.api.repository.TiposAtividades;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.AtividadeService;

@RestController
@RequestMapping("/atividades")
public class AtividadeResource {
	
	@Autowired
	private Atividades atividades;
	
	@Autowired
	private AtividadesAtivasView atividadesativas;
	
	@Autowired
	private AtividadesInativasView atividadesinativasview;
	
	@Autowired
	private TiposAtividades tiposAtividades;
	
	@Autowired
	private AtividadeService atividadeService;
	
	@Autowired
	private AvaliacoesAlunosAtividades avaliacoes;
	
	@Autowired
	private TiposArquivos tiposarquivos;
	
	@Autowired
	private ArquivosAtividadesView arquivosAtividadesView;
	
	@Autowired
	private AtividadesAlunosAvaliacoesView avaliacoesPorAluno;
	
	@Autowired
	private SituacoesAtividades situacoesatividades;
	
	@Autowired
	private Arquivos arquivos;
	
	@Autowired
	private ArquivosPorAtividadesView arquivosPorAtividadesView;
	
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_SALVAR')")
	public RetornoWrapper<Atividade> salvar(@RequestBody Atividade atividade) {
		RetornoWrapper<Atividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			if (atividade.getArquivos().isEmpty()) {
//				Atividade atividadeAux = atividades.findOne( atividade.getAtividade_id() );
//				if (!atividadeAux.getArquivos().isEmpty()) {
//					atividade.setArquivos( atividadeAux.getArquivos() );
//				}
//			} else {
				//Verificar se o tamanho do arquivo que esta entrando é menor que o que já está gravado.
				//Caso seja, ele deve localizar o arquivo que falta e incluir novamente na lista.
				//para isso precisamos filtrar os arquivos que são diferentes.
//				List<Arquivo> merge = new ArrayList<>();
//				if (atividade.getArquivos().size() < atividades.findOne(atividade.getAtividade_id()).getArquivos().size()) {
//					atividade.getArquivos().forEach(conteudo -> {
//						System.out.println("OLAAAAAAAAAAAAAAAAAAAA "+atividade.getArquivos().size());
//						atividades.findOne(atividade.getAtividade_id()).getArquivos().stream().filter( filtro -> !filtro.equals( conteudo ) ).forEach(agregar -> {
//							System.out.println("<<<<<<<<<<<<<<<<"+agregar.getClass()+">>>>>>>>>>>>>>>>");
//							merge.add(agregar);
//						});
//						System.out.println("OLAAAAAAAAAAAAAAAAAAAA "+atividade.getArquivos().size());
//					});
//					if (merge.size() > 0) { 
//						merge.forEach(conteudo -> {
//							atividade.getArquivos().add(conteudo);
//						}); 
//					} 
//				} else {
//					if (!atividades.findOne(atividade.getAtividade_id()).getArquivos().isEmpty() && atividades.findOne(atividade.getAtividade_id()).getArquivos().size() == 1 ) {
//						atividades.findOne(atividade.getAtividade_id()).getArquivos().forEach(conteudo -> {
//							atividade.getArquivos().add(conteudo);
//						});
//					}
//					
//				}
//			}
			
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+atividade.getAtividade_id());
			if (atividade.getAtividade_id() != null) {
				if ( atividades.findOne( atividade.getAtividade_id() ) != null) {
					atividade.setArquivos( atividades.findOne( atividade.getAtividade_id() ).getArquivos() );				

					if (atividade.getArquivos() != null) {
						atividade.getArquivos().forEach(conteudo -> {
							if (conteudo.getArquivo_id() == null) {
				 				conteudo.setRegistro(LocalDateTime.now());
							} else {
								conteudo.setRegistro(arquivos.findOne(conteudo.getArquivo_id()).getRegistro());
							}
						});
					}
				}
				
				Atividade findAtividade = atividades.findOne(atividade.getAtividade_id());
				atividade.setArquivos(findAtividade.getArquivos());
				
			}
			
			retorno.setSingle( atividadeService.salvarAtividade( atividade ) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a atividade, verifique e entente novamente.");
			exception.setDebug(""+e);
			e.printStackTrace();
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/ativas/{professor_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<AtividadeAtivaView> conteudoAtivo(@PathVariable Long professor_id) {
		System.out.println("ESTOU AQUI ???");
		RetornoWrapper<AtividadeAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			if ( professor_id > 0 )
				retorno.setConteudo( atividadesativas.findByAtividadePorProfessor(professor_id) );
			else
				retorno.setConteudo( atividadesativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar informações relacionadas a atividades, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	} 
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<AtividadeInativaView> conteudoInativo() {
		RetornoWrapper<AtividadeInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( atividadesinativasview.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar atividades inativas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<Atividade> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<Atividade> retorno = new RetornoWrapper<Atividade>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( atividades.findOne(codigo) );
		} catch(Exception e) {
			System.out.println(""+e);
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizada a atividade, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<TipoAtividade> conteudoAtividade() {
		RetornoWrapper<TipoAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposAtividades.findAll() );
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar os tipos das atividades.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping("/avaliacoes")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<AvaliacaoAlunoAtividade> salvarAvaliacao(@RequestBody AvaliacaoAlunoAtividade avaliacao) {
		RetornoWrapper<AvaliacaoAlunoAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			SituacaoAtividade situacao;
			if (avaliacao.getRevisar()) {
				situacao = situacoesatividades.findOne(2L);				
			} else {
				situacao = situacoesatividades.findOne(1L);
			}
			avaliacao.setSituacao(situacao);
			retorno.setSingle( avaliacoes.saveAndFlush(avaliacao) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a avaliação.");
			exception.setDebug(""+e);
			retorno.setException(exception);
			e.printStackTrace();
		}
		
		return retorno;
	}

	@GetMapping("/avaliacoes/{atividade_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<AtividadeAlunoAvaliacaoView> conteudoAvaliacao(@PathVariable Long atividade_id) {
		RetornoWrapper<AtividadeAlunoAvaliacaoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {			
			List<AtividadeAlunoAvaliacaoView> ats = avaliacoesPorAluno.findByAlunos(atividade_id);
			ats.forEach(conteudo -> {
				List<ArquivoPorAtividadeView> arqs = arquivosPorAtividadesView.findByArquivosAlunos(atividade_id, conteudo.getAluno_id(), conteudo.getComposicao_aluno_id());  
				if ( arqs.size() > 0 ) {
					conteudo.setIsanexo(true);
				} else { conteudo.setIsanexo(false); }
			});
						
			retorno.setConteudo( ats );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a avaliação.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping("/upload/{atividade_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	@Transactional
	public RetornoWrapper<Arquivo> upload(@RequestParam MultipartFile file, @PathVariable Long atividade_id) {
		RetornoWrapper<Arquivo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
			
//		Disco disco = new Disco( "/var/local/insideincloud","repositorio" );
	
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(convFile);
			fos.write( file.getBytes() );
			fos.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
//		String nome = convFile.getName().substring(0, convFile.getName().indexOf("."));

		
		//String x = convFile.getName().substring( convFile.getName(). );
		char[] conteudo = convFile.getName().toCharArray();
		int posicaoCount = 0;
		for(int x = convFile.getName().length()-1; x >= 0; x--) {
			if (conteudo[x] == '.') {
				posicaoCount++;
			}
		}
		posicaoCount--;
		String extensao = convFile.getName().substring( convFile.getName().length()-posicaoCount, convFile.getName().length());
		String nome = convFile.getName().substring(0, (convFile.getName().length()-posicaoCount)-1);
		

		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< "+nome+" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ extensao+" - "+posicaoCount);
		
		Arquivo arquivo = new Arquivo();
		arquivo.setNome(nome);
		arquivo.setExtensao(extensao);
		arquivo.setTamanho(file.getSize());
		arquivo.setRegistro(LocalDateTime.now());
		arquivo.setTipo("");
//		arquivo.setTime();
		
//		Base64 codec = new Base64();
		try {
//			String encoded = codec.encodeBase64String( file.getBytes() );
			arquivo.setArquivo( Base64.encodeBase64String( file.getBytes() ) );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		arquivo.setUrl(url);
		
		Atividade atividade = atividades.findOne(atividade_id);
		if (atividade.getArquivos().size() <= 0 ) {
			arquivo.setPrincipal(true);
		}
		atividade.getArquivos().add(arquivo);
		
		
		
		atividade = atividades.saveAndFlush(atividade);
//			disco.salvarArquivo(file);
		retorno.setConteudo( atividade.getArquivos()  );
		
		return retorno;
	}
	
	@GetMapping("/upload/registros/{atividade_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<ArquivoAtividadeView> conteudoRegistros(@PathVariable Long atividade_id) {
		RetornoWrapper<ArquivoAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(arquivosAtividadesView.findByAtividade(atividade_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum upload desta atividade.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/upload/deletar/{arquivo_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_DELETAR')")
	public RetornoWrapper<Arquivo> deletarUpload(@PathVariable Long arquivo_id) {
		RetornoWrapper<Arquivo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			ArquivoAtividadeView arquivoDeletar =  arquivosAtividadesView.findOne(arquivo_id);
			
			List<Arquivo> manter = new ArrayList<Arquivo>();
			List<Arquivo> remover = new ArrayList<Arquivo>();
			Atividade atividade = atividades.findOne(arquivoDeletar.getAtividade_id());
			atividade.getArquivos().forEach(conteudo -> {
			
				if (conteudo.getArquivo_id() != arquivo_id) {
					manter.add(conteudo);
				} else { remover.add(conteudo); }
			});

			atividade.setArquivos(manter);
			atividades.saveAndFlush(atividade);
			
			remover.forEach(conteudo -> {
				arquivos.delete(conteudo);
			});
			
//			Arquivo arquivo = arquivos.findOne(arquivo_id);
//			arquivos.delete( arquivo );
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível excluir o arquivo, favor verificar e tentar novamente dentro de alguns instatantes.");
			exception.setDebug(""+e);
			e.printStackTrace();
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/arquivosalunos/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<ArquivoPorAtividadeView> arquivosPorAtividade(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<ArquivoPorAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(arquivosPorAtividadesView.findByArquivos(atividade_id, aluno_id, composicao_aluno_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi posspivel localizar nenhum arquivo por atividade deste aluno");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
