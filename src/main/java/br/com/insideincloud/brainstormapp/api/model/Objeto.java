package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="objetos",schema="construtor")
public class Objeto implements Serializable {
	private static final long serialVersionUID = 2875494902729766296L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long objeto_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="identificacao",length=40)
	private String identificacao;
	@OneToOne
	@JoinColumn(name="tipo_id")
	private TipoConstrutor tipo;
	
	public Long getObjeto_id() {
		return objeto_id;
	}
	public void setObjeto_id(Long objeto_id) {
		this.objeto_id = objeto_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdentificacao() {
		return identificacao;
	}
	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}
	public TipoConstrutor getTipo() {
		return tipo;
	}
	public void setTipo(TipoConstrutor tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objeto_id == null) ? 0 : objeto_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Objeto other = (Objeto) obj;
		if (objeto_id == null) {
			if (other.objeto_id != null)
				return false;
		} else if (!objeto_id.equals(other.objeto_id))
			return false;
		return true;
	}
	
}
