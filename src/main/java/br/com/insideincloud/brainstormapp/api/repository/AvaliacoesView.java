package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AvaliacaoView;
import br.com.insideincloud.brainstormapp.api.repository.avaliacaoview.AvaliacoesViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.AvaliacaoViewFilter;

@Repository
public interface AvaliacoesView extends JpaRepository<AvaliacaoView,Long>, AvaliacoesViewQuery {
	public Page<AvaliacaoView> filtrar(AvaliacaoViewFilter filtro, Pageable page);
	
	@Query("select a from AvaliacaoView a where a.caderno_id = ?1")
	public List<AvaliacaoView> findByAvaliacoes(Long caderno_id);
	
	@Query("select a from AvaliacaoView a where a.caderno_id = ?1 and a.periodo_letivo_item_id = ?2")
	public List<AvaliacaoView> findByAvalicoesPeriodo(Long caderno_id, Long periodo_item_id);
}
