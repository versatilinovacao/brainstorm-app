package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionTratada;
import br.com.insideincloud.brainstormapp.api.model.MateriaConteudo;
import br.com.insideincloud.brainstormapp.api.repository.MateriasConteudos;
import br.com.insideincloud.brainstormapp.api.retorno.RetornoFachada;

@Service
public class MateriaConteudoService {
	@Autowired
	private MateriasConteudos conteudos;
	
	public MateriaConteudo salvar(MateriaConteudo conteudo) {
		return conteudos.saveAndFlush(conteudo);
	}
}
