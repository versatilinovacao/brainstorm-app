package br.com.insideincloud.brainstormapp.api.repository.gradecurricular;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularFilter;

public class GradesCurricularesImpl implements GradesCurricularesQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<GradeCurricular> filtrar(GradeCurricularFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<GradeCurricular> criteria = builder.createQuery(GradeCurricular.class);
		Root<GradeCurricular> root = criteria.from(GradeCurricular.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<GradeCurricular> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(GradeCurricularFilter filtro, CriteriaBuilder builder, Root<GradeCurricular> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			if (!StringUtils.isEmpty(filtro.getCurso_id())) {
				predicates.add(builder.equal(root.get("curso").get("curso_id"), Long.parseLong(filtro.getCurso_id())));
			}
			
			if (!StringUtils.isEmpty(filtro.getGradecurricular_id())) {
				predicates.add(builder.equal(root.get("gradecurricular_id"), Long.parseLong(filtro.getGradecurricular_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador").get("colaborador_id"), Long.parseLong(filtro.getColaborador_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getMateria_id())) {
				predicates.add(builder.equal(root.get("materia").get("materia_id"), Long.parseLong(filtro.getMateria_id())));
			}		
			
//			if (!StringUtils.isEmpty(filtro.getDescricao())) {
//				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
//			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(GradeCurricularFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<GradeCurricular> root = criteria.from(GradeCurricular.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
