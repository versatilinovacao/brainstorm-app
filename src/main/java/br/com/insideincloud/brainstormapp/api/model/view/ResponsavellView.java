package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="responsaveis_view")
public class ResponsavellView implements Serializable {
	private static final long serialVersionUID = 7149086256746541960L;

	@Id
	private Long responsavel_id;
	private Long colaborador_id;
	private Long composicao_colaborador_id;
	private String nome;
	private String telefone;
	private Long tiporesponsavel_id;
	private String descricaotiporesponsavel;
	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getTiporesponsavel_id() {
		return tiporesponsavel_id;
	}
	public void setTiporesponsavel_id(Long tiporesponsavel_id) {
		this.tiporesponsavel_id = tiporesponsavel_id;
	}
	public String getDescricaotiporesponsavel() {
		return descricaotiporesponsavel;
	}
	public void setDescricaotiporesponsavel(String descricaotiporesponsavel) {
		this.descricaotiporesponsavel = descricaotiporesponsavel;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public Long getResponsavel_id() {
		return responsavel_id;
	}
	public void setResponsavel_id(Long responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
