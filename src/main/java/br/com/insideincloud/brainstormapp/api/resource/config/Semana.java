package br.com.insideincloud.brainstormapp.api.resource.config;

import java.time.LocalDate;

public class Semana {

	private Integer id;
	private LocalDate first;
	private LocalDate last;
	
	public Semana( Integer id, LocalDate first, LocalDate last ) {
		this.id = id;
		this.first = first;
		this.last = last;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public LocalDate getFirst() {
		return first;
	}
	public void setFirst(LocalDate first) {
		this.first = first;
	}
	public LocalDate getLast() {
		return last;
	}
	public void setLast(LocalDate last) {
		this.last = last;
	}
	
}
