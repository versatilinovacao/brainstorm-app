package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Sexo;
import br.com.insideincloud.brainstormapp.api.repository.Sexos;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/sexos")
public class SexoResource {

	@Autowired
	private Sexos sexos;
	
	@GetMapping
	public RetornoWrapper<Sexo> conteudo() {
		RetornoWrapper<Sexo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception =new ExceptionWrapper();
		
		try {
			retorno.setConteudo(sexos.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}

