package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="planos_contas",schema="contabil")
public class PlanoContaMinor implements Serializable {
	private static final long serialVersionUID = -6658365271362345199L;

	@Id
	private Long planoconta_id;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="composicao_id",insertable = true, updatable = true)
	private PlanoContaMinor composicao;
	
	
	@JsonManagedReference
	@OneToMany(mappedBy="composicao")
	private List<PlanoContaMinor> contas;
	
	private Integer codigo;
	private String descricao;
	
	public Long getPlanoconta_id() {
		return planoconta_id;
	}
	public void setPlanoconta_id(Long planoconta_id) {
		this.planoconta_id = planoconta_id;
	}
	public PlanoContaMinor getComposicao() {
		return composicao;
	}
	public void setComposicao(PlanoContaMinor composicao) {
		this.composicao = composicao;
	}
	public List<PlanoContaMinor> getContas() {
		return contas;
	}
	public void setContas(List<PlanoContaMinor> contas) {
		this.contas = contas;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planoconta_id == null) ? 0 : planoconta_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoContaMinor other = (PlanoContaMinor) obj;
		if (planoconta_id == null) {
			if (other.planoconta_id != null)
				return false;
		} else if (!planoconta_id.equals(other.planoconta_id))
			return false;
		return true;
	}

}
