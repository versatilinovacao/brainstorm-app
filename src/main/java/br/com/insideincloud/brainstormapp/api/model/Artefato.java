package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.insideincloud.brainstormapp.api.model.minor.PlanoContaMinor;

@Entity
@Table(name="artefatos",schema="almoxarifado")
public class Artefato implements Serializable {
	private static final long serialVersionUID = -8763656428720918894L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long artefato_id;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="composicao_id",insertable = true, updatable = false)
	private Artefato composicao;
	
	@JsonManagedReference
	@OneToMany(mappedBy = "composicao")
	private List<Artefato> artefatos;
	
	@Column(name="nome",length=60)
	private String nome;
	@Column(name="descricao",length=200)
	private String descricao;
	@OneToOne
	@JoinColumn(name="classificacao_id")
	private ClassificacaoArtefato classificacao;
	@OneToOne
	@JoinColumn(name="tipo_id")
	private TipoArtefato tipo;
	@OneToOne
	@JoinColumn(name="artigo_id")
	private ArtigoArtefato artigo;
	@Column(name="preco")
	private double valor;
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador colaborador;
	private Boolean status;
	
	@OneToOne
	@JoinColumn(name="servico_id")
	private ServicoMunicipio servico;
	
	@OneToOne
	@JoinColumn(name="planoconta_id")
	private PlanoContaMinor planoconta;
		
	public PlanoContaMinor getPlanoconta() {
		return planoconta;
	}
	public void setPlanoconta(PlanoContaMinor planoconta) {
		this.planoconta = planoconta;
	}
	public Artefato getComposicao() {
		return composicao;
	}
	public void setComposicao(Artefato composicao) {
		this.composicao = composicao;
	}
	public List<Artefato> getArtefatos() {
		return artefatos;
	}
	public void setArtefatos(List<Artefato> artefatos) {
		this.artefatos = artefatos;
	}
	public ServicoMunicipio getServico() {
		return servico;
	}
	public void setServico(ServicoMunicipio servico) {		
		this.servico = servico;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public Long getArtefato_id() {
		return artefato_id;
	}
	public void setArtefato_id(Long artefato_id) {
		this.artefato_id = artefato_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public ClassificacaoArtefato getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(ClassificacaoArtefato classificacao) {
		this.classificacao = classificacao;
	}
	public TipoArtefato getTipo() {
		return tipo;
	}
	public void setTipo(TipoArtefato tipo) {
		this.tipo = tipo;
	}
	public ArtigoArtefato getArtigo() {
		return artigo;
	}
	public void setArtigo(ArtigoArtefato artigo) {
		this.artigo = artigo;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artefato_id == null) ? 0 : artefato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artefato other = (Artefato) obj;
		if (artefato_id == null) {
			if (other.artefato_id != null)
				return false;
		} else if (!artefato_id.equals(other.artefato_id))
			return false;
		return true;
	}
	
}
