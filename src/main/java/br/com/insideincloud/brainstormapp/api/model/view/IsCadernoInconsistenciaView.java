package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="is_cadernos_inconsistencias_view", schema="escola")
public class IsCadernoInconsistenciaView implements Serializable {
	private static final long serialVersionUID = -3893907726895933596L;

	@Id
	private Long caderno_id;
	@Column(name="iserro")
	private boolean iserro;
	
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public boolean isIserro() {
		return iserro;
	}
	public void setIserro(boolean iserro) {
		this.iserro = iserro;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caderno_id == null) ? 0 : caderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IsCadernoInconsistenciaView other = (IsCadernoInconsistenciaView) obj;
		if (caderno_id == null) {
			if (other.caderno_id != null)
				return false;
		} else if (!caderno_id.equals(other.caderno_id))
			return false;
		return true;
	}
	
}
