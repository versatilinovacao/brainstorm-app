package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorConsumidor;

@Entity
@Table(name="requisicoes",schema="comercial")
public class Requisicao implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long requisicao_id;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="composicao_id")
	private Requisicao composicao;
	@JsonManagedReference
	@OneToMany(mappedBy="composicao")
	private List<Requisicao> requisicoes;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="empresa_id"),@JoinColumn(name="composicao_empresa_id")})
	private ColaboradorConsumidor empresa;
	@OneToOne
	@JoinColumns({@JoinColumn(name="cliente_id"),@JoinColumn(name="composicao_cliente_id")})
	private ColaboradorConsumidor consumidor; 
	@OneToOne
	@JoinColumns({@JoinColumn(name="fornecedor_id"),@JoinColumn(name="composicao_fornecedor_id")})
	private ColaboradorConsumidor fornecedor;
	
//	@OneToOne
//	@JoinColumn(name="tiporequisicao_id")
//	private TipoRequisicao tipo;
	
	@OneToOne
	@JoinColumn(name="parcelamento_id")
	private Parcelamento parcelamento;	
	@OneToOne
	@JoinColumn(name="objetocontratado_id")
	private Artefato objetocontrato;
	
	private LocalDateTime assinatura;
	private Boolean renovacaoautomatica;
	private Integer pagamentodia;
	@Column(name="validade_mes")
	private Integer validademes;
	
	private Double multa;
	private Double mora;
//	private IndiceReajuste indice;
	
	private Double basecalculoipi;
	private Double aliquotaipi;
	private Double valoripi;
	
	private Double basecalculoicms;
	private Double aliquotaicms;
	private Double valoricms;
	
	private Double basecalculoicmsst;
	private Double aliquotaicmsst;
	private Double valoricmsst;
	
	private Double basecalculopis;
	private Double aliquotapis;
	private Double valorpis;
	
	private Double basecalculocofins;
	private Double aliquotacofins;
	private Double valorcofins;
	
	private Double descontopercentual;
	private Double valordesconto;
	
	private Double total;
	
}
