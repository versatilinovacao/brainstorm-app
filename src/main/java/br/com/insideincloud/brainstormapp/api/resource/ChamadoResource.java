package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.Chamado;
import br.com.insideincloud.brainstormapp.api.model.DeletarChamadoAction;
import br.com.insideincloud.brainstormapp.api.repository.Chamados;
import br.com.insideincloud.brainstormapp.api.repository.DeletarChamadosAction;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadoFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.ChamadoDelete;

@RestController
@RequestMapping("/chamados")
public class ChamadoResource {
	
	@Autowired
	private Chamados chamados;
	
	@Autowired
	private DeletarChamadosAction deletarChamados;

	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADO_CONTEUDO')")//,sort= {"prioridade","abertura","titulo"}
	public Page<Chamado> conteudo(@RequestBody ChamadoFilter filtro,@PageableDefault(size=10,sort= {"prioridade","abertura","titulo","chamado_id"}) Pageable page) {
		
		return chamados.filtrar(filtro, page);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADO_SALVAR')")
	public ResponseEntity<Chamado> salvar(@RequestBody Chamado chamado, HttpServletResponse response) {
		chamado = chamados.saveAndFlush(chamado);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(chamado.getChamado_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(chamado);
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADO_DELETAR')")
	public Page<Chamado> deletar(@RequestBody ChamadoDelete chamadoDelete, @PageableDefault(size=10) Pageable page) {
		
		for (Chamado conteudo : chamadoDelete.getChamados()) {
			DeletarChamadoAction deletarChamado = new DeletarChamadoAction();
			deletarChamado.setChamado_id(conteudo.getChamado_id());
						
			deletarChamados.save(deletarChamado);
		}
		
		return chamados.findAll(page);
	}
	
	
}
