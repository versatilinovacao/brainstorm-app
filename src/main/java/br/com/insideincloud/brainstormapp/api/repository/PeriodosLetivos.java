package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivo;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoFilter;
import br.com.insideincloud.brainstormapp.api.repository.periodoletivo.PeriodosLetivosQuery;

@Repository
public interface PeriodosLetivos extends JpaRepository<PeriodoLetivo,Long>, PeriodosLetivosQuery {
	public Page<PeriodoLetivo> filtrar(PeriodoLetivoFilter filtro, Pageable page);
	
	@Query("select p from PeriodoLetivo p where p.empresa.empresa_id = ?1")
	public List<PeriodoLetivo> findByPeriodosLetivos(Long empresa_id);
	
}
