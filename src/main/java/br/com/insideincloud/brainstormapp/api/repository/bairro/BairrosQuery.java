package br.com.insideincloud.brainstormapp.api.repository.bairro;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Bairro;
import br.com.insideincloud.brainstormapp.api.repository.filter.BairroFilter;

public interface BairrosQuery {
	public Page<Bairro> filtrar(BairroFilter filtro, Pageable page);
}
