package br.com.insideincloud.brainstormapp.api.repository.avaliacaoview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.AvaliacaoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AvaliacaoViewFilter;

public interface AvaliacoesViewQuery {
	public Page<AvaliacaoView> filtrar(AvaliacaoViewFilter filtro, Pageable page);

}
