package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="responsavel_view")
public class ResponsavelView implements Serializable {
	private static final long serialVersionUID = -2378678583086447332L;

	@Id
	@JoinColumn(name="responsavel_id")
	private Long responsavel_id;
	@JoinColumn(name="composicao_colaborador_id")
	private Long composicao_colaborador_id;
	@Column(name="nome")
	private String nome;
	@Column(name="telefone")
	private String telefone;
	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public Long getResponsavel_id() {
		return responsavel_id;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public void setResponsavel_id(Long responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((responsavel_id == null) ? 0 : responsavel_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsavelView other = (ResponsavelView) obj;
		if (responsavel_id == null) {
			if (other.responsavel_id != null)
				return false;
		} else if (!responsavel_id.equals(other.responsavel_id))
			return false;
		return true;
	}
	
}
