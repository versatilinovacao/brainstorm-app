package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.OrdemServico;
import br.com.insideincloud.brainstormapp.api.repository.OrdensdeServicos;
import br.com.insideincloud.brainstormapp.api.resource.delete.OrdemServicoDeleta;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.OrdemServicoService;
import br.com.insideincloud.brainstormapp.api.service.transport.LoteEnviarRps;

@RestController
@RequestMapping("/ordensservicos")
public class OrdemServicoResource {
	
	@Autowired
	private OrdensdeServicos ordensServicos; 
	
	@Autowired
	private OrdemServicoService ordemService;
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ORDEMSERVICO_CONTEUDO')")
	public OrdemServico buscaPeloCodigo(@PathVariable Long codigo) {
		
		return ordensServicos.findOne(codigo);
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_ORDEMSERVICO_SALVAR')")
	public RetornoWrapper<OrdemServico> conteudo() {
		RetornoWrapper<OrdemServico> retorno = new RetornoWrapper<OrdemServico>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<OrdemServico> resultado = new ArrayList<>();
			resultado = ordensServicos.findAll();
			retorno.setConteudo(resultado);
		} catch (java.lang.Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar informações relacionado as Ordens de Serviço");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PostMapping
	@Transactional
	@PreAuthorize("hasAuthority('ROLE_ORDEMSERVICO_SALVAR')")
	public RetornoWrapper<OrdemServico> salvar(@RequestBody OrdemServico ordemServico, HttpServletResponse response) {
		RetornoWrapper<OrdemServico> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			ordemServico = ordensServicos.saveAndFlush(ordemServico);
			retorno.setSingle(ordemServico);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível salvar o salvar a Ordem de Serviço");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(ordemServico.getOrdemservico_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());

		return retorno;
	}
	
	@PutMapping
	@Transactional
	@PreAuthorize("hasAuthority('ROLE_ORDEMSERVICO_DELETAR')")
	public RetornoWrapper<OrdemServico> deletarRps(@RequestBody OrdemServicoDeleta ordemdeleta) {
		RetornoWrapper<OrdemServico> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			ordemdeleta.getOrdens().forEach( conteudo -> {
				ordensServicos.delete(conteudo);
			});
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Erro ao deletar um dos arquivo de Ordem de Serviço.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PutMapping("/gerarrps")
	@PreAuthorize("hasAuthority('ROLE_ORDEMSERVICO_SALVAR')")
	public RetornoWrapper<?> gerarRps(@RequestBody LoteEnviarRps lote) {
		RetornoWrapper<?> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			lote.getOrdens().forEach(conteudo -> {
				
				try {
					ordemService.gerarRps(conteudo);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Ocorreu um erro no momento da geração do RPS.");
			erro.setDebug(""+e);
			retorno.setException(erro);
			
		}
		
		return retorno;
	}
	
	@PutMapping("/consultarrps")
	@PreAuthorize("hasAutority('ROLE_CONSULTA_RPS')")
	public RetornoWrapper<?> consultaRps() {
		return null;
	}
	
	@PutMapping("/cancelarps/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CANCELA_RPS')")
	public RetornoWrapper<?> cancelarRps() {
		
		return null;
	}
}
