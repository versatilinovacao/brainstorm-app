package br.com.insideincloud.brainstormapp.api.repository.materiacompostaview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;

public class MateriasCompostaViewImpl implements MateriasCompostaViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<MateriaCompostaView> filtrar(MateriaCompostaViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<MateriaCompostaView> criteria = builder.createQuery(MateriaCompostaView.class);
		Root<MateriaCompostaView> root = criteria.from(MateriaCompostaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<MateriaCompostaView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(MateriaCompostaViewFilter filtro, CriteriaBuilder builder, Root<MateriaCompostaView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			System.out.println("XXXXXXXXXXXXXXXXx "+filtro.getAowner()); 
			
			if (!StringUtils.isEmpty(filtro.getMateria_id())) {
				predicates.add(builder.equal(root.get("materia_id"), Long.parseLong(filtro.getMateria_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(filtro.getAowner())) {
				predicates.add(builder.equal(root.get("aowner"), Long.parseLong(filtro.getAowner())));
			}		
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(MateriaCompostaViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<MateriaCompostaView> root = criteria.from(MateriaCompostaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
