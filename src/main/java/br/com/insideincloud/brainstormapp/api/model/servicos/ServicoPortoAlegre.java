package br.com.insideincloud.brainstormapp.api.model.servicos;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="servicos",schema="servico")
public class ServicoPortoAlegre implements Serializable {
	private static final long serialVersionUID = 8073938823828639768L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long servico_id;
	private Double referencia;
	private String descricao;
	private Double aliquota;
	private Long municipio;
	private LocalDate inicio;
	private LocalDate fim;
	@Column(name="codigo_procempa")
	private String codigo;
	private Boolean status;
	
	public Long getServico_id() {
		return servico_id;
	}
	public void setServico_id(Long servico_id) {
		this.servico_id = servico_id;
	}
	public Double getReferencia() {
		return referencia;
	}
	public void setReferencia(Double referencia) {
		this.referencia = referencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAliquota() {
		return aliquota;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public Long getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Long municipio) {
		this.municipio = municipio;
	}
	public LocalDate getInicio() {
		return inicio;
	}
	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}
	public LocalDate getFim() {
		return fim;
	}
	public void setFim(LocalDate fim) {
		this.fim = fim;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((servico_id == null) ? 0 : servico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicoPortoAlegre other = (ServicoPortoAlegre) obj;
		if (servico_id == null) {
			if (other.servico_id != null)
				return false;
		} else if (!servico_id.equals(other.servico_id))
			return false;
		return true;
	}

}
