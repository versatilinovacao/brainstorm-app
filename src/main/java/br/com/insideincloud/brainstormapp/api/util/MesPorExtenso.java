package br.com.insideincloud.brainstormapp.api.util;

public class MesPorExtenso {
	
	private int mes;
	public MesPorExtenso(int mes) {
		this.mes = mes;		
	}
	
	public String descricao() {
		if (mes == 1) {
			return "Janeiro".toUpperCase();
		} else if (mes == 2) {
			return "Fevereiro".toUpperCase();
		} else if (mes == 3) {
			return "Março".toUpperCase();
		} else if (mes == 4) {
			return "Abril".toUpperCase();
		} else if (mes == 5) {
			return "Maio".toUpperCase();
		} else if (mes == 6) {
			return "Junho".toUpperCase();					
		} else if (mes == 7) {
			return "Julho".toUpperCase();
		} else if (mes == 8) {
			return "Agosto".toUpperCase();
		} else if (mes == 9) {
			return "Setembro".toUpperCase();
		} else if (mes == 10) {
			return "Outubro".toUpperCase();
		} else if (mes == 11) {
			return "Novembro".toUpperCase();
		} else if (mes == 12) {
			return "Dezembro".toUpperCase();
		}		
		
		return "";
	}

}
