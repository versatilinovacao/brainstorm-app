package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TipoMedico;

@Repository
public interface TiposMedicos extends JpaRepository<TipoMedico, Long> {

}
