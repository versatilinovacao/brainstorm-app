package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cidades")
public class CidadeMinor implements Serializable {
	private static final long serialVersionUID = 2254524354154831474L;
	
	@Id
	private Long cidade_id;
	@Column(name="descricao",length=150)
	private String descricao;
	
	public Long getCidade_id() {
		return cidade_id;
	}
	public void setCidade_id(Long cidade_id) {
		this.cidade_id = cidade_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cidade_id == null) ? 0 : cidade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CidadeMinor other = (CidadeMinor) obj;
		if (cidade_id == null) {
			if (other.cidade_id != null)
				return false;
		} else if (!cidade_id.equals(other.cidade_id))
			return false;
		return true;
	}
	
	
}
