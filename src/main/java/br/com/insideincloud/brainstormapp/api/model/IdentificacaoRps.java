package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.jdom.Element;

@Entity
@DiscriminatorValue("IDENTIFICACAORPS")
public class IdentificacaoRps extends Rps implements Serializable {
	private static final long serialVersionUID = -4176703348968259011L;

	private String numero;
	private String serie;
	private String tipo;
	
	private Element numeroNode = new Element("Numero");
	private Element serieNode = new Element("Serie");
	private Element tipoNode = new Element("Tipo");
	private Element identificacaoRpsNode = new Element("IdentificacaoRps");
	
	public Element nodeXML() { return identificacaoRpsNode; }
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		identificacaoRpsNode.addContent(numeroNode);
		numeroNode.setText(numero);
		this.numero = numero;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		identificacaoRpsNode.addContent(serieNode);
		serieNode.setText(serie);
		this.serie = serie;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		identificacaoRpsNode.addContent(tipoNode);
		tipoNode.setText(tipo);
		this.tipo = tipo;
	}	

}
