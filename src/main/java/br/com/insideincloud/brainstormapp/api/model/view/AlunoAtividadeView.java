package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//alunos_atividades_view
@Entity
@Table(name="atividades_alunos_view",schema="escola")
public class AlunoAtividadeView implements Serializable {
	private static final long serialVersionUID = -5308763704112337383L;
	
	@Id
	private Long alunoatividade_id;
	private Long atividade_id;
	private Long aluno_id;
	private Long composicao_aluno_id;
	private Long gradecurricular_id;
	@Column(name="data_atividade")
	private LocalDate dataatividade;
	private LocalDate entrega;
	private String titulo;
	private String descricao;
	private String avaliacao;
	private Integer nota;
	private String conceito;
	private String descricaotipoatividade;
	private String descricaoturma;
	private String orientacoes;
	private Long materia_id;
	private String descricaomateria;
	private Long professor_id;
	private Long composicao_professor_id;
	private String nomeprofessor;
	private Long situacaoatividade_id;
	private String nomesituacaoatividade;
	private String transcricao;
	private Boolean revisar;
	
	public String getTranscricao() {
		return transcricao;
	}
	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
	private Boolean concluido;
	private Boolean status;
	
	public Boolean getRevisar() {
		return revisar;
	}
	public void setRevisar(Boolean revisar) {
		this.revisar = revisar;
	}
	public Long getSituacaoatividade_id() {
		return situacaoatividade_id;
	}
	public void setSituacaoatividade_id(Long situacaoatividade_id) {
		this.situacaoatividade_id = situacaoatividade_id;
	}
	public String getNomesituacaoatividade() {
		return nomesituacaoatividade;
	}
	public void setNomesituacaoatividade(String nomesituacaoatividade) {
		this.nomesituacaoatividade = nomesituacaoatividade;
	}
	public Long getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(Long professor_id) {
		this.professor_id = professor_id;
	}
	public Long getComposicao_professor_id() {
		return composicao_professor_id;
	}
	public void setComposicao_professor_id(Long composicao_professor_id) {
		this.composicao_professor_id = composicao_professor_id;
	}
	public String getNomeprofessor() {
		return nomeprofessor;
	}
	public void setNomeprofessor(String nomeprofessor) {
		this.nomeprofessor = nomeprofessor;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricaomateria() {
		return descricaomateria;
	}
	public void setDescricaomateria(String descricaomateria) {
		this.descricaomateria = descricaomateria;
	}
	public String getDescricaotipoatividade() {
		return descricaotipoatividade;
	}
	public void setDescricaotipoatividade(String descricaotipoatividade) {
		this.descricaotipoatividade = descricaotipoatividade;
	}
	public String getDescricaoturma() {
		return descricaoturma;
	}
	public void setDescricaoturma(String descricaoturma) {
		this.descricaoturma = descricaoturma;
	}
	public String getOrientacoes() {
		return orientacoes;
	}
	public void setOrientacoes(String orientacoes) {
		this.orientacoes = orientacoes;
	}
	public Long getAlunoatividade_id() {
		return alunoatividade_id;
	}
	public void setAlunoatividade_id(Long alunoatividade_id) {
		this.alunoatividade_id = alunoatividade_id;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public LocalDate getDataatividade() {
		return dataatividade;
	}
	public void setDataatividade(LocalDate dataatividade) {
		this.dataatividade = dataatividade;
	}
	public LocalDate getEntrega() {
		return entrega;
	}
	public void setEntrega(LocalDate entrega) {
		this.entrega = entrega;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public Boolean getConcluido() {
		return concluido;
	}
	public void setConcluido(Boolean concluido) {
		this.concluido = concluido;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade_id == null) ? 0 : atividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoAtividadeView other = (AlunoAtividadeView) obj;
		if (atividade_id == null) {
			if (other.atividade_id != null)
				return false;
		} else if (!atividade_id.equals(other.atividade_id))
			return false;
		return true;
	}
	
}
