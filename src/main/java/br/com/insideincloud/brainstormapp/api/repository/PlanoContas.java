package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PlanoConta;

@Repository
public interface PlanoContas extends JpaRepository<PlanoConta, Long> {
	
//	@Query("select p from PlanoConta p where p.planoconta_id.planoconta_id = ?1 and p.planoconta_id.composicao_id = ?2")
//	public PlanoConta findById(Long planoconta_id, Long composicao_id);
//	
//	@Query("select p from PlanoConta p where p.planoconta_id.composicao_id = ?1")
//	public List<PlanoConta> findByComposicao(Long composicao_id);

}
