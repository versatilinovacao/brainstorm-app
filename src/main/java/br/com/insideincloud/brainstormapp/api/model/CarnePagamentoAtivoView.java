package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="carnes_ativos_view",schema="financeiro")
public class CarnePagamentoAtivoView implements Serializable {
	private static final long serialVersionUID = -2310719907506065471L;
	
	@Id
	private Long carne_id;
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao;
	private Integer parcelas;
	private Double valor;
	@Column(name="valor_quitacao")
	private Double valorquitacao;
	private Long empresa_id;
	private Long composicao_empresa_id;
	private String nomeempresa;
	private Long responsavel_id;
	private Long composicao_responsavel_id;
	private String nomeresponsavel;
	private Long artefato_id;
	private String nomeartefato;
	private Long planoconta_id;
	private String descricaoplanoconta;
	private Boolean status;
	
	public Long getCarne_id() {
		return carne_id;
	}
	public void setCarne_id(Long carne_id) {
		this.carne_id = carne_id;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public Integer getParcelas() {
		return parcelas;
	}
	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getValorquitacao() {
		return valorquitacao;
	}
	public void setValorquitacao(Double valorquitacao) {
		this.valorquitacao = valorquitacao;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Long getComposicao_empresa_id() {
		return composicao_empresa_id;
	}
	public void setComposicao_empresa_id(Long composicao_empresa_id) {
		this.composicao_empresa_id = composicao_empresa_id;
	}
	public String getNomeempresa() {
		return nomeempresa;
	}
	public void setNomeempresa(String nomeempresa) {
		this.nomeempresa = nomeempresa;
	}
	public Long getResponsavel_id() {
		return responsavel_id;
	}
	public void setResponsavel_id(Long responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public Long getComposicao_responsavel_id() {
		return composicao_responsavel_id;
	}
	public void setComposicao_responsavel_id(Long composicao_responsavel_id) {
		this.composicao_responsavel_id = composicao_responsavel_id;
	}
	public String getNomeresponsavel() {
		return nomeresponsavel;
	}
	public void setNomeresponsavel(String nomeresponsavel) {
		this.nomeresponsavel = nomeresponsavel;
	}
	public Long getArtefato_id() {
		return artefato_id;
	}
	public void setArtefato_id(Long artefato_id) {
		this.artefato_id = artefato_id;
	}
	public String getNomeartefato() {
		return nomeartefato;
	}
	public void setNomeartefato(String nomeartefato) {
		this.nomeartefato = nomeartefato;
	}
	public Long getPlanoconta_id() {
		return planoconta_id;
	}
	public void setPlanoconta_id(Long planoconta_id) {
		this.planoconta_id = planoconta_id;
	}
	public String getDescricaoplanoconta() {
		return descricaoplanoconta;
	}
	public void setDescricaoplanoconta(String descricaoplanoconta) {
		this.descricaoplanoconta = descricaoplanoconta;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carne_id == null) ? 0 : carne_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarnePagamentoAtivoView other = (CarnePagamentoAtivoView) obj;
		if (carne_id == null) {
			if (other.carne_id != null)
				return false;
		} else if (!carne_id.equals(other.carne_id))
			return false;
		return true;
	}
	
}
