package br.com.insideincloud.brainstormapp.api.repository.filter;

public class PeriodoLetivoCadernoViewFilter {
	private String periodo_letivo_item_id;
	private String periodo_letivo_nome;
	private String caderno_id;
	
	public String getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(String periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public String getPeriodo_letivo_nome() {
		return periodo_letivo_nome;
	}
	public void setPeriodo_letivo_nome(String periodo_letivo_nome) {
		this.periodo_letivo_nome = periodo_letivo_nome;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}

}
