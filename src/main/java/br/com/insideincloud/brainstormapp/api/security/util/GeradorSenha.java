package br.com.insideincloud.brainstormapp.api.security.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.insideincloud.brainstormapp.api.util.WordDocument;

public class GeradorSenha {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//		System.out.print(encoder.encode("ckldfvq88536")); //$2a$10$jvvqI58e2naN149yt7iPZOoUlvv4uwnnDclx1Se3TQwEuYhxvS26e
//		System.out.println(encoder.encode("visitante")); //$2a$10$5fwcPgpl3dUY4uz7apF93O7Ni9KCEEOSpH/xnu.2DUvA/8ghny8ku
//		System.out.println(encoder.encode("prim@v3r@")); //$2a$10$5fwcPgpl3dUY4uz7apF93O7Ni9KCEEOSpH/xnu.2DUvA/8ghny8ku
		System.out.println(encoder.encode("123456789")); //$2a$10$5fwcPgpl3dUY4uz7apF93O7Ni9KCEEOSpH/xnu.2DUvA/8ghny8ku

		
//		System.out.print(encoder.encode("adminadmin"));
//		System.out.print(encoder.encode("s!mone-pros@ber"));
		
		WordDocument d = new WordDocument();
		d.CriarArquivo();
		
	}
	
}


//30645022