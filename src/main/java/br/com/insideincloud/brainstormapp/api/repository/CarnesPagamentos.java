package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.CarnePagamento;

@Repository
public interface CarnesPagamentos extends JpaRepository<CarnePagamento, Long> {

}
