package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="cursos")
public class Curso implements Serializable {
	private static final long serialVersionUID = -5899739322720617121L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long curso_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="escola_id"),@JoinColumn(name="composicao_escola_id")})
	private Colaborador escola;
	
	@Fetch(FetchMode.SELECT)
	@OneToOne
	@JoinColumn(name="periodo_letivo_id")
	private PeriodoLetivo periodoletivo;
	private boolean status;
	
	@Transient
	private LocalDate competencia_inicial;
	@Transient
	private LocalDate competencia_final;
	
	public Colaborador getEscola() {
		return escola;
	}
	public void setEscola(Colaborador escola) {
		this.escola = escola;
	}
	public LocalDate getCompetencia_inicial() {
		return competencia_inicial;
	}
	public void setCompetencia_inicial(LocalDate competencia_inicial) {
		this.competencia_inicial = competencia_inicial;
	}
	public LocalDate getCompetencia_final() {
		return competencia_final;
	}
	public void setCompetencia_final(LocalDate competencia_final) {
		this.competencia_final = competencia_final;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public PeriodoLetivo getPeriodoletivo() {
		return periodoletivo;
	}
	public void setPeriodoletivo(PeriodoLetivo periodoletivo) {
		this.periodoletivo = periodoletivo;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((curso_id == null) ? 0 : curso_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		if (curso_id == null) {
			if (other.curso_id != null)
				return false;
		} else if (!curso_id.equals(other.curso_id))
			return false;
		return true;
	}	
	
}
