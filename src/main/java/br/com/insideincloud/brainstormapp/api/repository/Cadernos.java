package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Caderno;
import br.com.insideincloud.brainstormapp.api.repository.caderno.CadernosQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoFilter;

@Repository
public interface Cadernos extends JpaRepository<Caderno,Long>, CadernosQuery {
	public Page<Caderno> filtrar(CadernoFilter filtro, Pageable page);

}
