package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="confirmacadastros",schema="public")
public class ConfirmarCadastro implements Serializable {
	private static final long serialVersionUID = -8959251099498310074L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long confirmacadastro_id;
	private String codigo;
	private LocalDate registro;
	private LocalTime ativo; //Horário em que o registro de confirmação foi ativado.
	private Integer validade;
	@OneToOne
	@JoinColumn(name="usuario_id")
	private Usuario usuario;
	
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Long getConfirmacadastro_id() {
		return confirmacadastro_id;
	}
	public void setConfirmacadastro_id(Long confirmacadastro_id) {
		this.confirmacadastro_id = confirmacadastro_id;
	}
//	public String getCodigo() {
//		return codigo;
//	}
//	public void setCodigo(String codigo) {
//		this.codigo = codigo;
//	}
	public LocalDate getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDate registro) {
		this.registro = registro;
	}
	public LocalTime getAtivo() {
		return ativo;
	}
	public void setAtivo(LocalTime ativo) {
		this.ativo = ativo;
	}
	public Integer getValidade() {
		return validade;
	}
	public void setValidade(Integer validade) {
		this.validade = validade;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((confirmacadastro_id == null) ? 0 : confirmacadastro_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfirmarCadastro other = (ConfirmarCadastro) obj;
		if (confirmacadastro_id == null) {
			if (other.confirmacadastro_id != null)
				return false;
		} else if (!confirmacadastro_id.equals(other.confirmacadastro_id))
			return false;
		return true;
	}
	
	
}
