package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.materiacompostaview.MateriasCompostaViewQuery;

@Repository
public interface MateriasCompostaView extends JpaRepository<MateriaCompostaView,Long>, MateriasCompostaViewQuery {
	public List<MateriaCompostaView> findByAownerOrAowner(Long aowner,Long aowner1);
	public List<MateriaCompostaView> findByAowner(Long aowner);
	public Page<MateriaCompostaView> filtrar(MateriaCompostaViewFilter filtro, Pageable page);
}
