package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.TelefoneView;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.telefoneview.TelefonesViewQuery;

@Repository
public interface TelefonesView extends JpaRepository<TelefoneView,Long>, TelefonesViewQuery {
	public Page<TelefoneView> filtrar(TelefoneViewFilter filtro, Pageable page);

}
