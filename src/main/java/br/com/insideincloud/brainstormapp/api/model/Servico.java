package br.com.insideincloud.brainstormapp.api.model;

import br.com.insideincloud.brainstormapp.api.model.view.ServicoAtivoView;

public class Servico {
	
	private Long servico_id;
	private String descricao;
//	private Contrato contrato;
	private Artefato artefato;
	private NaturezaOpServico natureza;
	
	private ServicoAtivoView servico;
	
	private Double basecalculo;
	private Double aliquotaissqn;
	private Double retencao;
	private Double valorretido;
	private Double valor;

}
