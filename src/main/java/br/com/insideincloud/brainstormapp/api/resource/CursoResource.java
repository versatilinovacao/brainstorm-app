package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Curso;
import br.com.insideincloud.brainstormapp.api.model.view.CursoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CursoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.Cursos;
import br.com.insideincloud.brainstormapp.api.repository.CursosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.CursosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.filter.CursoFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.CursoDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.CursoService;

@RestController
@RequestMapping("/cursos")
public class CursoResource {
	@Autowired
	private Cursos cursos;
	
	@Autowired
	private CursosAtivosView cursosativos;
	
	@Autowired
	private CursosInativosView cursosinativos;
	
	@Autowired
	private CursoService cursoservice;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CURSO_CONTEUDO')")
	public RetornoWrapper<CursoAtivoView> conteudo() {
		RetornoWrapper<CursoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			List<CursoAtivoView> cursos = new ArrayList<CursoAtivoView>();
			cursosativos.findAll().forEach(conteudo -> {
				cursos.add(cursoservice.competencias( conteudo ));
			});
			retorno.setConteudo( cursos );
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os cursos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CURSO_CONTEUDO')")
	public RetornoWrapper<CursoInativoView> conteudoInativo() {
		RetornoWrapper<CursoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			List<CursoInativoView> cursos = new ArrayList<CursoInativoView>();
			cursosinativos.findAll().forEach(conteudo -> {
				cursos.add(cursoservice.competencias(conteudo));
			});
			retorno.setConteudo( cursos );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os cursos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		} 
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CURSO_SALVAR')")
	public RetornoWrapper<Curso> salvar(@RequestBody Curso curso, HttpServletResponse response) {
		RetornoWrapper<Curso> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
			
		curso = cursos.save(curso);
		retorno.setSingle(curso);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(curso.getCurso_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CURSO_CONTEUDO')")
	public RetornoWrapper<Curso> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Curso> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			
			retorno.setSingle( cursoservice.competencias(cursos.findOne(codigo)) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar o curso, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_CURSO_DELETAR')")
	public Page<Curso> deletar(@RequestBody CursoDelete deleta, Pageable page, HttpServletResponse response) {
		CursoFilter filtro = new CursoFilter();
		
		for (Curso curso : deleta.getCursos()) {
			cursos.delete(curso.getCurso_id());
		};
		
		return cursos.filtrar(filtro,page);
				
	}
	
}
