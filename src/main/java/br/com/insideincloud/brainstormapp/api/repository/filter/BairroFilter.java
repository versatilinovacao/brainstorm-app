package br.com.insideincloud.brainstormapp.api.repository.filter;

public class BairroFilter {
	private String bairro_id;
	private String descricao;
	
	public String getBairro_id() {
		return bairro_id;
	}
	public void setBairro_id(String bairro_id) {
		this.bairro_id = bairro_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
