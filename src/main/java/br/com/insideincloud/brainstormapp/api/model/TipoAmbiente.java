package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tiposambientes")
public class TipoAmbiente implements Serializable {
	private static final long serialVersionUID = -7472914596850074154L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_ambiente_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	public Long getTipoambiente_id() {
		return tipo_ambiente_id;
	}
	public void setTipoambiente_id(Long tipoambiente_id) {
		this.tipo_ambiente_id = tipoambiente_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_ambiente_id == null) ? 0 : tipo_ambiente_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAmbiente other = (TipoAmbiente) obj;
		if (tipo_ambiente_id == null) {
			if (other.tipo_ambiente_id != null)
				return false;
		} else if (!tipo_ambiente_id.equals(other.tipo_ambiente_id))
			return false;
		return true;
	}
	
	
}
