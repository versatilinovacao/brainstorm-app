package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.view.UsuarioView;

@Entity
@Table(name="mensagens",schema="comunicacao")
public class Mensagem implements Serializable {
	private static final long serialVersionUID = 8746149258658875052L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long mensagem_id;
	private UsuarioView locutor;
	private UsuarioView receptor;
	private String mensagem;
	private LocalDateTime evento;
	@OneToOne
	@JoinColumn(name="dispositivo_id")
	private Dispositivo dispositivo;
	
	public Long getMensagem_id() {
		return mensagem_id;
	}
	public void setMensagem_id(Long mensagem_id) {
		this.mensagem_id = mensagem_id;
	}
	public UsuarioView getLocutor() {
		return locutor;
	}
	public void setLocutor(UsuarioView locutor) {
		this.locutor = locutor;
	}
	public UsuarioView getReceptor() {
		return receptor;
	}
	public void setReceptor(UsuarioView receptor) {
		this.receptor = receptor;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public LocalDateTime getEvento() {
		return evento;
	}
	public void setEvento(LocalDateTime evento) {
		this.evento = evento;
	}
	public Dispositivo getDispositivo() {
		return dispositivo;
	}
	public void setDispositivo(Dispositivo dispositivo) {
		this.dispositivo = dispositivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mensagem_id == null) ? 0 : mensagem_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mensagem other = (Mensagem) obj;
		if (mensagem_id == null) {
			if (other.mensagem_id != null)
				return false;
		} else if (!mensagem_id.equals(other.mensagem_id))
			return false;
		return true;
	}

}
