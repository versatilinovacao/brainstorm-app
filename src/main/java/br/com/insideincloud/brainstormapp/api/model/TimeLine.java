package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="time_line")
public class TimeLine implements Serializable{
	private static final long serialVersionUID = -3221042798936703546L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long time_line_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	public Long getTime_line_id() {
		return time_line_id;
	}
	public void setTime_line_id(Long time_line_id) {
		this.time_line_id = time_line_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((time_line_id == null) ? 0 : time_line_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeLine other = (TimeLine) obj;
		if (time_line_id == null) {
			if (other.time_line_id != null)
				return false;
		} else if (!time_line_id.equals(other.time_line_id))
			return false;
		return true;
	}
	
	
}
