package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="chamados")
public class Chamado implements Serializable {
	private static final long serialVersionUID = -366162776463295626L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long chamado_id;
	@OneToOne
	@JoinColumn(name="chamado_composto_id")
	private Chamado chamado_composto;
	@OneToOne
	@JoinColumns({@JoinColumn(name="empresa_id"),@JoinColumn(name="composicao_empresa_id")})
	private Colaborador empresa;
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador colaborador;
	private LocalDateTime abertura;
	private LocalDateTime fechamento;
	private String titulo;
	private String resumo;
	private String descricao;
	private String solucao;
	@OneToOne
	@JoinColumn(name="time_line_id")
	private TimeLine timeline;
	@OneToOne
	@JoinColumn(name="prioridade_id")
	private Prioridade prioridade;
	
	public Prioridade getPrioridade() {
		return prioridade;
	}
	public void setPrioridade(Prioridade prioridade) {
		this.prioridade = prioridade;
	}
	public Long getChamado_id() {
		return chamado_id;
	}
	public void setChamado_id(Long chamado_id) {
		this.chamado_id = chamado_id;
	}
	public Chamado getChamado_composto() {
		return chamado_composto;
	}
	public void setChamado_composto(Chamado chamado_composto) {
		this.chamado_composto = chamado_composto;
	}
	public Colaborador getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Colaborador empresa) {
		this.empresa = empresa;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public LocalDateTime getAbertura() {
		return abertura;
	}
	public void setAbertura(LocalDateTime abertura) {
		this.abertura = abertura;
	}
	public LocalDateTime getFechamento() {
		return fechamento;
	}
	public void setFechamento(LocalDateTime fechamento) {
		this.fechamento = fechamento;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSolucao() {
		return solucao;
	}
	public void setSolucao(String solucao) {
		this.solucao = solucao;
	}
	public TimeLine getTimeline() {
		return timeline;
	}
	public void setTimeline(TimeLine timeline) {
		this.timeline = timeline;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chamado_id == null) ? 0 : chamado_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chamado other = (Chamado) obj;
		if (chamado_id == null) {
			if (other.chamado_id != null)
				return false;
		} else if (!chamado_id.equals(other.chamado_id))
			return false;
		return true;
	}
	
}
