package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="chamadas_alunos",schema="escola")
public class ChamadaAluno implements Serializable {
	private static final long serialVersionUID = 7946147351482987148L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long chamada_aluno_id;
	@OneToOne
	@JoinColumn(name="caderno_id")
	private Caderno caderno;
	@OneToOne
	@JoinColumn(name="chamada_id")
	private Chamada chamada;
	private LocalDate evento;
	@Column(name="conceito",length=1)
	private String conceito;
	private BigDecimal nota;
	private String avaliacao;
	private boolean presente;
	
	public Long getChamada_aluno_id() {
		return chamada_aluno_id;
	}
	public void setChamada_aluno_id(Long chamada_aluno_id) {
		this.chamada_aluno_id = chamada_aluno_id;
	}
	public Caderno getCaderno() {
		return caderno;
	}
	public void setCaderno(Caderno caderno) {
		this.caderno = caderno;
	}
	public Chamada getChamada() {
		return chamada;
	}
	public void setChamada(Chamada chamada) {
		this.chamada = chamada;
	}
	public LocalDate getEvento() {
		return evento;
	}
	public void setEvento(LocalDate evento) {
		this.evento = evento;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	public boolean isPresente() {
		return presente;
	}
	public void setPresente(boolean presente) {
		this.presente = presente;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chamada_aluno_id == null) ? 0 : chamada_aluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChamadaAluno other = (ChamadaAluno) obj;
		if (chamada_aluno_id == null) {
			if (other.chamada_aluno_id != null)
				return false;
		} else if (!chamada_aluno_id.equals(other.chamada_aluno_id))
			return false;
		return true;
	}
	
	
}
