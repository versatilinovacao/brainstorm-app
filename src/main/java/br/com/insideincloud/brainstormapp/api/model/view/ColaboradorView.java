package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="colaborador_view",schema="public")
public class ColaboradorView implements Serializable{
	private static final long serialVersionUID = -763131108228030267L;

	
	@EmbeddedId
	private ColaboradorPK colaborador_id;
	
//	@Id
	private Long id;
	private String nome;
	private String fantasia;
	private String referencia;
	private String cep;
	private String email;
	private Boolean status;
	private String foto_thumbnail;
	private String telefone;
	private Boolean isempresa;
	private Boolean iscliente;
	private Boolean isterceiro;
	private Boolean isparceiro;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Boolean getIsempresa() {
		return isempresa;
	}
	public void setIsempresa(Boolean isempresa) {
		this.isempresa = isempresa;
	}
	public Boolean getIscliente() {
		return iscliente;
	}
	public void setIscliente(Boolean iscliente) {
		this.iscliente = iscliente;
	}
	public Boolean getIsterceiro() {
		return isterceiro;
	}
	public void setIsterceiro(Boolean isterceiro) {
		this.isterceiro = isterceiro;
	}
	public Boolean getIsparceiro() {
		return isparceiro;
	}
	public void setIsparceiro(Boolean isparceiro) {
		this.isparceiro = isparceiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getFoto_thumbnail() {
		return foto_thumbnail;
	}
	public void setFoto_thumbnail(String foto_thumbnail) {
		this.foto_thumbnail = foto_thumbnail;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorView other = (ColaboradorView) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}
	
}
