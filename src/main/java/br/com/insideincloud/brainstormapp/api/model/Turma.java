package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="turmas")
public class Turma implements Serializable {
	private static final long serialVersionUID = 7140522201931436382L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long turma_id;
	@Column(name="descricao")
	private String descricao;
	private LocalDate inicio;
	private LocalDate fim;
	
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="alunos_turmas", joinColumns = @JoinColumn(name="turma_id"), 
									 inverseJoinColumns = {@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private List<Aluno> alunos;
//	private List<AlunoTurmaView> alunos;
	private Boolean status;
//	@OneToMany
//	@JoinColumn(name="vinculo")
//	private List<AlunoTurmaView> alunosturma;
	
//	public List<AlunoTurmaView> getAlunosturma() {
//		return alunosturma;
//	}
//	public void setAlunosturma(List<AlunoTurmaView> alunosturma) {
//		this.alunosturma = alunosturma;
//	}
	
	public Long getTurma_id() {
		return turma_id;
	}
	public List<Aluno> getAlunos() {
		return alunos;
	}
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getInicio() {
		return inicio;
	}
	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}
	public LocalDate getFim() {
		return fim;
	}
	public void setFim(LocalDate fim) {
		this.fim = fim;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turma_id == null) ? 0 : turma_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turma other = (Turma) obj;
		if (turma_id == null) {
			if (other.turma_id != null)
				return false;
		} else if (!turma_id.equals(other.turma_id))
			return false;
		return true;
	}
	
}
