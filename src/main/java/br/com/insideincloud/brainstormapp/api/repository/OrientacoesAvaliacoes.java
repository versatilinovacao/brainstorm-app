package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.OrientacaoAvaliacao;

@Repository
public interface OrientacoesAvaliacoes extends JpaRepository<OrientacaoAvaliacao, Long> {
	
	@Query("select a from OrientacaoAvaliacao a where a.avaliacao.avaliacaoalunoatividade_id = ?1")
	public List<OrientacaoAvaliacao> findByAvaliacao(Long avaliacao_id);
}
