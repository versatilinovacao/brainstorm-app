package br.com.insideincloud.brainstormapp.api.repository.filter;

public class PeriodoLetivoItemFilter {
	private String periodoletivo_id;
	private String periodo_letivo_item_id;
	
	public String getPeriodoletivo_id() {
		return periodoletivo_id;
	}
	public void setPeriodoletivo_id(String periodoletivo_id) {
		this.periodoletivo_id = periodoletivo_id;
	}
	public String getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(String periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}

}
