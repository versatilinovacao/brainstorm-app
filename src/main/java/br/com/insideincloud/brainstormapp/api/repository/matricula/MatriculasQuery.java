package br.com.insideincloud.brainstormapp.api.repository.matricula;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Matricula;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaFilter;

public interface MatriculasQuery {
	public Page<Matricula> filtrar(MatriculaFilter filtro, Pageable page);
}
