package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Contrato;
import br.com.insideincloud.brainstormapp.api.model.view.ContratoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.ContratoInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.ContratoParcelaView;
import br.com.insideincloud.brainstormapp.api.repository.ContratosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.ContratosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.ContratosParcelasView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.ContratoService;

@RestController
@RequestMapping("/contratos")
public class ContratoResource {
	
	@Autowired
	private ContratoService contratos;
	
	@Autowired
	private ContratosAtivosView contratosAtivos;
	
	@Autowired
	private ContratosInativosView contratosInativos;
	
	@Autowired
	private ContratosParcelasView contratosParcelas;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTRATO_SALVAR')")
	public RetornoWrapper<Contrato> salvar(@RequestBody Contrato contrato) {
		RetornoWrapper<Contrato> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			
			retorno.setSingle( contratos.salvar(contrato) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o contrato");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/parcelas/{contrato_id}")
	@PreAuthorize("hasAuthority('ROLE_CONTRATO_CONTEUDO')")
	public RetornoWrapper<ContratoParcelaView> conteudoParcelas(@PathVariable Long contrato_id) {
		RetornoWrapper<ContratoParcelaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contratosParcelas.findByContrato(contrato_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foram localizadas parcelas para este contrato.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CONTRATO_CONTEUDO')")
	public RetornoWrapper<Contrato> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<Contrato> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( contratos.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o contrato.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTRATO_CONTEUDO')")
	public RetornoWrapper<ContratoAtivoView> conteudoAtivo() {
		RetornoWrapper<ContratoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contratosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os contratos ativos.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CONTRATO_CONTEUDO')")
	public RetornoWrapper<ContratoInativoView> conteudoInativo() {
		RetornoWrapper<ContratoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contratosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar os contratos inativos.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

}
