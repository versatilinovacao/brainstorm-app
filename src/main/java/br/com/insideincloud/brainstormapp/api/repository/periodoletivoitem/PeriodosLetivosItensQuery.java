package br.com.insideincloud.brainstormapp.api.repository.periodoletivoitem;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoItemFilter;

public interface PeriodosLetivosItensQuery {
	public Page<PeriodoLetivoItem> filtrar(PeriodoLetivoItemFilter filtro, Pageable page);
}
