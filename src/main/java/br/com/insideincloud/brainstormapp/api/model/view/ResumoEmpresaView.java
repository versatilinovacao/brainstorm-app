package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="resumo_empresas_view", schema="public")
public class ResumoEmpresaView implements Serializable {
	private static final long serialVersionUID = 3621570093732163467L;

	@Id
	private Long escola_id;
	@Column(name="nome")
	private String nome;
	
	public Long getEscola_id() {
		return escola_id;
	}
	public void setEscola_id(Long escola_id) {
		this.escola_id = escola_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((escola_id == null) ? 0 : escola_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResumoEmpresaView other = (ResumoEmpresaView) obj;
		if (escola_id == null) {
			if (other.escola_id != null)
				return false;
		} else if (!escola_id.equals(other.escola_id))
			return false;
		return true;
	}
	
}
