package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_matricula", schema="action")
public class DeletarMatriculaAction implements Serializable {
	private static final long serialVersionUID = -1784878136202749746L;

	@Id
	private Long matricula_id;

	public Long getMatricula_id() {
		return matricula_id;
	}

	public void setMatricula_id(Long matricula_id) {
		this.matricula_id = matricula_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula_id == null) ? 0 : matricula_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeletarMatriculaAction other = (DeletarMatriculaAction) obj;
		if (matricula_id == null) {
			if (other.matricula_id != null)
				return false;
		} else if (!matricula_id.equals(other.matricula_id))
			return false;
		return true;
	}
	
	
}
