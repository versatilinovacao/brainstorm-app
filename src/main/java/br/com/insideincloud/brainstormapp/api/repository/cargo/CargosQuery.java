package br.com.insideincloud.brainstormapp.api.repository.cargo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Cargo;
import br.com.insideincloud.brainstormapp.api.repository.filter.CargoFilter;

public interface CargosQuery {
	Page<Cargo> filtrar(CargoFilter filtro, Pageable page);
}
