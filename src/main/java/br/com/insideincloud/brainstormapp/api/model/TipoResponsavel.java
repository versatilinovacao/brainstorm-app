package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_responsaveis")
public class TipoResponsavel implements Serializable {
	private static final long serialVersionUID = -1141825097068310523L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	private Long tiporesponsavel_id;
	@Column(name="descricao")
	private String nome;
	
	public Long getTiporesponsavel_id() {
		return tiporesponsavel_id;
	}
	public void setTiporesponsavel_id(Long tiporesponsavel_id) {
		this.tiporesponsavel_id = tiporesponsavel_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiporesponsavel_id == null) ? 0 : tiporesponsavel_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoResponsavel other = (TipoResponsavel) obj;
		if (tiporesponsavel_id == null) {
			if (other.tiporesponsavel_id != null)
				return false;
		} else if (!tiporesponsavel_id.equals(other.tiporesponsavel_id))
			return false;
		return true;
	}

}
