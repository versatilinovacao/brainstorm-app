package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="gradecurricular",schema="escola")
public class GradeCurricular implements Serializable {
	private static final long serialVersionUID = 6779943940906345193L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long gradecurricular_id;
//	@ManyToOne
//	@JoinColumn(name="composicao_id")
//	private GradeCurricular composicao;
	@OneToOne
	@JoinColumn(name="curso_id")
	private Curso curso;
	@OneToOne
	@JoinColumn(name="materia_id")
	private Materia materia;
	@OneToOne
	@JoinColumn(name="turma_id")
	private Turma turma;
	@OneToOne
	@JoinColumns({@JoinColumn(name="professor_id"),@JoinColumn(name="composicao_professor_id")})
	private Colaborador professor;
	@ManyToOne
	@JoinColumn(name = "cursoperiodo_id")
	private CursoPeriodo cursoperiodo;
	private Boolean status;

	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Colaborador getProfessor() {
		return professor;
	}
	public void setProfessor(Colaborador professor) {
		this.professor = professor;
	}
	public CursoPeriodo getCursoperiodo() {
		return cursoperiodo;
	}
	public void setCursoperiodo(CursoPeriodo cursoperiodo) {
		this.cursoperiodo = cursoperiodo;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gradecurricular_id == null) ? 0 : gradecurricular_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GradeCurricular other = (GradeCurricular) obj;
		if (gradecurricular_id == null) {
			if (other.gradecurricular_id != null)
				return false;
		} else if (!gradecurricular_id.equals(other.gradecurricular_id))
			return false;
		return true;
	}

}
