package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ResumoEmpresaViewFilter {
	private String escola_id;
	private String nome;
	
	public String getEscola_id() {
		return escola_id;
	}
	public void setEscola_id(String escola_id) {
		this.escola_id = escola_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
