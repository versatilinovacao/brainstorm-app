package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class Contato {
	private String Telefone;
	private String Email;
	
	public String getTelefone() {
		return Telefone;
	}
	public void setTelefone(String telefone) {
		Telefone = telefone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}

}
