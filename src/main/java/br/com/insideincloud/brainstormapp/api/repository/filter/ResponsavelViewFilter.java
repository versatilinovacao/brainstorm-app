package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ResponsavelViewFilter {
	private String colaborador_id;
	private String composicao_colaborador_id;
	private String responsavel_id;
	private String nome;
	
	
	public String getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(String composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getResponsavel_id() {
		return responsavel_id;
	}
	public void setResponsavel_id(String responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	

}
