package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos",schema="recurso")
public class TipoProduto implements Serializable {
	private static final long serialVersionUID = -388583240831394394L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_id;
	@Column(name="nome",length= 60)
	
	private String nome;
	public Long getTipo_id() {
		return tipo_id;
	}
	public void setTipo_id(Long tipo_id) {
		this.tipo_id = tipo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_id == null) ? 0 : tipo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProduto other = (TipoProduto) obj;
		if (tipo_id == null) {
			if (other.tipo_id != null)
				return false;
		} else if (!tipo_id.equals(other.tipo_id))
			return false;
		return true;
	}
	
	
}
