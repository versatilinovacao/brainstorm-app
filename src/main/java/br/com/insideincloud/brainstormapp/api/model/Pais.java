package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Table(name="paises")

@Entity
@Table(name="pais_view",schema="remoto")
public class Pais implements Serializable {
	private static final long serialVersionUID = -8054466030030733679L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pais_id;
	@Column(name="sigla",length=3)
	private String sigla;
	@Column(name="descricao",length=40)
	private String descricao;
	
	public Long getPais_id() {
		return pais_id;
	}
	public void setPais_id(Long pais_id) {
		this.pais_id = pais_id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pais_id == null) ? 0 : pais_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (pais_id == null) {
			if (other.pais_id != null)
				return false;
		} else if (!pais_id.equals(other.pais_id))
			return false;
		return true;
	}
	
}
