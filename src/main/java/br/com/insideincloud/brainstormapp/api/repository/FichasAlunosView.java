package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.FichaAlunoView;

@Repository
public interface FichasAlunosView extends JpaRepository<FichaAlunoView, Long> {

	@Query("select f from FichaAlunoView f where f.aluno_id = ?1")
	public List<FichaAlunoView> findByAluno(Long aluno_id);
	
}
