package br.com.insideincloud.brainstormapp.api.resource;

import java.io.IOException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ContratoModelo;
import br.com.insideincloud.brainstormapp.api.repository.ContratosModelos;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/contratosmodelos")
public class ContratoModeloResource {

	@Autowired
	private ContratosModelos contratos;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTRATOMODELO_SALVAR')")
	public RetornoWrapper<ContratoModelo> salvar(@RequestParam String contrato, @RequestParam MultipartFile file) {
		RetornoWrapper<ContratoModelo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		
		
		Gson json = new Gson();
		ContratoModelo modelo = json.fromJson(contrato,ContratoModelo.class);
		modelo.setDescricao(file.getOriginalFilename());

		try {
			modelo.setDocumento( Base64.encodeBase64String( file.getBytes() ) );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(modelo.getDescricao());
 		
		try {
			retorno.setSingle( contratos.saveAndFlush(modelo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o modelo, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTRATOMODELO_CONTEUDO')")
	public RetornoWrapper<ContratoModelo> conteudo() {
		RetornoWrapper<ContratoModelo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contratos.findAll() );
		} catch( Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum modelo de contrato.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{contrato_id}")
	@PreAuthorize("hasAuthority('ROLE_CONTRATOMODELO_CONTEUDO')")
	public RetornoWrapper<ContratoModelo> conteudoPorId(@PathVariable Long contrato_id) {
		RetornoWrapper<ContratoModelo> retorno = new RetornoWrapper<ContratoModelo>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(contratos.findOne(contrato_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("O modelo do contrato não foi localizado.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
