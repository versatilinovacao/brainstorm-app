package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.OrdemServico;
import br.com.insideincloud.brainstormapp.api.model.Rps;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.Rpss;

@Service
public class OrdemServicoService {
	
	private OrdemServico ordem;
	
	@Autowired
	private Rpss rpss;
	
	@Autowired 
	private Colaboradores colaboradores;
	
	public void gerarRps(OrdemServico ordem) throws Exception {
		this.ordem = ordem;
		Rps rps = new Rps();	
		rps = rpss.saveAndFlush( parseRps(rps) );
	}
	
	private Rps parseRps(Rps rps) {
//		Colaborador empresa = colaboradores.findOne(1L);
//		rps.setPrestador(null);
//		rps.setTomador(null);
//		rps.setIncentivadorcultural(empresa.getIncentivadorCultural());
//		rps.setOptantesimplesnacional(empresa.getOptanteSimplesNascional());
		
		return null;
	}

}
