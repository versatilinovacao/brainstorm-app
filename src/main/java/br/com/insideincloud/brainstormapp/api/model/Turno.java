package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="turnos")
public class Turno implements Serializable {
	private static final long serialVersionUID = -4244333674387807855L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long turno_id;
	@Column(name="descricao",length=100)
	private String nome;
	
	public Long getTurno_id() {
		return turno_id;
	}
	public void setTurno_id(Long turno_id) {
		this.turno_id = turno_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turno_id == null) ? 0 : turno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Turno other = (Turno) obj;
		if (turno_id == null) {
			if (other.turno_id != null)
				return false;
		} else if (!turno_id.equals(other.turno_id))
			return false;
		return true;
	}
	
	
}
