package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="permissoes_usuario_view",schema="public")
public class PermissaoUsuarioView implements Serializable {
	private static final long serialVersionUID = 3767030119291038459L;

	@Id
	private Long permissaousuarioview_id;
	private Long usuario_id;
	private Long permissao_id;
	private String nome;
	private String label;
	
	public Long getPermissaousuarioview_id() {
		return permissaousuarioview_id;
	}
	public void setPermissaousuarioview_id(Long permissaousuarioview_id) {
		this.permissaousuarioview_id = permissaousuarioview_id;
	}
	public Long getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Long usuario_id) {
		this.usuario_id = usuario_id;
	}
	public Long getPermissao_id() {
		return permissao_id;
	}
	public void setPermissao_id(Long permissao_id) {
		this.permissao_id = permissao_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((usuario_id == null) ? 0 : usuario_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermissaoUsuarioView other = (PermissaoUsuarioView) obj;
		if (usuario_id == null) {
			if (other.usuario_id != null)
				return false;
		} else if (!usuario_id.equals(other.usuario_id))
			return false;
		return true;
	}
	
	
}
