package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.dblink.Estado;

@Repository
public interface EstadosRemoto extends JpaRepository<Estado, Long> {
	
	@Query("select e from Estado e where e.pais.pais_id = ?1")
	public List<Estado> findByEstados(Long pais_id);

}
