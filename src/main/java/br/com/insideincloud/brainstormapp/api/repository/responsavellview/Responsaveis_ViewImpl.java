package br.com.insideincloud.brainstormapp.api.repository.responsavellview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.ResponsavellView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavellViewFilter;

public class Responsaveis_ViewImpl implements Responsaveis_ViewQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ResponsavellView> filtrar(ResponsavellViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResponsavellView> criteria = builder.createQuery(ResponsavellView.class);
		Root<ResponsavellView> root = criteria.from(ResponsavellView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ResponsavellView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ResponsavellViewFilter filtro, CriteriaBuilder builder, Root<ResponsavellView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getResponsavel_id())) {
				predicates.add(builder.equal(root.get("responsavel_id"), Long.parseLong(filtro.getResponsavel_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getVinculo_id())) {
				predicates.add(builder.equal(root.get("vinculo_id"), Long.parseLong(filtro.getVinculo_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getNome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ResponsavellViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ResponsavellView> root = criteria.from(ResponsavellView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
