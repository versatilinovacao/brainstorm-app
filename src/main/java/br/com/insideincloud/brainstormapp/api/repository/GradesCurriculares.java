package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularFilter;
import br.com.insideincloud.brainstormapp.api.repository.gradecurricular.GradesCurricularesQuery;

@Repository
public interface GradesCurriculares extends JpaRepository<GradeCurricular,Long>,GradesCurricularesQuery {
	
	public Page<GradeCurricular> filtrar(GradeCurricularFilter filtro, Pageable page);
	public Optional<GradeCurricular> findByTurma(Turma turma);
	
	@Query("select g from GradeCurricular g where g.turma.turma_id = ?1 and g.status = true")
	public List<GradeCurricular> findByTurmasAtivas(Long turma);
	
	@Query("select g from GradeCurricular g where g.curso.curso_id = ?1")
	public List<GradeCurricular> findByGradePorCurso(Long curso_id); 
	
}
