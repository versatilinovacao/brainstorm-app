package br.com.insideincloud.brainstormapp.api.repository.materiacompostaview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;

public interface MateriasCompostaViewQuery {
	public Page<MateriaCompostaView> filtrar(MateriaCompostaViewFilter filtro, Pageable page);

}
