package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="estoque_id")
public class Estoque implements Serializable {
	private static final long serialVersionUID = 7582503861972758831L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long estoque_id;
	@Column(name="quantidade",length=16,precision=4)
	private BigDecimal quantidade;
	
	public Long getEstoque_id() {
		return estoque_id;
	}
	public void setEstoque_id(Long estoque_id) {
		this.estoque_id = estoque_id;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estoque_id == null) ? 0 : estoque_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estoque other = (Estoque) obj;
		if (estoque_id == null) {
			if (other.estoque_id != null)
				return false;
		} else if (!estoque_id.equals(other.estoque_id))
			return false;
		return true;
	}
	
}
