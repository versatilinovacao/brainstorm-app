package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.view.NaturezaOpServicoAtivaView;
import br.com.insideincloud.brainstormapp.api.repository.NaturezasOpServicosAtivasView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/naturezasopservicos")
public class NaturezaOpServicoResource {

	@Autowired
	private NaturezasOpServicosAtivasView naturezas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_NATUREZAOPSERVICO_CONTEUDO')")
	public RetornoWrapper<NaturezaOpServicoAtivaView> conteudoAtivo() {
		RetornoWrapper<NaturezaOpServicoAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( naturezas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações de naturezas de operações de serviços ativos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
		}
		
		return retorno;
	}
	
}
