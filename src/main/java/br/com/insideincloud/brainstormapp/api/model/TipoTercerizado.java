package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_tercerizados")
public class TipoTercerizado implements Serializable {
	/*
	 * Tipos: Fornecedor - Representante - Contador - Financeira - Banco - Etc
	 **/	
	private static final long serialVersionUID = 5879804769739178913L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipotercerizado_id;
	private String descricao;
	
	public Long getTipotercerizado_id() {
		return tipotercerizado_id;
	}
	public void setTipotercerizado_id(Long tipotercerizado_id) {
		this.tipotercerizado_id = tipotercerizado_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipotercerizado_id == null) ? 0 : tipotercerizado_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoTercerizado other = (TipoTercerizado) obj;
		if (tipotercerizado_id == null) {
			if (other.tipotercerizado_id != null)
				return false;
		} else if (!tipotercerizado_id.equals(other.tipotercerizado_id))
			return false;
		return true;
	}
	
}
