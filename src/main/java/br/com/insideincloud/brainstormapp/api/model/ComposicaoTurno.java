package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="composicao_turnos",schema="public")
public class ComposicaoTurno implements Serializable {
	private static final long serialVersionUID = -3621194605132115996L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long turno_id;
	private String descricao;
	private LocalTime inicio;
	private LocalTime fim;
	
	public Long getTurno_id() {
		return turno_id;
	}
	public void setTurno_id(Long turno_id) {
		this.turno_id = turno_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalTime getInicio() {
		return inicio;
	}
	public void setInicio(LocalTime inicio) {
		this.inicio = inicio;
	}
	public LocalTime getFim() {
		return fim;
	}
	public void setFim(LocalTime fim) {
		this.fim = fim;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turno_id == null) ? 0 : turno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComposicaoTurno other = (ComposicaoTurno) obj;
		if (turno_id == null) {
			if (other.turno_id != null)
				return false;
		} else if (!turno_id.equals(other.turno_id))
			return false;
		return true;
	}

}
