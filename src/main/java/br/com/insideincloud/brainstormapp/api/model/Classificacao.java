package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="classificacoes",schema="configuracao")
public class Classificacao implements Serializable {
	private static final long serialVersionUID = -7369641531562486430L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classificacao_id;
	private String descricao;
	private Boolean valor;
		
	public Boolean getValor() {
		return valor;
	}
	public void setValor(Boolean valor) {
		this.valor = valor;
	}
	public Long getClassificacao_id() {
		return classificacao_id;
	}
	public void setClassificacao_id(Long classificacao_id) {
		this.classificacao_id = classificacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classificacao_id == null) ? 0 : classificacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classificacao other = (Classificacao) obj;
		if (classificacao_id == null) {
			if (other.classificacao_id != null)
				return false;
		} else if (!classificacao_id.equals(other.classificacao_id))
			return false;
		return true;
	}
	
	
	
}
