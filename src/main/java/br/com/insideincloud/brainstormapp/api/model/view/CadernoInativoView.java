package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cadernos_inativos_view",schema="escola")
public class CadernoInativoView implements Serializable {
	private static final long serialVersionUID = 132704376854596979L;
	
	@Id
	private Long caderno_id;
	private String descricao;
	private Long gradecurricular_id;
	private String conceito;
	private Double nota;
	private Long escola_id;
	private Long composicao_escola_id;
	private Long periodo_letivo_id;
	private Long turma_id;
	@Column(name="nome_turma")
	private String nometurma;
	@Column(name="ano")
	private String anoletivo;
	private Boolean status;
	
	public String getAnoletivo() {
		return anoletivo;
	}
	public void setAnoletivo(String anoletivo) {
		this.anoletivo = anoletivo;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public Double getNota() {
		return nota;
	}
	public void setNota(Double nota) {
		this.nota = nota;
	}
	public Long getEscola_id() {
		return escola_id;
	}
	public void setEscola_id(Long escola_id) {
		this.escola_id = escola_id;
	}
	public Long getComposicao_escola_id() {
		return composicao_escola_id;
	}
	public void setComposicao_escola_id(Long composicao_escola_id) {
		this.composicao_escola_id = composicao_escola_id;
	}
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public String getNometurma() {
		return nometurma;
	}
	public void setNometurma(String nometurma) {
		this.nometurma = nometurma;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caderno_id == null) ? 0 : caderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadernoInativoView other = (CadernoInativoView) obj;
		if (caderno_id == null) {
			if (other.caderno_id != null)
				return false;
		} else if (!caderno_id.equals(other.caderno_id))
			return false;
		return true;
	}
	
}
