package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hd",schema="arquivos")
public class Arquivo implements Serializable {
	private static final long serialVersionUID = -2725424778370868750L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long arquivo_id;
	private String nome;
	private LocalDateTime registro;
	private String extensao;
	private String arquivo;
	private String tipo;
	private Long tamanho;
	private String url;	
	private Boolean principal;
		
	public Boolean getPrincipal() {
		return principal;
	}
	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Long getTamanho() {
		return tamanho;
	}
	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	//	public String getBase64() {
//		byte[] encoded = Base64.getEncoder().encode( arquivo );
//		this.base64 = new String(encoded);
//		return base64;
//	}
	public Long getArquivo_id() {
		return arquivo_id;
	}
	public void setArquivo_id(Long arquivo_id) {
		this.arquivo_id = arquivo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDateTime getRegistro() {
		if (registro == null) { registro = LocalDateTime.now();		}
		
		return registro;
	}
	public void setRegistro(LocalDateTime registro) {
		if (registro == null) {
			registro = LocalDateTime.now();
		}
		
		this.registro = registro;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivo_id == null) ? 0 : arquivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arquivo other = (Arquivo) obj;
		if (arquivo_id == null) {
			if (other.arquivo_id != null)
				return false;
		} else if (!arquivo_id.equals(other.arquivo_id))
			return false;
		return true;
	}

}
