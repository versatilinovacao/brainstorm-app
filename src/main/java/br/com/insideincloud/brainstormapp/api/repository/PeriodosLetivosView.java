package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.periodoletivoview.PeriodosLetivosViewQuery;

@Repository
public interface PeriodosLetivosView extends JpaRepository<PeriodoLetivoView,Long>, PeriodosLetivosViewQuery {
	public Page<PeriodoLetivoView> filtrar(PeriodoLetivoViewFilter filtro, Pageable page);
	
	@Query("select p from PeriodoLetivoView p where p.empresa_id = ?1")
	public List<PeriodoLetivoView> findByPeriodoLetivoView(Long empresa_id);
}
