package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaPagarPK;
import br.com.insideincloud.brainstormapp.api.model.view.ContaPagarPendenteView;

@Repository
public interface ContasPagarPendentesView extends JpaRepository<ContaPagarPendenteView, ContaPagarPK> {

}
