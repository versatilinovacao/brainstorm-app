package br.com.insideincloud.brainstormapp.api.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.multipart.MultipartFile;

public class Disco {
	
	private Path diretorioPath;
	private Path arquivoPath;
	
	public Disco(String raiz, String diretorio) {
		diretorioPath = Paths.get(raiz,diretorio);
		
	}
	
	public void salvarArquivo(MultipartFile arquivo) throws IOException {
		arquivoPath = diretorioPath.resolve(arquivo.getOriginalFilename());
		
		Files.createDirectories(diretorioPath);
		arquivo.transferTo(arquivoPath.toFile());
		
	}
	
	public Boolean deletarArquivo(String arquivo) {
		arquivoPath = diretorioPath.resolve(arquivo);
		System.out.println(" <<<<<<<<<<<<<<<<<<<<<<<< "+arquivoPath.toFile());
		
		File folder = new File("/var/local/insideincloud/repositorio");
		if (folder.isDirectory()) {
			File[] sun = folder.listFiles();
			for (File toDelete : sun) {
				if (toDelete.getName().equals( arquivo )) {
					toDelete.delete();
				}
			}
		}
		
//		File pasta = new File(arquivoPath.toFile().getPath());
//		File[] arquivos = pasta.listFiles();
//		
//		for(File conteudo : arquivos) {
//			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>"+conteudo+"<<<<<<<<<<<<<<<<<<<<<<<<<<");
//		}
//		
		return (new File(arquivoPath.toFile()+arquivo).delete());		
	}
	
}
