package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.CepImport;

@Repository
public interface CepsImports extends JpaRepository<CepImport,Long> {

}
