package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TaxaSelic;
import br.com.insideincloud.brainstormapp.api.repository.filter.TaxaSelicFilter;
import br.com.insideincloud.brainstormapp.api.repository.taxaselic.TaxasSelicsQuery;

@Repository
public interface TaxasSelics extends JpaRepository<TaxaSelic,Long>, TaxasSelicsQuery {
	public Page<TaxaSelic> filtrar(TaxaSelicFilter filtro, Pageable page);
}