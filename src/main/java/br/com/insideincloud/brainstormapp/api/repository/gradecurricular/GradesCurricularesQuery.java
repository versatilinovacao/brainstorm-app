package br.com.insideincloud.brainstormapp.api.repository.gradecurricular;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularFilter;

public interface GradesCurricularesQuery {
	public Page<GradeCurricular> filtrar(GradeCurricularFilter filtro, Pageable page);
}
