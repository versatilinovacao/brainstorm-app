package br.com.insideincloud.brainstormapp.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Colaborador.class)
public abstract class Colaborador_ {
	public static volatile SingularAttribute<Colaborador, Long> colaborador_id;
	public static volatile SingularAttribute<Colaborador, String> nome;
	public static volatile SingularAttribute<Colaborador, String> fantasia;
}
