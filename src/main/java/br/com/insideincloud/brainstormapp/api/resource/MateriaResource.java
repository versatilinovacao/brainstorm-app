package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Materia;
import br.com.insideincloud.brainstormapp.api.model.MateriaConteudo;
import br.com.insideincloud.brainstormapp.api.model.TipoMateria;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaInativaView;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaView;
import br.com.insideincloud.brainstormapp.api.repository.Materias;
import br.com.insideincloud.brainstormapp.api.repository.MateriasInativasView;
import br.com.insideincloud.brainstormapp.api.repository.MateriasView;
import br.com.insideincloud.brainstormapp.api.repository.TiposMaterias;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.MateriaDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.MateriaCompostaService;
import br.com.insideincloud.brainstormapp.api.service.MateriaConteudoService;

@RestController
@RequestMapping("/materias")
public class MateriaResource {
	@Autowired
	private Materias materias;
	
	@Autowired
	private TiposMaterias tiposmaterias;
	
	@Autowired
	private MateriaCompostaService materiacomposta;
	
	@Autowired
	private MateriaConteudoService conteudos;
	
	@Autowired
	private MateriasView materiasAtivas;
	
	@Autowired
	private MateriasInativasView materiasInativas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<MateriaView> conteudo() {
		RetornoWrapper<MateriaView> retorno = new RetornoWrapper<MateriaView>();
		retorno.setConteudo(materiasAtivas.findAll());
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<MateriaInativaView> conteudoInativo() {
		RetornoWrapper<MateriaInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<MateriaInativaView> conteudo = materiasInativas.findAll();
			retorno.setConteudo(conteudo);			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foram localizados informações de Materias Inativas ou ocorreu um instabilidade.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<Materia> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Materia> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(materias.findOne(codigo));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("A materia que você procura não esta disponivel no momento, tente novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_MATERIA_SALVAR')")
	@Transactional
	public RetornoWrapper<Materia> salvar(@RequestBody Materia materia, HttpServletResponse response) {
		RetornoWrapper<Materia> retorno = new RetornoWrapper<Materia>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			materia = materias.save(materia);
			retorno.setSingle(materia);
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Erro ao salvar a permissão, favor verificar as inconsistências.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(materia.getMateria_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@GetMapping("/tiposmaterias")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_SALVAR')")
	public RetornoWrapper<TipoMateria> conteudoTiposMaterias() {
		RetornoWrapper<TipoMateria> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
				
		try {
			List<TipoMateria> conteudo = tiposmaterias.findAll();
			retorno.setConteudo(conteudo);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não localizamos Tipos de Materias ou estamos com alguma indisponibilidade momentanea!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PutMapping("/materias")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<MateriaCompostaView> conteudo(@RequestBody MateriaCompostaViewFilter filtro, Pageable page) {
		
		return materiacomposta.filtrar(filtro, page);
	}
	
	@PostMapping("/conteudo")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	@Transactional
	public RetornoWrapper<MateriaConteudo> salvar(@RequestBody MateriaConteudo conteudo, HttpServletResponse response) {
		RetornoWrapper<MateriaConteudo> retorno = new RetornoWrapper<MateriaConteudo>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			conteudo = conteudos.salvar(conteudo);
			retorno.setSingle(conteudo);
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Erro ao salvar a permissão, favor verificar as inconsistências.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
				
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(conteudo.getMateria_conteudo_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_MATERIA_DELETAR')")
	@Transactional
	public RetornoWrapper<Materia> deletar(@RequestBody MateriaDelete deleta, Pageable page, HttpServletResponse response) {
		RetornoWrapper<Materia> retorno = new RetornoWrapper<Materia>();
		ExceptionWrapper erro = new ExceptionWrapper();

		try {
			for (Materia materia : deleta.getMaterias()) {
				materias.delete(materia.getMateria_id());
			};
			retorno.setConteudo(materias.findAll());
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível deletar um ou mais de um arquivo selecionado, favor tentar novamente mais tarde.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
				
	}

}
