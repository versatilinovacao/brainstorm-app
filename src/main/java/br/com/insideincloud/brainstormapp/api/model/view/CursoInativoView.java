package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cursos_inativos_view",schema="escola")
public class CursoInativoView implements Serializable {
	private static final long serialVersionUID = 1405608116633191854L;
	
	@Id
	private Long curso_id;
	private String descricao;
	private LocalDate competencia_inicial;
	private LocalDate competencia_final;
	private Boolean status;
	private Long periodo_letivo_id;
	
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getCompetencia_inicial() {
		return competencia_inicial;
	}
	public void setCompetencia_inicial(LocalDate competencia_inicial) {
		this.competencia_inicial = competencia_inicial;
	}
	public LocalDate getCompetencia_final() {
		return competencia_final;
	}
	public void setCompetencia_final(LocalDate competencia_final) {
		this.competencia_final = competencia_final;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((curso_id == null) ? 0 : curso_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CursoInativoView other = (CursoInativoView) obj;
		if (curso_id == null) {
			if (other.curso_id != null)
				return false;
		} else if (!curso_id.equals(other.curso_id))
			return false;
		return true;
	}	

}
