package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.CardapioSemana;

public class CardapiosSemanasDelete {

	private Long cardapio_id;
	private List<CardapioSemana> semanas;

	public Long getCardapio_id() {
		return cardapio_id;
	}

	public void setCardapio_id(Long cardapio_id) {
		this.cardapio_id = cardapio_id;
	}

	public List<CardapioSemana> getSemanas() {
		return semanas;
	}

	public void setSemanas(List<CardapioSemana> semanas) {
		this.semanas = semanas;
	}
	
}
