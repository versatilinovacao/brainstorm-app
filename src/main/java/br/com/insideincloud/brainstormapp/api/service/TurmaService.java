package br.com.insideincloud.brainstormapp.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionTratada;
import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosMatriculadosView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosVinculadosTurmasView;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurriculares;
import br.com.insideincloud.brainstormapp.api.repository.Turmas;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoViewFilter;
import br.com.insideincloud.brainstormapp.api.retorno.RetornoFachada;

@Service
public class TurmaService {
	@Autowired
	private Turmas turmas;
	
	@Autowired
	private GradesCurriculares grades;
	
	@Autowired
	private AlunosMatriculadosView alunosmatriculados;
	
	@Autowired
	private AlunosVinculadosTurmasView vinculos;

	private Long curso_id;
	
	public List<MatriculaAlunoView> findByAlunosMatriculados(Long turma_id) {
		List<MatriculaAlunoView> alunos = new ArrayList<MatriculaAlunoView>();
		
		try {
			if (getValidaGrade(turma_id)) {
				new Exception("Ocorreu um erro, verifique!");
			} 
			
			alunos = alunosmatriculados.findByAlunosMatriculadosPorCurso(this.curso_id);
		} catch(Exception e) {
			
		}
		
		return alunos;
	}
	
	public RetornoFachada<MatriculaAlunoView> instanciaFachada(MatriculaAlunoViewFilter filtro, Pageable page) {
		RetornoFachada<MatriculaAlunoView> retorno = new RetornoFachada<MatriculaAlunoView>();
		
		try {
			if (getValidaGrade(Long.valueOf(filtro.getTurma_id()))) {
				new Exception("Ocorreu um erro, verifique!");
			} 
			
			MatriculaAlunoViewFilter filter = new MatriculaAlunoViewFilter();
			filter.setAluno_id(filtro.getAluno_id());
			filter.setAlunonome(filtro.getAlunonome());
			filter.setMatricula(filtro.getMatricula());
			if (this.curso_id > 0L ) {
				filter.setCurso_id( String.valueOf(this.curso_id) );				
			} else {
				filter.setCurso_id("");				
			} 
						
			retorno.setPage(alunosmatriculados.filtrar(filter,page));			
			retorno.setException(null);
		} catch (Exception e) {	
				//e.printStackTrace();
			ExceptionTratada excecao = new ExceptionTratada();
			excecao.setCodigo(0000);
			excecao.setMensagem("Ola. Antes de continuar, você precisa vincular esta turma a uma Grade Curricular!");
			
			retorno.setException(excecao);
		}
		
		return retorno;
	}
	
	private boolean getValidaGrade(Long turma_id) {
		Turma turma = turmas.findOne(turma_id);
//		System.out.println(turma.getTurma_id());
		
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>> "+turma.getDescricao()+" - "+turma.getTurma_id());
		
		List<GradeCurricular> gradesVinculadas = grades.findByTurmasAtivas(turma.getTurma_id());
//		Optional<GradeCurricular> gradeOptional  = grades.findByTurmaAtiva(turma.getTurma_id());
		
//		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>> "+gradeOptional.get().getTurma());
//		GradeCurricular grade = gradeOptional.get();
//		System.out.println("...................................."+grade.getTurma().getTurma_id().equals(turma_id));
		
		if (gradesVinculadas != null) {
			if (gradesVinculadas.get(0).getTurma().getTurma_id().equals(turma_id)) {
				this.curso_id = gradesVinculadas.get(0).getCurso().getCurso_id();
				return !true;
			}			
		}
		
		return !false;
	}
}
