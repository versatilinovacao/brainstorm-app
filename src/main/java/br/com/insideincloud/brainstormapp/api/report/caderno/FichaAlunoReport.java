package br.com.insideincloud.brainstormapp.api.report.caderno;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.Empresa;
import br.com.insideincloud.brainstormapp.api.model.view.FichaAlunoView;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
public class FichaAlunoReport {

	public byte[] reportFicha(List<FichaAlunoView> alunos, Empresa empresa) throws JRException {
		InputStream inputStream;		
		try {
			inputStream = new FileInputStream("/var/local/ecosistema/repositorio/relatorios/escola/ficha.jasper");
			Map<String, Object> allParameters = new HashMap<>();
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(alunos);
			
			allParameters.put("FichaAluno", 	   "/var/local/ecosistema/repositorio/relatorios/escola/ficha_aluno.jasper");
//			allParameters.put("SUBREPORT_DIR","/var/local/ecosistema/repositorio/relatorios/escola/");
			allParameters.put("subReport","/var/local/ecosistema/repositorio/relatorios/escola/ficha_aluno_filiacao.jasper");
													
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.size());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getNome());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getFichas().size());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getFichas().get(0).getNome());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getFichas().get(0).getFiliacao().size());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getFichas().get(0).getFiliacao().get(0).getNome());
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>... "+alunos.get(0).getFichas().get(0).getFiliacao().get(1).getNome());
			
			JasperPrint print = JasperFillManager.fillReport(inputStream,allParameters,dataSource);
			return JasperExportManager.exportReportToPdf(print);
						
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
