package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Cargo;
import br.com.insideincloud.brainstormapp.api.model.view.CargoInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.CargoView;
import br.com.insideincloud.brainstormapp.api.repository.Cargos;
import br.com.insideincloud.brainstormapp.api.repository.CargosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.CargosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/cargos")
public class CargoResource {
	@Autowired 
	private Cargos cargos;
	
	@Autowired
	private CargosView cargosView;
	
	@Autowired
	private CargosInativosView cargosInativosView;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CARGO_CONTEUDO')")
	public RetornoWrapper<CargoView> conteudo() {
		RetornoWrapper<CargoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(cargosView.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CARGO_CONTEUDO')")
	public RetornoWrapper<CargoInativoView> conteudoview() {
		RetornoWrapper<CargoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(cargosInativosView.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CARGO_SALVAR')")
	public RetornoWrapper<Cargo> salvar(@RequestBody Cargo cargo, HttpServletResponse response) {
		RetornoWrapper<Cargo> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			cargo = cargos.saveAndFlush(cargo);
			retorno.setSingle(cargo);
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível salvar o cargo, favor verificar suas informações e tentar novamente!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}		
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(cargo.getCargo_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CARGO_CONTEUDO')")
	public RetornoWrapper<Cargo> buscaPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Cargo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(cargos.findOne(codigo));			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar a informação solicitada, favor tentar novamente dentro de alguns instantes");
			exception.setMensagem(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}
