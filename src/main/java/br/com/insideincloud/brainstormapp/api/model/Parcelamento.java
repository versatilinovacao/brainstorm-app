package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="parcelamento",schema="financeiro")
public class Parcelamento implements Serializable {
	private static final long serialVersionUID = -6763598149944410299L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long parcelamento_id;
	private String nome;
	
	public Long getParcelamento_id() {
		return parcelamento_id;
	}
	public void setParcelamento_id(Long parcelamento_id) {
		this.parcelamento_id = parcelamento_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parcelamento_id == null) ? 0 : parcelamento_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parcelamento other = (Parcelamento) obj;
		if (parcelamento_id == null) {
			if (other.parcelamento_id != null)
				return false;
		} else if (!parcelamento_id.equals(other.parcelamento_id))
			return false;
		return true;
	}

}
