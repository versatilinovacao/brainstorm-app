package br.com.insideincloud.brainstormapp.api.repository.filter;

public class MatriculaAlunoFilterView {
	private String curso_id;

	public String getCurso_id() {
		return curso_id;
	}

	public void setCurso_id(String curso_id) {
		this.curso_id = curso_id;
	}

}
