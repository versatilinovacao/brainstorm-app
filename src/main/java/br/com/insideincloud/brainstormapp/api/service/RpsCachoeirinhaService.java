//https://sourceforge.net/projects/jnfe/
//javac.com.br/jc/posts/list/1676-assinar-pdf-digitalmente-usando-certificado-a1.page
package br.com.insideincloud.brainstormapp.api.service;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;

//import org.demoiselle.signer.core.keystore.loader.implementation.FileSystemKeyStoreLoader;
import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Contato;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.CpfCnpj;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Endereco;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.EnviarLoteRpsEnvio;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Id;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.IdConverter;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.IdentificacaoRps;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.IdentificacaoTomador;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.InfRps;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.LoteRps;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Prestador;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Rps;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Servico;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Tomador;
import br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha.Valores;

import br.gov.frameworkdemoiselle.certificate.keystore.loader.implementation.FileSystemKeyStoreLoader;
import br.gov.frameworkdemoiselle.certificate.signer.SignerAlgorithmEnum;
import br.gov.frameworkdemoiselle.certificate.signer.factory.PKCS7Factory;
import br.gov.frameworkdemoiselle.certificate.signer.pkcs7.PKCS7Signer;

@Service
public class RpsCachoeirinhaService {
	private XStream stream = new XStream(new DomDriver());
	private PrivateKey privateKey;
	private KeyInfo keyInfo;  
	
	public void enviar() {
		
	}
	
	public void loadCertificado(String certificado, String senha, XMLSignatureFactory signature) {
		try {
			InputStream entrada = new FileInputStream(certificado);
			KeyStore ks = KeyStore.getInstance("pkcs12");
			ks.load(entrada,senha.toCharArray());
			
			KeyStore.PrivateKeyEntry pkEntry = null;
			Enumeration<String> aliasesEnum = ks.aliases();
			while (aliasesEnum.hasMoreElements()) {
				String alias = (String) aliasesEnum.nextElement();
				if (ks.isKeyEntry(alias)) {
					pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias, 
							new KeyStore.PasswordProtection(senha.toCharArray()));
							privateKey = pkEntry.getPrivateKey();
							break;
				}				
			}
			
			X509Certificate cert = (X509Certificate) pkEntry.getCertificate();
			KeyInfoFactory keyInfoFactory = signature.getKeyInfoFactory();
			List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
			
			x509Content.add(cert);
			X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
			keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableEntryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private ArrayList<Transform> signatureFactory(XMLSignatureFactory signatureFactory) throws NoSuchAlgorithmException,InvalidAlgorithmParameterException {
		ArrayList<Transform> transformList = new ArrayList<Transform>();
		TransformParameterSpec tps = null;
		Transform envelopedTransform = signatureFactory.newTransform(Transform.ENVELOPED, tps);
		Transform c14NTransform = signatureFactory.newTransform("http://www.w3.org/TR/2001/REC-xml-c14n-20010315", tps);
		transformList.add(envelopedTransform);
		transformList.add(c14NTransform);
		
		return transformList;
	}
	
	public void loadCertificates(String certificado, String senha, XMLSignatureFactory signatureFactory) throws FileNotFoundException, KeyStoreException, NoSuchAlgorithmException, UnrecoverableEntryException {
		certificado = "/var/local/ecosistema/tmp/insideincloud_certificado.pfx";
		senha = "1234";
		
		System.out.println("ENTREI AQUI.......");
		
		InputStream entrada = new FileInputStream(certificado);
		KeyStore ks = KeyStore.getInstance("pkcs12");
		try {
			ks.load(entrada,senha.toCharArray());
		} catch (NoSuchAlgorithmException | CertificateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		KeyStore.PrivateKeyEntry pkEntry = null;
		Enumeration<String> aliasesEnum = ks.aliases();
		while (aliasesEnum.hasMoreElements()) {
			String alias = (String) aliasesEnum.nextElement();
			if (ks.isKeyEntry(alias)) {
				pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(alias, 
							new KeyStore.PasswordProtection(senha.toCharArray()));
				privateKey = pkEntry.getPrivateKey();
				break;
			}
		}
		
		System.out.println(">>>>>>>>>>>>>>>>>>"+privateKey+"<<<<<<<<<<<<<<<<<<<<<<");
		
		X509Certificate cert = (X509Certificate) pkEntry.getCertificate();
		
		KeyInfoFactory keyInfoFactory = signatureFactory.getKeyInfoFactory();
		List<X509Certificate> x509Content = new ArrayList<X509Certificate>();
		
		x509Content.add(cert);
		X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
		keyInfo = keyInfoFactory.newKeyInfo(Collections.singletonList(x509Data));
		
	}
	
	public void assinatura(String xmlArquivo, String autenticacao) throws UnrecoverableKeyException,KeyStoreException,NoSuchAlgorithmException,IOException {
		String senha = autenticacao;

		//FileSystemKeyStoreLoader loader = (FileSystemKeyStoreLoader) KeyStoreLoaderFactory.factoryKeyStoreLoader(new File("/var/local/ecosistema/tmp/insideincloud_certificado.p12"));
		//KeyStoreLoader loader =  (KeyStoreLoader) KeyStoreLoaderFactory.factoryKeyStoreLoader(new File("/var/local/ecosistema/tmp/insideincloud_certificado.p12"));

		KeyStore keystore = (new FileSystemKeyStoreLoader(new File("/var/local/ecosistema/tmp/insideincloud_certificado.p12"))).getKeyStore("ckldfvq88536");
		System.out.println("Senha "+keystore.aliases().nextElement().toString());
		System.out.println("Senha "+keystore.aliases().nextElement().toString());

		String alias = "";
		PrivateKey pk = (PrivateKey) keystore.getKey(alias, senha.toCharArray());
		Certificate[] chain = keystore.getCertificateChain(alias);
		
		String arquivo = "";
		FileReader file = new FileReader(new File(xmlArquivo));
		BufferedReader buffer = new BufferedReader(file);
		while (buffer.ready()) {
			String linha = buffer.readLine();
			arquivo = arquivo + linha;
		}
		buffer.close();
		file.close();
		byte[] content = arquivo.getBytes();
		
		PKCS7Signer signer = PKCS7Factory.getInstance().factoryDefault();
		
		signer.setCertificates(chain);
		signer.setPrivateKey(pk);
		signer.setAttached(true);
		
		signer.setAlgorithm(SignerAlgorithmEnum.SHA256withRSA);

		byte[] signed = signer.signer(content);
		signer.check(content, signed);
		System.out.println(">>>>>>>>>>>>>>>> "+chain);
		
		
		BufferedOutputStream xmlAssinado = null;
		FileOutputStream xmlOriginal = new FileOutputStream(xmlArquivo);
		xmlAssinado = new BufferedOutputStream(xmlOriginal);
		xmlAssinado.write(signed);
		xmlAssinado.close();
		
	}
	
	public void gerarXML() throws FileNotFoundException {
				
		EnviarLoteRpsEnvio enviarLoteRpsEnvio = new EnviarLoteRpsEnvio();
		
		Id id = new Id("L13032509575798");
		
		LoteRps loteRps = new LoteRps();
		loteRps.setNumeroLote("0000001");
		loteRps.setCnpj("99.999.999/0009-99");
		loteRps.setInscricaoMunicipal("ISENTO");
		loteRps.setQuantidadeRps(1);
		loteRps.setId(id); 
		
		CpfCnpj cpfCnpj = new CpfCnpj();
		cpfCnpj.setCpf("11111111111");
		
		IdentificacaoTomador identificacaoTomador = new IdentificacaoTomador();
		identificacaoTomador.setCpfCnpj(cpfCnpj);
		
		Endereco endereco = new Endereco();
		endereco.setEndereco("TESTE ENDERECO");
		endereco.setNumero("123");
		endereco.setComplemento("321");
		endereco.setBairro("TESTE BAIRRO");
		endereco.setCodigoMunicipio("4303103");
		endereco.setUf("RS");
		endereco.setCep("90990990");
		
		Contato contato = new Contato();
		contato.setTelefone("05199999999");
		contato.setEmail("teste@teste.com.br");
		
		Tomador tomador = new Tomador();
		tomador.setIdentificacaoTomador(identificacaoTomador);
		tomador.setEndereco(endereco);
		tomador.setContato(contato);
		
		Prestador prestador = new Prestador();
		prestador.setCnpj("11111110000199");
		prestador.setInscricaoMunicipal("000000000111111");
		
		Valores valor = new Valores();
		valor.setValorServicos("10.00");
		valor.setIssRetido("2");
		valor.setValorIss("0.25");
		valor.setBaseCalculo("10.00");
		valor.setAliquota("0.025");
		valor.setValorLiquidoNfse("10.00");
		
		List<Valores> valores = new ArrayList<Valores>();
		valores.add(valor);
		
		Servico servico = new Servico();
		servico.setValores(valores);
		servico.setItemListaServico("101");
		servico.setCodigoCnae("111301");
		servico.setDiscriminacao("Teste discriminacao servicos");
		servico.setCodigoMunicipio("4303103");
		
		IdentificacaoRps identificacaoRps = new IdentificacaoRps();
		identificacaoRps.setNumero("13032509574052");
		identificacaoRps.setSerie("NFOF");
		identificacaoRps.setTipo("2");
		
		InfRps infRps = new InfRps();
		infRps.setIdentificacaoRps(identificacaoRps);
		infRps.setDataEmissao("2013-03-25T09:56:23.703125-03:00");
		infRps.setNaturezaOperacao("52");
		infRps.setOptanteSimplesNacional("2");
		infRps.setIncentivadorCultural("2");
		infRps.setStatus("1");
		infRps.setServico(servico);
		infRps.setPrestador(prestador);
		infRps.setTomador(tomador);
		infRps.setId(new Id("R130325095740511"));
		
		List<Rps> listaRps = new ArrayList<Rps>();
		Rps rps = new Rps();
		rps.setInfRps(infRps);
		listaRps.add(rps);
		
		loteRps.setListaRps(listaRps); 
		
		enviarLoteRpsEnvio.setLoteRps(loteRps);
		
		stream.alias("EnviarLoteRpsEnvio", EnviarLoteRpsEnvio.class);
		stream.alias("LoteRps", LoteRps.class);
		stream.alias("Rps", Rps.class);
		stream.alias("Valores", Valores.class);
		stream.useAttributeFor(LoteRps.class,"Id");
		stream.useAttributeFor(InfRps.class,"Id");
		stream.aliasField("Id", LoteRps.class, "Id");
		stream.aliasField("Id", InfRps.class, "Id");
		stream.setMode(XStream.NO_REFERENCES);
		stream.registerConverter(new IdConverter());
		
		PrintStream out = new PrintStream(new File("/var/local/ecosistema/tmp/loteRPS_16072019.xml"));
		out.println(stream.toXML(enviarLoteRpsEnvio));
		
		try {
			assinatura("/var/local/ecosistema/tmp/loteRPS_16072019.xml","ckldfvq88536");
			enviar();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
