package br.com.insideincloud.brainstormapp.api.report.caderno;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.CapaCaderno;
import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.repository.CapasCadernos;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
//import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.util.JRLoader;

@Component
public class CapasCadernoSinteticaReport {
	
	public byte[] Report(List<CapaCaderno> capas) throws Exception {
		
		
		InputStream inputStream = this.getClass().getResourceAsStream("capacadernosintetico.jasper");
		
//		final JasperReport report = (JasperReport) JRLoader.loadObject(Thread.currentThread()
//													 						 .getContextClassLoader()
//													 						 .getResourceAsStream("/capacadernosintetico.jasper"));
		
		Map<String,Object> allParameters = new HashMap<>();
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(capas);
		
		JasperPrint print = JasperFillManager.fillReport(inputStream, allParameters,dataSource);
		return JasperExportManager.exportReportToPdf(print);
		
	}

}
