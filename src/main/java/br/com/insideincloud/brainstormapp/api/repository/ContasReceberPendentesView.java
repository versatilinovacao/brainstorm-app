package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaReceberPK;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberPendenteView;

@Repository
public interface ContasReceberPendentesView extends JpaRepository<ContaReceberPendenteView, ContaReceberPK> {

}
