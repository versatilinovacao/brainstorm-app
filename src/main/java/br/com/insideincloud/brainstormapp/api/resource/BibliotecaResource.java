package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.view.DataHoraView;
import br.com.insideincloud.brainstormapp.api.repository.DatasHorasView;

@RestController
@RequestMapping("/biblioteca")
public class BibliotecaResource {
	@Autowired
	private DatasHorasView datasHoras;

	@GetMapping("/data_hora")
	public List<DataHoraView> dataHora() {
		return datasHoras.findAll();
	}
}
