package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

import java.util.List;

public class LoteRps {
	private Id Id;
	private String NumeroLote;
	private String Cnpj;
	private String InscricaoMunicipal;
	private Integer QuantidadeRps;
	private List<Rps> ListaRps;
	
	public Id getId() {
		return Id;
	}
	public void setId(Id id) {
		Id = id;
	}
	public List<Rps> getListaRps() {
		return ListaRps;
	}
	public void setListaRps(List<Rps> listaRps) {
		ListaRps = listaRps;
	}
	public String getNumeroLote() {
		return NumeroLote;
	}
	public void setNumeroLote(String numeroLote) {
		NumeroLote = numeroLote;
	}
	public String getCnpj() {
		return Cnpj;
	}
	public void setCnpj(String cnpj) {
		Cnpj = cnpj;
	}
	public String getInscricaoMunicipal() {
		return InscricaoMunicipal;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		InscricaoMunicipal = inscricaoMunicipal;
	}
	public Integer getQuantidadeRps() {
		return QuantidadeRps;
	}
	public void setQuantidadeRps(Integer quantidadeRps) {
		QuantidadeRps = quantidadeRps;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Cnpj == null) ? 0 : Cnpj.hashCode());
		result = prime * result + ((InscricaoMunicipal == null) ? 0 : InscricaoMunicipal.hashCode());
		result = prime * result + ((NumeroLote == null) ? 0 : NumeroLote.hashCode());
		result = prime * result + ((QuantidadeRps == null) ? 0 : QuantidadeRps.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoteRps other = (LoteRps) obj;
		if (Cnpj == null) {
			if (other.Cnpj != null)
				return false;
		} else if (!Cnpj.equals(other.Cnpj))
			return false;
		if (InscricaoMunicipal == null) {
			if (other.InscricaoMunicipal != null)
				return false;
		} else if (!InscricaoMunicipal.equals(other.InscricaoMunicipal))
			return false;
		if (NumeroLote == null) {
			if (other.NumeroLote != null)
				return false;
		} else if (!NumeroLote.equals(other.NumeroLote))
			return false;
		if (QuantidadeRps == null) {
			if (other.QuantidadeRps != null)
				return false;
		} else if (!QuantidadeRps.equals(other.QuantidadeRps))
			return false;
		return true;
	}
	
}
