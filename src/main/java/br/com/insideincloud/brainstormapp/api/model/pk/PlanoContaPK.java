package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PlanoContaPK implements Serializable {
	private static final long serialVersionUID = 3051514187309645650L;
	
	private Long planoconta_id;
	private Long composicao_id;
	
	public Long getPlanoconta_id() {
		return planoconta_id;
	}
	public void setPlanoconta_id(Long planoconta_id) {
		this.planoconta_id = planoconta_id;
	}
	public Long getComposicao_id() {
		if (composicao_id == null) { composicao_id = 0l; }
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicao_id == null) ? 0 : composicao_id.hashCode());
		result = prime * result + ((planoconta_id == null) ? 0 : planoconta_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoContaPK other = (PlanoContaPK) obj;
		if (composicao_id == null) {
			if (other.composicao_id != null)
				return false;
		} else if (!composicao_id.equals(other.composicao_id))
			return false;
		if (planoconta_id == null) {
			if (other.planoconta_id != null)
				return false;
		} else if (!planoconta_id.equals(other.planoconta_id))
			return false;
		return true;
	}
}
