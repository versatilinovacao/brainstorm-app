package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="gruposecurity",schema="public")
public class GrupoSecurity implements Serializable {
	private static final long serialVersionUID = -3623131809577254242L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long gruposecurity_id;
	private String nome;
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)  //, cascade=CascadeType.ALL
	@JoinTable(name="gruposecurity_telasecurity",joinColumns=@JoinColumn(name="gruposecurity_id"),inverseJoinColumns=@JoinColumn(name="telasecurity_id"))
	private List<TelaSecurity> telas;
	
	public List<TelaSecurity> getTelas() {
		return telas;
	}
	public void setTelas(List<TelaSecurity> telas) {
		this.telas = telas;
	}
	public Long getGruposecurity_id() {
		return gruposecurity_id;
	}
	public void setGruposecurity_id(Long gruposecurity_id) {
		this.gruposecurity_id = gruposecurity_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gruposecurity_id == null) ? 0 : gruposecurity_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoSecurity other = (GrupoSecurity) obj;
		if (gruposecurity_id == null) {
			if (other.gruposecurity_id != null)
				return false;
		} else if (!gruposecurity_id.equals(other.gruposecurity_id))
			return false;
		return true;
	}
	
}
