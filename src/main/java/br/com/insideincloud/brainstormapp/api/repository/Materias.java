package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Materia;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaFilter;
import br.com.insideincloud.brainstormapp.api.repository.materia.MateriasQuery;

@Repository
public interface Materias extends JpaRepository<Materia, Long>, MateriasQuery {
	public Page<Materia> filtrar(MateriaFilter filtro, Pageable page);

	@Query("select m from Materia m where m.materia_id != ?1")
	public List<Materia> findByMateriasNot(Long materia_id);
}
