package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.Porte;
import br.com.insideincloud.brainstormapp.api.repository.Portes;

@RestController
@RequestMapping("/portesempresas")
public class PorteResouce {

	@Autowired
	private Portes portes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PORTESEMPRESAS_CONTEUDO')")
	public List<Porte> conteudo() {
		
		return portes.findAll();
	}
	
}
