
package br.com.insideincloud.brainstormapp.api.soap.producao.cachoeirinha;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import br.com.insideincloud.brainstormapp.api.soap.producao.cachoeirinha.entidade.xsd.DadosCadastrais;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://entidade.server.nfse.thema.inf.br/xsd}DadosCadastrais" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "retornarDadosCadastraisResponse")
public class RetornarDadosCadastraisResponse {

    @XmlElementRef(name = "return", namespace = "http://server.nfse.thema.inf.br", type = JAXBElement.class, required = false)
    protected JAXBElement<DadosCadastrais> _return;

    /**
     * Obtém o valor da propriedade return.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DadosCadastrais }{@code >}
     *     
     */
    public JAXBElement<DadosCadastrais> getReturn() {
        return _return;
    }

    /**
     * Define o valor da propriedade return.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DadosCadastrais }{@code >}
     *     
     */
    public void setReturn(JAXBElement<DadosCadastrais> value) {
        this._return = value;
    }

}
