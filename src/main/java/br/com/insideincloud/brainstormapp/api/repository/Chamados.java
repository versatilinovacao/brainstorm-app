package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Chamado;
import br.com.insideincloud.brainstormapp.api.repository.chamado.ChamadosQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadoFilter;

@Repository
public interface Chamados extends JpaRepository<Chamado,Long>, ChamadosQuery {
	public Page<Chamado> filtrar(ChamadoFilter filtro, Pageable page); 
}
