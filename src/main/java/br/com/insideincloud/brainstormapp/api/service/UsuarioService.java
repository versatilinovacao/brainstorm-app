package br.com.insideincloud.brainstormapp.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.model.Permissao;
import br.com.insideincloud.brainstormapp.api.model.Usuario;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurity;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;

@Service
public class UsuarioService {
	@Autowired
	private Usuarios usuarios;
	
	@Autowired
	private GruposSecurity grupos;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private ColaboradorService colaboradorService;
	
	public Usuario salvar(Usuario usuario) throws javax.persistence.RollbackException {
		List<Permissao> permissoes = new ArrayList<>();
		Colaborador colaborador;

		if (usuario.getColaborador() == null) { 
			colaborador = new Colaborador();	
			colaborador.setColaborador_id(colaboradorService.request_Id(colaborador.getColaborador_id()));

		} else {
			if (usuario.getColaborador().getColaborador_id() != null && usuario.getColaborador().getColaborador_id().getColaborador_id() > 0) {
				ColaboradorPK id = new ColaboradorPK();
				id.setColaborador_id(usuario.getColaborador().getColaborador_id().getColaborador_id());
				id.setComposicao_id(usuario.getColaborador().getColaborador_id().getComposicao_id());
				
				colaborador = colaboradores.findOne(id);
			} else {
				colaborador = new Colaborador();			
			}			
		}
		
		colaborador.setNome(usuario.getNomecompleto());
		
		usuario.setColaborador(colaborador);
		
		if (usuario.getGrupos() != null) {
			usuario.getGrupos().forEach(conteudo -> {
				GrupoSecurity grupo = grupos.findOne(conteudo.getGruposecurity_id());
				if (grupo.getTelas() != null) {
					grupo.getTelas().forEach(tela -> {
						if (tela.getPermissoes() != null) {
							tela.getPermissoes().forEach(permissao -> {
								permissoes.add(permissao);
								
							});
						}
					});					
				}
				
			});		
		}
		if (!permissoes.isEmpty()) {
			List<Permissao> p = permissoes.stream().distinct().collect(Collectors.toList());
			usuario.setPermissoes(p);
		}
		
		
		
		if (usuario.getSenha() == null || usuario.getSenha() == "") {
			usuario.setSenhaUpdate( usuarios.findOne(usuario.getUsuario_id()).getSenha() );			
		}

		try {
			if (usuario.getSenha() == null || usuario.getSenha() == "") { throw new Exception("Não foi possível salvar usuário sem senha!"); }
			usuario = usuarios.saveAndFlush(usuario);
			usuario.getGrupos().forEach( conteudo -> {
				System.out.println("GRUPOS ---------------------> "+conteudo.getNome());
			} );
//			usuario.getPermissoes().forEach(conteudo -> {
//				System.out.println("PERMISSÃO ------------------> "+conteudo.getNome());
//			});
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return usuario;
	}
}

/*
 * 
 * 
 */