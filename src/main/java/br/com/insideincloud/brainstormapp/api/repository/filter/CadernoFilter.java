package br.com.insideincloud.brainstormapp.api.repository.filter;

public class CadernoFilter {
	private String caderno_id;
	private String descricao;
	private String turma_id;
	private String professor_id;
	private String conceito;
	private String nota;
	
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(String professor_id) {
		this.professor_id = professor_id;
	}
	

}
