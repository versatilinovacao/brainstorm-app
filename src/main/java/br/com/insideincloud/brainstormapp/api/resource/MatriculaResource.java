package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.DeletarMatriculaAction;
import br.com.insideincloud.brainstormapp.api.model.Matricula;
import br.com.insideincloud.brainstormapp.api.model.SituacaoMatricula;
import br.com.insideincloud.brainstormapp.api.model.view.AlunoView;
import br.com.insideincloud.brainstormapp.api.model.view.CursoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaInativaView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosMatriculadosView;
import br.com.insideincloud.brainstormapp.api.repository.AlunosView;
import br.com.insideincloud.brainstormapp.api.repository.Cursos;
import br.com.insideincloud.brainstormapp.api.repository.CursosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.DeletarMatriculasAction;
import br.com.insideincloud.brainstormapp.api.repository.Matriculas;
import br.com.insideincloud.brainstormapp.api.repository.MatriculasAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.MatriculasInativasView;
import br.com.insideincloud.brainstormapp.api.repository.SituacoesMatriculas;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoFilterView;
import br.com.insideincloud.brainstormapp.api.resource.delete.MatriculaDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/matriculas")
public class MatriculaResource {
	@Autowired
	private AlunosMatriculadosView alunosmatriculados;
	
	@Autowired
	private Matriculas matriculas;
	
	@Autowired
	private AlunosView alunosView;
	
	@Autowired
	private Cursos cursos;
	
	@Autowired
	private SituacoesMatriculas situacoesMatriculas;
	
	@Autowired
	private DeletarMatriculasAction deletarMatriculas;
	
	@Autowired
	private MatriculasAtivasView matriculasAtivas;
	
	@Autowired
	private MatriculasInativasView matriculasInativas;
	
	@Autowired
	private CursosAtivosView cursosAtivos;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<MatriculaAlunoView> conteudo(@RequestBody MatriculaAlunoFilterView filtro) {
		RetornoWrapper<MatriculaAlunoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		alunosmatriculados.findAll(page)
		try {
			retorno.setConteudo( alunosmatriculados.findByAlunosMatriculadosPorCurso( Long.parseLong(filtro.getCurso_id()) ) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os alunos matriculados no curso, estamos temporariamente indisponíveis, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<MatriculaAtivaView> conteudo() {
		RetornoWrapper<MatriculaAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		matriculas.filtrar(filtro, page)
		try {
			retorno.setConteudo( matriculasAtivas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as matriculas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	};
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<MatriculaInativaView> conteudoInativo() {
		RetornoWrapper<MatriculaInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		matriculas.filtrar(filtro, page)
		try {
			retorno.setConteudo( matriculasInativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as matriculas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	};	
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<Matricula> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Matricula> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( matriculas.findOne(codigo) );
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar a matricula, favor tentar novamente dentro de alguns instantes;");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_SALVAR')")
	public RetornoWrapper<Matricula> salvar(@RequestBody Matricula matricula, HttpServletResponse response) {
		RetornoWrapper<Matricula> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		matricula = matriculas.save(matricula);
		retorno.setSingle(matricula);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(matricula.getMatricula_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@GetMapping("/alunos/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<AlunoView> conteudoAlunos(@PathVariable Long codigo) {
		RetornoWrapper<AlunoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		alunosView.filtrar(filtro, page)
		try {
//			retorno.setConteudo( alunosView.findAll() );
			retorno.setConteudo( alunosView.findByCurso(codigo) );			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os Alunos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/cursos")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<CursoAtivoView> conteudoCursos() {
		RetornoWrapper<CursoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		cursos.filtrar(filtro, page)
		try {
			retorno.setConteudo( cursosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os cursos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/situacoes")
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_CONTEUDO')")
	public RetornoWrapper<SituacaoMatricula> conteudoSituacoes() {
		RetornoWrapper<SituacaoMatricula> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(situacoesMatriculas.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as situações da matricula, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}		
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_MATRICULA_DELETAR')")
	public Page<Matricula> deletar(@RequestBody MatriculaDelete matricula, @PageableDefault(size=10) Pageable page) {
		

		for (Matricula conteudo : matricula.getMatriculas()) {
			DeletarMatriculaAction deletarMatricula = new DeletarMatriculaAction();
			deletarMatricula.setMatricula_id(conteudo.getMatricula_id());
			
			deletarMatriculas.save(deletarMatricula);
			
		};
				
		return matriculas.findAll(page);
	}
	
}
