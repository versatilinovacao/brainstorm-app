package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="produtos",schema="recurso")
public class Produto implements Serializable {
	private static final long serialVersionUID = 5150089024520928720L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long produto_id;
	@OneToOne
	@JoinColumn(name="produto_composto_id")
	private Produto produto_composto;
	@Column(name="nome",length=60)
	private String nome;
	@Column(name="descricao",length=200)
	private String descricao;
	@OneToOne
	@JoinColumn(name="seguimento_id")
	private Seguimento seguimento;
	@OneToOne
	@JoinColumn(name="familia_id")
	private Familia familia;
	@OneToOne
	@JoinColumn(name="classe_id")
	private Classe classe;
	@OneToOne
	@JoinColumn(name="subclasse_id")
	private SubClasse subclasse;
	@OneToOne
	@JoinColumn(name="marca_id")
	private Marca marca;
	@OneToOne
	@JoinColumn(name="unidades_medidas")
	private UnidadeMedida unidademedida;
	@OneToOne
	@JoinColumn(name="finalidade_id")
	private Finalidade finalidade;
	@OneToOne
	@JoinColumn(name="producao_id")
	private Producao producao;
	@OneToOne
	@JoinColumn(name="tipo_id")
	private TipoProduto tipo;
	@OneToOne
	@JoinColumn(name="estoque_id")
	private Estoque estoque;
	@OneToOne
	@JoinColumn(name="tipo_material_id")
	private TipoMaterial tipomaterial;
	@OneToOne
	@JoinColumn(name="modelo_id")
	private ModeloProduto modelo;
	@OneToOne
	@JoinColumn(name="grupo_id")
	private GrupoProduto grupo;
	
	
	public GrupoProduto getGrupo() {
		return grupo;
	}
	public void setGrupo(GrupoProduto grupo) {
		this.grupo = grupo;
	}
	public ModeloProduto getModelo() {
		return modelo;
	}
	public void setModelo(ModeloProduto modelo) {
		this.modelo = modelo;
	}
	public TipoMaterial getTipomaterial() {
		return tipomaterial;
	}
	public void setTipomaterial(TipoMaterial tipomaterial) {
		this.tipomaterial = tipomaterial;
	}
	public TipoProduto getTipo() {
		return tipo;
	}
	public void setTipo(TipoProduto tipo) {
		this.tipo = tipo;
	}
	public Estoque getEstoque() {
		return estoque;
	}
	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}
	public Producao getProducao() {
		return producao;
	}
	public void setProducao(Producao producao) {
		this.producao = producao;
	}
	public Finalidade getFinalidade() {
		return finalidade;
	}
	public void setFinalidade(Finalidade finalidade) {
		this.finalidade = finalidade;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public UnidadeMedida getUnidademedida() {
		return unidademedida;
	}
	public void setUnidademedida(UnidadeMedida unidademedida) {
		this.unidademedida = unidademedida;
	}
	public Long getProduto_id() {
		return produto_id;
	}
	public void setProduto_id(Long produto_id) {
		this.produto_id = produto_id;
	}
	public Produto getProduto_composto() {
		return produto_composto;
	}
	public void setProduto_composto(Produto produto_composto) {
		this.produto_composto = produto_composto;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Seguimento getSeguimento() {
		return seguimento;
	}
	public void setSeguimento(Seguimento seguimento) {
		this.seguimento = seguimento;
	}
	public Familia getFamilia() {
		return familia;
	}
	public void setFamilia(Familia familia) {
		this.familia = familia;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	public SubClasse getSubclasse() {
		return subclasse;
	}
	public void setSubclasse(SubClasse subclasse) {
		this.subclasse = subclasse;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((produto_id == null) ? 0 : produto_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (produto_id == null) {
			if (other.produto_id != null)
				return false;
		} else if (!produto_id.equals(other.produto_id))
			return false;
		return true;
	}
	
	
}
