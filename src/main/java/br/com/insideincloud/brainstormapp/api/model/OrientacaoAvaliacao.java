package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="orientacoes_avaliacoes",schema="escola")
public class OrientacaoAvaliacao implements Serializable {
	private static final long serialVersionUID = 8845266129807741655L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long orientacao_id;
	@OneToOne
	@ManyToOne
	@JoinColumn(name="avaliacaoalunoatividade_id")
//	@JsonInclude(Include.NON_NULL)
	private AvaliacaoAlunoAtividade avaliacao;
	private String descricao;
	
	public Long getOrientacao_id() {
		return orientacao_id;
	}
	public void setOrientacao_id(Long orientacao_id) {
		this.orientacao_id = orientacao_id;
	}
	public AvaliacaoAlunoAtividade getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(AvaliacaoAlunoAtividade avaliacao) {
		this.avaliacao = avaliacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orientacao_id == null) ? 0 : orientacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrientacaoAvaliacao other = (OrientacaoAvaliacao) obj;
		if (orientacao_id == null) {
			if (other.orientacao_id != null)
				return false;
		} else if (!orientacao_id.equals(other.orientacao_id))
			return false;
		return true;
	}

}
