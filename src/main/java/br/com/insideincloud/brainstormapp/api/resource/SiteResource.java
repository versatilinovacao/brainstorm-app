package br.com.insideincloud.brainstormapp.api.resource;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Cardapio;
import br.com.insideincloud.brainstormapp.api.model.CardapioSemana;
import br.com.insideincloud.brainstormapp.api.model.view.CardapioAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CardapioInativoView;
import br.com.insideincloud.brainstormapp.api.repository.Cardapios;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosSemanas;
import br.com.insideincloud.brainstormapp.api.resource.config.RegistreConfigModel;
import br.com.insideincloud.brainstormapp.api.resource.config.Semana;
import br.com.insideincloud.brainstormapp.api.resource.config.SiteConfigModel;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/sites")
public class SiteResource {
	private int semanaCount = 1;
	private int semanaCorrente = 0;
	
//	@Autowired
//	private SecaoSiteService registro;
	
	@Autowired
	private static SiteConfigModel configModel;
	
	@Autowired
	private CardapiosAtivosView cardapiosAtivos;
	
	@Autowired
	private CardapiosInativosView cardapiosInativos;
	
	@Autowired
	private Cardapios cardapios;
	
	@Autowired
	private CardapiosSemanas semanas;
	
	@GetMapping
	public ResponseEntity<SiteConfigModel> teste(HttpServletResponse response) {
//		System.out.println("XXXXXXXXXXXXXXXXX");
		configModel = new SiteConfigModel();
		configModel.setPagina(true);
		configModel.setSistema(false);
		return ResponseEntity.ok(configModel);
	}
	
	@PutMapping("/site")
	public void site(@RequestBody RegistreConfigModel model) {
//		registro.RegistrarAcesso(ip);
//		return registro.isSiteAtivo();
		
		configModel.setPagina(true);
		configModel.setSistema(false);
	}
	
	@PutMapping("/sistema")
	@PreAuthorize("hasAuthority('ROLE_ACESSO_SISTEMA')")
	public void sistema(@RequestBody RegistreConfigModel model) {
//		registro.RegistrarAcesso(ip);
//		return registro.isSistemaAtivo();

		configModel.setPagina(false);
		configModel.setSistema(true);
		
	}
	
	@GetMapping("/cardapiosativos")
	public RetornoWrapper<CardapioAtivoView> conteudoCardapiosAtivos() {
		RetornoWrapper<CardapioAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cardapiosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum cardápio.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/cardapiosinativos")
	public RetornoWrapper<CardapioInativoView> conteudoCardapiosInativos() {
		RetornoWrapper<CardapioInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cardapiosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum cardápio.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/cardapios/{codigo_id}")
	public RetornoWrapper<Cardapio> conteudoPorId(@PathVariable Long codigo_id) {
		RetornoWrapper<Cardapio> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( cardapios.findOne(codigo_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizado o cardapio solicitado, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/cardapios/semanas/{cardapio_id}/{ano}/{mes}/{semana}")
	public RetornoWrapper<CardapioSemana> semanas(@PathVariable Long cardapio_id, @PathVariable int ano, @PathVariable int mes, @PathVariable int semana ) {
		RetornoWrapper<CardapioSemana> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
						
			Semana semanaCorrente = primeiroDia(ano,mes,semana);			
			retorno.setConteudo( semanas.findByCardapioPorSemana(semanaCorrente.getFirst(), semanaCorrente.getLast(), cardapio_id) );				
			
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações das semanas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}		
		
		return retorno;
	}
	
	private Semana primeiroDia(int ano, int mes, int semana) {
		semana = semana - 1;
		
		List<LocalDate> listaDosDiasDoMes = new ArrayList<>();
		YearMonth anoMes = YearMonth.of(ano,mes);	
		for (int x = 0 ; x<= anoMes.lengthOfMonth() -1 ; x++) {
			if (x == 0) {
				listaDosDiasDoMes.add(anoMes.atDay(1));
			} else {
				listaDosDiasDoMes.add(anoMes.atDay(x).plusDays(1));
			}
		}
		
		semanaCount = 1;
		List<Semana> semanasFind = new ArrayList<>();
		listaDosDiasDoMes.forEach(conteudo -> {			
			int plus = 0;
			if ( conteudo.getDayOfMonth() == 1 ) {
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.SUNDAY) )    { plus = 6; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.MONDAY) )    { plus = 5; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.TUESDAY) )   { plus = 4; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.WEDNESDAY) ) { plus = 3; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.THURSDAY) )  { plus = 2; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.FRIDAY) )    { plus = 1; }
				if ( conteudo.getDayOfWeek().equals(DayOfWeek.SATURDAY) )  { plus = 0; }
				if (plus == 0) {
					semanasFind.add( new Semana( semanaCount, conteudo, conteudo ) );
				} else { semanasFind.add( new Semana( semanaCount, conteudo, conteudo.plusDays( plus ) ) ); }
				
				semanaCount = semanaCount + 1;
			} else {
				if (( conteudo.getDayOfWeek().equals( DayOfWeek.SUNDAY ) ) && (conteudo.getDayOfMonth() <= conteudo.lengthOfMonth())) {
					plus = 6;
				
					if (conteudo.plusDays( plus ).getDayOfMonth() > conteudo.lengthOfMonth()) { plus = conteudo.lengthOfMonth() - conteudo.getDayOfMonth(); }
					
					semanasFind.add( new Semana( semanaCount, conteudo, conteudo.plusDays( plus ) ) );
					semanaCount = semanaCount + 1;
				}				
			}
					
		});
		
		semanaCorrente = 0;
		if (semana == -1) {
			semanasFind.forEach(conteudo -> {
				
				if (((conteudo.getFirst().isBefore(LocalDate.now())) || (conteudo.getFirst().isEqual(LocalDate.now()))) && ((conteudo.getLast().isAfter(LocalDate.now())) || (conteudo.getLast().isEqual(LocalDate.now())))) {
					 semanaCorrente = conteudo.getId();
				}
			});
		}
		
		if (LocalDate.now().getMonthValue() != mes) {
			System.out.println("ENTREI "+LocalDate.now().getDayOfMonth());
			semanaCorrente = 1;
		}
		
		if (semanaCorrente == 0) {
			return semanasFind.get(semana);
		} else {
			return semanasFind.get(semanaCorrente - 1);
		}
	}
	
}


