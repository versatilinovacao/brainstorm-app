package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.time.LocalDate;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Caderno;
import br.com.insideincloud.brainstormapp.api.model.CadernoInconsistencia;
import br.com.insideincloud.brainstormapp.api.model.CapaCaderno;
import br.com.insideincloud.brainstormapp.api.model.DeletarCadernoAction;
import br.com.insideincloud.brainstormapp.api.model.FormatoCaderno;
import br.com.insideincloud.brainstormapp.api.model.execute.CancelarCadernoAction;
import br.com.insideincloud.brainstormapp.api.model.view.AvaliacaoView;
import br.com.insideincloud.brainstormapp.api.model.view.CadernoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CadernoInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.ChamadaView;
import br.com.insideincloud.brainstormapp.api.model.view.IsCadernoInconsistenciaView;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.model.view.ResumoEmpresaView;
import br.com.insideincloud.brainstormapp.api.repository.AvaliacoesView;
import br.com.insideincloud.brainstormapp.api.repository.Cadernos;
import br.com.insideincloud.brainstormapp.api.repository.CadernosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.CadernosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.CadernosInconsistencias;
import br.com.insideincloud.brainstormapp.api.repository.CancelarCadernosAction;
import br.com.insideincloud.brainstormapp.api.repository.CapasCadernos;
import br.com.insideincloud.brainstormapp.api.repository.ChamadasView;
import br.com.insideincloud.brainstormapp.api.repository.DeletarCadernosAction;
import br.com.insideincloud.brainstormapp.api.repository.FormatosCadernos;
import br.com.insideincloud.brainstormapp.api.repository.IsCadernosInconsistenciasView;
import br.com.insideincloud.brainstormapp.api.repository.MateriasCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.ResumosEmpresasView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AvaliacaoViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoInconsistenciaFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoMesViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.IsCadernoInconsistenciaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaComposicaoFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResumoEmpresaViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.CadernoDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.retorno.RetornoFachada;
import br.com.insideincloud.brainstormapp.api.service.CadernoService;
import br.com.insideincloud.brainstormapp.api.service.caderno.MateriaComposicao;

@RestController
@RequestMapping("/cadernos")
public class CadernoResource {
	@Autowired
	private Cadernos cadernos;
	
	@Autowired
	private CadernoService cadernoService;
	
	@Autowired
	private ChamadasView chamadasview;
	
	@Autowired
	private AvaliacoesView avaliacoesView;

	@Autowired
	private DeletarCadernosAction deletarCadernos;
		
	@Autowired
	private FormatosCadernos formatosCadernos;
	
	@Autowired
	private ResumosEmpresasView resumoEmpresasView;
	
	@Autowired
	private IsCadernosInconsistenciasView inconsistencias;
	
	@Autowired
	private CadernosInconsistencias inconsistencias_find;
	
	@Autowired
	private MateriasCompostaView materias;
	
	@Autowired
	private CapasCadernos capas;
	
	@Autowired
	private CancelarCadernosAction cancelarCadernos;
	
	@Autowired
	private CadernosAtivosView cadernosAtivos;
	
	@Autowired
	private CadernosInativosView cadernosInativos;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADERNO_SALVAR')")
	public ResponseEntity<Caderno> salvar(@RequestBody Caderno caderno, HttpServletResponse response) {
		caderno = cadernos.saveAndFlush(caderno);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(caderno.getCaderno_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());

		return ResponseEntity.created(uri).body(caderno);
	}
	 
	@PostMapping("/conteudo")
	@PreAuthorize("hasAuthority('ROLE_CADERNO_SALVAR')")
	public RetornoFachada<MateriaComposicao> salvar(@RequestBody MateriaComposicao conteudo, HttpServletResponse response) {
		RetornoFachada<MateriaComposicao> retorno = cadernoService.salvar(conteudo);
		
		
		return retorno;
	}

	@PostMapping("/capasintetica")
	@PreAuthorize("hasAuthority('ROLE_CADERNO_SALVAR')")
	@Transactional
	public ResponseEntity<RetornoWrapper<CapaCaderno>> salvar(@RequestBody CapaCaderno capa, HttpServletResponse response) {
		RetornoWrapper<CapaCaderno> retorno = new RetornoWrapper<CapaCaderno>();
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(capa.getCapacaderno_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());			
		ExceptionWrapper except = new ExceptionWrapper();
		except.setCodigo(2);
		except.setMensagem("N�o foi poss�vel salvar a capa do caderno.");
		
		try {
			capa = capas.saveAndFlush(capa);
			retorno.setSingle(capa);			
		} catch (Exception e) {			
			
			System.out.println("ERRO: "+e.getStackTrace().toString());
			retorno.setException(except);
			
		} finally {
			return ResponseEntity.created(uri).body(retorno);		
		}	
		
	}	
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CADERNO_CONTEUDO')")
	public RetornoWrapper<CadernoAtivoView> conteudo() {
		RetornoWrapper<CadernoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(cadernosAtivos.findAll());
		} catch(Exception e) {
			System.out.println(e);
			exception.setCodigo(2);
			exception.setMensagem("Não foi possível retornar informações do caderno, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}		
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CADERNO_CONTEUDO')")
	public RetornoWrapper<CadernoInativoView> conteudoInativo() {
		RetornoWrapper<CadernoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cadernosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações dos cadernos inativos, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<Caderno> buscarPeloCodigo(@PathVariable Long codigo, HttpServletResponse response) {
		RetornoWrapper<Caderno> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			Caderno cadernoLocalizado = cadernos.findOne(codigo);
			retorno.setSingle(cadernoLocalizado);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(cadernoLocalizado.getCaderno_id()).toUri();
			response.setHeader("Location", uri.toASCIIString());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações do caderno, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
				
		return retorno;
	}

	@PutMapping("/chamadas")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_SALVAR')")
	public RetornoWrapper<ChamadaView> conteudo(@RequestBody ChamadaViewFilter filtro, Pageable page) {
		RetornoWrapper<ChamadaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			chamadasview.filtrar(filtro, page);	
			
//			String data = "25/01/2016";
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//			LocalDate date = LocalDate.parse(data,formatter);
			
			retorno.setConteudo(chamadasview.findByChamadas(Long.parseLong(filtro.getCaderno_id()), LocalDate.parse(filtro.getEvento()) ));
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as chamadas para este caderno e período, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno .setException(exception);
		}
		
		System.out.println("QUANTIDADE "+chamadasview.filtrar(filtro, page).getSize());
		
		return retorno;
	}
	
	@PutMapping("/avaliacoes")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_SALVAR')")
	public RetornoWrapper<AvaliacaoView> conteudo(@RequestBody AvaliacaoViewFilter filtro, Pageable page) {
		RetornoWrapper<AvaliacaoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			avaliacoesView.filtrar(filtro, page)
			retorno.setConteudo( avaliacoesView.findByAvaliacoes(Long.parseLong(filtro.getCaderno_id())) );
			if (filtro.getPeriodo_letivo_item_id() != null  && filtro.getPeriodo_letivo_item_id() != "" ) {
				retorno.setConteudo( avaliacoesView.findByAvalicoesPeriodo(Long.parseLong( filtro.getCaderno_id() ), Long.parseLong( filtro.getPeriodo_letivo_item_id() )) );
			}
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações das avaliações, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/escolas")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public Page<ResumoEmpresaView> escolas(@RequestBody ResumoEmpresaViewFilter filtro, Pageable page) {
		
		return resumoEmpresasView.filtrar(filtro,page);
	}
	
	@PutMapping("/inconsistencias")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public Page<IsCadernoInconsistenciaView> isInconsistencia(@RequestBody IsCadernoInconsistenciaViewFilter filtro, Pageable page) {
		return inconsistencias.filtrar(filtro, page);
	}
	
	@PutMapping("/inconsistencias_find")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public Page<CadernoInconsistencia> find_inconsistencia(@RequestBody CadernoInconsistenciaFilter filtro, Pageable page) {
		return inconsistencias_find.filtrar(filtro, page);
	}
	
	@PutMapping("/materiasview")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public Page<MateriaCompostaView> conteudo(@RequestBody MateriaCompostaViewFilter filtro, @PageableDefault(size = 8) Pageable page) {
		
		return materias.filtrar(filtro, page);
	}
	
	@PutMapping("/composicao")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<MateriaComposicao> conteudo(@RequestBody MateriaComposicaoFilter filtro) {
		
		return null;
	}
	
  	@DeleteMapping("/cancelar")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_DELETAR')")
	public RetornoWrapper<Cadernos> cancelar(@RequestBody CadernoDelete caderno, @PageableDefault(size=10) Pageable page) {
  		
  		for (Caderno conteudo : caderno.getCadernos()) {
  			CancelarCadernoAction cancelarCaderno = new CancelarCadernoAction();
  			cancelarCaderno.setCaderno_id(conteudo.getCaderno_id());
  			
  			cancelarCadernos.save(cancelarCaderno);
  		}
		return null;
	}
	
//	public Page<Caderno> deletar(@RequestBody CadernoDelete caderno, @PageableDefault (size=10) Pageable page) {
  	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_CADERNO_DELETAR')")
	public RetornoWrapper<Caderno> deletar(@RequestBody CadernoDelete caderno, @PageableDefault (size=10) Pageable page, HttpServletResponse response) {
		RetornoWrapper<Caderno> retorno = new RetornoWrapper<Caderno>();
		System.out.println("PASSEI AQUI");
		try {
			for (Caderno conteudo : caderno.getCadernos()) {
				DeletarCadernoAction deletarCaderno = new DeletarCadernoAction();
				deletarCaderno.setCaderno_id(conteudo.getCaderno_id());
				deletarCaderno.setDeletar(true);
				
				deletarCadernos.save(deletarCaderno);
				conteudo = null;
			}
			
			
			retorno.setPage(cadernos.findAll(page));
			
		} catch (Exception e) {
			ExceptionWrapper erro = new ExceptionWrapper();
			erro.setCodigo(1049);
			erro.setMensagem("Erro na exclusão, se desejar realmente remover este item, click no botão OK para cancelar o caderno ou use o botão CANCEL para retornar.");
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/formatos")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<FormatoCaderno> formatosCaderno() {
		RetornoWrapper<FormatoCaderno> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(formatosCadernos.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/capacaderno")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public ResponseEntity<byte[]> report() {
			byte[] relatorio = cadernoService.report(); 
		
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);
	}
	
//	@PutMapping("/frequencias")
//	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
//	public ResponseEntity<byte[]> reportFrequencia(@RequestBody ChamadaReportViewFilter filtro,Pageable page) {
//		byte[] relatorio = cadernoService.reportFrequencia(filtro,page);
//		
//		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);
//	}
	
	@PutMapping("/frequencias")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public ResponseEntity<byte[]> reportFrequencia(@RequestBody CadernoMesViewFilter filtro, Pageable page) {
		byte[] relatorio = cadernoService.reportFrequencia(filtro, page);
		
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE).body(relatorio);
	}
	
	/*
	 * Exemplo tratamento de data:
	 * public ResponseEntity<byte[]> relatorio(
	 * 		@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inicio
	 * */
	
}


/*
 * 		System.out.println("ANTES DE COME�AR A DELETAR ****************************************************************");
		GrupoCadernoDelete grupo = new GrupoCadernoDelete();
		grupo.setDeletar(false);
		grupo = grupos.saveAndFlush(grupo);
		System.out.println("ANTES DE ENTRAR NO FOR ********************************************************************");
		for (int x = 1 ; x <= caderno.getCadernos().size() ; x++) {
			System.out.println("DENTRO DO FOR ************************************************************************* "+x);
			System.out.println("ESTA VAZIO ? "+caderno.getCadernos().size());
			Caderno conteudo = caderno.getCadernos().get(x - 1);
			
			
			System.out.println("DeletarCadernoAction 1 *****************************************************************");
			DeletarCadernoAction deletarCaderno = new DeletarCadernoAction();
			deletarCaderno.setCaderno_id(conteudo.getCaderno_id());
			deletarCaderno.setGrupocadernodelete(grupo);
			System.out.println("DeletarCadernoAction 2 ***************************************************************** "+caderno.getCadernos().size());
			if (x < caderno.getCadernos().size()) {
				deletarCaderno.setDeletar(false);
				System.out.println("DeletarCadernoAction 3 ***************************************************************** "+x);
			} else {
				System.out.println("DeletarCadernoAction 4 *****************************************************************");
				deletarCaderno.setDeletar(true);
			}
			
			System.out.println("DeletarCadernoAction 5 *****************************************************************");
			deletarCadernos.save(deletarCaderno);
			
			conteudo = null;
			
		}
		
		return cadernos.findAll(page);

 * */
