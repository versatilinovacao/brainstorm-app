package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.model.Permissao;
import br.com.insideincloud.brainstormapp.api.model.Usuario;
import br.com.insideincloud.brainstormapp.api.model.view.SecurityView;
import br.com.insideincloud.brainstormapp.api.model.view.UsuarioLoginInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.UsuarioView;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurity;
import br.com.insideincloud.brainstormapp.api.repository.SecuritsView;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;
import br.com.insideincloud.brainstormapp.api.repository.UsuariosLoginInativosView;
import br.com.insideincloud.brainstormapp.api.repository.UsuariosView;
import br.com.insideincloud.brainstormapp.api.resource.delete.UsuarioDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.GrupoSecurityService;
import br.com.insideincloud.brainstormapp.api.service.SecurityService;
import br.com.insideincloud.brainstormapp.api.service.UsuarioService;

@RestController
@RequestMapping("/security")
public class SecurityResource {
	@Autowired
	private Usuarios usuarios;
	@Autowired
	private UsuariosView usuariosView;
	
	@Autowired
	private SecurityService security;
		
	@Autowired
	private SecuritsView securitsView;
	
	@Autowired
	private GruposSecurity grupossecurity;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private UsuariosLoginInativosView usuariosInativos;
	
	@Autowired
	private GrupoSecurityService grupoSecurityService;
	
	@PostMapping("/grupos")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	public RetornoWrapper<GrupoSecurity> salvar(@RequestBody GrupoSecurity grupoSecurity, HttpServletResponse response) {
		RetornoWrapper<GrupoSecurity> retorno = new RetornoWrapper<GrupoSecurity>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setSingle(grupossecurity.saveAndFlush(grupoSecurity));
		} catch (Exception e) {
			erro.setCodigo(7);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde. ");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	@Transactional
	public RetornoWrapper<GrupoSecurity> salvarSecurity(@RequestBody GrupoSecurity grupoSecurity, HttpServletResponse response) {
		RetornoWrapper<GrupoSecurity> retorno = new RetornoWrapper<GrupoSecurity>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			grupoSecurity = grupoSecurityService.salvar(grupoSecurity);
			retorno.setSingle(grupoSecurity);
		} catch (EmptyResultDataAccessException w) {
			erro.setCodigo(7);
			erro.setMensagem("Erro na base de dados.");
			erro.setDebug(""+w);
			retorno.setException(erro);
		} catch(Exception e) {
			erro.setCodigo(7);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde. "+e);
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

	
//	{"gruposecurity":{"gruposecurity_id":2},"telasistema":{"telasistema_id":4},
//		"permissoes":[{"permissao_id":5},{"permissao_id":6}]
//	}
	
//	{"gruposecurity_id":2,"nome":"ROOT","telas":[
//	                                     		{"gruposecurity":{"gruposecurity_id":2},"telasistema":{"telasistema_id":4},
//	                                     			"permissoes":[{"permissao_id":5},{"permissao_id":6}]
//	                                     		}
//	                                     	]}	
	
//	@PostMapping
//	@PreAuthorize("hasAuthority('ROLE_SECURITY_SALVAR')")
//	public RetornoWrapper<Security> salvar(@RequestBody Security security, HttpServletResponse response) {
//		RetornoWrapper<Security> retorno = new RetornoWrapper<Security>();
//		ExceptionWrapper erro = new ExceptionWrapper();
//		
//		try {
//			retorno.setSingle(securits.saveAndFlush(security));
//		} catch (Exception e) {
//			erro.setCodigo(7);
//			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde.");
//			retorno.setException(erro);
//		}
//		
//		return retorno;
//	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_SECURITY_CONTEUDO')")
	public RetornoWrapper<SecurityView> conteudoSecurity() {
		RetornoWrapper<SecurityView> retorno = new RetornoWrapper<SecurityView>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(securitsView.findAll());
		} catch (Exception e) {
			erro.setCodigo(7);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde.");
			retorno.setException(erro);			
		}
		
		return retorno;
	}
		
	@PostMapping("/usuarios")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_SALVAR')")
	@Transactional
	public RetornoWrapper<Usuario> salvar(@RequestBody Usuario usuario, HttpServletResponse response) {
		RetornoWrapper<Usuario> retorno = new RetornoWrapper<Usuario>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			usuario = usuarioService.salvar(usuario);
			
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>"+usuario.getNomecompleto());
			retorno.setSingle(usuario);
		} catch (javax.persistence.RollbackException w) {
			w.printStackTrace();
			erro.setCodigo(1);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde. ");
			erro.setDebug(""+w);
			retorno.setException(erro);
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde. ");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(usuario.getUsuario_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());

		return retorno;
	}
	
	@GetMapping("/usuarios")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_CONTEUDO')")
	public RetornoWrapper<UsuarioView> conteudo() {
		RetornoWrapper<UsuarioView> retorno = new RetornoWrapper<UsuarioView>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(usuariosView.findAll());
		} catch (Exception e) {
			erro.setCodigo(9);
			erro.setMensagem("Não foi possível retornar a informação solicitada, tente novamente.");
			retorno.setException(erro);			
		}
		
		return retorno;
	}
	
	@GetMapping("/usuarios/inativos")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_CONTEUDO')")
	public RetornoWrapper<UsuarioLoginInativoView> conteudoInativos() {
		RetornoWrapper<UsuarioLoginInativoView> retorno = new RetornoWrapper<UsuarioLoginInativoView>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		System.out.println("USUARIOS INATIVO LOGIN >>>>>>>>>>>>>>>>>>>>> ");
		
		try {
			retorno.setConteudo(usuariosInativos.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar a informação solicitada, tente novamente.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/usuarios/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_CONTEUDO')")
	public RetornoWrapper<Usuario> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Usuario> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setSingle( usuarios.findOne(codigo) );
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Ocorreu um erro na pesquisa do usuario, favor tentar novamente mais tarde!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PutMapping("/usuarios")
	@PreAuthorize("hasAuthority('ROLE_USUARIO_DELETAR')")
	public RetornoWrapper<Usuario> deletar(@RequestBody UsuarioDelete usuario) {
		RetornoWrapper<Usuario> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			for (Usuario conteudo : usuario.getUsuarios()) {
				usuarios.delete(conteudo);
			}
			
			retorno.setConteudo(usuarios.findAll());
			
		} catch (Exception e) {
			erro.setCodigo(6);
			erro.setMensagem("Ocorreu um erro ao deletar um ou mais de um dos arquivos de usuários informados."); 
		}
		
		return retorno;
	}
	
	@GetMapping("/confirmarcadastro/{codigo}")
//	@PreAuthorize("hasAuthotity('ROLE_USUARIO_CONFIRMAR')")
	public String confirmar(@PathVariable("codigo") Long codigo) {
		try {
			security.confirmarusuario(codigo);
			return "Seu cadastro foi confirmado com sucesso";
		} catch (Exception e) {
			e.printStackTrace();
			return "Ocorreu um erro";
		}
		
	}
	
	@PutMapping("/cadastre-se")
//	@PreAuthorize("hasAuthority('ROLE_CADASTRE_USUARIO')")
//	@PreAuthorize("hasAuthority('ROLE_USUARIO_CONTEUDO')")
	public ResponseEntity<Usuario> cadastrar(@RequestBody Usuario usuario, HttpServletResponse response) {
		usuario = security.cadastrar(usuario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(usuario.getUsuario_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(usuario);
	}
	
	/* Registro de Permissões */
	/*
	 * Criar modal com lista de permissões disponíveis, e permitir selecionar as que serão dadas para o usuário em questão,
	 * mas existirá uma exceção, pois vai existir um grupo de permissões diferente para cada tipo de acesso de usuário,
	 * sendo um para usuários classificados como colaboradores parceiros e outro para colaboradores não parceiros.
	 * */
	
	public Page<Permissao> permissao() {
		return null;
	}
	
}
