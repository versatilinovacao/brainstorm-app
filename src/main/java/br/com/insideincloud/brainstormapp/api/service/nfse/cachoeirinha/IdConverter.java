package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

import com.thoughtworks.xstream.converters.SingleValueConverter;

public class IdConverter implements SingleValueConverter {

	@Override
	public boolean canConvert(Class type) {
		return type.equals(Id.class);
	}

	@Override
	public String toString(Object obj) {
		// TODO Auto-generated method stub
		return ((Id)obj).getDescricao();
	}

	@Override
	public Object fromString(String descricao) {
		// TODO Auto-generated method stub
		return new Id(descricao);
	}

}
