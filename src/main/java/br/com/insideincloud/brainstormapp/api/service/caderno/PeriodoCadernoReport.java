package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.util.List;

public class PeriodoCadernoReport {
	private List<CadernoReport> cadernos;

	public List<CadernoReport> getCadernos() {
		return cadernos;
	}

	public void setCadernos(List<CadernoReport> cadernos) {
		this.cadernos = cadernos;
	}
	
}
