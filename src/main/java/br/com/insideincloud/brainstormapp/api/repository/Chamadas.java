package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Chamada;
import br.com.insideincloud.brainstormapp.api.repository.chamada.ChamadasQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaFilter;

@Repository
public interface Chamadas extends JpaRepository<Chamada,Long>,ChamadasQuery {
	public Page<Chamada> filtrar(ChamadaFilter filtro, Pageable page);

}
