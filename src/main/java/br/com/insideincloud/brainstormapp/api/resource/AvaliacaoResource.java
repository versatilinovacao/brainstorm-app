package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Avaliacao;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoCadernoView;
import br.com.insideincloud.brainstormapp.api.repository.Avaliacoes;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosCadernosView;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosItens;
import br.com.insideincloud.brainstormapp.api.resource.resumo.PeriodoLetivoItemResumo;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/avaliacoes")
public class AvaliacaoResource {

	@Autowired
	private Avaliacoes avaliacoes;
	
	@Autowired
	private PeriodosLetivosItens periodosletivositens;
	
	@Autowired
	private PeriodosLetivosCadernosView periodosletivoscadernosview;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<Avaliacao> salvar(@RequestBody Avaliacao avaliacao, HttpServletResponse response) {
		RetornoWrapper<Avaliacao> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			avaliacao = avaliacoes.saveAndFlush(avaliacao);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(avaliacao.getAvaliacao_id()).toUri();
			response.setHeader("Location", uri.toASCIIString());

			retorno.setSingle(avaliacao);
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a avaliação, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}

		return retorno;			
	}
	
	@GetMapping("/periodoscadernos/{caderno_id}")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoCadernoView> conteudo(@PathVariable Long caderno_id) {
		RetornoWrapper<PeriodoLetivoCadernoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(periodosletivoscadernosview.findByPeriodoPorCaderno(caderno_id));
			
		} catch (Exception e ) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações do periodo letivo, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/periodoatual")
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoItemResumo> periodoatual(HttpServletResponse response) {
		LocalDate hoje = LocalDate.now();

		RetornoWrapper<PeriodoLetivoItemResumo> retorno = new RetornoWrapper<>();
//		ExceptionWrapper exception = new ExceptionWrapper();
		
//		ResponseEntity<PeriodoLetivoItemResumo> retorno = null;
		List<PeriodoLetivoItem> periodos = periodosletivositens.findAll();
		for(PeriodoLetivoItem conteudo: periodos) {
			//Period.between(conteudo.getPeriodo_inicial(), conteudo.getPeriodo_final());
			System.out.println(hoje+" = "+conteudo.getPeriodo_inicial()+"------------ "+conteudo.getPeriodo_inicial().isAfter(hoje));
			if ((conteudo.getPeriodo_inicial().isBefore(hoje)) && (conteudo.getPeriodo_final().isAfter(hoje))) {
				
				retorno.setSingle(new PeriodoLetivoItemResumo(conteudo.getPeriodo_letivo_item_id(),conteudo.getDescricao()));

				URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(conteudo.getPeriodo_letivo_item_id()).toUri();
				response.setHeader("Loaction",uri.toASCIIString());
						
//				retorno = ResponseEntity.created(uri).body(new PeriodoLetivoItemResumo(conteudo.getPeriodo_letivo_item_id(),conteudo.getDescricao()));
			}
		}
		
		return retorno;
	}
	
}
