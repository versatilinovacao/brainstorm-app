package br.com.insideincloud.brainstormapp.api.resource.wrapper;

import java.util.List;

import org.springframework.data.domain.Page;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;

public class RetornoWrapper<T> {
	private T single;
	private String json;
	private Page<T> page;
	private List<T> conteudo;
	private ExceptionWrapper exception;
	
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	public List<T> getConteudo() {
		return conteudo;
	}
	public void setConteudo(List<T> conteudo) {
		this.conteudo = conteudo;
	}
	public T getSingle() {
		return single;
	}
	public void setSingle(T single) {
		this.single = single;
	}
	public Page<T> getPage() {
		return page;
	}
	public void setPage(Page<T> page) {
		this.page = page;
	}
	public ExceptionWrapper getException() {
		return exception;
	}
	public void setException(ExceptionWrapper exception) {
		this.exception = exception;
	}
	
	public static Object toJson(Object value) {
		
		
		return null;
	}

}
