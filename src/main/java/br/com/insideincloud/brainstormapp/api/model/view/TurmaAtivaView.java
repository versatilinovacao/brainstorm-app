package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="turmas_ativas_view", schema="escola")
public class TurmaAtivaView implements Serializable {
	private static final long serialVersionUID = -3425972804420584988L;

	@Id
	private Long turma_id;
	private String descricao;
	private LocalDate inicio;
	private LocalDate fim;
	private Boolean status;

	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getInicio() {
		return inicio;
	}
	public void setInicio(LocalDate inicio) {
		this.inicio = inicio;
	}
	public LocalDate getFim() {
		return fim;
	}
	public void setFim(LocalDate fim) {
		this.fim = fim;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((turma_id == null) ? 0 : turma_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurmaAtivaView other = (TurmaAtivaView) obj;
		if (turma_id == null) {
			if (other.turma_id != null)
				return false;
		} else if (!turma_id.equals(other.turma_id))
			return false;
		return true;
	}

}
