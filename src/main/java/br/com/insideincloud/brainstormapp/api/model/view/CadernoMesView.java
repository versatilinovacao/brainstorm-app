package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="caderno_mes_view",schema="escola")
public class CadernoMesView implements Serializable {
	private static final long serialVersionUID = -2856031936954547786L;

	@Id
	private Long _caderno_id;
	@Column(name="nome_curso")
	private String nomecurso;
	@Column(name="nome_turma")
	private String nometurma;
	@Column(name="nome_caderno")
	private String nomecaderno;
	@Column(name="_mes")
	private BigDecimal mes;
	@Column(name="mes_por_extenso")
	private String mesporextenso;
	@Column(name="_ano")
	private BigDecimal ano;
	
	public Long get_caderno_id() {
		return _caderno_id;
	}
	public void set_caderno_id(Long _caderno_id) {
		this._caderno_id = _caderno_id;
	}
	public String getNomecurso() {
		return nomecurso;
	}
	public void setNomecurso(String nomecurso) {
		this.nomecurso = nomecurso;
	}
	public String getNometurma() {
		return nometurma;
	}
	public void setNometurma(String nometurma) {
		this.nometurma = nometurma;
	}
	public String getNomecaderno() {
		return nomecaderno;
	}
	public void setNomecaderno(String nomecaderno) {
		this.nomecaderno = nomecaderno;
	}
	public BigDecimal getMes() {
		return mes;
	}
	public void setMes(BigDecimal mes) {
		this.mes = mes;
	}
	public String getMesporextenso() {
		return mesporextenso;
	}
	public void setMesporextenso(String mesporextenso) {
		this.mesporextenso = mesporextenso;
	}
	public BigDecimal getAno() {
		return ano;
	}
	public void setAno(BigDecimal ano) {
		this.ano = ano;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_caderno_id == null) ? 0 : _caderno_id.hashCode());
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((mes == null) ? 0 : mes.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadernoMesView other = (CadernoMesView) obj;
		if (_caderno_id == null) {
			if (other._caderno_id != null)
				return false;
		} else if (!_caderno_id.equals(other._caderno_id))
			return false;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (mes == null) {
			if (other.mes != null)
				return false;
		} else if (!mes.equals(other.mes))
			return false;
		return true;
	}

}
