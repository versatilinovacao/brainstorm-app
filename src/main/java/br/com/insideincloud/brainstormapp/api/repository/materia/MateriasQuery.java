package br.com.insideincloud.brainstormapp.api.repository.materia;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Materia;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaFilter;

public interface MateriasQuery {
	public Page<Materia> filtrar(MateriaFilter filtro, Pageable page);
}
