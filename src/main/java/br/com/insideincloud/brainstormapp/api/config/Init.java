package br.com.insideincloud.brainstormapp.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:data/dbo.properties")
public class Init {
	
//	@Bean
//	public InitConfig() {
//		
//		return null;
//	}
	
}
