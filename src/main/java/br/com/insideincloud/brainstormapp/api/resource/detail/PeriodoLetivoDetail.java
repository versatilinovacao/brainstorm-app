package br.com.insideincloud.brainstormapp.api.resource.detail;

import java.util.List;

public class PeriodoLetivoDetail {
	private Long periodo_letivo_id;
	private String descricao;
	private String ano;
	private boolean status;
	private List<PeriodoLetivoItemDetail> periodoletivoitens;
	private Long empresa_id;
		
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public List<PeriodoLetivoItemDetail> getPeriodoletivoitens() {
		return periodoletivoitens;
	}
	public void setPeriodoletivoitens(List<PeriodoLetivoItemDetail> periodoletivoitens) {
		this.periodoletivoitens = periodoletivoitens;
	}
	
}
