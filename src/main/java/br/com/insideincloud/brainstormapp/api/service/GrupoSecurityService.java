package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurity;

@Service
public class GrupoSecurityService { 
	
	@Autowired
	private GruposSecurity grupos;
	
	public GrupoSecurity salvar(GrupoSecurity grupoSecurity) throws DataAccessException {
		
		return grupos.saveAndFlush(grupoSecurity);
	} 

}
