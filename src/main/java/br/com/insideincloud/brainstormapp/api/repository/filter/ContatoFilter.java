package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ContatoFilter {
	private String contato_id;
	private String descricao;
	private String colaborador_id;
	public String getContato_id() {
		return contato_id;
	}
	
	public void setContato_id(String contato_id) {
		this.contato_id = contato_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	
}
