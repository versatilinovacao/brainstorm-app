package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="login",schema="action")
public class Login implements Serializable {
	private static final long serialVersionUID = 1099682736518428242L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long login_id;
	@OneToOne
	@JoinColumn(name="usuario_id")
	private Usuario usuario_id;
	
	public Long getLogin_id() {
		return login_id;
	}
	public void setLogin_id(Long login_id) {
		this.login_id = login_id;
	}
	public Usuario getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Usuario usuario_id) {
		this.usuario_id = usuario_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login_id == null) ? 0 : login_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Login other = (Login) obj;
		if (login_id == null) {
			if (other.login_id != null)
				return false;
		} else if (!login_id.equals(other.login_id))
			return false;
		return true;
	}
	
	
}
