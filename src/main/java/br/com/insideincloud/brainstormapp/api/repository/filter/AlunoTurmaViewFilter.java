package br.com.insideincloud.brainstormapp.api.repository.filter;

public class AlunoTurmaViewFilter {
	private String colaborador_id;
	private String composicao_colaborador_id;
	private String vinculo;
	private String nome;
	private String matricula;
	
	public String getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(String composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getVinculo() {
		return vinculo;
	}
	public void setVinculo(String vinculo) {
		this.vinculo = vinculo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	

}
