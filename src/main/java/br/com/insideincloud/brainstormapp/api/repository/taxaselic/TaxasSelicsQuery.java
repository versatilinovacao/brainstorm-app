package br.com.insideincloud.brainstormapp.api.repository.taxaselic;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.TaxaSelic;
import br.com.insideincloud.brainstormapp.api.repository.filter.TaxaSelicFilter;

public interface TaxasSelicsQuery {
	public Page<TaxaSelic> filtrar(TaxaSelicFilter filtro, Pageable page);
}
