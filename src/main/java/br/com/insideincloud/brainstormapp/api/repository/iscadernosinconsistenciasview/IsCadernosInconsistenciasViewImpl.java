package br.com.insideincloud.brainstormapp.api.repository.iscadernosinconsistenciasview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.IsCadernoInconsistenciaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.IsCadernoInconsistenciaViewFilter;

public class IsCadernosInconsistenciasViewImpl implements IsCadernosInconsistenciasViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<IsCadernoInconsistenciaView> filtrar(IsCadernoInconsistenciaViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<IsCadernoInconsistenciaView> criteria = builder.createQuery(IsCadernoInconsistenciaView.class);
		Root<IsCadernoInconsistenciaView> root = criteria.from(IsCadernoInconsistenciaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<IsCadernoInconsistenciaView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(IsCadernoInconsistenciaViewFilter filtro, CriteriaBuilder builder, Root<IsCadernoInconsistenciaView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(IsCadernoInconsistenciaViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<IsCadernoInconsistenciaView> root = criteria.from(IsCadernoInconsistenciaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}


}
