package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class Id {
	private String descricao;

	public Id(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	

}
