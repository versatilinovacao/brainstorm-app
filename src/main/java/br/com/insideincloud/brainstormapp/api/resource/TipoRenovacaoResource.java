package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoRenovacao;
import br.com.insideincloud.brainstormapp.api.repository.TiposRenovacoes;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposrenovacoes")
public class TipoRenovacaoResource {

	@Autowired
	private TiposRenovacoes tipos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIPORENOVACAO_CONTEUDO')")
	public RetornoWrapper<TipoRenovacao> conteudo() {
		RetornoWrapper<TipoRenovacao> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tipos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de renovação.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
