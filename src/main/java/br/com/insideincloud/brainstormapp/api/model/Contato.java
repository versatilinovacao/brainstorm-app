package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="contatos")
public class Contato implements Serializable {
	private static final long serialVersionUID = 4406666333095404036L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long contato_id;
	
	
	@ManyToMany(mappedBy = "contatos")
	private List<Colaborador> colaboradores;
	
	
	
	
////	@OneToOne
////	@JoinColumn(name="colaborador_id")
//	public Colaborador colaborador;
////	@Column(name="telefone",length=15)
//	private String telefone;
////	@Column(name="ramal",length=6)
//	private String ramal;
////	@OneToOne
////	@JoinColumn(name="tipo_telefone_id")
//	private TipoTelefone tipo;
////	@Column(name="email",length=200)
//	private String email;
	
	
//	public Colaborador getColaborador() {
//		return colaborador;
//	}
//	public void setColaborador(Colaborador colaborador) {
//		this.colaborador = colaborador;
//	}
//	public Long getContato_id() {
//		return contato_id;
//	}
//	public void setContato_id(Long contato_id) {
//		this.contato_id = contato_id;
//	}
//	public String getTelefone() {
//		return telefone;
//	}
//	public void setTelefone(String telefone) {
//		this.telefone = telefone;
//	}
//	public String getRamal() {
//		return ramal;
//	}
//	public void setRamal(String ramal) {
//		this.ramal = ramal;
//	}
//	public TipoTelefone getTipo() {
//		return tipo;
//	}
//	public void setTipo(TipoTelefone tipo) {
//		this.tipo = tipo;
//	}	
//	public String getEmail() {
//		return email;
//	}
//	public void setEmail(String email) {
//		this.email = email;
//	}
	
	public Long getContato_id() {
		return contato_id;
	}
	public void setContato_id(Long contato_id) {
		this.contato_id = contato_id;
	}
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}
	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contato_id == null) ? 0 : contato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contato other = (Contato) obj;
		if (contato_id == null) {
			if (other.contato_id != null)
				return false;
		} else if (!contato_id.equals(other.contato_id))
			return false;
		return true;
	}	

}
