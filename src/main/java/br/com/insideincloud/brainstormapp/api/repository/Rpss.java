package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Rps;
import br.com.insideincloud.brainstormapp.api.model.pk.RpsPK;

@Repository
public interface Rpss extends JpaRepository<Rps,RpsPK> {

}
