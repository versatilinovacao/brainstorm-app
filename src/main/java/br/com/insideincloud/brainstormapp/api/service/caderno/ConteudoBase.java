package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

@Component
public class ConteudoBase {
	private Long conteudo_id;
	private String titulo;
	private String descricao;
	private BigDecimal carga_horaria;
	
	public Long getConteudo_id() {
		return conteudo_id;
	}
	public void setConteudo_id(Long conteudo_id) {
		this.conteudo_id = conteudo_id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getCarga_horaria() {
		return carga_horaria;
	}
	public void setCarga_horaria(BigDecimal carga_horaria) {
		this.carga_horaria = carga_horaria;
	}	
	
}
