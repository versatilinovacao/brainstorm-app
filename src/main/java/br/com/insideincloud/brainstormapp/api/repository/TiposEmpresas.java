package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TipoEmpresa;

@Repository
public interface TiposEmpresas extends JpaRepository<TipoEmpresa,Long> {

}
