package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="classes")
public class Classe implements Serializable {
	private static final long serialVersionUID = -8517877434515960151L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classe_id;
	@Column(name="codigo",length=5)
	private String codigo;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="descricao",length=500)
	private String descricao;
	
	public Long getClasse_id() {
		return classe_id;
	}
	public void setClasse_id(Long classe_id) {
		this.classe_id = classe_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classe_id == null) ? 0 : classe_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classe other = (Classe) obj;
		if (classe_id == null) {
			if (other.classe_id != null)
				return false;
		} else if (!classe_id.equals(other.classe_id))
			return false;
		return true;
	}
	
}
