package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Permissao;
import br.com.insideincloud.brainstormapp.api.model.TelaSistema;
import br.com.insideincloud.brainstormapp.api.repository.Permissoes;
import br.com.insideincloud.brainstormapp.api.repository.TelasSistema;
import br.com.insideincloud.brainstormapp.api.resource.delete.TelaSistemaDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/telassistema")
public class TelaSistemaResource {
	@Autowired
	private TelasSistema telasSistema;
	
	@Autowired
	private Permissoes permissoes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TELASISTEMA_CONTEUDO')")
	public RetornoWrapper<TelaSistema> conteudo() {
		RetornoWrapper<TelaSistema> retorno = new RetornoWrapper<TelaSistema>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(telasSistema.findAll());
		} catch (Exception e) {
			erro.setCodigo(17);
			erro.setMensagem("Não foi possível carregar as informações do cadastro de telas do sistema, tente mais tarde.");
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_TELASISTEMA_CONTEUDO')")
	public TelaSistema buscaPeloCodigo(@PathVariable Long codigo) {
		return telasSistema.findOne(codigo);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_TELASISTEMA_SALVAR')")
//	@Transactional
	public RetornoWrapper<TelaSistema> salvar(@RequestBody TelaSistema telaSistema, HttpServletResponse response) {
		RetornoWrapper<TelaSistema> retorno = new RetornoWrapper<TelaSistema>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(telaSistema.getTelasistema_id()).toUri();
			response.setHeader("Location", uri.toASCIIString());			
			
			retorno.setSingle(telasSistema.saveAndFlush(telaSistema));
		} catch (Exception e) {
			erro.setCodigo(23);
			erro.setMensagem("Não foi posspivel salvar a tela cadastrada, favor verique as informações. "+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_TELASISTEMA_DELETAR')")
	@Transactional
	public RetornoWrapper<TelaSistema> deletar(@RequestBody TelaSistemaDelete telasistema) {
		RetornoWrapper<TelaSistema> retorno = new RetornoWrapper<TelaSistema>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			for (TelaSistema conteudo: telasistema.getTelassistema()) {
				telasSistema.delete(conteudo);
			}
		} catch (Exception e) {
			erro.setCodigo(22);
			erro.setMensagem("Não foi possível deletar um ou mais de um arquivo selecionado, tente novamente mais tarde. " + e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PostMapping("/inicializar")
	@PreAuthorize("hasAuthority('ROLE_INICIALIZAPERMISSAO_EXECUTAR')")
	public void permissoes() {
		Permissao permissaoC = new Permissao();
		permissaoC.setNome("ROLE_TELASISTEMA_CONTEUDO");
		permissoes.save(permissaoC);
		Permissao permissaoS = new Permissao();
		permissaoS.setNome("ROLE_TELASISTEMA_SALVAR");
		permissoes.save(permissaoS);
		Permissao permissaoD = new Permissao();
		permissaoD.setNome("ROLE_TELASISTEMA_DELETAR");
		permissoes.save(permissaoD);
	}
	
}
