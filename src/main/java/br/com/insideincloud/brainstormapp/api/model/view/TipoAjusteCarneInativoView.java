package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.PlanoConta;

@Entity
@Table(name="tipos_ajustes_carnes_inativos_view",schema="financeiro")
public class TipoAjusteCarneInativoView implements Serializable {

	@Id
	private Long tipoajustecarne_id;
	private String nome;
	@OneToOne
	@JoinColumn(name="planoconta_id")
	private PlanoConta planoconta;
	private Boolean status;
	
	public Long getTipoajustecarne_id() {
		return tipoajustecarne_id;
	}
	public void setTipoajustecarne_id(Long tipoajustecarne_id) {
		this.tipoajustecarne_id = tipoajustecarne_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public PlanoConta getPlanoconta() {
		return planoconta;
	}
	public void setPlanoconta(PlanoConta planoconta) {
		this.planoconta = planoconta;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoajustecarne_id == null) ? 0 : tipoajustecarne_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAjusteCarneInativoView other = (TipoAjusteCarneInativoView) obj;
		if (tipoajustecarne_id == null) {
			if (other.tipoajustecarne_id != null)
				return false;
		} else if (!tipoajustecarne_id.equals(other.tipoajustecarne_id))
			return false;
		return true;
	}

}
