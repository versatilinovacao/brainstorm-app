package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.repository.filter.TurmaFilter;
import br.com.insideincloud.brainstormapp.api.repository.turma.TurmasQuery;

@Repository
public interface Turmas extends JpaRepository<Turma,Long>, TurmasQuery {
	public Page<Turma> filtrar(TurmaFilter filtro, Pageable page);
	
	

}
