
package br.com.insideincloud.brainstormapp.api.soap.homologacao.cachoeirinha.entidade.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DadosCadastrais complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DadosCadastrais">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="atividadeCnae" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="bairro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnpjCpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigoBairro" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoCidade" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="codigoLogradouro" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="complementoEndereco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endereco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fantasia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificacao" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="inscricao" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numeroEndereco" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="razaoSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DadosCadastrais", propOrder = {
    "atividadeCnae",
    "bairro",
    "cep",
    "cidade",
    "cnpjCpf",
    "codigoBairro",
    "codigoCidade",
    "codigoLogradouro",
    "complementoEndereco",
    "contato",
    "email",
    "endereco",
    "fantasia",
    "identificacao",
    "inscricao",
    "numeroEndereco",
    "razaoSocial",
    "telefone",
    "uf"
})
public class DadosCadastrais {

    @XmlElementRef(name = "atividadeCnae", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> atividadeCnae;
    @XmlElementRef(name = "bairro", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> bairro;
    @XmlElementRef(name = "cep", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cep;
    @XmlElementRef(name = "cidade", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cidade;
    @XmlElementRef(name = "cnpjCpf", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cnpjCpf;
    @XmlElementRef(name = "codigoBairro", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> codigoBairro;
    @XmlElementRef(name = "codigoCidade", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> codigoCidade;
    @XmlElementRef(name = "codigoLogradouro", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> codigoLogradouro;
    @XmlElementRef(name = "complementoEndereco", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> complementoEndereco;
    @XmlElementRef(name = "contato", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> contato;
    @XmlElementRef(name = "email", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> email;
    @XmlElementRef(name = "endereco", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> endereco;
    @XmlElementRef(name = "fantasia", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fantasia;
    @XmlElementRef(name = "identificacao", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> identificacao;
    @XmlElementRef(name = "inscricao", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> inscricao;
    @XmlElementRef(name = "numeroEndereco", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<Long> numeroEndereco;
    @XmlElementRef(name = "razaoSocial", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> razaoSocial;
    @XmlElementRef(name = "telefone", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> telefone;
    @XmlElementRef(name = "uf", namespace = "http://entidade.server.nfse.thema.inf.br/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> uf;

    /**
     * Obtém o valor da propriedade atividadeCnae.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getAtividadeCnae() {
        return atividadeCnae;
    }

    /**
     * Define o valor da propriedade atividadeCnae.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setAtividadeCnae(JAXBElement<Long> value) {
        this.atividadeCnae = value;
    }

    /**
     * Obtém o valor da propriedade bairro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBairro() {
        return bairro;
    }

    /**
     * Define o valor da propriedade bairro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBairro(JAXBElement<String> value) {
        this.bairro = value;
    }

    /**
     * Obtém o valor da propriedade cep.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCep() {
        return cep;
    }

    /**
     * Define o valor da propriedade cep.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCep(JAXBElement<String> value) {
        this.cep = value;
    }

    /**
     * Obtém o valor da propriedade cidade.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCidade() {
        return cidade;
    }

    /**
     * Define o valor da propriedade cidade.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCidade(JAXBElement<String> value) {
        this.cidade = value;
    }

    /**
     * Obtém o valor da propriedade cnpjCpf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCnpjCpf() {
        return cnpjCpf;
    }

    /**
     * Define o valor da propriedade cnpjCpf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCnpjCpf(JAXBElement<String> value) {
        this.cnpjCpf = value;
    }

    /**
     * Obtém o valor da propriedade codigoBairro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getCodigoBairro() {
        return codigoBairro;
    }

    /**
     * Define o valor da propriedade codigoBairro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setCodigoBairro(JAXBElement<Long> value) {
        this.codigoBairro = value;
    }

    /**
     * Obtém o valor da propriedade codigoCidade.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getCodigoCidade() {
        return codigoCidade;
    }

    /**
     * Define o valor da propriedade codigoCidade.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setCodigoCidade(JAXBElement<Long> value) {
        this.codigoCidade = value;
    }

    /**
     * Obtém o valor da propriedade codigoLogradouro.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getCodigoLogradouro() {
        return codigoLogradouro;
    }

    /**
     * Define o valor da propriedade codigoLogradouro.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setCodigoLogradouro(JAXBElement<Long> value) {
        this.codigoLogradouro = value;
    }

    /**
     * Obtém o valor da propriedade complementoEndereco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getComplementoEndereco() {
        return complementoEndereco;
    }

    /**
     * Define o valor da propriedade complementoEndereco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setComplementoEndereco(JAXBElement<String> value) {
        this.complementoEndereco = value;
    }

    /**
     * Obtém o valor da propriedade contato.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContato() {
        return contato;
    }

    /**
     * Define o valor da propriedade contato.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContato(JAXBElement<String> value) {
        this.contato = value;
    }

    /**
     * Obtém o valor da propriedade email.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmail() {
        return email;
    }

    /**
     * Define o valor da propriedade email.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmail(JAXBElement<String> value) {
        this.email = value;
    }

    /**
     * Obtém o valor da propriedade endereco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEndereco() {
        return endereco;
    }

    /**
     * Define o valor da propriedade endereco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEndereco(JAXBElement<String> value) {
        this.endereco = value;
    }

    /**
     * Obtém o valor da propriedade fantasia.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFantasia() {
        return fantasia;
    }

    /**
     * Define o valor da propriedade fantasia.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFantasia(JAXBElement<String> value) {
        this.fantasia = value;
    }

    /**
     * Obtém o valor da propriedade identificacao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getIdentificacao() {
        return identificacao;
    }

    /**
     * Define o valor da propriedade identificacao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setIdentificacao(JAXBElement<Long> value) {
        this.identificacao = value;
    }

    /**
     * Obtém o valor da propriedade inscricao.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getInscricao() {
        return inscricao;
    }

    /**
     * Define o valor da propriedade inscricao.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setInscricao(JAXBElement<Long> value) {
        this.inscricao = value;
    }

    /**
     * Obtém o valor da propriedade numeroEndereco.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public JAXBElement<Long> getNumeroEndereco() {
        return numeroEndereco;
    }

    /**
     * Define o valor da propriedade numeroEndereco.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Long }{@code >}
     *     
     */
    public void setNumeroEndereco(JAXBElement<Long> value) {
        this.numeroEndereco = value;
    }

    /**
     * Obtém o valor da propriedade razaoSocial.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * Define o valor da propriedade razaoSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRazaoSocial(JAXBElement<String> value) {
        this.razaoSocial = value;
    }

    /**
     * Obtém o valor da propriedade telefone.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTelefone() {
        return telefone;
    }

    /**
     * Define o valor da propriedade telefone.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTelefone(JAXBElement<String> value) {
        this.telefone = value;
    }

    /**
     * Obtém o valor da propriedade uf.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUf() {
        return uf;
    }

    /**
     * Define o valor da propriedade uf.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUf(JAXBElement<String> value) {
        this.uf = value;
    }

}
