package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.Produto;
import br.com.insideincloud.brainstormapp.api.repository.Produtos;

@RestController
@RequestMapping("/produtos")
public class ProdutoResource {
	@Autowired
	private Produtos produtos;

	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_PRODUTO_SALVAR')")
	public ResponseEntity<Produto> salvar(@RequestBody Produto produto, HttpServletResponse response) {
		produto = produtos.saveAndFlush(produto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(produto.getProduto_id()).toUri();
		response.setHeader("Location",uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(produto);
	} 

}