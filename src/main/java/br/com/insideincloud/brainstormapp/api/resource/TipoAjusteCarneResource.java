package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoAjusteCarne;
import br.com.insideincloud.brainstormapp.api.model.view.TipoAjusteCarneAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.TipoAjusteCarneInativoView;
import br.com.insideincloud.brainstormapp.api.repository.TiposAjustesCarnes;
import br.com.insideincloud.brainstormapp.api.repository.TiposAjustesCarnesAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.TiposAjustesCarnesInativosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposajustescarnes")
public class TipoAjusteCarneResource {
	
	@Autowired
	private TiposAjustesCarnes ajustesCarnes;

	@Autowired
	private TiposAjustesCarnesAtivosView ajusteCarnesAtivos;
	
	@Autowired
	private TiposAjustesCarnesInativosView ajustesCarnesInativos; 
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIPOAJUSTECARNE_CONTEUDO')")
	public RetornoWrapper<TipoAjusteCarneAtivoView> conteudoAtivo() {
		RetornoWrapper<TipoAjusteCarneAtivoView> retorno = new RetornoWrapper<TipoAjusteCarneAtivoView>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( ajusteCarnesAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações referente a tipos de ajuste de carnes, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_TIPOAJUSTECARNE_CONTEUDO')")
	public RetornoWrapper<TipoAjusteCarneInativoView> conteudoInativo() {
		RetornoWrapper<TipoAjusteCarneInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( ajustesCarnesInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações dos tipos de ajustes de carnes, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_TIPOAJUSTECARNE_CONTEUDO')")
	public RetornoWrapper<TipoAjusteCarne> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<TipoAjusteCarne> retorno = new RetornoWrapper<TipoAjusteCarne>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( ajustesCarnes.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_TIPOAJUSTECARNE_SALVAR')")
	public RetornoWrapper<TipoAjusteCarne> salvar(@RequestBody TipoAjusteCarne tipo) {
		RetornoWrapper<TipoAjusteCarne> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( ajustesCarnes.saveAndFlush(tipo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o tipo de ajuste do Carnê.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
