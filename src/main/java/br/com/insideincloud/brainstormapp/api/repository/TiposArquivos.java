package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TipoArquivo;

@Repository
public interface TiposArquivos extends JpaRepository<TipoArquivo, Long> {
	
	@Query("select t from TipoArquivo t where t.sigla = ?1")
	public TipoArquivo findByTipo(String tipo);

}
