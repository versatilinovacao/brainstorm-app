package br.com.insideincloud.brainstormapp.api.model;

import java.math.BigDecimal;

public class Indexador {
	private Long indexador_id;
	private int codigo_bacen;
	private String nome;
	private BigDecimal valor;
	
	public Long getIndexador_id() {
		return indexador_id;
	}
	public void setIndexador_id(Long indexador_id) {
		this.indexador_id = indexador_id;
	}
	public int getCodigo_bacen() {
		return codigo_bacen;
	}
	public void setCodigo_bacen(int codigo_bacen) {
		this.codigo_bacen = codigo_bacen;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo_bacen;
		result = prime * result + ((indexador_id == null) ? 0 : indexador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Indexador other = (Indexador) obj;
		if (codigo_bacen != other.codigo_bacen)
			return false;
		if (indexador_id == null) {
			if (other.indexador_id != null)
				return false;
		} else if (!indexador_id.equals(other.indexador_id))
			return false;
		return true;
	}
	
}
