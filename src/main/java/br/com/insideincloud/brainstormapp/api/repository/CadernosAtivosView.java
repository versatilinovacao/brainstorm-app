package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.CadernoAtivoView;

@Repository
public interface CadernosAtivosView extends JpaRepository<CadernoAtivoView, Long> {

}
