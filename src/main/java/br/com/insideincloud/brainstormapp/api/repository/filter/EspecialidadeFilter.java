package br.com.insideincloud.brainstormapp.api.repository.filter;

public class EspecialidadeFilter {
	private String especializacao_id;
	private String descricao;
	
	public String getEspecializacao_id() {
		return especializacao_id;
	}
	public void setEspecializacao_id(String especializacao_id) {
		this.especializacao_id = especializacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
