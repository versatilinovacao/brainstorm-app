package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ResponsavellView;
import br.com.insideincloud.brainstormapp.api.repository.responsavellview.Responsaveis_ViewQuery;

@Repository
public interface Responsaveis_View extends JpaRepository<ResponsavellView,Long>,Responsaveis_ViewQuery  {
	
	@Query("select r from ResponsavellView r where r.colaborador_id = ?1 and r.composicao_colaborador_id = ?2")
	public List<ResponsavellView> findByResponsaveisConteudo(Long colaborador_id, Long composicao_colaborador_id);
}
