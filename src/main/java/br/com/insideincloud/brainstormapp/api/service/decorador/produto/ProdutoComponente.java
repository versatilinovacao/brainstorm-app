package br.com.insideincloud.brainstormapp.api.service.decorador.produto;

import java.math.BigDecimal;

public interface ProdutoComponente {
	public String getNome();
	public String getDescricao();
	
	public String getCFOP();
	public String getNCM();
	public String getTributacao();
	public BigDecimal getPreco();
	public BigDecimal getQuantidade();
	public BigDecimal getFrete();
	public BigDecimal getOutrasDespesas();
	public BigDecimal getFcp();
	public BigDecimal getDesconto();
	public BigDecimal getPis();
	public BigDecimal getConfis();
	public BigDecimal getIpi();
}
