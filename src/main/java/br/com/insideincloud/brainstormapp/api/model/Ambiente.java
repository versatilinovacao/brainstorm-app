package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ambientes")
public class Ambiente implements Serializable {
	private static final long serialVersionUID = -4686144886768759056L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long ambiente_id;
	@ManyToOne
	@JoinColumn(name="composicao_id")
	private Ambiente composicao;
	private String descricao;
	@OneToOne
	@JoinColumn(name="tipo_ambiente_id")
	private TipoAmbiente tipoambiente;
	private BigDecimal largura;
	private BigDecimal altura;
	private BigDecimal comprimento;
	@Column(name="capacidade_pessoas")
	private Long capacidadepessoas;
	
	public Long getAmbiente_id() {
		return ambiente_id;
	}
	public void setAmbiente_id(Long ambiente_id) {
		this.ambiente_id = ambiente_id;
	}
	public Ambiente getComposicao() {
		return composicao;
	}
	public void setComposicao(Ambiente composicao) {
		this.composicao = composicao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public TipoAmbiente getTipoambiente() {
		return tipoambiente;
	}
	public void setTipoambiente(TipoAmbiente tipoambiente) {
		this.tipoambiente = tipoambiente;
	}
	public BigDecimal getLargura() {
		return largura;
	}
	public void setLargura(BigDecimal largura) {
		this.largura = largura;
	}
	public BigDecimal getAltura() {
		return altura;
	}
	public void setAltura(BigDecimal altura) {
		this.altura = altura;
	}
	public BigDecimal getComprimento() {
		return comprimento;
	}
	public void setComprimento(BigDecimal comprimento) {
		this.comprimento = comprimento;
	}
	public Long getCapacidadepessoas() {
		return capacidadepessoas;
	}
	public void setCapacidadepessoas(Long capacidadepessoas) {
		this.capacidadepessoas = capacidadepessoas;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ambiente_id == null) ? 0 : ambiente_id.hashCode());
		result = prime * result + ((composicao == null) ? 0 : composicao.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ambiente other = (Ambiente) obj;
		if (ambiente_id == null) {
			if (other.ambiente_id != null)
				return false;
		} else if (!ambiente_id.equals(other.ambiente_id))
			return false;
		if (composicao == null) {
			if (other.composicao != null)
				return false;
		} else if (!composicao.equals(other.composicao))
			return false;
		return true;
	}

	
}
