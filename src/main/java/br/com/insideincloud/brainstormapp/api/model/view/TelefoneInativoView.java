package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.TipoTelefone;

@Entity
@Table(name="telefones_inativos_view")
public class TelefoneInativoView implements Serializable {
	private static final long serialVersionUID = 3566101170234556076L;

	@Id
	private Long telefone_id;
	private String numero;
	private String ramal;
	@OneToOne
	@JoinColumn(name="tipo_telefone_id")
	private TipoTelefone tipotelefone;
	private Boolean status;
	
	public Long getTelefone_id() {
		return telefone_id;
	}
	public void setTelefone_id(Long telefone_id) {
		this.telefone_id = telefone_id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public TipoTelefone getTipotelefone() {
		return tipotelefone;
	}
	public void setTipotelefone(TipoTelefone tipotelefone) {
		this.tipotelefone = tipotelefone;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telefone_id == null) ? 0 : telefone_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelefoneInativoView other = (TelefoneInativoView) obj;
		if (telefone_id == null) {
			if (other.telefone_id != null)
				return false;
		} else if (!telefone_id.equals(other.telefone_id))
			return false;
		return true;
	}
	
}
