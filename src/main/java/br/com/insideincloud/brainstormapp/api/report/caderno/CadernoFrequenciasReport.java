package br.com.insideincloud.brainstormapp.api.report.caderno;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.service.caderno.CadernoPeriodoReport;
import br.com.insideincloud.brainstormapp.api.service.caderno.CadernoReport;
import ch.qos.logback.core.net.SyslogOutputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Component
public class CadernoFrequenciasReport {
	public byte[] Report(List<CadernoReport> cadernos) throws JRException {
//		URL urlCadernoChamada = getClass().getClassLoader().getResource("cadernochamada.jasper");
//		URL urlFrequenciaAlunos = getClass().getClassLoader().getResource("frequenciaalunos.jasper");
		//URL urlImagem = getClass().getClassLoader().getResource("logo_caderno.png");
//		System.out.println("XXXXXXXXXXXXXXXXXTTTTTTWWWWWWW - "+urlFrequenciaAlunos.getPath());
		
		

//		InputStream inputStream = this.getClass().getResourceAsStream("cadernos.jasper");
		InputStream inputStream;
		try {
//			inputStream = new FileInputStream("/var/local/ecosistema/relatorios_producao/brainstormteste/cadernos.jasper");
//			inputStream = new FileInputStream("/var/local/ecosistema/relatorios_producao/prosaber/cadernos.jasper");
			inputStream = new FileInputStream("/var/local/ecosistema/relatorios_producao/localhost/cadernos.jasper");
			Map<String,Object> allParameters = new HashMap<>();
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(cadernos);
		
			/*
			 * file:/var/local/ecosistema/brainstorm-app.api-1.0.1-SNAPSHOT.jar!/BOOT-INF/classes!/frequenciaalunos.jasper
			 * 
			 * @Value("classpath:data/resource-data.txt")
			   Resource resourceFile;
			 * */

			
			allParameters.put("Report", "/var/local/ecosistema/relatorios_producao/localhost/cadernochamada.jasper");
			allParameters.put("subReport", "/var/local/ecosistema/relatorios_producao/localhost/frequenciaalunos.jasper");
			allParameters.put("imagem_logomarca", "/var/local/ecosistema/relatorios_producao/localhost/imagens/logo_caderno.png");
			
//			allParameters.put("Report", "/var/local/ecosistema/relatorios_producao/prosaber/cadernochamada.jasper");
//			allParameters.put("subReport", "/var/local/ecosistema/relatorios_producao/prosaber/frequenciaalunos.jasper");
//			allParameters.put("imagem_logomarca", "/var/local/ecosistema/relatorios_producao/prosaber/imagens/logo_caderno.png");
			
			
//			allParameters.put("Report", "/var/local/ecosistema/relatorios_producao/brainstormteste/cadernochamada.jasper");
//			allParameters.put("subReport", "/var/local/ecosistema/relatorios_producao/brainstormteste/frequenciaalunos.jasper");
//			allParameters.put("imagem_logomarca", "/var/local/ecosistema/relatorios_producao/brainstormteste/imagens/logo_caderno.png");
			
			JasperPrint print = JasperFillManager.fillReport(inputStream, allParameters, dataSource);
			return JasperExportManager.exportReportToPdf(print);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return null;
	}
}


/*
 * 	public byte[] Report(List<CapaCaderno> capas) throws Exception {
		
		
		InputStream inputStream = this.getClass().getResourceAsStream("capacadernosintetico.jasper");
		
//		final JasperReport report = (JasperReport) JRLoader.loadObject(Thread.currentThread()
//													 						 .getContextClassLoader()
//													 						 .getResourceAsStream("/capacadernosintetico.jasper"));
		
		Map<String,Object> allParameters = new HashMap<>();
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(capas);
		
		JasperPrint print = JasperFillManager.fillReport(inputStream, allParameters,dataSource);
		return JasperExportManager.exportReportToPdf(print);
		
	}

 * 
 */