package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Estado;
import br.com.insideincloud.brainstormapp.api.repository.Estados;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/estados")
public class EstadoResource {
	@Autowired
	private Estados estados;
	
	@GetMapping("/{pais_id}")
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Estado> conteudo(@PathVariable Long pais_id) {
		RetornoWrapper<Estado> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(estados.findByPais(pais_id));
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
		}
		
		return retorno;
	}

}
