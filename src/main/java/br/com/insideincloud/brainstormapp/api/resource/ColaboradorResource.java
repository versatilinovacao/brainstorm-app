package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ClassificacaoEmpresa;
import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.DeletarColaboradorAction;
import br.com.insideincloud.brainstormapp.api.model.FormatoCaderno;
import br.com.insideincloud.brainstormapp.api.model.TipoCliente;
import br.com.insideincloud.brainstormapp.api.model.TipoCriptografia;
import br.com.insideincloud.brainstormapp.api.model.TipoTercerizado;
import br.com.insideincloud.brainstormapp.api.model.minor.ClienteMinor;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorEmpresaResumoView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorParceiroView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorTercerizadoView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorUserView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorView;
import br.com.insideincloud.brainstormapp.api.model.view.EmpresaComposicaoView;
import br.com.insideincloud.brainstormapp.api.repository.ClassificacoesEmpresas;
import br.com.insideincloud.brainstormapp.api.repository.ClientesMinor;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresEmpresasResumosView;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresInativosView;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresParceirosView;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresTercerizadosView;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresUsersView;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresView;
import br.com.insideincloud.brainstormapp.api.repository.DeletarColaboradoresAction;
import br.com.insideincloud.brainstormapp.api.repository.EmpresasComposicoesView;
import br.com.insideincloud.brainstormapp.api.repository.FormatosCadernos;
import br.com.insideincloud.brainstormapp.api.repository.TiposClientes;
import br.com.insideincloud.brainstormapp.api.repository.TiposCriptografias;
import br.com.insideincloud.brainstormapp.api.repository.TiposTercerizados;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.ColaboradorDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.ColaboradorService;

@RestController
@RequestMapping("/colaboradores")
public class ColaboradorResource {
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private TiposClientes tiposclientes;
	
	@Autowired
	private DeletarColaboradoresAction deletarColaboradores;
	
	@Autowired
	private ColaboradoresView colaboradoresView;
	
	@Autowired
	private ClassificacoesEmpresas classificacoesEmpresas;

	@Autowired
	private ColaboradoresEmpresasResumosView empresasResumo;

	@Autowired
	private ColaboradoresUsersView colaboradoresUsersView;
	
	@Autowired
	private ColaboradoresInativosView colaboradoresInativos;
	
	@Autowired
	private ColaboradorService colaboradorService;
	
	@Autowired
	private EmpresasComposicoesView empresasComposicoes;

	@Autowired
	private TiposTercerizados tipostercerizados;
	
	@Autowired
	private ColaboradoresTercerizadosView colaboradoresTercerizados;
	
	@Autowired
	private FormatosCadernos formatosCadernos;
	
	@Autowired
	private ColaboradoresParceirosView colaboradoresParceirosView;
	
	@Autowired
	private TiposCriptografias tiposCriptografias;
	
	@Autowired
	private ClientesMinor clientesMinor;
	
	@GetMapping("/formatos")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<FormatoCaderno> formatosCaderno() {
		RetornoWrapper<FormatoCaderno> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(formatosCadernos.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/tercerizados")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<ColaboradorTercerizadoView> colaboradoresTercerizadosConteudo() {
		RetornoWrapper<ColaboradorTercerizadoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(colaboradoresTercerizados.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações de colaboradores tercerizados (Financeira/Banco), favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/tipostercerizados")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<TipoTercerizado> conteudoTipoTercerizado() {
		RetornoWrapper<TipoTercerizado> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(tipostercerizados.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);			
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<Colaborador> conteudoDefault() {
		RetornoWrapper<Colaborador> retorno = new RetornoWrapper<Colaborador>();
		retorno.setConteudo(colaboradores.findAll());
		
		return retorno;		
	}
	
	@GetMapping("/resumido")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<ColaboradorUserView> conteudoResumido() {
		RetornoWrapper<ColaboradorUserView> retorno = new RetornoWrapper<ColaboradorUserView>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ColaboradorUserView> colaboradores = colaboradoresUsersView.findAll();
			retorno.setConteudo(colaboradores);
			
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível carregar as informações resumidas do colaborador, tente novamente mais tarde!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<ColaboradorInativoView> conteudoInativo() {
		RetornoWrapper<ColaboradorInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			List<ColaboradorInativoView> conteudo = colaboradoresInativos.findAll();
			retorno.setConteudo(conteudo);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não existem informações de Colaboradores Inativos ou existe uma indisponibilidade momentanea.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@PutMapping("/geral")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
//	public Page<ColaboradorView> conteudo(@RequestBody ColaboradorViewFilter filtro, @PageableDefault(size=10) Pageable page) {
	public RetornoWrapper<ColaboradorView> conteudo(@RequestBody ColaboradorViewFilter filtro,Pageable page) {
		RetornoWrapper<ColaboradorView> retorno = new RetornoWrapper<ColaboradorView>();
		try {
//			retorno.setPage(colaboradoresView.filtrar(filtro,page));
			
			retorno.setConteudo(colaboradoresView.findAll());
			if (colaboradoresView.filtrar(filtro,page).getContent().isEmpty()) {
				ExceptionWrapper except = new ExceptionWrapper();
				except.setCodigo(1);
				except.setMensagem("Não existem dados para serem listados dos Colaboradores");
			}
		} catch (Exception e) {
			ExceptionWrapper except = new ExceptionWrapper();
			except.setCodigo(1);
			except.setMensagem("Ocorreu um erro ao buscar as informações dos Colaboradores");
			retorno.setException(except);
		}
		return retorno;			
	}
	
	@GetMapping("/classificacoes")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public List<ClassificacaoEmpresa> conteudo() {
		return classificacoesEmpresas.findAll();
	}
	
	@GetMapping("/empresas")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<ColaboradorEmpresaResumoView> resumir() {
		RetornoWrapper<ColaboradorEmpresaResumoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(empresasResumo.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações das empresas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/empresas/tiposcriptografias")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<TipoCriptografia> conteudoEmpresaTiposCriptografia() {
		RetornoWrapper<TipoCriptografia> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposCriptografias.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar tipos de criptografia de email");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/parceiros")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<ColaboradorParceiroView> conteudoParceiros() {
		RetornoWrapper<ColaboradorParceiroView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(colaboradoresParceirosView.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações dos parceiros, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_SALVAR')") //TODO: Mudar para salvar depois
	public RetornoWrapper<Colaborador> salvar(@RequestBody Colaborador colaborador, HttpServletResponse response) {
		RetornoWrapper<Colaborador> retorno = new RetornoWrapper<Colaborador>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {

//			System.out.println(">>>>>>>>>>>>>"+colaborador.getColaborador_id()+"<<<<<<<<<<<<<");
			
			colaborador.setColaborador_id(colaboradorService.request_Id(colaborador.getColaborador_id()));
			
			colaborador = colaboradores.save(colaborador);
			
			retorno.setSingle(colaborador);
			
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Erro ao salvar");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(colaborador.getColaborador_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@GetMapping("/{codigo}/{composicao}")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<Colaborador> buscarPeloCodigo(@PathVariable Long codigo,@PathVariable Long composicao) {
		RetornoWrapper<Colaborador> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
//			System.out.println("CODIGO -> "+codigo+" - "+composicao);
//			
			ColaboradorPK id = new ColaboradorPK();
			id.setColaborador_id(codigo);
			id.setComposicao_id(composicao);
			
			Colaborador conteudo = colaboradores.findOne(id);
			retorno.setSingle(conteudo);
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível localizar informações o estamos com alguma indisponibilidade momentanea.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<TipoCliente> tiposClientes() {
		RetornoWrapper<TipoCliente> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(tiposclientes.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível recuperar as informações solicitadas, favor tentar novamente mais tarde!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_DELETAR')")
	public Page<Colaborador> deletar(@RequestBody ColaboradorDelete colaborador, @PageableDefault(size=10) Pageable page) {
		for (Colaborador conteudo : colaborador.getColaboradores()) {
			DeletarColaboradorAction deletarColaborador = new DeletarColaboradorAction();
			deletarColaborador.setColaborador_id(conteudo.getColaborador_id().getColaborador_id());
			
			deletarColaboradores.save(deletarColaborador);
		}
		
		return colaboradores.findAll(page);
	}
	
	@GetMapping("/composicoes/empresas")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<EmpresaComposicaoView> composicaoEmpresas() {
		RetornoWrapper<EmpresaComposicaoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(empresasComposicoes.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/cliente/{cliente_id}/{composicao_cliente_id}")
	@PreAuthorize("hasAuthority('ROLE_CLIENTE_CONTEUDO')")
	public RetornoWrapper<ClienteMinor> conteudoClientePorId(@PathVariable Long cliente_id, @PathVariable Long composicao_cliente_id) {
		RetornoWrapper<ClienteMinor> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			ColaboradorPK id = new ColaboradorPK();
			id.setColaborador_id(cliente_id);
			id.setComposicao_id(composicao_cliente_id);
			retorno.setSingle( clientesMinor.findOne(id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizado um usuário com este id");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}




//@GetMapping
//public ResponseEntity<?> conteudo() {
//	List<Colaborador> categorias = colaboradores.findAll();
//	
//	return !categorias.isEmpty() ? ResponseEntity.ok(categorias) : ResponseEntity.notFound().build();
//	
//}
