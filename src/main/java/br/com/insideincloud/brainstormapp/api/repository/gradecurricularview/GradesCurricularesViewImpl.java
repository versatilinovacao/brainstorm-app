package br.com.insideincloud.brainstormapp.api.repository.gradecurricularview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularView;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularViewFilter;

public class GradesCurricularesViewImpl implements GradesCurricularesViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<GradeCurricularView> filtrar(GradeCurricularViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<GradeCurricularView> criteria = builder.createQuery(GradeCurricularView.class);
		Root<GradeCurricularView> root = criteria.from(GradeCurricularView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<GradeCurricularView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(GradeCurricularViewFilter filtro, CriteriaBuilder builder, Root<GradeCurricularView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getGradecurricular_id())) {
				predicates.add(builder.equal(root.get("gradecurricular_id"), Long.parseLong(filtro.getGradecurricular_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getCurso_id())) {
				predicates.add(builder.equal(root.get("curso_id"), Long.parseLong(filtro.getCurso_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getTurma_id())) {
				predicates.add(builder.equal(root.get("turma_id"), Long.parseLong(filtro.getTurma_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getProfessor_id())) {
				predicates.add(builder.equal(root.get("professor_id"), Long.parseLong(filtro.getProfessor_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getMateria_id())) {
				predicates.add(builder.equal(root.get("materia_id"), Long.parseLong(filtro.getMateria_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getNome_curso())) {
				predicates.add(builder.like(builder.lower(root.get("nome_curso")), "%" + filtro.getNome_curso().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getNome_turma())) {
				predicates.add(builder.like(builder.lower(root.get("nome_turma")), "%" + filtro.getNome_turma().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(filtro.getNome_professor())) {
				predicates.add(builder.like(builder.lower(root.get("nome_professor")), "%" + filtro.getNome_professor().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getNome_materia())) {
				predicates.add(builder.like(builder.lower(root.get("nome_materia")), "%" + filtro.getNome_materia().toLowerCase() + "%"));
			}
						
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(GradeCurricularViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<GradeCurricularView> root = criteria.from(GradeCurricularView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
