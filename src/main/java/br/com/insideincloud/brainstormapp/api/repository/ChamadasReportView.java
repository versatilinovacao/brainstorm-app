package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaReportView;
import br.com.insideincloud.brainstormapp.api.repository.chamadareportview.ChamadasReportViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaReportViewFilter;

@Repository
public interface ChamadasReportView extends JpaRepository<ChamadaReportView,Long>, ChamadasReportViewQuery {
	public Page<ChamadaReportView> filtrar(ChamadaReportViewFilter filtro,Pageable page);

}
