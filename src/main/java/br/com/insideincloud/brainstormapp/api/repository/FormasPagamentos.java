package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.FormaPagamento;

@Repository
public interface FormasPagamentos extends JpaRepository<FormaPagamento, Long> {
	
	@Query("select f from FormaPagamento f where f.descricao = ?1")
	public FormaPagamento findByDescricao(String descricao); 

}
