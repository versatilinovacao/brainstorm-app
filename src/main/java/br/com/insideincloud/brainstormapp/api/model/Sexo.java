package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sexos")
public class Sexo implements Serializable {
	private static final long serialVersionUID = -397575937977073480L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long sexo_id;
	@Column(name="descricao",length=50)
	private String descricao;
	
	public Long getSexo_id() {
		return sexo_id;
	}
	public void setSexo_id(Long sexo_id) {
		this.sexo_id = sexo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sexo_id == null) ? 0 : sexo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sexo other = (Sexo) obj;
		if (sexo_id == null) {
			if (other.sexo_id != null)
				return false;
		} else if (!sexo_id.equals(other.sexo_id))
			return false;
		return true;
	}
	
	
}
