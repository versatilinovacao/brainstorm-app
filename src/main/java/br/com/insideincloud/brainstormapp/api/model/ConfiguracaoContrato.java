package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="configuracoes_contratos")
public class ConfiguracaoContrato implements Serializable {
	private static final long serialVersionUID = 5337370166072355905L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long configuracaocontrato_id;
	private Double desconto_pagamento;
	private Double desconto_sazonal;
	private Double desconto_bolsista;
	
	public Long getConfiguracaocontrato_id() {
		return configuracaocontrato_id;
	}
	public void setConfiguracaocontrato_id(Long configuracaocontrato_id) {
		this.configuracaocontrato_id = configuracaocontrato_id;
	}
	public Double getDesconto_pagamento() {
		return desconto_pagamento;
	}
	public void setDesconto_pagamento(Double desconto_pagamento) {
		this.desconto_pagamento = desconto_pagamento;
	}
	public Double getDesconto_sazonal() {
		return desconto_sazonal;
	}
	public void setDesconto_sazonal(Double desconto_sazonal) {
		this.desconto_sazonal = desconto_sazonal;
	}
	public Double getDesconto_bolsista() {
		return desconto_bolsista;
	}
	public void setDesconto_bolsista(Double desconto_bolsista) {
		this.desconto_bolsista = desconto_bolsista;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((configuracaocontrato_id == null) ? 0 : configuracaocontrato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfiguracaoContrato other = (ConfiguracaoContrato) obj;
		if (configuracaocontrato_id == null) {
			if (other.configuracaocontrato_id != null)
				return false;
		} else if (!configuracaocontrato_id.equals(other.configuracaocontrato_id))
			return false;
		return true;
	}
	
}
