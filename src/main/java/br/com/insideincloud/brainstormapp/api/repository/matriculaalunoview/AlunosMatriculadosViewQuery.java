package br.com.insideincloud.brainstormapp.api.repository.matriculaalunoview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoViewFilter;

public interface AlunosMatriculadosViewQuery {
	public Page<MatriculaAlunoView> filtrar(MatriculaAlunoViewFilter filtro, Pageable page);
}
