package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoResponsavel;
import br.com.insideincloud.brainstormapp.api.model.view.ResponsavelView;
import br.com.insideincloud.brainstormapp.api.model.view.ResponsavellView;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.ResponsaveisView;
import br.com.insideincloud.brainstormapp.api.repository.Responsaveis_View;
import br.com.insideincloud.brainstormapp.api.repository.TiposResponsaveis;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavelViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavellViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/responsaveis")
public class ResponsavelResource {
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private ResponsaveisView responsaveisView;
	
	@Autowired
	private Responsaveis_View responsaveis_view;
	
	@Autowired
	private TiposResponsaveis tipos;
	
//	@PutMapping(value="/colaboradores")
//	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
//	public Page<Colaborador> colaboradores(@RequestBody ColaboradorFilter filtro, @PageableDefault(size=8) Pageable page) {
//		
//		return colaboradores.filtrar(filtro, page);
//	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_RESPONSAVEL_CONTEUDO')")
	public RetornoWrapper<ResponsavelView> conteudoView(@RequestBody ResponsavelViewFilter filtro) {
		System.out.println("------------------------------------------------------------------------");
		RetornoWrapper<ResponsavelView> retorno = new RetornoWrapper<ResponsavelView>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			responsaveisView.filtrar(filtro, page)
			System.out.println(">>>>>>> "+filtro.getColaborador_id());
			if (filtro.getColaborador_id() == null) {
				retorno.setConteudo(responsaveisView.findAll());
			} else {
				retorno.setConteudo(responsaveisView.findResponsaveis( Long.parseLong(filtro.getColaborador_id()), Long.parseLong(filtro.getComposicao_colaborador_id()) ));
			}

//			retorno.setConteudo(responsaveisView.findAll());
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações dos responsáveis, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);			
		}
		
		return retorno;
	}
	
	@PutMapping("/conteudo")
	@PreAuthorize("hasAuthority('ROLE_RESPONSAVEL_CONTEUDO')")
	public RetornoWrapper<ResponsavellView> conteudoView(@RequestBody ResponsavellViewFilter filtro) {
		RetornoWrapper<ResponsavellView> retorno = new RetornoWrapper<ResponsavellView>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		System.out.println("<<<<<<<<<<<<<XXXXXX0XXXXXX>>>>>>>>>>>"+filtro.getColaborador_id()+" - "+filtro.getComposicao_colaborador_id());
		try {
//			responsaveis_view.filtrar(filtro, page)
			if (filtro != null) {
				if (filtro.getColaborador_id() != null && filtro.getComposicao_colaborador_id() != null) {
					List<ResponsavellView> resps = responsaveis_view.findByResponsaveisConteudo(Long.parseLong(filtro.getColaborador_id()), Long.parseLong(filtro.getComposicao_colaborador_id()));
//					System.out.println("<<<<<<<<<<<<<XXXXXX0XXXXXX>>>>>>>>>>>"+resps.size());
					if (! resps.isEmpty()) { 
						retorno.setConteudo(resps);
					} else {
						exception.setCodigo(1);
						exception.setMensagem("Não foi possível localizar responsáveis vinculados, favor verificar se os vinculos foram feitos realmente.");
						retorno.setException(exception);
					}
				} else {
					exception.setCodigo(1);
					exception.setMensagem("Não foi possível localizar responsáveis vinculados, favor verificar se os vinculos foram feitos realmente.");
					retorno.setException(exception);
				}				
			} else {
				exception.setCodigo(1);
				exception.setMensagem("Não foi possível localizar responsáveis vinculados, favor verificar se os vinculos foram feitos realmente.");
				retorno.setException(exception);
			}
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}	
		
		return retorno;
	}
	
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<TipoResponsavel> conteudoTipos(){
		RetornoWrapper<TipoResponsavel> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tipos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de responsável");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
//	@DeleteMapping
//	@PreAuthorize("hasAuthority('ROLE_RESPONSAVEL_DELETAR')")
//	public Page<Colaborador> delete(@RequestBody ColaboradorFilter filtro, @PageableDefault(size=8) Pageable page) {
//		
//	}

}
