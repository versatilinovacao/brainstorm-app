package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="periodos_letivos_inativos_view",schema="escola")
public class PeriodoLetivoInativoView implements Serializable {
	private static final long serialVersionUID = 4359909582719637048L;
	
	@Id
	private Long periodo_letivo_id;
	private String descricao;
	private String ano;
	private Long empresa_id;
	private Boolean status;
	
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		result = prime * result + ((periodo_letivo_id == null) ? 0 : periodo_letivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoLetivoInativoView other = (PeriodoLetivoInativoView) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		if (periodo_letivo_id == null) {
			if (other.periodo_letivo_id != null)
				return false;
		} else if (!periodo_letivo_id.equals(other.periodo_letivo_id))
			return false;
		return true;
	}

}
