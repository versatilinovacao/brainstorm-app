package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="naturezas_servicos_ativas",schema="contabil")
public class NaturezaOpServicoAtivaView implements Serializable {
	private static final long serialVersionUID = 4968632224308874671L;

	@Id
	private Long naturezaopservico_id;

	private Double referencia;
	private String titulo;
	private String descricao;
	
	public Long getNaturezaopservico_id() {
		return naturezaopservico_id;
	}
	public void setNaturezaopservico_id(Long naturezaopservico_id) {
		this.naturezaopservico_id = naturezaopservico_id;
	}
	public Double getReferencia() {
		return referencia;
	}
	public void setReferencia(Double referencia) {
		this.referencia = referencia;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((naturezaopservico_id == null) ? 0 : naturezaopservico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NaturezaOpServicoAtivaView other = (NaturezaOpServicoAtivaView) obj;
		if (naturezaopservico_id == null) {
			if (other.naturezaopservico_id != null)
				return false;
		} else if (!naturezaopservico_id.equals(other.naturezaopservico_id))
			return false;
		return true;
	}
	
}
