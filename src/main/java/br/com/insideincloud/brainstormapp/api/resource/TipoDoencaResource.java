package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.dblink.TipoDoenca;
import br.com.insideincloud.brainstormapp.api.repository.TiposDoencas;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposdoencas")
public class TipoDoencaResource {

	@Autowired
	private TiposDoencas tiposDoencas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIPODOENCA_CONTEUDO')")
	public RetornoWrapper<TipoDoenca> conteudo() {
		RetornoWrapper<TipoDoenca> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposDoencas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de doença.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{tipodoenca_id}")
	@PreAuthorize("hasAuthority('ROLE_TIPODOENCA_CONTEUDO')")
	public RetornoWrapper<TipoDoenca> conteudoPorId(@PathVariable Long tipodoenca_id) {
		RetornoWrapper<TipoDoenca> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( tiposDoencas.findOne(tipodoenca_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de doença.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
