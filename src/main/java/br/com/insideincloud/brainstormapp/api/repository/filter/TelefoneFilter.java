package br.com.insideincloud.brainstormapp.api.repository.filter;

public class TelefoneFilter {
	private String telefone_id;
	private String numero;
	private String ramal;
	private String tipo;
	
	public String getTelefone_id() {
		return telefone_id;
	}
	public void setTelefone_id(String telefone_id) {
		this.telefone_id = telefone_id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	

}
