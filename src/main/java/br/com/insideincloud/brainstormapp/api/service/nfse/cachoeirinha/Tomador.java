package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class Tomador {
	private IdentificacaoTomador IdentificacaoTomador;
	private String RazaoSocial;
	private Endereco Endereco;
	private Contato Contato;

	public String getRazaoSocial() {
		return RazaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		RazaoSocial = razaoSocial;
	}

	public Endereco getEndereco() {
		return Endereco;
	}

	public void setEndereco(Endereco endereco) {
		Endereco = endereco;
	}

	public Contato getContato() {
		return Contato;
	}

	public void setContato(Contato contato) {
		Contato = contato;
	}

	public IdentificacaoTomador getIdentificacaoTomador() {
		return IdentificacaoTomador;
	}

	public void setIdentificacaoTomador(IdentificacaoTomador identificacaoTomador) {
		IdentificacaoTomador = identificacaoTomador;
	}

}
