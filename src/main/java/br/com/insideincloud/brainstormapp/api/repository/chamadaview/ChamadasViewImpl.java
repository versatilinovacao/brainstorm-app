package br.com.insideincloud.brainstormapp.api.repository.chamadaview;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaViewFilter;

public class ChamadasViewImpl implements ChamadasViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ChamadaView> filtrar(ChamadaViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ChamadaView> criteria = builder.createQuery(ChamadaView.class);
		Root<ChamadaView> root = criteria.from(ChamadaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ChamadaView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ChamadaViewFilter filtro, CriteriaBuilder builder, Root<ChamadaView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getAluno_id())) {
				predicates.add(builder.equal(root.get("aluno_id"), Long.parseLong(filtro.getAluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getChamada_aowner_id())) {
				predicates.add(builder.equal(root.get("chamada_aowner_id"), Long.parseLong(filtro.getChamada_aowner_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getChamada_id())) {
				predicates.add(builder.equal(root.get("chamada_id"), Long.parseLong(filtro.getChamada_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getNome_aluno())) {
				predicates.add(builder.like(builder.lower(root.get("nome_aluno")), "%" + filtro.getNome_aluno().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getEvento())) {
				predicates.add(builder.equal(root.get("evento"), LocalDate.parse(filtro.getEvento())));
			}		

			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ChamadaViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ChamadaView> root = criteria.from(ChamadaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}


}
