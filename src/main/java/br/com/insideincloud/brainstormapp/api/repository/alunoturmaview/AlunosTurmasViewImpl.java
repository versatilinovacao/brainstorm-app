package br.com.insideincloud.brainstormapp.api.repository.alunoturmaview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoTurmaView;
import br.com.insideincloud.brainstormapp.api.model.view.AlunoVinculadoTurmaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoTurmaViewFilter;

public class AlunosTurmasViewImpl implements AlunosTurmasViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<AlunoTurmaView> filtrar(AlunoTurmaViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<AlunoTurmaView> criteria = builder.createQuery(AlunoTurmaView.class);
		Root<AlunoTurmaView> root = criteria.from(AlunoTurmaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<AlunoTurmaView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(AlunoTurmaViewFilter filtro, CriteriaBuilder builder, Root<AlunoTurmaView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), Long.parseLong(filtro.getColaborador_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getVinculo())) { 
				predicates.add(builder.equal(root.get("vinculo"), Long.parseLong(filtro.getVinculo())));
//				predicates.add( getAlunosVinculados(Long.parseLong(filtro.getVinculo())) );

			}		

			if (!StringUtils.isEmpty(filtro.getNome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
			}

//			if (!StringUtils.isEmpty(filtro.getMatricula())) {
//				predicates.add(builder.like(builder.lower(root.get("matricula")), "%" + filtro.getMatricula().toLowerCase() + "%"));
//			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(AlunoTurmaViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<AlunoTurmaView> root = criteria.from(AlunoTurmaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}


}
