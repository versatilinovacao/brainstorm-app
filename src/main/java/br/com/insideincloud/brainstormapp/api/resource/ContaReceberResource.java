package br.com.insideincloud.brainstormapp.api.resource;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ContaReceber;
import br.com.insideincloud.brainstormapp.api.model.FormaPagamento;
import br.com.insideincloud.brainstormapp.api.model.Negociacao;
import br.com.insideincloud.brainstormapp.api.model.TipoConta;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberInativaView;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberPendenteView;
import br.com.insideincloud.brainstormapp.api.repository.ContasReceber;
import br.com.insideincloud.brainstormapp.api.repository.ContasReceberAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.ContasReceberInativasView;
import br.com.insideincloud.brainstormapp.api.repository.ContasReceberPendentesView;
import br.com.insideincloud.brainstormapp.api.repository.FormasPagamentos;
import br.com.insideincloud.brainstormapp.api.repository.Negociacoes;
import br.com.insideincloud.brainstormapp.api.repository.TiposContas;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/contasreceber")
public class ContaReceberResource {

	@Autowired
	private ContasReceberAtivasView contasAtivas;
	
	@Autowired
	private ContasReceberInativasView contasInativas;
	
	@Autowired
	private ContasReceber contas;
	
	@Autowired
	private TiposContas tipos;
	
	@Autowired
	private Negociacoes negociacoes;
	
	@Autowired
	private FormasPagamentos formas;
	
	@Autowired
	private ContasReceberPendentesView contasPendentes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaReceberAtivaView> conteudoAtivo() {
		RetornoWrapper<ContaReceberAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasAtivas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma conta ativa, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaReceberInativaView> conteudoInativo() {
		RetornoWrapper<ContaReceberInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasInativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma conta inativa, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaReceber> conteudoPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<ContaReceber> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			ContaReceberPK id = new ContaReceberPK();
//			id.setContareceber_id(codigo);
//			id.setComposicao_id(composicao);
			
			retorno.setSingle( contas.findOne(codigo) );
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar a conta solicitada, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{vencimento}/{devedor_id}/{composicao_devedor_id}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaReceberAtivaView> conteudoVencimentoDevedor(@PathVariable LocalDate vencimento, @PathVariable Long devedor_id, @PathVariable Long composicao_devedor_id) {
		
		return null;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_SALVAR')")
	public RetornoWrapper<ContaReceber> salvar(@RequestBody ContaReceber conta) {
		RetornoWrapper<ContaReceber> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( contas.saveAndFlush(conta) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a conta, verfique e tente novamente.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<TipoConta> tipos() {
		RetornoWrapper<TipoConta> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tipos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum tipo de pagamento, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/negociacoes")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<Negociacao> negociacoes() {
		RetornoWrapper<Negociacao> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( negociacoes.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma forma de negociação, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	} 
	
	@GetMapping("/formas")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<FormaPagamento> formas() {
		RetornoWrapper<FormaPagamento> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( formas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma forma de pagamento, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/pendentes")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaReceberPendenteView> conteudoPendente() {
		RetornoWrapper<ContaReceberPendenteView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasPendentes.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível apresentar listar as contas pendentes, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
