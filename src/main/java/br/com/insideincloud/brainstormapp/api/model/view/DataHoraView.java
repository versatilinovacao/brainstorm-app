package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="data_hora_view",schema="biblioteca")
public class DataHoraView implements Serializable {
	private static final long serialVersionUID = -5866573892028734638L;

	@Id
	private String indice;
	@Column(name="timezone")		
	private LocalDateTime data_hora_view;

	public LocalDateTime getData_hora_view() {
		return data_hora_view;
	}

	public void setData_hora_view(LocalDateTime data_hora_view) {
		this.data_hora_view = data_hora_view;
	}
	
	
	public String getIndice() {
		return indice;
	}

	public void setIndice(String indice) {
		this.indice = indice;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data_hora_view == null) ? 0 : data_hora_view.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataHoraView other = (DataHoraView) obj;
		if (data_hora_view == null) {
			if (other.data_hora_view != null)
				return false;
		} else if (!data_hora_view.equals(other.data_hora_view))
			return false;
		return true;
	}
	
	
}
