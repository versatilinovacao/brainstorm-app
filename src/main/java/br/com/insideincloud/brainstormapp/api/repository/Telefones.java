package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Telefone;
import br.com.insideincloud.brainstormapp.api.repository.telefone.TelefonesQuery;

@Repository
public interface Telefones extends JpaRepository<Telefone,Long>, TelefonesQuery {

}
