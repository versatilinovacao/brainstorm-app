package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="atualizar_usuarios",schema="interface")
public class GrupoSecurityInterface implements Serializable {
	private static final long serialVersionUID = -3826664146274211669L;

	@Id
	private Long gruposecurity_id;
	private Long usuario_id;
	
	public Long getGruposecurity_id() {
		return gruposecurity_id;
	}
	public void setGruposecurity_id(Long gruposecurity_id) {
		this.gruposecurity_id = gruposecurity_id;
	}
	public Long getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Long usuario_id) {
		this.usuario_id = usuario_id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gruposecurity_id == null) ? 0 : gruposecurity_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoSecurityInterface other = (GrupoSecurityInterface) obj;
		if (gruposecurity_id == null) {
			if (other.gruposecurity_id != null)
				return false;
		} else if (!gruposecurity_id.equals(other.gruposecurity_id))
			return false;
		return true;
	}
	
}
