package br.com.insideincloud.brainstormapp.api.repository.telefonecolaboradorview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.TelefoneColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneColaboradorViewFilter;

public interface TelefonesColaboradoresViewQuery {
	public Page<TelefoneColaboradorView> filtrar(TelefoneColaboradorViewFilter filtro, Pageable page);

}
