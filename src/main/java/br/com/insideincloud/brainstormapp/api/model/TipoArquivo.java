package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_arquivos",schema="escola")
public class TipoArquivo implements Serializable {
	private static final long serialVersionUID = -3101043245292009915L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipoarquivo_id;
	private String nome;
	private String sigla;
	
	public Long getTipoarquivo_id() {
		return tipoarquivo_id;
	}
	public void setTipoarquivo_id(Long tipoarquivo_id) {
		this.tipoarquivo_id = tipoarquivo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoarquivo_id == null) ? 0 : tipoarquivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoArquivo other = (TipoArquivo) obj;
		if (tipoarquivo_id == null) {
			if (other.tipoarquivo_id != null)
				return false;
		} else if (!tipoarquivo_id.equals(other.tipoarquivo_id))
			return false;
		return true;
	}
	
}
