package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.view.ServicoAtivoView;
import br.com.insideincloud.brainstormapp.api.repository.ServicosAtivosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.ServicoService;

@RestController
@RequestMapping("/servicos")
public class ServicoResource {
	
	@Autowired
	private ServicoService servicoService;
	
	@Autowired
	private ServicosAtivosView servicosAtivosView;
	
//	@Autowired
//	private ServicosInativosView servicosInativosView;
	
//	@PostMapping
//	@PreAuthorize("hasAuthority('ROLE_SERVICO_SALVAR')")
//	public RetornoWrapper<Servico> salvar(@RequestBody Servico servico) {
//		RetornoWrapper<Servico> retorno = new RetornoWrapper<>();
//		ExceptionWrapper exception = new ExceptionWrapper();
//		
//		try {
//			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> "+servico.getTributacoes().get(1).getServico().getServico_id());
//			retorno.setSingle(servicoService.salvar(servico));
//		} catch (Exception e) {
//			exception.setCodigo(1);
//			exception.setMensagem("Não foi possível salvar o serviço, favor tentar novamente dentro de alguns instantes.");
//			exception.setDebug(""+e);
//			retorno.setException(exception);
//		}
//		
//		return retorno;
//	}
//	
//	@GetMapping("/{codigo}")
//	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
//	public RetornoWrapper<Servico> conteudoPorCodigo(@PathVariable Long codigo) {
//		RetornoWrapper<Servico> retorno = new RetornoWrapper<>();
//		ExceptionWrapper exception = new ExceptionWrapper();
//		
//		try {
//			retorno.setSingle( servicoService.findOne(codigo) );
//		} catch(Exception e) {
//			exception.setCodigo(1);
//			exception.setMensagem("Não foi possível retornar informações do Serviço, favor tentar novamente dentro de alguns instantes.");
//			exception.setDebug(""+e);
//			retorno.setException(exception);
//		}
//		
//		return retorno;
//	}

	@GetMapping("/{municipio}")
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoAtivoView> conteudoAtivo(@PathVariable Long municipio) {
		RetornoWrapper<ServicoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(servicosAtivosView.findByMunicipio(municipio));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações de serviços ativos, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
//	@GetMapping("/inativos")
//	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
//	public RetornoWrapper<ServicoInativoView> conteudoInativo() {
//		RetornoWrapper<ServicoInativoView> retorno = new RetornoWrapper<>();
//		ExceptionWrapper exception = new ExceptionWrapper();
//		
//		try {
//			retorno.setConteudo(servicosInativosView.findAll());
//		} catch(Exception e) {
//			exception.setCodigo(1);
//			exception.setMensagem("Não foi possível retornar as informações de serviços inativos, favor tentar novamente dentro de alguns instantes");
//			exception.setDebug(""+e);
//			retorno.setException(exception);
//		}
//		
//		return retorno;
//	}
	
}
