package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.TelaSistema;

public class TelaSistemaDelete {

	private List<TelaSistema> telassistema;

	public List<TelaSistema> getTelassistema() {
		return telassistema;
	}

	public void setTelassistema(List<TelaSistema> telassistema) {
		this.telassistema = telassistema;
	}
	
	
}
