package br.com.insideincloud.brainstormapp.api.repository.sede;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Sede;
import br.com.insideincloud.brainstormapp.api.repository.filter.SedeFilter;

public interface SedesQuery {
	public Page<Sede> filtrar(SedeFilter filtro, Pageable page);
}
