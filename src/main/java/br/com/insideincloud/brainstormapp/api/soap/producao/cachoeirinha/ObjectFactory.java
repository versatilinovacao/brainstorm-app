
package br.com.insideincloud.brainstormapp.api.soap.producao.cachoeirinha;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.inf.thema.nfse.server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.inf.thema.nfse.server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListarMensagens }
     * 
     */
    public ListarMensagens createListarMensagens() {
        return new ListarMensagens();
    }

    /**
     * Create an instance of {@link ListarMensagensResponse }
     * 
     */
    public ListarMensagensResponse createListarMensagensResponse() {
        return new ListarMensagensResponse();
    }

}
