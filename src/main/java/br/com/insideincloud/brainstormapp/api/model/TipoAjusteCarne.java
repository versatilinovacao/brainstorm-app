package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_ajustes_carnes",schema="financeiro")
public class TipoAjusteCarne implements Serializable {
	private static final long serialVersionUID = 6461638452594288964L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tipoajustecarne_id;
	private String nome;
	private PlanoConta planoconta;
	
	public Long getTipoajustecarne_id() {
		return tipoajustecarne_id;
	}
	public void setTipoajustecarne_id(Long tipoajustecarne_id) {
		this.tipoajustecarne_id = tipoajustecarne_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public PlanoConta getPlanoconta() {
		return planoconta;
	}
	public void setPlanoconta(PlanoConta planoconta) {
		this.planoconta = planoconta;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoajustecarne_id == null) ? 0 : tipoajustecarne_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAjusteCarne other = (TipoAjusteCarne) obj;
		if (tipoajustecarne_id == null) {
			if (other.tipoajustecarne_id != null)
				return false;
		} else if (!tipoajustecarne_id.equals(other.tipoajustecarne_id))
			return false;
		return true;
	}

}
