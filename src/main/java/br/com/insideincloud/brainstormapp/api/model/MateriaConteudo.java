package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="materiaconteudos",schema="escola")
public class MateriaConteudo implements Serializable {
	private static final long serialVersionUID = 556069537734449061L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long materia_conteudo_id;
	private String descricao;
	private BigDecimal carga_horaria;

	public Long getMateria_conteudo_id() {
		return materia_conteudo_id;
	}
	public void setMateria_conteudo_id(Long materia_conteudo_id) {
		this.materia_conteudo_id = materia_conteudo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getCarga_horaria() {
		return carga_horaria;
	}
	public void setCarga_horaria(BigDecimal carga_horaria) {
		this.carga_horaria = carga_horaria;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materia_conteudo_id == null) ? 0 : materia_conteudo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MateriaConteudo other = (MateriaConteudo) obj;
		if (materia_conteudo_id == null) {
			if (other.materia_conteudo_id != null)
				return false;
		} else if (!materia_conteudo_id.equals(other.materia_conteudo_id))
			return false;
		return true;
	}
	
}
