package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="situacoes_atividades",schema="escola")
public class SituacaoAtividade implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long situacaoatividade_id;
	private String nome;
	
	public Long getSituacaoatividade_id() {
		return situacaoatividade_id;
	}
	public void setSituacaoatividade_id(Long situacaoatividade_id) {
		this.situacaoatividade_id = situacaoatividade_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((situacaoatividade_id == null) ? 0 : situacaoatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SituacaoAtividade other = (SituacaoAtividade) obj;
		if (situacaoatividade_id == null) {
			if (other.situacaoatividade_id != null)
				return false;
		} else if (!situacaoatividade_id.equals(other.situacaoatividade_id))
			return false;
		return true;
	}
}
