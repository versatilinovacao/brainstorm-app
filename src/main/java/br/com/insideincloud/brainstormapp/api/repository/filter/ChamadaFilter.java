package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadaFilter {
	private String chamada_id;
	private String caderno_id;
	private String aluno_id;
	private String aproveitamento;
	private String conceito;
	private String nota;
	
	public String getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(String chamada_id) {
		this.chamada_id = chamada_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(String aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getAproveitamento() {
		return aproveitamento;
	}
	public void setAproveitamento(String aproveitamento) {
		this.aproveitamento = aproveitamento;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}

}
