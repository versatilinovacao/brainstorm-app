
package br.com.insideincloud.brainstormapp.api.soap.homologacao.cachoeirinha.entidade.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.inf.thema.nfse.server.entidade.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DadosCadastraisCnpjCpf_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "cnpjCpf");
    private final static QName _DadosCadastraisCodigoBairro_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "codigoBairro");
    private final static QName _DadosCadastraisCep_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "cep");
    private final static QName _DadosCadastraisCodigoLogradouro_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "codigoLogradouro");
    private final static QName _DadosCadastraisComplementoEndereco_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "complementoEndereco");
    private final static QName _DadosCadastraisCodigoCidade_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "codigoCidade");
    private final static QName _DadosCadastraisIdentificacao_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "identificacao");
    private final static QName _DadosCadastraisEndereco_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "endereco");
    private final static QName _DadosCadastraisBairro_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "bairro");
    private final static QName _DadosCadastraisInscricao_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "inscricao");
    private final static QName _DadosCadastraisCidade_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "cidade");
    private final static QName _DadosCadastraisTelefone_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "telefone");
    private final static QName _DadosCadastraisContato_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "contato");
    private final static QName _DadosCadastraisRazaoSocial_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "razaoSocial");
    private final static QName _DadosCadastraisEmail_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "email");
    private final static QName _DadosCadastraisAtividadeCnae_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "atividadeCnae");
    private final static QName _DadosCadastraisNumeroEndereco_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "numeroEndereco");
    private final static QName _DadosCadastraisUf_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "uf");
    private final static QName _DadosCadastraisFantasia_QNAME = new QName("http://entidade.server.nfse.thema.inf.br/xsd", "fantasia");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.inf.thema.nfse.server.entidade.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DadosCadastrais }
     * 
     */
    public DadosCadastrais createDadosCadastrais() {
        return new DadosCadastrais();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "cnpjCpf", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisCnpjCpf(String value) {
        return new JAXBElement<String>(_DadosCadastraisCnpjCpf_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "codigoBairro", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisCodigoBairro(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisCodigoBairro_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "cep", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisCep(String value) {
        return new JAXBElement<String>(_DadosCadastraisCep_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "codigoLogradouro", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisCodigoLogradouro(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisCodigoLogradouro_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "complementoEndereco", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisComplementoEndereco(String value) {
        return new JAXBElement<String>(_DadosCadastraisComplementoEndereco_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "codigoCidade", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisCodigoCidade(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisCodigoCidade_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "identificacao", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisIdentificacao(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisIdentificacao_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "endereco", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisEndereco(String value) {
        return new JAXBElement<String>(_DadosCadastraisEndereco_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "bairro", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisBairro(String value) {
        return new JAXBElement<String>(_DadosCadastraisBairro_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "inscricao", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisInscricao(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisInscricao_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "cidade", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisCidade(String value) {
        return new JAXBElement<String>(_DadosCadastraisCidade_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "telefone", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisTelefone(String value) {
        return new JAXBElement<String>(_DadosCadastraisTelefone_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "contato", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisContato(String value) {
        return new JAXBElement<String>(_DadosCadastraisContato_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "razaoSocial", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisRazaoSocial(String value) {
        return new JAXBElement<String>(_DadosCadastraisRazaoSocial_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "email", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisEmail(String value) {
        return new JAXBElement<String>(_DadosCadastraisEmail_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "atividadeCnae", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisAtividadeCnae(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisAtividadeCnae_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "numeroEndereco", scope = DadosCadastrais.class)
    public JAXBElement<Long> createDadosCadastraisNumeroEndereco(Long value) {
        return new JAXBElement<Long>(_DadosCadastraisNumeroEndereco_QNAME, Long.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "uf", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisUf(String value) {
        return new JAXBElement<String>(_DadosCadastraisUf_QNAME, String.class, DadosCadastrais.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://entidade.server.nfse.thema.inf.br/xsd", name = "fantasia", scope = DadosCadastrais.class)
    public JAXBElement<String> createDadosCadastraisFantasia(String value) {
        return new JAXBElement<String>(_DadosCadastraisFantasia_QNAME, String.class, DadosCadastrais.class, value);
    }

}
