package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoAtivoView;

@Repository
public interface PeriodosLetivosAtivosView extends JpaRepository<PeriodoLetivoAtivoView, Long> {

	@Query("select p from PeriodoLetivoAtivoView p where p.empresa_id = ?1")
	public List<PeriodoLetivoAtivoView> findByPeriodosLetivosAtivos(Long empresa_id);
	
}
