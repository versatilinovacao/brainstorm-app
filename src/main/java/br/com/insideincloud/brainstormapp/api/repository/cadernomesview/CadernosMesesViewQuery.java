package br.com.insideincloud.brainstormapp.api.repository.cadernomesview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.CadernoMesView;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoMesViewFilter;

public interface CadernosMesesViewQuery {
	public Page<CadernoMesView> filtrar(CadernoMesViewFilter filtro, Pageable page);
}
