package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="indices",schema="administracao")
public class Indice implements Serializable {
	private static final long serialVersionUID = -4692074952008739277L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long indice_id;
	private String sigla;
	private String descricao;
	
	public Long getIndice_id() {
		return indice_id;
	}
	public void setIndice_id(Long indice_id) {
		this.indice_id = indice_id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indice_id == null) ? 0 : indice_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Indice other = (Indice) obj;
		if (indice_id == null) {
			if (other.indice_id != null)
				return false;
		} else if (!indice_id.equals(other.indice_id))
			return false;
		return true;
	}
	
}
