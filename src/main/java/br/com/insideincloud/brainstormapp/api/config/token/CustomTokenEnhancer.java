package br.com.insideincloud.brainstormapp.api.config.token;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import br.com.insideincloud.brainstormapp.api.security.UsuarioSistema;

public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		UsuarioSistema usuarioSistema = (UsuarioSistema) authentication.getPrincipal();
		
		Map<String, Object> addInfo = new HashMap<>();
		addInfo.put("usuario_id", usuarioSistema.getUsuario().getUsuario_id());
		addInfo.put("nomecompleto", usuarioSistema.getUsuario().getNomecompleto());
		addInfo.put("status", usuarioSistema.getUsuario().getStatus());
		addInfo.put("empresa", usuarioSistema.getUsuario().getEmpresa());
		addInfo.put("colaborador_id",usuarioSistema.getUsuario().getColaborador_id());
		addInfo.put("composicao_id",usuarioSistema.getUsuario().getComposicao_id());
		
//		addInfo.put("tiposacessos", usuarioSistema.getUsuario().getTiposacessos());
		addInfo.put("cargo_id", usuarioSistema.getUsuario().getCargo_id());
		addInfo.put("descricaocargo",usuarioSistema.getUsuario().getDescricaocargo());
		addInfo.put("foto_thumbnail",usuarioSistema.getUsuario().getFoto_thumbnail());
		addInfo.put("isServer", usuarioSistema.getUsuario().getIsserver());
		addInfo.put("isClient", usuarioSistema.getUsuario().getIsclient());
		addInfo.put("isCounter", usuarioSistema.getUsuario().getIscounter());
		addInfo.put("isSistema", usuarioSistema.getUsuario().getIssistema());
		addInfo.put("isProfessor", usuarioSistema.getUsuario().getIsprofessor());
//		addInfo.put("grupos", usuarioSistema.getUsuario().getGrupos());
		
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);
		
		return accessToken;
	}

}
