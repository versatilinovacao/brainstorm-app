package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAtivaView;

@Repository
public interface MatriculasAtivasView extends JpaRepository<MatriculaAtivaView, Long> {

	@Query("select m from MatriculaAtivaView m where m.curso_id = ?1")
	public List<MatriculaAtivaView> findByMatriculasCursos(Long curso_id);
}
