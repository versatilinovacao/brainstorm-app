package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

//Team
@Entity
@Table(name="periodos_letivos",schema="escola")
public class PeriodoLetivo implements Serializable {
	private static final long serialVersionUID = 578282975607707360L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long periodo_letivo_id;
	@Column(name="descricao",length=100)
	private String descricao;
	@Column(name="ano",length=4)
	private String ano;
	@Column(name="status")
	private boolean status;
//	@Fetch(FetchMode.SELECT)
//	@OneToMany(mappedBy = "periodoletivo", targetEntity = PeriodoLetivoItem.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	private List<PeriodoLetivoItem> periodosletivositens;
	@OneToOne
	@JoinColumn(name="empresa_id")
	private Empresa empresa;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
//	public List<PeriodoLetivoItem> getPeriodosletivositens() {
//		return periodosletivositens;
//	}
//	public void setPeriodosletivositens(List<PeriodoLetivoItem> periodosletivositens) {
//		this.periodosletivositens = periodosletivositens;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((periodo_letivo_id == null) ? 0 : periodo_letivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoLetivo other = (PeriodoLetivo) obj;
		if (periodo_letivo_id == null) {
			if (other.periodo_letivo_id != null)
				return false;
		} else if (!periodo_letivo_id.equals(other.periodo_letivo_id))
			return false;
		return true;
	}
	
}
