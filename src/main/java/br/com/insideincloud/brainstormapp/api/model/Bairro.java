package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="bairros_view",schema="remoto")
public class Bairro implements Serializable {
	private static final long serialVersionUID = -5368561921996071183L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long bairro_id;
	@Column(name="descricao",length=100)
	private String descricao;
	@ManyToOne
	@JoinColumn(name="cidade_id")
	private Cidade cidade;
	
	public Long getBairro_id() {
		return bairro_id;
	}
	public void setBairro_id(Long bairro_id) {
		this.bairro_id = bairro_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bairro_id == null) ? 0 : bairro_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bairro other = (Bairro) obj;
		if (bairro_id == null) {
			if (other.bairro_id != null)
				return false;
		} else if (!bairro_id.equals(other.bairro_id))
			return false;
		return true;
	}
	
	
}
