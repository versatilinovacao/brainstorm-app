package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_materiais")
public class TipoMaterial implements Serializable {
	private static final long serialVersionUID = -1900644269709957275L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_material_id;
	@Column(name="nome",length=100)
	private String nome;
	
	public Long getTipo_material_id() {
		return tipo_material_id;
	}
	public void setTipo_material_id(Long tipo_material_id) {
		this.tipo_material_id = tipo_material_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_material_id == null) ? 0 : tipo_material_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoMaterial other = (TipoMaterial) obj;
		if (tipo_material_id == null) {
			if (other.tipo_material_id != null)
				return false;
		} else if (!tipo_material_id.equals(other.tipo_material_id))
			return false;
		return true;
	}
	
}
