package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alunos_matriculados_view")
public class MatriculaAlunoView implements Serializable {
	private static final long serialVersionUID = -1044792556061055636L;

	@Id
	private Long aluno_id;
	@Column(name="composicao_aluno_id")
	private Long composicaoaluno_id;
	private String nome;
	@Column(name="codigo")
	private String matricula;
	private Long curso_id;
		
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicaoaluno_id() {
		return composicaoaluno_id;
	}
	public void setComposicaoaluno_id(Long composicaoaluno_id) {
		this.composicaoaluno_id = composicaoaluno_id;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatriculaAlunoView other = (MatriculaAlunoView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		return true;
	}
	
}
