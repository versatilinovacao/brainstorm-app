package br.com.insideincloud.brainstormapp.api.repository.logradouro;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Logradouro;
import br.com.insideincloud.brainstormapp.api.repository.filter.LogradouroFilter;

public class LogradourosImpl implements LogradourosQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Logradouro> filtrar(LogradouroFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Logradouro> criteria = builder.createQuery(Logradouro.class);
		Root<Logradouro> root = criteria.from(Logradouro.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<Logradouro> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));
	}
	
	private Predicate[] criarRestricoes(LogradouroFilter filtro, CriteriaBuilder builder, Root<Logradouro> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		System.out.println("FILTRO....:"+filtro.getCep());
		
		if(filtro != null) {
			
			if (filtro.getCep() != null && !StringUtils.isEmpty(filtro.getCep())) {
				predicates.add(builder.equal(root.get("cep"), filtro.getCep()));
			}
			
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}

			System.out.println(" >>> DENTRO DO FILTRO LOGRADOURO BAIRRO <<< ");
			
			if(!StringUtils.isEmpty(filtro.getBairro_id())) {
				predicates.add(builder.equal(root.get("bairro"),Long.parseLong(filtro.getBairro_id())));
			};
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(LogradouroFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Logradouro> root = criteria.from(Logradouro.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
