package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="alunos_view")
public class AlunoView implements Serializable {
	private static final long serialVersionUID = -7713048266535734804L;

	@EmbeddedId
	private ColaboradorPK colaborador_id;
	
	private Long aluno_id;
	private Long composicao_aluno_id;
	private String nome;
//	private String matricula;
	private Long curso_id;
	
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
//	public String getMatricula() {
//		return matricula;
//	}
//	public void setMatricula(String matricula) {
//		this.matricula = matricula;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoView other = (AlunoView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		return true;
	}
	
}
