package br.com.insideincloud.brainstormapp.api.repository.periodoletivocadernoview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoCadernoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoCadernoViewFilter;

public interface PeriodosLetivosCadernosViewQuery {
	public Page<PeriodoLetivoCadernoView> filtrar(PeriodoLetivoCadernoViewFilter filtro, Pageable page);
}
