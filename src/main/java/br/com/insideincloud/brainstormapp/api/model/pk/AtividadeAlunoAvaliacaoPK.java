package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AtividadeAlunoAvaliacaoPK implements Serializable {
	private static final long serialVersionUID = -246491825251123680L;
	
	private Long atividade_id;
	private Long aluno_id;
	private Long composicao_aluno_id;
	
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}

}
