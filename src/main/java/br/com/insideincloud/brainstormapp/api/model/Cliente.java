package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
public class Cliente implements Serializable {
	private static final long serialVersionUID = -6882794432163105843L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cliente_id;
	private String Matricula;
	private Long fichamedica;
	private Long consumocalorico;
	@OneToOne
	@JoinColumn(name="tipo_cliente_id")
	private TipoCliente tipo;	
	@OneToOne
	@JoinColumn(name="configuracaocontrato_id")
	private ConfiguracaoContrato configuracaocontrato;
	private String profissao;
	private Boolean outraescola;
	private String qualoutraescola;
	private String  qualoutraescolatempo;
	private Boolean qualoutraescolaadaptacao;
	@OneToOne
	@JoinColumn(name="guardaaluno_id")
	private GuardaAluno guardaaluno;
	
	public GuardaAluno getGuardaaluno() {
		return guardaaluno;
	}
	public void setGuardaaluno(GuardaAluno guardaaluno) {
		this.guardaaluno = guardaaluno;
	}
	public String getQualoutraescolatempo() {
		return qualoutraescolatempo;
	}
	public void setQualoutraescolatempo(String qualoutraescolatempo) {
		this.qualoutraescolatempo = qualoutraescolatempo;
	}
	public Boolean getQualoutraescolaadaptacao() {
		return qualoutraescolaadaptacao;
	}
	public void setQualoutraescolaadaptacao(Boolean qualoutraescolaadaptacao) {
		this.qualoutraescolaadaptacao = qualoutraescolaadaptacao;
	}
	public Boolean getOutraescola() {
		return outraescola;
	}
	public void setOutraescola(Boolean outraescola) {
		this.outraescola = outraescola;
	}
	public String getQualoutraescola() {
		return qualoutraescola;
	}
	public void setQualoutraescola(String qualoutraescola) {
		this.qualoutraescola = qualoutraescola;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public ConfiguracaoContrato getConfiguracaocontrato() {
		return configuracaocontrato;
	}
	public void setConfiguracaocontrato(ConfiguracaoContrato configuracaocontrato) {
		this.configuracaocontrato = configuracaocontrato;
	}
	public TipoCliente getTipo() {
		return tipo;
	}
	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo;
	}
	public Long getCliente_id() {
		return cliente_id;
	}
	public void setCliente_id(Long cliente_id) {
		this.cliente_id = cliente_id;
	}
	public String getMatricula() {
		return Matricula;
	}
	public void setMatricula(String matricula) {
		Matricula = matricula;
	}
	public Long getFichamedica() {
		return fichamedica;
	}
	public void setFichamedica(Long fichamedica) {
		this.fichamedica = fichamedica;
	}
	public Long getConsumocalorico() {
		return consumocalorico;
	}
	public void setConsumocalorico(Long consumocalorico) {
		this.consumocalorico = consumocalorico;
	}
	
	
}
