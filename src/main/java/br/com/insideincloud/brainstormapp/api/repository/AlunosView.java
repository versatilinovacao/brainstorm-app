package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoView;
import br.com.insideincloud.brainstormapp.api.repository.alunoview.AlunosViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoFilterView;

@Repository
public interface AlunosView extends JpaRepository<AlunoView,Long>, AlunosViewQuery {
	public Page<AlunoView> filtrar(AlunoFilterView filtro, Pageable page);

//	@Query("select a from AlunoView a where a.curso_id != ?1")
	@Query(value="select a.aluno_id,a.composicao_aluno_id,a.nome, min(a.curso_id) as curso_id,a.colaborador_id,a.composicao_id from alunos_view a where coalesce(a.curso_id,0) != ?1 group by a.aluno_id,a.composicao_aluno_id,a.nome,a.colaborador_id,a.composicao_id",nativeQuery = true)
	public List<AlunoView> findByCurso(Long curso_id);
	
}
