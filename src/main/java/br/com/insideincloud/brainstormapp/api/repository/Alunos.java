package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Aluno;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Repository
public interface Alunos extends JpaRepository<Aluno, ColaboradorPK> {

}
