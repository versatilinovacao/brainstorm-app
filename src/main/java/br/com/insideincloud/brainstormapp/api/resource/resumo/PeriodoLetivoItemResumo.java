package br.com.insideincloud.brainstormapp.api.resource.resumo;

public class PeriodoLetivoItemResumo {
	public PeriodoLetivoItemResumo(Long periodo_letivo_item_id,String descricao) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
		this.descricao = descricao;
	}
	
	private Long periodo_letivo_item_id;
	private String descricao;
	
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
