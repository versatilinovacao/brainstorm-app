package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class Prestador {
	private String Cnpj;
	private String InscricaoMunicipal;
	
	public String getCnpj() {
		return Cnpj;
	}
	public void setCnpj(String cnpj) {
		Cnpj = cnpj;
	}
	public String getInscricaoMunicipal() {
		return InscricaoMunicipal;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		InscricaoMunicipal = inscricaoMunicipal;
	}

}
