package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Repository
public interface ColaboradoresMinor extends JpaRepository<ColaboradorMinor, ColaboradorPK> {

}
