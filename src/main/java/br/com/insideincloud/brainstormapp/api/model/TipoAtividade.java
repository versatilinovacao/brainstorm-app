package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_atividades",schema="escola")
public class TipoAtividade implements Serializable {
	private static final long serialVersionUID = 1043511266090513712L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipoatividade_id;
	private String descricao;
	
	public Long getTipoatividade_id() {
		return tipoatividade_id;
	}
	public void setTipoatividade_id(Long tipoatividade_id) {
		this.tipoatividade_id = tipoatividade_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoatividade_id == null) ? 0 : tipoatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAtividade other = (TipoAtividade) obj;
		if (tipoatividade_id == null) {
			if (other.tipoatividade_id != null)
				return false;
		} else if (!tipoatividade_id.equals(other.tipoatividade_id))
			return false;
		return true;
	}
	
}
