package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.ChamadaAluno;
import br.com.insideincloud.brainstormapp.api.repository.ChamadasAlunos;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaAlunoFilter;

@RestController
@RequestMapping("/chamadasalunos")
public class ChamadaAlunoResource {
	
	@Autowired
	private ChamadasAlunos chamadasalunos;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADAALUNO_CONTEUDO')")
	public Page<ChamadaAluno> conteudo(@RequestBody ChamadaAlunoFilter filtro, Pageable page) {
		return chamadasalunos.filtrar(filtro, page);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADAALUNO_SALVAR')")
	public ResponseEntity<ChamadaAluno> salvar(@RequestBody ChamadaAluno chamadaaluno, HttpServletResponse response) {
		chamadaaluno = chamadasalunos.saveAndFlush(chamadaaluno);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(chamadaaluno.getChamada_aluno_id()).toUri();
		response.setHeader("Location",uri.toASCIIString());
		return ResponseEntity.created(uri).body(chamadaaluno);
	} 
	
}
