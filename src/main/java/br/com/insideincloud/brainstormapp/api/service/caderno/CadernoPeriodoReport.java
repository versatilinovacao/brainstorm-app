package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.util.ArrayList;
import java.util.List;

public class CadernoPeriodoReport {
	private List<CadernoReport> cadernos = new ArrayList<>();

	public List<CadernoReport> getCadernos() {
		return cadernos;
	}

	public void setCadernos(List<CadernoReport> cadernos) {
		this.cadernos = cadernos;
	}
	
	
}
