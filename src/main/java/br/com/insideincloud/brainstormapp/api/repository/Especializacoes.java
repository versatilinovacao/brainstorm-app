package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Especialidade;
import br.com.insideincloud.brainstormapp.api.repository.especialidade.EspecializacoesQuery;

@Repository
public interface Especializacoes extends JpaRepository<Especialidade,Long>, EspecializacoesQuery {

}
