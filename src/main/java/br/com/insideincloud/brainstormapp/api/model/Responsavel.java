package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="responsaveis")
public class Responsavel implements Serializable {
	private static final long serialVersionUID = 8590863868203236290L;

	@Id
	private Long responsavel_id;
	
	private String nome;

	@OneToOne
	@JoinColumn(name="tiporesponsavel_id")
	private TipoResponsavel tipo;
	
//	@ManyToMany(mappedBy="responsaveis",fetch = FetchType.EAGER)
//	private List<Colaborador> colaboradores;

	public TipoResponsavel getTipo() {
		return tipo;
	}

	public void setTipo(TipoResponsavel tipo) {
		this.tipo = tipo;
	}

	public Long getResponsavel_id() {
		return responsavel_id;
	}

	public void setResponsavel_id(Long responsavel_id) {
		this.responsavel_id = responsavel_id;
	}

//	public List<Colaborador> getColaboradores() {
//		return colaboradores;
//	}
//
//	public void setColaboradores(List<Colaborador> colaboradores) {
//		this.colaboradores = colaboradores;
//	}
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((responsavel_id == null) ? 0 : responsavel_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Responsavel other = (Responsavel) obj;
		if (responsavel_id == null) {
			if (other.responsavel_id != null)
				return false;
		} else if (!responsavel_id.equals(other.responsavel_id))
			return false;
		return true;
	}
	
	
}
