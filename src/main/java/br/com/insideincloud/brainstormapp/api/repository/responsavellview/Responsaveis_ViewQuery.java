package br.com.insideincloud.brainstormapp.api.repository.responsavellview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.ResponsavellView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavellViewFilter;

public interface Responsaveis_ViewQuery {
	public Page<ResponsavellView> filtrar(ResponsavellViewFilter filtro, Pageable page);

}
