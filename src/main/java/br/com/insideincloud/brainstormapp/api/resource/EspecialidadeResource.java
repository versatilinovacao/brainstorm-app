package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Especialidade;
import br.com.insideincloud.brainstormapp.api.model.view.EspecialidadeAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.EspecialidadeInativaView;
import br.com.insideincloud.brainstormapp.api.repository.EspecialidadesAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.EspecialidadesInativasView;
import br.com.insideincloud.brainstormapp.api.repository.Especializacoes;
import br.com.insideincloud.brainstormapp.api.repository.filter.EspecialidadeFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/especializacoes")
public class EspecialidadeResource {
	@Autowired
	private Especializacoes especializacoes;
	
	@Autowired
	private EspecialidadesAtivasView especialidadesAtivas;
	
	@Autowired
	private EspecialidadesInativasView especialidadesInativas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIZACAO_CONTEUDO')")
	public RetornoWrapper<EspecialidadeAtivaView> conteudo() {
		RetornoWrapper<EspecialidadeAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(especialidadesAtivas.findAll());
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIZACAO_CONTEUDO')")
	public RetornoWrapper<EspecialidadeInativaView> conteudoInativo() {
		RetornoWrapper<EspecialidadeInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(especialidadesInativas.findAll());
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
		
	}

	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIZACAO_CONTEUDO')")
	public RetornoWrapper<Especialidade> buscaPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Especialidade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(especializacoes.findOne(codigo));
		}  catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIZACAO_SALVAR')")
	public RetornoWrapper<Especialidade> salvar(@RequestBody Especialidade especialidade, HttpServletResponse response) {
		RetornoWrapper<Especialidade> retorno = new RetornoWrapper<Especialidade>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			especialidade = especializacoes.saveAndFlush(especialidade);
			retorno.setSingle(especialidade);
						
		} catch (Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível salvar a especialidade, favor verificar suas informações e tentar novamente!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(especialidade.getEspecializacao_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_ESPECIALIZACAO_DELETAR')")
	public Page<Especialidade> deletar(@RequestBody EspecialidadeFilter filtro, @PageableDefault(size=5) Pageable page) {
		especializacoes.delete( Long.parseLong(filtro.getEspecializacao_id()) );
		filtro.setDescricao("");
		filtro.setEspecializacao_id("");
		return especializacoes.filtrar(filtro, page);
	};

}
