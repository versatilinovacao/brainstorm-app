package br.com.insideincloud.brainstormapp.api.repository.caderno;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Caderno;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoFilter;

public interface CadernosQuery {
	public Page<Caderno> filtrar(CadernoFilter filtro, Pageable page);
}
