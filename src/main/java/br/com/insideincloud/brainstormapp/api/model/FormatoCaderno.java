package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="formato_caderno",schema="empresa")
public class FormatoCaderno implements Serializable {
	private static final long serialVersionUID = -6872931804227277256L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long formato_caderno_id;
	private String descricao;
	
	public Long getFormato_caderno_id() {
		return formato_caderno_id;
	}
	public void setFormato_caderno_id(Long formato_caderno_id) {
		this.formato_caderno_id = formato_caderno_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formato_caderno_id == null) ? 0 : formato_caderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormatoCaderno other = (FormatoCaderno) obj;
		if (formato_caderno_id == null) {
			if (other.formato_caderno_id != null)
				return false;
		} else if (!formato_caderno_id.equals(other.formato_caderno_id))
			return false;
		return true;
	}
	
}
