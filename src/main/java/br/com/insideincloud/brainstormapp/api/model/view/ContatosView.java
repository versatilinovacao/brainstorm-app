package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contatosview")
public class ContatosView implements Serializable{
	private static final long serialVersionUID = -1148119388583116659L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long contato_id;
	@Column(name="nome")
	private String nome;
	@Column(name="telefone")
	private String telefone;
	@Column(name="ramal")
	private String ramal;
	@Column(name="tipo")
	private String tipo;
	@Column(name="email")
	private String email;
	
	public Long getContato_id() {
		return contato_id;
	}
	public void setContato_id(Long contato_id) {
		this.contato_id = contato_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contato_id == null) ? 0 : contato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContatosView other = (ContatosView) obj;
		if (contato_id == null) {
			if (other.contato_id != null)
				return false;
		} else if (!contato_id.equals(other.contato_id))
			return false;
		return true;
	}
	
	
	
}
