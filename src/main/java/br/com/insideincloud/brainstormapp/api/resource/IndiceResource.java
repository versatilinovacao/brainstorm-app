package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Indice;
import br.com.insideincloud.brainstormapp.api.repository.Indices;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/indices")
public class IndiceResource {
	
	@Autowired
	private Indices indices;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_INDICE_CONTEUDO')")
	public RetornoWrapper<Indice> conteudo() {
		RetornoWrapper<Indice> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( indices.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum indice.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
 		
		return retorno;
	}
}
