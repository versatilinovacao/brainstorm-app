package br.com.insideincloud.brainstormapp.api.repository.alunoturmaview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoTurmaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoTurmaViewFilter;

public interface AlunosTurmasViewQuery {
	public Page<AlunoTurmaView> filtrar(AlunoTurmaViewFilter filtro, Pageable page);
}
