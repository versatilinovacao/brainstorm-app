package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="arquivos_por_atividades_view",schema="escola")
public class ArquivoPorAtividadeView implements Serializable {
	private static final long serialVersionUID = -7456391651049047941L;
	
	@Id
	private Long arquivoporatividade_id;
    private Long aluno_id;
    private Long composicao_aluno_id;
    private Long arquivo_id;
    private Long atividade_id;
    private String nome;
    private String extensao;
    private String tipo;
    private Integer tamanho;
    private LocalDateTime registro;
    private String url;
    private String arquivo;
     
	public Long getArquivoporatividade_id() {
		return arquivoporatividade_id;
	}
	public void setArquivoporatividade_id(Long arquivoporatividade_id) {
		this.arquivoporatividade_id = arquivoporatividade_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public Long getArquivo_id() {
		return arquivo_id;
	}
	public void setArquivo_id(Long arquivo_id) {
		this.arquivo_id = arquivo_id;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getTamanho() {
		return tamanho;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	public LocalDateTime getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDateTime registro) {
		this.registro = registro;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivoporatividade_id == null) ? 0 : arquivoporatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoPorAtividadeView other = (ArquivoPorAtividadeView) obj;
		if (arquivoporatividade_id == null) {
			if (other.arquivoporatividade_id != null)
				return false;
		} else if (!arquivoporatividade_id.equals(other.arquivoporatividade_id))
			return false;
		return true;
	}     
     
}
