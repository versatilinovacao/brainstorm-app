package br.com.insideincloud.brainstormapp.api.repository.telefone;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Telefone;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneFilter;

public interface TelefonesQuery {
	public Page<Telefone> filtrar(TelefoneFilter filtro,Pageable page);

}
