package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.io.Serializable;
import java.util.List;

public class CadernoReport implements Serializable {
	private static final long serialVersionUID = -1676205517077459893L;
	
	private String escola;
	private String descricao;
	private String nome_caderno;
	private String ano;
	private String nome_professor;
	private String cnpj;
	private String rua;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	private String pais;
	private String telefone;
	private String email;
	private List<CadernoPeriodo> meses;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getEscola() {
		return escola;
	}
	public void setEscola(String escola) {
		this.escola = escola;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getNome_caderno() {
		return nome_caderno;
	}
	public void setNome_caderno(String nome_caderno) {
		this.nome_caderno = nome_caderno;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		if (ano == null) { this.ano = "";};
		this.ano = ano;
	}
	public String getNome_professor() {
		return nome_professor;
	}
	public void setNome_professor(String nome_professor) {
		this.nome_professor = nome_professor;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<CadernoPeriodo> getMeses() {
		return meses;
	}
	public void setMeses(List<CadernoPeriodo> meses) {
		this.meses = meses;
	}
	
}
