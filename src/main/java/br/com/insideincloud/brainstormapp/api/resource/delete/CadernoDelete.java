package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.Caderno;

public class CadernoDelete {
	private List<Caderno> cadernos;
	
	public List<Caderno> getCadernos() {
		return cadernos;
	}
	
	public void setCadernos(List<Caderno> cadernos) {
		this.cadernos = cadernos;
	}
}

