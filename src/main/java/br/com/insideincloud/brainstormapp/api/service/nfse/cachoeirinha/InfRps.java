package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class InfRps {
	private Id Id;
	private IdentificacaoRps IdentificacaoRps;
	private String DataEmissao;
	private String NaturezaOperacao;
	private String OptanteSimplesNacional;
	private String IncentivadorCultural;
	private String Status;
	private Servico Servico;
	private Prestador Prestador;
	private Tomador Tomador;
	
	public Id getId() {
		return Id;
	}
	public void setId(Id id) {
		Id = id;
	}
	public Tomador getTomador() {
		return Tomador;
	}
	public void setTomador(Tomador tomador) {
		Tomador = tomador;
	}
	public Prestador getPrestador() {
		return Prestador;
	}
	public void setPrestador(Prestador prestador) {
		Prestador = prestador;
	}
	public Servico getServico() {
		return Servico;
	}
	public void setServico(Servico servico) {
		Servico = servico;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public IdentificacaoRps getIdentificacaoRps() {
		return IdentificacaoRps;
	}
	public void setIdentificacaoRps(IdentificacaoRps identificacaoRps) {
		IdentificacaoRps = identificacaoRps;
	}
	public String getDataEmissao() {
		return DataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		DataEmissao = dataEmissao;
	}
	public String getNaturezaOperacao() {
		return NaturezaOperacao;
	}
	public void setNaturezaOperacao(String naturezaOperacao) {
		NaturezaOperacao = naturezaOperacao;
	}
	public String getOptanteSimplesNacional() {
		return OptanteSimplesNacional;
	}
	public void setOptanteSimplesNacional(String optanteSimplesNacional) {
		OptanteSimplesNacional = optanteSimplesNacional;
	}
	public String getIncentivadorCultural() {
		return IncentivadorCultural;
	}
	public void setIncentivadorCultural(String incentivadorCultural) {
		IncentivadorCultural = incentivadorCultural;
	}
	
}
