package br.com.insideincloud.brainstormapp.api.repository.filter;

public class TaxaSelicFilter {
    private String taxaselic_id;
    private String movimento_inical;
    private String movimento_final;
    
	public String getTaxaselic_id() {
		return taxaselic_id;
	}
	public void setTaxaselic_id(String taxaselic_id) {
		this.taxaselic_id = taxaselic_id;
	}
	public String getMovimento_inical() {
		return movimento_inical;
	}
	public void setMovimento_inical(String movimento_inical) {
		this.movimento_inical = movimento_inical;
	}
	public String getMovimento_final() {
		return movimento_final;
	}
	public void setMovimento_final(String movimento_final) {
		this.movimento_final = movimento_final;
	}
    

    
}