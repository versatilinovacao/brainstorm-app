package br.com.insideincloud.brainstormapp.api.service.nfse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.repository.CertificadosA1;
import br.com.insideincloud.brainstormapp.api.repository.LotesRps;

@Component
public class EnviarLoteRps {
	private Long lote;
	
	@Autowired
	private LotesRps lotes;
	
	@Autowired
	private CertificadosA1 certificadosA1;
	public Long getLote() {
		return lote;
	}
	public void setLote(Long lote) {
		this.lote = lote;
	}	
	
}
