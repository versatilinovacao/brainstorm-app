package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_criptografia",schema="public")
public class TipoCriptografia implements Serializable {
	private static final long serialVersionUID = 4890041784120073140L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipocriptografia_id;
	private String nome;
	private String sigla;
	
	public Long getTipocriptografia_id() {
		return tipocriptografia_id;
	}
	public void setTipocriptografia_id(Long tipocriptografia_id) {
		this.tipocriptografia_id = tipocriptografia_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipocriptografia_id == null) ? 0 : tipocriptografia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoCriptografia other = (TipoCriptografia) obj;
		if (tipocriptografia_id == null) {
			if (other.tipocriptografia_id != null)
				return false;
		} else if (!tipocriptografia_id.equals(other.tipocriptografia_id))
			return false;
		return true;
	}
	
}
