package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.CertificadoA1PK;

@Entity
@Table(name="certificados_a1",schema="empresa")
public class CertificadoA1 implements Serializable {
	private static final long serialVersionUID = -2196275955911996458L;
	
	@EmbeddedId
	private CertificadoA1PK certificado_id;
	private String path;
	private String senha;

	public CertificadoA1PK getCertificado_id() {
		return certificado_id;
	}
	public void setCertificado_id(CertificadoA1PK certificado_id) {
		this.certificado_id = certificado_id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((certificado_id == null) ? 0 : certificado_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CertificadoA1 other = (CertificadoA1) obj;
		if (certificado_id == null) {
			if (other.certificado_id != null)
				return false;
		} else if (!certificado_id.equals(other.certificado_id))
			return false;
		return true;
	}
	
}
