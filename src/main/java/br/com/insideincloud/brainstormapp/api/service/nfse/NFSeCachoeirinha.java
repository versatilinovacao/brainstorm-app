package br.com.insideincloud.brainstormapp.api.service.nfse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.LoteRps;
import br.com.insideincloud.brainstormapp.api.repository.LotesRps;
import br.com.insideincloud.brainstormapp.api.soap.nfse.abrasf.EnviarLoteRpsEnvio;
import br.com.insideincloud.brainstormapp.api.soap.nfse.abrasf.TcLoteRps;

@Component
public class NFSeCachoeirinha {
	@Autowired
	private LotesRps lotes;
	
	private String remessaXML;
	
	protected static void EnviarRPS() {
		
	}
	
	public void ConsultarRPS() {
		
	}
	
	public void CancelarRPS() {
		
	}
	
	public void ConsultaCadastro() {
		
	}
	
	public void RemessaXML() {
		EnviarLoteRpsEnvio enviarLoteRpsEnvio = new EnviarLoteRpsEnvio();
		TcLoteRps tcLoteRps = new TcLoteRps();
		
		LoteRps l = lotes.findOne(1L);

		
		tcLoteRps.setCnpj(l.getCnpj());
		tcLoteRps.setInscricaoMunicipal(l.getInscricaomunicipal());
		
		enviarLoteRpsEnvio.setLoteRps(tcLoteRps);
		
	}
	
	public void RespostaXML() {}

}
