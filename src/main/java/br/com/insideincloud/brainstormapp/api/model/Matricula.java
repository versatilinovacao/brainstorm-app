package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;
import br.com.insideincloud.brainstormapp.api.model.minor.CursoMinor;

@Entity
@Table(name="matriculas")
public class Matricula implements Serializable {
	private static final long serialVersionUID = 6484903649800216621L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long matricula_id;
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private ColaboradorMinor colaborador;
	@OneToOne
	@JoinColumn(name="curso_id")
	private CursoMinor curso;
	@OneToOne		 
	@JoinColumn(name="situacao_matricula_id")
	private SituacaoMatricula situacaomatricula;
	@Column(name="codigo")
	private String matricula;
	@Column(name="competencia_inicial")
	private LocalDate competenciainicial;
	@Column(name="competencia_final")
	private LocalDate competenciafinal;
	private boolean status;
		
	public CursoMinor getCurso() {
		return curso;
	}
	public void setCurso(CursoMinor curso) {
		this.curso = curso;
	}
	public ColaboradorMinor getColaborador() {
		return colaborador;
	}
	public void setColaborador(ColaboradorMinor colaborador) {
		this.colaborador = colaborador;
	}
	public Long getMatricula_id() {
		return matricula_id;
	}
	public void setMatricula_id(Long matricula_id) {
		this.matricula_id = matricula_id;
	}
	public SituacaoMatricula getSituacaomatricula() {
		return situacaomatricula;
	}
	public void setSituacaomatricula(SituacaoMatricula situacaomatricula) {
		this.situacaomatricula = situacaomatricula;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public LocalDate getCompetenciainicial() {
		return competenciainicial;
	}
	public void setCompetenciainicial(LocalDate competenciainicial) {
		this.competenciainicial = competenciainicial;
	}
	public LocalDate getCompetenciafinal() {
		return competenciafinal;
	}
	public void setCompetenciafinal(LocalDate competenciafinal) {
		this.competenciafinal = competenciafinal;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((matricula_id == null) ? 0 : matricula_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matricula other = (Matricula) obj;
		if (matricula_id == null) {
			if (other.matricula_id != null)
				return false;
		} else if (!matricula_id.equals(other.matricula_id))
			return false;
		return true;
	}	
	
}
