package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alunos_turmas_view")
public class AlunoTurmaView implements Serializable {
	private static final long serialVersionUID = -2892579844807350921L;

//	@EmbeddedId
//	private ColaboradorPK colaborador_id;

	@Id
	private Long colaborador_id;
	private Long composicao_id;
	
	
	private Long aluno_id;
	@Column(name="composicao_aluno_id")
	private Long composicaoaluno_id;
	private String nome;
	private String matricula;
	private Long vinculo;
	
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicaoaluno_id() {
		return composicaoaluno_id;
	}
	public void setComposicaoaluno_id(Long composicao_aluno_id) {
		this.composicaoaluno_id = composicao_aluno_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public Long getVinculo() {
		return vinculo;
	}
	public void setVinculo(Long vinculo) {
		this.vinculo = vinculo;
	}
	@Override
	public int hashCode() {
		return Objects.hash(colaborador_id, composicao_id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AlunoTurmaView))
			return false;
		AlunoTurmaView other = (AlunoTurmaView) obj;
		return Objects.equals(colaborador_id, other.colaborador_id)
				&& Objects.equals(composicao_id, other.composicao_id);
	}	
		
}
