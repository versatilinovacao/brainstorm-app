package br.com.insideincloud.brainstormapp.api.repository.filter;

public class PeriodoLetivoFilter {
	private String periodo_letivo_id;
	private String descricao;
	private String status;
	private String ano;
	private String empresa_id;
	
	public String getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(String empresa_id) {
		this.empresa_id = empresa_id;
	}

	public String getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}

	public void setPeriodo_letivo_id(String periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
	
		
}
