package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadoFilter {
	private String chamado_id;
	private String chamado_composto_id;
	private String colaborador_id;
	private String titulo;
	private String timeline;
	
	
	public String getTimeline() {
		return timeline;
	}
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}
	public String getChamado_id() {
		return chamado_id;
	}
	public void setChamado_id(String chamado_id) {
		this.chamado_id = chamado_id;
	}
	public String getChamado_composto_id() {
		return chamado_composto_id;
	}
	public void setChamado_composto_id(String chamado_composto_id) {
		this.chamado_composto_id = chamado_composto_id;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	
}
