package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.TipoMateria;

@Entity
@Table(name="materias_inativas_view",schema="escola")
public class MateriaInativaView implements Serializable {
	private static final long serialVersionUID = -1558636669463117634L;

	@Id
	private Long materia_id;
	private String descricao;
	@OneToOne
	@JoinColumn(name="tipo_materia_id")
	private TipoMateria tipomateria;
	private Boolean status;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public TipoMateria getTipomateria() {
		return tipomateria;
	}
	public void setTipomateria(TipoMateria tipomateria) {
		this.tipomateria = tipomateria;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materia_id == null) ? 0 : materia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MateriaInativaView other = (MateriaInativaView) obj;
		if (materia_id == null) {
			if (other.materia_id != null)
				return false;
		} else if (!materia_id.equals(other.materia_id))
			return false;
		return true;
	}

}
