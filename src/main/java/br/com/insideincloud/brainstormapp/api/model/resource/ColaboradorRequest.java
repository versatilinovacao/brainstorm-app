package br.com.insideincloud.brainstormapp.api.model.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.Sequencia;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.Sequencias;

@Component
public class ColaboradorRequest {

	@Autowired
	private Colaboradores colaborador;
	
	@Autowired
	private Sequencias sequencias;
	
	public ColaboradorPK chavePrimaria(ColaboradorPK id) {
		System.out.println("Entrei aqui -> "+id);
		ColaboradorPK new_id = new ColaboradorPK();

		if (id == null) {
			new_id.setColaborador_id(getSequencia("COLABORADOR"));
			new_id.setComposicao_id(0L);
		} else {
			if (id.getColaborador_id() == null) {
				new_id.setColaborador_id(getSequencia("COLABORADOR"));
				new_id.setComposicao_id(id.getComposicao_id());
			} else {
				new_id.setColaborador_id(id.getColaborador_id());
				new_id.setComposicao_id(id.getComposicao_id());
			}
		}
		
		return new_id;
	}
	
	private Long getSequencia(String sequencia) {
		Sequencia sq = new Sequencia();
		try {
			sq = sequencias.getOne(sequencia);
			sq.setSequencia_id(sq.getSequencia_id()+1L);
			System.out.println("<<<<<<<<<>>>>>>>>>"+sq);
		} catch (Exception e) {
			sq = new Sequencia();
			sq.setSequencia("COLABORADOR");
			System.out.println("<<<<<<<<<>>>>>>>>>"+sequencia);
			sq.setSequencia_id(1L);
		}
		sq = sequencias.saveAndFlush(sq);
		
		return sq.getSequencia_id();
	}
}
