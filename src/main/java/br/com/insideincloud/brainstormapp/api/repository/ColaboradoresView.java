package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.colaboradorview.ColaboradoresViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorViewFilter;

@Repository
public interface ColaboradoresView extends JpaRepository<ColaboradorView,Long>, ColaboradoresViewQuery {
	public Page<ColaboradorView> filtrar(ColaboradorViewFilter filtro, Pageable page);
}
