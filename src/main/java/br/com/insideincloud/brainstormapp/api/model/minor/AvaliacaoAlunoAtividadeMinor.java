package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.Aluno;
import br.com.insideincloud.brainstormapp.api.model.SituacaoAtividade;

@Entity
@Table(name="avaliacao_aluno_atividade",schema="escola")
public class AvaliacaoAlunoAtividadeMinor implements Serializable {
	private static final long serialVersionUID = 9209698560878020034L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long avaliacaoalunoatividade_id;
	@OneToOne
	@JoinColumns({@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")})
	private Aluno aluno;
	@OneToOne
	@JoinColumn(name="atividade_id")
	private AtividadeMinor atividade;
	private Integer nota;
	private String conceito;
	@Column(name="avaliacao_descritiva")
	private String avaliacaodescritiva;
	@OneToOne
	@JoinColumn(name="situacaoatividade_id")
	private SituacaoAtividade situacao;
	private String transcricao;
	private Boolean revisar;
	
	public Long getAvaliacaoalunoatividade_id() {
		return avaliacaoalunoatividade_id;
	}
	public void setAvaliacaoalunoatividade_id(Long avaliacaoalunoatividade_id) {
		this.avaliacaoalunoatividade_id = avaliacaoalunoatividade_id;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public AtividadeMinor getAtividade() {
		return atividade;
	}
	public void setAtividade(AtividadeMinor atividade) {
		this.atividade = atividade;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getAvaliacaodescritiva() {
		return avaliacaodescritiva;
	}
	public void setAvaliacaodescritiva(String avaliacaodescritiva) {
		this.avaliacaodescritiva = avaliacaodescritiva;
	}
	public SituacaoAtividade getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoAtividade situacao) {
		this.situacao = situacao;
	}
	public String getTranscricao() {
		return transcricao;
	}
	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
	public Boolean getRevisar() {
		return revisar;
	}
	public void setRevisar(Boolean revisar) {
		this.revisar = revisar;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avaliacaoalunoatividade_id == null) ? 0 : avaliacaoalunoatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoAlunoAtividadeMinor other = (AvaliacaoAlunoAtividadeMinor) obj;
		if (avaliacaoalunoatividade_id == null) {
			if (other.avaliacaoalunoatividade_id != null)
				return false;
		} else if (!avaliacaoalunoatividade_id.equals(other.avaliacaoalunoatividade_id))
			return false;
		return true;
	}

}
