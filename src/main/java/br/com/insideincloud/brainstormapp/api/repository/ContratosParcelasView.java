package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ContratoParcelaView;

@Repository
public interface ContratosParcelasView extends JpaRepository<ContratoParcelaView, Long> {
	
	@Query("select p from ContratoParcelaView p where p.contrato_id = ?1")
	public List<ContratoParcelaView> findByContrato(Long contrato_id);

}
