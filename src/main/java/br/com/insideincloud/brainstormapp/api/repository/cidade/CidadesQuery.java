package br.com.insideincloud.brainstormapp.api.repository.cidade;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Cidade;
import br.com.insideincloud.brainstormapp.api.repository.filter.CidadeFilter;

public interface CidadesQuery {
	public Page<Cidade> filtrar(CidadeFilter filtro, Pageable page); 
}
