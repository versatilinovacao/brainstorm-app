package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

/*
 * public interface Colaboradores extends JpaRepository<Colaborador,ColaboradorPK>, ColaboradoresQuery {
 * */

@Repository
public interface Colaboradores extends JpaRepository<Colaborador,ColaboradorPK> {
	
	@Query("select c from Colaborador c where c.colaborador_id.colaborador_id = ?1 and c.colaborador_id.composicao_id = ?2")
	public Colaborador findByColaborador(Long colaborador_id, Long composicao_id);
}
