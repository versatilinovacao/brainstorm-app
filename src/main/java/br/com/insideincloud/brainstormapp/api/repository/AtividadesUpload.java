package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.AtividadeUpload;

@Repository
public interface AtividadesUpload extends JpaRepository<AtividadeUpload, Long> {
	
	@Query("select a from AtividadeUpload a where a.atividade.atividade_id = ?1")
	public List<AtividadeUpload> findAllAtividades(Long atividade_id);
	
	@Query("select a from AtividadeUpload a where a.atividade.atividade_id = ?1 and a.nome = ?2")
	public AtividadeUpload findByArquivo(Long atividade_id, String nome);

}
