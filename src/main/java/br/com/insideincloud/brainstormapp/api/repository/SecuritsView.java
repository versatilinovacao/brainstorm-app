package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.model.view.SecurityView;

@Repository
public interface SecuritsView extends JpaRepository<SecurityView,Long> {

}
