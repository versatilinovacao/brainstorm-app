package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ProfessorGradeViewFilter {
	private String professor_id;
	private String nome;
	
	public String getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(String professor_id) {
		this.professor_id = professor_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
