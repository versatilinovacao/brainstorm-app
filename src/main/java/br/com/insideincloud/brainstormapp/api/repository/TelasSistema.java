package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TelaSistema;

@Repository
public interface TelasSistema extends JpaRepository<TelaSistema,Long> {

}
