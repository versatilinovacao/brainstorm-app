package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cardapios_ativos_view",schema="escola")
public class CardapioAtivoView implements Serializable {
	private static final long serialVersionUID = -3590430436254952601L;
	
	@Id
	private Long cardapio_id;
	private String descricao;
	private Integer mes;
	private Integer ano;
	private Boolean status;
	
	public Long getCardapio_id() {
		return cardapio_id;
	}
	public void setCardapio_id(Long cardapio_id) {
		this.cardapio_id = cardapio_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getMes() {
		return mes;
	}
	public void setMes(Integer mes) {
		this.mes = mes;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardapio_id == null) ? 0 : cardapio_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardapioAtivoView other = (CardapioAtivoView) obj;
		if (cardapio_id == null) {
			if (other.cardapio_id != null)
				return false;
		} else if (!cardapio_id.equals(other.cardapio_id))
			return false;
		return true;
	}
		
}
