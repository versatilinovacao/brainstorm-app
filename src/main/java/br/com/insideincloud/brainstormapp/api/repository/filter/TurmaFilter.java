package br.com.insideincloud.brainstormapp.api.repository.filter;

public class TurmaFilter {
	private String turma_id;
	private String descricao;
	private String inicio;
	private String fim;
	private String turno_id;
	
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public String getFim() {
		return fim;
	}
	public void setFim(String fim) {
		this.fim = fim;
	}
	public String getTurno_id() {
		return turno_id;
	}
	public void setTurno_id(String turno_id) {
		this.turno_id = turno_id;
	}
	
}
