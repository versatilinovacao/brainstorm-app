package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ordemservicotipo",schema="servico")
public class OrdemServicoTipo implements Serializable {
	private static final long serialVersionUID = -6826336459482921484L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long ordemservicotipo_id;
	private String nome;
	
	public Long getOrdemservicotipo_id() {
		return ordemservicotipo_id;
	}
	public void setOrdemservicotipo_id(Long ordemservicotipo_id) {
		this.ordemservicotipo_id = ordemservicotipo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ordemservicotipo_id == null) ? 0 : ordemservicotipo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdemServicoTipo other = (OrdemServicoTipo) obj;
		if (ordemservicotipo_id == null) {
			if (other.ordemservicotipo_id != null)
				return false;
		} else if (!ordemservicotipo_id.equals(other.ordemservicotipo_id))
			return false;
		return true;
	}
	
}
