package br.com.insideincloud.brainstormapp.api.repository.caderno;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Caderno;
import br.com.insideincloud.brainstormapp.api.model.Matricula;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaFilter;

public class CadernosImpl implements CadernosQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Caderno> filtrar(CadernoFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Caderno> criteria = builder.createQuery(Caderno.class);
		Root<Caderno> root = criteria.from(Caderno.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		Sort sort = page.getSort();
		System.out.println(">>>>>>>>>>>>>>>>..."+page.getSort());
		
		List<Order> orders = new ArrayList<Order>();
		
		if (sort != null) {
			Sort.Order order = sort.iterator().next();
			String property = order.getProperty();
			orders.add(builder.asc(root.get(property)));
		}

		criteria.orderBy(orders);
		
		TypedQuery<Caderno> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(CadernoFilter filtro, CriteriaBuilder builder, Root<Caderno> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}

			if (!StringUtils.isEmpty(filtro.getProfessor_id())) {
				predicates.add(builder.equal(root.get("grademateria").get("grademateria_id"), Long.parseLong(filtro.getProfessor_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getTurma_id())) {
				predicates.add(builder.equal(root.get("grademateria").get("turma_id"), Long.parseLong(filtro.getTurma_id())));
			}		
			
//			if (!StringUtils.isEmpty(filtro.getCodigo())) {
//				predicates.add(builder.like(builder.lower(root.get("matricula")), "%" + filtro.getCodigo().toLowerCase() + "%"));
//			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(CadernoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Caderno> root = criteria.from(Caderno.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
