package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="atividades_inativas_view",schema="escola")
public class AtividadeInativaView implements Serializable           {
	private static final long serialVersionUID = -2799509177975300342L;
	
	@Id 
	private Long atividade_id;
	private Long gradecurricular_id;  
	@Column(name="data_atividade")
	private LocalDate data;
	private String titulo;
	private String descricao;
	private String objetivo;
	private String orientacoes;
	private Boolean avaliacao;
	@Column(name="tipoatividade_id")
	private Long tipo;
	@Column(name="descricao_atividade")
	private String descricaoatividade;
	private Integer nota;
	private String conceito;
	private LocalDateTime entrega;
	private Long turma_id;
	private Long materia_id;
	private String descricaomateria;
	private String descricaoturma;
	private Long professor_id;
	private Long composicao_professor_id;
	private String nomeprofessor;
	private Boolean status;
	
	public String getNomeprofessor() {
		return nomeprofessor;
	}
	public void setNomeprofessor(String nomeprofessor) {
		this.nomeprofessor = nomeprofessor;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public String getOrientacoes() {
		return orientacoes;
	}
	public void setOrientacoes(String orientacoes) {
		this.orientacoes = orientacoes;
	}
	public Boolean getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Boolean avaliacao) {
		this.avaliacao = avaliacao;
	}
	public Long getTipo() {
		return tipo;
	}
	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}
	public String getDescricaoatividade() {
		return descricaoatividade;
	}
	public void setDescricaoatividade(String descricaoatividade) {
		this.descricaoatividade = descricaoatividade;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public LocalDateTime getEntrega() {
		return entrega;
	}
	public void setEntrega(LocalDateTime entrega) {
		this.entrega = entrega;
	}
	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricaomateria() {
		return descricaomateria;
	}
	public void setDescricaomateria(String descricaomateria) {
		this.descricaomateria = descricaomateria;
	}
	public String getDescricaoturma() {
		return descricaoturma;
	}
	public void setDescricaoturma(String descricaoturma) {
		this.descricaoturma = descricaoturma;
	}
	public Long getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(Long professor_id) {
		this.professor_id = professor_id;
	}
	public Long getComposicao_professor_id() {
		return composicao_professor_id;
	}
	public void setComposicao_professor_id(Long composicao_professor_id) {
		this.composicao_professor_id = composicao_professor_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade_id == null) ? 0 : atividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtividadeInativaView other = (AtividadeInativaView) obj;
		if (atividade_id == null) {
			if (other.atividade_id != null)
				return false;
		} else if (!atividade_id.equals(other.atividade_id))
			return false;
		return true;
	}

}
