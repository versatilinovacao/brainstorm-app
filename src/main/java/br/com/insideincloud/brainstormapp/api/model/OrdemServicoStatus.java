package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class OrdemServicoStatus implements Serializable {
	
	private Long ordemservicostatus_id;
	
	private LocalDateTime abertura;
	private LocalDateTime fechamento;
	private Long servico; //Vincula um serviço
	private String motivo;
	
	//  - Resolvida (Pode estar resolvida, mas não concluida.) 
	//	- Pendente (Aguardando o inicio do atendimento.)
	//  - Pausa (Quando o chamado precisou ser interrompido e )
	//  - Aguardando (Quando o serviço depende de resposta do cliente)
	//  - Analise (Quando precisa analisar como proceder para o atendimento em questão)
	//  - Antendimento ()
	//  - Concluida (Quando o cliente já deu o serviço como entregue)
	private Status status;
}

enum Status {
	CONCLUIDA,
	RESOLVIDA,
	PENDENTE,
	AGUARDANDO,
	ANALISE,
	PAUSA
	
}
