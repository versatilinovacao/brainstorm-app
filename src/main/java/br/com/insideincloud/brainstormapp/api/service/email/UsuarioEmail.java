package br.com.insideincloud.brainstormapp.api.service.email;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.ConfirmarCadastro;
import br.com.insideincloud.brainstormapp.api.model.Usuario;
import br.com.insideincloud.brainstormapp.api.repository.ConfirmarCadastros;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;
import br.com.insideincloud.brainstormapp.api.service.lib.EmailService;

@Component
public class UsuarioEmail extends EmailService {
	private String email;
	
	public void salvar(ConfirmarCadastros confirmar,Usuario usuario) {	
		ConfirmarCadastro cadastro = new ConfirmarCadastro();
		Random rdn = new Random();
		cadastro.setRegistro(LocalDate.now());
		cadastro.setAtivo(LocalTime.now());
		cadastro.setValidade(30);
		cadastro.setConfirmacadastro_id(rdn.nextLong()+LocalTime.now().getNano());
		cadastro.setCodigo("x"+LocalTime.now().getNano());
		cadastro.setUsuario(usuario);
		cadastro = confirmar.saveAndFlush(cadastro);
		
		email = "<html>";
		email = email + "<body>";	
		
		email = email + "Favor clicar no link para validar seu cadastro <br/>";
		email = email + "<a href=\"";
		email = email + "localhost:8080/security/confirmarcadastro/"+cadastro.getConfirmacadastro_id();
		email = email + "\">";
		email = email + "Confirmar</a>";
		email = email + "<br/>"; 
		email = email + "localhost:8080/security/confirmarcadastro/"+cadastro.getConfirmacadastro_id();

		email = email + "</body>";
		email = email + "</html>";
		
		setConteudo(email);

	}
	
	public void confirmar(Long codigo,ConfirmarCadastros confirmar, Usuarios usuarios) {
		ConfirmarCadastro cadastro = confirmar.findOne(codigo);
		Usuario usuario = usuarios.findOne(cadastro.getUsuario().getUsuario_id());
		usuario.setStatus(true);
		usuarios.save(usuario);
	}
		
}
