package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="telefones_colaboradores_view")
public class TelefoneColaboradorView implements Serializable {
	private static final long serialVersionUID = 3520914771441508049L;

	@Id
	private Long telefone_id;
	@Column(name="numero")
	private String numero;
	@Column(name="ramal")
	private String ramal;
	@Column(name="tipo")
	private String tipo;
	@Column(name="colaborador_id")
	private Long colaborador_id;
	@Column(name="composicao_colaborador_id")
	private Long composicao_colaborador_id;
	
	public Long getTelefone_id() {
		return telefone_id;
	}
	public void setTelefone_id(Long telefone_id) {
		this.telefone_id = telefone_id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		result = prime * result + ((composicao_colaborador_id == null) ? 0 : composicao_colaborador_id.hashCode());
		result = prime * result + ((telefone_id == null) ? 0 : telefone_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelefoneColaboradorView other = (TelefoneColaboradorView) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		if (composicao_colaborador_id == null) {
			if (other.composicao_colaborador_id != null)
				return false;
		} else if (!composicao_colaborador_id.equals(other.composicao_colaborador_id))
			return false;
		if (telefone_id == null) {
			if (other.telefone_id != null)
				return false;
		} else if (!telefone_id.equals(other.telefone_id))
			return false;
		return true;
	}
	

}
