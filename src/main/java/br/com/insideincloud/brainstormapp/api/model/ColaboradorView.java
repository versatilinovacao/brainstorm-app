package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="colaboradores_view")
public class ColaboradorView implements Serializable {
	private static final long serialVersionUID = 8881640259796656116L;

	@Id
	private Long colaborador_id;
	private Long composicao_id;
	private String nome;
	private String referencia;
	private String telefone;
	private String classificacao;

	
}
