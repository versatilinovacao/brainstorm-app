package br.com.insideincloud.brainstormapp.api.repository.filter;

public class IsCadernoInconsistenciaViewFilter {
	private String caderno_id;

	public String getCaderno_id() {
		return caderno_id;
	}

	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	
	
}
