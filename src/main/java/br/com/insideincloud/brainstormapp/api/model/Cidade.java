package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="cidades_view",schema="remoto")
public class Cidade implements Serializable {
	private static final long serialVersionUID = -1295677134220064470L;

	@Id
	private Long cidade_id;
	@Column(name="descricao",length=150)
	private String descricao;
	private String uf;
	private String cep;
	private String codigo_ibge;
	private Double area;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="estado_id")
	private Estado estado;
	
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getCodigo_ibge() {
		return codigo_ibge;
	}
	public void setCodigo_ibge(String codigo_ibge) {
		this.codigo_ibge = codigo_ibge;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
	public Long getCidade_id() {
		return cidade_id;
	}
	public void setCidade_id(Long cidade_id) {
		this.cidade_id = cidade_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
}
