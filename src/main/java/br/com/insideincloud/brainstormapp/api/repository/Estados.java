package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Estado;
import br.com.insideincloud.brainstormapp.api.repository.estado.EstadosQuery;

@Repository
public interface Estados extends JpaRepository<Estado,Long>,EstadosQuery {

	@Query("select e from Estado e where e.pais.pais_id = ?1 order by e.descricao")
	public List<Estado> findByPais(Long pais_id);
	
}

//0800 772 2017
