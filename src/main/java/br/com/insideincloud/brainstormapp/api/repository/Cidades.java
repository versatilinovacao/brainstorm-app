package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Cidade;
import br.com.insideincloud.brainstormapp.api.repository.cidade.CidadesQuery;

@Repository
public interface Cidades extends JpaRepository<Cidade,Long>, CidadesQuery {
	
	@Query("select c from Cidade c where c.estado.estado_id = ?1 order by c.descricao")
	public List<Cidade> findByEstado(Long estado_id);

}
