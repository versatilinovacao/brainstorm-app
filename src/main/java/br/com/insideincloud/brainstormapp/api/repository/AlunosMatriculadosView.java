package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MatriculaAlunoViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.matriculaalunoview.AlunosMatriculadosViewQuery;

@Repository
public interface AlunosMatriculadosView extends JpaRepository<MatriculaAlunoView,Long>, AlunosMatriculadosViewQuery {
	public Page<MatriculaAlunoView> filtrar(MatriculaAlunoViewFilter filtro, Pageable page);
	
	@Query("select m from MatriculaAlunoView m where m.curso_id = ?1")
	public List<MatriculaAlunoView> findByAlunosMatriculadosPorCurso(Long curso_id);
}
