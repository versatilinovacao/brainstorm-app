package br.com.insideincloud.brainstormapp.api.resource.engine;

import br.com.insideincloud.brainstormapp.api.model.Mensagem;

public class MensagemEvent {

	private Mensagem mensagem;
	
	public MensagemEvent(Mensagem mensagem) {
		super();
		this.mensagem = mensagem;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}
	
}
