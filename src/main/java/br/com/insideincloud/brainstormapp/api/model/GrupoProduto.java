package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grupos",schema="recurso")
public class GrupoProduto implements Serializable {
	private static final long serialVersionUID = 1535017036607675925L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long grupo_id;
	@Column(name="nome",length=100)
	private String nome;
	
	public Long getGrupo_id() {
		return grupo_id;
	}
	public void setGrupo_id(Long grupo_id) {
		this.grupo_id = grupo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo_id == null) ? 0 : grupo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoProduto other = (GrupoProduto) obj;
		if (grupo_id == null) {
			if (other.grupo_id != null)
				return false;
		} else if (!grupo_id.equals(other.grupo_id))
			return false;
		return true;
	}
	
	
}
