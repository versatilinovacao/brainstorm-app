package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="artefatos", schema="almoxarifado")
public class ArtefatoMinor implements Serializable {
	private static final long serialVersionUID = 8061530280632764408L;
	
	@Id
	private Long artefato_id;
	
	@JsonBackReference
	@OneToOne
	@JoinColumn(name="composicao_id")
	private ArtefatoMinor composicao;
	
	@OneToMany(mappedBy="composicao")
	@JsonManagedReference
	private List<ArtefatoMinor> artefatos;
	
	private String nome;
	private String descricao;
	
	@Column(name="preco")
	private Double valor;
	
	private Boolean status;
	
	public List<ArtefatoMinor> getArtefatos() {
		return artefatos;
	}
	public void setArtefatos(List<ArtefatoMinor> artefatos) {
		this.artefatos = artefatos;
	}
	public Long getArtefato_id() {
		return artefato_id;
	}
	public void setArtefato_id(Long artefato_id) {
		this.artefato_id = artefato_id;
	}
	public ArtefatoMinor getComposicao() {
		return composicao;
	}
	public void setComposicao(ArtefatoMinor composicao) {
		this.composicao = composicao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(artefato_id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ArtefatoMinor))
			return false;
		ArtefatoMinor other = (ArtefatoMinor) obj;
		return Objects.equals(artefato_id, other.artefato_id);
	}
}
