package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ArquivoAtividadeView;

@Repository
public interface ArquivosAtividadesView extends JpaRepository<ArquivoAtividadeView, Long> {
	
	@Query("select a from ArquivoAtividadeView a where a.atividade_id = ?1")
	public List<ArquivoAtividadeView> findByAtividade(Long atividade_id);

}
