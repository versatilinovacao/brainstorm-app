package br.com.insideincloud.brainstormapp.api.repository.filter;

public class MateriaCompostaViewFilter {
	private String materia_id;
	private String descricao;
	private String aowner;
	
	public String getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(String materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAowner() {
		return aowner;
	}
	public void setAowner(String aowner) {
		this.aowner = aowner;
	}
	
	
}
