package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unidades",schema="empresa")
public class UnidadeEmpresa implements Serializable {
	private static final long serialVersionUID = 7095466938495131837L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long unidade_id;
	private String descricao;
	
	public Long getUnidade_id() {
		return unidade_id;
	}
	public void setUnidade_id(Long unidade_id) {
		this.unidade_id = unidade_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((unidade_id == null) ? 0 : unidade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeEmpresa other = (UnidadeEmpresa) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (unidade_id == null) {
			if (other.unidade_id != null)
				return false;
		} else if (!unidade_id.equals(other.unidade_id))
			return false;
		return true;
	}
		
}
