package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoServico;
import br.com.insideincloud.brainstormapp.api.model.view.TipoServicoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.TipoServicoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.TiposServicos;
import br.com.insideincloud.brainstormapp.api.repository.TiposServicosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.TiposServicosInativosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposservicos")
public class TipoServicoResource {
	
	@Autowired
	private TiposServicosAtivosView tiposServicosAtivos;
	
	@Autowired
	private TiposServicosInativosView tiposServicosInativos;
	
	@Autowired
	private TiposServicos tiposServicos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIPOSERVICO_CONTEUDO')")
	public RetornoWrapper<TipoServicoAtivoView> conteudoAtivo() {
		RetornoWrapper<TipoServicoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposServicosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizado informações de Tipos de Serviços Ativos, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_TIPOSERVICO_CONTEUDO')")
	public RetornoWrapper<TipoServicoInativoView> conteudoInativo() {
		RetornoWrapper<TipoServicoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tiposServicosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizado informações de Tipos de Serviços Inativos, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_TIPOSERVICO_CONTEUDO')")
	public RetornoWrapper<TipoServico> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<TipoServico> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( tiposServicos.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o tipo de servico, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
