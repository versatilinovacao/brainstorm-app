package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="cursosperiodos",schema="escola")
public class CursoPeriodo implements Serializable {
	private static final long serialVersionUID = 2115198972700337373L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cursoperiodo_id;
	@ManyToOne
	@JoinColumn(name="curso_id")
	private Curso curso;
	@Fetch(FetchMode.SELECT)
	@OneToOne
	@JoinColumn(name="periodoletivo_id")
	private PeriodoLetivo periodo;
	private LocalDate inicio_curso;
	private LocalDate fim_curso;
	@OneToMany(mappedBy = "cursoperiodo", cascade = {CascadeType.PERSIST})
	private List<GradeCurricular> curriculo;
	private boolean status;
	
	
	public PeriodoLetivo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(PeriodoLetivo periodo) {
		this.periodo = periodo;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public LocalDate getInicio_curso() {
		return inicio_curso;
	}

	public void setInicio_curso(LocalDate inicio_curso) {
		this.inicio_curso = inicio_curso;
	}

	public LocalDate getFim_curso() {
		return fim_curso;
	}

	public void setFim_curso(LocalDate fim_curso) {
		this.fim_curso = fim_curso;
	}

	public List<GradeCurricular> getCurriculo() {
		return curriculo;
	}

	public void setCurriculo(List<GradeCurricular> curriculo) {
		this.curriculo = curriculo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setCursoperiodo_id(Long cursoperiodo_id) {
		this.cursoperiodo_id = cursoperiodo_id;
	}
	
	public Long getCursoperiodo_id() {
		return this.cursoperiodo_id;
	}
	
	public void setPeriodoletivo(PeriodoLetivo periodo) {
		this.periodo = periodo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cursoperiodo_id == null) ? 0 : cursoperiodo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CursoPeriodo other = (CursoPeriodo) obj;
		if (cursoperiodo_id == null) {
			if (other.cursoperiodo_id != null)
				return false;
		} else if (!cursoperiodo_id.equals(other.cursoperiodo_id))
			return false;
		return true;
	}
	
	
}