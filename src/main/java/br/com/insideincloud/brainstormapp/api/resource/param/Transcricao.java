package br.com.insideincloud.brainstormapp.api.resource.param;

import java.io.Serializable;

public class Transcricao implements Serializable {
	private static final long serialVersionUID = -1800382059266109490L;
	
	private String transcricao;

	public String getTranscricao() {
		return transcricao;
	}

	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
}
