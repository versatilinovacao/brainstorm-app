package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ContaReceberPK implements Serializable {
	private static final long serialVersionUID = 634657745021176100L;
	
	private Long contareceber_id;	
	private Long composicao_id;
	
	public Long getContareceber_id() {
		return contareceber_id;
	}
	public void setContareceber_id(Long contareceber_id) {
		this.contareceber_id = contareceber_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicao_id == null) ? 0 : composicao_id.hashCode());
		result = prime * result + ((contareceber_id == null) ? 0 : contareceber_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaReceberPK other = (ContaReceberPK) obj;
		if (composicao_id == null) {
			if (other.composicao_id != null)
				return false;
		} else if (!composicao_id.equals(other.composicao_id))
			return false;
		if (contareceber_id == null) {
			if (other.contareceber_id != null)
				return false;
		} else if (!contareceber_id.equals(other.contareceber_id))
			return false;
		return true;
	}

}
