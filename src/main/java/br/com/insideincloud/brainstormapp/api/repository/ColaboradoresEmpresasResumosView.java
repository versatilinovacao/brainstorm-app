package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorEmpresaResumoView;

@Repository
public interface ColaboradoresEmpresasResumosView extends JpaRepository<ColaboradorEmpresaResumoView,Long> {

}
