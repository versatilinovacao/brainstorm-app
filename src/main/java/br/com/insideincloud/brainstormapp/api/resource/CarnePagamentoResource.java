package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.CarnePagamento;
import br.com.insideincloud.brainstormapp.api.model.CarnePagamentoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CarnePagamentoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.CarnesPagamentos;
import br.com.insideincloud.brainstormapp.api.repository.CarnesPagamentosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.CarnesPagamentosInativosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/carnespagamentos")
public class CarnePagamentoResource {

	@Autowired
	private CarnesPagamentos carnes;
	
	@Autowired
	private CarnesPagamentosAtivosView carnesAtivos;
	
	@Autowired
	private CarnesPagamentosInativosView carnesInativos;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CARNEPAGAMENTO_SALVAR')")
	public RetornoWrapper<CarnePagamento> salvar(@RequestBody CarnePagamento carne) {
		RetornoWrapper<CarnePagamento> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( carnes.saveAndFlush(carne) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o Carne de Pagamento");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CARNEPAGAMENTO_CONTEUDO')")
	public RetornoWrapper<CarnePagamentoAtivoView> conteudoAtivo() {
		RetornoWrapper<CarnePagamentoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( carnesAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações dos carnes de pagamentos ativos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CARNEPAGAMENTO_CONTEUDO')")
	public RetornoWrapper<CarnePagamentoInativoView> conteudoInativo() {
		RetornoWrapper<CarnePagamentoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( carnesInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações dos carnes de pagamentos inativos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CARNEPAGAMENTO_CONTEUDO')")
	public RetornoWrapper<CarnePagamento> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<CarnePagamento> retorno = new RetornoWrapper<CarnePagamento>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( carnes.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizado um carne com o código: "+codigo);
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
