package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="deletar_colaborador")
public class DeletarColaboradorAction implements Serializable {
	private static final long serialVersionUID = -8621663324467661330L;

	@Id
	private Long colaborador_id;

	public Long getColaborador_id() {
		return colaborador_id;
	}

	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	
}
