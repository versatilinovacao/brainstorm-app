package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Pais;
import br.com.insideincloud.brainstormapp.api.repository.pais.PaisesQuery;

@Repository
public interface Paises extends JpaRepository<Pais,Long>, PaisesQuery {

}
