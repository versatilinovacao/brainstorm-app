package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="formas_pagamentos",schema="financeiro")
public class FormaPagamento implements Serializable {
	private static final long serialVersionUID = 8769177580031905403L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long formapagamento_id;
	private String descricao;
	
	public Long getFormapagamento_id() {
		return formapagamento_id;
	}
	public void setFormapagamento_id(Long formapagamento_id) {
		this.formapagamento_id = formapagamento_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((formapagamento_id == null) ? 0 : formapagamento_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormaPagamento other = (FormaPagamento) obj;
		if (formapagamento_id == null) {
			if (other.formapagamento_id != null)
				return false;
		} else if (!formapagamento_id.equals(other.formapagamento_id))
			return false;
		return true;
	}
	
}
