package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.servicos.ServicoPortoAlegre;
import br.com.insideincloud.brainstormapp.api.model.view.ServicoPortoAlegreAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.ServicoPortoAlegreInativoView;
import br.com.insideincloud.brainstormapp.api.repository.ServicosPortoAlegre;
import br.com.insideincloud.brainstormapp.api.repository.ServicosPortoAlegreAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.ServicosPortoAlegreInativosView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/servicosportoalegre")
public class ServicoPortoAlegreResource {
	
	@Autowired
	private ServicosPortoAlegre servicos;
	
	@Autowired
	private ServicosPortoAlegreAtivosView servicosAtivos;
	
	@Autowired
	private ServicosPortoAlegreInativosView servicosInativos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoPortoAlegreAtivoView> conteudoAtivo() {
		RetornoWrapper<ServicoPortoAlegreAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( servicosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações da lista de Serviços de Porto Alegre, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoPortoAlegreInativoView> conteudoInativo() {
		RetornoWrapper<ServicoPortoAlegreInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try { 
			retorno.setConteudo( servicosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar informações da lista de Serviços inativos de Porto Alegre");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_SERVICO_CONTEUDO')")
	public RetornoWrapper<ServicoPortoAlegre> conteudoPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<ServicoPortoAlegre> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( servicos.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar a informação solicitada, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_SERVICO_SALVAR')")
	public RetornoWrapper<ServicoPortoAlegre> salvar(@RequestBody ServicoPortoAlegre servico) {
		RetornoWrapper<ServicoPortoAlegre> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( servicos.saveAndFlush(servico) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o Serviço, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
