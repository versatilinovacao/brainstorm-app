package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="subclasses")
public class SubClasse implements Serializable {
	private static final long serialVersionUID = 8936336083278753236L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long subclasse_id;
	@Column(name="codigo",length=5)
	private String codigo;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="descricao",length=500)
	private String descricao;
	
	public Long getSubclasse_id() {
		return subclasse_id;
	}
	public void setSubclasse_id(Long subclasse_id) {
		this.subclasse_id = subclasse_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((subclasse_id == null) ? 0 : subclasse_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubClasse other = (SubClasse) obj;
		if (subclasse_id == null) {
			if (other.subclasse_id != null)
				return false;
		} else if (!subclasse_id.equals(other.subclasse_id))
			return false;
		return true;
	}
	
	
}
