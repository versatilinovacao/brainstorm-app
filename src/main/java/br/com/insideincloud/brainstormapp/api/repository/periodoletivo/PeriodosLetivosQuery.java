package br.com.insideincloud.brainstormapp.api.repository.periodoletivo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivo;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoFilter;

public interface PeriodosLetivosQuery {
	public Page<PeriodoLetivo> filtrar(PeriodoLetivoFilter filtro, Pageable page);
}
