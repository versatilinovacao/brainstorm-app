package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contas_receber_ativas_view",schema="financeiro")
public class ContaReceberAtivaView implements Serializable {
	private static final long serialVersionUID = -8077255476619776016L;
	
	@Id
	private Long contareceber_id;
//	private Long contareceber_id;
	private Long composicao_id;
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao;
	private double total;
	private Long devedor_id;
	private Long composicao_devedor_id;
	private String nomedevedor;
	private Integer parcela;
	private Boolean status;
	
	public Long getContareceber_id() {
		return contareceber_id;
	}
	public void setContareceber_id(Long contareceber_id) {
		this.contareceber_id = contareceber_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public String getNomedevedor() {
		return nomedevedor;
	}
	public void setNomedevedor(String nomedevedor) {
		this.nomedevedor = nomedevedor;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Long getDevedor_id() {
		return devedor_id;
	}
	public void setDevedor_id(Long devedor_id) {
		this.devedor_id = devedor_id;
	}
	public Long getComposicao_devedor_id() {
		return composicao_devedor_id;
	}
	public void setComposicao_devedor_id(Long composicao_devedor_id) {
		this.composicao_devedor_id = composicao_devedor_id;
	}
	public Integer getParcela() {
		return parcela;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contareceber_id == null) ? 0 : contareceber_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaReceberAtivaView other = (ContaReceberAtivaView) obj;
		if (contareceber_id == null) {
			if (other.contareceber_id != null)
				return false;
		} else if (!contareceber_id.equals(other.contareceber_id))
			return false;
		return true;
	}
	
}
