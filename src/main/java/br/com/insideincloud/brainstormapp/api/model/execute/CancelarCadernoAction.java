package br.com.insideincloud.brainstormapp.api.model.execute;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cancelar_caderno",schema="action")
public class CancelarCadernoAction implements Serializable {
	private static final long serialVersionUID = 1609184694759767827L;

	@Id
	private Long caderno_id;

	public Long getCaderno_id() {
		return caderno_id;
	}

	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	
	
}
