package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="chamadas",schema="escola")
public class Chamada implements Serializable {
	private static final long serialVersionUID = -5242631341699444181L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long chamada_id; 
	@OneToOne
	@JoinColumn(name="caderno_id")
	private Caderno caderno;
	private LocalDate evento;
	@OneToOne
	@JoinColumns({@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador aluno;
	@OneToOne
	@JoinColumn(name="periodo_letivo_item_id")
	private PeriodoLetivoItem periodoletivoitem;
	@Column(name="conceito",length=1)
	private String conceito;
	private BigDecimal nota;
	private String avaliacao;
	private boolean status;
	private String acompanhamento;
	
	public String getAcompanhamento() {
		return acompanhamento;
	}
	public void setAcompanhamento(String acompanhamento) {
		this.acompanhamento = acompanhamento;
	}
	public PeriodoLetivoItem getPeriodoletivoitem() {
		return periodoletivoitem;
	}
	public void setPeriodoletivoitem(PeriodoLetivoItem periodoletivoitem) {
		this.periodoletivoitem = periodoletivoitem;
	}
	public Long getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(Long chamada_id) {
		this.chamada_id = chamada_id;
	}
	public Caderno getCaderno() {
		return caderno;
	}
	public void setCaderno(Caderno caderno) {
		this.caderno = caderno;
	}
	public LocalDate getEvento() {
		return evento;
	}
	public void setEvento(LocalDate evento) {
		this.evento = evento;
	}
	public Colaborador getAluno() {
		return aluno;
	}
	public void setAluno(Colaborador aluno) {
		this.aluno = aluno;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chamada_id == null) ? 0 : chamada_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chamada other = (Chamada) obj;
		if (chamada_id == null) {
			if (other.chamada_id != null)
				return false;
		} else if (!chamada_id.equals(other.chamada_id))
			return false;
		return true;
	}
	
	
	
}
