package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;

@Entity
@Table(name="grupo")
public class Grupo implements Serializable {
	private static final long serialVersionUID = -4701716845876966579L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long grupo_id;
	@Column(name="nome",length=100)
	private String nome;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usuario_grupo", joinColumns = @JoinColumn(name="grupo_id"), inverseJoinColumns = @JoinColumn(name="usuario_id"))
	private List<Permissao> permissoes;
	
	public Long getGrupo_id() {
		return grupo_id;
	}
	public void setGrupo_id(Long grupo_id) {
		this.grupo_id = grupo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grupo_id == null) ? 0 : grupo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupo other = (Grupo) obj;
		if (grupo_id == null) {
			if (other.grupo_id != null)
				return false;
		} else if (!grupo_id.equals(other.grupo_id))
			return false;
		return true;
	}	
	
}
