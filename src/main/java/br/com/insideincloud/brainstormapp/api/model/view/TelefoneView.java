package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="telefones_view")
public class TelefoneView implements Serializable {
	private static final long serialVersionUID = 853510918265958928L;

	@Id
	private Long telefone_id;
	@Column(name="numero")
	private String numero;
	@Column(name="ramal")
	private String ramal;
	@Column(name="colaborador_id")
	private Long colaborador_id;
	@Column(name="composicao_colaborador_id")
	private Long composicaocolaborador_id;
	
	public Long getComposicaocolaborador_id() {
		return composicaocolaborador_id;
	}
	public void setComposicaocolaborador_id(Long composicaocolaborador_id) {
		this.composicaocolaborador_id = composicaocolaborador_id;
	}
	public Long getTelefone_id() {
		return telefone_id;
	}
	public void setTelefone_id(Long telefone_id) {
		this.telefone_id = telefone_id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telefone_id == null) ? 0 : telefone_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelefoneView other = (TelefoneView) obj;
		if (telefone_id == null) {
			if (other.telefone_id != null)
				return false;
		} else if (!telefone_id.equals(other.telefone_id))
			return false;
		return true;
	}
	
	
}
