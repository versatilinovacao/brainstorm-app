package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaReceberPK;
import br.com.insideincloud.brainstormapp.api.model.view.ContaReceberInativaView;

@Repository
public interface ContasReceberInativasView extends JpaRepository<ContaReceberInativaView,ContaReceberPK> {

}
