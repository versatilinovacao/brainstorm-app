package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.insideincloud.brainstormapp.api.model.minor.GradeCurricularMinor;

@Entity
@Table(name="atividades",schema="escola")
public class Atividade implements Serializable {
	private static final long serialVersionUID = 4393728131443452883L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long atividade_id;
	@OneToOne
	@JoinColumn(name="gradecurricular_id")
	private GradeCurricularMinor gradecurricular;
	@Column(name="data_atividade")
	private LocalDate dataatividade;
	private String titulo;
	private String descricao;
	private String objetivo;
	private String orientacoes;
	private Boolean avaliacao;
	@OneToOne
	@JoinColumn(name="tipoatividade_id")
	private TipoAtividade tipo;
	private Integer nota;
	@Column(name="conceito",length=3)
	private String conceito;
	private LocalDateTime entrega;
	private String urlvideoatividade;
	
//	@OneToMany(fetch = FetchType.LAZY)
//	private List<OrientacaoAtividade> orientacoes;
	private Boolean status;
	
	@Fetch(FetchMode.SELECT)
	@ManyToMany
	@JoinTable(name="alunos_atividades",schema="escola",joinColumns= @JoinColumn(name="atividade_id"),
														inverseJoinColumns= {@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")})
	private List<Aluno> alunos;
	
	@JsonIgnore
	@Fetch(FetchMode.JOIN) 
	@Cascade(CascadeType.ALL)
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="arquivos_atividades",schema="escola",joinColumns=@JoinColumn(name="atividade_id"),inverseJoinColumns=@JoinColumn(name="arquivo_id"))
	private List<Arquivo> arquivos;
	
//	public List<OrientacaoAtividade> getOrientacoes() {
//		return orientacoes;
//	}
//	public void setOrientacoes(List<OrientacaoAtividade> orientacoes) {
//		this.orientacoes = orientacoes;
//	}
	public List<Arquivo> getArquivos() {
		return arquivos;
	}
	public void setArquivos(List<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}
	public String getUrlvideoatividade() {
		return urlvideoatividade;
	}
	public void setUrlvideoatividade(String urlvideoatividade) {
		this.urlvideoatividade = urlvideoatividade;
	}
	public GradeCurricularMinor getGradecurricular() {
		return gradecurricular;
	}
	public void setGradecurricular(GradeCurricularMinor gradecurricular) {
		this.gradecurricular = gradecurricular;
	}
	public List<Aluno> getAlunos() {
		return alunos;
	}
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public LocalDate getDataatividade() {
		return dataatividade;
	}
	public void setDataatividade(LocalDate dataatividade) {
		this.dataatividade = dataatividade;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public String getOrientacoes() {
		return orientacoes;
	}
	public void setOrientacoes(String orientacoes) {
		this.orientacoes = orientacoes;
	}
	public Boolean getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Boolean avaliacao) {
		this.avaliacao = avaliacao;
	}
	public TipoAtividade getTipo() {
		return tipo;
	}
	public void setTipo(TipoAtividade tipo) {
		this.tipo = tipo;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public LocalDateTime getEntrega() {
		return entrega;
	}
	public void setEntrega(LocalDateTime entrega) {
		this.entrega = entrega;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atividade_id == null) ? 0 : atividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atividade other = (Atividade) obj;
		if (atividade_id == null) {
			if (other.atividade_id != null)
				return false;
		} else if (!atividade_id.equals(other.atividade_id))
			return false;
		return true;
	}	
	
}
