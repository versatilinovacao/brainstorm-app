package br.com.insideincloud.brainstormapp.api.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.CardapioSemana;

@Repository
public interface CardapiosSemanas extends JpaRepository<CardapioSemana, Long> {
	
	@Query("select c from CardapioSemana c where c.cardapio.cardapio_id = ?1")
	public List<CardapioSemana> findBySemanaPorCardapio(Long cardapio_id);
	
	@Query("select c from CardapioSemana c where c.evento = ?1")
	public List<CardapioSemana> findByEvento(LocalDate evento);
	
	@Query("select c from CardapioSemana c where c.evento = ?1 and c.cardapio.cardapio_id = ?2")
	public List<CardapioSemana> findByCardapioPorEvento(LocalDate evento, Long cardapio_id);

	@Query("select c from CardapioSemana c where c.evento between ?1 and ?2 and c.cardapio.cardapio_id = ?3")
	public List<CardapioSemana> findByCardapioPorSemana(LocalDate evento_segunda,LocalDate evento_sexta, Long cardapio_id);
}
