package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.SituacaoAtividade;
import br.com.insideincloud.brainstormapp.api.model.TipoAtividade;

@Entity
@Table(name="atividades_minor_view",schema="escola")
public class AtividadeMinor implements Serializable {
	private static final long serialVersionUID = 7554117345202459161L;
	
	@Id
	private Long atividade_id;
	private Long aluno_id;
	private Long composicao_aluno_id;
	@OneToOne
	@JoinColumn(name="gradecurricular_id")
	private GradeCurricularMinor gradecurricular;
	@Column(name="data_atividade")
	private LocalDate dataatividade;
	private String titulo;
	private String descricao;
	private String objetivo;
	private String orientacoes;
	@OneToOne
	@JoinColumn(name="tipoatividade_id")
	private TipoAtividade tipo;
	private Integer nota;
	@Column(name="conceito",length=3)
	private String conceito;
	private LocalDateTime entrega;
	private String urlvideoatividade;
	private String transcricao;
	private Boolean avaliacao;
	@OneToOne
	@JoinColumn(name="situacaoatividade_id")
	private SituacaoAtividade situacao;
	private Boolean status;
	
	public SituacaoAtividade getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoAtividade situacao) {
		this.situacao = situacao;
	}
	public Boolean getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(Boolean avaliacao) {
		this.avaliacao = avaliacao;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public Long getComposicao_aluno_id() {
		return composicao_aluno_id;
	}
	public void setComposicao_aluno_id(Long composicao_aluno_id) {
		this.composicao_aluno_id = composicao_aluno_id;
	}
	public String getTranscricao() {
		return transcricao;
	}
	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public GradeCurricularMinor getGradecurricular() {
		return gradecurricular;
	}
	public void setGradecurricular(GradeCurricularMinor gradecurricular) {
		this.gradecurricular = gradecurricular;
	}
	public LocalDate getDataatividade() {
		return dataatividade;
	}
	public void setDataatividade(LocalDate dataatividade) {
		this.dataatividade = dataatividade;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getObjetivo() {
		return objetivo;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public String getOrientacoes() {
		return orientacoes;
	}
	public void setOrientacoes(String orientacoes) {
		this.orientacoes = orientacoes;
	}
	public TipoAtividade getTipo() {
		return tipo;
	}
	public void setTipo(TipoAtividade tipo) {
		this.tipo = tipo;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public LocalDateTime getEntrega() {
		return entrega;
	}
	public void setEntrega(LocalDateTime entrega) {
		this.entrega = entrega;
	}
	public String getUrlvideoatividade() {
		return urlvideoatividade;
	}
	public void setUrlvideoatividade(String urlvideoatividade) {
		this.urlvideoatividade = urlvideoatividade;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}

	
}
