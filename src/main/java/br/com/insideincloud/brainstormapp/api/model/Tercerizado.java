package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="tercerizados")
public class Tercerizado implements Serializable {
	private static final long serialVersionUID = 9026707533647626031L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tercerizado_id;
	@OneToOne
	@JoinColumn(name="tipotercerizado_id")
	private TipoTercerizado tipo;
	@OneToOne
	@JoinColumn(name="configuracaocontrato_id")
	private ConfiguracaoContrato configuracaocontrato;
	
	public ConfiguracaoContrato getConfiguracaocontrato() {
		return configuracaocontrato;
	}
	public void setConfiguracaocontrato(ConfiguracaoContrato configuracaocontrato) {
		this.configuracaocontrato = configuracaocontrato;
	}
	public Long getTercerizado_id() {
		return tercerizado_id;
	}
	public void setTercerizado_id(Long tercerizado_id) {
		this.tercerizado_id = tercerizado_id;
	}
	public TipoTercerizado getTipo() {
		return tipo;
	}
	public void setTipo(TipoTercerizado tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tercerizado_id == null) ? 0 : tercerizado_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tercerizado other = (Tercerizado) obj;
		if (tercerizado_id == null) {
			if (other.tercerizado_id != null)
				return false;
		} else if (!tercerizado_id.equals(other.tercerizado_id))
			return false;
		return true;
	}
	
}
