package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="taxa_selic_import",schema="mercadofinanceiro")
public class TaxaSelicImport implements Serializable {
	private static final long serialVersionUID = 1074757492233205254L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long taxaselicimport_id;
	private LocalDate evento;
	private BigDecimal taxaanual;
	private BigDecimal fatordiario;
	private BigDecimal basefinanceiro;
	private BigDecimal baseoperacoes;
	private BigDecimal media;
	private BigDecimal mediana;
	private BigDecimal moda;
	private BigDecimal desviopadrao;
	private BigDecimal indicecurtose;
	
	public Long getTaxaselicimport_id() {
		return taxaselicimport_id;
	}
	public void setTaxaselicimport_id(Long taxaselicimport_id) {
		this.taxaselicimport_id = taxaselicimport_id;
	}
	public LocalDate getEvento() {
		return evento;
	}
	public void setEvento(LocalDate evento) {
		this.evento = evento;
	}
	public BigDecimal getTaxaanual() {
		return taxaanual;
	}
	public void setTaxaanual(BigDecimal taxaanual) {
		this.taxaanual = taxaanual;
	}
	public BigDecimal getFatordiario() {
		return fatordiario;
	}
	public void setFatordiario(BigDecimal fatordiario) {
		this.fatordiario = fatordiario;
	}
	public BigDecimal getBasefinanceiro() {
		return basefinanceiro;
	}
	public void setBasefinanceiro(BigDecimal basefinanceiro) {
		this.basefinanceiro = basefinanceiro;
	}
	public BigDecimal getBaseoperacoes() {
		return baseoperacoes;
	}
	public void setBaseoperacoes(BigDecimal baseoperacoes) {
		this.baseoperacoes = baseoperacoes;
	}
	public BigDecimal getMedia() {
		return media;
	}
	public void setMedia(BigDecimal media) {
		this.media = media;
	}
	public BigDecimal getMediana() {
		return mediana;
	}
	public void setMediana(BigDecimal mediana) {
		this.mediana = mediana;
	}
	public BigDecimal getModa() {
		return moda;
	}
	public void setModa(BigDecimal moda) {
		this.moda = moda;
	}
	public BigDecimal getDesviopadrao() {
		return desviopadrao;
	}
	public void setDesviopadrao(BigDecimal desviopadrao) {
		this.desviopadrao = desviopadrao;
	}
	public BigDecimal getIndicecurtose() {
		return indicecurtose;
	}
	public void setIndicecurtose(BigDecimal indicecurtose) {
		this.indicecurtose = indicecurtose;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taxaselicimport_id == null) ? 0 : taxaselicimport_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxaSelicImport other = (TaxaSelicImport) obj;
		if (taxaselicimport_id == null) {
			if (other.taxaselicimport_id != null)
				return false;
		} else if (!taxaselicimport_id.equals(other.taxaselicimport_id))
			return false;
		return true;
	}
	
}
