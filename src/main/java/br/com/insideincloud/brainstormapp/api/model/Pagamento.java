package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pagamentos",schema="financeiro")
public class Pagamento implements Serializable {
	private static final long serialVersionUID = -3550106312261092419L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pagamento_id;
	private String nome;
	
	public Long getPagamento_id() {
		return pagamento_id;
	}
	public void setPagamento_id(Long pagamento_id) {
		this.pagamento_id = pagamento_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pagamento_id == null) ? 0 : pagamento_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagamento other = (Pagamento) obj;
		if (pagamento_id == null) {
			if (other.pagamento_id != null)
				return false;
		} else if (!pagamento_id.equals(other.pagamento_id))
			return false;
		return true;
	}

}
