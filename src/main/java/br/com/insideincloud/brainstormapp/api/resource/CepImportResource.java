package br.com.insideincloud.brainstormapp.api.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.insideincloud.brainstormapp.api.model.CepImport;
import br.com.insideincloud.brainstormapp.api.repository.CepsImports;

@RestController
@RequestMapping("/importarceps")
public class CepImportResource {
	private RestTemplate rest = new RestTemplate();
	
	@Autowired
	private CepsImports ceps;
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public List<CepImport> conteudo(@PathVariable Long codigo) {
		System.out.println(">>>>>>>>>>>>>>>> viacep.com.br/ws/"+codigo+"/json/ <<<<<<<<<<<<<<");
		CepImport cep = rest.getForObject("http://viacep.com.br/ws/"+codigo+"/json/", CepImport.class);
//		List<CepImport> cepsImport = new ArrayList<>();
		
		for (long x = 90000000 ; x < 99999999 ; x++) {
			try {
				cep = rest.getForObject("http://viacep.com.br/ws/"+codigo+"/json/", CepImport.class);
				ceps.save(cep);
			} catch(Exception e) {
				System.out.println("Não Localizei "+x);
			}
			
		}
		
		
		return ceps.findAll();
	}

}
