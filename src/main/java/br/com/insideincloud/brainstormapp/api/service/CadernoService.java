package br.com.insideincloud.brainstormapp.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.NullArgumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Caderno;
import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.Materia;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivo;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.model.view.ChamadaReportView;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.report.caderno.CadernoFrequenciasReport;
import br.com.insideincloud.brainstormapp.api.report.caderno.CapasCadernoSinteticaReport;
import br.com.insideincloud.brainstormapp.api.repository.Cadernos;
import br.com.insideincloud.brainstormapp.api.repository.CadernosMesesView;
import br.com.insideincloud.brainstormapp.api.repository.CapasCadernos;
import br.com.insideincloud.brainstormapp.api.repository.ChamadasReportView;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.Materias;
import br.com.insideincloud.brainstormapp.api.repository.MateriasCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivos;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosItens;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoMesViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaReportViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaComposicaoFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoItemFilter;
import br.com.insideincloud.brainstormapp.api.retorno.RetornoFachada;
import br.com.insideincloud.brainstormapp.api.service.caderno.CadernoFrequencias;
import br.com.insideincloud.brainstormapp.api.service.caderno.CadernoPeriodo;
import br.com.insideincloud.brainstormapp.api.service.caderno.CadernoReport;
import br.com.insideincloud.brainstormapp.api.service.caderno.MateriaComposicao;
import br.com.insideincloud.brainstormapp.api.util.MesPorExtenso;

@Service
public class CadernoService {

	@Autowired
	private MateriaComposicao composicao;
	
	@Autowired
	private MateriasCompostaView materiasCompostas;
	
	@Autowired 
	private Materias materias;
	
	@Autowired
	private Cadernos cadernos;
	
	@Autowired
	private PeriodosLetivos periodos;
	
	private CapasCadernoSinteticaReport capasReport;
	
	@Autowired
	private CapasCadernos capas;
	
	@Autowired
	private ChamadasReportView frequencias;
	
	@Autowired
	private CadernoFrequenciasReport frequenciasReport;
	
	@Autowired
	private CadernosMesesView cadernosmeses;
	
	@Autowired
	PeriodosLetivosItens periodosItens;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@Transactional
	public RetornoFachada<MateriaComposicao> salvar(MateriaComposicao conteudo) {
		
		return null;
	}
	
	private int contador;
	
	public RetornoFachada<MateriaComposicao> composicao(MateriaComposicaoFilter filtro) {
		RetornoFachada<MateriaComposicao> retorno = new RetornoFachada<MateriaComposicao>();
		
		Caderno caderno = cadernos.findOne(Long.parseLong(filtro.getCaderno_id()));		
		composicao.setPeriodo( periodo(caderno.getPeriodoletivo().getPeriodo_letivo_id()) );
		
		List<MateriaCompostaView> resultMaterias = materiasCompostas.findByAownerOrAowner(Long.parseLong(filtro.getCaderno_id()), 0L);
		
		List<Materia> composicaoMateria = new ArrayList<Materia>();
		for (MateriaCompostaView conteudo : resultMaterias) {
			Materia mat = materias.findOne(conteudo.getMateria_id());
			composicaoMateria.add(mat);
		}
		
//		composicao.setMaterias(composicaoMateria);
//		composicao.setPeriodos(null);

		retorno.setException(null);
		retorno.setPage(null);
		retorno.setSingle(composicao);
		return retorno;
	}
	
	public byte[] report() {
		byte[] report = null;
		capasReport = new CapasCadernoSinteticaReport();
		try {
			report = capasReport.Report(capas.findAll());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return report;
	}
	
//	public byte[] reportFrequencia(ChamadaReportViewFilter filtro, Pageable page) {		
//		byte[] report = null;
//		CadernoFrequenciasReport frequenciaReport = new CadernoFrequenciasReport();
//		try {
//			report = frequenciasReport.Report(frequencias.filtrar(filtro, page).getContent());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		return report;
//	}
	
	public byte[] reportFrequencia(CadernoMesViewFilter filtro, Pageable page) {
		byte[] report = null;
		try {
			report = frequenciasReport.Report( cadernoReport(filtro,page) );
		} catch (Exception e) {
			e.printStackTrace();
		}
		return report;
	}
	
	private List<CadernoReport> cadernoReport(CadernoMesViewFilter filtro,@PageableDefault(size = Integer.MAX_VALUE) Pageable page) {
		
		try {
			Caderno cadernoFind = cadernos.findOne( Long.parseLong(filtro.getCaderno_id()) );
			Long periodo_id = cadernoFind.getPeriodoletivo().getPeriodo_letivo_id();
			
			PeriodoLetivoItemFilter filtroPeriodo = new PeriodoLetivoItemFilter();
			filtroPeriodo.setPeriodoletivo_id(periodo_id.toString());
			List<PeriodoLetivoItem> periodosFind = periodosItens.filtrar(filtroPeriodo, page).getContent();

			List<CadernoReport> cadernosPeriodo = new ArrayList<>();
//			List<CadernoPeriodoReport> cadernoPeriodoReport = new ArrayList<>();
			
			periodosFind.forEach(periodo->{
				String ano = periodos.findOne(periodo_id).getAno();			
				Colaborador empresa = colaboradores.findOne(cadernoFind.getEscola().getColaborador_id());
				CadernoReport caderno = new CadernoReport();
				caderno.setCnpj(empresa.getPessoajuridica().getCnpj());
				caderno.setRua(empresa.getLogradouro().getDescricao());
				caderno.setNumero(empresa.getNumero());
				caderno.setComplemento(empresa.getComplemento());
				caderno.setBairro(empresa.getLogradouro().getBairro().getDescricao());
				caderno.setCidade(empresa.getLogradouro().getBairro().getCidade().getDescricao());
				caderno.setEstado(empresa.getLogradouro().getBairro().getCidade().getEstado().getDescricao());
				caderno.setPais(empresa.getLogradouro().getBairro().getCidade().getEstado().getPais().getDescricao());
				caderno.setAno(ano);
				caderno.setTelefone(empresa.getTelefone().getNumero());
				caderno.setEmail(empresa.getEmail());
				caderno.setDescricao("");
				caderno.setEscola("");
				
				caderno.setNome_caderno(cadernoFind.getDescricao());
				caderno.setNome_professor(cadernoFind.getGradecurricular().getProfessor().getNome());
				
				int primeiroMes = periodo.getPeriodo_inicial().getMonthValue();
				int ultimoMes   = periodo.getPeriodo_final().getMonthValue();
				List<Integer> mesPeriodo = new ArrayList<>();
				mesPeriodo.add(primeiroMes);		
				int mesCorrente = primeiroMes;
				for (int x = primeiroMes ; x < ultimoMes ; x++) {
					if (mesCorrente+1 != ultimoMes) {
						mesCorrente ++;
						mesPeriodo.add(mesCorrente);
					}
				}
				mesPeriodo.add(ultimoMes);
				
				System.out.println("PRIMEIRO MÊS "+primeiroMes+" ULTIMO MÊS "+ultimoMes);
				
				List<CadernoPeriodo> meses = new ArrayList<>();
				
				mesPeriodo.forEach(item->{
					CadernoPeriodo cadernoPeriodo = new CadernoPeriodo();
					
					cadernoPeriodo.setNomecurso(cadernoFind.getGradecurricular().getCurso().getDescricao());
					cadernoPeriodo.setNometurma(cadernoFind.getGradecurricular().getTurma().getDescricao());
					cadernoPeriodo.setMes(item.toString());
					cadernoPeriodo.setMesporextenso(new MesPorExtenso(item.intValue()).descricao());
					
					ChamadaReportViewFilter filtroMeses = new ChamadaReportViewFilter();
					filtroMeses.setAno(periodos.findOne(periodo_id).getAno());
					filtroMeses.setMes(item.toString());
					filtroMeses.setCaderno_id(cadernos.findOne( Long.parseLong(filtro.getCaderno_id()) ).getCaderno_id().toString());
//					filtroMeses.setCaderno_id(cadernoFind.getCaderno_id().toString());				
					
//					System.out.println("X1"+frequencias.filtrar(filtroMeses, page).getContent().get(0).getNome_aluno());
//					System.out.println("X2"+frequencias.filtrar(filtroMeses, page).getContent().get(19).getNome_aluno());
					List<ChamadaReportView> frequenciasRetorno = frequencias.filtrar(filtroMeses, page).getContent();
					List<CadernoFrequencias> freqs = new ArrayList<>();
					
					Contador(0);
					frequenciasRetorno.forEach(frequencia->{
						CadernoFrequencias freq = new CadernoFrequencias();
						freq.setItem(Contador(1));
						freq.setAluno_id(frequencia.getAluno_id());
						freq.setNome_aluno(frequencia.getNome_aluno());
											
						CarregarDias(frequencia,freq);
						
						System.out.println("ALUNO "+freq.getNome_aluno());
						freqs.add(freq);
					});
					System.out.println("TAMANHO OBJETOS "+frequenciasRetorno.size());
					
					for (int j = 0 ; j <= (29 - frequenciasRetorno.size())  ; j++) {
						System.out.println("OBJETOS "+freqs.size());
						CadernoFrequencias freq = new CadernoFrequencias();
						freq.setItem(Contador(1));
						freq.setAluno_id(0L);
						freq.setNome_aluno("");
						
						freqs.add(freq);
						
					}
									
					cadernoPeriodo.setFrequenciasalunos(freqs);					
					
					meses.add(cadernoPeriodo);
				});			
				
				caderno.setMeses(meses);

//				CadernoPeriodoReport periodoReport = new CadernoPeriodoReport();
//				periodoReport.getCadernos().add(caderno);
				cadernosPeriodo.add(caderno);

			});
			return cadernosPeriodo; 
		} catch (NullPointerException e) {
			System.out.print("Objeto filtro dentro do CadernoService metodo cadernoReport, pode estar null "+ e);
		} catch (NullArgumentException e) {
			System.out.println("Existe um método od objeto filtro que esta chegando NULL. (CadernoService, cadernoReport)"+ e);
		}
		
		return null;
	}

	private int Contador(int status) {
		if (status == 0) {
			contador = 0;
			return 0;
		} else {
			return contador = contador + 1;
		}
	}
	
	private void CarregarDias(ChamadaReportView pai, CadernoFrequencias filho) {
		filho.setUm( pai.getUm() );
		filho.setDois( pai.getDois() );			
		filho.setTres( pai.getTres() );
		filho.setQuatro( pai.getQuatro() );		
		filho.setCinco( pai.getCinco() );		
		filho.setSeis( pai.getSeis() );
		filho.setSete( pai.getSete() );			
		filho.setOito( pai.getOito() );			
		filho.setNove( pai.getNove() );
		filho.setDez( pai.getDez() );			
		filho.setOnze( pai.getOnze() );			
		filho.setDoze( pai.getDoze() );
		filho.setTreze( pai.getTreze() );		
		filho.setQuatorze( pai.getQuatorze() );		
		filho.setQuinze( pai.getQuinze() );
		filho.setDezesseis( pai.getDezesseis() );	
		filho.setDezessete( pai.getDezessete() );	
		filho.setDezoito( pai.getDezoito() );
		filho.setDezenove( pai.getDezenove() );		
		filho.setVinte( pai.getVinte() );		
		filho.setVinteum( pai.getVinteum() );
		filho.setVintedois( pai.getVintedois() );	
		filho.setVintetres( pai.getVintetres() );	
		filho.setVintequatro( pai.getVintequatro() );
		filho.setVintecinco( pai.getVintecinco() );	
		filho.setVinteseis( pai.getVinteseis() );	
		filho.setVintesete( pai.getVintesete() );
		filho.setVinteoito( pai.getVinteoito() );	
		filho.setVintenove( pai.getVintenove() );	
		filho.setTrinta( pai.getTrinta() );
		filho.setTrintaeum( pai.getTrintaeum() );
		
	}
	
	private String StatusMes( int status ) {
		if (status == 0) {
			return "";
		} else if (status == 1) {
			return ".";
		} else {
			return "x";
		}
		
	}
	
	private PeriodoLetivo periodo(Long periodo_id) {
		return periodos.findOne(periodo_id);
	}
}
