package br.com.insideincloud.brainstormapp.api.repository.logradouro;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Logradouro;
import br.com.insideincloud.brainstormapp.api.repository.filter.LogradouroFilter;

public interface LogradourosQuery {
	public Page<Logradouro> filtrar(LogradouroFilter filtro, Pageable page);
	
}

