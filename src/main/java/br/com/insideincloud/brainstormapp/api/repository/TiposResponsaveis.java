package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.TipoResponsavel;

@Repository
public interface TiposResponsaveis extends JpaRepository<TipoResponsavel, Long> {

}
