package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="comunicacao_aluno_professor",schema="comunicacao")
public class ComunicacaoAlunoProfessor implements Serializable {
	private static final long serialVersionUID = 5988241502445767909L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long comunicacao_id;
	private String descricao;
	
	public Long getComunicacao_id() {
		return comunicacao_id;
	}
	public void setComunicacao_id(Long comunicacao_id) {
		this.comunicacao_id = comunicacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comunicacao_id == null) ? 0 : comunicacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComunicacaoAlunoProfessor other = (ComunicacaoAlunoProfessor) obj;
		if (comunicacao_id == null) {
			if (other.comunicacao_id != null)
				return false;
		} else if (!comunicacao_id.equals(other.comunicacao_id))
			return false;
		return true;
	}

}
