package br.com.insideincloud.brainstormapp.api.repository.filter;

public class CargoFilter {
	private String cargo_id;
	private String descricao;
	
	public String getCargo_id() {
		return cargo_id;
	}
	public void setCargo_id(String cargo_id) {
		this.cargo_id = cargo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	

}
