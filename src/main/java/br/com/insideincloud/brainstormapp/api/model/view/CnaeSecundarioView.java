package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cnaes_secundarios_view",schema="contabil")
public class CnaeSecundarioView implements Serializable {
	private static final long serialVersionUID = -2729920174824997423L;

	@Id
	private Long cnae_id;
	private String codigo;
	private String denominacao;
	private Boolean status;
	
	public Long getCnae_id() {
		return cnae_id;
	}
	public void setCnae_id(Long cnae_id) {
		this.cnae_id = cnae_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDenominacao() {
		return denominacao;
	}
	public void setDenominacao(String denominacao) {
		this.denominacao = denominacao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cnae_id == null) ? 0 : cnae_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CnaeSecundarioView other = (CnaeSecundarioView) obj;
		if (cnae_id == null) {
			if (other.cnae_id != null)
				return false;
		} else if (!cnae_id.equals(other.cnae_id))
			return false;
		return true;
	}

}
