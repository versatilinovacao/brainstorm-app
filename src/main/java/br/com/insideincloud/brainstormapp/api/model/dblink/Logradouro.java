package br.com.insideincloud.brainstormapp.api.model.dblink;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="logradouros_view",schema="remoto")
public class Logradouro implements Serializable {
	private static final long serialVersionUID = -5267756099391943305L;
	
	@Id
	private String cep;
	@Column(name="logradouro")
	private String nome;
	private String complemento;
	private String local;
	private Long bairro_id;
	private Long cidade_id;
	private Long estado_id;
	private Long pais_id;
	private String descricaobairro;
	private String descricaoestado;
	private String descricaopais;
	private String descricaocidade;
	private String siglauf;
	private String siglapais;
	
	public String getDescricaocidade() {
		return descricaocidade;
	}
	public void setDescricaocidade(String descricaocidade) {
		this.descricaocidade = descricaocidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public Long getBairro_id() {
		return bairro_id;
	}
	public void setBairro_id(Long bairro_id) {
		this.bairro_id = bairro_id;
	}
	public Long getCidade_id() {
		return cidade_id;
	}
	public void setCidade_id(Long cidade_id) {
		this.cidade_id = cidade_id;
	}
	public Long getEstado_id() {
		return estado_id;
	}
	public void setEstado_id(Long estado_id) {
		this.estado_id = estado_id;
	}
	public Long getPais_id() {
		return pais_id;
	}
	public void setPais_id(Long pais_id) {
		this.pais_id = pais_id;
	}
	public String getDescricaobairro() {
		return descricaobairro;
	}
	public void setDescricaobairro(String descricaobairro) {
		this.descricaobairro = descricaobairro;
	}
	public String getDescricaoestado() {
		return descricaoestado;
	}
	public void setDescricaoestado(String descricaoestado) {
		this.descricaoestado = descricaoestado;
	}
	public String getDescricaopais() {
		return descricaopais;
	}
	public void setDescricaopais(String descricaopais) {
		this.descricaopais = descricaopais;
	}
	public String getSiglauf() {
		return siglauf;
	}
	public void setSiglauf(String siglauf) {
		this.siglauf = siglauf;
	}
	public String getSiglapais() {
		return siglapais;
	}
	public void setSiglapais(String siglapais) {
		this.siglapais = siglapais;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Logradouro other = (Logradouro) obj;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		return true;
	}
	
}
