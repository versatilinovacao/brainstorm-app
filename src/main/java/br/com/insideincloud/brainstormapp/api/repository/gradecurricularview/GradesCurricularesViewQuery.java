package br.com.insideincloud.brainstormapp.api.repository.gradecurricularview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularView;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularViewFilter;

public interface GradesCurricularesViewQuery {
	public Page<GradeCurricularView> filtrar(GradeCurricularViewFilter filtro, Pageable page);
}
