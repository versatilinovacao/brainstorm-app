package br.com.insideincloud.brainstormapp.api.repository.taxaselic;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.TaxaSelic;
import br.com.insideincloud.brainstormapp.api.repository.filter.TaxaSelicFilter;

public class TaxasSelicsImpl implements TaxasSelicsQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<TaxaSelic> filtrar(TaxaSelicFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<TaxaSelic> criteria = builder.createQuery(TaxaSelic.class);
		Root<TaxaSelic> root = criteria.from(TaxaSelic.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		criteria.orderBy(predicateOrder(page.getSort(),builder,root));

		//criteria.orderBy(orders);

		TypedQuery<TaxaSelic> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}

	private List<Order> predicateOrder(Sort sort, CriteriaBuilder builder, Root<TaxaSelic> root) {
		List<Order> orders = new ArrayList<Order>();
		
		if (sort != null) {
			Sort.Order order = sort.iterator().next();
			String property = order.getProperty();
			orders.add(builder.asc(root.get(property)));
		}
		
		return orders;
	
	}

	private Predicate[] criarRestricoes(TaxaSelicFilter filtro, CriteriaBuilder builder, Root<TaxaSelic> root) {
		List<Predicate> predicates = new ArrayList<>();
				
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getTaxaselic_id())) {
				predicates.add(builder.equal(root.get("chamado_id"), Long.parseLong(filtro.getTaxaselic_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getMovimento_inical()) && !StringUtils.isEmpty(filtro.getMovimento_final())) {
				predicates.add(
					builder.between(root.get("movimento"), filtro.getMovimento_inical(), filtro.getMovimento_inical())	
		
				);
			}
					
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
		
	}
	
	private Long total(TaxaSelicFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<TaxaSelic> root = criteria.from(TaxaSelic.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
