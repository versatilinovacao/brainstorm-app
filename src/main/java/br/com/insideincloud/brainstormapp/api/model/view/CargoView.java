package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cargos_ativos_view",schema="public")
public class CargoView implements Serializable {
	
	private static final long serialVersionUID = 8865024231470154276L;

	@Id
	private Long cargo_id;
	private String descricao;
	
	public Long getCargo_id() {
		return cargo_id;
	}
	public void setCargo_id(Long cargo_id) {
		this.cargo_id = cargo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargo_id == null) ? 0 : cargo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoView other = (CargoView) obj;
		if (cargo_id == null) {
			if (other.cargo_id != null)
				return false;
		} else if (!cargo_id.equals(other.cargo_id))
			return false;
		return true;
	}
}
