package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Permissao;
import br.com.insideincloud.brainstormapp.api.model.PermissaoSalvar;
import br.com.insideincloud.brainstormapp.api.repository.Permissoes;
import br.com.insideincloud.brainstormapp.api.repository.PermissoesSalvar;
import br.com.insideincloud.brainstormapp.api.resource.delete.PermissaoDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/permissoes")
public class PermissaoResource {

	@Autowired
	private Permissoes permissoes;
	
	@Autowired
	private PermissoesSalvar permissoesSalvar;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PERMISSAO_CONTEUDO')")
	public RetornoWrapper<Permissao> conteudo() {
		RetornoWrapper<Permissao> retorno = new RetornoWrapper<Permissao>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(permissoes.findAll());			
		} catch (Exception e) {
			erro.setCodigo(7);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde.");
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PERMISSAO_CONTEUDO')")
	public Permissao buscarPeloCodigo(@PathVariable Long codigo) {
						
		return permissoes.findOne(codigo);
	}

	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_PERMISSAO_SALVAR')")
	@Transactional
	public RetornoWrapper<PermissaoSalvar> salvar(@RequestBody PermissaoSalvar permissao, HttpServletResponse response) {
		RetornoWrapper<PermissaoSalvar> retorno = new RetornoWrapper<PermissaoSalvar>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			permissao = permissoesSalvar.saveAndFlush(permissao);
			retorno.setSingle(permissao);
		} catch (Exception e) {
			erro.setCodigo(11);
			erro.setMensagem("Erro ao salvar a permissão, favor verificar as inconsistências.");
			retorno.setException(erro);
			
		}

		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(permissao.getPermissao_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());			
		
		return retorno;
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_PERMISSAO_DELETAR')")
	@Transactional
	public RetornoWrapper<Permissao> deletar(@RequestBody PermissaoDelete permissao, HttpServletResponse response) {
		RetornoWrapper<Permissao> retorno = new RetornoWrapper<Permissao>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			for (Permissao conteudo: permissao.getPermissoes()) {
				permissoes.delete(conteudo);
			}
			
		} catch (Exception e) {
			erro.setCodigo(15);
			erro.setMensagem("Não foi possível deletar um ou mais de um arquivo selecionado, favor tentar novamente mais tarde.");
			retorno.setException(erro);
		}
		
		return retorno;
	}
}
