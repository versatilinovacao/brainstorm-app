package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tipos_acessos",schema="public")
public class TipoAcesso implements Serializable {
	private static final long serialVersionUID = -1649829216314737244L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipoacesso_id;
	private String nome;
	
	public Long getTipoacesso_id() {
		return tipoacesso_id;
	}
	public void setTipoacesso_id(Long tipoacesso_id) {
		this.tipoacesso_id = tipoacesso_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipoacesso_id == null) ? 0 : tipoacesso_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAcesso other = (TipoAcesso) obj;
		if (tipoacesso_id == null) {
			if (other.tipoacesso_id != null)
				return false;
		} else if (!tipoacesso_id.equals(other.tipoacesso_id))
			return false;
		return true;
	}
	
}
