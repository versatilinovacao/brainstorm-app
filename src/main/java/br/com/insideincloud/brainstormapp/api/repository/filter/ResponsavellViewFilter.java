package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ResponsavellViewFilter {
	private String responsavel_id;
	private String nome;
	private String vinculo_id;
	private String colaborador_id;
	private String composicao_colaborador_id;
	private String telefone;
	
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(String composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getResponsavel_id() {
		return responsavel_id;
	}
	public void setResponsavel_id(String responsavel_id) {
		this.responsavel_id = responsavel_id;
	}
	public String getVinculo_id() {
		return vinculo_id;
	}
	public void setVinculo_id(String vinculo_id) {
		this.vinculo_id = vinculo_id;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	

}
