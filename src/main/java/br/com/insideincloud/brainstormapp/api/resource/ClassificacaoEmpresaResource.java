package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.ClassificacaoEmpresa;
import br.com.insideincloud.brainstormapp.api.repository.ClassificacaoEmpresas;

@RestController
@RequestMapping("/classificacaoempresas")
public class ClassificacaoEmpresaResource {
	
	@Autowired
	private ClassificacaoEmpresas classificacaoempresas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CLASSIFICACAOEMPRESAS_CONTEUDO')")
	public List<ClassificacaoEmpresa> conteudo() {
		
		return classificacaoempresas.findAll();
	}

}
