package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Saldo implements Serializable {
	private static final long serialVersionUID = 3428636999175945168L;

	private Long saldo_id;
	private Acao acao;
	private Long quantidade;
	private BigDecimal preco;
	
	public Long getSaldo_id() {
		return saldo_id;
	}
	public void setSaldo_id(Long saldo_id) {
		this.saldo_id = saldo_id;
	}
	public Acao getAcao() {
		return acao;
	}
	public void setAcao(Acao acao) {
		this.acao = acao;
	}
	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getPreco() {
		return preco;
	}
	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((saldo_id == null) ? 0 : saldo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Saldo other = (Saldo) obj;
		if (saldo_id == null) {
			if (other.saldo_id != null)
				return false;
		} else if (!saldo_id.equals(other.saldo_id))
			return false;
		return true;
	}
	
	
}
