package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="avaliacoes",schema="escola")
public class Avaliacao implements Serializable {
	private static final long serialVersionUID = 2368377222637603546L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long avaliacao_id;
	@OneToOne
	@JoinColumn(name="caderno_id")
	private Caderno caderno;
	@OneToOne
	@JoinColumns({@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")})
	private Colaborador aluno;
	private BigDecimal aproveitamento;
	@Column(name="conceito",length=1)
	private String conceito;
	private BigDecimal nota;
	private String avaliacao;
	
	public Long getAvaliacao_id() {
		return avaliacao_id;
	}
	public void setAvaliacao_id(Long avaliacao_id) {
		this.avaliacao_id = avaliacao_id;
	}
	public Caderno getCaderno() {
		return caderno;
	}
	public void setCaderno(Caderno caderno) {
		this.caderno = caderno;
	}
	public Colaborador getAluno() {
		return aluno;
	}
	public void setAluno(Colaborador aluno) {
		this.aluno = aluno;
	}
	public BigDecimal getAproveitamento() {
		return aproveitamento;
	}
	public void setAproveitamento(BigDecimal aproveitamento) {
		this.aproveitamento = aproveitamento;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avaliacao_id == null) ? 0 : avaliacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Avaliacao other = (Avaliacao) obj;
		if (avaliacao_id == null) {
			if (other.avaliacao_id != null)
				return false;
		} else if (!avaliacao_id.equals(other.avaliacao_id))
			return false;
		return true;
	}
	
	
	
}
