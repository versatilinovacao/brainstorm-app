package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularInativaView;
import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularView;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaView;
import br.com.insideincloud.brainstormapp.api.model.view.ProfessorGradeView;
import br.com.insideincloud.brainstormapp.api.model.view.TurmaAtivaView;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurriculares;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurricularesAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurricularesInativasView;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurricularesView;
import br.com.insideincloud.brainstormapp.api.repository.MateriasView;
import br.com.insideincloud.brainstormapp.api.repository.ProfessoresGradesView;
import br.com.insideincloud.brainstormapp.api.repository.TurmasAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.GradeCurricularDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/gradescurricular")
public class GradeCurricularResource {

	@Autowired
	private GradesCurriculares grades;
	
	@Autowired
	private GradesCurricularesView gradesView;
	
	@Autowired
	private ProfessoresGradesView professores;
	
	@Autowired 
	private TurmasAtivasView turmas;
	
	@Autowired
	private MateriasView materiasAtivas;
	
	@Autowired
	private GradesCurriculares gradescurriculares;
	
	@Autowired
	private GradesCurricularesAtivasView gradescurricularesativas;
	
	@Autowired
	private GradesCurricularesInativasView gradescurricularesinativas;

	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<GradeCurricular> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<GradeCurricular> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(gradescurriculares.findOne(codigo));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as grades ativas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	};
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<GradeCurricularAtivaView> conteudo(@RequestBody GradeCurricularFilter filter) {
		RetornoWrapper<GradeCurricularAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(gradescurricularesativas.findByGradePorCurso( Long.parseLong(filter.getCurso_id()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as grades ativas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	};
	
	@GetMapping("/inativas/{curso_id}")
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<GradeCurricularInativaView> conteudoInativo(@PathVariable Long curso_id){
		RetornoWrapper<GradeCurricularInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( gradescurricularesinativas.findByGradePorCurso(curso_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as grades inativas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/view")
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<GradeCurricularView> conteudoView() {
		RetornoWrapper<GradeCurricularView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		gradesView.filtrar(filtro, page)
		try { 
			retorno.setConteudo( gradesView.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as grades curriculares, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_SALVAR')")
	public RetornoWrapper<GradeCurricular> salvar(@RequestBody GradeCurricular grade, HttpServletResponse response) {
		RetornoWrapper<GradeCurricular> retorno = new RetornoWrapper<>();
		
		
		grade = grades.save(grade);
		retorno.setSingle(grade);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(grade.getGradecurricular_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
//		return ResponseEntity.created(uri).body(grade);
	}; 
	
	@GetMapping("/professores")
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<ProfessorGradeView> conteudoProfessores() {
		RetornoWrapper<ProfessorGradeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( professores.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os professores, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/turmas")
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_CONTEUDO')")
	public RetornoWrapper<TurmaAtivaView> conteudoTurmas() {
		RetornoWrapper<TurmaAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(turmas.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as turmas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;		
	}
	
	@GetMapping("/materias")
	@PreAuthorize("hasAuthority('ROLE_MATERIA_CONTEUDO')")
	public RetornoWrapper<MateriaView> conteudoMaterias() {
		RetornoWrapper<MateriaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		materias.filtrar(filtro,page)
		try {
			retorno.setConteudo( materiasAtivas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as materias, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_GRADECURRICULAR_DELETAR')")
	public Page<GradeCurricular> deletar(@RequestBody GradeCurricularDelete deleta, @PageableDefault(size=5) Pageable page, HttpServletResponse response) {
		GradeCurricularFilter filtro = new GradeCurricularFilter();
		
		for (GradeCurricular grade : deleta.getGradesCurriculares()) {
			grades.delete(grade);
		};
		
		return grades.filtrar(filtro,page);
	};
	
}
