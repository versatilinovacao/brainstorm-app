package br.com.insideincloud.brainstormapp.api.resource;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ComunicacaoAlunoProfessor;
import br.com.insideincloud.brainstormapp.api.model.MensagemAlunoProfessor;
import br.com.insideincloud.brainstormapp.api.model.UsuarioLoginView;
import br.com.insideincloud.brainstormapp.api.model.view.UsuarioView;
import br.com.insideincloud.brainstormapp.api.repository.ComunicacoesAlunosProfessores;
import br.com.insideincloud.brainstormapp.api.repository.MensagensAlunosProfessores;
import br.com.insideincloud.brainstormapp.api.repository.UsuariosLoginView;
import br.com.insideincloud.brainstormapp.api.repository.UsuariosView;
import br.com.insideincloud.brainstormapp.api.resource.fachada.MensagemAlunoProfessorFachada;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/mensagensprofessoralunos")
public class MensagemAlunoProfessorResource {

	@Autowired
	private ComunicacoesAlunosProfessores comunicacoes;
	
	@Autowired
	private MensagensAlunosProfessores mensagens;
	
	@Autowired
	private UsuariosView usuarios;
	
	@Autowired
	private UsuariosLoginView usuariosLogin;
	
	@GetMapping("/comunicacoes")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTECOMUNICACAO_CONTEUDO')")
	public RetornoWrapper<ComunicacaoAlunoProfessor> conteudoComunicacao() {
		RetornoWrapper<ComunicacaoAlunoProfessor> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( comunicacoes.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum tipo de comunicação.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{professor_id}/{composicao_professor_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTECOMUNICACAO_CONTEUDO')")
	public RetornoWrapper<MensagemAlunoProfessor> conteudoMensagens(@PathVariable Long professor_id, @PathVariable Long composicao_professor_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<MensagemAlunoProfessor> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			UsuarioView professor = usuarios.findByUsuarioProfessor(professor_id, composicao_professor_id);
			UsuarioView aluno = usuarios.findByUsuarioAluno_id(aluno_id, composicao_aluno_id);
			
			retorno.setConteudo( mensagens.findByMensagens(professor.getUsuario_id(),aluno.getUsuario_id()) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar Mensagens para o aluno e o professor");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAnyAuthority('ROLE_AMBIENTECOMUNICACAO_SALVAR','ROLE_ATIVIDADE_CONTEUDO')")
	public RetornoWrapper<MensagemAlunoProfessorFachada> salvar(@RequestBody MensagemAlunoProfessorFachada mensagemFachada) {
		RetornoWrapper<MensagemAlunoProfessorFachada> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		UsuarioView aluno = usuarios.findByUsuarioAluno_id(mensagemFachada.getAluno().getColaborador_id().getColaborador_id(), mensagemFachada.getAluno().getColaborador_id().getComposicao_id());
//		UsuarioView professor = usuarios.findByUsuarioProfessor(mensagemFachada.getProfessor().getColaborador_id().getColaborador_id() , mensagemFachada.getProfessor().getColaborador_id().getColaborador_id());
		
		UsuarioLoginView aluno = usuariosLogin.findByUsuarioAluno_id(mensagemFachada.getAluno().getColaborador_id().getColaborador_id(), mensagemFachada.getAluno().getColaborador_id().getComposicao_id());
		UsuarioLoginView professor = usuariosLogin.findByUsuarioProfessor(mensagemFachada.getProfessor().getColaborador_id().getColaborador_id() , mensagemFachada.getProfessor().getColaborador_id().getColaborador_id());
		
		try {
			MensagemAlunoProfessor mensagem = new MensagemAlunoProfessor();
			mensagem.setProfessor(professor);
			mensagem.setAluno(aluno);
			mensagem.setEmissor(mensagemFachada.getEmissor());
			mensagem.setMensagem(mensagemFachada.getMensagem());
			mensagem.setRegistro(LocalDateTime.now());
			if (mensagemFachada.getMensagemaluno_id() != null) {
				mensagem.setMensagemaluno_id(mensagemFachada.getMensagemaluno_id());				
			}
			
			mensagem = mensagens.saveAndFlush(mensagem);
			mensagemFachada.setMensagemaluno_id(mensagem.getMensagemaluno_id());
			retorno.setSingle( mensagemFachada );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível processar a mensagem, favor enviar novamente!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}
