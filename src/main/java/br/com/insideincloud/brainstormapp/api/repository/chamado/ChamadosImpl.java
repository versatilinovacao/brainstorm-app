package br.com.insideincloud.brainstormapp.api.repository.chamado;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Chamado;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadoFilter;

public class ChamadosImpl implements ChamadosQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Chamado> filtrar(ChamadoFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Chamado> criteria = builder.createQuery(Chamado.class);
		Root<Chamado> root = criteria.from(Chamado.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		criteria.orderBy(predicateOrder(page.getSort(),builder,root));

		//criteria.orderBy(orders);

		TypedQuery<Chamado> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}

	private List<Order> predicateOrder(Sort sort, CriteriaBuilder builder, Root<Chamado> root) {
		List<Order> orders = new ArrayList<Order>();
		
		if (sort != null) {
			Sort.Order order = sort.iterator().next();
			String property = order.getProperty();
			orders.add(builder.asc(root.get(property)));
		}
		
		return orders;
	
	}

	private Predicate[] criarRestricoes(ChamadoFilter filtro, CriteriaBuilder builder, Root<Chamado> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getChamado_id())) {
				predicates.add(builder.equal(root.get("chamado_id"), Long.parseLong(filtro.getChamado_id())));
			}		

//			if (!StringUtils.isEmpty(filtro.get)) {
//				predicates.add(builder.like(builder.lower(root.get("matricula")), "%" + filtro.getTitulo().toLowerCase() + "%"));
//			}
			
			if (!StringUtils.isEmpty(filtro.getTimeline())) {
				predicates.add(builder.equal(root.get("timeline"),Long.parseLong(filtro.getTimeline()) ));
			}
			
			if (!StringUtils.isEmpty(filtro.getTitulo())) {
				predicates.add(builder.like(builder.lower(root.get("titulo")), "%" + filtro.getTitulo().toLowerCase() + "%"));
			}
		
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
		
	}
	
	private Long total(ChamadoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Chamado> root = criteria.from(Chamado.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
