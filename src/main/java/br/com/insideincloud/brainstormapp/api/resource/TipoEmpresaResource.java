package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TipoEmpresa;
import br.com.insideincloud.brainstormapp.api.repository.TiposEmpresas;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/tiposempresas")
public class TipoEmpresaResource {
	
	@Autowired
	private TiposEmpresas tiposempresas;
	
	@GetMapping
//	@PreAuthorize("hasAuthority('ROLE_TIPOSEMPRESAS_CONTEUDO')")
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public RetornoWrapper<TipoEmpresa> conteudo() {
		RetornoWrapper<TipoEmpresa> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(tiposempresas.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente  dentro de alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

}
