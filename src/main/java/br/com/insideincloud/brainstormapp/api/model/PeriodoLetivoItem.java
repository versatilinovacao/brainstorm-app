package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//Player
@Entity
@Table(name="periodos_letivos_itens",schema="escola")
public class PeriodoLetivoItem implements Serializable {
	private static final long serialVersionUID = -5257781180029168171L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long periodo_letivo_item_id;
	@ManyToOne
	@JoinColumn(name="periodo_letivo_id")
	private PeriodoLetivo periodoletivo;
	@Column(name="descricao")
	private String descricao;
	@Column(name="periodo_inicial")
	private LocalDate periodo_inicial;
	@Column(name="periodo_final")
	private LocalDate periodo_final;
	
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public PeriodoLetivo getPeriodoletivo() {
		return periodoletivo;
	}
	public void setPeriodoletivo(PeriodoLetivo periodoletivo) {
		this.periodoletivo = periodoletivo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getPeriodo_inicial() {
		return periodo_inicial;
	}
	public void setPeriodo_inicial(LocalDate periodo_inicial) {
		this.periodo_inicial = periodo_inicial;
	}
	public LocalDate getPeriodo_final() {
		return periodo_final;
	}
	public void setPeriodo_final(LocalDate periodo_final) {
		this.periodo_final = periodo_final;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((periodo_letivo_item_id == null) ? 0 : periodo_letivo_item_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeriodoLetivoItem other = (PeriodoLetivoItem) obj;
		if (periodo_letivo_item_id == null) {
			if (other.periodo_letivo_item_id != null)
				return false;
		} else if (!periodo_letivo_item_id.equals(other.periodo_letivo_item_id))
			return false;
		return true;
	}	
	
}
