package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.model.Permissao;
import br.com.insideincloud.brainstormapp.api.model.TelaSecurity;
import br.com.insideincloud.brainstormapp.api.model.Usuario;
import br.com.insideincloud.brainstormapp.api.model.view.GrupoSecurityInterface;
import br.com.insideincloud.brainstormapp.api.model.view.GrupoSecurityView;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurity;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurityInterface;
import br.com.insideincloud.brainstormapp.api.repository.GruposSecurityView;
import br.com.insideincloud.brainstormapp.api.repository.TelasSecurity;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;
import br.com.insideincloud.brainstormapp.api.resource.delete.GrupoSecurityDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.detail.GrupoSecurityDetail;

@RestController
@RequestMapping("/grupossecurity")
public class GrupoSecurityResource {
	@Autowired
	private GruposSecurity gruposSecurity;
	
	@Autowired
	private GruposSecurityView gruposSecurityView;

	@Autowired
	private TelasSecurity telasSecurity;
	
	@Autowired
	private GruposSecurityInterface gruposSecurityInterface;
	
	@Autowired
	private Usuarios usuarios;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_CONTEUDO')")
	public RetornoWrapper<GrupoSecurityView> conteudo() {
		RetornoWrapper<GrupoSecurityView> retorno = new RetornoWrapper<GrupoSecurityView>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(gruposSecurityView.findAll());
		} catch (Exception e) {
			erro.setCodigo(17);
			erro.setMensagem("Não foi possível carregar as informações do cadastro de telas do sistema, tente mais tarde.");
			erro.setDebug(""+e);
			retorno.setException(erro);
			
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_CONTEUDO')")
	public GrupoSecurity buscaPeloCodigo(@PathVariable Long codigo) {
		return gruposSecurity.findOne(codigo);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	@Transactional
	public RetornoWrapper<GrupoSecurity> salvar(@RequestBody GrupoSecurityDetail grupoSecurity, HttpServletResponse response) {
		RetornoWrapper<GrupoSecurity> retorno = new RetornoWrapper<GrupoSecurity>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(grupoSecurity.getGruposecurity().getGruposecurity_id()).toUri();
			response.setHeader("Location", uri.toASCIIString());
			List<TelaSecurity> telas = new ArrayList<TelaSecurity>();
			
			GrupoSecurity grupo = grupoSecurity.getGruposecurity();
			
			for (TelaSecurity conteudo: grupoSecurity.getTelasecurity()) {
				
				conteudo = telasSecurity.saveAndFlush(conteudo);
				telas.add(conteudo);
			}
						
			List<GrupoSecurityInterface> gruposInterface = gruposSecurityInterface.findByGruposecurity_id(grupo.getGruposecurity_id());
			gruposInterface.forEach(conteudo -> {
				Usuario user = usuarios.findOne(conteudo.getUsuario_id());
				List<Permissao> permissoes = new ArrayList<>();
				telas.forEach( tela -> {
					tela.getPermissoes().forEach( permissao -> {
						permissoes.add(permissao);
					});
				});
				user.setPermissoes(permissoes);
				usuarios.save(user);
			});
			
			grupo.setTelas(telas);						
			retorno.setSingle(gruposSecurity.saveAndFlush(grupo));
			
			if (grupoSecurity.getDeletar() != null) {
				for (TelaSecurity tela: grupoSecurity.getDeletar()) {
					telasSecurity.delete(tela.getTelasecurity_id());				
				}
			}
		} catch (Exception e) {
			erro.setCodigo(23);
			erro.setMensagem("Não foi possível salvar o grupo, favor verifique as informações.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;		
	}
	
	@PostMapping("/telassecurity")
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_SALVAR')")
	@Transactional
	public RetornoWrapper<TelaSecurity> salvarTelas(@RequestBody TelaSecurity telaSecurity, HttpServletResponse response) {
		RetornoWrapper<TelaSecurity> retorno = new RetornoWrapper<TelaSecurity>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setSingle(telasSecurity.saveAndFlush(telaSecurity));
		} catch (Exception e) {
			erro.setCodigo(7);
			erro.setMensagem("Ocorreu um erro na carga das Permissões, favor tentar novamente mais tarde. ");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}

	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_GRUPOSECURITY_DELETAR')")
	@Transactional
	public RetornoWrapper<GrupoSecurity> deletar(@RequestBody GrupoSecurityDelete gruposecurity) {
		RetornoWrapper<GrupoSecurity> retorno = new RetornoWrapper<GrupoSecurity>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			for (GrupoSecurity conteudo: gruposecurity.getGruposSecurity()) {
				gruposSecurity.delete(conteudo);
			}
		} catch (Exception e) {
			erro.setCodigo(22);
			erro.setMensagem("Não foi possível deletar um ou mais de um arquivo selecionado, tente novamente mais tarde.");
		}
		
		return retorno;
	}
	
}
