package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="avaliacao_aluno_atividade",schema="escola")
public class AvaliacaoAlunoAtividade implements Serializable {
	private static final long serialVersionUID = 8643368854261624737L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long avaliacaoalunoatividade_id;
	@OneToOne
	@JoinColumns({@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")})
	private Aluno aluno;
	@OneToOne
	@JoinColumn(name="atividade_id")
	private Atividade atividade;
//	@OneToOne
//	@JoinColumn(name="gradecurricular_id")
//	private GradeCurricularMinor gradecurricular;
	private Integer nota;
	private String conceito;
	@Column(name="avaliacao_descritiva")
	private String avaliacaodescritiva;
	@OneToOne
	@JoinColumn(name="situacaoatividade_id")
	private SituacaoAtividade situacao;
	private String transcricao;
	private Boolean revisar;
	
	public String getTranscricao() {
		return transcricao;
	}
	public void setTranscricao(String transcricao) {
		this.transcricao = transcricao;
	}
	public Boolean getRevisar() {
		return revisar;
	}
	public void setRevisar(Boolean revisar) {
		this.revisar = revisar;
	}
	public SituacaoAtividade getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoAtividade situacao) {
		this.situacao = situacao;
	}
	//	public GradeCurricularMinor getGradecurricular() {
//		return gradecurricular;
//	}
//	public void setGradecurricular(GradeCurricularMinor gradecurricular) {
//		this.gradecurricular = gradecurricular;
//	}
	public Long getAvaliacaoalunoatividade_id() {
		return avaliacaoalunoatividade_id;
	}
	public void setAvaliacaoalunoatividade_id(Long avaliacaoalunoatividade_id) {
		this.avaliacaoalunoatividade_id = avaliacaoalunoatividade_id;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getAvaliacaodescritiva() {
		return avaliacaodescritiva;
	}
	public void setAvaliacaodescritiva(String avaliacaodescritiva) {
		this.avaliacaodescritiva = avaliacaodescritiva;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avaliacaoalunoatividade_id == null) ? 0 : avaliacaoalunoatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoAlunoAtividade other = (AvaliacaoAlunoAtividade) obj;
		if (avaliacaoalunoatividade_id == null) {
			if (other.avaliacaoalunoatividade_id != null)
				return false;
		} else if (!avaliacaoalunoatividade_id.equals(other.avaliacaoalunoatividade_id))
			return false;
		return true;
	}
	
}
