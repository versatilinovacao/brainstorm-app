package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ServicoAtivoView;

@Repository
public interface ServicosAtivosView extends JpaRepository<ServicoAtivoView, Long> {

	@Query("select s from ServicoAtivoView s where s.municipio = ?1")
	public List<ServicoAtivoView> findByMunicipio(Long municipio);
}
