package br.com.insideincloud.brainstormapp.api.repository.colaboradorview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorGeralView;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorViewFilter;

public interface ColaboradoresViewQuery {
	public Page<ColaboradorView> filtrar(ColaboradorViewFilter filtro, Pageable page);
}
