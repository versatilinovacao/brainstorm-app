package br.com.insideincloud.brainstormapp.api.soap.producao.cachoeirinha;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "NFSEcancelamentoPortType", targetNamespace = "http://server.nfse.thema.inf.br")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface NFSEcancelamentoPortType {


    /**
     * 
     * @param xml
     * @return
     *     returns java.lang.String
     */
    @WebMethod(action = "urn:cancelarNfse")
    @WebResult(targetNamespace = "http://server.nfse.thema.inf.br")
    @RequestWrapper(localName = "cancelarNfse", targetNamespace = "http://server.nfse.thema.inf.br", className = "br.inf.thema.nfse.server.CancelarNfse")
    @ResponseWrapper(localName = "cancelarNfseResponse", targetNamespace = "http://server.nfse.thema.inf.br", className = "br.inf.thema.nfse.server.CancelarNfseResponse")
    public String cancelarNfse(
        @WebParam(name = "xml", targetNamespace = "http://server.nfse.thema.inf.br")
        String xml);

}
