package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PaisRemotoView;

@Repository
public interface PaisesRemoto extends JpaRepository<PaisRemotoView, Long> {
	
	@Query(value="select * from remoto.pais_view",nativeQuery = true)
	public List<PaisRemotoView> findByAll();

}
