package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="colaborador_user_view",schema="public")
public class ColaboradorUserView implements Serializable {
	private static final long serialVersionUID = -5169822520776085627L;

	@Id
	private Long colaborador_id;
	private String nome;
	private LocalDate nascimento;
	@Column(name="foto_thumbnail")
	private String fotothumbnail;
	private Long composicao_id;
	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}
	public String getFotothumbnail() {
		return fotothumbnail;
	}
	public void setFotothumbnail(String fotothumbnail) {
		this.fotothumbnail = fotothumbnail;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorUserView other = (ColaboradorUserView) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}

}
