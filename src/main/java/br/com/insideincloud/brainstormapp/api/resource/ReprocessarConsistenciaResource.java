package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.service.execute.ReprocessarConsistenciaService;

@RestController
@RequestMapping("/reprocessar")
public class ReprocessarConsistenciaResource {
	@Autowired
	private ReprocessarConsistenciaService reprocessar;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_SALVAR')")
	public void reprocessar() {
		reprocessar.executar();
	}
}
