package br.com.insideincloud.brainstormapp.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Curso;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.model.view.CursoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CursoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosItens;

@Service
public class CursoService {
	
	@Autowired
	private PeriodosLetivosItens peridosletivositens;
	
	public Curso competencias(Curso curso) {
		if (curso.getPeriodoletivo() != null) {
			List<PeriodoLetivoItem> periodos = peridosletivositens.findByPeriodosLetivosItens(curso.getPeriodoletivo().getPeriodo_letivo_id());
			
			int ultimoPeriodo = periodos.size() - 1;
			curso.setCompetencia_inicial(periodos.get(1).getPeriodo_inicial());
			curso.setCompetencia_final(periodos.get(ultimoPeriodo).getPeriodo_final());			
		}
		
		return curso;
	}

	public CursoAtivoView competencias(CursoAtivoView curso) {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>> "+curso.getPeriodo_letivo_id());
		if (curso.getPeriodo_letivo_id() != null) {
			List<PeriodoLetivoItem> periodos = peridosletivositens.findByPeriodosLetivosItens(curso.getPeriodo_letivo_id());
	
			if (periodos.size() == 1) {
				curso.setCompetencia_inicial(periodos.get(0).getPeriodo_inicial());
				curso.setCompetencia_final(periodos.get(0).getPeriodo_final());			
			} else {
				int ultimoPeriodo = periodos.size() - 1;
				curso.setCompetencia_inicial(periodos.get(0).getPeriodo_inicial());
				curso.setCompetencia_final(periodos.get(ultimoPeriodo).getPeriodo_final());			
				
			}
		}
		
		return curso;
	}

	public CursoInativoView competencias(CursoInativoView curso) {
		if (curso.getPeriodo_letivo_id() > 0l) {
			List<PeriodoLetivoItem> periodos = peridosletivositens.findByPeriodosLetivosItens(curso.getPeriodo_letivo_id());
			
			int ultimoPeriodo = periodos.size() - 1;
			curso.setCompetencia_inicial(periodos.get(1).getPeriodo_inicial());
			curso.setCompetencia_final(periodos.get(ultimoPeriodo).getPeriodo_final());			
		}
		
		return curso;
	}
	
}
