package br.com.insideincloud.brainstormapp.api.repository.chamadareportview;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaReportView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaReportViewFilter;



public class ChamadasReportViewImpl implements ChamadasReportViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ChamadaReportView> filtrar(ChamadaReportViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ChamadaReportView> criteria = builder.createQuery(ChamadaReportView.class);
		Root<ChamadaReportView> root = criteria.from(ChamadaReportView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ChamadaReportView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ChamadaReportViewFilter filtro, CriteriaBuilder builder, Root<ChamadaReportView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getAluno_id())) {
				predicates.add(builder.equal(root.get("aluno_id"), Integer.parseInt(filtro.getAluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getChamada_id())) {
				predicates.add(builder.equal(root.get("chamada_id"), Long.parseLong(filtro.getChamada_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getAluno_id())) {
				predicates.add(builder.equal(root.get("aluno_id"), Long.parseLong(filtro.getAluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getMes())) {
				predicates.add(builder.equal(root.get("mes"),     Long.parseLong(filtro.getMes()) ));
			}
			
			if (!StringUtils.isEmpty(filtro.getAno())) {
				predicates.add(builder.equal(root.get("ano"),     Long.parseLong(filtro.getAno()) ));
			}
			
//			if (!StringUtils.isEmpty(filtro.getNome_aluno())) {
//				predicates.add(builder.like(builder.lower(root.get("nome_aluno")), "%" + filtro.getNome_aluno().toLowerCase() + "%"));
//			}
//			
//			if (!StringUtils.isEmpty(filtro.getEvento())) {
//				predicates.add(builder.equal(root.get("evento"), LocalDate.parse(filtro.getEvento())));
//			}		
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ChamadaReportViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ChamadaReportView> root = criteria.from(ChamadaReportView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}
