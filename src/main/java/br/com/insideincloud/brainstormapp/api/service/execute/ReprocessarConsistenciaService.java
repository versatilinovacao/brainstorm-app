package br.com.insideincloud.brainstormapp.api.service.execute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.execute.ReprocessarConsistencia;
import br.com.insideincloud.brainstormapp.api.repository.ReprocessarConsistencias;

@Service
public class ReprocessarConsistenciaService {
	@Autowired
	private ReprocessarConsistencias reprocessar;
	
	public void executar() {
		ReprocessarConsistencia reprocessarConsistencia = new ReprocessarConsistencia();
		reprocessarConsistencia.setExecutar(true);
		reprocessar.save(reprocessarConsistencia);
	}

}
