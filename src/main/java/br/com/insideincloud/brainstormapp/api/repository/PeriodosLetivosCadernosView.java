package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoCadernoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoCadernoViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.periodoletivocadernoview.PeriodosLetivosCadernosViewQuery;

@Repository
public interface PeriodosLetivosCadernosView extends JpaRepository<PeriodoLetivoCadernoView,Long>, PeriodosLetivosCadernosViewQuery {
	public Page<PeriodoLetivoCadernoView> filtrar(PeriodoLetivoCadernoViewFilter filtro, Pageable page);
	
	@Query("select p from PeriodoLetivoCadernoView p where p.caderno_id = ?1")
	public List<PeriodoLetivoCadernoView> findByPeriodoPorCaderno(Long caderno_id);
}
