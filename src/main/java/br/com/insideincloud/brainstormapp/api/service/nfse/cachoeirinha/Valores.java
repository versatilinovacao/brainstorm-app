package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class Valores {
	private String ValorServicos;
	private String IssRetido;
	private String ValorIss;
	private String BaseCalculo;
	private String Aliquota;
	private String ValorLiquidoNfse;
	
	public String getValorServicos() {
		return ValorServicos;
	}
	public void setValorServicos(String valorServicos) {
		ValorServicos = valorServicos;
	}
	public String getIssRetido() {
		return IssRetido;
	}
	public void setIssRetido(String issRetido) {
		IssRetido = issRetido;
	}
	public String getValorIss() {
		return ValorIss;
	}
	public void setValorIss(String valorIss) {
		ValorIss = valorIss;
	}
	public String getBaseCalculo() {
		return BaseCalculo;
	}
	public void setBaseCalculo(String baseCalculo) {
		BaseCalculo = baseCalculo;
	}
	public String getAliquota() {
		return Aliquota;
	}
	public void setAliquota(String aliquota) {
		Aliquota = aliquota;
	}
	public String getValorLiquidoNfse() {
		return ValorLiquidoNfse;
	}
	public void setValorLiquidoNfse(String valorLiquidoNfse) {
		ValorLiquidoNfse = valorLiquidoNfse;
	}

}
