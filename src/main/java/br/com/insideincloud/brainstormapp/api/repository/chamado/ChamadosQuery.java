package br.com.insideincloud.brainstormapp.api.repository.chamado;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Chamado;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadoFilter;

public interface ChamadosQuery {
	public Page<Chamado> filtrar(ChamadoFilter filtro, Pageable page);
}
