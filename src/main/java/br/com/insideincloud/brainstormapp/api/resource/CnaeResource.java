package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Cnae;
import br.com.insideincloud.brainstormapp.api.model.execute.DeletarCnaeAction;
import br.com.insideincloud.brainstormapp.api.model.view.CnaeSecundarioView;
import br.com.insideincloud.brainstormapp.api.repository.Cnaes;
import br.com.insideincloud.brainstormapp.api.repository.CnaesSecundariosView;
import br.com.insideincloud.brainstormapp.api.repository.DeletarCnaesAction;
import br.com.insideincloud.brainstormapp.api.resource.delete.CnaeDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/cnaes")
public class CnaeResource {
	@Autowired
	private Cnaes cnaes;
	
	@Autowired
	private DeletarCnaesAction deletarCnaes;
	
	@Autowired
	private CnaesSecundariosView cnaesSecundarios;
	
	@GetMapping("/secundarios")
	@PreAuthorize("hasAuthority('ROLE_CNAE_CONTEUDO')")
	public RetornoWrapper<CnaeSecundarioView> cnaesSecundarios() {
		RetornoWrapper<CnaeSecundarioView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cnaesSecundarios.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("As informações de cnaes não estão disponiveis no momento, tente novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CNAE_CONTEUDO')")
	public RetornoWrapper<Cnae> conteudo() {
		RetornoWrapper<Cnae> retorno = new RetornoWrapper<Cnae>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(cnaes.findAll());
			
		} catch (Exception e) {
			erro.setCodigo(15);
			erro.setMensagem("Falha na carga dos CNAES, favor tentar novamente em alguns minutos.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CNAE_CONTEUDO')")
	public Cnae editar(@PathVariable Long codigo, HttpServletResponse response) {
		return cnaes.findOne(codigo);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CNAE_SALVAR')")
	public RetornoWrapper<Cnae> salvar(@RequestBody Cnae cnae, HttpServletResponse response) {
		RetornoWrapper<Cnae> retorno = new RetornoWrapper<Cnae>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			cnae = cnaes.saveAndFlush(cnae);
			retorno.setSingle(cnae);
			retorno.setConteudo(cnaes.findAll());
		} catch (Exception e) {
			erro.setCodigo(10);
			erro.setMensagem("Ocorreu um erro ao tentar salvar o CNAE, favor tentar novamente.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(cnae.getCnae_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_CNAE_DELETAR')")
	public RetornoWrapper<Cnae> deletar(@RequestBody CnaeDelete cnae, HttpServletResponse response) {
		RetornoWrapper<Cnae> retorno = new RetornoWrapper<Cnae>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			
			for (Cnae conteudo : cnae.getCnaes()) {
				DeletarCnaeAction deletarCnae = new DeletarCnaeAction();
				deletarCnae.setCnae_id(conteudo.getCnae_id());

				deletarCnaes.save(deletarCnae);
			}
			retorno.setConteudo(cnaes.findAll());
			
		} catch (Exception e) {
			erro.setCodigo(12);
			erro.setMensagem("Ocorreu um erro ao tentar deletar o CNAE, favor tentar novamente se necessário.");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	
}
