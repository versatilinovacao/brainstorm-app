package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contratos_parcelas_view",schema="administracao")
public class ContratoParcelaView implements Serializable {
	private static final long serialVersionUID = -703943214822905394L;
	
	@Id
	private Long id;
	private Long contrato_id;
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao;
	private Long devedor_id;
	private Long composicao_devedor_id;
	private Double desconto;
	private Double valorpago;
	private Double saldo;
	private Double total;
	private Long negociacao_id;
	private String descricaonegociacao;
	private Long formapagamento_id;
	private String descricaoformapagamento;
	private Integer parcela;
	private Boolean protesto;
	private Long tipopagamento_id;
	private String descricaotipopagamento;
	private String descricao;
	private String status;
	
	public Long getContrato_id() {
		return contrato_id;
	}
	public void setContrato_id(Long contrato_id) {
		this.contrato_id = contrato_id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public Long getDevedor_id() {
		return devedor_id;
	}
	public void setDevedor_id(Long devedor_id) {
		this.devedor_id = devedor_id;
	}
	public Long getComposicao_devedor_id() {
		return composicao_devedor_id;
	}
	public void setComposicao_devedor_id(Long composicao_devedor_id) {
		this.composicao_devedor_id = composicao_devedor_id;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Double getValorpago() {
		return valorpago;
	}
	public void setValorpago(Double valorpago) {
		this.valorpago = valorpago;
	}
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Long getNegociacao_id() {
		return negociacao_id;
	}
	public void setNegociacao_id(Long negociacao_id) {
		this.negociacao_id = negociacao_id;
	}
	public String getDescricaonegociacao() {
		return descricaonegociacao;
	}
	public void setDescricaonegociacao(String descricaonegociacao) {
		this.descricaonegociacao = descricaonegociacao;
	}
	public Long getFormapagamento_id() {
		return formapagamento_id;
	}
	public void setFormapagamento_id(Long formapagamento_id) {
		this.formapagamento_id = formapagamento_id;
	}
	public String getDescricaoformapagamento() {
		return descricaoformapagamento;
	}
	public void setDescricaoformapagamento(String descricaoformapagamento) {
		this.descricaoformapagamento = descricaoformapagamento;
	}
	public Integer getParcela() {
		return parcela;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public Boolean getProtesto() {
		return protesto;
	}
	public void setProtesto(Boolean protesto) {
		this.protesto = protesto;
	}
	public Long getTipopagamento_id() {
		return tipopagamento_id;
	}
	public void setTipopagamento_id(Long tipopagamento_id) {
		this.tipopagamento_id = tipopagamento_id;
	}
	public String getDescricaotipopagamento() {
		return descricaotipopagamento;
	}
	public void setDescricaotipopagamento(String descricaotipopagamento) {
		this.descricaotipopagamento = descricaotipopagamento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContratoParcelaView other = (ContratoParcelaView) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
