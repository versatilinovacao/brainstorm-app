package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="pessoajuridica")
public class PessoaJuridica implements Serializable {
	private static final long serialVersionUID = 7685560573964560040L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pessoajuridica_id;
	@Column(name="cnpj",length=14)
	private String cnpj;
//	private InscricaoEstadual inscricao_estadual;
	private String inscricao_municipal;
	@OneToOne
	@JoinColumn(name="cnae_id")
	private Cnae cnae;	

	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="pessoajuridica_cnaes", schema="empresa", joinColumns = {@JoinColumn(name="pessoajuridica_id")}, inverseJoinColumns = {@JoinColumn(name="cnae_id")})
	private List<Cnae> cnaes;
	
//	private Cae cae;
//	private Colaborador colaborador;

	public Long getPessoajuridica_id() {
		return pessoajuridica_id;
	}
	public List<Cnae> getCnaes() {
		return cnaes;
	}
	public void setCnaes(List<Cnae> cnaes) {
		this.cnaes = cnaes;
	}
	public Cnae getCnae() {
		return cnae;
	}
	public void setCnae(Cnae cnae) {
		this.cnae = cnae;
	}
	public void setPessoajuridica_id(Long pessoajuridica_id) {
		this.pessoajuridica_id = pessoajuridica_id;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getInscricao_municipal() {
		return inscricao_municipal;
	}
	public void setInscricao_municipal(String inscricao_municipal) {
		this.inscricao_municipal = inscricao_municipal;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pessoajuridica_id == null) ? 0 : pessoajuridica_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (pessoajuridica_id == null) {
			if (other.pessoajuridica_id != null)
				return false;
		} else if (!pessoajuridica_id.equals(other.pessoajuridica_id))
			return false;
		return true;
	}

	
	
}
