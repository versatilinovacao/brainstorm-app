package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_usuarios")
public class TipoUsuario implements Serializable {
	private static final long serialVersionUID = -8546808096813274524L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipousuario_id;
	private String nome;
	
	public Long getTipousuario_id() {
		return tipousuario_id;
	}
	public void setTipousuario_id(Long tipousuario_id) {
		this.tipousuario_id = tipousuario_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipousuario_id == null) ? 0 : tipousuario_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoUsuario other = (TipoUsuario) obj;
		if (tipousuario_id == null) {
			if (other.tipousuario_id != null)
				return false;
		} else if (!tipousuario_id.equals(other.tipousuario_id))
			return false;
		return true;
	}
	
	
}
