package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_servicos_inativos_view",schema="contabil")
public class TipoServicoInativoView implements Serializable {
	private static final long serialVersionUID = 4251981962959184264L;
	
	@Id
	private Long tiposervico_id;
	private String descricao;
	private Boolean status;
	
	public Long getTiposervico_id() {
		return tiposervico_id;
	}
	public void setTiposervico_id(Long tiposervico_id) {
		this.tiposervico_id = tiposervico_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiposervico_id == null) ? 0 : tiposervico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoServicoInativoView other = (TipoServicoInativoView) obj;
		if (tiposervico_id == null) {
			if (other.tiposervico_id != null)
				return false;
		} else if (!tiposervico_id.equals(other.tiposervico_id))
			return false;
		return true;
	}
	
}
