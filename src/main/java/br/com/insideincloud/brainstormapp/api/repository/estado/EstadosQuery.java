package br.com.insideincloud.brainstormapp.api.repository.estado;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Estado;
import br.com.insideincloud.brainstormapp.api.repository.filter.EstadoFilter;

public interface EstadosQuery {
	public Page<Estado> filtrar(EstadoFilter filtro, Pageable page);

}
