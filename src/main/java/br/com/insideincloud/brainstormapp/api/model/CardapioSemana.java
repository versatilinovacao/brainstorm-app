package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="cardapios_semanas",schema="escola")
public class CardapioSemana implements Serializable {
	private static final long serialVersionUID = 1953434604634969733L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cardapiosemana_id;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="cardapio_id")
	private Cardapio cardapio;
	private LocalDate evento;
	private String cafemanha;
	private String lanchemanha;
	private String almoco;
	private String lanchetarde;
	private String janta;
	
	public LocalDate getEvento() {
		return evento;
	}
	public void setEvento(LocalDate evento) {
		this.evento = evento;
	}
	public Long getCardapiosemana_id() {
		return cardapiosemana_id;
	}
	public void setCardapiosemana_id(Long cardapiosemana_id) {
		this.cardapiosemana_id = cardapiosemana_id;
	}
	public Cardapio getCardapio() {
		return cardapio;
	}
	public void setCardapio(Cardapio cardapio) {
		this.cardapio = cardapio;
	}
	public String getCafemanha() {
		return cafemanha;
	}
	public void setCafemanha(String cafemanha) {
		this.cafemanha = cafemanha;
	}
	public String getLanchemanha() {
		return lanchemanha;
	}
	public void setLanchemanha(String lanchemanha) {
		this.lanchemanha = lanchemanha;
	}
	public String getAlmoco() {
		return almoco;
	}
	public void setAlmoco(String almoco) {
		this.almoco = almoco;
	}
	public String getLanchetarde() {
		return lanchetarde;
	}
	public void setLanchetarde(String lanchetarde) {
		this.lanchetarde = lanchetarde;
	}
	public String getJanta() {
		return janta;
	}
	public void setJanta(String janta) {
		this.janta = janta;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardapiosemana_id == null) ? 0 : cardapiosemana_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardapioSemana other = (CardapioSemana) obj;
		if (cardapiosemana_id == null) {
			if (other.cardapiosemana_id != null)
				return false;
		} else if (!cardapiosemana_id.equals(other.cardapiosemana_id))
			return false;
		return true;
	}

}
