package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_renovacoes",schema="administracao")
public class TipoRenovacao implements Serializable {
	private static final long serialVersionUID = -628411795214414301L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tiporenovacao_id;
	private String descricao;

	public Long getTiporenovacao_id() {
		return tiporenovacao_id;
	}
	public void setTiporenovacao_id(Long tiporenovacao_id) {
		this.tiporenovacao_id = tiporenovacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiporenovacao_id == null) ? 0 : tiporenovacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoRenovacao other = (TipoRenovacao) obj;
		if (tiporenovacao_id == null) {
			if (other.tiporenovacao_id != null)
				return false;
		} else if (!tiporenovacao_id.equals(other.tiporenovacao_id))
			return false;
		return true;
	}
}
