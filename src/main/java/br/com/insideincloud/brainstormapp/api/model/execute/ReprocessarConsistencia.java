package br.com.insideincloud.brainstormapp.api.model.execute;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reprocessar_consistencias",schema="public")
public class ReprocessarConsistencia implements Serializable {
	private static final long serialVersionUID = 1401858871666427446L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long reprocessar_consistencia_id;
	private boolean executar;
	
	public Long getReprocessar_consistencia_id() {
		return reprocessar_consistencia_id;
	}
	public void setReprocessar_consistencia_id(Long reprocessar_consistencia_id) {
		this.reprocessar_consistencia_id = reprocessar_consistencia_id;
	}
	public boolean isExecutar() {
		return executar;
	}
	public void setExecutar(boolean executar) {
		this.executar = executar;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((reprocessar_consistencia_id == null) ? 0 : reprocessar_consistencia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReprocessarConsistencia other = (ReprocessarConsistencia) obj;
		if (reprocessar_consistencia_id == null) {
			if (other.reprocessar_consistencia_id != null)
				return false;
		} else if (!reprocessar_consistencia_id.equals(other.reprocessar_consistencia_id))
			return false;
		return true;
	}
	
	
}
