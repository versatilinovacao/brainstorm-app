package br.com.insideincloud.brainstormapp.api.repository.cadernomesview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.CadernoMesView;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoMesViewFilter;

public class CadernosMesesViewImpl implements CadernosMesesViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<CadernoMesView> filtrar(CadernoMesViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CadernoMesView> criteria = builder.createQuery(CadernoMesView.class);
		Root<CadernoMesView> root = criteria.from(CadernoMesView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<CadernoMesView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(CadernoMesViewFilter filtro, CriteriaBuilder builder, Root<CadernoMesView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("_caderno_id"), Integer.parseInt(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getAno())) {
				predicates.add(builder.equal(root.get("ano"), Long.parseLong(filtro.getAno())));
			}		

			if (!StringUtils.isEmpty(filtro.getMes())) {
				predicates.add(builder.equal(root.get("mes"), Long.parseLong(filtro.getMes())));
			}		
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(CadernoMesViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<CadernoMesView> root = criteria.from(CadernoMesView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
