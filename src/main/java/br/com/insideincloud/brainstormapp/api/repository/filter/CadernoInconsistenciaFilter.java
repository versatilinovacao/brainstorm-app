package br.com.insideincloud.brainstormapp.api.repository.filter;

public class CadernoInconsistenciaFilter {
	private String caderno_inconsistencia_id;
	private String caderno_id;
	private String resumo;
	
	public String getCaderno_inconsistencia_id() {
		return caderno_inconsistencia_id;
	}
	public void setCaderno_inconsistencia_id(String caderno_inconsistencia_id) {
		this.caderno_inconsistencia_id = caderno_inconsistencia_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	
}
