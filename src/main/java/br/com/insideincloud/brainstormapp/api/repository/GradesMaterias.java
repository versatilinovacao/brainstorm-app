package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.GradeMateria;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeMateriaFilter;

@Repository
public interface GradesMaterias extends JpaRepository<GradeMateria,Long> {
	public Page<GradeMateria> filtrar(GradeMateriaFilter filtro, Pageable page);
}
