package br.com.insideincloud.brainstormapp.api.resource;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Aluno;
import br.com.insideincloud.brainstormapp.api.model.AtividadeUpload;
import br.com.insideincloud.brainstormapp.api.model.AvaliacaoAlunoAtividade;
import br.com.insideincloud.brainstormapp.api.model.SituacaoAtividade;
import br.com.insideincloud.brainstormapp.api.model.TipoArquivo;
import br.com.insideincloud.brainstormapp.api.model.minor.AtividadeMinor;
import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.repository.AlunosView;
import br.com.insideincloud.brainstormapp.api.repository.Atividades;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesMinor;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesUpload;
import br.com.insideincloud.brainstormapp.api.repository.AvaliacoesAlunosAtividades;
import br.com.insideincloud.brainstormapp.api.repository.ColaboradoresMinor;
import br.com.insideincloud.brainstormapp.api.repository.EmpresasComposicoesView;
import br.com.insideincloud.brainstormapp.api.repository.SituacoesAtividades;
import br.com.insideincloud.brainstormapp.api.repository.TiposArquivos;
import br.com.insideincloud.brainstormapp.api.resource.param.Transcricao;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.util.Disco;

@RestController
@RequestMapping("/ambiente")
public class AmbienteUsuarioResource {
	
	@Autowired
	private AvaliacoesAlunosAtividades avaliacoes;
	
	@Autowired
	private EmpresasComposicoesView empresas;
	
	@Autowired
	private ColaboradoresMinor alunos;
	
	@Autowired
	private TiposArquivos tiposarquivos;
	
	@Autowired
	private Atividades atividades;	
	
	@Autowired
	private AtividadesUpload atividadesUpload;	
	
	@Autowired
	private SituacoesAtividades situacoesatividades;
	
	@Autowired
	private AtividadesMinor atividadesMinor;

	@Autowired
	private AlunosView a;
	
	@PutMapping("/conteudo/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_SALVAR')")
	public RetornoWrapper<AvaliacaoAlunoAtividade> salvarAtividade(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id, @RequestBody Transcricao transcricao) {
		RetornoWrapper<AvaliacaoAlunoAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			
			AvaliacaoAlunoAtividade avaliacao = avaliacoes.findByAvaliacaoAluno(atividade_id, aluno_id, composicao_aluno_id);
			if (avaliacao == null) {
				ColaboradorPK id = new ColaboradorPK();
				id.setColaborador_id(aluno_id);
				id.setComposicao_id(composicao_aluno_id);
				
				Aluno aluno = new Aluno();
				aluno.setAluno_id(id);
				
				avaliacao = new AvaliacaoAlunoAtividade();
				avaliacao.setAtividade( atividades.findOne(atividade_id) );
				avaliacao.setAluno( aluno );
				avaliacao.setTranscricao(transcricao.getTranscricao());
				
				
			} else {
				avaliacao.setTranscricao(transcricao.getTranscricao());
			}
			
			avaliacoes.saveAndFlush(avaliacao);
			
		} catch(Exception e) {
			exception.setCodigo(4);
			exception.setMensagem("Não foi possível salvar a atividade para ser avaliada. Favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			e.printStackTrace();
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/arquivo/{atividade_id}/{aluno_id}/{composicao_aluno_id}/{nome_arquivo}/{tipo}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_SALVAR')")
	public RetornoWrapper<AtividadeUpload> salvarArquivo(@PathVariable Long atividade_id, 
														 @PathVariable Long aluno_id, 
														 @PathVariable Long composicao_aluno_id, 
														 @PathVariable String nome_arquivo, 
														 @PathVariable String tipo,
														 @RequestParam MultipartFile file) {
		
		RetornoWrapper<AtividadeUpload> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		String empresa = empresas.getOne(1L).getNome();
				
		ColaboradorPK id = new ColaboradorPK();
		id.setColaborador_id(aluno_id);
		id.setComposicao_id(composicao_aluno_id);
		
		ColaboradorMinor aluno = alunos.getOne(id);
		
		Disco disco = new Disco( "/var/local/insideincloud/repositorio"+"/"+empresa, aluno.getNome() );
		TipoArquivo tipoArquivo = tiposarquivos.findByTipo(tipo.toUpperCase());
		
		AtividadeUpload atividadeUpload = new AtividadeUpload();
		atividadeUpload.setNome( nome_arquivo );
		atividadeUpload.setPath( "/var/local/insideincloud/repositorio"+"/"+empresa+"/"+aluno.getNome() );
		atividadeUpload.setSize(file.getSize());
		atividadeUpload.setTipoarquivo(tipoArquivo);
		atividadeUpload.setAtividade(atividades.findOne(atividade_id));
		atividadeUpload.setEvento(LocalDateTime.now());
		
		try {
			atividadesUpload.saveAndFlush(atividadeUpload);
			disco.salvarArquivo(file);
			retorno.setConteudo(atividadesUpload.findAllAtividades(atividade_id));
		} catch (IOException e) {
			exception.setCodigo(1);
			exception.setMensagem("ERRO: Não foi possível transferir o arquivo.");
			exception.setDebug(""+e);
			throw new RuntimeException("Problemas na tentativa de salvar o arquivo");
		}
		
		return retorno;
	}
	
	@PutMapping("/atividade/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_SALVAR')")
	public RetornoWrapper<AvaliacaoAlunoAtividade> entregarAtividade(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<AvaliacaoAlunoAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			AvaliacaoAlunoAtividade avaliacao = avaliacoes.findByAvaliacaoAluno(atividade_id, aluno_id, composicao_aluno_id);
			if (avaliacao == null) {
				ColaboradorPK id = new ColaboradorPK();
				id.setColaborador_id(aluno_id);
				id.setComposicao_id(composicao_aluno_id);
				
				Aluno aluno = new Aluno();
				aluno.setAluno_id(id);
				
				avaliacao = new AvaliacaoAlunoAtividade();
				avaliacao.setAtividade( atividades.findOne(atividade_id) );
				avaliacao.setAluno( aluno );
			
			}
			
			avaliacao.setRevisar(false);
			SituacaoAtividade situacao;
			situacao = situacoesatividades.findOne(3L);				
			avaliacao.setSituacao(situacao);
			
//			if (avaliacao.getAtividade().getDataatividade(). <= LocalDate.now()) {
				retorno.setSingle( avaliacoes.saveAndFlush(avaliacao) );
//			}
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a avaliação.");
			exception.setDebug(""+e);
			retorno.setException(exception);
			e.printStackTrace();
		}
		
		
		return retorno;
	}
	
	
	@GetMapping("/atividade/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<AtividadeMinor> conteudoAtividadePorId(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<AtividadeMinor> retorno = new RetornoWrapper<AtividadeMinor>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( atividadesMinor.findByAtividade(atividade_id,aluno_id,composicao_aluno_id) );
		} catch(Exception e) {
			System.out.println(""+e);
			exception.setCodigo(1);
			exception.setMensagem("Não foi localizada a atividade, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}	
	
	
	
	
}
