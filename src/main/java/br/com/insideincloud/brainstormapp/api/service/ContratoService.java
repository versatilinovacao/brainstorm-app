package br.com.insideincloud.brainstormapp.api.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.ContaReceber;
import br.com.insideincloud.brainstormapp.api.model.Contrato;
import br.com.insideincloud.brainstormapp.api.model.FormaPagamento;
import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorConsumidor;
import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.ContasReceber;
import br.com.insideincloud.brainstormapp.api.repository.Contratos;
import br.com.insideincloud.brainstormapp.api.repository.FormasPagamentos;

@Service
public class ContratoService {

	@Autowired
	private Contratos contratos;
	
	@Autowired
	private ContasReceber contasReceber;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@Autowired
	private FormasPagamentos formasPagamentos;
	
	public Contrato findOne(Long contrato_id) {
		return contratos.findOne(contrato_id);
	}
	
	@Transactional
	public Contrato salvar(Contrato contrato) {
				
		try {
			contrato.setParcelas(montarParcelas(contrato));
			if (contrato.getFormapagamento() != null) {
				if (contrato.getFormapagamento().getFormapagamento_id() == null) { contrato.setFormapagamento(null); }
			}
			contrato = contratos.saveAndFlush(contrato);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contrato;
	}
	
	private List<ContaReceber> montarParcelas(Contrato contrato) {
		
		Integer isentas = 0;
		List<ContaReceber> parcelas = new ArrayList<>();
		LocalDate vencimento = contrato.getIniciocontrato();
		for (int x=0 ; x < contrato.getDuracaoinicial(); x++) {
			vencimento = LocalDate.of(
					vencimento.getYear(), 
					vencimento.getMonthValue(), 
					contrato.getDiapagamento());
			
			ContaReceber conta = new ContaReceber();
			conta.setDevedor(parseColaborador(contrato.getContratante()));
			conta.setDescricao("Contrato firmado com: "+colaboradores.findOne( contrato.getContratante().getColaborador_id() ).getNome());
			conta.setEmissao(contrato.getIniciocontrato());
			conta.setNegociacao( contrato.getNegociacao());
			if (contrato.getParcelasisentas() != null) {
				if(contrato.getParcelasisentas() > 0 && isentas <= contrato.getParcelasisentas()) {
					conta.setDesconto(contrato.getMensalidade());
					conta.setPagamento(contrato.getIniciocontrato());
					conta.setQuitacao(contrato.getIniciocontrato());
//					conta.setFormapagamento(  );
					conta.setValorpago(contrato.getMensalidade());
					
					if (contrato.getFormapagamento() == null) {
						FormaPagamento formaPagamento = formasPagamentos.findByDescricao("Isento");
						conta.setFormapagamento(formaPagamento);
					}
					
					isentas++;
				}				
			}
			conta.setParcela(x);
			conta.setVencimento(vencimento);
			conta.setTotal(contrato.getMensalidade());

			conta.setStatus(true);	
			
			parcelas.add(conta);
			vencimento = vencimento.plusMonths(1);
		}
		
		
		return parcelas;
	}
	
//	private List<ContaReceber> renovarParcelas(Contrato contrato) {
//		
//		return null;
//	}
	
	private ColaboradorConsumidor parseColaborador(ColaboradorMinor colaborador) {
		ColaboradorConsumidor devedor = new ColaboradorConsumidor();
		devedor.setColaborador_id(colaborador.getColaborador_id());
		devedor.setNome(colaborador.getNome());
		devedor.setFantasia(colaborador.getFantasia());
		
		return devedor;
	}
	
}
