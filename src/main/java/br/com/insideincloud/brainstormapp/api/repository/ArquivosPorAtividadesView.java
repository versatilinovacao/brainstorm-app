package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ArquivoPorAtividadeView;

@Repository
public interface ArquivosPorAtividadesView extends JpaRepository<ArquivoPorAtividadeView, Long> {
	
	@Query("select a from ArquivoPorAtividadeView a where a.atividade_id = ?1 and a.aluno_id = ?2 and a.composicao_aluno_id = ?3")
	public List<ArquivoPorAtividadeView> findByArquivos(Long atividade_id,Long aluno_id, Long composicao_aluno_id);
	
	@Query("select aa from ArquivoPorAtividadeView aa where aa.atividade_id = ?1 and aa.aluno_id = ?2 and aa.composicao_aluno_id = ?3")
	public List<ArquivoPorAtividadeView> findByArquivosAlunos(Long atividade_id, Long aluno_id, Long composicao_aluno_id);

}
