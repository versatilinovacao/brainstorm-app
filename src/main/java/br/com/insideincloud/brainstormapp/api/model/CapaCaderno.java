package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="capas_cadernos", schema="escola")
public class CapaCaderno implements Serializable {
	private static final long serialVersionUID = 7095550737388739741L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long capacaderno_id;
	@Column(name="titulo")
	private String titulo;
	@Column(name="descricao")
	private String descricao;
	@Column(name="apresentacao")
	private String apresentacao;
	@ManyToOne
	@JoinColumn(name="caderno_id")
	@Cascade(CascadeType.MERGE)
	private Caderno caderno;
	
	public Caderno getCaderno() {
		return this.caderno;
	}
	public void setCaderno(Caderno caderno) {
		this.caderno = caderno;
	}
	public Long getCapacaderno_id() {
		return capacaderno_id;
	}
	public void setCapacaderno_id(Long capacaderno_id) {
		this.capacaderno_id = capacaderno_id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getApresentacao() {
		return apresentacao;
	}
	public void setApresentacao(String apresentacao) {
		this.apresentacao = apresentacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((capacaderno_id == null) ? 0 : capacaderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CapaCaderno other = (CapaCaderno) obj;
		if (capacaderno_id == null) {
			if (other.capacaderno_id != null)
				return false;
		} else if (!capacaderno_id.equals(other.capacaderno_id))
			return false;
		return true;
	}
	
	
}
