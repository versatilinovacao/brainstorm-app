package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadaAlunoFilter {
	private String chamada_aluno_id;
	private String caderno_id;
	private String chamada_id;
	private String evento;
	
	public String getChamada_aluno_id() {
		return chamada_aluno_id;
	}
	public void setChamada_aluno_id(String chamada_aluno_id) {
		this.chamada_aluno_id = chamada_aluno_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(String chamada_id) {
		this.chamada_id = chamada_id;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	
}
