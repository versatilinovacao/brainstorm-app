package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.Sede;

public class SedeDelete {
	private List<Sede> sedes;

	public List<Sede> getSedes() {
		return sedes;
	}

	public void setSedes(List<Sede> sedes) {
		this.sedes = sedes;
	}


}
