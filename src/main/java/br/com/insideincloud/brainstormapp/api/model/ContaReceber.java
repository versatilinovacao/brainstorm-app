package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorConsumidor;
import br.com.insideincloud.brainstormapp.api.model.minor.EmpresaDevedorCredor;
import br.com.insideincloud.brainstormapp.api.model.view.PlanoContaAtivaView;

@Entity
@Table(name="contas_receber",schema="financeiro")
public class ContaReceber implements Serializable {
	private static final long serialVersionUID = -2774792988084988717L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long contareceber_id;
	
	@ManyToOne
	@JoinColumn(name="composicao_id",insertable = true, updatable = false)
	@JsonBackReference
	private ContaReceber composicao;
	
	@JsonManagedReference
	@OneToMany(mappedBy="composicao")
	private List<ContaReceber> contas;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="empresa_id"),@JoinColumn(name="composicao_empresa_id")})
	private EmpresaDevedorCredor empresa;
	
	@OneToOne
	@JoinColumn(name="planoconta_id")
	private PlanoContaAtivaView planoconta;
	
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao; //Data que realmente entrou o dinheiro.

	private double desconto;
	private double valorpago;
	private double saldo;
	private double total;
	
	@OneToOne
	@JoinColumn(name="tipopagamento_id")
	private TipoConta tipo;
	
	@OneToOne
	@JoinColumn(name="negociacao_id")
	private Negociacao negociacao; //Identifica o tipo de pagamento combinado (Dinheiro, Cartão, Boleto, Promissória, Cheque,
	@OneToOne
	@JoinColumn(name="formapagamento_id")
	private FormaPagamento formapagamento; //Identifica o tipo de pagamento utilizado (Dinheiro, Cartao, Boleto, Promissória, Cheque, 
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="devedor_id"),@JoinColumn(name="composicao_devedor_id")})
	private ColaboradorConsumidor devedor;
	
	private Integer parcela; //Identifica o número da parcela.

	private Boolean protesto; //Identifica se este cobrança esta em protesto.
	
	private Boolean status;
	
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public PlanoContaAtivaView getPlanoconta() {
		return planoconta;
	}

	public void setPlanoconta(PlanoContaAtivaView planoconta) {
		this.planoconta = planoconta;
	}

	public ColaboradorConsumidor getDevedor() {
		return devedor;
	}

	public void setDevedor(ColaboradorConsumidor devedor) {
		this.devedor = devedor;
	}

	public List<ContaReceber> getContas() {
		return contas;
	}

	public void setContas(List<ContaReceber> contas) {
		this.contas = contas;
	}

	public ContaReceber getComposicao() {
		return composicao;
	}

	public void setComposicao(ContaReceber composicao) {
		this.composicao = composicao;
	}

	public Long getContareceber_id() {
		return contareceber_id;
	}

//	public Long getComposicao_id() {
//		return composicao_id;
//	}
//
//	public void setComposicao_id(Long composicao_id) {
//		if (composicao == null) { composicao_id = 0l; }
//		this.composicao_id = composicao_id;
//	}

	public void setContareceber_id(Long contareceber_id) {
		this.contareceber_id = contareceber_id;
	}

	public TipoConta getTipo() {
		return tipo;
	}

	public void setTipo(TipoConta tipo) {
		this.tipo = tipo;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public EmpresaDevedorCredor getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaDevedorCredor empresa) {
		this.empresa = empresa;
	}

	public LocalDate getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}

	public LocalDate getEmissao() {
		return emissao;
	}

	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}

	public LocalDate getPagamento() {
		return pagamento;
	}

	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}

	public LocalDate getQuitacao() {
		return quitacao;
	}

	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}

	public double getValorpago() {
		return valorpago;
	}

	public void setValorpago(double valorpago) {
		this.valorpago = valorpago;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Negociacao getNegociacao() {
		return negociacao;
	}

	public void setNegociacao(Negociacao negociacao) {
		this.negociacao = negociacao;
	}

	public FormaPagamento getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(FormaPagamento formapagamento) {
		this.formapagamento = formapagamento;
	}

	public Integer getParcela() {
		return parcela;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public Boolean getProtesto() {
		return protesto;
	}

	public void setProtesto(Boolean protesto) {
		this.protesto = protesto;
	}
	
}
