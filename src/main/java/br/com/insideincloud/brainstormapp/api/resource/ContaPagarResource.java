package br.com.insideincloud.brainstormapp.api.resource;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ContaPagar;
import br.com.insideincloud.brainstormapp.api.model.FormaPagamento;
import br.com.insideincloud.brainstormapp.api.model.Negociacao;
import br.com.insideincloud.brainstormapp.api.model.TipoConta;
import br.com.insideincloud.brainstormapp.api.model.view.ContaPagarAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.ContaPagarInativaView;
import br.com.insideincloud.brainstormapp.api.model.view.ContaPagarPendenteView;
import br.com.insideincloud.brainstormapp.api.repository.ContasPagar;
import br.com.insideincloud.brainstormapp.api.repository.ContasPagarAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.ContasPagarInativasView;
import br.com.insideincloud.brainstormapp.api.repository.ContasPagarPendentesView;
import br.com.insideincloud.brainstormapp.api.repository.FormasPagamentos;
import br.com.insideincloud.brainstormapp.api.repository.Negociacoes;
import br.com.insideincloud.brainstormapp.api.repository.TiposContas;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/contaspagar")
public class ContaPagarResource {

	@Autowired
	private ContasPagarAtivasView contasAtivas;
	
	@Autowired
	private ContasPagarInativasView contasInativas;
	
	@Autowired
	private ContasPagar contas;
	
	@Autowired
	private TiposContas tipos;
	
	@Autowired
	private Negociacoes negociacoes;
	
	@Autowired
	private FormasPagamentos formas;
	
	@Autowired
	private ContasPagarPendentesView contasPendentes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaPagarAtivaView> conteudoAtivo() {
		RetornoWrapper<ContaPagarAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasAtivas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma conta ativa, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaPagarInativaView> conteudoInativo() {
		RetornoWrapper<ContaPagarInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasInativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma conta inativa, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaPagar> conteudoPorCodigo(@PathVariable Long codigo,@PathVariable Long composicao) {
		RetornoWrapper<ContaPagar> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			ContaPagarPK id = new ContaPagarPK();
//			id.setContapagar_id(codigo);
//			id.setComposicao_id(composicao);
			
			retorno.setSingle( contas.findOne(codigo) );
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar a conta solicitada, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{vencimento}/{devedor_id}/{composicao_devedor_id}")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaPagarAtivaView> conteudoVencimentoDevedor(@PathVariable LocalDate vencimento, @PathVariable Long credor_id, @PathVariable Long composicao_credor_id) {
		
		return null;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_SALVAR')")
	public RetornoWrapper<ContaPagar> salvar(@RequestBody ContaPagar conta) {
		RetornoWrapper<ContaPagar> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( contas.saveAndFlush(conta) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a conta, verfique e tente novamente.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<TipoConta> tipos() {
		RetornoWrapper<TipoConta> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( tipos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum tipo de pagamento, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/negociacoes")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<Negociacao> negociacoes() {
		RetornoWrapper<Negociacao> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( negociacoes.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma forma de negociação, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	} 
	
	@GetMapping("/formas")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<FormaPagamento> formas() {
		RetornoWrapper<FormaPagamento> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( formas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhuma forma de pagamento, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/pendentes")
	@PreAuthorize("hasAuthority('ROLE_CONTARECEBER_CONTEUDO')")
	public RetornoWrapper<ContaPagarPendenteView> conteudoPendente() {
		RetornoWrapper<ContaPagarPendenteView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( contasPendentes.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível apresentar listar as contas pendentes, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
