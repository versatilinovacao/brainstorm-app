package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.Classificacao;
import br.com.insideincloud.brainstormapp.api.repository.Classificacoes;

@RestController
@RequestMapping("/classificacoes")
public class ClassificacaoResource {
	@Autowired
	private Classificacoes classificacoes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CLASSIFICACOES_CONTEUDO')")
	public List<Classificacao> conteudo() {
		return classificacoes.findAll();
	}

}
