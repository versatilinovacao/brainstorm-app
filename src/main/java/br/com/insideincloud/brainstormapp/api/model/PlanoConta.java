package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="planos_contas",schema="contabil")
public class PlanoConta implements Serializable {
	private static final long serialVersionUID = -6076327291322434516L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long planoconta_id;
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name="composicao_id",insertable = true, updatable = true)
	private PlanoConta composicao;
	
	
	@JsonManagedReference
	@OneToMany(mappedBy="composicao")
	private List<PlanoConta> contas;
	
	private Integer codigo;
	private Integer numeral;
	private String descricao;
	private Boolean status;
		
	public PlanoConta getComposicao() {
		return composicao;
	}
	public void setComposicao(PlanoConta composicao) {
		this.composicao = composicao;
	}
	public List<PlanoConta> getContas() {
		return contas;
	}
	public void setContas(List<PlanoConta> contas) {
		this.contas = contas;
	}
	public Long getPlanoconta_id() {
		return planoconta_id;
	}
	public void setPlanoconta_id(Long planoconta_id) {
		this.planoconta_id = planoconta_id;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Integer getNumeral() {
		return numeral;
	}
	public void setNumeral(Integer numeral) {
		this.numeral = numeral;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((planoconta_id == null) ? 0 : planoconta_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanoConta other = (PlanoConta) obj;
		if (planoconta_id == null) {
			if (other.planoconta_id != null)
				return false;
		} else if (!planoconta_id.equals(other.planoconta_id))
			return false;
		return true;
	}
	
}
