package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.model.GradeMateria;
import br.com.insideincloud.brainstormapp.api.repository.GradesMaterias;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeMateriaFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.GradeMateriaDelete;

@RestController
@RequestMapping("/gradesmaterias")
public class GradeMateriaResource {

	@Autowired
	private GradesMaterias grades;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_GRADEMATERIA_CONTEUDO')")
	public Page<GradeMateria> conteudo(@RequestBody GradeMateriaFilter filtro, @PageableDefault Pageable page) {
		return grades.filtrar(filtro, page);
	};
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_GRADEMATERIA_SALVAR')")
	public ResponseEntity<GradeMateria> salvar(@RequestBody GradeMateria grade, HttpServletResponse response) {
		grade = grades.save(grade);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(grade.getGrademateria_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(grade);
	}; 
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_GRADEMATERIA_DELETAR')")
	public Page<GradeMateria> deletar(@RequestBody GradeMateriaDelete deleta, @PageableDefault(size=5) Pageable page, HttpServletResponse response) {
		GradeMateriaFilter filtro = new GradeMateriaFilter();
		
		for (GradeMateria grade : deleta.getGradesMaterias()) {
			grades.delete(grade);
		};
		
		return grades.filtrar(filtro,page);
	};
	
}
