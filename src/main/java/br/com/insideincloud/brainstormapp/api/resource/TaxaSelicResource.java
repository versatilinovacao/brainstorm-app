package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import br.com.insideincloud.brainstormapp.api.model.TaxaSelic;
import br.com.insideincloud.brainstormapp.api.repository.filter.TaxaSelicFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.TaxaSelicService;

@RestController
@RequestMapping("/taxasselic")
public class TaxaSelicResource {
    @Autowired
    private TaxaSelicService taxaService;
    
    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_TAXASELIC_SALVAR')")
    public ResponseEntity<RetornoWrapper<TaxaSelic>> salvar(@RequestBody TaxaSelic taxaSelic, HttpServletResponse response) {
    
        return taxaService.salvar(taxaSelic,response);
    }
    
    @PutMapping
    @PreAuthorize("hasAuthority('ROLE_TAXASELIC_CONTEUDO')")
    public RetornoWrapper<TaxaSelic> conteudo(@RequestBody TaxaSelicFilter filtro, @PageableDefault Pageable page) {
        return taxaService.conteudo(filtro,page);
    }
    
    
}