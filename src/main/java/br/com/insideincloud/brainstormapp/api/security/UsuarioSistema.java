package br.com.insideincloud.brainstormapp.api.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import br.com.insideincloud.brainstormapp.api.model.UsuarioLoginView;

public class UsuarioSistema extends User {
	private static final long serialVersionUID = 1L;
	
	private UsuarioLoginView usuario;
	public UsuarioSistema(UsuarioLoginView usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getEmail(), usuario.getSenha(), authorities);
		System.out.println("USUARIO :"+usuario.getEmail()+" SENHA :"+usuario.getSenha());
		this.usuario = usuario;
	}
	
	public UsuarioLoginView getUsuario() {
		return usuario;
	}
	
}
