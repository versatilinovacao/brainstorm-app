package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="especializacoes_inativas_view")
public class EspecialidadeInativaView implements Serializable {
	private static final long serialVersionUID = -3139978490149731270L;
	
	@Id
	private Long especializacao_id;
	private String descricao;

	public Long getEspecializacao_id() {
		return especializacao_id;
	}
	public void setEspecializacao_id(Long especializacao_id) {
		this.especializacao_id = especializacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((especializacao_id == null) ? 0 : especializacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EspecialidadeInativaView other = (EspecialidadeInativaView) obj;
		if (especializacao_id == null) {
			if (other.especializacao_id != null)
				return false;
		} else if (!especializacao_id.equals(other.especializacao_id))
			return false;
		return true;
	}
	
}
