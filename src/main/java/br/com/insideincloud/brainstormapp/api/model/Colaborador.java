package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.insideincloud.brainstormapp.api.model.minor.PlanoContaMinor;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.view.ResponsavelView;

@Entity
@Table(name="colaborador",schema="public")
public class Colaborador implements Serializable {
	private static final long serialVersionUID = -3508853037659353859L;
	
	@EmbeddedId
	private ColaboradorPK colaborador_id;
//	@ManyToOne
//	@JoinColumns({@JoinColumn(name="colaborador_id",insertable=false,updatable=false),@JoinColumn(name="composicao_id",insertable=false,updatable=false)})
//	private Colaborador composicao;
	@OneToOne
	@JoinColumn(name="empresa_id")
	@Cascade(CascadeType.ALL)
	private Empresa empresa;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="fantasia",length=100)
	private String fantasia;
	@Column(name="numero",length=15)
	private String numero;
	@Column(name="complemento",length=15)
	private String complemento;
	@OneToOne
	@JoinColumn(name="usuario_id")
	private Usuario usuario;
	
//	@Fetch(FetchMode.SELECT)
//	@ManyToMany(fetch=FetchType.EAGER)
//	@JoinTable(name="classificacao_colaborador", joinColumns = @JoinColumn(name="colaborador_id"), inverseJoinColumns = @JoinColumn(name="classificacao_id"))
//	private List<Classificacao> classificacoes;

	@Column(name="referencia",length=10)
	private String referencia;
	
	@OneToOne
	@JoinColumn(name="cep")
	private Logradouro logradouro;
	
	@OneToOne
	@JoinColumn(name="identificador_id")
	@Cascade(CascadeType.ALL)
	private Identificador identificador;
	
	@OneToOne
	@JoinColumn(name="pessoajuridica_id")
	@Cascade(CascadeType.ALL)
	private PessoaJuridica pessoajuridica;
	
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="telefones_colaboradores", joinColumns = { @JoinColumn(name="colaborador_id"),
															   @JoinColumn(name="composicao_colaborador_id") }, 
											   inverseJoinColumns = @JoinColumn(name="telefone_id"))
	private List<Telefone> telefones;
	
	@JsonBackReference
	@Fetch(FetchMode.SELECT)
	@Cascade(CascadeType.SAVE_UPDATE)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="contatos_colaboradores", joinColumns = { @JoinColumn(name="colaborador_id"),
				 											  @JoinColumn(name="composicao_colaborador_id")  }, 
											  inverseJoinColumns = { @JoinColumn(name="contato_id") })
	private List<Contato> contatos;
	
	@JoinColumn(name="responsavel_id")
	@OneToOne
	@JoinColumns({@JoinColumn(name="responsavel_id", insertable=true, updatable=true)})
	private ResponsavelView responsavel;
		
//	private ResponsavelView responsavel;

//	@JsonBackReference
	@Fetch(FetchMode.SELECT)
	@Cascade(CascadeType.ALL)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="responsaveis_colaboradores", joinColumns = { @JoinColumn(name="colaborador_id"),
																  @JoinColumn(name="composicao_colaborador_id")}, 
												  inverseJoinColumns = { @JoinColumn(name="responsavel_id") })
	private List<Responsavel> responsaveis;

	@OneToOne
	@JoinColumn(name="telefone_id")
	private Telefone telefone;	
			
	@Column(name="email", length=250)
	private String email;
	
	@Fetch(FetchMode.SELECT)
	@Cascade(CascadeType.ALL)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="colaboradores_classificacoes",joinColumns = { @JoinColumn(name="colaborador_id"),
																   @JoinColumn(name="composicao_colaborador_id") }, 
												   inverseJoinColumns = @JoinColumn(name="classificacao_id"), schema="configuracao")	
	private List<PillColaborador> classificacao;
	
	@OneToOne
	@JoinColumn(name="cliente_id")
	@Cascade(CascadeType.ALL)
	private Cliente cliente;	
	
	@OneToOne
	@JoinColumn(name="parceiro_id")
	@Cascade(CascadeType.ALL)
	private Parceiro parceiro;
	
	@OneToOne
	@JoinColumn(name="tercerizado_id")
	@Cascade(CascadeType.ALL)
	private Tercerizado tercerizado;
	
	@OneToOne
	@JoinColumn(name="planoconta_id")
	private PlanoContaMinor planoconta;
	
	@Column(name="foto_real")
	private String fotoreal;
	@Column(name="foto_thunbnail")
	private String fotothunbnail;
	
	private boolean status;
	
//	@OneToOne
//	@JoinColumn(name="empresa_id")
//	private Empresa empresa;
	
//	public Empresa getEmpresa() {
//		return empresa;
//	}
//	public void setEmpresa(Empresa empresa) {  23:40  177726
//		this.empresa = empresa;
//	}

	public String getFotoreal() {
		return fotoreal;
	}
	public void setFotoreal(String fotoreal) {
		this.fotoreal = fotoreal;
	}
	public String getFotothunbnail() {
		return fotothunbnail;
	}
	public void setFotothunbnail(String fotothunbnail) {
		this.fotothunbnail = fotothunbnail;
	}
	public PlanoContaMinor getPlanoconta() {
		return planoconta;
	}
	public void setPlanoconta(PlanoContaMinor planoconta) {
		this.planoconta = planoconta;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Tercerizado getTercerizado() {
		return tercerizado;
	}
	public void setTercerizado(Tercerizado tercerizado) {
		this.tercerizado = tercerizado;
	}
	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Parceiro getParceiro() {
		return parceiro;
	}
	public void setParceiro(Parceiro parceiro) {
		this.parceiro = parceiro;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<PillColaborador> getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(List<PillColaborador> classificacao) {
		this.classificacao = classificacao;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public Telefone getTelefone() {
		return telefone;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public List<Responsavel> getResponsaveis() {
		return responsaveis;
	}
	public void setResponsaveis(List<Responsavel> responsaveis) {
		this.responsaveis = responsaveis;
	}
	public ResponsavelView getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(ResponsavelView responsavel) {
		this.responsavel = responsavel;
	}
	public List<Contato> getContatos() {
		return contatos;
	}
	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}
	public List<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
//	public Colaborador getComposicao() {
//		return composicao;
//	}
//	public void setComposicao(Colaborador composicao) {
//		this.composicao = composicao;
//	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}	
//	public List<Classificacao> getClassificacoes() {
//		return classificacoes;
//	}
//	public void setClassificacoes(List<Classificacao> classificacoes) {
//		this.classificacoes = classificacoes;
//	}
	public Logradouro getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(Logradouro logradouro) {
		this.logradouro = logradouro;
	}	
	public Identificador getIdentificador() {
		return identificador;
	}
	public void setIdentificador(Identificador identificador) {
		this.identificador = identificador;
	}
	
	
	public PessoaJuridica getPessoajuridica() {
		return pessoajuridica;
	}
	public void setPessoajuridica(PessoaJuridica pessoajuridica) {
		this.pessoajuridica = pessoajuridica;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colaborador other = (Colaborador) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}

}
