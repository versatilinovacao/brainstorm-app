package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoAtividadeView;

@Repository
public interface AlunosAtividadesView extends JpaRepository<AlunoAtividadeView, Long> {

	@Query("select a from AlunoAtividadeView a where a.aluno_id = ?1")
	public List<AlunoAtividadeView> alunosAtividades(Long codigo);
	
	@Query("select a from AlunoAtividadeView a where a.aluno_id = ?1 and (a.situacaoatividade_id = 4 or a.situacaoatividade_id = 2)")
	public List<AlunoAtividadeView> alunosAtividadesPendentes(Long aluno_id);
	
	@Query("select a from AlunoAtividadeView a where a.entrega < now() and a.situacaoatividade_id != 4")
	public List<AlunoAtividadeView> findByAtividadesPendentes();
}
