package br.com.insideincloud.brainstormapp.api.repository.chamada;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Chamada;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaFilter;

public interface ChamadasQuery {
	public Page<Chamada> filtrar(ChamadaFilter filtro, Pageable page);

}
