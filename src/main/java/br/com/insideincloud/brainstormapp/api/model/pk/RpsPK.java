package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

public class RpsPK implements Serializable {
	private static final long serialVersionUID = -8743666261608707613L;

	private Long rps_id;
	private Long empresa_id;
	private Long filial_id;
	
	public Long getRps_id() {
		return rps_id;
	}
	public void setRps_id(Long rps_id) {
		this.rps_id = rps_id;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Long getFilial_id() {
		return filial_id;
	}
	public void setFilial_id(Long filial_id) {
		this.filial_id = filial_id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		result = prime * result + ((filial_id == null) ? 0 : filial_id.hashCode());
		result = prime * result + ((rps_id == null) ? 0 : rps_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RpsPK other = (RpsPK) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		if (filial_id == null) {
			if (other.filial_id != null)
				return false;
		} else if (!filial_id.equals(other.filial_id))
			return false;
		if (rps_id == null) {
			if (other.rps_id != null)
				return false;
		} else if (!rps_id.equals(other.rps_id))
			return false;
		return true;
	}
	
}
