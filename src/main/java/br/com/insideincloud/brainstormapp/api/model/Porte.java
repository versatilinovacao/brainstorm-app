package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="portes",schema="empresa")
public class Porte implements Serializable {
	private static final long serialVersionUID = -3481753369556764247L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long porte_id;
	private String classificacao;
	private String descricao;
	private BigDecimal de;
	private BigDecimal ate;
	
	public Long getPorte_id() {
		return porte_id;
	}
	public void setPorte_id(Long porte_id) {
		this.porte_id = porte_id;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getDe() {
		return de;
	}
	public void setDe(BigDecimal de) {
		this.de = de;
	}
	public BigDecimal getAte() {
		return ate;
	}
	public void setAte(BigDecimal ate) {
		this.ate = ate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((porte_id == null) ? 0 : porte_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Porte other = (Porte) obj;
		if (porte_id == null) {
			if (other.porte_id != null)
				return false;
		} else if (!porte_id.equals(other.porte_id))
			return false;
		return true;
	}
	
}
