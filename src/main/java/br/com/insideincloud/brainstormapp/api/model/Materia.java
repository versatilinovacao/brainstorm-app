package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="materias")
public class Materia implements Serializable {
	private static final long serialVersionUID = -488841473641943749L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long materia_id;
	@Column(name="descricao",length=100)
	private String descricao;
	@OneToOne
	@JoinColumn(name="tipo_materia_id")
	private TipoMateria tipomateria;
	@ManyToMany
	@JoinTable(name="composicao_materia", schema="escola", joinColumns = @JoinColumn(name="materia_id"), inverseJoinColumns = @JoinColumn(name="materia_composta_id"))
	private List<ObjetivoMateria> objetivos; 	
	@OneToMany
	@JoinColumn(name="materia_id")
	private List<MateriaConteudo> conteudo;
	private Boolean status;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public List<MateriaConteudo> getConteudo() {
		return conteudo;
	}
	public void setConteudo(List<MateriaConteudo> conteudo) {
		this.conteudo = conteudo;
	}
	public List<ObjetivoMateria> getObjetivos() {
		return objetivos;
	}
	public void setObjetivos(List<ObjetivoMateria> objetivos) {
		this.objetivos = objetivos;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public TipoMateria getTipomateria() {
		return tipomateria;
	}
	public void setTipomateria(TipoMateria tipomateria) {
		this.tipomateria = tipomateria;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materia_id == null) ? 0 : materia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Materia other = (Materia) obj;
		if (materia_id == null) {
			if (other.materia_id != null)
				return false;
		} else if (!materia_id.equals(other.materia_id))
			return false;
		return true;
	}
	
	
	
}
