package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularAtivaView;

@Repository
public interface GradesCurricularesAtivasView extends JpaRepository<GradeCurricularAtivaView, Long> {

	@Query("select g from GradeCurricularAtivaView g where g.curso_id = ?1")
	public List<GradeCurricularAtivaView> findByGradePorCurso(Long curso_id);
	
}
