package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="resumo_empresas_view")
public class ColaboradorEmpresaResumoView implements Serializable {
	private static final long serialVersionUID = 3099362696684123320L;

	@Id
	private Long empresa_id;
	@Column(name="composicao_empresa_id")
	private Long composicaoempresa_id;
	private String nome;
	private String fantasia;
	@Column(name="cidade_id")
	private Long municipio;
	private Long empresa_type;
	private Long classificacao_empresa_id;
	private String classificacao;
	
	public Long getClassificacao_empresa_id() {
		return classificacao_empresa_id;
	}
	public void setClassificacao_empresa_id(Long classificacao_empresa_id) {
		this.classificacao_empresa_id = classificacao_empresa_id;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	public Long getEmpresa_type() {
		return empresa_type;
	}
	public void setEmpresa_type(Long empresa_type) {
		this.empresa_type = empresa_type;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public Long getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Long municipio) {
		this.municipio = municipio;
	}
	public Long getComposicaoempresa_id() {
		return composicaoempresa_id;
	}
	public void setComposicaoempresa_id(Long composicaoempresa_id) {
		this.composicaoempresa_id = composicaoempresa_id;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorEmpresaResumoView other = (ColaboradorEmpresaResumoView) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		return true;
	}
	
}
