package br.com.insideincloud.brainstormapp.api.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.UsuarioLoginView;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;
import br.com.insideincloud.brainstormapp.api.repository.UsuariosLoginView;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private UsuariosLoginView usuarios;
	
	@Autowired
	private Usuarios users;
	
	//private Usuarios u;

//	@Autowired
//	private Logins logins;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<UsuarioLoginView> usuarioOptional = usuarios.findByEmail(email);
		UsuarioLoginView usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Oi, temos um problema, seu usuário ou senha esta errado."));
		
//		Login login = new Login();
//		login.setUsuario_id(usuario);
//		logins.save(login);
		
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}
	
	private Collection<? extends GrantedAuthority> getPermissoes(UsuarioLoginView usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		usuario.getPermissoes().forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getNome().toUpperCase())));
		
		return authorities;
	}

}
