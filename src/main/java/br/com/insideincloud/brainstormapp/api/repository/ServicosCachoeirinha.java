package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.servicos.ServicoCachoeirinha;

@Repository
public interface ServicosCachoeirinha extends JpaRepository<ServicoCachoeirinha, Long> {

}
