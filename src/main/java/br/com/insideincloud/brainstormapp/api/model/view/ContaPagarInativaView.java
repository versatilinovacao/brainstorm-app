package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ContaPagarPK;

@Entity
@Table(name="contas_pagar_inativas_view",schema="financeiro")
public class ContaPagarInativaView implements Serializable {
	private static final long serialVersionUID = 8673257700144472035L;
	
	@Id
	private ContaPagarPK contapagar_id;
//	private Long contareceber_id;
//	private Long composicaocontareceber_id;
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao;
	private double total;
	private Long credor_id;
	private Long composicao_credor_id;
	private String nomedevedor;
	private Integer parcela;
	private Boolean status;
	
	public ContaPagarPK getContapagar_id() {
		return contapagar_id;
	}
	public void setContapagar_id(ContaPagarPK contapagar_id) {
		this.contapagar_id = contapagar_id;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Long getCredor_id() {
		return credor_id;
	}
	public void setCredor_id(Long credor_id) {
		this.credor_id = credor_id;
	}
	public Long getComposicao_credor_id() {
		return composicao_credor_id;
	}
	public void setComposicao_credor_id(Long composicao_credor_id) {
		this.composicao_credor_id = composicao_credor_id;
	}
	public String getNomedevedor() {
		return nomedevedor;
	}
	public void setNomedevedor(String nomedevedor) {
		this.nomedevedor = nomedevedor;
	}
	public Integer getParcela() {
		return parcela;
	}
	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contapagar_id == null) ? 0 : contapagar_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaPagarInativaView other = (ContaPagarInativaView) obj;
		if (contapagar_id == null) {
			if (other.contapagar_id != null)
				return false;
		} else if (!contapagar_id.equals(other.contapagar_id))
			return false;
		return true;
	}
	
}
