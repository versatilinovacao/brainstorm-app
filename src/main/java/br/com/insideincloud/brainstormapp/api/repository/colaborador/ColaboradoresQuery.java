package br.com.insideincloud.brainstormapp.api.repository.colaborador;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorFilter;
import br.com.insideincloud.brainstormapp.api.repository.projection.ResumoColaborador;

public interface ColaboradoresQuery {
	public Page<Colaborador> filtrar(ColaboradorFilter filtro, Pageable pageable);
	public Page<ResumoColaborador> resumir(ColaboradorFilter filtro, Pageable pageable);
	
}
