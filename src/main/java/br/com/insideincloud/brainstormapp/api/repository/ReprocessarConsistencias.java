package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.execute.ReprocessarConsistencia;

@Repository
public interface ReprocessarConsistencias extends JpaRepository<ReprocessarConsistencia,Long> {

}
