package br.com.insideincloud.brainstormapp.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.resource.ColaboradorRequest;

@Service
public class ColaboradorService {

	@Autowired
	private ColaboradorRequest colaboradorRequest;
	
	public ColaboradorPK request_Id(ColaboradorPK id) {
		return colaboradorRequest.chavePrimaria(id);
	}
}
