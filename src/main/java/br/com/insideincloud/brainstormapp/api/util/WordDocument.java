package br.com.insideincloud.brainstormapp.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class WordDocument {
	
	public void CriarArquivo( ) {
		FileOutputStream out = null;
		XWPFDocument document = new XWPFDocument();
		
		try {
			lerDocx(new FileInputStream(new File("/var/local/ecosistema/arquivos/tmp/contrato_prosaber.docx"))).forEach(conteudo -> {
//				System.out.println(">>>>>>>>>>>>>>>>>>> "+conteudo);
			});
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			
			out = new FileOutputStream(new File("DocumentoGeradorProgramainhas.docx"));
			
			XWPFParagraph paragrafo1 = document.createParagraph();
			XWPFRun runPaRun1 = paragrafo1.createRun();
			
			runPaRun1.setText("CONTRATO TITULO 1");
			runPaRun1.setBold(true);
			runPaRun1.addBreak(); //adiciona quebra de linha
			runPaRun1.addBreak(); //adiciona quebra de linha
			
			XWPFParagraph paragrafo2 = document.createParagraph();
			XWPFRun runPaRun2 = paragrafo2.createRun();
			runPaRun2.addTab();
			runPaRun2.setText("Olá, este é um paragrafo criado usando mágica.");
			

			document.write(out);
			
		} catch(FileNotFoundException ex) {
			ex.printStackTrace();
			
		} catch(IOException e) {
			e.printStackTrace();
			
		}
	}
	
	public List<String> lerDocx(InputStream inputStream) {
		List<String> listTerms = new ArrayList<>();
		FileOutputStream out = null;
		XWPFDocument document = new XWPFDocument();
		
		try {
			//Crio uma instando do objeto XWPFDocument para armazenar o arquivo que veio
			//Através do stream de entrada do arquivo que foi carregado
			XWPFDocument xwpfDocument = new XWPFDocument(inputStream);
			
			//Crio uma lista dos paragrafos do texto
			List<XWPFParagraph> paragraphs = xwpfDocument.getParagraphs();
			
			//Crio uma lista para armazenar os paragrafos em foma de string;
//			listTerms = new ArrayList<>();
			
			//Extraio os paragrafos para string e adiciono um a um na lista
			paragraphs.forEach(conteudo -> {
				String aux = conteudo.getText().replaceAll("#versatil_nomecontratada", "SOCIEDADE MANTENEDORA PRÓ-SABER LTDA")
											   .replaceAll("#versatil_cnpjcontratada","02.236.296/0001-70");
				conteudo.getRuns().forEach(aux1 -> {
					
					System.out.println(aux1.isBold());
				});
				listTerms.add( aux );
			});

			
			try {
				out = new FileOutputStream(new File("contrato_prosaber.docx"));
				paragraphs.forEach(conteudo -> {
					XWPFParagraph paragrafo = document.createParagraph();
					XWPFRun executeParagrafo = paragrafo.createRun();
					
					executeParagrafo.setText(conteudo.getText()
							.replaceAll("#versatil_nomecontratada", "SOCIEDADE MANTENEDORA PRÓ-SABER LTDA")
							.replaceAll("#versatil_cnpjcontratada","02.236.296/0001-70")
						);

					//					System.out.println("FONTE NOME E SIZE: "+p.getFontName()+" - "+p.getFontSize());
					
					System.out.println("---"+conteudo.getFontAlignment());
					System.out.println(" < "+conteudo.getText());
					executeParagrafo.addTab();
					conteudo.getRuns().forEach( style -> {
						if (style.isBold()) {
							executeParagrafo.setBold(true);
						} else executeParagrafo.setBold(false);
						
						if (style.getFontSize() == -1) {
							executeParagrafo.setFontSize(10);
						} else 
							executeParagrafo.setFontSize(style.getFontSize());		
//						FontCharRange f = new FontCharRange();
						System.out.println("FONT SIZE -> "+executeParagrafo.getFontFamily(null));
						executeParagrafo.setFontFamily(style.getFontFamily());
					});
					
					System.out.println(" >< "+conteudo.getElementType());
					
					if(conteudo.getAlignment() == ParagraphAlignment.CENTER) {
						paragrafo.setAlignment(ParagraphAlignment.CENTER);
					} else if (conteudo.getAlignment() == ParagraphAlignment.LEFT) {
						paragrafo.setAlignment(ParagraphAlignment.LEFT);
					} else if (conteudo.getAlignment() == ParagraphAlignment.DISTRIBUTE) {
						paragrafo.setAlignment(ParagraphAlignment.DISTRIBUTE);	
					} else if (conteudo.getAlignment() == ParagraphAlignment.BOTH) {
						paragrafo.setAlignment(ParagraphAlignment.BOTH);	
					} else if (conteudo.getAlignment() == ParagraphAlignment.MEDIUM_KASHIDA) {
						paragrafo.setAlignment(ParagraphAlignment.MEDIUM_KASHIDA);
					} else if (conteudo.getAlignment() == ParagraphAlignment.HIGH_KASHIDA) {
						paragrafo.setAlignment(ParagraphAlignment.HIGH_KASHIDA);
					} else if (conteudo.getAlignment() == ParagraphAlignment.LOW_KASHIDA) {
						paragrafo.setAlignment(ParagraphAlignment.LOW_KASHIDA);
					} else {
						paragrafo.setAlignment(ParagraphAlignment.RIGHT);
					}
					
					
					
				});
				document.write(out);
			} catch(FileNotFoundException e) {}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return listTerms;
	}

}
