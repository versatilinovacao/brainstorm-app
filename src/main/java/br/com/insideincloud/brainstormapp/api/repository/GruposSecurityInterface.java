package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.GrupoSecurityInterface;

@Repository
public interface GruposSecurityInterface extends JpaRepository<GrupoSecurityInterface, Long> {
	@Query("select g from GrupoSecurityInterface g where g.gruposecurity_id = ?1")
	public List<GrupoSecurityInterface> findByGruposecurity_id(Long gruposecurity_id); 

}
