package br.com.insideincloud.brainstormapp.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Aluno;
import br.com.insideincloud.brainstormapp.api.model.Atividade;
import br.com.insideincloud.brainstormapp.api.model.AtividadeUpload;
import br.com.insideincloud.brainstormapp.api.model.GradeCurricular;
import br.com.insideincloud.brainstormapp.api.model.Turma;
import br.com.insideincloud.brainstormapp.api.repository.Atividades;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesUpload;
import br.com.insideincloud.brainstormapp.api.repository.GradesCurriculares;
import br.com.insideincloud.brainstormapp.api.repository.Turmas;
import br.com.insideincloud.brainstormapp.api.repository.Usuarios;

@Service
public class AtividadeService {

	@Autowired
	private Atividades atividades;
	
	@Autowired
	private GradesCurriculares grades;
	
	@Autowired
	private Turmas turmas;
	
	@Autowired
	private AtividadesUpload atividadesUpload;
	
	@Autowired
	private Usuarios usuario;
	
	public List<AtividadeUpload> conteudoUpload(AtividadeUpload atividade) {
		
		atividade = atividadesUpload.saveAndFlush(atividade);
		
		return atividadesUpload.findAllAtividades(atividade.getAtividade().getAtividade_id());
	}
	
	public Atividade salvarAtividade(Atividade atividade) {
		
		if (atividade.getGradecurricular() != null) {
			GradeCurricular grade = grades.findOne(atividade.getGradecurricular().getGradecurricular_id());
			if (grade.getTurma() != null) {
				Turma turma = turmas.findOne(grade.getTurma().getTurma_id());
				if (turma != null) {
					List<Aluno> alunos = new ArrayList<>();
					turma.getAlunos().forEach( conteudo -> {
						alunos.add(conteudo);
					} );
					
					atividade.setAlunos(alunos);
					
				}
			}
			
		}
		
		return atividades.saveAndFlush(atividade);
	}
	
}
