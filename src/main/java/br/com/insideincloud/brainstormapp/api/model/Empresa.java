package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="empresa",schema="empresa")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 7108327655283800409L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long empresa_id;
	@OneToOne
	@JoinColumn(name="classificacao_empresa_id")
	private ClassificacaoEmpresa classificacao;
	
	@OneToOne
	@JoinColumn(name="tipo_empresa_id")
	private TipoEmpresa tipoempresa;
	
	@OneToOne
	@JoinColumn(name="formato_caderno_id")
	private FormatoCaderno formatocaderno;
	private BigDecimal criterio;
	@OneToOne
	@JoinColumn(name="unidade_id")
	private UnidadeEmpresa unidade;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="financeira_id"),@JoinColumn(name="composicaofinanceira_id")})
	private Colaborador financeira;
	
	private String smtp;
	private Integer porta_smtp;
	@OneToOne
	@JoinColumn(name="criptografia_id")
	private TipoCriptografia criptografia;
	private Boolean autenticado;
	private String login;
	private String senha;
	@OneToOne
	@JoinColumn(name="configuracaocontrato_id")
	private ConfiguracaoContrato configuracaocontrato;
	@Column(name="relatorio")
	private String relatoriopath;
	private String parecer;
	
	@OneToOne
	@JoinColumn(name="contratomodelo_id")
	private ContratoModelo contratomodelo;
	
	public ContratoModelo getContratomodelo() {
		return contratomodelo;
	}
	public void setContratomodelo(ContratoModelo contratomodelo) {
		this.contratomodelo = contratomodelo;
	}
	public String getParecer() {
		return parecer;
	}
	public void setParecer(String parecer) {
		this.parecer = parecer;
	}
	public String getRelatoriopath() {
		return relatoriopath;
	}
	public void setRelatoriopath(String relatoriopath) {
		this.relatoriopath = relatoriopath;
	}
	public ConfiguracaoContrato getConfiguracaocontrato() {
		return configuracaocontrato;
	}
	public void setConfiguracaocontrato(ConfiguracaoContrato configuracaocontrato) {
		this.configuracaocontrato = configuracaocontrato;
	}
	public String getSmtp() {
		return smtp;
	}
	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}
	public Integer getPorta_smtp() {
		return porta_smtp;
	}
	public void setPorta_smtp(Integer porta_smtp) {
		this.porta_smtp = porta_smtp;
	}
	public TipoCriptografia getCriptografia() {
		return criptografia;
	}
	public void setCriptografia(TipoCriptografia criptografia) {
		this.criptografia = criptografia;
	}
	public Boolean getAutenticado() {
		return autenticado;
	}
	public void setAutenticado(Boolean autenticado) {
		this.autenticado = autenticado;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Colaborador getFinanceira() {
		return financeira;
	}
	public void setFinanceira(Colaborador financeira) {
		this.financeira = financeira;
	}
	public UnidadeEmpresa getUnidade() {
		return unidade;
	}
	public void setUnidade(UnidadeEmpresa unidade) {
		this.unidade = unidade;
	}
	public BigDecimal getCriterio() {
		return criterio;
	}
	public void setCriterio(BigDecimal criterio) {
		this.criterio = criterio;
	}
	public FormatoCaderno getFormatocaderno() {
		return formatocaderno;
	}
	public void setFormatocaderno(FormatoCaderno formatocaderno) {
		this.formatocaderno = formatocaderno;
	}	
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public ClassificacaoEmpresa getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(ClassificacaoEmpresa classificacao) {
		this.classificacao = classificacao;
	}
	
	
	public TipoEmpresa getTipoempresa() {
		return tipoempresa;
	}
	public void setTipoempresa(TipoEmpresa tipoempresa) {
		this.tipoempresa = tipoempresa;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		return true;
	}
	
	
	
}
