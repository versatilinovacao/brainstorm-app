package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ContratoPK implements Serializable {
	private static final long serialVersionUID = -589147502221947092L;
	
	private Long contrato_id;
	private Long composicao_id;
	
	public Long getContrato_id() {
		return contrato_id;
	}
	public void setContrato_id(Long contrato_id) {
		this.contrato_id = contrato_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	
}
