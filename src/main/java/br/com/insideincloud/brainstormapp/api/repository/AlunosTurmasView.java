package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoTurmaView;
import br.com.insideincloud.brainstormapp.api.repository.alunoturmaview.AlunosTurmasViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoTurmaViewFilter;

@Repository
public interface AlunosTurmasView extends JpaRepository<AlunoTurmaView,Long>, AlunosTurmasViewQuery {
	public Page<AlunoTurmaView> filtrar(AlunoTurmaViewFilter filtro, Pageable page);
	
	@Query("select a from AlunoTurmaView a where a.vinculo = ?1")
	public List<AlunoTurmaView> findByAlunosDaTurma(Long turma_id);
}
