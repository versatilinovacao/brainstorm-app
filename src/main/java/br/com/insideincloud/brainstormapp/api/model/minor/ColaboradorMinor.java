package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="colaborador_view")
public class ColaboradorMinor implements Serializable {

	private static final long serialVersionUID = -1992704211752178621L;

	@EmbeddedId
	private ColaboradorPK colaborador_id;
	@Column(name="id")
	private Long colaboradorminor_id;
	private Long composicao;
	private String nome;
	private String fantasia;
	private String referencia;
	private String cep;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String siglaestado;
	private String paissigla;
	private String email;
	private String foto_thumbnail;
	private String telefone;
	private Boolean status;

	public ColaboradorPK getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(ColaboradorPK colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getColaboradorminor_id() {
		return colaboradorminor_id;
	}
	public void setColaboradorminor_id(Long colaboradorminor_id) {
		this.colaboradorminor_id = colaboradorminor_id;
	}
	public Long getComposicao() {
		return composicao;
	}
	public void setComposicao(Long composicao) {
		this.composicao = composicao;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getSiglaestado() {
		return siglaestado;
	}
	public void setSiglaestado(String siglaestado) {
		this.siglaestado = siglaestado;
	}
	public String getPaissigla() {
		return paissigla;
	}
	public void setPaissigla(String paissigla) {
		this.paissigla = paissigla;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFoto_thumbnail() {
		return foto_thumbnail;
	}
	public void setFoto_thumbnail(String foto_thumbnail) {
		this.foto_thumbnail = foto_thumbnail;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colaborador_id == null) ? 0 : colaborador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorMinor other = (ColaboradorMinor) obj;
		if (colaborador_id == null) {
			if (other.colaborador_id != null)
				return false;
		} else if (!colaborador_id.equals(other.colaborador_id))
			return false;
		return true;
	}
		
}
