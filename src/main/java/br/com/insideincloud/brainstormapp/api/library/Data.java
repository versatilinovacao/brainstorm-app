package br.com.insideincloud.brainstormapp.api.library;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

import br.com.insideincloud.brainstormapp.api.resource.config.Semana;

public class Data {
	
	public static int DiaSemana(LocalDate data) {
		
		if ( data.getDayOfWeek().equals(DayOfWeek.SUNDAY) ) { return 1; } 
		else if (data.getDayOfWeek().equals(DayOfWeek.MONDAY)) { return 2; }
		else if (data.getDayOfWeek().equals(DayOfWeek.TUESDAY)) { return 3; }
		else if (data.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) { return 4; }
		else if (data.getDayOfWeek().equals(DayOfWeek.THURSDAY)) { return 5; }
		else if (data.getDayOfWeek().equals(DayOfWeek.FRIDAY)) { return 6; }
		else { return 7; }
	}
	
	public static void validSaturday(LocalDate sabado) {
		
		if (sabado.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
			sabado.minusDays(1);
		} else if ( sabado.getDayOfWeek().equals(DayOfWeek.FRIDAY) ) {
			sabado.plusDays(1);
		}
		
	}
	
	public static boolean isSunday(LocalDate dia) { return  dia.getDayOfWeek().equals(DayOfWeek.SUNDAY); }
	public static boolean mondayOne(LocalDate dia) { return  dia.getDayOfWeek().equals(DayOfWeek.MONDAY); }
	
	public static LocalDate StartWeek(LocalDate evento) {
		
		if ((evento.getDayOfWeek().equals(DayOfWeek.SUNDAY)) && (evento.getDayOfMonth() == 1)) { 
			return evento; 
		} else {
			if (evento.getDayOfWeek().equals(DayOfWeek.SUNDAY)) { return evento; }
		}
				
		return evento;
	}
	
	public static LocalDate FinalyWeek(LocalDate evento,List<Semana> startWeek) {

//		if (conteudo.plusDays( plus ).getDayOfMonth() > conteudo.lengthOfMonth()) { plus = conteudo.lengthOfMonth() - conteudo.getDayOfMonth(); }

		if (startWeek.size() > 0) {
			if (startWeek.get(0).getFirst().getDayOfWeek().equals( DayOfWeek.SUNDAY )) {
				System.out.println("111111111111111111111111111111111");
				if (evento.plusDays(7).getDayOfMonth() > evento.lengthOfMonth()) {
					return evento.plusDays( evento.lengthOfMonth() - evento.getDayOfMonth() );
				} else {
					return evento.plusDays(7);					
				}
			} else {
				System.out.println("333333333333333333333333333333333");
				if (evento.plusDays(7).getDayOfMonth() > evento.lengthOfMonth()) {
					return evento.plusDays( evento.lengthOfMonth() - evento.getDayOfMonth() );
				} else {
					if (evento.getDayOfWeek().equals(DayOfWeek.SUNDAY)) { return evento.plusDays(6); }
					if (evento.getDayOfWeek().equals(DayOfWeek.MONDAY)) { return evento.plusDays(5); }
					if (evento.getDayOfWeek().equals(DayOfWeek.TUESDAY)) { return evento.plusDays(4); }
					if (evento.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) { return evento.plusDays(3); }
					if (evento.getDayOfWeek().equals(DayOfWeek.THURSDAY)) { return evento.plusDays(2); }
					if (evento.getDayOfWeek().equals(DayOfWeek.FRIDAY)) { return evento.plusDays(1); }
					if (evento.getDayOfWeek().equals(DayOfWeek.SATURDAY)) { return evento; }
				}
	
			}		
			
		} else {
			System.out.println("22222222222222222222222");
			if (evento.getDayOfWeek().equals(DayOfWeek.SUNDAY)) { return evento.plusDays(7); }
			if (evento.getDayOfWeek().equals(DayOfWeek.MONDAY)) { return evento.plusDays(6); }
			if (evento.getDayOfWeek().equals(DayOfWeek.TUESDAY)) { return evento.plusDays(5); }
			if (evento.getDayOfWeek().equals(DayOfWeek.WEDNESDAY)) { return evento.plusDays(4); }
			if (evento.getDayOfWeek().equals(DayOfWeek.THURSDAY)) { return evento.plusDays(3); }
			if (evento.getDayOfWeek().equals(DayOfWeek.FRIDAY)) { return evento.plusDays(2); }
			if (evento.getDayOfWeek().equals(DayOfWeek.SATURDAY)) { return evento; }
		}
		
		return evento;
	}

}
