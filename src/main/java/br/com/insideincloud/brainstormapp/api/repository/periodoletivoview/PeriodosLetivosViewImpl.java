package br.com.insideincloud.brainstormapp.api.repository.periodoletivoview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoViewFilter;

public class PeriodosLetivosViewImpl implements PeriodosLetivosViewQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<PeriodoLetivoView> filtrar(PeriodoLetivoViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<PeriodoLetivoView> criteria = builder.createQuery(PeriodoLetivoView.class);
		Root<PeriodoLetivoView> root = criteria.from(PeriodoLetivoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<PeriodoLetivoView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(PeriodoLetivoViewFilter filtro, CriteriaBuilder builder, Root<PeriodoLetivoView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getPeriodo_letivo_id())) {
				predicates.add(builder.equal(root.get("periodo_letivo_id"), Long.parseLong(filtro.getPeriodo_letivo_id())));
			}		
			
			if (!StringUtils.isEmpty(filtro.getEmpresa_id())) {
				predicates.add(builder.equal(root.get("empresa_id"), Long.parseLong(filtro.getEmpresa_id())));
			}
			
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}
	
			if (!StringUtils.isEmpty(filtro.getAno())) {
				predicates.add(builder.equal(root.get("ano"), filtro.getAno()));
			}
			
			if (!StringUtils.isEmpty(filtro.getStatus())) {
				if (filtro.getStatus().toUpperCase() == "TRUE") {
					predicates.add(builder.equal(builder.isTrue(root.get("status")), true ));
				}
				
				if (filtro.getStatus().toUpperCase() == "FALSE") {
					predicates.add(builder.equal(builder.isFalse(root.get("status")), false ));
				}
			
			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(PeriodoLetivoViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<PeriodoLetivoView> root = criteria.from(PeriodoLetivoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
