package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ColaboradorFilter {

	private String aowner;
	private String colaborador_id;
	private String nome;
	private String fantasia;
	private String notcontatos;
	private String referencia;

	
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getAowner() {
		return aowner;
	}
	public void setAowner(String aowner) {
		this.aowner = aowner;
	}
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	public String getNotcontatos() {
		return notcontatos;
	}
	public void setNotcontatos(String notcontatos) {
		this.notcontatos = notcontatos;
	}
	
}
