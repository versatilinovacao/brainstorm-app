package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_empresa",schema="empresa")
public class TipoEmpresa implements Serializable {
	private static final long serialVersionUID = 3268254984425907703L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_empresa_id;
	private String classificacao;
	private String descricao;
	
	public Long getTipo_empresa_id() {
		return tipo_empresa_id;
	}
	public void setTipo_empresa_id(Long tipo_empresa_id) {
		this.tipo_empresa_id = tipo_empresa_id;
	}
	public String getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_empresa_id == null) ? 0 : tipo_empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoEmpresa other = (TipoEmpresa) obj;
		if (tipo_empresa_id == null) {
			if (other.tipo_empresa_id != null)
				return false;
		} else if (!tipo_empresa_id.equals(other.tipo_empresa_id))
			return false;
		return true;
	}
	
	
}
