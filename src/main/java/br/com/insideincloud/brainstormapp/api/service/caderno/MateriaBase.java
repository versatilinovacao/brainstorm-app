package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class MateriaBase {
	private Long materia_id;
	private String nome;
	private List<ConteudoBase> conteudos;
	
	public MateriaBase() {}
	
	public MateriaBase(Long materia_id, String nome) {
		this.materia_id = materia_id;
		this.nome = nome;
		
	}

	public List<ConteudoBase> getConteudos() {
		return conteudos;
	}

	public void setConteudos(List<ConteudoBase> conteudos) {
		this.conteudos = conteudos;
	}

	public Long getMateria_id() {
		return materia_id;
	}

	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
