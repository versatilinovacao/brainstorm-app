package br.com.insideincloud.brainstormapp.api.repository.filter;

public class PeriodoLetivoViewFilter {
	private String periodo_letivo_id;
	private String composicaoempresa_id;
	private String descricao;
	private String ano;
	private String status;
	private String empresa_id;
	
	public String getComposicaoempresa_id() {
		return composicaoempresa_id;
	}
	public void setComposicaoempresa_id(String composicaoempresa_id) {
		this.composicaoempresa_id = composicaoempresa_id;
	}
	public String getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(String empresa_id) {
		this.empresa_id = empresa_id;
	}
	public String getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(String periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
