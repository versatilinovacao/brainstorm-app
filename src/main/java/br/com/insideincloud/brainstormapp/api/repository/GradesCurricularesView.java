package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularView;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeCurricularViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.gradecurricularview.GradesCurricularesViewQuery;

@Repository
public interface GradesCurricularesView extends JpaRepository<GradeCurricularView,Long>,GradesCurricularesViewQuery {
	public Page<GradeCurricularView> filtrar(GradeCurricularViewFilter filtro, Pageable page);
}
