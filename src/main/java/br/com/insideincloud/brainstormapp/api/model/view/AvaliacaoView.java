package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="avaliacoes_view",schema="escola")
public class AvaliacaoView implements Serializable {
	private static final long serialVersionUID = -7970861905369128217L;

	@Id
	private Long avaliacao_id;
	private Long periodo_letivo_item_id;
	private Long caderno_id;
	private Long aluno_id;
	@Column(name="composicao_aluno_id")
	private Long composicaoaluno_id;
	private String nome_aluno;
	private String periodo_letivo_nome;
	private BigDecimal faltas;
	private BigDecimal frequencia;
	private BigDecimal aproveitamento;
	private String conceito;
	private BigDecimal nota;
	private String avaliacao;
	
	public Long getComposicaoaluno_id() {
		return composicaoaluno_id;
	}
	public void setComposicaoaluno_id(Long composicaoaluno_id) {
		this.composicaoaluno_id = composicaoaluno_id;
	}
	public BigDecimal getFaltas() {
		return faltas;
	}
	public void setFaltas(BigDecimal faltas) {
		this.faltas = faltas;
	}
	public BigDecimal getFrequencia() {
		return frequencia;
	}
	public void setFrequencia(BigDecimal frequencia) {
		this.frequencia = frequencia;
	}
	public Long getAvaliacao_id() {
		return avaliacao_id;
	}
	public void setAvaliacao_id(Long avaliacao_id) {
		this.avaliacao_id = avaliacao_id;
	}
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public String getPeriodo_letivo_nome() {
		return periodo_letivo_nome;
	}
	public void setPeriodo_letivo_nome(String periodo_letivo_nome) {
		this.periodo_letivo_nome = periodo_letivo_nome;
	}
	public BigDecimal getAproveitamento() {
		return aproveitamento;
	}
	public void setAproveitamento(BigDecimal aproveitamento) {
		this.aproveitamento = aproveitamento;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avaliacao_id == null) ? 0 : avaliacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoView other = (AvaliacaoView) obj;
		if (avaliacao_id == null) {
			if (other.avaliacao_id != null)
				return false;
		} else if (!avaliacao_id.equals(other.avaliacao_id))
			return false;
		return true;
	}
	
	
}
