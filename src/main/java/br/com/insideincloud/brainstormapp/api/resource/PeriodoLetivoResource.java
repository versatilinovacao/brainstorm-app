package br.com.insideincloud.brainstormapp.api.resource;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Empresa;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivo;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoView;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoInativoView;
import br.com.insideincloud.brainstormapp.api.repository.Colaboradores;
import br.com.insideincloud.brainstormapp.api.repository.Empresas;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivos;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosItens;
import br.com.insideincloud.brainstormapp.api.repository.PeriodosLetivosView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoItemFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.detail.PeriodoLetivoDetail;
import br.com.insideincloud.brainstormapp.api.resource.detail.PeriodoLetivoItemDetail;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/periodosletivos")
public class PeriodoLetivoResource {
	
	@Autowired
	private PeriodosLetivos periodosletivos;
	
	@Autowired
	private PeriodosLetivosItens periodosletivositens;
	
	@Autowired
	private PeriodosLetivosView periodosletivosview;

	@Autowired
	private Empresas empresas;
	
	@Autowired
	private PeriodosLetivosAtivosView periodosLetivosAtivos;
	
	@Autowired
	private PeriodosLetivosInativosView periodosLetivosInativos;
	
	@Autowired
	private Colaboradores colaboradores;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoAtivoView> conteudo(@RequestBody PeriodoLetivoFilter filtro) {
		RetornoWrapper<PeriodoLetivoAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		//periodosletivos.filtrar(filtro, page)
		try {
			retorno.setConteudo(periodosLetivosAtivos.findByPeriodosLetivosAtivos( Long.parseLong(filtro.getEmpresa_id()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("As informações de períodos letivos não podem ser retornadas agora, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoInativoView> conteudoInativos(@RequestBody PeriodoLetivoFilter filtro) {
		RetornoWrapper<PeriodoLetivoInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		//periodosletivos.filtrar(filtro, page)
		try {
			retorno.setConteudo(periodosLetivosInativos.findByPeriodosLetivosInativos( Long.parseLong(filtro.getEmpresa_id()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("As informações de períodos letivos não podem ser retornadas agora, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@PutMapping("/itens")
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoItem> conteudoItem(@RequestBody PeriodoLetivoItemFilter filtro) {
		RetornoWrapper<PeriodoLetivoItem> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		periodosletivositens.filtrar(filtro, page)
		try {
			retorno.setConteudo(periodosletivositens.findByPeriodosLetivosItens( Long.parseLong(filtro.getPeriodoletivo_id()) ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os itens do período letivo, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		 
		return retorno;
	}

	@PutMapping("/view")
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoView> conteudoView(@RequestBody PeriodoLetivoViewFilter filtro) {
		RetornoWrapper<PeriodoLetivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
//		periodosletivosview.filtrar(filtro, page)
		try {
			ColaboradorPK id = new ColaboradorPK();
			id.setColaborador_id( Long.parseLong(filtro.getEmpresa_id()) );
			id.setComposicao_id( Long.parseLong(filtro.getComposicaoempresa_id()) );
			Long empresa_id = colaboradores.findOne(id).getEmpresa().getEmpresa_id();
			retorno.setConteudo(periodosletivosview.findByPeriodoLetivoView( empresa_id ));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornas as informações solicitadas, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
//	@PostMapping
//	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_SALVAR')")
//	public ResponseEntity<PeriodoLetivo> salvar(@RequestBody PeriodoLetivo periodoletivo, HttpServletResponse response) {
//		
//		periodoletivo = periodosletivos.saveAndFlush(periodoletivo);
//
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(periodoletivo.getPeriodo_letivo_id()).toUri();
//		response.setHeader("Location", uri.toASCIIString());
//		
//		return ResponseEntity.created(uri).body(periodoletivo);
//		
//	};
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivo> buscaPeriodoLetivoPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<PeriodoLetivo> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( periodosletivos.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar o período letivo, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
			
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_SALVAR')")
	public RetornoWrapper<PeriodoLetivoItem> salvar(@RequestBody PeriodoLetivoDetail periodoletivodetail, HttpServletResponse response, @PageableDefault(size=5) Pageable page) {
		RetornoWrapper<PeriodoLetivoItem> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		PeriodoLetivo periodo = new PeriodoLetivo();
		
		if (periodoletivodetail.getPeriodo_letivo_id() != null) {
			periodo.setPeriodo_letivo_id(periodoletivodetail.getPeriodo_letivo_id());
		}
		
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<< " + periodoletivodetail.getEmpresa_id() + " >>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		Empresa emp  = empresas.findOne(periodoletivodetail.getEmpresa_id());
		
		periodo.setDescricao(periodoletivodetail.getDescricao());
		periodo.setAno(periodoletivodetail.getAno());
		periodo.setStatus(periodoletivodetail.isStatus());
		periodo.setEmpresa(emp);
		
		periodo = periodosletivos.saveAndFlush(periodo);
		
		if (periodoletivodetail.getPeriodoletivoitens() != null) {
			for (PeriodoLetivoItemDetail periodoletivoitem : periodoletivodetail.getPeriodoletivoitens() ) {
				PeriodoLetivoItem item = new PeriodoLetivoItem();
				
				if (periodoletivoitem.getPeriodo_letivo_item_id() != null) {
					item.setPeriodo_letivo_item_id(periodoletivoitem.getPeriodo_letivo_item_id());
				}
				
				item.setDescricao(periodoletivoitem.getDescricao());
				item.setPeriodo_inicial(periodoletivoitem.getPeriodo_inicial());
				item.setPeriodo_final(periodoletivoitem.getPeriodo_final());
				item.setPeriodoletivo(periodo);
				
				periodosletivositens.save(item);
			}
			
		}
		
		PeriodoLetivoItemFilter filtro = new PeriodoLetivoItemFilter();
		filtro.setPeriodoletivo_id(periodo.getPeriodo_letivo_id().toString());
		
		
//		periodosletivositens.filtrar(filtro, page)
		retorno.setConteudo(periodosletivositens.findByPeriodosLetivosItens( Long.parseLong( filtro.getPeriodoletivo_id() ) ));
		
		return retorno;
	}
	
	@GetMapping("/itens/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_CONTEUDO')")
	public RetornoWrapper<PeriodoLetivoItem> buscarPeriodoLetivoItemPorCodigo(@PathVariable Long codigo) {
		RetornoWrapper<PeriodoLetivoItem> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( periodosletivositens.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar o item do período letivo, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
			
		}
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_PERIODO_LETIVO_DELETAR')")
	public Page<PeriodoLetivoItem> deletar() {
		
		return null;
	}
	
}
