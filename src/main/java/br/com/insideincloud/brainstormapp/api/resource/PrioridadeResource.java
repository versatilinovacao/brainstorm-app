package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.Prioridade;
import br.com.insideincloud.brainstormapp.api.repository.Prioridades;

@RestController
@RequestMapping("/prioridades")
public class PrioridadeResource {
	
	@Autowired
	private Prioridades prioridades;
	
	@GetMapping
	public List<Prioridade> conteudo() {
		
		return prioridades.findAll();
	}
}
