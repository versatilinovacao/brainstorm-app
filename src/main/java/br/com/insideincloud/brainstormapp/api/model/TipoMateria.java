package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tiposmaterias")
public class TipoMateria implements Serializable {
	private static final long serialVersionUID = 7699756716022749219L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_materia_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	public Long getTipo_materia_id() {
		return tipo_materia_id;
	}
	public void setTipo_materia_id(Long tipo_materia_id) {
		this.tipo_materia_id = tipo_materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_materia_id == null) ? 0 : tipo_materia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoMateria other = (TipoMateria) obj;
		if (tipo_materia_id == null) {
			if (other.tipo_materia_id != null)
				return false;
		} else if (!tipo_materia_id.equals(other.tipo_materia_id))
			return false;
		return true;
	}	
	
}
