package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="pill_colaborador")
public class PillColaborador implements Serializable {
	private static final long serialVersionUID = -7369641531562486430L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long pill_colaborador_id;
	private String label;
		
	public Long getPill_colaborador_id() {
		return pill_colaborador_id;
	}
	public void setPill_colaborador_id(Long pill_colaborador_id) {
		this.pill_colaborador_id = pill_colaborador_id;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

}
