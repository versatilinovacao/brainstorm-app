package br.com.insideincloud.brainstormapp.api.model.servicos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="servicos",schema="servico")
public class ServicoCachoeirinha implements Serializable {
	private static final long serialVersionUID = -768834160058804040L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long servico_id;
	private Double referencia;
	private String descricao;
	private Double aliquota;
	private Long municipio;
	private Boolean status;
	
	public Long getServico_id() {
		return servico_id;
	}
	public void setServico_id(Long servico_id) {
		this.servico_id = servico_id;
	}
	public Double getReferencia() {
		return referencia;
	}
	public void setReferencia(Double referencia) {
		this.referencia = referencia;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAliquota() {
		return aliquota;
	}
	public void setAliquota(Double aliquota) {
		this.aliquota = aliquota;
	}
	public Long getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Long municipio) {
		this.municipio = municipio;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((servico_id == null) ? 0 : servico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServicoCachoeirinha other = (ServicoCachoeirinha) obj;
		if (servico_id == null) {
			if (other.servico_id != null)
				return false;
		} else if (!servico_id.equals(other.servico_id))
			return false;
		return true;
	}

}
