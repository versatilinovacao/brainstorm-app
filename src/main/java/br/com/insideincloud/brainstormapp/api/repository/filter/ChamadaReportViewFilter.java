package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadaReportViewFilter {
	private String chamada_id;
	private String caderno_id;
	private String aluno_id;
	private String mes;
	private String ano;
	
	public String getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(String chamada_id) {
		this.chamada_id = chamada_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		if (caderno_id == null) { 
			this.caderno_id = ""; 
		};
		this.caderno_id = caderno_id;
	}
	public String getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(String aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		if (ano == null) { this.ano = ""; }
		this.ano = ano;
	}
	
}
