package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="contas_pagar",schema="financeiro")
public class ContaPagar implements Serializable {
	private static final long serialVersionUID = -2215929192701128459L;
	
	@Id
	private Long contapagar_id;
	
	@ManyToOne
	@JoinColumn(name="composicao_id",insertable = true, updatable = false)
	@JsonBackReference
	private ContaPagar composicao;

	@OneToMany(mappedBy="composicao")
	@JsonManagedReference
	private List<ContaPagar> contas;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="empresa_id"),@JoinColumn(name="composicao_empresa_id")})
	private Colaborador empresa;
	
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao; //Data que realmente entrou o dinheiro.

	private double desconto;
	private double valorpago;
	private double saldo;
	private double total;
	
	@OneToOne
	@JoinColumn(name="tipopagamento_id")
	private TipoConta tipo;
	
	@OneToOne
	@JoinColumn(name="negociacao_id")
	private Negociacao negociacao; //Identifica o tipo de pagamento combinado (Dinheiro, Cartão, Boleto, Promissória, Cheque,
	@OneToOne
	@JoinColumn(name="formapagamento_id")
	private FormaPagamento formapagamento; //Identifica o tipo de pagamento utilizado (Dinheiro, Cartao, Boleto, Promissória, Cheque, 
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="devedor_id"),@JoinColumn(name="composicao_devedor_id")})
	private Colaborador devedor;
	
	private Integer parcela; //Identifica o número da parcela.

	private Boolean protesto; //Identifica se este cobrança esta em protesto.
	
	private Boolean status;

	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getContapagar_id() {
		return contapagar_id;
	}

	public void setContapagar_id(Long contapagar_id) {
		this.contapagar_id = contapagar_id;
	}

	public ContaPagar getComposicao() {
		return composicao;
	}

	public void setComposicao(ContaPagar composicao) {
		this.composicao = composicao;
	}

	public List<ContaPagar> getContas() {
		return contas;
	}

	public void setContas(List<ContaPagar> contas) {
		this.contas = contas;
	}

	public Colaborador getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Colaborador empresa) {
		this.empresa = empresa;
	}

	public LocalDate getVencimento() {
		return vencimento;
	}

	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}

	public LocalDate getEmissao() {
		return emissao;
	}

	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}

	public LocalDate getPagamento() {
		return pagamento;
	}

	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}

	public LocalDate getQuitacao() {
		return quitacao;
	}

	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}

	public double getValorpago() {
		return valorpago;
	}

	public void setValorpago(double valorpago) {
		this.valorpago = valorpago;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public TipoConta getTipo() {
		return tipo;
	}

	public void setTipo(TipoConta tipo) {
		this.tipo = tipo;
	}

	public Negociacao getNegociacao() {
		return negociacao;
	}

	public void setNegociacao(Negociacao negociacao) {
		this.negociacao = negociacao;
	}

	public FormaPagamento getFormapagamento() {
		return formapagamento;
	}

	public void setFormapagamento(FormaPagamento formapagamento) {
		this.formapagamento = formapagamento;
	}

	public Colaborador getDevedor() {
		return devedor;
	}

	public void setDevedor(Colaborador devedor) {
		this.devedor = devedor;
	}

	public Integer getParcela() {
		return parcela;
	}

	public void setParcela(Integer parcela) {
		this.parcela = parcela;
	}

	public Boolean getProtesto() {
		return protesto;
	}

	public void setProtesto(Boolean protesto) {
		this.protesto = protesto;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contapagar_id == null) ? 0 : contapagar_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaPagar other = (ContaPagar) obj;
		if (contapagar_id == null) {
			if (other.contapagar_id != null)
				return false;
		} else if (!contapagar_id.equals(other.contapagar_id))
			return false;
		return true;
	}

}
