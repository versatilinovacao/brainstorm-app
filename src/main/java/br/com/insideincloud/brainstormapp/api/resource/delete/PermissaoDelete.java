package br.com.insideincloud.brainstormapp.api.resource.delete;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.Permissao;

public class PermissaoDelete {
	private List<Permissao> permissoes;

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	
}
