package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.service.CodeGeneratorService;

@RestController
@RequestMapping("/generator")
public class CodeGeneratorResource {

	@Autowired
	private CodeGeneratorService motor;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CHAMADA_CONTEUDO')")
	public void gerarCodigo() {
		motor.Construir();
	}
}
