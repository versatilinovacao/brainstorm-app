package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.Contato;
import br.com.insideincloud.brainstormapp.api.model.list.ContatoList;
import br.com.insideincloud.brainstormapp.api.model.view.NotColaboradorContatosView;
import br.com.insideincloud.brainstormapp.api.repository.Contatos;
import br.com.insideincloud.brainstormapp.api.repository.NotColaboradoresContatosView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ContatoFilter;

@RestController
@RequestMapping("/contatos")
public class ContatoResource {
	
	@Autowired 
	private Contatos contatos;
	@Autowired
	private NotColaboradoresContatosView notcontatos;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CONTATO_CONTEUDO')")
	public List<NotColaboradorContatosView> colaboradores() {
		System.out.println("ESTOU NA VISUALIZAÇÂO DA VIEW "+notcontatos.findAll().size());
		
		return notcontatos.findAll();
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_CONTATO_CONTEUDO')")
	public Page<Contato> conteudo(@RequestBody ContatoFilter filtro,@PageableDefault(size=2) Pageable page) {
		return contatos.filtrar(filtro, page);
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CONTATO_SALVAR')")
	public ResponseEntity<String> salvar(@RequestBody ContatoList contatoList, HttpServletResponse response) {
		
		for (Contato contato : contatoList.getTransaction()) {
//			System.out.println("SALVANDO CONTATO >>>>> "+contato.getTelefone());
			salvarContato(contato);
		}
		
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(colaborador.getColaborador_id()).toUri();
//		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.ok("");
	}
	
	@PostMapping("/alterar")
	@PreAuthorize("hasAuthority('ROLE_CONTATO_SALVAR')")
	public ResponseEntity<String> alterar(@RequestBody ContatoList contatoList, HttpServletResponse response) {
		for (Contato contato : contatoList.getTransaction()) {
			salvarContato(contato);
		}
		
		return ResponseEntity.ok("");
	}
	
	
	private void salvarContato(Contato contato) {
		contatos.save(contato);
	}
}
