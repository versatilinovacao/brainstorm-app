package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="loterps",schema="nfse")
public class LoteRps implements Serializable {
	private static final long serialVersionUID = 3687297569392405179L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long loterps_id;
	private String numerolote;
	private String cnpj;
	private String inscricaomunicipal;
	private Integer quantidaderps;
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="loterps_rps",joinColumns = {@JoinColumn(name="loterps_id")},
								  inverseJoinColumns = {@JoinColumn(name="rps_id"),
										  				@JoinColumn(name="empresa_id"),
										  				@JoinColumn(name="filial_id")},
								  uniqueConstraints= {@UniqueConstraint(columnNames= {"loterps_id","empresa_id","filial_id"})})
	private List<Rps> listarps;
	
	
	public List<Rps> getListarps() {
		return listarps;
	}



	public void setListarps(List<Rps> listarps) {
		this.listarps = listarps;
	}



	public Long getLoterps_id() {
		return loterps_id;
	}


	public void setLoterps_id(Long loterps_id) {
		this.loterps_id = loterps_id;
	}


	public String getNumerolote() {
		return numerolote;
	}


	public void setNumerolote(String numerolote) {
		this.numerolote = numerolote;
	}


	public String getCnpj() {
		return cnpj;
	}


	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}


	public void setInscricaomunicipal(String inscricaomunicipal) {
		this.inscricaomunicipal = inscricaomunicipal;
	}


	public Integer getQuantidaderps() {
		return quantidaderps;
	}


	public void setQuantidaderps(Integer quantidaderps) {
		this.quantidaderps = quantidaderps;
	}


//	public List<Rps> getListarps() {
//			
//		return listarps;
//	}
//
//
//	public void setListarps(List<Rps> listarps) {
//					
//		this.listarps = listarps;
//	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loterps_id == null) ? 0 : loterps_id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoteRps other = (LoteRps) obj;
		if (loterps_id == null) {
			if (other.loterps_id != null)
				return false;
		} else if (!loterps_id.equals(other.loterps_id))
			return false;
		return true;
	}
	
	

}
