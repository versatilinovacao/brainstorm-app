package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sede",schema="empresa")
public class Sede implements Serializable {
	private static final long serialVersionUID = 4243588642595393412L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long sede_id;
	private String descricao;
	private BigDecimal area_construida;
	private BigDecimal area_externa;
	private BigDecimal area_estacionamento;
	private BigDecimal area_total;
	
	public Long getSede_id() {
		return sede_id;
	}
	public void setSede_id(Long sede_id) {
		this.sede_id = sede_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getArea_construida() {
		return area_construida;
	}
	public void setArea_construida(BigDecimal area_construida) {
		this.area_construida = area_construida;
	}
	public BigDecimal getArea_externa() {
		return area_externa;
	}
	public void setArea_externa(BigDecimal area_externa) {
		this.area_externa = area_externa;
	}
	public BigDecimal getArea_estacionamento() {
		return area_estacionamento;
	}
	public void setArea_estacionamento(BigDecimal area_estacionamento) {
		this.area_estacionamento = area_estacionamento;
	}
	public BigDecimal getArea_total() {
		return area_total;
	}
	public void setArea_total(BigDecimal area_total) {
		this.area_total = area_total;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sede_id == null) ? 0 : sede_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sede other = (Sede) obj;
		if (sede_id == null) {
			if (other.sede_id != null)
				return false;
		} else if (!sede_id.equals(other.sede_id))
			return false;
		return true;
	}
	
}
