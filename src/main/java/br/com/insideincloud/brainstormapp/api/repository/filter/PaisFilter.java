package br.com.insideincloud.brainstormapp.api.repository.filter;

public class PaisFilter {
	private String pais_id;
	private String sigla;
	private String descricao;
	
	public String getPais_id() {
		return pais_id;
	}
	public void setPais_id(String pais_id) {
		this.pais_id = pais_id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
