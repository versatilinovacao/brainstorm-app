package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cursos")
public class CursoMinor implements Serializable {
	private static final long serialVersionUID = 5331154054507847037L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long curso_id;
	@Column(name="descricao",length=100)
	private String descricao;
	
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((curso_id == null) ? 0 : curso_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CursoMinor other = (CursoMinor) obj;
		if (curso_id == null) {
			if (other.curso_id != null)
				return false;
		} else if (!curso_id.equals(other.curso_id))
			return false;
		return true;
	}
	
}
