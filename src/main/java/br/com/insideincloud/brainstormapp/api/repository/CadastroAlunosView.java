package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.view.CadastroAlunoView;

@Repository
public interface CadastroAlunosView extends JpaRepository<CadastroAlunoView, ColaboradorPK> {

}
