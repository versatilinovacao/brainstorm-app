package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ServicoPortoAlegreInativoView;

@Repository
public interface ServicosPortoAlegreInativosView extends JpaRepository<ServicoPortoAlegreInativoView, Long> {

}
