package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.AtividadeUpload;
import br.com.insideincloud.brainstormapp.api.repository.AtividadesUpload;
import br.com.insideincloud.brainstormapp.api.repository.TiposArquivos;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/arquivosupload")
public class ArquivoUploadResource {

	@Autowired
	private TiposArquivos tipoArquivos;
	
	@Autowired
	private AtividadesUpload atividades;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_ARQUIVOUPLOAD_SALVAR')")
	public RetornoWrapper<AtividadeUpload> salvarAtividade(@RequestBody AtividadeUpload atividade) {
		RetornoWrapper<AtividadeUpload> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( atividades.saveAndFlush(atividade) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o upload da atividade.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{atividade_id}")
	@PreAuthorize("hasAuthority('ROLE_ARQUIVOUPLOAD_CONTEUDO')")
	public RetornoWrapper<AtividadeUpload> conteudo(@PathVariable Long atividade_id) {
		RetornoWrapper<AtividadeUpload> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( atividades.findAllAtividades(atividade_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar arquivos de upload");
		}
		
		return retorno;
	}
	
	
	
}
