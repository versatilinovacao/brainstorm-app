package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.PeriodoLetivoInativoView;

@Repository
public interface PeriodosLetivosInativosView extends JpaRepository<PeriodoLetivoInativoView, Long> {

	@Query("select p from PeriodoLetivoInativoView p where p.empresa_id = ?1")
	public List<PeriodoLetivoInativoView> findByPeriodosLetivosInativos(Long empresa_id);
}
