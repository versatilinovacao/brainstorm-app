package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.IsCadernoInconsistenciaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.IsCadernoInconsistenciaViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.iscadernosinconsistenciasview.IsCadernosInconsistenciasViewQuery;

@Repository
public interface IsCadernosInconsistenciasView extends JpaRepository<IsCadernoInconsistenciaView,Long>, IsCadernosInconsistenciasViewQuery {
	public Page<IsCadernoInconsistenciaView> filtrar(IsCadernoInconsistenciaViewFilter filtro, Pageable page);
}
