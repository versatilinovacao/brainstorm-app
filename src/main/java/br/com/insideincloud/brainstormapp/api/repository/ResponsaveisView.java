package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ResponsavelView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResponsavelViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.responsavelview.ResponsaveisViewQuery;

@Repository
public interface ResponsaveisView extends JpaRepository<ResponsavelView,Long>, ResponsaveisViewQuery {
	public Page<ResponsavelView> filtrar(ResponsavelViewFilter filtro, Pageable page);
	
	@Query("select r from ResponsavelView r where r.responsavel_id <> ?1 and r.composicao_colaborador_id = ?2")
	public List<ResponsavelView> findResponsaveis(Long responsavel_id, Long composicao_colaborador_id); 
}
