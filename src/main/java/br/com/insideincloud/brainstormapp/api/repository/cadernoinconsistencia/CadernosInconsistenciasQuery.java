package br.com.insideincloud.brainstormapp.api.repository.cadernoinconsistencia;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.CadernoInconsistencia;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoInconsistenciaFilter;

public interface CadernosInconsistenciasQuery {
	public Page<CadernoInconsistencia> filtrar(CadernoInconsistenciaFilter filtro, Pageable page);
}
