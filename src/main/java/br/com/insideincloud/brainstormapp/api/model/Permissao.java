package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="permissoes_view",schema="remoto")
public class Permissao implements Serializable {
	private static final long serialVersionUID = -6754810987778845930L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long permissao_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="descricao")
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getPermissao_id() {
		return permissao_id;
	}
	public void setPermissao_id(Long permissao_id) {
		this.permissao_id = permissao_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((permissao_id == null) ? 0 : permissao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permissao other = (Permissao) obj;
		if (permissao_id == null) {
			if (other.permissao_id != null)
				return false;
		} else if (!permissao_id.equals(other.permissao_id))
			return false;
		return true;
	}
		
}
