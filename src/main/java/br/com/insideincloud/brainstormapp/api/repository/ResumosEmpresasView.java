package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ResumoEmpresaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResumoEmpresaViewFilter;

@Repository
public interface ResumosEmpresasView extends JpaRepository<ResumoEmpresaView,Long> {
	public Page<ResumoEmpresaView> filtrar(ResumoEmpresaViewFilter filtro,@PageableDefault(size=8) Pageable page);
}
