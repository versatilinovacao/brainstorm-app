package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.dblink.Bairro;
import br.com.insideincloud.brainstormapp.api.repository.BairrosRemoto;
import br.com.insideincloud.brainstormapp.api.repository.filter.BairroFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/bairros")
public class BairroResource {
	@Autowired
	private BairrosRemoto bairros;
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_BAIRRO_CONTEUDO')")
	public RetornoWrapper<Bairro> conteudo(@RequestBody BairroFilter filtro, @PageableDefault(size = 8) Pageable page) {
		RetornoWrapper<Bairro> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(bairros.findAll());
//			bairros.filtrar(filtro, page);
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível retornar informações, favor aguardar uns instantes e tente novamente!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;
	}
	

}
