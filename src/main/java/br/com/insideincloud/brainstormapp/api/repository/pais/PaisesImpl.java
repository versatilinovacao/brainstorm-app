package br.com.insideincloud.brainstormapp.api.repository.pais;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Pais;
import br.com.insideincloud.brainstormapp.api.model.Pais_;
import br.com.insideincloud.brainstormapp.api.repository.filter.PaisFilter;

public class PaisesImpl implements PaisesQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Pais> filtrar(PaisFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pais> criteria = builder.createQuery(Pais.class);
		Root<Pais> root = criteria.from(Pais.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<Pais> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(PaisFilter filtro, CriteriaBuilder builder, Root<Pais> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getPais_id())) {
				predicates.add(builder.equal(root.get("pais_id"), Long.parseLong(filtro.getPais_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getSigla())) {
				predicates.add(builder.equal(builder.lower(root.get("sigla")), filtro.getSigla().toLowerCase() ));
			}		
			if (!StringUtils.isEmpty(filtro.getDescricao())) {
				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
			}
		}
		
		System.out.println("FILTRO....:"+filtro.getPais_id());		
		System.out.println("FILTRO....:"+filtro.getSigla());		
		System.out.println("FILTRO....:"+filtro.getDescricao());		
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(PaisFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Pais> root = criteria.from(Pais.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	

}
