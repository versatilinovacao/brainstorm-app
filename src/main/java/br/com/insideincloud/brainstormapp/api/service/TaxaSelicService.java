package br.com.insideincloud.brainstormapp.api.service;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.TaxaSelic;
import br.com.insideincloud.brainstormapp.api.repository.TaxasSelics;
import br.com.insideincloud.brainstormapp.api.repository.filter.TaxaSelicFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@Service
public class TaxaSelicService {
    @Autowired
    private TaxasSelics taxasSelics;
    
    @Transactional
    public ResponseEntity<RetornoWrapper<TaxaSelic>> salvar(TaxaSelic taxaSelic, HttpServletResponse response) {
        URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(taxaSelic.getTaxaselic_id()).toUri();
        RetornoWrapper<TaxaSelic> retorno = new RetornoWrapper<TaxaSelic>();
        ExceptionWrapper except = new ExceptionWrapper(); 
        except.setCodigo(3);
        except.setMensagem("Não foi possível salvar a Taxa Selic.");
        response.setHeader("Location",uri.toASCIIString());
        
        try {
            taxaSelic = taxasSelics.saveAndFlush(taxaSelic);
            retorno.setSingle(taxaSelic);
            
        } catch (Exception e) {
            System.out.println("ERRO: "+e.getMessage());
            
            retorno.setException(except);            
            
        } finally {
            return ResponseEntity.created(uri).body(retorno);
        }
    

    }
    
    public RetornoWrapper<TaxaSelic> conteudo(TaxaSelicFilter filtro, Pageable page) {
        RetornoWrapper<TaxaSelic> retorno = new RetornoWrapper<TaxaSelic>();
        ExceptionWrapper except = new ExceptionWrapper();
        except.setCodigo(4);
        except.setMensagem("O correu um erro na busca das informações, tente novamente em alguns minutos.");
        
        try {
            //retorno.setPage( taxasSelics.filtrar(filtro,page) );
        } catch (Exception e) {
            retorno.setException(except);
        } finally {
            return retorno;        
        }
    }
}