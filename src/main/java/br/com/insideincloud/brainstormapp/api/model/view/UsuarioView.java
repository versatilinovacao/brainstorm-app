package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;

@Entity
@Table(name="usuario_view")
public class UsuarioView implements Serializable {
	private static final long serialVersionUID = 3832064841456469050L;

	@Id
	private Long usuario_id;
	private Long empresa_id;
	@Column(name="nome")
	private String nomecompleto;
	private boolean status;
	private String email;
	private String email_colaborador;
	private LocalDate data_nascimento;
	private String cpf;
	private String foto_thumbnail;
	private Boolean isserver;
	private Boolean isclient;
	private Boolean iscounter;
	private Boolean issistema;
	private Boolean isprofessor;
	private Long colaborador_id;
	private Long composicao_colaborador_id;
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="grupos_usuarios",joinColumns = @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name="gruposecurity_id"))
	private List<GrupoSecurityView> grupos;
	
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id", insertable=false, updatable=false),@JoinColumn(name="composicao_colaborador_id", insertable=false, updatable=false)})
	private Colaborador colaborador;
	
	public Boolean getIsprofessor() {
		return isprofessor;
	}
	public void setIsprofessor(Boolean isprofessor) {
		this.isprofessor = isprofessor;
	}
	public Boolean getIssistema() {
		return issistema;
	}
	public void setIssistema(Boolean issistema) {
		this.issistema = issistema;
	}
	public Colaborador getColaborador() {
		return colaborador;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public Long getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(Long composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public List<GrupoSecurityView> getGrupos() {
		return grupos;
	}
	public void setGrupos(List<GrupoSecurityView> grupos) {
		this.grupos = grupos;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Boolean getIsserver() {
		return isserver;
	}
	public void setIsserver(Boolean isserver) {
		this.isserver = isserver;
	}
	public Boolean getIsclient() {
		return isclient;
	}
	public void setIsclient(Boolean isclient) {
		this.isclient = isclient;
	}
	public Boolean getIscounter() {
		return iscounter;
	}
	public void setIscounter(Boolean iscounter) {
		this.iscounter = iscounter;
	}
	public String getEmail_colaborador() {
		return email_colaborador;
	}
	public void setEmail_colaborador(String email_colaborador) {
		this.email_colaborador = email_colaborador;
	}
	public LocalDate getData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(LocalDate data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getFoto_thumbnail() {
		return foto_thumbnail;
	}
	public void setFoto_thumbnail(String foto_thumbnail) {
		this.foto_thumbnail = foto_thumbnail;
	}
	public Long getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Long usuario_id) {
		this.usuario_id = usuario_id;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public String getNomecompleto() {
		return nomecompleto;
	}
	public void setNomecompleto(String nomecompleto) {
		this.nomecompleto = nomecompleto;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioView other = (UsuarioView) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		return true;
	}
	
}
