package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="classificacoes", schema="almoxarifado")
public class ClassificacaoArtefato implements Serializable{
	private static final long serialVersionUID = 3316322443502010840L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long classificacao_id;
	@Column(name="nome")
	private String nome;
	
	public Long getClassificacao_id() {
		return classificacao_id;
	}
	public void setClassificacao_id(Long classificacao_id) {
		this.classificacao_id = classificacao_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classificacao_id == null) ? 0 : classificacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassificacaoArtefato other = (ClassificacaoArtefato) obj;
		if (classificacao_id == null) {
			if (other.classificacao_id != null)
				return false;
		} else if (!classificacao_id.equals(other.classificacao_id))
			return false;
		return true;
	}
	
}
