package br.com.insideincloud.brainstormapp.api.model.execute;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="historico_controle",schema="action")
public class HistoricoControleAction implements Serializable {
	private static final long serialVersionUID = 3393637519486634144L;

	@Id
	private Long historico_controle_id;
	@Column(name="ano",length=4)
	private String ano;
	public Long getHistorico_controle_id() {
		return historico_controle_id;
	}
	public void setHistorico_controle_id(Long historico_controle_id) {
		this.historico_controle_id = historico_controle_id;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((historico_controle_id == null) ? 0 : historico_controle_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoControleAction other = (HistoricoControleAction) obj;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (historico_controle_id == null) {
			if (other.historico_controle_id != null)
				return false;
		} else if (!historico_controle_id.equals(other.historico_controle_id))
			return false;
		return true;
	}
	
	
}
