package br.com.insideincloud.brainstormapp.api.service.caderno;

public class CadernoFrequencias {
	private Integer item;
	private Long aluno_id;
	private String nome_aluno;
	
	private Boolean um;
	private Boolean dois;
	private Boolean tres;
	private Boolean quatro;
	private Boolean cinco;
	private Boolean seis;
	private Boolean sete;
	private Boolean oito;
	private Boolean nove;
	private Boolean dez;
	private Boolean onze;
	private Boolean doze;
	private Boolean treze;
	private Boolean quatorze;
	private Boolean quinze;
	private Boolean dezesseis;
	private Boolean dezessete;
	private Boolean dezoito;
	private Boolean dezenove;
	private Boolean vinte;
	private Boolean vinteum;
	private Boolean vintedois;
	private Boolean vintetres;
	private Boolean vintequatro;
	private Boolean vintecinco;
	private Boolean vinteseis;
	private Boolean vintesete;
	private Boolean vinteoito;
	private Boolean vintenove;
	private Boolean trinta;
	private Boolean trintaeum;
	
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public Boolean getUm() {
		return um;
	}
	public void setUm(Boolean um) {
		this.um = um;
	}
	public Boolean getDois() {
		return dois;
	}
	public void setDois(Boolean dois) {
		this.dois = dois;
	}
	public Boolean getTres() {
		return tres;
	}
	public void setTres(Boolean tres) {
		this.tres = tres;
	}
	public Boolean getQuatro() {
		return quatro;
	}
	public void setQuatro(Boolean quatro) {
		this.quatro = quatro;
	}
	public Boolean getCinco() {
		return cinco;
	}
	public void setCinco(Boolean cinco) {
		this.cinco = cinco;
	}
	public Boolean getSeis() {
		return seis;
	}
	public void setSeis(Boolean seis) {
		this.seis = seis;
	}
	public Boolean getSete() {
		return sete;
	}
	public void setSete(Boolean sete) {
		this.sete = sete;
	}
	public Boolean getOito() {
		return oito;
	}
	public void setOito(Boolean oito) {
		this.oito = oito;
	}
	public Boolean getNove() {
		return nove;
	}
	public void setNove(Boolean nove) {
		this.nove = nove;
	}
	public Boolean getDez() {
		return dez;
	}
	public void setDez(Boolean dez) {
		this.dez = dez;
	}
	public Boolean getOnze() {
		return onze;
	}
	public void setOnze(Boolean onze) {
		this.onze = onze;
	}
	public Boolean getDoze() {
		return doze;
	}
	public void setDoze(Boolean doze) {
		this.doze = doze;
	}
	public Boolean getTreze() {
		return treze;
	}
	public void setTreze(Boolean treze) {
		this.treze = treze;
	}
	public Boolean getQuatorze() {
		return quatorze;
	}
	public void setQuatorze(Boolean quatorze) {
		this.quatorze = quatorze;
	}
	public Boolean getQuinze() {
		return quinze;
	}
	public void setQuinze(Boolean quinze) {
		this.quinze = quinze;
	}
	public Boolean getDezesseis() {
		return dezesseis;
	}
	public void setDezesseis(Boolean dezesseis) {
		this.dezesseis = dezesseis;
	}
	public Boolean getDezessete() {
		return dezessete;
	}
	public void setDezessete(Boolean dezessete) {
		this.dezessete = dezessete;
	}
	public Boolean getDezoito() {
		return dezoito;
	}
	public void setDezoito(Boolean dezoito) {
		this.dezoito = dezoito;
	}
	public Boolean getDezenove() {
		return dezenove;
	}
	public void setDezenove(Boolean dezenove) {
		this.dezenove = dezenove;
	}
	public Boolean getVinte() {
		return vinte;
	}
	public void setVinte(Boolean vinte) {
		this.vinte = vinte;
	}
	public Boolean getVinteum() {
		return vinteum;
	}
	public void setVinteum(Boolean vinteum) {
		this.vinteum = vinteum;
	}
	public Boolean getVintedois() {
		return vintedois;
	}
	public void setVintedois(Boolean vintedois) {
		this.vintedois = vintedois;
	}
	public Boolean getVintetres() {
		return vintetres;
	}
	public void setVintetres(Boolean vintetres) {
		this.vintetres = vintetres;
	}
	public Boolean getVintequatro() {
		return vintequatro;
	}
	public void setVintequatro(Boolean vintequatro) {
		this.vintequatro = vintequatro;
	}
	public Boolean getVintecinco() {
		return vintecinco;
	}
	public void setVintecinco(Boolean vintecinco) {
		this.vintecinco = vintecinco;
	}
	public Boolean getVinteseis() {
		return vinteseis;
	}
	public void setVinteseis(Boolean vinteseis) {
		this.vinteseis = vinteseis;
	}
	public Boolean getVintesete() {
		return vintesete;
	}
	public void setVintesete(Boolean vintesete) {
		this.vintesete = vintesete;
	}
	public Boolean getVinteoito() {
		return vinteoito;
	}
	public void setVinteoito(Boolean vinteoito) {
		this.vinteoito = vinteoito;
	}
	public Boolean getVintenove() {
		return vintenove;
	}
	public void setVintenove(Boolean vintenove) {
		this.vintenove = vintenove;
	}
	public Boolean getTrinta() {
		return trinta;
	}
	public void setTrinta(Boolean trinta) {
		this.trinta = trinta;
	}
	public Boolean getTrintaeum() {
		return trintaeum;
	}
	public void setTrintaeum(Boolean trintaeum) {
		this.trintaeum = trintaeum;
	}
	
}
