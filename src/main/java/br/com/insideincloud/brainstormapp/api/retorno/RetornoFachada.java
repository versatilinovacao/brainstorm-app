package br.com.insideincloud.brainstormapp.api.retorno;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionTratada;
import br.com.insideincloud.brainstormapp.api.model.view.MatriculaAlunoView;

public class RetornoFachada<T> {
	
	private T single;
	private Page<T> page;
	private ExceptionTratada exception;
	
	public void setSingle(T single) {
		this.single = single;
	} 
	public T getSingle() {
		return this.single;
	}
	public Page<T> getPage() {
		return page;
	}
	public void setPage(Page<T> page) {
 
		this.page = page;
	}
	public ExceptionTratada getException() {
		return exception;
	}
	public void setException(ExceptionTratada exception) {
		this.exception = exception;
	}
	
	
	

}
