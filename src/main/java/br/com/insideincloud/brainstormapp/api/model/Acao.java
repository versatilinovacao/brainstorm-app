package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

public class Acao implements Serializable {
	private static final long serialVersionUID = 8608525318783866160L;

	private Long acao_id;
	private String sigla;
	private String nome;
	
	public Long getAcao_id() {
		return acao_id;
	}
	public void setAcao_id(Long acao_id) {
		this.acao_id = acao_id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acao_id == null) ? 0 : acao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Acao other = (Acao) obj;
		if (acao_id == null) {
			if (other.acao_id != null)
				return false;
		} else if (!acao_id.equals(other.acao_id))
			return false;
		return true;
	}
	
	

}
