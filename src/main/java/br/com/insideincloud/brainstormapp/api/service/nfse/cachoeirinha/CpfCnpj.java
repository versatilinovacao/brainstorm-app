package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class CpfCnpj {
	private String Cpf;
	private String Cnpj;
	
	public String getCpf() {
		return Cpf;
	}
	public void setCpf(String cpf) {
		Cpf = cpf;
	}
	public String getCnpj() {
		return Cnpj;
	}
	public void setCnpj(String cnpj) {
		Cnpj = cnpj;
	}

}
