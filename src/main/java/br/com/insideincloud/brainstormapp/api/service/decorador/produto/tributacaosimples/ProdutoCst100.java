package br.com.insideincloud.brainstormapp.api.service.decorador.produto.tributacaosimples;

import br.com.insideincloud.brainstormapp.api.service.decorador.produto.ProdutoComponente;
import br.com.insideincloud.brainstormapp.api.service.decorador.produto.ProdutoDecorado;

public class ProdutoCst100 extends ProdutoDecorado {

	public ProdutoCst100(ProdutoComponente produto) {
		super(produto);
	}

}
