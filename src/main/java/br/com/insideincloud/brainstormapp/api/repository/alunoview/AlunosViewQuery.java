package br.com.insideincloud.brainstormapp.api.repository.alunoview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AlunoFilterView;

public interface AlunosViewQuery {
	public Page<AlunoView> filtrar(AlunoFilterView filtro, Pageable page);

}
