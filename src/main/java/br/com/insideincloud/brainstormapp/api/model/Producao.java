package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="producao",schema="recurso")
public class Producao implements Serializable {
	private static final long serialVersionUID = 8047754177850429082L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long producao_id;
	@Column(name="nome",length=60)
	private String nome;
	
	public Long getProducao_id() {
		return producao_id;
	}
	public void setProducao_id(Long producao_id) {
		this.producao_id = producao_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((producao_id == null) ? 0 : producao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producao other = (Producao) obj;
		if (producao_id == null) {
			if (other.producao_id != null)
				return false;
		} else if (!producao_id.equals(other.producao_id))
			return false;
		return true;
	}
	
}
