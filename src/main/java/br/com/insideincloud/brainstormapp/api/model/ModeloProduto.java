package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="modelos",schema="recurso")
public class ModeloProduto implements Serializable {
	private static final long serialVersionUID = 8464064923802736185L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long modelo_id;
	@Column(name="nome",length=100)
	private String nome;
	
	public Long getModelo_id() {
		return modelo_id;
	}
	public void setModelo_id(Long modelo_id) {
		this.modelo_id = modelo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((modelo_id == null) ? 0 : modelo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeloProduto other = (ModeloProduto) obj;
		if (modelo_id == null) {
			if (other.modelo_id != null)
				return false;
		} else if (!modelo_id.equals(other.modelo_id))
			return false;
		return true;
	}
	
	
}
