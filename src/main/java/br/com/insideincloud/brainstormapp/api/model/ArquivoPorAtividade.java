package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="arquivos_por_atividades",schema="escola")
public class ArquivoPorAtividade implements Serializable {
	private static final long serialVersionUID = 8013423856971059348L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long arquivoporatividade_id;
	@OneToOne
	@JoinColumns({@JoinColumn(name="aluno_id"),@JoinColumn(name="composicao_aluno_id")})
	private Aluno aluno;
	@OneToOne
	@JoinColumn(name="arquivo_id")
	private Arquivo arquivo;
	@OneToOne
	@JoinColumn(name="atividade_id")
	private Atividade atividade;
	
	public Long getArquivoporatividade_id() {
		return arquivoporatividade_id;
	}
	public void setArquivoporatividade_id(Long arquivoporatividade_id) {
		this.arquivoporatividade_id = arquivoporatividade_id;
	}
	public Aluno getAluno() {
		return aluno;
	}
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public Arquivo getArquivo() {
		return arquivo;
	}
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivoporatividade_id == null) ? 0 : arquivoporatividade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoPorAtividade other = (ArquivoPorAtividade) obj;
		if (arquivoporatividade_id == null) {
			if (other.arquivoporatividade_id != null)
				return false;
		} else if (!arquivoporatividade_id.equals(other.arquivoporatividade_id))
			return false;
		return true;
	}

}
