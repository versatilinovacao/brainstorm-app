package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="periodo_letivo_caderno_view",schema="escola")
public class PeriodoLetivoCadernoView implements Serializable {
	private static final long serialVersionUID = -8985460457455107027L;

	@Id
	public Long periodo_letivo_item_id;
	@Column(name="periodo_letivo_nome",length=100)
	public String periodo_letivo_nome;
	public Long caderno_id;
	
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public String getPeriodo_letivo_nome() {
		return periodo_letivo_nome;
	}
	public void setPeriodo_letivo_nome(String periodo_letivo_nome) {
		this.periodo_letivo_nome = periodo_letivo_nome;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	
}
