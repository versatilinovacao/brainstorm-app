package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.CardapioAtivoView;

@Repository
public interface CardapiosAtivosView extends JpaRepository<CardapioAtivoView, Long> {

}
