package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="estados_view",schema="remoto")
public class Estado implements Serializable {
	private static final long serialVersionUID = 5381417263693238002L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long estado_id;
	@Column(name="descricao",length=80)
	private String descricao;
	@Column(name="sigla",length=3)
	private String sigla;
	@OneToOne
//	@JoinColumn(name="regiao_id")
//	private Regiao regiao;
	@ManyToOne
	@JoinColumn(name="pais_id")
	private Pais pais;
	private Integer codigo_ibge;
	
	public Integer getCodigo_ibge() {
		return codigo_ibge;
	}
	public void setCodigo_ibge(Integer codigo_ibge) {
		this.codigo_ibge = codigo_ibge;
	}
	public Long getEstado_id() {
		return estado_id;
	}
	public void setEstado_id(Long estado_id) {
		this.estado_id = estado_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
//	public Regiao getRegiao() {
//		return regiao;
//	}
//	public void setRegiao(Regiao regiao) {
//		this.regiao = regiao;
//	}
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estado_id == null) ? 0 : estado_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (estado_id == null) {
			if (other.estado_id != null)
				return false;
		} else if (!estado_id.equals(other.estado_id))
			return false;
		return true;
	}
	
}
