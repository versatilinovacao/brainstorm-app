package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import br.com.insideincloud.brainstormapp.api.model.minor.ArtefatoMinor;
import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorMinor;

@Entity
@Table(name="contratos",schema="administracao")
public class Contrato implements Serializable {
	private static final long serialVersionUID = 7143439353982286539L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long contrato_id;
	@OneToOne
	@JoinColumn(name="composicao_id")
	private Contrato contrato;
	@Column(name="inicio_contrato")
	private LocalDate iniciocontrato;
	@Column(name="duracao_inicial")
	private Integer duracaoinicial;
	@Column(name="renovacoes")
	private Integer proximasrenovacoes;
	
	@OneToOne
	@JoinColumn(name="tiporenovacao_id")
	private TipoRenovacao tiporenovacao;
	@OneToOne
	@JoinColumn(name="indicereajuste_id")
	private Indice indice;
	
	private Double mensalidade;

	@OneToOne
	@JoinColumns({@JoinColumn(name="contratante_id"),@JoinColumn(name="composicao_contratante_id")})
	private ColaboradorMinor contratante;
	@OneToOne
	@JoinColumns({@JoinColumn(name="contratada_id"),@JoinColumn(name="composicao_contratada_id")})
	private ColaboradorMinor contratada;
	
	@OneToOne
	@JoinColumn(name="artefato_id")
	private ArtefatoMinor artefato;

	@Column(name="parcelas_isentas")
	private Integer parcelasisentas;
	private Double desconto;
	@Column(name="ciclo_desconto")
	private Integer ciclodesconto;
	@Column(name="dia_pagamento")
	private Integer diapagamento;
	
	@OneToOne
	@JoinColumn(name="negociacao_id")
	private Negociacao negociacao; //Identifica o tipo de pagamento combinado (Dinheiro, Cartão, Boleto, Promissória, Cheque,
	
	private Double total;
	
	@OneToOne
	@JoinColumn(name="formapagamento_id")
//	@Cascade(CascadeType.ALL)	
	private FormaPagamento formapagamento; //Identifica o tipo de pagamento utilizado (Dinheiro, Cartao, Boleto, Promissória, Cheque, 
	
	@Fetch(FetchMode.SELECT)
	@ManyToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinTable(name="parcelas_por_contratos",schema="administracao",joinColumns= {@JoinColumn(name="contrato_id")},inverseJoinColumns= {@JoinColumn(name="contareceber_id")})
	private List<ContaReceber> parcelas;
	
	private Boolean status;

	@OneToOne
	@JoinColumn(name="contratomodelo_id")
	private ContratoModelo contratomodelo;
	
	public ContratoModelo getContratomodelo() {
		return contratomodelo;
	}
	public void setContratomodelo(ContratoModelo contratomodelo) {
		this.contratomodelo = contratomodelo;
	}
	public List<ContaReceber> getParcelas() {
		return parcelas;
	}
	public void setParcelas(List<ContaReceber> parcelas) {
		this.parcelas = parcelas;
	}
	public FormaPagamento getFormapagamento() {
		return formapagamento;
	}
	public void setFormapagamento(FormaPagamento formapagamento) {
		this.formapagamento = formapagamento;
	}
	public Negociacao getNegociacao() {
		return negociacao;
	}
	public void setNegociacao(Negociacao negociacao) {
		this.negociacao = negociacao;
	}
	public void setArtefato(ArtefatoMinor artefato) {
		this.artefato = artefato;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getContrato_id() {
		return contrato_id;
	}
	public void setContrato_id(Long contrato_id) {
		this.contrato_id = contrato_id;
	}
	public Contrato getContrato() {
		return contrato;
	}
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	public LocalDate getIniciocontrato() {
		return iniciocontrato;
	}
	public void setIniciocontrato(LocalDate iniciocontrato) {
		this.iniciocontrato = iniciocontrato;
	}
	public Integer getDuracaoinicial() {
		return duracaoinicial;
	}
	public void setDuracaoinicial(Integer duracaoinicial) {
		this.duracaoinicial = duracaoinicial;
	}
	public Integer getProximasrenovacoes() {
		return proximasrenovacoes;
	}
	public void setProximasrenovacoes(Integer proximasrenovacoes) {
		this.proximasrenovacoes = proximasrenovacoes;
	}
	public TipoRenovacao getTiporenovacao() {
		return tiporenovacao;
	}
	public void setTiporenovacao(TipoRenovacao tiporenovacao) {
		this.tiporenovacao = tiporenovacao;
	}
	public Indice getIndice() {
		return indice;
	}
	public void setIndice(Indice indice) {
		this.indice = indice;
	}
	public Double getMensalidade() {
		return mensalidade;
	}
	public void setMensalidade(Double mensalidade) {
		this.mensalidade = mensalidade;
	}
	public ColaboradorMinor getContratante() {
		return contratante;
	}
	public void setContratante(ColaboradorMinor contratante) {
		this.contratante = contratante;
	}
	public ColaboradorMinor getContratada() {
		return contratada;
	}
	public void setContratada(ColaboradorMinor contratada) {
		this.contratada = contratada;
	}
	public ArtefatoMinor getArtefato() {
		return artefato;
	}
	public void setObjeto(ArtefatoMinor artefato) {
		this.artefato = artefato;
	}
	public Integer getParcelasisentas() {
		return parcelasisentas;
	}
	public void setParcelasisentas(Integer parcelasisentas) {
		this.parcelasisentas = parcelasisentas;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Integer getCiclodesconto() {
		return ciclodesconto;
	}
	public void setCiclodesconto(Integer ciclodesconto) {
		this.ciclodesconto = ciclodesconto;
	}
	public Integer getDiapagamento() {
		return diapagamento;
	}
	public void setDiapagamento(Integer diapagamento) {
		this.diapagamento = diapagamento;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contrato_id == null) ? 0 : contrato_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contrato other = (Contrato) obj;
		if (contrato_id == null) {
			if (other.contrato_id != null)
				return false;
		} else if (!contrato_id.equals(other.contrato_id))
			return false;
		return true;
	}
	
}