package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivo;

@Component
public class MateriaComposicao {
	private Long materia_id;
	private String nome;
	private PeriodoLetivo periodo;
	private List<PeriodoLetivoItemBase> composicoes;
	
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public PeriodoLetivo getPeriodo() {
		return periodo;
	}
	public void setPeriodo(PeriodoLetivo periodo) {
		this.periodo = periodo;
	}
	public List<PeriodoLetivoItemBase> getComposicoes() {
		return composicoes;
	}
	public void setComposicoes(List<PeriodoLetivoItemBase> composicoes) {
		this.composicoes = composicoes;
	}
	
	
}



/*
 * 
 * 	
 * 
 * MateriaComposicao composicao = new MateriaComposicao();
 * 	composicao.setMateria_id(1L);
 * 	composicao.setNome("Teste");
 * PeriodoLetivoItemBase periodo = new PeriodoLetivoItemBase();
 *  periodo.setPeriodo_letivo_item_id(1L);
 *  periodo.setPeriodo_letivo_id(1L);
 *  periodo.setdescricao();
 *  periodo.setPeriodo_inicial();
 *  periodo.setPeriodo_final();
 * 	periodo.getMaterias().add(new MateriaBase(1,"Portugues");
 * 	periodo.getMaterias().add(new MateriaBase(2,"Matematica");
 * 	periodo.getMaterias().add(new MateriaBase(3,"Inglês");
 * 	composicao.getComposicoes().add(periodo);
 * 
 * 
 * 
 * 
 * */
