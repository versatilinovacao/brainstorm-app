package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gruposecurityview",schema="public")
public class GrupoSecurityView implements Serializable {
	private static final long serialVersionUID = 5039299105622151107L;

	@Id
	private Long gruposecurity_id;
	private String nome;
	
	public Long getGruposecurity_id() {
		return gruposecurity_id;
	}
	public void setGruposecurity_id(Long gruposecurity_id) {
		this.gruposecurity_id = gruposecurity_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gruposecurity_id == null) ? 0 : gruposecurity_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoSecurityView other = (GrupoSecurityView) obj;
		if (gruposecurity_id == null) {
			if (other.gruposecurity_id != null)
				return false;
		} else if (!gruposecurity_id.equals(other.gruposecurity_id))
			return false;
		return true;
	}
	
}
