package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_clientes")
public class TipoCliente implements Serializable {
	private static final long serialVersionUID = -5063882462661993444L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tipo_cliente_id;
	@Column(name="descricao")
	
	private String descricao;
	public Long getTipo_cliente_id() {
		return tipo_cliente_id;
	}
	public void setTipo_cliente_id(Long tipo_cliente_id) {
		this.tipo_cliente_id = tipo_cliente_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tipo_cliente_id == null) ? 0 : tipo_cliente_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoCliente other = (TipoCliente) obj;
		if (tipo_cliente_id == null) {
			if (other.tipo_cliente_id != null)
				return false;
		} else if (!tipo_cliente_id.equals(other.tipo_cliente_id))
			return false;
		return true;
	}	
	
}
