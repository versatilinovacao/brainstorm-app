package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Bairro;
import br.com.insideincloud.brainstormapp.api.repository.bairro.BairrosQuery;

@Repository
public interface Bairros extends JpaRepository<Bairro,Long>,BairrosQuery {

}
