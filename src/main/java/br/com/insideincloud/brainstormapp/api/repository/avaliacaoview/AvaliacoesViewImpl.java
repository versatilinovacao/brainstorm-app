package br.com.insideincloud.brainstormapp.api.repository.avaliacaoview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.AvaliacaoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.AvaliacaoViewFilter;

public class AvaliacoesViewImpl implements AvaliacoesViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<AvaliacaoView> filtrar(AvaliacaoViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<AvaliacaoView> criteria = builder.createQuery(AvaliacaoView.class);
		Root<AvaliacaoView> root = criteria.from(AvaliacaoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<AvaliacaoView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(AvaliacaoViewFilter filtro, CriteriaBuilder builder, Root<AvaliacaoView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getAluno_id())) {
				predicates.add(builder.equal(root.get("aluno").get("colaborador_id"), Long.parseLong(filtro.getAluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getAvaliacao_id())) {
				predicates.add(builder.equal(root.get("avaliacao_id"), Long.parseLong(filtro.getAvaliacao_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getPeriodo_letivo_item_id())) {
				predicates.add(builder.equal(root.get("periodo_letivo_item_id"), Long.parseLong(filtro.getPeriodo_letivo_item_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getNome_aluno())) {
				predicates.add(builder.like(builder.lower(root.get("nome_aluno")), "%" + filtro.getNome_aluno().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getPeriodo_letivo_nome())) {
				predicates.add(builder.like(builder.lower(root.get("periodo_letivo_nome")), "%" + filtro.getPeriodo_letivo_nome().toLowerCase() + "%"));
			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(AvaliacaoViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<AvaliacaoView> root = criteria.from(AvaliacaoView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
