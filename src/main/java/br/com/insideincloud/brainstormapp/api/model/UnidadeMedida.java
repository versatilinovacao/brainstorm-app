package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="unidades_medidas",schema="recurso")
public class UnidadeMedida implements Serializable {
	private static final long serialVersionUID = -6183787805720230506L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long unidade_medida_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="sigla",length=5)
	private String sigla;
	@Column(name="sped",length=5)
	private String sped;
	
	public Long getUnidade_medida_id() {
		return unidade_medida_id;
	}
	public void setUnidade_medida_id(Long unidade_medida_id) {
		this.unidade_medida_id = unidade_medida_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getSped() {
		return sped;
	}
	public void setSped(String sped) {
		this.sped = sped;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((unidade_medida_id == null) ? 0 : unidade_medida_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeMedida other = (UnidadeMedida) obj;
		if (unidade_medida_id == null) {
			if (other.unidade_medida_id != null)
				return false;
		} else if (!unidade_medida_id.equals(other.unidade_medida_id))
			return false;
		return true;
	}
	
	
}
