package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gradecurricular_view",schema="escola")
public class GradeCurricularView implements Serializable {
	private static final long serialVersionUID = -8967118764759893222L;

	@Id
	private Long gradecurricular_id;
	@Column(name="turma_id")
	private Long turma_id;
	@Column(name="nome_turma")
	private String nome_turma;
	@Column(name="professor_id")
	private Long professor_id;
	@Column(name="nome_professor")
	private String nome_professor;
	@Column(name="materia_id")
	private Long materia_id;
	@Column(name="nome_materia")
	private String nome_materia;
	@Column(name="curso_id")
	private Long curso_id;
	@Column(name="nome_curso")
	private String nome_curso;
	
	public String getNome_curso() {
		return nome_curso;
	}
	public void setNome_curso(String nome_curso) {
		this.nome_curso = nome_curso;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public String getNome_turma() {
		return nome_turma;
	}
	public void setNome_turma(String nome_turma) {
		this.nome_turma = nome_turma;
	}
	public Long getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(Long professor_id) {
		this.professor_id = professor_id;
	}
	public String getNome_professor() {
		return nome_professor;
	}
	public void setNome_professor(String nome_professor) {
		this.nome_professor = nome_professor;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getNome_materia() {
		return nome_materia;
	}
	public void setNome_materia(String nome_materia) {
		this.nome_materia = nome_materia;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((curso_id == null) ? 0 : curso_id.hashCode());
		result = prime * result + ((gradecurricular_id == null) ? 0 : gradecurricular_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GradeCurricularView other = (GradeCurricularView) obj;
		if (curso_id == null) {
			if (other.curso_id != null)
				return false;
		} else if (!curso_id.equals(other.curso_id))
			return false;
		if (gradecurricular_id == null) {
			if (other.gradecurricular_id != null)
				return false;
		} else if (!gradecurricular_id.equals(other.gradecurricular_id))
			return false;
		return true;
	}
	
	
}
