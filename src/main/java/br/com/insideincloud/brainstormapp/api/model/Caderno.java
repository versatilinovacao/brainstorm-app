package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="cadernos",schema="escola")
public class Caderno implements Serializable {
	private static final long serialVersionUID = 5771495256258632906L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long caderno_id;
	@Column(name="descricao",length=100)
	private String descricao;
	@OneToOne
	@JoinColumn(name="gradecurricular_id")
	private GradeCurricular gradecurricular;
	private String conceito;
	private Integer nota;
	@OneToOne
	@JoinColumns({@JoinColumn(name="escola_id"),@JoinColumn(name="composicao_escola_id")})
	private Colaborador escola;
	@OneToOne
	@JoinColumn(name="periodo_letivo_id")
	private PeriodoLetivo periodoletivo;
	@OneToMany(mappedBy="caderno")
	private List<CapaCaderno> capas;
	private Boolean status;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public List<CapaCaderno> getCapas() {
		return capas;
	}
	public void setCapas(List<CapaCaderno> capas) {
		this.capas = capas;
	}
	public PeriodoLetivo getPeriodoletivo() {
		return periodoletivo;
	}
	public void setPeriodoletivo(PeriodoLetivo periodoletivo) {
		this.periodoletivo = periodoletivo;
	}
	public Colaborador getEscola() {
		return escola;
	}
	public void setEscola(Colaborador escola) {
		this.escola = escola;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	public GradeCurricular getGradecurricular() {
		return gradecurricular;
	}
	public void setGradecurricular(GradeCurricular gradecurricular) {
		this.gradecurricular = gradecurricular;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public Integer getNota() {
		return nota;
	}
	public void setNota(Integer nota) {
		this.nota = nota;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caderno_id == null) ? 0 : caderno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Caderno other = (Caderno) obj;
		if (caderno_id == null) {
			if (other.caderno_id != null)
				return false;
		} else if (!caderno_id.equals(other.caderno_id))
			return false;
		return true;
	}
	
}
