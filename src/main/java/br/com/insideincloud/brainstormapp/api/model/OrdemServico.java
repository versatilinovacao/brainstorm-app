package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="ordensservicos",schema="servico")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discriminador",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("ORDEMSERVICO")
public class OrdemServico implements Serializable {
	private static final long serialVersionUID = 6363361964775244385L;
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long ordemservico_id;
	@OneToOne
	@JoinColumns({@JoinColumn(name="colaborador_id"),@JoinColumn(name="composicao_colaborador_id")})
	private Colaborador cliente;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime abertura;
	private LocalDate emissao; //Emissão da Order só acontecerar após status: "Resolvida"
	private LocalDate fechamento;
	@OneToOne
	@JoinColumn(name="ordemservicotipo_id")
	private OrdemServicoTipo tipo;
	
	@OneToMany(mappedBy="ordemservico", targetEntity=ItemOrdemServico.class, cascade=CascadeType.ALL)
	private List<ItemOrdemServico> servicos;
	
	private Double issqn;
	private Double retencaoissqn;
	@Column(name="desconto_percentual")
	private Double desconto;
	private Double total;
//	private List<OrdemServicoStatus> status;
	
	public Long getOrdemservico_id() {
		return ordemservico_id;
	}
	public void setOrdemservico_id(Long ordemservico_id) {
		this.ordemservico_id = ordemservico_id;
	}
	public Colaborador getCliente() {
		return cliente;
	}
	public void setCliente(Colaborador cliente) {
		this.cliente = cliente;
	}
	public LocalDateTime getAbertura() {
		return abertura;
	}
	public void setAbertura(LocalDateTime abertura) {
		this.abertura = abertura;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getFechamento() {
		return fechamento;
	}
	public void setFechamento(LocalDate fechamento) {
		this.fechamento = fechamento;
	}
	public OrdemServicoTipo getTipo() {
		return tipo;
	}
	public void setTipo(OrdemServicoTipo tipo) {
		this.tipo = tipo;
	}
	public List<ItemOrdemServico> getServicos() {
		return servicos;
	}
	public void setServicos(List<ItemOrdemServico> servicos) {
		this.servicos = servicos;
	}
	public Double getIssqn() {
		return issqn;
	}
	public void setIssqn(Double issqn) {
		this.issqn = issqn;
	}
	public Double getRetencaoissqn() {
		return retencaoissqn;
	}
	public void setRetencaoissqn(Double retencaoissqn) {
		this.retencaoissqn = retencaoissqn;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ordemservico_id == null) ? 0 : ordemservico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdemServico other = (OrdemServico) obj;
		if (ordemservico_id == null) {
			if (other.ordemservico_id != null)
				return false;
		} else if (!ordemservico_id.equals(other.ordemservico_id))
			return false;
		return true;
	}
	
}
