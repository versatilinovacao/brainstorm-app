package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.minor.ColaboradorConsumidor;
import br.com.insideincloud.brainstormapp.api.model.minor.PlanoContaMinor;
import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorEmpresaResumoView;

@Entity
@Table(name="carnes",schema="financeiro")
public class CarnePagamento implements Serializable {
	private static final long serialVersionUID = 8700155933058932941L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long carne_id;
	
	private LocalDate vencimento;
	private LocalDate emissao;
	private LocalDate pagamento;
	private LocalDate quitacao;
	
	private Double valor;
	@Column(name="valor_quitacao")
	private Double valorquitacao;
	
	@OneToOne
	@JoinColumn(name="empresa_id")
	private ColaboradorEmpresaResumoView empresa;
	
	@OneToOne
	@JoinColumns({ @JoinColumn(name="responsavel_id"),@JoinColumn(name="composicao_responsavel_id") })
	private ColaboradorConsumidor responsavel;
	
	@OneToOne
	@JoinColumn(name="artefato_id")
	private Artefato artefato;
	
	@OneToOne
	@JoinColumn(name="planoconta_id")
	private PlanoContaMinor planoconta;
	
	private Integer parcelas;
	
	private Boolean status;

	public Long getCarne_id() {
		return carne_id;
	}
	public void setCarne_id(Long carne_id) {
		this.carne_id = carne_id;
	}
	public LocalDate getVencimento() {
		return vencimento;
	}
	public void setVencimento(LocalDate vencimento) {
		this.vencimento = vencimento;
	}
	public LocalDate getEmissao() {
		return emissao;
	}
	public void setEmissao(LocalDate emissao) {
		this.emissao = emissao;
	}
	public LocalDate getPagamento() {
		return pagamento;
	}
	public void setPagamento(LocalDate pagamento) {
		this.pagamento = pagamento;
	}
	public LocalDate getQuitacao() {
		return quitacao;
	}
	public void setQuitacao(LocalDate quitacao) {
		this.quitacao = quitacao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getValorquitacao() {
		return valorquitacao;
	}
	public void setValorquitacao(Double valorquitacao) {
		this.valorquitacao = valorquitacao;
	}
	public ColaboradorEmpresaResumoView getEmpresa() {
		return empresa;
	}
	public void setEmpresa(ColaboradorEmpresaResumoView empresa) {
		this.empresa = empresa;
	}
	public ColaboradorConsumidor getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(ColaboradorConsumidor responsavel) {
		this.responsavel = responsavel;
	}
	public Artefato getArtefato() {
		return artefato;
	}
	public void setArtefato(Artefato artefato) {
		this.artefato = artefato;
	}
	public PlanoContaMinor getPlanoconta() {
		return planoconta;
	}
	public void setPlanoconta(PlanoContaMinor planoconta) {
		this.planoconta = planoconta;
	}
	public Integer getParcelas() {
		return parcelas;
	}
	public void setParcelas(Integer parcelas) {
		this.parcelas = parcelas;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(carne_id);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof CarnePagamento))
			return false;
		CarnePagamento other = (CarnePagamento) obj;
		return Objects.equals(carne_id, other.carne_id);
	}
	
}
