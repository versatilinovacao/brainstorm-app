/**
 * 
 * 		Serviço destinado a remessa de notas fiscais eletrônicas.
 * 	
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://server.nfse.thema.inf.br", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package br.com.insideincloud.brainstormapp.api.soap.homologacao.cachoeirinha;

