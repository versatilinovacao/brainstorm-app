package br.com.insideincloud.brainstormapp.api.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.model.Objeto;
import br.com.insideincloud.brainstormapp.api.repository.Objetos;

@Service
public class CodeGeneratorService {
	@Autowired
	private Objetos objetos;
	
	private List<String> linhas = new ArrayList<String>();
	
	public void Construir() {
		try {
			for(Objeto conteudo: this.objetos.findAll()) {
				linhas.add("angular.module(\"brainstorm\").factory(\""+conteudo.getIdentificacao()+"\", function() {");
				//linhas.add("var _getChamada = function(chamada){");
				iterator(Class.forName(conteudo.getIdentificacao()).getDeclaredFields());				
				linhas.add("}");
				
				novaApi();
				saveApi();
				filterApi();
				deleteApi();
			}
		} catch (Exception e) {
			
		}
	}
	
	private void iterator(Field[] fields) {
		for(Field conteudo : fields) {
			if (conteudo instanceof Object) {
				try {
					Object objIterado = Class.forName(conteudo.getClass().getName()).newInstance();
					iterator(objIterado.getClass().getDeclaredFields());
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				if (conteudo.getType().isArray()) {
					System.out.println("Sou um Array");
				} else if (conteudo.getType().isPrimitive()) {
					linhas.add("");
				}
			}
		}
		
	}

	private void novaApi() {	
		try {
			BufferedWriter bufWrite = new BufferedWriter(new FileWriter(""));
			Scanner in = new Scanner(System.in);
			
			for(String linha : linhas) {
				System.out.println(linha);
				linha = in.nextLine();
				bufWrite.append(linha + "\n");
			}	
			
			in.close();
			bufWrite.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void saveApi() {
		try {
			BufferedWriter bufWrite = new BufferedWriter(new FileWriter(""));
			Scanner in = new Scanner(System.in);
			
			for(String linha : linhas) {
				System.out.println(linha);
				linha = in.nextLine();
				bufWrite.append(linha + "\n");				
			}
			
			in.close();
			bufWrite.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void deleteApi() {
		// TODO Auto-generated method stub
		
	}

	private void filterApi() {
		// TODO Auto-generated method stub
		
	}
	
}
