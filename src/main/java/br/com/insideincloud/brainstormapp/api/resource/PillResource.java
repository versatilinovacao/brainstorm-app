package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.PillColaborador;
import br.com.insideincloud.brainstormapp.api.model.PillColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.PillsColaboradores;
import br.com.insideincloud.brainstormapp.api.repository.PillsColaboradoresView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PillColaboradorViewFilter;

@RestController
@RequestMapping("/pillscolaboradores")
public class PillResource {
	
	@Autowired
	private PillsColaboradores classificacoes;
	
	@Autowired
	private PillsColaboradoresView classificacoescolaboradores;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public List<PillColaborador> conteudo() {
		return classificacoes.findAll();
	}
	
	@PutMapping
	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_CONTEUDO')")
	public Page<PillColaboradorView> resultado(@RequestBody PillColaboradorViewFilter filtro,@PageableDefault(size=5) Pageable page) {
		return classificacoescolaboradores.filtrar(filtro, page);
	}

}
