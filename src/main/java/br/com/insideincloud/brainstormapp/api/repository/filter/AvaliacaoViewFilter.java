package br.com.insideincloud.brainstormapp.api.repository.filter;

public class AvaliacaoViewFilter {
	private String avaliacao_id;
	private String periodo_letivo_item_id;
	private String caderno_id;
	private String aluno_id;
	private String nome_aluno;
	private String periodo_letivo_nome;
	private String conceito;
	private String nota;
	
	public String getAvaliacao_id() {
		return avaliacao_id;
	}
	public void setAvaliacao_id(String avaliacao_id) {
		this.avaliacao_id = avaliacao_id;
	}
	public String getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(String periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(String aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public String getPeriodo_letivo_nome() {
		return periodo_letivo_nome;
	}
	public void setPeriodo_letivo_nome(String periodo_letivo_nome) {
		this.periodo_letivo_nome = periodo_letivo_nome;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}

	
}
