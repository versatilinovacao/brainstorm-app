package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="telas_security",schema="public")
public class TelaSecurity implements Serializable {
	private static final long serialVersionUID = 846283513987489176L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long telasecurity_id;
	@OneToOne
	@JoinColumn(name="telasistema_id")
	private TelaSistema telasistema;
	@Fetch(FetchMode.SELECT)
//	@Cascade(CascadeType.DELETE)
	@ManyToMany(fetch=FetchType.EAGER)                          
	@JoinTable(name="telasecurity_permissao",joinColumns = @JoinColumn(name="telasecurity_id"), inverseJoinColumns= @JoinColumn(name="permissao_id"))
	private List<Permissao> permissoes;
	
	public Long getTelasecurity_id() {
		return telasecurity_id;
	}
	public void setTelasecurity_id(Long telasecurity_id) {
		this.telasecurity_id = telasecurity_id;
	}
	public TelaSistema getTelasistema() {
		return telasistema;
	}
	public void setTelasistema(TelaSistema telasistema) {
		this.telasistema = telasistema;
	}
	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telasecurity_id == null) ? 0 : telasecurity_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelaSecurity other = (TelaSecurity) obj;
		if (telasecurity_id == null) {
			if (other.telasecurity_id != null)
				return false;
		} else if (!telasecurity_id.equals(other.telasecurity_id))
			return false;
		return true;
	}
	
}
