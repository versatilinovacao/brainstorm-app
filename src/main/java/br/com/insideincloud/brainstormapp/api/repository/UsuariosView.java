package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.UsuarioView;

@Repository
public interface UsuariosView extends JpaRepository<UsuarioView,Long> {
	
	@Query("select u from UsuarioView u where u.colaborador_id = ?1 and u.composicao_colaborador_id = ?2")
	public UsuarioView findByUsuarioProfessor(Long professor_id, Long composicao_professor_id);
	
	@Query("select u from UsuarioView u where u.colaborador_id = ?1 and u.composicao_colaborador_id = ?2")
	public UsuarioView findByUsuarioAluno_id(Long aluno_id, Long composicao_aluno_id);

}
