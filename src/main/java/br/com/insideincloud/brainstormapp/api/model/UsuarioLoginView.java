package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorEmpresaResumoView;

@Entity
@Table(name="usuariosview")
public class UsuarioLoginView implements Serializable {
	private static final long serialVersionUID = 189994838423637872L;

	@Id
	private Long usuario_id;
	private Long colaborador_id;
	private Long composicao_id;
	@Column(name="nome")
	private String nomecompleto;
	private String senha;
	private String email;
	private Boolean status;
	private Boolean isserver;
	private Boolean isclient;
	private Boolean iscounter;
	private Boolean issistema;
	private Boolean isprofessor;
	private LocalDate nascimento;
	private String foto_thumbnail;
	@OneToOne
	@JoinColumn(name="empresa_id")
	private ColaboradorEmpresaResumoView empresa;
	private Long cargo_id;
	@Column(name="descricao_cargo")
	private String descricaocargo;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="usuario_permissao",joinColumns = @JoinColumn(name="usuario_id"), inverseJoinColumns = @JoinColumn(name="permissao_id"))
	private List<Permissao> permissoes;	
		
	public Boolean getIsprofessor() {
		return isprofessor;
	}
	public void setIsprofessor(Boolean isprofessor) {
		this.isprofessor = isprofessor;
	}
	public Boolean getIssistema() {
		return issistema;
	}
	public void setIssistema(Boolean issistema) {
		this.issistema = issistema;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
	public Long getCargo_id() {
		return cargo_id;
	}
	public void setCargo_id(Long cargo_id) {
		this.cargo_id = cargo_id;
	}
	public String getDescricaocargo() {
		return descricaocargo;
	}
	public void setDescricaocargo(String descricaocargo) {
		this.descricaocargo = descricaocargo;
	}
	public ColaboradorEmpresaResumoView getEmpresa() {
		return empresa;
	}
	public void setEmpresa(ColaboradorEmpresaResumoView empresa) {
		this.empresa = empresa;
	}
	public String getFoto_thumbnail() {
		return foto_thumbnail;
	}
	public void setFoto_thumbnail(String foto_thumbnail) {
		this.foto_thumbnail = foto_thumbnail;
	}
	public List<Permissao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public Long getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(Long usuario_id) {
		this.usuario_id = usuario_id;
	}
	public String getNomecompleto() {
		return nomecompleto;
	}
	public void setNomecompleto(String nomecompleto) {
		this.nomecompleto = nomecompleto;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Boolean getIsserver() {
		return isserver;
	}
	public void setIsserver(Boolean isserver) {
		this.isserver = isserver;
	}
	public Boolean getIsclient() {
		return isclient;
	}
	public void setIsclient(Boolean isclient) {
		this.isclient = isclient;
	}
	public Boolean getIscounter() {
		return iscounter;
	}
	public void setIscounter(Boolean iscounter) {
		this.iscounter = iscounter;
	}
	public LocalDate getNascimento() {
		return nascimento;
	}
	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((usuario_id == null) ? 0 : usuario_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioLoginView other = (UsuarioLoginView) obj;
		if (usuario_id == null) {
			if (other.usuario_id != null)
				return false;
		} else if (!usuario_id.equals(other.usuario_id))
			return false;
		return true;
	}

}
