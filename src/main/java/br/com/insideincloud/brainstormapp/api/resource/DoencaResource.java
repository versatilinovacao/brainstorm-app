package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Doenca;
import br.com.insideincloud.brainstormapp.api.repository.Doencas;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/doencas")
public class DoencaResource {

	@Autowired
	private Doencas doencas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_DOENCA_CONTEUDO')")
	public RetornoWrapper<Doenca> conteudo() {
		RetornoWrapper<Doenca> retorno = new RetornoWrapper<Doenca>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( doencas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhuma doença.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{doenca_id}")
	@PreAuthorize("hasAuthority('ROLE_DOENCA_CONTEUDO')")
	public RetornoWrapper<Doenca> conteudoPorId(@PathVariable Long doenca_id) {
		RetornoWrapper<Doenca> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( doencas.findOne(doenca_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar a doença pesquisada.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
}
