package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cargos")
public class Cargo implements Serializable {
	private static final long serialVersionUID = -2103362257694704086L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cargo_id;
	@Column(name="descricao")
	private String descricao;
	private Boolean status;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getCargo_id() {
		return cargo_id;
	}
	public void setCargo_id(Long cargo_id) {
		this.cargo_id = cargo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargo_id == null) ? 0 : cargo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cargo other = (Cargo) obj;
		if (cargo_id == null) {
			if (other.cargo_id != null)
				return false;
		} else if (!cargo_id.equals(other.cargo_id))
			return false;
		return true;
	}
	
	
	
}
