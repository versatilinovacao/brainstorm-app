package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.GradeCurricularInativaView;

@Repository
public interface GradesCurricularesInativasView extends JpaRepository<GradeCurricularInativaView, Long> {

	@Query("select g from GradeCurricularInativaView g where g.curso_id = ?1")
	public List<GradeCurricularInativaView> findByGradePorCurso(Long curso_id);
}
