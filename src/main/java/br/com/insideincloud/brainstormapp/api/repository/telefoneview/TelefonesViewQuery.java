package br.com.insideincloud.brainstormapp.api.repository.telefoneview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.TelefoneView;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneViewFilter;

public interface TelefonesViewQuery {
	public Page<TelefoneView> filtrar(TelefoneViewFilter filtro,Pageable page);

}
