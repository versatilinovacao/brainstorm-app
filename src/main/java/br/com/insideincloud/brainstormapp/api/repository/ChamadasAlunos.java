package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.ChamadaAluno;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaAlunoFilter;

@Repository
public interface ChamadasAlunos extends JpaRepository<ChamadaAluno,Long> {
	public Page<ChamadaAluno> filtrar(ChamadaAlunoFilter filtro, Pageable page);
}
