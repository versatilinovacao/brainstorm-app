package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="dispositivos",schema="comunicacao")
public class Dispositivo implements Serializable {
	private static final long serialVersionUID = -1570045217309992427L;
	
	@Id
	@JoinColumn(name="dispositivo_id")
	private Long dispositivo_id;
	private String nome;
	
	public Long getDispositivo_id() {
		return dispositivo_id;
	}
	public void setDispositivo_id(Long dispositivo_id) {
		this.dispositivo_id = dispositivo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dispositivo_id == null) ? 0 : dispositivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dispositivo other = (Dispositivo) obj;
		if (dispositivo_id == null) {
			if (other.dispositivo_id != null)
				return false;
		} else if (!dispositivo_id.equals(other.dispositivo_id))
			return false;
		return true;
	}
}
