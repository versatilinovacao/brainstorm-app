package br.com.insideincloud.brainstormapp.api.repository.pais;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Pais;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.PaisFilter;

public interface PaisesQuery {
	public Page<Pais> filtrar(PaisFilter filtro, Pageable page);

}

