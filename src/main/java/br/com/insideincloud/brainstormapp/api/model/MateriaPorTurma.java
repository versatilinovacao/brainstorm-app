package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalTime;

public class MateriaPorTurma implements Serializable {
	private static final long serialVersionUID = -1773428639819454435L;

	private Long materiaporturma_id;
	private Materia materia;
//	private Turma turma;
	private Boolean segunda;
	private Boolean terca;
	private Boolean quarta;
	private Boolean quinta;
	private Boolean sexta;
	private Boolean sabado;
	private Boolean domingo;
	private LocalTime entradasegunda;
	private LocalTime saidasegunda;
	private LocalTime entradaterca;
	private LocalTime saidaterca;
	private LocalTime entradaquarta;
	private LocalTime saidaquarta;
	private LocalTime entradaquinta;
	private LocalTime saidaquinta;
	private LocalTime entradasexta;
	private LocalTime saidasexta;
	private LocalTime entradasabado;
	private LocalTime saidasabado;
	private LocalTime entradadomindo;
	private LocalTime saidadomingo;
	
	public Long getMateriaporturma_id() {
		return materiaporturma_id;
	}
	public void setMateriaporturma_id(Long materiaporturma_id) {
		this.materiaporturma_id = materiaporturma_id;
	}
	public Materia getMateria() {
		return materia;
	}
	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	public Boolean getSegunda() {
		return segunda;
	}
	public void setSegunda(Boolean segunda) {
		this.segunda = segunda;
	}
	public Boolean getTerca() {
		return terca;
	}
	public void setTerca(Boolean terca) {
		this.terca = terca;
	}
	public Boolean getQuarta() {
		return quarta;
	}
	public void setQuarta(Boolean quarta) {
		this.quarta = quarta;
	}
	public Boolean getQuinta() {
		return quinta;
	}
	public void setQuinta(Boolean quinta) {
		this.quinta = quinta;
	}
	public Boolean getSexta() {
		return sexta;
	}
	public void setSexta(Boolean sexta) {
		this.sexta = sexta;
	}
	public Boolean getSabado() {
		return sabado;
	}
	public void setSabado(Boolean sabado) {
		this.sabado = sabado;
	}
	public Boolean getDomingo() {
		return domingo;
	}
	public void setDomingo(Boolean domingo) {
		this.domingo = domingo;
	}
	public LocalTime getEntradasegunda() {
		return entradasegunda;
	}
	public void setEntradasegunda(LocalTime entradasegunda) {
		this.entradasegunda = entradasegunda;
	}
	public LocalTime getSaidasegunda() {
		return saidasegunda;
	}
	public void setSaidasegunda(LocalTime saidasegunda) {
		this.saidasegunda = saidasegunda;
	}
	public LocalTime getEntradaterca() {
		return entradaterca;
	}
	public void setEntradaterca(LocalTime entradaterca) {
		this.entradaterca = entradaterca;
	}
	public LocalTime getSaidaterca() {
		return saidaterca;
	}
	public void setSaidaterca(LocalTime saidaterca) {
		this.saidaterca = saidaterca;
	}
	public LocalTime getEntradaquarta() {
		return entradaquarta;
	}
	public void setEntradaquarta(LocalTime entradaquarta) {
		this.entradaquarta = entradaquarta;
	}
	public LocalTime getSaidaquarta() {
		return saidaquarta;
	}
	public void setSaidaquarta(LocalTime saidaquarta) {
		this.saidaquarta = saidaquarta;
	}
	public LocalTime getEntradaquinta() {
		return entradaquinta;
	}
	public void setEntradaquinta(LocalTime entradaquinta) {
		this.entradaquinta = entradaquinta;
	}
	public LocalTime getSaidaquinta() {
		return saidaquinta;
	}
	public void setSaidaquinta(LocalTime saidaquinta) {
		this.saidaquinta = saidaquinta;
	}
	public LocalTime getEntradasexta() {
		return entradasexta;
	}
	public void setEntradasexta(LocalTime entradasexta) {
		this.entradasexta = entradasexta;
	}
	public LocalTime getSaidasexta() {
		return saidasexta;
	}
	public void setSaidasexta(LocalTime saidasexta) {
		this.saidasexta = saidasexta;
	}
	public LocalTime getEntradasabado() {
		return entradasabado;
	}
	public void setEntradasabado(LocalTime entradasabado) {
		this.entradasabado = entradasabado;
	}
	public LocalTime getSaidasabado() {
		return saidasabado;
	}
	public void setSaidasabado(LocalTime saidasabado) {
		this.saidasabado = saidasabado;
	}
	public LocalTime getEntradadomindo() {
		return entradadomindo;
	}
	public void setEntradadomindo(LocalTime entradadomindo) {
		this.entradadomindo = entradadomindo;
	}
	public LocalTime getSaidadomingo() {
		return saidadomingo;
	}
	public void setSaidadomingo(LocalTime saidadomingo) {
		this.saidadomingo = saidadomingo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materiaporturma_id == null) ? 0 : materiaporturma_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MateriaPorTurma other = (MateriaPorTurma) obj;
		if (materiaporturma_id == null) {
			if (other.materiaporturma_id != null)
				return false;
		} else if (!materiaporturma_id.equals(other.materiaporturma_id))
			return false;
		return true;
	}
	
	
}
