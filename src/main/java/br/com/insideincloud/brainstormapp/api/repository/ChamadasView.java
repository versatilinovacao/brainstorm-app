package br.com.insideincloud.brainstormapp.api.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaView;
import br.com.insideincloud.brainstormapp.api.repository.chamadaview.ChamadasViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaViewFilter;

@Repository
public interface ChamadasView extends JpaRepository<ChamadaView,Long>,ChamadasViewQuery {
	public Page<ChamadaView> filtrar(ChamadaViewFilter filtro, Pageable page);
	
	@Query("select c from ChamadaView c where c.caderno_id = ?1 and c.evento = ?2")
	public List<ChamadaView> findByChamadas(Long caderno_id,LocalDate evento);
}
