package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="prioridades")
public class Prioridade implements Serializable {
	private static final long serialVersionUID = 3500594061772309134L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long prioridade_id;
	@Column(name="descricao",length=100)
	private String descricao;
	@Column(name="cor")
	private String cor;
	
	
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public Long getPrioridade_id() {
		return prioridade_id;
	}
	public void setPrioridade_id(Long prioridade_id) {
		this.prioridade_id = prioridade_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prioridade_id == null) ? 0 : prioridade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prioridade other = (Prioridade) obj;
		if (prioridade_id == null) {
			if (other.prioridade_id != null)
				return false;
		} else if (!prioridade_id.equals(other.prioridade_id))
			return false;
		return true;
	}
	
	
}
