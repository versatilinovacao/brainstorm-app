package br.com.insideincloud.brainstormapp.api.repository.filter;

public class MatriculaAlunoViewFilter {
	private String turma_id;
	private String aluno_id;
	private String alunonome;
	private String matricula;
	private String curso_id;
	
	public String getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(String curso_id) {
		this.curso_id = curso_id;
	}
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(String aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getAlunonome() {
		return alunonome;
	}
	public void setAlunonome(String alunonome) {
		this.alunonome = alunonome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	
}
