package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mensagems_alunos_professor")
public class MensagemAlunoProfessor implements Serializable {
	private static final long serialVersionUID = 3881546289219144570L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long mensagemaluno_id;
	@OneToOne
	@JoinColumn(name="aluno_id")
	private UsuarioLoginView aluno;
	@OneToOne
	@JoinColumn(name="professor_id")
	private UsuarioLoginView professor;
	private String mensagem;
	private LocalDateTime registro;
	private ComunicacaoAlunoProfessor emissor;

	public ComunicacaoAlunoProfessor getEmissor() {
		return emissor;
	}
	public void setEmissor(ComunicacaoAlunoProfessor emissor) {
		this.emissor = emissor;
	}
	public UsuarioLoginView getAluno() {
		return aluno;
	}
	public void setAluno(UsuarioLoginView aluno) {
		this.aluno = aluno;
	}
	public UsuarioLoginView getProfessor() {
		return professor;
	}
	public void setProfessor(UsuarioLoginView professor) {
		this.professor = professor;
	}
	public Long getMensagemaluno_id() {
		return mensagemaluno_id;
	}
	public void setMensagemaluno_id(Long mensagemaluno_id) {
		this.mensagemaluno_id = mensagemaluno_id;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public LocalDateTime getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDateTime registro) {
		this.registro = registro;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mensagemaluno_id == null) ? 0 : mensagemaluno_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MensagemAlunoProfessor other = (MensagemAlunoProfessor) obj;
		if (mensagemaluno_id == null) {
			if (other.mensagemaluno_id != null)
				return false;
		} else if (!mensagemaluno_id.equals(other.mensagemaluno_id))
			return false;
		return true;
	}

}
