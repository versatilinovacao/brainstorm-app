package br.com.insideincloud.brainstormapp.api.model.list;

import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.Contato;

public class ContatoList {
	private List<Contato> transaction;

	public List<Contato> getTransaction() {
		return transaction;
	}

	public void setTransaction(List<Contato> transaction) {
		this.transaction = transaction;
	}
	
}
