package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.AlunoVinculadoTurmaView;

@Repository
public interface AlunosVinculadosTurmasView extends JpaRepository<AlunoVinculadoTurmaView,Long> {
	List<AlunoVinculadoTurmaView> findByNomeStartingWithOrderByNome(String nome);
	@Async
	@Query("FROM AlunoVinculadoTurmaView a WHERE a.turma_id = ?1")
	List<AlunoVinculadoTurmaView> findByTurma_id(Long turma_id);
//	List<AlunoVinculadoTurmaView> findByColaborador_id(Long colaborador_id);

}
