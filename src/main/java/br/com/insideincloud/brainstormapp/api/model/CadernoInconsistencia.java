package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cadernos_inconsistencias_find",schema="escola")
public class CadernoInconsistencia implements Serializable {
	private static final long serialVersionUID = 6303962602923886397L;

	@Id	
	private Long caderno_inconsistencia_id;
	private Long caderno_id;
	private String resumo;
	private String descricao;
	private boolean status;
	
	public Long getCaderno_inconsistencia_id() {
		return caderno_inconsistencia_id;
	}
	public void setCaderno_inconsistencia_id(Long caderno_inconsistencia_id) {
		this.caderno_inconsistencia_id = caderno_inconsistencia_id;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caderno_inconsistencia_id == null) ? 0 : caderno_inconsistencia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadernoInconsistencia other = (CadernoInconsistencia) obj;
		if (caderno_inconsistencia_id == null) {
			if (other.caderno_inconsistencia_id != null)
				return false;
		} else if (!caderno_inconsistencia_id.equals(other.caderno_inconsistencia_id))
			return false;
		return true;
	}
	
	
}
