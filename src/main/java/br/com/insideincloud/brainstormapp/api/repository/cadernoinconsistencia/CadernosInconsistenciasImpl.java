package br.com.insideincloud.brainstormapp.api.repository.cadernoinconsistencia;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.CadernoInconsistencia;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoInconsistenciaFilter;

public class CadernosInconsistenciasImpl implements CadernosInconsistenciasQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<CadernoInconsistencia> filtrar(CadernoInconsistenciaFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<CadernoInconsistencia> criteria = builder.createQuery(CadernoInconsistencia.class);
		Root<CadernoInconsistencia> root = criteria.from(CadernoInconsistencia.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<CadernoInconsistencia> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(CadernoInconsistenciaFilter filtro, CriteriaBuilder builder, Root<CadernoInconsistencia> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		System.out.println("ESTOU AQUI >>>>>>>>>>>>>>>>>>>>>>>>>>>>XXXXXXXXXXXXXXXXXX = "+filtro.getCaderno_id()+" - "+filtro.getCaderno_inconsistencia_id());
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getCaderno_inconsistencia_id())) {
				predicates.add(builder.equal(root.get("caderno_inconsistencia_id"), Long.parseLong(filtro.getCaderno_inconsistencia_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getResumo())) {
				predicates.add(builder.like(builder.lower(root.get("resumo")), "%" + filtro.getResumo().toLowerCase() + "%"));
			}

//			if (!StringUtils.isEmpty(filtro.getMatricula())) {
//				predicates.add(builder.like(builder.lower(root.get("matricula")), "%" + filtro.getMatricula().toLowerCase() + "%"));
//			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(CadernoInconsistenciaFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<CadernoInconsistencia> root = criteria.from(CadernoInconsistencia.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
