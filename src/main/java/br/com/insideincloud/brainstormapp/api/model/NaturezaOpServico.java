package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.minor.CidadeMinor;

@Entity
@Table(name="naturezaopservicos",schema="contabil")
public class NaturezaOpServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long naturezaopservico_id;

	private Double referencia;
	private String titulo;
	private String descricao;
	

	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="opservicos_cidades",joinColumns = @JoinColumn(name="naturezaopservico_id"), inverseJoinColumns = @JoinColumn(name="cidade_id"))
	private List<CidadeMinor> cidades;


	public Long getNaturezaopservico_id() {
		return naturezaopservico_id;
	}
	public void setNaturezaopservico_id(Long naturezaopservico_id) {
		this.naturezaopservico_id = naturezaopservico_id;
	}
	public Double getReferencia() {
		return referencia;
	}
	public void setReferencia(Double referencia) {
		this.referencia = referencia;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<CidadeMinor> getCidades() {
		return cidades;
	}
	public void setCidades(List<CidadeMinor> cidades) {
		this.cidades = cidades;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((naturezaopservico_id == null) ? 0 : naturezaopservico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NaturezaOpServico other = (NaturezaOpServico) obj;
		if (naturezaopservico_id == null) {
			if (other.naturezaopservico_id != null)
				return false;
		} else if (!naturezaopservico_id.equals(other.naturezaopservico_id))
			return false;
		return true;
	}
	
	
}


