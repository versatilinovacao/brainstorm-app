package br.com.insideincloud.brainstormapp.api.repository.colaborador;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.Colaborador;
import br.com.insideincloud.brainstormapp.api.model.Colaborador_;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorFilter;
import br.com.insideincloud.brainstormapp.api.repository.projection.ResumoColaborador;

public class ColaboradoresImpl implements ColaboradoresQuery {
	
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Colaborador> filtrar(ColaboradorFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Colaborador> criteria = builder.createQuery(Colaborador.class);
		Root<Colaborador> root = criteria.from(Colaborador.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<Colaborador> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));
	}
	
	@Override
	public Page<ResumoColaborador> resumir(ColaboradorFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoColaborador> criteria = builder.createQuery(ResumoColaborador.class);
		Root<Colaborador> root = criteria.from(Colaborador.class);
		
		criteria.select(builder.construct(ResumoColaborador.class 
					,root.get(Colaborador_.colaborador_id), root.get(Colaborador_.nome)
					,root.get(Colaborador_.fantasia) ));
		
		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoColaborador> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, page);
		
		return new PageImpl<>(query.getResultList(), page, total(filtro));
		
	}
	
	private Predicate[] criarRestricoes(ColaboradorFilter filtro, CriteriaBuilder builder, Root<Colaborador> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if(filtro != null) {
//			if (!StringUtils.isEmpty(filtro.getNotcontatos())) {
//				predicates.add(builder.neg(arg0));
//			} 
			
//			if (!StringUtils.isEmpty(filtro.getAowner())) {
//				predicates.add( builder.in(root.get("colaborador_id").in(Long.parseLong(filtro.getAowner())).not() ) );				
//			}
				
//			if (filtro.getColaborador_id() != null && filtro.getColaborador_id() > 0) {
			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), filtro.getColaborador_id()));
		//			predicates.add(builder.equal(root.get(Colaborador_.colaborador_id), filtro.getColaborador_id()));
			}
			
			if (!StringUtils.isEmpty(filtro.getNome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
		//			predicates.add(builder.like(builder.lower(root.get(Colaborador_.nome)), "%" + filtro.getNome().toLowerCase() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getFantasia())) {
				predicates.add(builder.like(builder.lower(root.get("fantasia")), "%" + filtro.getFantasia() + "%"));
		//			predicates.add(builder.like(builder.lower(root.get(Colaborador_.fantasia)), "%" + filtro.getFantasia() + "%"));
			}
			
			if (!StringUtils.isEmpty(filtro.getReferencia())) {
				predicates.add(builder.equal(root.get("referencia"), filtro.getReferencia()));
			}
	}
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ColaboradorFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Colaborador> root = criteria.from(Colaborador.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}
