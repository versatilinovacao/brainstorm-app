package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="parceiros")
public class Parceiro implements Serializable {
	private static final long serialVersionUID = 3960255406332104861L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long parceiro_id;
	@OneToOne
	@JoinColumn(name="cargo_id")
	private Cargo cargo;
	
	@OneToOne
	@JoinColumn(name="especializacao_id")
	private Especialidade especializacao;
	
	public Especialidade getEspecializacao() {
		return especializacao;
	}
	public void setEspecializacao(Especialidade especializacao) {
		this.especializacao = especializacao;
	}
	public Long getParceiro_id() {
		return parceiro_id;
	}
	public void setParceiro_id(Long parceiro_id) {
		this.parceiro_id = parceiro_id;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parceiro_id == null) ? 0 : parceiro_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parceiro other = (Parceiro) obj;
		if (parceiro_id == null) {
			if (other.parceiro_id != null)
				return false;
		} else if (!parceiro_id.equals(other.parceiro_id))
			return false;
		return true;
	}

	
}
