package br.com.insideincloud.brainstormapp.api.repository.periodoletivoview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoView;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoViewFilter;

public interface PeriodosLetivosViewQuery {
	public Page<PeriodoLetivoView> filtrar(PeriodoLetivoViewFilter filtro, Pageable page);
}
