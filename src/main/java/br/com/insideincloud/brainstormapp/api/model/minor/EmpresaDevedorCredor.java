package br.com.insideincloud.brainstormapp.api.model.minor;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;

@Entity
@Table(name="colaborador",schema="public")
public class EmpresaDevedorCredor implements Serializable {
	private static final long serialVersionUID = -4640873119110025594L;

	@Id
	private ColaboradorPK empresa_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="fantasia",length=100)
	private String fantasia;
	
	public ColaboradorPK getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(ColaboradorPK empresa_id) {
		this.empresa_id = empresa_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFantasia() {
		return fantasia;
	}
	public void setFantasia(String fantasia) {
		this.fantasia = fantasia;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empresa_id == null) ? 0 : empresa_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaDevedorCredor other = (EmpresaDevedorCredor) obj;
		if (empresa_id == null) {
			if (other.empresa_id != null)
				return false;
		} else if (!empresa_id.equals(other.empresa_id))
			return false;
		return true;
	}
	
}
