package br.com.insideincloud.brainstormapp.api.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.repository.Empresas;

@Component
public class Sistema {
	@Autowired
	private Empresas empresas;
	
	private Long empresa_id;
	private Long composicao_empresa_id;
	private String diretorio;
	private String logomarca;
	
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	public Long getComposicao_empresa_id() {
		return composicao_empresa_id;
	}
	public void setComposicao_empresa_id(Long composicao_empresa_id) {
		this.composicao_empresa_id = composicao_empresa_id;
	}
	public String getDiretorio() {
		return diretorio;
	}
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	public String getLogomarca() {
		return logomarca;
	}
	public void setLogomarca(String logomarca) {
		this.logomarca = logomarca;
	}
	

}
