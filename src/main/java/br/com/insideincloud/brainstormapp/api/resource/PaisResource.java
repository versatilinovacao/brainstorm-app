package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Pais;
import br.com.insideincloud.brainstormapp.api.repository.Paises;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/paises")
public class PaisResource {

	@Autowired 
	private Paises paises;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_LOGRADOURO_CONTEUDO')")
	public RetornoWrapper<Pais> conteudo() {
		RetornoWrapper<Pais> retorno = new RetornoWrapper<>();
		ExceptionWrapper erro = new ExceptionWrapper();
		
		try { 
			retorno.setConteudo(paises.findAll());
		} catch(Exception e) {
			erro.setCodigo(1);
			erro.setMensagem("Não foi possível apresentar as informações solicitadas, favor tentar novamente em alguns instantes!");
			erro.setDebug(""+e);
			retorno.setException(erro);
		}
		
		return retorno;		
	}
	
//	@PostMapping
//	public ResponseEntity<PaisRemotoView> salvar(@RequestBody PaisRemotoView pais, HttpServletResponse response) {
//		pais = paises.save(pais);
//		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(pais.getPais_id()).toUri();
//		response.setHeader("Location", uri.toASCIIString());
//
//		return ResponseEntity.created(uri).body(pais);		
//	}
//	
//	@GetMapping("/{codigo}")
////	@PreAuthorize("hasAuthority('ROLE_COLABORADOR_PESQUISAR')")
//	public PaisRemotoView buscarPeloCodigo(@PathVariable Long codigo) {
//		return paises.findOne(codigo);
//	}
	
}
