package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Curso;
import br.com.insideincloud.brainstormapp.api.repository.curso.CursoQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.CursoFilter;

@Repository
public interface Cursos extends JpaRepository<Curso, Long>, CursoQuery {
	public Page<Curso> filtrar(CursoFilter filtro, Pageable page); 

}
