package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.servicos.ServicoPortoAlegre;

@Repository
public interface ServicosPortoAlegre extends JpaRepository<ServicoPortoAlegre, Long> {

}
