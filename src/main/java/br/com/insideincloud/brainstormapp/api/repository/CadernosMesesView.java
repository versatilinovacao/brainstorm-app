package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.CadernoMesView;
import br.com.insideincloud.brainstormapp.api.repository.cadernomesview.CadernosMesesViewQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoMesViewFilter;

@Repository
public interface CadernosMesesView extends JpaRepository<CadernoMesView,Long>, CadernosMesesViewQuery {
	public Page<CadernoMesView> filtrar(CadernoMesViewFilter filtro, Pageable page);

}
