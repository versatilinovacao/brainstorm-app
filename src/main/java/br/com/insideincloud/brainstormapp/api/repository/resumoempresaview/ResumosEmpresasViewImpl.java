package br.com.insideincloud.brainstormapp.api.repository.resumoempresaview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.ResumoEmpresaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ResumoEmpresaViewFilter;

public class ResumosEmpresasViewImpl implements ResumosEmpresasViewQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ResumoEmpresaView> filtrar(ResumoEmpresaViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoEmpresaView> criteria = builder.createQuery(ResumoEmpresaView.class);
		Root<ResumoEmpresaView> root = criteria.from(ResumoEmpresaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ResumoEmpresaView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ResumoEmpresaViewFilter filtro, CriteriaBuilder builder, Root<ResumoEmpresaView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getEscola_id())) {
				predicates.add(builder.equal(root.get("escola_id"), Long.parseLong(filtro.getEscola_id())));
			}		
			if (!StringUtils.isEmpty(filtro.getNome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ResumoEmpresaViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ResumoEmpresaView> root = criteria.from(ResumoEmpresaView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
