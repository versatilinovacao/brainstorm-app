package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="especializacoes")
public class Especialidade implements Serializable {
	private static final long serialVersionUID = 4908764047479852544L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long especializacao_id;
	@Column(name="descricao")
	private String descricao;
	private boolean status;
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Long getEspecializacao_id() {
		return especializacao_id;
	}
	public void setEspecializacao_id(Long especializacao_id) {
		this.especializacao_id = especializacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((especializacao_id == null) ? 0 : especializacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Especialidade other = (Especialidade) obj;
		if (especializacao_id == null) {
			if (other.especializacao_id != null)
				return false;
		} else if (!especializacao_id.equals(other.especializacao_id))
			return false;
		return true;
	}
		
}
