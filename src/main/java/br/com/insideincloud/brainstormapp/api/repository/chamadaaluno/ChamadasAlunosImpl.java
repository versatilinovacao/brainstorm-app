package br.com.insideincloud.brainstormapp.api.repository.chamadaaluno;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.ChamadaAluno;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaAlunoFilter;

public class ChamadasAlunosImpl implements ChamadasAlunosQuery {
	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ChamadaAluno> filtrar(ChamadaAlunoFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ChamadaAluno> criteria = builder.createQuery(ChamadaAluno.class);
		Root<ChamadaAluno> root = criteria.from(ChamadaAluno.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ChamadaAluno> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ChamadaAlunoFilter filtro, CriteriaBuilder builder, Root<ChamadaAluno> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		
//		if (filtro.getPais_id() > 0) {
//			predicates.add(builder.equal(root.get(Pais_.pais_id), filtro.getPais_id()));
//		}
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getCaderno_id())) {
				predicates.add(builder.equal(root.get("caderno").get("caderno_id"), Long.parseLong(filtro.getCaderno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getEvento())) {
				predicates.add(builder.between(root.get("evento").as(Date.class), Date.valueOf(filtro.getEvento()), Date.valueOf(filtro.getEvento())));
			}
			
//			if (!StringUtils.isEmpty(filtro.getDescricao())) {
//				predicates.add(builder.like(builder.lower(root.get("descricao")), "%" + filtro.getDescricao().toLowerCase() + "%"));
//			}

			if (!StringUtils.isEmpty(filtro.getChamada_aluno_id())) {
				predicates.add(builder.equal(root.get("chamada_aluno_id"), Long.parseLong(filtro.getChamada_aluno_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getChamada_id())) {
				predicates.add(builder.equal(root.get("chamada_id"), Long.parseLong(filtro.getChamada_id())));
			}		
			
//			if (!StringUtils.isEmpty(filtro.getCodigo())) {
//				predicates.add(builder.like(builder.lower(root.get("matricula")), "%" + filtro.getCodigo().toLowerCase() + "%"));
//			}
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ChamadaAlunoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ChamadaAluno> root = criteria.from(ChamadaAluno.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
}
