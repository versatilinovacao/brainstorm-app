package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Contato;
import br.com.insideincloud.brainstormapp.api.repository.contato.ContatosQuery;

@Repository
public interface Contatos extends JpaRepository<Contato,Long>, ContatosQuery {

}
