package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.io.Serializable;
import java.util.List;

public class CadernoPeriodo implements Serializable {
	private static final long serialVersionUID = -3167568111030307032L;

	private Long caderno_id;
	private String mes;
	private String ano;
	private String nomecurso;
	private String nometurma;
	private String mesporextenso;
	private List<CadernoFrequencias> frequenciasalunos;
	
	public Long getCaderno_id() {
		if (caderno_id == null) { this.caderno_id = 0L; }
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		if (ano == null) { this.ano = ""; }
		return ano;
	}
	public void setAno(String ano) {
		if (ano == null) { this.ano = ""; }
		this.ano = ano;
	}
	public String getNomecurso() {
		return nomecurso;
	}
	public void setNomecurso(String nomecurso) {
		this.nomecurso = nomecurso;
	}
	public String getNometurma() {
		return nometurma;
	}
	public void setNometurma(String nometurma) {
		this.nometurma = nometurma;
	}
	public String getMesporextenso() {
		return mesporextenso;
	}
	public void setMesporextenso(String mesporextenso) {
		this.mesporextenso = mesporextenso;
	}
	public List<CadernoFrequencias> getFrequenciasalunos() {
		return frequenciasalunos;
	}
	public void setFrequenciasalunos(List<CadernoFrequencias> frequenciasalunos) {
		this.frequenciasalunos = frequenciasalunos;
	}
	
}
