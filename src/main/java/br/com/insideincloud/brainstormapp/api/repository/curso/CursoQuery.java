package br.com.insideincloud.brainstormapp.api.repository.curso;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Curso;
import br.com.insideincloud.brainstormapp.api.repository.filter.CursoFilter;

public interface CursoQuery {
	public Page<Curso> filtrar(CursoFilter filtro, Pageable page);

}
