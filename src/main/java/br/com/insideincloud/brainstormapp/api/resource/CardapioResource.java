package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Cardapio;
import br.com.insideincloud.brainstormapp.api.model.CardapioSemana;
import br.com.insideincloud.brainstormapp.api.model.view.CardapioAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.CardapioInativoView;
import br.com.insideincloud.brainstormapp.api.repository.Cardapios;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosInativosView;
import br.com.insideincloud.brainstormapp.api.repository.CardapiosSemanas;
import br.com.insideincloud.brainstormapp.api.resource.delete.CardapiosSemanasDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/cardapios")
public class CardapioResource {

	@Autowired
	private Cardapios cardapios;
	
	@Autowired
	private CardapiosAtivosView cardapiosAtivos;
	
	@Autowired
	private CardapiosInativosView cardapiosInativos;
	
	@Autowired
	private CardapiosSemanas cardapiosSemanas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_CONTEUDO')")
	public RetornoWrapper<CardapioAtivoView> conteudoAtivo() {
		RetornoWrapper<CardapioAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cardapiosAtivos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível listar os cardapios ativos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_CONTEUDO')")
	public RetornoWrapper<CardapioInativoView> conteudoInativo() {
		RetornoWrapper<CardapioInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cardapiosInativos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível listar os cardapios ativos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_CONTEUDO')")
	public RetornoWrapper<Cardapio> conteudoPorId(@PathVariable Long codigo) {
		RetornoWrapper<Cardapio> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( cardapios.findOne(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o cardapio, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_SALVAR')")
	public RetornoWrapper<Cardapio> salvar(@RequestBody Cardapio cardapio ) {
		RetornoWrapper<Cardapio> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( cardapios.saveAndFlush(cardapio) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o cardapio, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/semanas/{cardapio_id}")
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_CONTEUDO')")
	public RetornoWrapper<CardapioSemana> conteudoCardapioSemana(@PathVariable Long cardapio_id) {
		RetornoWrapper<CardapioSemana> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( cardapiosSemanas.findBySemanaPorCardapio( cardapio_id ) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar as semanas dos cardapios, favor tentar novamente dentro de aluns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/semanas/{id}")
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_CONTEUDO')")
	public RetornoWrapper<CardapioSemana> conteudoCardapioSemanas(@PathVariable Long id) {
		RetornoWrapper<CardapioSemana> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( cardapiosSemanas.findOne(id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar a semana do cardapio, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/deletar")
	@PreAuthorize("hasAuthority('ROLE_CARDAPIO_DELETAR')")
	public RetornoWrapper<CardapioSemana> deletar(@RequestBody CardapiosSemanasDelete delete) {
		RetornoWrapper<CardapioSemana> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			delete.getSemanas().forEach(conteudo -> {
				cardapiosSemanas.delete(conteudo.getCardapiosemana_id());				
			});
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível aplicar o delete, favor verificar e tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		retorno.setConteudo( cardapiosSemanas.findBySemanaPorCardapio( delete.getCardapio_id() ) );
		return retorno;
	}
	
}
