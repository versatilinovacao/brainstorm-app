package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorParceiroView;

@Repository
public interface ColaboradoresParceirosView extends JpaRepository<ColaboradorParceiroView, Long> {
	
	@Query("select p from ColaboradorParceiroView p where p.parceiro_id = ?1 and p.composicaoparceiro_id = ?2")
	public List<ColaboradorParceiroView> findByColaborador(Long parceiro_id, Long composicaoparceiro_id);

}
