package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sistemas",schema="insideincloud")
public class Sistema implements Serializable {
	private static final long serialVersionUID = -9123021498523362764L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long sistema_id;
	private String nome;
	private String url;
	
	public Long getSistema_id() {
		return sistema_id;
	}
	public void setSistema_id(Long sistema_id) {
		this.sistema_id = sistema_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sistema_id == null) ? 0 : sistema_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sistema other = (Sistema) obj;
		if (sistema_id == null) {
			if (other.sistema_id != null)
				return false;
		} else if (!sistema_id.equals(other.sistema_id))
			return false;
		return true;
	}
	
}
