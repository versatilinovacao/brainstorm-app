package br.com.insideincloud.brainstormapp.api.repository.filter;

public class TelefoneColaboradorViewFilter {
	
	private String telefone_id;
	private String colaborador_id;
	private String composicao_colaborador_id;
	private String numero;
	private String tipo;
	private String ramal;
	
	public String getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(String colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getComposicao_colaborador_id() {
		return composicao_colaborador_id;
	}
	public void setComposicao_colaborador_id(String composicao_colaborador_id) {
		this.composicao_colaborador_id = composicao_colaborador_id;
	}
	public String getTelefone_id() {
		return telefone_id;
	}
	public void setTelefone_id(String telefone_id) {
		this.telefone_id = telefone_id;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getRamal() {
		return ramal;
	}
	public void setRamal(String ramal) {
		this.ramal = ramal;
	}

}
