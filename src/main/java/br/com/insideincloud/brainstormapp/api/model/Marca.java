package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="marcas",schema="recurso")
public class Marca implements Serializable {
	private static final long serialVersionUID = -1330672400029993215L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long marca_id;
	@Column(name="nome",length=100)	
	private String nome;
	
	public Long getMarca_id() {
		return marca_id;
	}
	public void setMarca_id(Long marca_id) {
		this.marca_id = marca_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca_id == null) ? 0 : marca_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marca other = (Marca) obj;
		if (marca_id == null) {
			if (other.marca_id != null)
				return false;
		} else if (!marca_id.equals(other.marca_id))
			return false;
		return true;
	}
	
	
}
