package br.com.insideincloud.brainstormapp.api.model;

import java.time.LocalDate;

import br.com.insideincloud.brainstormapp.api.model.view.ProvisorView;

public class Fatura {

	private Long fatura_id;
	private Long composicao_id;
	private Long empresa_id;
	private Long composicaoempresa_id;
	
	private LocalDate emissao;
	private LocalDate vencimento;
	private LocalDate pagamento;
	private LocalDate compensado;
	private LocalDate protesto;
	
	private ProvisorView provisor;
//	private FinanciadorView financiador;
//	private FiadorView fiador;
	
//	private TipoFatura tipo;
	
	
	
	
	private double taxa;
	private double juros;
	private double mora;
	
	private double acrescimo;
	private double desconto;
	private double valor; //Valor do pagamento
	private double valorcorrigido;
	private double total;
	
}
