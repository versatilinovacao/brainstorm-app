package br.com.insideincloud.brainstormapp.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.view.MateriaCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.MateriasCompostaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.MateriaCompostaViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@Service
public class MateriaCompostaService {
	@Autowired
	private MateriasCompostaView materias;
	
	public List<MateriaCompostaView> conteudo(Long aowner) {		
		return materias.findByAowner(aowner);
	}
	
	public RetornoWrapper<MateriaCompostaView> filtrar(MateriaCompostaViewFilter filtro, Pageable page) {
		RetornoWrapper<MateriaCompostaView> retorno = new RetornoWrapper<MateriaCompostaView>();
		ExceptionWrapper excecao = new ExceptionWrapper();
		
		try {
			
			retorno.setPage(materias.filtrar(filtro, page));
			retorno.setException(null);
			
			if (retorno.getPage().getContent().isEmpty()) {
				excecao.setCodigo(1);
				excecao.setMensagem("Não foram localizados informações para esta consulta.");
				
				retorno.setException(excecao);
			}
			
		} catch (Exception e) {
			excecao.setCodigo(1);
			excecao.setMensagem("Não foram localizados informações para esta consulta.");
			excecao.setDebug(""+e);
			
			retorno.setException(excecao);
		}

		return retorno;
	}
}
