package br.com.insideincloud.brainstormapp.api.service.caderno;

import java.util.List;

public class PeriodoLetivoItemBase {
	private Long periodo_letivo_item_id;
	private Long periodo_letivo_id;
	private String descricao;
	private String periodo_inicial;
	private String periodo_final;
	private List<MateriaBase> materias;
	
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public Long getPeriodo_letivo_id() {
		return periodo_letivo_id;
	}
	public void setPeriodo_letivo_id(Long periodo_letivo_id) {
		this.periodo_letivo_id = periodo_letivo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getPeriodo_inicial() {
		return periodo_inicial;
	}
	public void setPeriodo_inicial(String periodo_inicial) {
		this.periodo_inicial = periodo_inicial;
	}
	public String getPeriodo_final() {
		return periodo_final;
	}
	public void setPeriodo_final(String periodo_final) {
		this.periodo_final = periodo_final;
	}
	public List<MateriaBase> getMaterias() {
		return materias;
	}
	public void setMaterias(List<MateriaBase> materias) {
		this.materias = materias;
	}
	
	

}
