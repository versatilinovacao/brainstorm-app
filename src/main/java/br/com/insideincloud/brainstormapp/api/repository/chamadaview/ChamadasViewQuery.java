package br.com.insideincloud.brainstormapp.api.repository.chamadaview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.ChamadaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ChamadaViewFilter;

public interface ChamadasViewQuery {
	public Page<ChamadaView> filtrar(ChamadaViewFilter filtro, Pageable page);
}
