package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="identificador")
public class Identificador implements Serializable {
	private static final long serialVersionUID = 120018572976095057L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long identificador_id;
	@Column(name="cpf",length=11)
	private String cpf;
	@Column(name="rg",length=20)
	private String rg;
	@Column(name="certidao",length=33)
	private String certidao;
	
	private LocalDate data_nascimento;
	@Column(name="registro_profissional",length=20)
	private String registro_profissional;
	private LocalDate registro;
	private String habilitacao;
	private String habilitacao_modelo;
//	private String habilitacao_registro;
//	private String habilitacao_vencimento;
	private String titulo_numero;
	private String titulo_zona;
	private String titulo_secao;
	private String certificado_reservista;
	private String naturalidade;
	private String nacionalidade;
	private String passaporte_numero;
	private LocalDate passaporte_datavalidade;
	private String orgaoemissor;
	private String militar_numero;
	@OneToOne
	@JoinColumn(name="sexo_id")
	private Sexo sexo;
	@Column(columnDefinition="text")
	private String foto;

	
//	@Lob
//	@Column(name="assinatura")
//	private byte[] assinatura;
//	@Lob
//	@Column(name="foto")
//	private byte[] foto;
	
//	@Transient
//	private String dt_nascimento;
	
	
//	public String getDt_nascimento() {
//		String dt = getData_nascimento().toString().substring(8,10);
//		dt = dt+getData_nascimento().toString().substring(5,7);
//		dt = dt+getData_nascimento().toString().substring(0,4);
//		System.out.println(":>>>>>>>>> "+dt);
//		dt_nascimento = dt;
//		return dt_nascimento;
//	}
//	public void setDt_nascimento(String dt_nascimento) {
////			this.dt_nascimento = dt_nascimento.replaceAll("-", "");
//			
//			this.dt_nascimento = dt_nascimento;
//		
//		setData_nascimento( LocalDate.parse( this.dt_nascimento ) );
//	}
	
	
	public Long getIdentificador_id() {
		return identificador_id;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public String getMilitar_numero() {
		return militar_numero;
	}
	public void setMilitar_numero(String militar_numero) {
		this.militar_numero = militar_numero;
	}
	public String getOrgaoemissor() {
		return orgaoemissor;
	}
	public void setOrgaoemissor(String orgaoemissor) {
		this.orgaoemissor = orgaoemissor;
	}
	public LocalDate getPassaporte_datavalidade() {
		return passaporte_datavalidade;
	}
	public void setPassaporte_datavalidade(LocalDate passaporte_datavalidade) {
		this.passaporte_datavalidade = passaporte_datavalidade;
	}
	public String getNaturalidade() {
		return naturalidade;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public String getPassaporte_numero() {
		return passaporte_numero;
	}
	public void setPassaporte_numero(String passaporte_numero) {
		this.passaporte_numero = passaporte_numero;
	}
	public void setIdentificador_id(Long identificador_id) {
		this.identificador_id = identificador_id;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCertidao() {
		return certidao;
	}
	public void setCertidao(String certidao) {
		this.certidao = certidao;
	}
	public LocalDate getData_nascimento() {
		return data_nascimento;
	}
	public void setData_nascimento(LocalDate data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	public String getRegistro_profissional() {
		return registro_profissional;
	}
	public void setRegistro_profissional(String registro_profissional) {
		this.registro_profissional = registro_profissional;
	}
	public LocalDate getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDate registro) {
		this.registro = registro;
	}
	public String getHabilitacao() {
		return habilitacao;
	}
	public void setHabilitacao(String habilitacao) {
		this.habilitacao = habilitacao;
	}
	public String getHabilitacao_modelo() {
		return habilitacao_modelo;
	}
	public void setHabilitacao_modelo(String habilitacao_modelo) {
		this.habilitacao_modelo = habilitacao_modelo;
	}
//	public String getHabilitacao_registro() {
//		return habilitacao_registro;
//	}
//	public void setHabilitacao_registro(String habilitacao_registro) {
//		this.habilitacao_registro = habilitacao_registro;
//	}
//	public String getHabilitacao_vencimento() {
//		return habilitacao_vencimento;
//	}
//	public void setHabilitacao_vencimento(String habilitacao_vencimento) {
//		this.habilitacao_vencimento = habilitacao_vencimento;
//	}
	public String getTitulo_numero() {
		return titulo_numero;
	}
	public void setTitulo_numero(String titulo_numero) {
		this.titulo_numero = titulo_numero;
	}
	public String getTitulo_zona() {
		return titulo_zona;
	}
	public void setTitulo_zona(String titulo_zona) {
		this.titulo_zona = titulo_zona;
	}
	public String getTitulo_secao() {
		return titulo_secao;
	}
	public void setTitulo_secao(String titulo_secao) {
		this.titulo_secao = titulo_secao;
	}
	public String getCertificado_reservista() {
		return certificado_reservista;
	}
	public void setCertificado_reservista(String certificado_reservista) {
		this.certificado_reservista = certificado_reservista;
	}
//	public byte[] getAssinatura() {
//		return assinatura;
//	}
//	public void setAssinatura(byte[] assinatura) {
//		this.assinatura = assinatura;
//	}
//	public byte[] getFoto() {
//		return foto;
//	}
//	public void setFoto(byte[] foto) {
//		this.foto = foto;
//	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((identificador_id == null) ? 0 : identificador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identificador other = (Identificador) obj;
		if (identificador_id == null) {
			if (other.identificador_id != null)
				return false;
		} else if (!identificador_id.equals(other.identificador_id))
			return false;
		return true;
	}	
	
}
