package br.com.insideincloud.brainstormapp.api.resource.chamada;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class ChamadaNavegador implements Serializable {
	private static final long serialVersionUID = -5293875101304666010L;
	
	private LocalDate ontem;
	private LocalDate hoje;
	private LocalDate amanha;
	private Long periodoletivoanterior_id;
	private Long periodoletivo_id;
	private Long periodoletivoposterior_id;
	
	public LocalDate getOntem() {
		return ontem;
	}
	public void setOntem(String ontem) {
		DateTimeFormatter formato = DateTimeFormatter.ofPattern(formatoTipo(ontem));		
		this.ontem = LocalDate.parse((String)ontem.trim().replaceAll("-","/"),formato);
	}
	public LocalDate getHoje() {
		return hoje;
	}
	public void setHoje(String hoje) {
		DateTimeFormatter formato = DateTimeFormatter.ofPattern(formatoTipo(hoje));
		this.hoje = LocalDate.parse((String)hoje.trim().replaceAll("-","/"),formato);
	}
	public LocalDate getAmanha() {
		return amanha;
	}
	public void setAmanha(String amanha) {		
		DateTimeFormatter formato = DateTimeFormatter.ofPattern(formatoTipo(amanha));
		this.amanha = LocalDate.parse((String)amanha.trim().replaceAll("-","/"),formato);
	}
	public Long getPeriodoletivoanterior_id() {
		return periodoletivoanterior_id;
	}
	public void setPeriodoletivoanterior_id(Long periodoletivoanterior_id) {
		this.periodoletivoanterior_id = periodoletivoanterior_id;
	}
	public Long getPeriodoletivo_id() {
		return periodoletivo_id;
	}
	public void setPeriodoletivo_id(Long periodoletivo_id) {
		this.periodoletivo_id = periodoletivo_id;
	}
	public Long getPeriodoletivoposterior_id() {
		return periodoletivoposterior_id;
	}
	public void setPeriodoletivoposterior_id(Long periodoletivoposterior_id) {
		this.periodoletivoposterior_id = periodoletivoposterior_id;
	}

	private String formatoTipo(String data) {
		
		String dataFormato = data.replaceAll("0","9")
								 .replaceAll("1","9")
								 .replaceAll("2","9")
								 .replaceAll("3","9")
								 .replaceAll("4","9")
								 .replaceAll("5","9")
								 .replaceAll("6","9")
								 .replaceAll("7","9")
								 .replaceAll("8","9")
								 .replaceAll("-","/");
			System.out.println("---------------------"+dataFormato+" "+data);
		if (dataFormato.equals("99/99/9999")) {
			System.out.println("1");
			return "dd/MM/yyyy";
		} else if (dataFormato.equals("9999/99/99")) {
			System.out.println("2");
			return "yyyy/MM/dd";
		} else {
			System.out.println("3");
			return "yyyy/dd/MM";			
		}
 		
	}
}
