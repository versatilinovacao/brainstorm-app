package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.MensagemAlunoProfessor;

@Repository
public interface MensagensAlunosProfessores extends JpaRepository<MensagemAlunoProfessor, Long> {
	
	@Query("select m from MensagemAlunoProfessor m where m.professor.usuario_id = ?1 and m.aluno.usuario_id = ?2")
	public List<MensagemAlunoProfessor> findByMensagens(Long professor_id, Long aluno_id);

}
