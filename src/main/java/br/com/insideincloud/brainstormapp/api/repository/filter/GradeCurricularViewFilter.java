package br.com.insideincloud.brainstormapp.api.repository.filter;

public class GradeCurricularViewFilter {
	
	private String gradecurricular_id;
	private String turma_id;
	private String nome_turma;
	private String professor_id;
	private String nome_professor;
	private String materia_id;
	private String nome_materia;
	private String curso_id;
	private String nome_curso;
	
	
	public String getNome_curso() {
		return nome_curso;
	}
	public void setNome_curso(String nome_curso) {
		this.nome_curso = nome_curso;
	}
	public String getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(String gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public String getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(String turma_id) {
		this.turma_id = turma_id;
	}
	public String getNome_turma() {
		return nome_turma;
	}
	public void setNome_turma(String nome_turma) {
		this.nome_turma = nome_turma;
	}
	public String getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(String professor_id) {
		this.professor_id = professor_id;
	}
	public String getNome_professor() {
		return nome_professor;
	}
	public void setNome_professor(String nome_professor) {
		this.nome_professor = nome_professor;
	}
	public String getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(String materia_id) {
		this.materia_id = materia_id;
	}
	public String getNome_materia() {
		return nome_materia;
	}
	public void setNome_materia(String nome_materia) {
		this.nome_materia = nome_materia;
	}
	public String getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(String curso_id) {
		this.curso_id = curso_id;
	}

}
