package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="familias")
public class Familia implements Serializable {
	private static final long serialVersionUID = -3405408560071654566L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long familia_id;
	@Column(name="codigo",length=3)
	private String codigo;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="descricao",length=500)
	private String descricao;
	
	public Long getFamilia_id() {
		return familia_id;
	}
	public void setFamilia_id(Long familia_id) {
		this.familia_id = familia_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((familia_id == null) ? 0 : familia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Familia other = (Familia) obj;
		if (familia_id == null) {
			if (other.familia_id != null)
				return false;
		} else if (!familia_id.equals(other.familia_id))
			return false;
		return true;
	}
	
	
	
}
