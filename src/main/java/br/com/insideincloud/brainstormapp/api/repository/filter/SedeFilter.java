package br.com.insideincloud.brainstormapp.api.repository.filter;

public class SedeFilter {

	private String sede_id;
	private String descricao;
	
	public String getSede_id() {
		return sede_id;
	}
	public void setSede_id(String sede_id) {
		this.sede_id = sede_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
