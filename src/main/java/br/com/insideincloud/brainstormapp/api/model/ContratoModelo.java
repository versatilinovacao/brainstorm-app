package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contratos_modelos",schema="public")
public class ContratoModelo implements Serializable {
	private static final long serialVersionUID = 1431780435910533787L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long contratomodelo_id;
	private String descricao;
	private String documento;
	
	public Long getContratomodelo_id() {
		return contratomodelo_id;
	}
	public void setContratomodelo_id(Long contratomodelo_id) {
		this.contratomodelo_id = contratomodelo_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contratomodelo_id == null) ? 0 : contratomodelo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContratoModelo other = (ContratoModelo) obj;
		if (contratomodelo_id == null) {
			if (other.contratomodelo_id != null)
				return false;
		} else if (!contratomodelo_id.equals(other.contratomodelo_id))
			return false;
		return true;
	}
	
}
