package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.jdom.Element;

@Entity
@Table(name="prestador_view",schema="nfse")
public class PrestadorView implements Serializable {
	
	@Id
	private Long prestador_id;
	private String cnpj;
	private String inscricaomunicipal;
	private String cnae;
	
	private Element prestadorNode = new Element("Prestador");
	private Element cnpjNode = new Element("Cnpj");
	private Element inscricaoMunicipalNode = new Element("InscricaoMunicipal");
	
	public Element nodeXML() { return prestadorNode; }
	
	public Long getprestador_id() {
		return prestador_id;
	}
	public void setprestador_id(Long prestador_id) {
		this.prestador_id = prestador_id;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		prestadorNode.addContent(cnpjNode);
		cnpjNode.setText(cnpj);
		this.cnpj = cnpj;
	}
	public String getInscricaomunicipal() {
		return inscricaomunicipal;
	}
	public void setInscricaomunicipal(String inscricaomunicipal) {
		prestadorNode.addContent(inscricaoMunicipalNode);
		inscricaoMunicipalNode.setText(inscricaomunicipal);
		this.inscricaomunicipal = inscricaomunicipal;
	}
	public String getCnae() {
		return cnae;
	}
	public void setCnae(String cnae) {
		this.cnae = cnae;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prestador_id == null) ? 0 : prestador_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrestadorView other = (PrestadorView) obj;
		if (prestador_id == null) {
			if (other.prestador_id != null)
				return false;
		} else if (!prestador_id.equals(other.prestador_id))
			return false;
		return true;
	}

}
