package br.com.insideincloud.brainstormapp.api.model.minor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gradecurricular_minor",schema="escola")
public class GradeCurricularMinor {

	@Id
	private Long gradecurricular_id;
	private Long turma_id;
	private String nometurma;
	private Long professor_id;
	private Long composicao_professor_id;
	private String nomeprofessor;
	private Long materia_id;
	private String nomemateria;
	private Long curso_id;
	private Long cursoperiodo_id;
	private Boolean status;
	
	public String getNometurma() {
		return nometurma;
	}
	public void setNometurma(String nometurma) {
		this.nometurma = nometurma;
	}
	public String getNomeprofessor() {
		return nomeprofessor;
	}
	public void setNomeprofessor(String nomeprofessor) {
		this.nomeprofessor = nomeprofessor;
	}
	public String getNomemateria() {
		return nomemateria;
	}
	public void setNomemateria(String nomemateria) {
		this.nomemateria = nomemateria;
	}
	public Long getGradecurricular_id() {
		return gradecurricular_id;
	}
	public void setGradecurricular_id(Long gradecurricular_id) {
		this.gradecurricular_id = gradecurricular_id;
	}
	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public Long getProfessor_id() {
		return professor_id;
	}
	public void setProfessor_id(Long professor_id) {
		this.professor_id = professor_id;
	}
	public Long getComposicao_professor_id() {
		return composicao_professor_id;
	}
	public void setComposicao_professor_id(Long composicao_professor_id) {
		this.composicao_professor_id = composicao_professor_id;
	}
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public Long getCurso_id() {
		return curso_id;
	}
	public void setCurso_id(Long curso_id) {
		this.curso_id = curso_id;
	}
	public Long getCursoperiodo_id() {
		return cursoperiodo_id;
	}
	public void setCursoperiodo_id(Long cursoperiodo_id) {
		this.cursoperiodo_id = cursoperiodo_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gradecurricular_id == null) ? 0 : gradecurricular_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GradeCurricularMinor other = (GradeCurricularMinor) obj;
		if (gradecurricular_id == null) {
			if (other.gradecurricular_id != null)
				return false;
		} else if (!gradecurricular_id.equals(other.gradecurricular_id))
			return false;
		return true;
	}
	
}
