package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import br.com.insideincloud.brainstormapp.api.model.pk.RpsPK;
import br.com.insideincloud.brainstormapp.api.model.view.PrestadorView;

@Entity
@Table(name="rps",schema="nfse")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discriminador",discriminatorType= DiscriminatorType.STRING)
@DiscriminatorValue("RPS")
public class Rps implements Serializable {
	private static final long serialVersionUID = 6491447536308087673L;
	
	@EmbeddedId
	private RpsPK rps_id;	
	
	@OneToOne
	@JoinColumn(name="tomador_id")
	private Tomador tomador;
	@OneToOne
	@JoinColumn(name="prestador_id")
	private PrestadorView prestador;
	private Boolean status;
	private LocalDate dataemissao;
//	private NaturezaOperacao naturezaoperacao;
	private Boolean incentivadorcultural;
	private Boolean optantesimplesnacional;

//	private List<Artefato> servicos;
	
	public RpsPK getRps_id() {
		return rps_id;
	}

	public void setRps_id(RpsPK rps_id) {
		this.rps_id = rps_id;
	}

	public Tomador getTomador() {
		return tomador;
	}

	public void setTomador(Tomador tomador) {
		this.tomador = tomador;
	}

	public PrestadorView getPrestador() {
		return prestador;
	}

	public void setPrestador(PrestadorView prestador) {
		this.prestador = prestador;
	}

	public Boolean getStatus() {		
		return status;
	}

	public void setStatus(Boolean status) {	
		this.status = status;
	}

	public LocalDate getDataemissao() {
		return dataemissao;
	}

	public void setDataemissao(LocalDate dataemissao) {
		this.dataemissao = dataemissao;
	}

	public Boolean getIncentivadorcultural() {
		return incentivadorcultural;
	}

	public void setIncentivadorcultural(Boolean incentivadorcultural) {
		this.incentivadorcultural = incentivadorcultural;
	}

	public Boolean getOptantesimplesnacional() {
		return optantesimplesnacional;
	}

	public void setOptantesimplesnacional(Boolean optantesimplesnacional) {
		this.optantesimplesnacional = optantesimplesnacional;
	}

//	public List<Artefato> getServicos() {
//		return servicos;
//	}
//
//	public void setServicos(List<Artefato> servicos) {
//		this.servicos = servicos;
//	}

}

/*
Pedido
Compra/Venda/Atendimento

A opção de atendimento refere-se a emissão de contratos, recibos e demais situações que referen-se a prestação de 
serviço.

Contrato
Recibo
*/