package br.com.insideincloud.brainstormapp.api.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.PlanoConta;
import br.com.insideincloud.brainstormapp.api.model.view.PlanoContaAtivaView;
import br.com.insideincloud.brainstormapp.api.model.view.PlanoContaInativaView;
import br.com.insideincloud.brainstormapp.api.repository.PlanoContas;
import br.com.insideincloud.brainstormapp.api.repository.PlanoContasAtivasView;
import br.com.insideincloud.brainstormapp.api.repository.PlanoContasInativasView;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/planoscontas")
public class PlanoContaResource {

	@Autowired
	private PlanoContasAtivasView planoContasAtivas;
	
	@Autowired
	private PlanoContasInativasView planoContasInativas;
	
	@Autowired
	private PlanoContas planosContas;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PLANOCONTA_CONTEUDO')")
	public RetornoWrapper<PlanoContaAtivaView> conteudoAtivo() {
		RetornoWrapper<PlanoContaAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			List<PlanoContaAtivaView> conteudoResult = new ArrayList<>(); 

			planoContasAtivas.findAll().forEach(conteudo -> {
				if (conteudo.getComposicao_id() == null) {
					conteudoResult.add(conteudo);
				}
				
			});
			
			retorno.setConteudo( conteudoResult );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum plano de conta ativo, favor tentar novamente dentro de alguns instates.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/vinculos")
	@PreAuthorize("hasAuthority('ROLE_PLANOCONTA_CONTEUDO')")
	public RetornoWrapper<PlanoContaAtivaView> conteudoVinculo() {
		RetornoWrapper<PlanoContaAtivaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {			
			retorno.setConteudo( planoContasAtivas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum plano de conta ativo, favor tentar novamente dentro de alguns instates.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}	

	@GetMapping("/inativas")
	@PreAuthorize("hasAuthority('ROLE_PLANOCONTA_CONTEUDO')")
	public RetornoWrapper<PlanoContaInativaView> conteudoInativo() {
		RetornoWrapper<PlanoContaInativaView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( planoContasInativas.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar nenhum plano de conta inativo, favor tentar novamente dentro de alguns instates.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{planoconta_id}")
	@PreAuthorize("hasAuthority('ROLE_PLANOCONTA_CONTEUDO')")
	public RetornoWrapper<PlanoConta> conteudoPorId(@PathVariable Long planoconta_id) {
		RetornoWrapper<PlanoConta> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			PlanoConta p = planosContas.findById(planoconta_id, composicao_id);
//			p.setContas( planosContas.findByComposicao( p.getPlanoconta_id().getComposicao_id() ) );
			
			retorno.setSingle( planosContas.findOne(planoconta_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o plano de conta pesquisado, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_PLANOCONTA_SALVAR')")
	public RetornoWrapper<PlanoConta> salvar(@RequestBody PlanoConta plano) {
		RetornoWrapper<PlanoConta> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( planosContas.saveAndFlush(plano) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o plano de conta, favor verificar as informações e tentar novamente.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
