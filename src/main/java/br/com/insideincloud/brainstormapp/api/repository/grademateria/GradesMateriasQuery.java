package br.com.insideincloud.brainstormapp.api.repository.grademateria;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.GradeMateria;
import br.com.insideincloud.brainstormapp.api.repository.filter.GradeMateriaFilter;

public interface GradesMateriasQuery {
	public Page<GradeMateria> filtrar(GradeMateriaFilter filtro, Pageable page);
}
