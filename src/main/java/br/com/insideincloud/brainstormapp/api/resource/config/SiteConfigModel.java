package br.com.insideincloud.brainstormapp.api.resource.config;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class SiteConfigModel implements Serializable {
	private static final long serialVersionUID = -2821432615991408157L;
	
	private boolean pagina;
	private boolean sistema;
	
	public boolean isPagina() {
		return pagina;
	}
	public void setPagina(boolean pagina) {
		this.pagina = pagina;
	}
	public boolean isSistema() {
		return sistema;
	}
	public void setSistema(boolean sistema) {
		this.sistema = sistema;
	}
	
	

}
