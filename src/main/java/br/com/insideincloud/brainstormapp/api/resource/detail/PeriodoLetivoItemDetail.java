package br.com.insideincloud.brainstormapp.api.resource.detail;

import java.time.LocalDate;

public class PeriodoLetivoItemDetail {
	private Long periodo_letivo_item_id;
	private String descricao;
	private LocalDate periodo_inicial;
	private LocalDate periodo_final;
	
	public Long getPeriodo_letivo_item_id() {
		return periodo_letivo_item_id;
	}
	public void setPeriodo_letivo_item_id(Long periodo_letivo_item_id) {
		this.periodo_letivo_item_id = periodo_letivo_item_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public LocalDate getPeriodo_inicial() {
		return periodo_inicial;
	}
	public void setPeriodo_inicial(LocalDate periodo_inicial) {
		this.periodo_inicial = periodo_inicial;
	}
	public LocalDate getPeriodo_final() {
		return periodo_final;
	}
	public void setPeriodo_final(LocalDate periodo_final) {
		this.periodo_final = periodo_final;
	}
	
	
}
