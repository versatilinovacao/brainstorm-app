package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.ArquivoPorAtividade;

@Repository
public interface ArquivosPorAtividades extends JpaRepository<ArquivoPorAtividade, Long> {

	@Query("select a from ArquivoPorAtividade a where a.atividade.atividade_id = ?1 and a.aluno.colaborador_id.colaborador_id = ?2 and a.aluno.colaborador_id.composicao_id = ?3 and a.arquivo.arquivo_id = ?4")
	public ArquivoPorAtividade findByArquivo(Long atividade_id, Long aluno_id, Long composicao_aluno_id, Long arquivo_id);
}
