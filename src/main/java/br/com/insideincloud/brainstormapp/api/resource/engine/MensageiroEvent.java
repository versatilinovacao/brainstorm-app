package br.com.insideincloud.brainstormapp.api.resource.engine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import br.com.insideincloud.brainstormapp.api.repository.Mensagens;

@Component
public class MensageiroEvent {

	@Autowired
	private Mensagens mensagens;
	
	@EventListener
	public void enviarMensagemEvent(MensagemEvent mensagem) {
		mensagens.saveAndFlush(mensagem.getMensagem());
		
	}
	
}
