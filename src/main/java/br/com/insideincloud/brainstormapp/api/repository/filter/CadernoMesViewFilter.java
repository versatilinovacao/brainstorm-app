package br.com.insideincloud.brainstormapp.api.repository.filter;

import java.io.Serializable;

public class CadernoMesViewFilter implements Serializable {
	private static final long serialVersionUID = -2245034520467110046L;
	
	private String caderno_id;
	private String nomecurso;
	private String nometurma;
	private String nomecaderno;
	private String mes;
	private String mesporextenso;
	private String ano;
	
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getNomecurso() {
		return nomecurso;
	}
	public void setNomecurso(String nomecurso) {
		this.nomecurso = nomecurso;
	}
	public String getNometurma() {
		return nometurma;
	}
	public void setNometurma(String nometurma) {
		this.nometurma = nometurma;
	}
	public String getNomecaderno() {
		return nomecaderno;
	}
	public void setNomecaderno(String nomecaderno) {
		this.nomecaderno = nomecaderno;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getMesporextenso() {
		return mesporextenso;
	}
	public void setMesporextenso(String mesporextenso) {
		this.mesporextenso = mesporextenso;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	
}
