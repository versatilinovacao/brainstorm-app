package br.com.insideincloud.brainstormapp.api.cron;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import br.com.insideincloud.brainstormapp.api.model.AvaliacaoAlunoAtividade;
import br.com.insideincloud.brainstormapp.api.repository.AvaliacoesAlunosAtividades;
import br.com.insideincloud.brainstormapp.api.repository.SituacoesAtividades;
import br.com.insideincloud.brainstormapp.api.service.AlunoService;


/*
 * "0 0 * * * *"                The top of every hour of every day.
 * "/10 * * * *"                every ten seconds.
 * "0 0 8-10 * * *"             8, 9 and 10 o'clock of every day.
 * "0 0/30 8-10 * * *"          8:00, 8:30, 9:00 and 10 o´clock every day.
 * "0 0 9-17 * * MON-FRI"       on the hour nine-to-five weekdays
 * "0 0 0 25 12 ?"              every Christmas Day at midnight
 * 
 * */

@Configuration
@EnableScheduling
public class Agenda {

	private final String TIME_ZONE = "America/Sao_Paulo";
	
	@Autowired
	private AlunoService alunosService;
	
	@Autowired
	private AvaliacoesAlunosAtividades alunosAtividades;
	
	@Autowired
	private SituacoesAtividades situacoes;
	
//	@Scheduled(cron = "5 0 0 * * *", zone = TIME_ZONE)
	@Scheduled(cron = "0 0/59 23 * * ?")
	public void encerrarAtividades() {
		alunosService.findByAtividadesPendentes().forEach(conteudo -> {
			System.out.println("OLA MUNDO DA CRON");
			if (conteudo.getEntrega().isBefore( LocalDate.now() )) {
				AvaliacaoAlunoAtividade avaliacao = alunosAtividades.findByAvaliacaoAluno(conteudo.getAtividade_id(), conteudo.getAluno_id(), conteudo.getComposicao_aluno_id());
				if (avaliacao.getAtividade().getAvaliacao()) {
					if (conteudo.getSituacaoatividade_id() == 3L && conteudo.getNota()==null) {
						avaliacao.setNota(10);
					} else { avaliacao.setNota(0); }
					
				}
				avaliacao.setSituacao( situacoes.findOne(1L) );
				avaliacao.setRevisar(null);
				
				alunosAtividades.save(avaliacao);
			}
			
		});
	}
	
}
