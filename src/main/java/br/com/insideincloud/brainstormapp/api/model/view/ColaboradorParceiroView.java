package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="colaboradores_parceiros_view", schema="public")
public class ColaboradorParceiroView implements Serializable {
	private static final long serialVersionUID = -8504240690772462081L;
	
	@Id
	private Long id;
	private Long parceiro_id;
	@Column(name="composicao_parceiros_id")
	private Long composicaoparceiro_id;
	private String nome;
	private Boolean status;
	
	public Long getComposicaoparceiro_id() {
		return composicaoparceiro_id;
	}
	public void setComposicaoparceiro_id(Long composicaoparceiro_id) {
		this.composicaoparceiro_id = composicaoparceiro_id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getParceiro_id() {
		return parceiro_id;
	}
	public void setParceiro_id(Long parceiro_id) {
		this.parceiro_id = parceiro_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composicaoparceiro_id == null) ? 0 : composicaoparceiro_id.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((parceiro_id == null) ? 0 : parceiro_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ColaboradorParceiroView other = (ColaboradorParceiroView) obj;
		if (composicaoparceiro_id == null) {
			if (other.composicaoparceiro_id != null)
				return false;
		} else if (!composicaoparceiro_id.equals(other.composicaoparceiro_id))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parceiro_id == null) {
			if (other.parceiro_id != null)
				return false;
		} else if (!parceiro_id.equals(other.parceiro_id))
			return false;
		return true;
	}
	

}
