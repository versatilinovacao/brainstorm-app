package br.com.insideincloud.brainstormapp.api.service.detail;

import java.io.Serializable;
import java.util.List;

import br.com.insideincloud.brainstormapp.api.model.GrupoSecurity;
import br.com.insideincloud.brainstormapp.api.model.TelaSecurity;

public class GrupoSecurityDetail implements Serializable {
	private static final long serialVersionUID = -1204571883632594483L;

	private GrupoSecurity gruposecurity;
	private List<TelaSecurity> telasecurity;
	private List<TelaSecurity> deletar;
	
	public GrupoSecurity getGruposecurity() {
		return gruposecurity;
	}
	public void setGruposecurity(GrupoSecurity gruposecurity) {
		this.gruposecurity = gruposecurity;
	}
	public List<TelaSecurity> getTelasecurity() {
		return telasecurity;
	}
	public void setTelasecurity(List<TelaSecurity> telasecurity) {
		this.telasecurity = telasecurity;
	}
	public List<TelaSecurity> getDeletar() {
		return deletar;
	}
	public void setDeletar(List<TelaSecurity> deletar) {
		this.deletar = deletar;
	}	
	
	
}
