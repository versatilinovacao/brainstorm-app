package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="frequenciasreport",schema="escola")
public class ChamadaReportView implements Serializable {
	private static final long serialVersionUID = 555398616228267926L;

	@Id
	private Integer chamada_id;
	@Column(name="caderno_id")
	private Long caderno_id;
	@Column(name="aluno_id")
	private Long aluno_id;
	private String nome_aluno;
	@Column(name="um")
	private Boolean um;
	@Column(name="dois")
	private Boolean dois;
	@Column(name="tres")
	private Boolean tres;
	@Column(name="quatro")
	private Boolean quatro;
	@Column(name="cinco")
	private Boolean cinco;
	@Column(name="seis")
	private Boolean seis;
	@Column(name="sete")
	private Boolean sete;
	@Column(name="oito")
	private Boolean oito;
	@Column(name="nove")
	private Boolean nove;
	@Column(name="dez")
	private Boolean dez;
	@Column(name="onze")
	private Boolean onze;
	@Column(name="doze")
	private Boolean doze;
	@Column(name="treze")
	private Boolean treze;
	@Column(name="quatorze")
	private Boolean quatorze;
	@Column(name="quinze")
	private Boolean quinze;
	@Column(name="dezesseis")
	private Boolean dezesseis;
	@Column(name="dezessete")
	private Boolean dezessete;
	@Column(name="dezoito")
	private Boolean dezoito;
	@Column(name="dezenove")
	private Boolean dezenove;
	@Column(name="vinte")
	private Boolean vinte;
	@Column(name="vinteum")
	private Boolean vinteum;
	@Column(name="vintedois")
	private Boolean vintedois;
	@Column(name="vintetres")
	private Boolean vintetres;
	@Column(name="vintequatro")
	private Boolean vintequatro;
	@Column(name="vintecinco")
	private Boolean vintecinco;
	@Column(name="vinteseis")
	private Boolean vinteseis;
	@Column(name="vintesete")
	private Boolean vintesete;
	@Column(name="vinteoito")
	private Boolean vinteoito;
	@Column(name="vintenove")
	private Boolean vintenove;
	@Column(name="trinta")
	private Boolean trinta;
	@Column(name="trintaeum")
	private Boolean trintaeum;
	@Column(name="mes")
	private String mes;
	@Column(name="ano")
	private String ano;
	
	private Integer media;
	private String conceito;
	private String avaliacao;
	private String nome_curso;
	private String nome_turma;
	private String mes_por_extenso;
	
	public Integer getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(Integer chamada_id) {
		this.chamada_id = chamada_id;
	}
	public Long getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(Long caderno_id) {
		this.caderno_id = caderno_id;
	}
	public Long getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(Long aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public Boolean getUm() {
		return um;
	}
	public void setUm(Boolean um) {
		this.um = um;
	}
	public Boolean getDois() {
		return dois;
	}
	public void setDois(Boolean dois) {
		this.dois = dois;
	}
	public Boolean getTres() {
		return tres;
	}
	public void setTres(Boolean tres) {
		this.tres = tres;
	}
	public Boolean getQuatro() {
		return quatro;
	}
	public void setQuatro(Boolean quatro) {
		this.quatro = quatro;
	}
	public Boolean getCinco() {
		return cinco;
	}
	public void setCinco(Boolean cinco) {
		this.cinco = cinco;
	}
	public Boolean getSeis() {
		return seis;
	}
	public void setSeis(Boolean seis) {
		this.seis = seis;
	}
	public Boolean getSete() {
		return sete;
	}
	public void setSete(Boolean sete) {
		this.sete = sete;
	}
	public Boolean getOito() {
		return oito;
	}
	public void setOito(Boolean oito) {
		this.oito = oito;
	}
	public Boolean getNove() {
		return nove;
	}
	public void setNove(Boolean nove) {
		this.nove = nove;
	}
	public Boolean getDez() {
		return dez;
	}
	public void setDez(Boolean dez) {
		this.dez = dez;
	}
	public Boolean getOnze() {
		return onze;
	}
	public void setOnze(Boolean onze) {
		this.onze = onze;
	}
	public Boolean getDoze() {
		return doze;
	}
	public void setDoze(Boolean doze) {
		this.doze = doze;
	}
	public Boolean getTreze() {
		return treze;
	}
	public void setTreze(Boolean treze) {
		this.treze = treze;
	}
	public Boolean getQuatorze() {
		return quatorze;
	}
	public void setQuatorze(Boolean quatorze) {
		this.quatorze = quatorze;
	}
	public Boolean getQuinze() {
		return quinze;
	}
	public void setQuinze(Boolean quinze) {
		this.quinze = quinze;
	}
	public Boolean getDezesseis() {
		return dezesseis;
	}
	public void setDezesseis(Boolean dezesseis) {
		this.dezesseis = dezesseis;
	}
	public Boolean getDezessete() {
		return dezessete;
	}
	public void setDezessete(Boolean dezessete) {
		this.dezessete = dezessete;
	}
	public Boolean getDezoito() {
		return dezoito;
	}
	public void setDezoito(Boolean dezoito) {
		this.dezoito = dezoito;
	}
	public Boolean getDezenove() {
		return dezenove;
	}
	public void setDezenove(Boolean dezenove) {
		this.dezenove = dezenove;
	}
	public Boolean getVinte() {
		return vinte;
	}
	public void setVinte(Boolean vinte) {
		this.vinte = vinte;
	}
	public Boolean getVinteum() {
		return vinteum;
	}
	public void setVinteum(Boolean vinteum) {
		this.vinteum = vinteum;
	}
	public Boolean getVintedois() {
		return vintedois;
	}
	public void setVintedois(Boolean vintedois) {
		this.vintedois = vintedois;
	}
	public Boolean getVintetres() {
		return vintetres;
	}
	public void setVintetres(Boolean vintetres) {
		this.vintetres = vintetres;
	}
	public Boolean getVintequatro() {
		return vintequatro;
	}
	public void setVintequatro(Boolean vintequatro) {
		this.vintequatro = vintequatro;
	}
	public Boolean getVintecinco() {
		return vintecinco;
	}
	public void setVintecinco(Boolean vintecinco) {
		this.vintecinco = vintecinco;
	}
	public Boolean getVinteseis() {
		return vinteseis;
	}
	public void setVinteseis(Boolean vinteseis) {
		this.vinteseis = vinteseis;
	}
	public Boolean getVintesete() {
		return vintesete;
	}
	public void setVintesete(Boolean vintesete) {
		this.vintesete = vintesete;
	}
	public Boolean getVinteoito() {
		return vinteoito;
	}
	public void setVinteoito(Boolean vinteoito) {
		this.vinteoito = vinteoito;
	}
	public Boolean getVintenove() {
		return vintenove;
	}
	public void setVintenove(Boolean vintenove) {
		this.vintenove = vintenove;
	}
	public Boolean getTrinta() {
		return trinta;
	}
	public void setTrinta(Boolean trinta) {
		this.trinta = trinta;
	}
	public Boolean getTrintaeum() {
		return trintaeum;
	}
	public void setTrintaeum(Boolean trintaeum) {
		this.trintaeum = trintaeum;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}
	public Integer getMedia() {
		return media;
	}
	public void setMedia(Integer media) {
		this.media = media;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	public String getNome_curso() {
		return nome_curso;
	}
	public void setNome_curso(String nome_curso) {
		this.nome_curso = nome_curso;
	}
	public String getNome_turma() {
		return nome_turma;
	}
	public void setNome_turma(String nome_turma) {
		this.nome_turma = nome_turma;
	}
	public String getMes_por_extenso() {
		return mes_por_extenso;
	}
	public void setMes_por_extenso(String mes_por_extenso) {
		this.mes_por_extenso = mes_por_extenso;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno_id == null) ? 0 : aluno_id.hashCode());
		result = prime * result + ((ano == null) ? 0 : ano.hashCode());
		result = prime * result + ((caderno_id == null) ? 0 : caderno_id.hashCode());
		result = prime * result + ((chamada_id == null) ? 0 : chamada_id.hashCode());
		result = prime * result + ((mes == null) ? 0 : mes.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChamadaReportView other = (ChamadaReportView) obj;
		if (aluno_id == null) {
			if (other.aluno_id != null)
				return false;
		} else if (!aluno_id.equals(other.aluno_id))
			return false;
		if (ano == null) {
			if (other.ano != null)
				return false;
		} else if (!ano.equals(other.ano))
			return false;
		if (caderno_id == null) {
			if (other.caderno_id != null)
				return false;
		} else if (!caderno_id.equals(other.caderno_id))
			return false;
		if (chamada_id == null) {
			if (other.chamada_id != null)
				return false;
		} else if (!chamada_id.equals(other.chamada_id))
			return false;
		if (mes == null) {
			if (other.mes != null)
				return false;
		} else if (!mes.equals(other.mes))
			return false;
		return true;
	}
	
}
