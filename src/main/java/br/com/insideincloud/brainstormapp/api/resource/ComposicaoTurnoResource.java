package br.com.insideincloud.brainstormapp.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.ComposicaoTurno;
import br.com.insideincloud.brainstormapp.api.repository.ComposicoesTurnos;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/composicoesturnos")
public class ComposicaoTurnoResource {
	
	@Autowired
	private ComposicoesTurnos turnos;
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_COMPOSICAOTURNO_SALVAR')")
	public RetornoWrapper<ComposicaoTurno> salvar(@RequestBody ComposicaoTurno turno) {
		RetornoWrapper<ComposicaoTurno> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( turnos.save(turno) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o turno");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{turno_id}")
	@PreAuthorize("hasAuthority('ROLE_COMPOSICAOTURNO_CONTEUDO')")
	public RetornoWrapper<ComposicaoTurno> conteudoPorId(@PathVariable Long turno_id) {
		RetornoWrapper<ComposicaoTurno> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle( turnos.findOne(turno_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar a composição do turno solicitado.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_COMPOSICAOTURNO_CONTEUDO')")
	public RetornoWrapper<ComposicaoTurno> conteudo() {
		RetornoWrapper<ComposicaoTurno> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( turnos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhuma composição de turno.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

}
