package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="arquivos_atividades_view",schema="escola")
public class ArquivoAtividadeView implements Serializable {
	private static final long serialVersionUID = -1728092880233366513L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long arquivo_id;
	private Long atividade_id;
	private String nome;
	private String extensao;
	private String tipo;
	private Integer tamanho;
	private LocalDateTime registro;
	private String url;
	private String arquivo;
	
	public Long getArquivo_id() {
		return arquivo_id;
	}
	public void setArquivo_id(Long arquivo_id) {
		this.arquivo_id = arquivo_id;
	}
	public Long getAtividade_id() {
		return atividade_id;
	}
	public void setAtividade_id(Long atividade_id) {
		this.atividade_id = atividade_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getExtensao() {
		return extensao;
	}
	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getTamanho() {
		return tamanho;
	}
	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
	public LocalDateTime getRegistro() {
		return registro;
	}
	public void setRegistro(LocalDateTime registro) {
		this.registro = registro;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getArquivo() {
		return arquivo;
	}
	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arquivo_id == null) ? 0 : arquivo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArquivoAtividadeView other = (ArquivoAtividadeView) obj;
		if (arquivo_id == null) {
			if (other.arquivo_id != null)
				return false;
		} else if (!arquivo_id.equals(other.arquivo_id))
			return false;
		return true;
	}
	

}
