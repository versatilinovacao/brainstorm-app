package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.pk.AtividadeAlunoAvaliacaoPK;
import br.com.insideincloud.brainstormapp.api.model.view.AtividadeAlunoAvaliacaoView;

@Repository
public interface AtividadesAlunosAvaliacoesView extends JpaRepository<AtividadeAlunoAvaliacaoView, AtividadeAlunoAvaliacaoPK> {

	@Query("select a from AtividadeAlunoAvaliacaoView a where a.id.atividade_id = ?1 order by a.nomealuno")
	public List<AtividadeAlunoAvaliacaoView> findByAlunos(Long atividade_id);
	
}
