package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.CertificadoA1;
import br.com.insideincloud.brainstormapp.api.model.pk.CertificadoA1PK;

@Repository
public interface CertificadosA1 extends JpaRepository<CertificadoA1,CertificadoA1PK> {
//	public CertificadoA1 findByCertificado_idAndEmpresa_id(CertificadoA1PK id);
}
