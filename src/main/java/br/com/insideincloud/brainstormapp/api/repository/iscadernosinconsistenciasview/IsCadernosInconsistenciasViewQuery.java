package br.com.insideincloud.brainstormapp.api.repository.iscadernosinconsistenciasview;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.view.IsCadernoInconsistenciaView;
import br.com.insideincloud.brainstormapp.api.repository.filter.IsCadernoInconsistenciaViewFilter;

public interface IsCadernosInconsistenciasViewQuery {
	public Page<IsCadernoInconsistenciaView> filtrar(IsCadernoInconsistenciaViewFilter filtro, Pageable page);
}
