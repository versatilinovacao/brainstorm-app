package br.com.insideincloud.brainstormapp.api.repository.colaboradorview;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.insideincloud.brainstormapp.api.model.view.ColaboradorView;
import br.com.insideincloud.brainstormapp.api.repository.filter.ColaboradorViewFilter;

public class ColaboradoresViewImpl implements ColaboradoresViewQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<ColaboradorView> filtrar(ColaboradorViewFilter filtro, Pageable page) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ColaboradorView> criteria = builder.createQuery(ColaboradorView.class);
		Root<ColaboradorView> root = criteria.from(ColaboradorView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		TypedQuery<ColaboradorView> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query,page);
		
		return new PageImpl<>(query.getResultList(),page,total(filtro));		
	}


	private Predicate[] criarRestricoes(ColaboradorViewFilter filtro, CriteriaBuilder builder, Root<ColaboradorView> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (filtro != null) {
			
			if (!StringUtils.isEmpty(filtro.getColaborador_id())) {
				predicates.add(builder.equal(root.get("colaborador_id"), Long.parseLong(filtro.getColaborador_id())));
			}		

			if (!StringUtils.isEmpty(filtro.getTelefone())) {
				predicates.add(builder.equal(root.get("telefone"), filtro.getTelefone()));
			}		

			if (!StringUtils.isEmpty(filtro.getReferencia())) {
				predicates.add(builder.equal(root.get("referencia"), filtro.getReferencia()));
			}		

			if (!StringUtils.isEmpty(filtro.getNome())) {
				predicates.add(builder.like(builder.lower(root.get("nome")), "%" + filtro.getNome().toLowerCase() + "%"));
			}
			
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable page) {
		int paginaAtual = page.getPageNumber();
		int totalRegistrosPorPagina = page.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ColaboradorViewFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<ColaboradorView> root = criteria.from(ColaboradorView.class);
		
		Predicate[] predicates = criarRestricoes(filtro,builder,root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
