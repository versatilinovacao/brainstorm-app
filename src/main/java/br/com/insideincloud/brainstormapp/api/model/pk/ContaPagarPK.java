package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ContaPagarPK implements Serializable {
	private static final long serialVersionUID = 3209285894260808940L;
	
	private Long contapagar_id;	
	private Long composicao_id;
	
	public Long getContapagar_id() {
		return contapagar_id;
	}
	public void setContapagar_id(Long contapagar_id) {
		this.contapagar_id = contapagar_id;
	}
	public Long getComposicao_id() {
		return composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		this.composicao_id = composicao_id;
	}
}
