package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tipos_servicos",schema="contabil")
public class TipoServico implements Serializable {
	private static final long serialVersionUID = 3401780287223799426L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long tiposervico_id;
	private String descricao;
	private Boolean status;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getTiposervico_id() {
		return tiposervico_id;
	}
	public void setTiposervico_id(Long tiposervico_id) {
		this.tiposervico_id = tiposervico_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tiposervico_id == null) ? 0 : tiposervico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoServico other = (TipoServico) obj;
		if (tiposervico_id == null) {
			if (other.tiposervico_id != null)
				return false;
		} else if (!tiposervico_id.equals(other.tiposervico_id))
			return false;
		return true;
	}
	
}
