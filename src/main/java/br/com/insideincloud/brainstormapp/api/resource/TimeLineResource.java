package br.com.insideincloud.brainstormapp.api.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.insideincloud.brainstormapp.api.model.TimeLine;
import br.com.insideincloud.brainstormapp.api.repository.TimesLines;

@RestController
@RequestMapping("/timeline")
public class TimeLineResource {
	
	@Autowired
	private TimesLines timeslines;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TIMELINE_CONTEUDO')")
	public List<TimeLine> conteudo() {
		return timeslines.findAll();
	}
	
}
