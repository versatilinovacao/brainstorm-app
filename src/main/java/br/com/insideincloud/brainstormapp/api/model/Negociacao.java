package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="negociacoes",schema="financeiro")
public class Negociacao implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long negociacao_id;
	private String descricao;
	
	public Long getNegociacao_id() {
		return negociacao_id;
	}
	public void setNegociacao_id(Long negociacao_id) {
		this.negociacao_id = negociacao_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((negociacao_id == null) ? 0 : negociacao_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Negociacao other = (Negociacao) obj;
		if (negociacao_id == null) {
			if (other.negociacao_id != null)
				return false;
		} else if (!negociacao_id.equals(other.negociacao_id))
			return false;
		return true;
	}

}
