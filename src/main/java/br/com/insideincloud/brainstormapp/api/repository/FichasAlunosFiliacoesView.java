package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.view.FichaAlunoFiliacaoView;

@Repository
public interface FichasAlunosFiliacoesView extends JpaRepository<FichaAlunoFiliacaoView, Long> {

	@Query("select f from FichaAlunoFiliacaoView f where f.aluno_id = ?1 and f.composicao_aluno_id = ?2")
	public List<FichaAlunoFiliacaoView> findByResponsaveis(Long aluno_id, Long composicao_aluno_id);
	
}
