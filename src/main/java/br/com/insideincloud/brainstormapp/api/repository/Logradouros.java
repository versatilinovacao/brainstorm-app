package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.Logradouro;
import br.com.insideincloud.brainstormapp.api.repository.logradouro.LogradourosQuery;

@Repository
public interface Logradouros extends JpaRepository<Logradouro,Long>, LogradourosQuery {

}
