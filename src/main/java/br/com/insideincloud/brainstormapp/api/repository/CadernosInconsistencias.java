package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.CadernoInconsistencia;
import br.com.insideincloud.brainstormapp.api.repository.cadernoinconsistencia.CadernosInconsistenciasQuery;
import br.com.insideincloud.brainstormapp.api.repository.filter.CadernoInconsistenciaFilter;

@Repository
public interface CadernosInconsistencias extends JpaRepository<CadernoInconsistencia,Long>, CadernosInconsistenciasQuery {
	public Page<CadernoInconsistencia> filtrar(CadernoInconsistenciaFilter filtro, Pageable page);

}
