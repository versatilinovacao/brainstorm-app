package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class AlunoViewPK implements Serializable {
	private static final long serialVersionUID = -3301200252444751919L;
	
	private Long Colaborador_id;
	private Long Composicao_id;
	
	public Long getColaborador_id() {
		return Colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		Colaborador_id = colaborador_id;
	}
	public Long getComposicao_id() {
		return Composicao_id;
	}
	public void setComposicao_id(Long composicao_id) {
		Composicao_id = composicao_id;
	}
	
	
	
}
