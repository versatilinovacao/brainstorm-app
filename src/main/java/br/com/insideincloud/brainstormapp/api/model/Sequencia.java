package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sequencias",schema="public")
public class Sequencia implements Serializable {
	private static final long serialVersionUID = 1862165422454697164L;

	@Id
	private String sequencia;
	private Long sequencia_id;
	
	public String getSequencia() {
		return sequencia;
	}
	public void setSequencia(String sequencia) {
		this.sequencia = sequencia;
	}
	public Long getSequencia_id() {
		return sequencia_id;
	}
	public void setSequencia_id(Long sequencia_id) {
		this.sequencia_id = sequencia_id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sequencia == null) ? 0 : sequencia.hashCode());
		result = prime * result + ((sequencia_id == null) ? 0 : sequencia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sequencia other = (Sequencia) obj;
		if (sequencia == null) {
			if (other.sequencia != null)
				return false;
		} else if (!sequencia.equals(other.sequencia))
			return false;
		if (sequencia_id == null) {
			if (other.sequencia_id != null)
				return false;
		} else if (!sequencia_id.equals(other.sequencia_id))
			return false;
		return true;
	}
	
}
