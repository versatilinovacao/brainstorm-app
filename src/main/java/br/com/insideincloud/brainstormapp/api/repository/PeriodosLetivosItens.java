package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.PeriodoLetivoItem;
import br.com.insideincloud.brainstormapp.api.repository.filter.PeriodoLetivoItemFilter;
import br.com.insideincloud.brainstormapp.api.repository.periodoletivoitem.PeriodosLetivosItensQuery;

@Repository
public interface PeriodosLetivosItens extends JpaRepository<PeriodoLetivoItem, Long>, PeriodosLetivosItensQuery {
	public Page<PeriodoLetivoItem> filtrar(PeriodoLetivoItemFilter filtro,Pageable page);
	
	@Query("select p from PeriodoLetivoItem p where p.periodoletivo.periodo_letivo_id = ?1")
	public List<PeriodoLetivoItem> findByPeriodosLetivosItens(Long periodoLetivoItem);
	
	
}
