package br.com.insideincloud.brainstormapp.api.soap.homologacao.cachoeirinha;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.inf.thema.nfse.server package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RecepcionarLoteRpsDocumentoResponseReturn_QNAME = new QName("http://server.nfse.thema.inf.br", "return");
    private final static QName _RecepcionarLoteRpsDocumentoXml_QNAME = new QName("http://server.nfse.thema.inf.br", "xml");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.inf.thema.nfse.server
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsResponse }
     * 
     */
    public RecepcionarLoteRpsResponse createRecepcionarLoteRpsResponse() {
        return new RecepcionarLoteRpsResponse();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsLimitado }
     * 
     */
    public RecepcionarLoteRpsLimitado createRecepcionarLoteRpsLimitado() {
        return new RecepcionarLoteRpsLimitado();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsLimitadoResponse }
     * 
     */
    public RecepcionarLoteRpsLimitadoResponse createRecepcionarLoteRpsLimitadoResponse() {
        return new RecepcionarLoteRpsLimitadoResponse();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRps }
     * 
     */
    public RecepcionarLoteRps createRecepcionarLoteRps() {
        return new RecepcionarLoteRps();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsDocumentoResponse }
     * 
     */
    public RecepcionarLoteRpsDocumentoResponse createRecepcionarLoteRpsDocumentoResponse() {
        return new RecepcionarLoteRpsDocumentoResponse();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsDocumento }
     * 
     */
    public RecepcionarLoteRpsDocumento createRecepcionarLoteRpsDocumento() {
        return new RecepcionarLoteRpsDocumento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "return", scope = RecepcionarLoteRpsDocumentoResponse.class)
    public JAXBElement<String> createRecepcionarLoteRpsDocumentoResponseReturn(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoResponseReturn_QNAME, String.class, RecepcionarLoteRpsDocumentoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "xml", scope = RecepcionarLoteRpsDocumento.class)
    public JAXBElement<String> createRecepcionarLoteRpsDocumentoXml(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoXml_QNAME, String.class, RecepcionarLoteRpsDocumento.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "xml", scope = RecepcionarLoteRpsLimitado.class)
    public JAXBElement<String> createRecepcionarLoteRpsLimitadoXml(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoXml_QNAME, String.class, RecepcionarLoteRpsLimitado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "return", scope = RecepcionarLoteRpsLimitadoResponse.class)
    public JAXBElement<String> createRecepcionarLoteRpsLimitadoResponseReturn(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoResponseReturn_QNAME, String.class, RecepcionarLoteRpsLimitadoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "return", scope = RecepcionarLoteRpsResponse.class)
    public JAXBElement<String> createRecepcionarLoteRpsResponseReturn(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoResponseReturn_QNAME, String.class, RecepcionarLoteRpsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.nfse.thema.inf.br", name = "xml", scope = RecepcionarLoteRps.class)
    public JAXBElement<String> createRecepcionarLoteRpsXml(String value) {
        return new JAXBElement<String>(_RecepcionarLoteRpsDocumentoXml_QNAME, String.class, RecepcionarLoteRps.class, value);
    }

}
