package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.ArtigoArtefato;

@Repository
public interface ArtigosArtefatos extends JpaRepository<ArtigoArtefato, Long> {

}
