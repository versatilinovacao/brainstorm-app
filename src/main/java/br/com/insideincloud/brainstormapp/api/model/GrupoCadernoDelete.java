package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grupo_cadernos_delete",schema="action")
public class GrupoCadernoDelete implements Serializable {
	private static final long serialVersionUID = -629106170520011324L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long grupo_caderno_delete_id;
	private boolean deletar;
	
	public Long getGrupo_caderno_delete_id() {
		return grupo_caderno_delete_id;
	}
	public void setGrupo_caderno_delete_id(Long grupo_caderno_delete_id) {
		this.grupo_caderno_delete_id = grupo_caderno_delete_id;
	}
	public boolean isDeletar() {
		return deletar;
	}
	public void setDeletar(boolean deletar) {
		this.deletar = deletar;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (deletar ? 1231 : 1237);
		result = prime * result + ((grupo_caderno_delete_id == null) ? 0 : grupo_caderno_delete_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoCadernoDelete other = (GrupoCadernoDelete) obj;
		if (deletar != other.deletar)
			return false;
		if (grupo_caderno_delete_id == null) {
			if (other.grupo_caderno_delete_id != null)
				return false;
		} else if (!grupo_caderno_delete_id.equals(other.grupo_caderno_delete_id))
			return false;
		return true;
	}
	
	
	
}
