package br.com.insideincloud.brainstormapp.api.repository.contato;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.insideincloud.brainstormapp.api.model.Contato;
import br.com.insideincloud.brainstormapp.api.repository.filter.ContatoFilter;

public interface ContatosQuery {
	public Page<Contato> filtrar(ContatoFilter filtro, Pageable page);
}
