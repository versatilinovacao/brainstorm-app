package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadaViewFilter {

	private String chamada_aowner_id;
	private String evento;
	private String chamada_id;
	private String caderno_id;
	private String aluno_id;
	private String nome_aluno;
	private String conceito;
	private String nota;
	
	public String getChamada_aowner_id() {
		return chamada_aowner_id;
	}
	public void setChamada_aowner_id(String chamada_aowner_id) {
		this.chamada_aowner_id = chamada_aowner_id;
	}
	public String getEvento() {
		return evento;
	}
	public void setEvento(String evento) {
		this.evento = evento;
	}
	public String getChamada_id() {
		return chamada_id;
	}
	public void setChamada_id(String chamada_id) {
		this.chamada_id = chamada_id;
	}
	public String getCaderno_id() {
		return caderno_id;
	}
	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}
	public String getAluno_id() {
		return aluno_id;
	}
	public void setAluno_id(String aluno_id) {
		this.aluno_id = aluno_id;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public String getConceito() {
		return conceito;
	}
	public void setConceito(String conceito) {
		this.conceito = conceito;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	
}
