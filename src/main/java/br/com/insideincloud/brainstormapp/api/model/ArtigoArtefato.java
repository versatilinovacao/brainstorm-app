package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="artigos",schema="almoxarifado")
public class ArtigoArtefato implements Serializable {
	private static final long serialVersionUID = -5027091864123383563L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long artigo_id;
	private String nome;
	
	public Long getArtigo_id() {
		return artigo_id;
	}
	public void setArtigo_id(Long artigo_id) {
		this.artigo_id = artigo_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artigo_id == null) ? 0 : artigo_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArtigoArtefato other = (ArtigoArtefato) obj;
		if (artigo_id == null) {
			if (other.artigo_id != null)
				return false;
		} else if (!artigo_id.equals(other.artigo_id))
			return false;
		return true;
	}
	
}
