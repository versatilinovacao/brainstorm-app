package br.com.insideincloud.brainstormapp.api.resource;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.DeletarTelefoneAction;
import br.com.insideincloud.brainstormapp.api.model.Telefone;
import br.com.insideincloud.brainstormapp.api.model.TipoTelefone;
import br.com.insideincloud.brainstormapp.api.model.view.TelefoneAtivoView;
import br.com.insideincloud.brainstormapp.api.model.view.TelefoneColaboradorView;
import br.com.insideincloud.brainstormapp.api.model.view.TelefoneInativoView;
import br.com.insideincloud.brainstormapp.api.model.view.TelefoneView;
import br.com.insideincloud.brainstormapp.api.repository.DeletarTelefonesAction;
import br.com.insideincloud.brainstormapp.api.repository.Telefones;
import br.com.insideincloud.brainstormapp.api.repository.TelefonesAtivosView;
import br.com.insideincloud.brainstormapp.api.repository.TelefonesColaboradoresView;
import br.com.insideincloud.brainstormapp.api.repository.TelefonesInativosView;
import br.com.insideincloud.brainstormapp.api.repository.TelefonesView;
import br.com.insideincloud.brainstormapp.api.repository.TiposTelefones;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneColaboradorViewFilter;
import br.com.insideincloud.brainstormapp.api.repository.filter.TelefoneViewFilter;
import br.com.insideincloud.brainstormapp.api.resource.delete.TelefoneDelete;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;

@RestController
@RequestMapping("/telefones")
public class TelefoneResource {
	
	@Autowired
	private Telefones telefones;
	
	@Autowired
	private TiposTelefones tipostelefones;
	
	@Autowired 
	private TelefonesColaboradoresView telefones_colaboradores;
	
	@Autowired
	private DeletarTelefonesAction telefonesAction;
	
	@Autowired 
	private TelefonesView telefonesView;
	
	@Autowired
	private TelefonesAtivosView telefonesAtivos;
	
	@Autowired
	private TelefonesInativosView telefonesInativos;
	
	@GetMapping("/tipos")
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<TipoTelefone> tipos() {
		RetornoWrapper<TipoTelefone> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(tipostelefones.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	};
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<TelefoneAtivoView> conteudo() {
		RetornoWrapper<TelefoneAtivoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(telefonesAtivos.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;		
	};
	
	@PutMapping("/colaboradores")
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<TelefoneColaboradorView> conteudoTelefoneColaboradores(@RequestBody TelefoneColaboradorViewFilter filtro, @PageableDefault(size=1000) Pageable page) {
		RetornoWrapper<TelefoneColaboradorView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
//			retorno.setPage(telefones_colaboradores.filtrar(filtro, page));
			retorno.setConteudo(telefones_colaboradores.findByVinculosColaborador(Long.parseLong(filtro.getColaborador_id()), Long.parseLong(filtro.getComposicao_colaborador_id())));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os telefones do colaborador solicitado, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/inativos")
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<TelefoneInativoView> conteudoInativo() {
		RetornoWrapper<TelefoneInativoView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(telefonesInativos.findAll());
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar os telefones inativos, favor tentar novamente dentro de alguns instantes");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/colaboradoresview")
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<TelefoneView> conteudoView(@RequestBody TelefoneViewFilter filtro, @PageableDefault(size=1000) Pageable page) {
		RetornoWrapper<TelefoneView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setPage(telefonesView.filtrar(filtro, page));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno; 
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_CONTEUDO')")
	public RetornoWrapper<Telefone> buscarPeloCodigo(@PathVariable Long codigo) {
		RetornoWrapper<Telefone> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(telefones.findOne(codigo));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar o telefone pesquisado, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_SALVAR')") //TODO: Mudar para salvar depois
	public RetornoWrapper<Telefone> salvar(@RequestBody Telefone telefone, HttpServletResponse response) {
		RetornoWrapper<Telefone> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			telefone = telefones.saveAndFlush(telefone);
			retorno.setSingle(telefone);
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível retornar as informações solicitadas, favor tentar novamente dentro de alguns instantes!");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}").buildAndExpand(telefone.getTelefone_id()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		
		return retorno;
	}
	
	@DeleteMapping
	@PreAuthorize("hasAuthority('ROLE_TELEFONE_DELETAR')")
	public Page<Telefone> deletar(@RequestBody TelefoneDelete deleta,@PageableDefault(size=5) Pageable page, HttpServletResponse response) {
		for (Telefone telefone: deleta.getTelefones()) {
			DeletarTelefoneAction deletarTelefone = new DeletarTelefoneAction();
			deletarTelefone.setTelefone_id(telefone.getTelefone_id());
			
			telefonesAction.save(deletarTelefone);
		};
		
		return telefones.findAll(page);
		
	}

}

