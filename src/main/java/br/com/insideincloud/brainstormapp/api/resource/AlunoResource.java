package br.com.insideincloud.brainstormapp.api.resource;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.insideincloud.brainstormapp.api.excecoes.ExceptionWrapper;
import br.com.insideincloud.brainstormapp.api.model.Aluno;
import br.com.insideincloud.brainstormapp.api.model.Arquivo;
import br.com.insideincloud.brainstormapp.api.model.ArquivoPorAtividade;
import br.com.insideincloud.brainstormapp.api.model.Atividade;
import br.com.insideincloud.brainstormapp.api.model.Empresa;
import br.com.insideincloud.brainstormapp.api.model.OrientacaoAvaliacao;
import br.com.insideincloud.brainstormapp.api.model.pk.ColaboradorPK;
import br.com.insideincloud.brainstormapp.api.model.view.AlunoAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.ArquivoAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.ArquivoPorAtividadeView;
import br.com.insideincloud.brainstormapp.api.model.view.CadastroAlunoView;
import br.com.insideincloud.brainstormapp.api.repository.Alunos;
import br.com.insideincloud.brainstormapp.api.repository.AlunosAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.Arquivos;
import br.com.insideincloud.brainstormapp.api.repository.ArquivosAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.ArquivosPorAtividades;
import br.com.insideincloud.brainstormapp.api.repository.ArquivosPorAtividadesView;
import br.com.insideincloud.brainstormapp.api.repository.Atividades;
import br.com.insideincloud.brainstormapp.api.repository.CadastroAlunosView;
import br.com.insideincloud.brainstormapp.api.repository.Empresas;
import br.com.insideincloud.brainstormapp.api.repository.OrientacoesAvaliacoes;
import br.com.insideincloud.brainstormapp.api.resource.wrapper.RetornoWrapper;
import br.com.insideincloud.brainstormapp.api.service.AlunoService;

@RestController
@RequestMapping("/alunos")
public class AlunoResource {

	@Autowired
	private CadastroAlunosView alunos;
	
	@Autowired
	private Alunos alu;
	
	@Autowired
	private AlunosAtividadesView alunosAtividades;
	
	@Autowired
	private AlunoService alunoService;
	
	@Autowired
	private Empresas empresas;
	
	@Autowired
	private ArquivosAtividadesView arquivosAtividades;
	
	@Autowired
	private Atividades atividades;
	
	@Autowired
	private ArquivosPorAtividades arquivosPorAtividades;
	
	@Autowired
	private ArquivosPorAtividadesView arquivosPorAtividadesView;
	
	@Autowired
	private Arquivos arquivos;
	
	@Autowired
	private OrientacoesAvaliacoes orientacoes;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_ALUNO_CONTEUDO')")
	public RetornoWrapper<CadastroAlunoView> conteudoAtivo() {
		RetornoWrapper<CadastroAlunoView> retorno = new RetornoWrapper<CadastroAlunoView>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( alunos.findAll() );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foram localizados alunos, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/{id}/{composicao}")
	@PreAuthorize("hasAuthority('ROLE_ALUNO_CONTEUDO')")
	public RetornoWrapper<CadastroAlunoView> conteudoPorId(@PathVariable Long id, @PathVariable Long composicao) {
		RetornoWrapper<CadastroAlunoView> retorno = new RetornoWrapper<CadastroAlunoView>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			ColaboradorPK key = new ColaboradorPK();
			key.setColaborador_id(id);
			key.setComposicao_id(composicao);
			retorno.setSingle( alunos.findOne(key) );
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar o aluno, verifique sua informação e tente novamente.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/atividades/ambiente/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<AlunoAtividadeView> atividadesClientes(@PathVariable Long codigo) {
		RetornoWrapper<AlunoAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( alunoService.findAlunosAtividades(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar atividades para este aluno");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
		
	}
	
	@GetMapping("/atividades/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_ALUNO_CONTEUDO')")
	public RetornoWrapper<AlunoAtividadeView> conteudoAtividades(@PathVariable Long codigo) {
		RetornoWrapper<AlunoAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( alunoService.findAlunosAtividades(codigo) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar atividades para este aluno.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}

	@GetMapping("/atividadesabertas/{aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<AlunoAtividadeView> conteudoAtividadesPendente(@PathVariable Long aluno_id) {
		RetornoWrapper<AlunoAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo( alunoService.findAlunosAtividadesPendentes(aluno_id) );
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar atividades pendentes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	  

	
	@GetMapping("/ficha/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_ALUNO_CONTEUDO')")
	public ResponseEntity<byte[]> fichaReport(@PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id, HttpServletRequest request) {
		Empresa empresa = empresas.findOne(Long.parseLong(request.getHeader("Empresa-Type")));
		byte[] ficha = alunoService.fichaAluno(aluno_id,composicao_aluno_id,empresa);		
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_PDF_VALUE).body(ficha);
	}
	
	@GetMapping("/arquivos/{atividade_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<ArquivoAtividadeView> arquivosAtividades(@PathVariable Long atividade_id) {
		RetornoWrapper<ArquivoAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(arquivosAtividades.findByAtividade(atividade_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhum upload desta atividade.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
		
	}
	
	@GetMapping("/arquivosentregues/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<ArquivoPorAtividadeView> conteudoArquivosPorAtividade(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<ArquivoPorAtividadeView> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(arquivosPorAtividadesView.findByArquivos(atividade_id,aluno_id,composicao_aluno_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foram localizados arquivos para este aluno e atividade.");
			exception.setDebug(""+e);
			retorno.setException(exception);
			e.printStackTrace();
		}
		
		return retorno;
	}	
	
	@PostMapping("/uploadarquivo/{atividade_id}/{aluno_id}/{composicao_aluno_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_CONTEUDO')")
	public RetornoWrapper<ArquivoPorAtividade> salvarArquivo(@RequestParam MultipartFile file, @PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id) {
		RetornoWrapper<ArquivoPorAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		
		try {
			
			File convFile = new File(file.getOriginalFilename());
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(convFile);
				fos.write(file.getBytes());
				fos.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			String extensao = convFile.getName().substring( convFile.getName().indexOf(".")+1, convFile.getName().length() );
			String nome = convFile.getName().substring(0,convFile.getName().indexOf("."));

			Arquivo arquivo = new Arquivo();
			arquivo.setNome(nome);
			arquivo.setExtensao(extensao);
			arquivo.setRegistro(LocalDateTime.now());
			arquivo.setTamanho( file.getSize() );
			arquivo.setArquivo( Base64.encodeBase64String( file.getBytes() ) );
			
			arquivo = arquivos.save(arquivo);
			
			ColaboradorPK id = new ColaboradorPK();
			id.setColaborador_id(aluno_id);
			id.setComposicao_id(composicao_aluno_id);
			
			Atividade atividade = atividades.findOne(atividade_id);
			
			Aluno aluno = alu.findOne(id); 
			ArquivoPorAtividade arquivoPorAtividade = new ArquivoPorAtividade();
			arquivoPorAtividade.setAluno(aluno);
			arquivoPorAtividade.setAtividade(atividade);
			arquivoPorAtividade.setArquivo(arquivo);
			
			retorno.setSingle(arquivosPorAtividades.saveAndFlush( arquivoPorAtividade ));
			
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar o arquivo");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PutMapping("/deletararquivo/{atividade_id}/{aluno_id}/{composicao_aluno_id}/{arquivo_id}")
	@PreAuthorize("hasAuthority('ROLE_AMBIENTEALUNO_DELETAR')")
	public RetornoWrapper<ArquivoPorAtividade> deletar(@PathVariable Long atividade_id, @PathVariable Long aluno_id, @PathVariable Long composicao_aluno_id, @PathVariable Long arquivo_id) {
		RetornoWrapper<ArquivoPorAtividade> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			ArquivoPorAtividade arquivoPorAtividade = arquivosPorAtividades.findByArquivo(atividade_id, aluno_id, composicao_aluno_id, arquivo_id);
			Arquivo arquivo = arquivoPorAtividade.getArquivo();
			
			arquivosPorAtividades.delete(arquivoPorAtividade); 
			arquivos.delete(arquivo);
			
			retorno.setConteudo( arquivosPorAtividades.findAll() );
			
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível deletar o arquivo solicitado");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@GetMapping("/orientacoes/{avaliacao_id}")
	@PreAuthorize("hasAuthority('ROLE_ALUNO_CONTEUDO')")
	public RetornoWrapper<OrientacaoAvaliacao> conteudoOrientacoes(@PathVariable Long avaliacao_id) {
		RetornoWrapper<OrientacaoAvaliacao> retorno = new RetornoWrapper<OrientacaoAvaliacao>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setConteudo(orientacoes.findByAvaliacao(avaliacao_id));
		} catch(Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível localizar nenhuma orientação para esta avaliação;");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
	@PostMapping("/orientacao")
	@PreAuthorize("hasAuthority('ROLE_ALUNO_SALVAR')")
	public RetornoWrapper<OrientacaoAvaliacao> salvarOrientacao(@RequestBody OrientacaoAvaliacao orientacao) {
		RetornoWrapper<OrientacaoAvaliacao> retorno = new RetornoWrapper<>();
		ExceptionWrapper exception = new ExceptionWrapper();
		
		try {
			retorno.setSingle(orientacoes.saveAndFlush(orientacao));
		} catch (Exception e) {
			exception.setCodigo(1);
			exception.setMensagem("Não foi possível salvar a orientação, favor tentar novamente dentro de alguns instantes.");
			exception.setDebug(""+e);
			retorno.setException(exception);
		}
		
		return retorno;
	}
	
}
