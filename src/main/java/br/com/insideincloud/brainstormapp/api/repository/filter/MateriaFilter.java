package br.com.insideincloud.brainstormapp.api.repository.filter;

public class MateriaFilter {
	private String materia_id;
	private String descricao;
	private String notmateria_id;
	
	public String getNotmateria_id() {
		return notmateria_id;
	}
	public void setNotmateria_id(String notmateria_id) {
		this.notmateria_id = notmateria_id;
	}
	public String getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(String materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
