package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity @Table(name="itensordensservicos",schema="servico")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discriminador",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("ITEMORDEMSERVICO")
public class ItemOrdemServico implements Serializable {
	private static final long serialVersionUID = -179519511357690814L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long itemordemservico_id;
	@ManyToOne @JoinColumn(name="ordemservico_id")
	private OrdemServico ordemservico;
	@OneToOne @JoinColumn(name="artefato_id")
	private Artefato artefato;
	@OneToOne @JoinColumn(name="cnae_id")
	private Cnae cnae;
	private String descricao;
	private Double valor;
	private Double issqn;
	private Double basecalculo;
	private Double desconto;
	private Double valorliquido;
	
	public Long getItemordemservico_id() {
		return itemordemservico_id;
	}
	public void setItemordemservico_id(Long itemordemservico_id) {
		this.itemordemservico_id = itemordemservico_id;
	}
	public OrdemServico getOrdemservico() {
		return ordemservico;
	}
	public void setOrdemservico(OrdemServico ordemservico) {
		this.ordemservico = ordemservico;
	}
	public Artefato getArtefato() {
		return artefato;
	}
	public void setArtefato(Artefato artefato) {
		this.artefato = artefato;
	}
	public Cnae getCnae() {
		return cnae;
	}
	public void setCnae(Cnae cnae) {
		this.cnae = cnae;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Double getIssqn() {
		return issqn;
	}
	public void setIssqn(Double issqn) {
		this.issqn = issqn;
	}
	public Double getBasecalculo() {
		return basecalculo;
	}
	public void setBasecalculo(Double basecalculo) {
		this.basecalculo = basecalculo;
	}
	public Double getDesconto() {
		return desconto;
	}
	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}
	public Double getValorliquido() {
		return valorliquido;
	}
	public void setValorliquido(Double valorliquido) {
		this.valorliquido = valorliquido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemordemservico_id == null) ? 0 : itemordemservico_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemOrdemServico other = (ItemOrdemServico) obj;
		if (itemordemservico_id == null) {
			if (other.itemordemservico_id != null)
				return false;
		} else if (!itemordemservico_id.equals(other.itemordemservico_id))
			return false;
		return true;
	}
	
}
