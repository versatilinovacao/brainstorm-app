package br.com.insideincloud.brainstormapp.api.repository.filter;

public class CidadeFilter {
	
	private String cidade_id;
	private String reduzido;
	private String descricao;
	
	public String getCidade_id() {
		return cidade_id;
	}
	public void setCidade_id(String cidade_id) {
		this.cidade_id = cidade_id;
	}
	public String getReduzido() {
		return reduzido;
	}
	public void setReduzido(String reduzido) {
		this.reduzido = reduzido;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
