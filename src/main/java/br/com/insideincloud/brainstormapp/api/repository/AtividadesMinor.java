package br.com.insideincloud.brainstormapp.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.minor.AtividadeMinor;

@Repository
public interface AtividadesMinor extends JpaRepository<AtividadeMinor, Long> {
	
	@Query("select a from AtividadeMinor a where a.atividade_id = ?1 and a.aluno_id = ?2 and a.composicao_aluno_id = ?3")
	public AtividadeMinor findByAtividade(Long atividade_id, Long aluno_id, Long composicao_aluno_id);

}
