package br.com.insideincloud.brainstormapp.api.repository.filter;

public class ChamadaNavegadorFilter {
	private String caderno_id;
	private String hoje;

	public String getCaderno_id() {
		return caderno_id;
	}

	public void setCaderno_id(String caderno_id) {
		this.caderno_id = caderno_id;
	}

	public String getHoje() {
		return hoje;
	}

	public void setHoje(String hoje) {
		this.hoje = hoje;
	}
	
	

}
