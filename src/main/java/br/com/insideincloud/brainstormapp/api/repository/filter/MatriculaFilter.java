package br.com.insideincloud.brainstormapp.api.repository.filter;

public class MatriculaFilter {
	private String matricula_id;
	private String codigo;
	
	public String getMatricula_id() {
		return matricula_id;
	}
	public void setMatricula_id(String matricula_id) {
		this.matricula_id = matricula_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
