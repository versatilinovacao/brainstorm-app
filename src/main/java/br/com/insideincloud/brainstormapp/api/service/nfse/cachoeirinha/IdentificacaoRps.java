package br.com.insideincloud.brainstormapp.api.service.nfse.cachoeirinha;

public class IdentificacaoRps {
	private String Numero;
	private String Serie;
	private String Tipo;
	
	public String getNumero() {
		return Numero;
	}
	public void setNumero(String numero) {
		Numero = numero;
	}
	public String getSerie() {
		return Serie;
	}
	public void setSerie(String serie) {
		Serie = serie;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}

	
}
