package br.com.insideincloud.brainstormapp.api.model.pk;

import java.io.Serializable;

public class CertificadoA1PK implements Serializable {
	private static final long serialVersionUID = 491698687802207471L;

	private Long certificado_id;
	private Long empresa_id;
	public Long getCertificado_id() {
		return certificado_id;
	}
	public void setCertificado_id(Long certificado_id) {
		this.certificado_id = certificado_id;
	}
	public Long getEmpresa_id() {
		return empresa_id;
	}
	public void setEmpresa_id(Long empresa_id) {
		this.empresa_id = empresa_id;
	}
	
	
}
