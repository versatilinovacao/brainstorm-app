package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="materias_composta_view",schema="escola")
public class MateriaCompostaView implements Serializable {
	private static final long serialVersionUID = 5708067986363573989L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long materia_id;
	private String descricao;
	private Long aowner;
	
	public Long getMateria_id() {
		return materia_id;
	}
	public void setMateria_id(Long materia_id) {
		this.materia_id = materia_id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Long getAowner() {
		return aowner;
	}
	public void setAowner(Long aowner) {
		this.aowner = aowner;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((materia_id == null) ? 0 : materia_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MateriaCompostaView other = (MateriaCompostaView) obj;
		if (materia_id == null) {
			if (other.materia_id != null)
				return false;
		} else if (!materia_id.equals(other.materia_id))
			return false;
		return true;
	}
	
	
}
