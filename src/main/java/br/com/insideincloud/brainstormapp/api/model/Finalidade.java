package br.com.insideincloud.brainstormapp.api.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="finalidades",schema="recurso")
public class Finalidade implements Serializable {
	private static final long serialVersionUID = 6660040325529853914L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long finalidade_id;
	@Column(name="nome",length=100)
	private String nome;
	@Column(name="descricao",length=500)
	private String descricao;
	
	public Long getFinalidade_id() {
		return finalidade_id;
	}
	public void setFinalidade_id(Long finalidade_id) {
		this.finalidade_id = finalidade_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((finalidade_id == null) ? 0 : finalidade_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Finalidade other = (Finalidade) obj;
		if (finalidade_id == null) {
			if (other.finalidade_id != null)
				return false;
		} else if (!finalidade_id.equals(other.finalidade_id))
			return false;
		return true;
	}
	
	
}
