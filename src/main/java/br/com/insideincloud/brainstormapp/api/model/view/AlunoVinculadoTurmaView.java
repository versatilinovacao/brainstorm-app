package br.com.insideincloud.brainstormapp.api.model.view;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alunos_vinculados_turmas",schema="escola")
public class AlunoVinculadoTurmaView implements Serializable {
	private static final long serialVersionUID = 2200404303568118509L;

	@Id
	private Long alunovinculadoturma_id;
	private Long turma_id;
	private Long colaborador_id;
	private String nome;
	
	public Long getAlunovinculadoturma_id() {
		return alunovinculadoturma_id;
	}
	public void setAlunovinculadoturma_id(Long alunovinculadoturma_id) {
		this.alunovinculadoturma_id = alunovinculadoturma_id;
	}
	public Long getTurma_id() {
		return turma_id;
	}
	public void setTurma_id(Long turma_id) {
		this.turma_id = turma_id;
	}
	public Long getColaborador_id() {
		return colaborador_id;
	}
	public void setColaborador_id(Long colaborador_id) {
		this.colaborador_id = colaborador_id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}  
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alunovinculadoturma_id == null) ? 0 : alunovinculadoturma_id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoVinculadoTurmaView other = (AlunoVinculadoTurmaView) obj;
		if (alunovinculadoturma_id == null) {
			if (other.alunovinculadoturma_id != null)
				return false;
		} else if (!alunovinculadoturma_id.equals(other.alunovinculadoturma_id))
			return false;
		return true;
	}

}
