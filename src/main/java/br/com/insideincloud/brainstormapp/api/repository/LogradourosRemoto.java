package br.com.insideincloud.brainstormapp.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.insideincloud.brainstormapp.api.model.dblink.Logradouro;

@Repository
public interface LogradourosRemoto extends JpaRepository<Logradouro, String> {
	
	@Query("select l from Logradouro l where l.cep = ?1")
	public Logradouro findByEndereco(String cep);
	
	@Query("select l from Logradouro l where l.pais_id = ?1 and l.estado_id = ?2 order by l.descricaopais,l.descricaoestado")
	public List<Logradouro> findByEstado(Long pais_id, Long estado_id);
	
	@Query("select l from Logradouro l where l.pais_id = ?1 and l.estado_id = ?2 and l.cidade_id = ?3 order by l.descricaopais,l.descricaoestado,l.descricaocidade, l.nome")
	public List<Logradouro> findByEstadoCidade(Long pais_id, Long estado_id, Long cidade_id);

//	@Query("select l from Logradouro l where l.pais_id = ?1 and l.estado_id = ?2 and l.cidade_id = ?3 and l.nome like '%' || ?4 || '%'")
	@Query(value="select * from remoto.logradouros_view where pais_id = ?1 and estado_id = ?2 and cidade_id = ?3 and logradouro like '%' || ?4 || '%'",nativeQuery=true)
	public List<Logradouro> findByEstadoCidadeRua(Long pais_id, Long estado_id, Long cidade_id, String rua); 
}
